#!/bin/bash

max_bytes=0
count=0
tcount=0

trap ctrl_c INT

function ctrl_c() {
            date
            echo "max bytes so far $max_bytes"
            (( count += 1 ))                              # Increment.
            if [ "$count" -gt 10 ]
            then
                exit 
            fi
}

for i in `seq 1 1000`; do 
    bytes=`cat /sys/module/dmbufio/parameters/current_allocated_bytes`
    if [ "$bytes" -gt "$max_bytes" ]
    then
    max_bytes=$bytes
    fi
    sleep 1
    (( tcount += 1 ))                              # Increment.
    if [ "$tcount" -gt 2 ]
    then
        date
        echo "current max bytes $max_bytes"
        tcount=0
    fi
done
