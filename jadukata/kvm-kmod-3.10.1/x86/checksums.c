
#ifndef __KERNEL__
#ifdef BSD
#include <sys/types.h>
#else
#include <linux/types.h>
#include <linux/string.h>
#endif
#endif

uint32_t murmur_hash2( const void * key, int len, uint32_t csum){
  // 'm' and 'r' are mixing constants generated offline.
  // They're not really 'magic', they just happen to work well.

  const uint32_t m = 0x5bd1e995;
  const int r = 24;

  // Initialize the hash to a 'random' value

  uint32_t h = csum;

  // Mix 4 bytes at a time into the hash

  const unsigned char * data = (const unsigned char *)key;

  while(len >= 4)
  {
    uint32_t k = *(uint32_t*)data;

    k *= m;
    k ^= k >> r;
    k *= m;

    h *= m;
    h ^= k;

    data += 4;
    len -= 4;
  }

  // Handle the last few bytes of the input array

  switch(len)
  {
  case 3: h ^= data[2] << 16;
  case 2: h ^= data[1] << 8;
  case 1: h ^= data[0];
      h *= m;
  };

  // Do a few final mixes of the hash to ensure the last few
  // bytes are well-incorporated.

  h ^= h >> 13;
  h *= m;
  h ^= h >> 15;

  return h;
}

#define FORCE_INLINE inline __attribute__((always_inline))

FORCE_INLINE uint32_t rotl32 ( uint32_t x, int8_t r )
{
  return (x << r) | (x >> (32 - r));
}

// Finalization mix - force all bits of a hash block to avalanche

FORCE_INLINE uint32_t fmix32 ( uint32_t h )
{
  h ^= h >> 16;
  h *= 0x85ebca6b;
  h ^= h >> 13;
  h *= 0xc2b2ae35;
  h ^= h >> 16;

  return h;
}

//-----------------------------------------------------------------------------


unsigned int murmur_hash3(const void * key, int len, unsigned int csum){
  const uint8_t * data = (const uint8_t*)key;
  const int nblocks = len / 4;

  uint32_t h1 = csum;

  const uint32_t c1 = 0xcc9e2d51;
  const uint32_t c2 = 0x1b873593;

  //----------
  // body

  const uint32_t * blocks = (const uint32_t *)(data + nblocks*4);
  const uint8_t * tail;
  uint32_t k1;

  int i;
  for(i = -nblocks; i; i++)
  {
    uint32_t k1 = blocks[i];

    k1 *= c1;
    k1 = rotl32(k1,15);
    k1 *= c2;
    
    h1 ^= k1;
    h1 = rotl32(h1,13); 
    h1 = h1*5+0xe6546b64;
  }

  //----------
  // tail

  tail = (const uint8_t*)(data + nblocks*4);

  k1 = 0;

  switch(len & 3)
  {
  case 3: k1 ^= tail[2] << 16;
  case 2: k1 ^= tail[1] << 8;
  case 1: k1 ^= tail[0];
          k1 *= c1; k1 = rotl32(k1,15); k1 *= c2; h1 ^= k1;
  };

  //----------
  // finalization

  h1 ^= len;

  h1 = fmix32(h1);
    return h1;
}

uint32_t crc32(const unsigned char *data, int len, uint32_t crc){
    static const unsigned int table[256] = {
        0x00000000U,0x04C11DB7U,0x09823B6EU,0x0D4326D9U,
        0x130476DCU,0x17C56B6BU,0x1A864DB2U,0x1E475005U,
        0x2608EDB8U,0x22C9F00FU,0x2F8AD6D6U,0x2B4BCB61U,
        0x350C9B64U,0x31CD86D3U,0x3C8EA00AU,0x384FBDBDU,
        0x4C11DB70U,0x48D0C6C7U,0x4593E01EU,0x4152FDA9U,
        0x5F15ADACU,0x5BD4B01BU,0x569796C2U,0x52568B75U,
        0x6A1936C8U,0x6ED82B7FU,0x639B0DA6U,0x675A1011U,
        0x791D4014U,0x7DDC5DA3U,0x709F7B7AU,0x745E66CDU,
        0x9823B6E0U,0x9CE2AB57U,0x91A18D8EU,0x95609039U,
        0x8B27C03CU,0x8FE6DD8BU,0x82A5FB52U,0x8664E6E5U,
        0xBE2B5B58U,0xBAEA46EFU,0xB7A96036U,0xB3687D81U,
        0xAD2F2D84U,0xA9EE3033U,0xA4AD16EAU,0xA06C0B5DU,
        0xD4326D90U,0xD0F37027U,0xDDB056FEU,0xD9714B49U,
        0xC7361B4CU,0xC3F706FBU,0xCEB42022U,0xCA753D95U,
        0xF23A8028U,0xF6FB9D9FU,0xFBB8BB46U,0xFF79A6F1U,
        0xE13EF6F4U,0xE5FFEB43U,0xE8BCCD9AU,0xEC7DD02DU,
        0x34867077U,0x30476DC0U,0x3D044B19U,0x39C556AEU,
        0x278206ABU,0x23431B1CU,0x2E003DC5U,0x2AC12072U,
        0x128E9DCFU,0x164F8078U,0x1B0CA6A1U,0x1FCDBB16U,
        0x018AEB13U,0x054BF6A4U,0x0808D07DU,0x0CC9CDCAU,
        0x7897AB07U,0x7C56B6B0U,0x71159069U,0x75D48DDEU,
        0x6B93DDDBU,0x6F52C06CU,0x6211E6B5U,0x66D0FB02U,
        0x5E9F46BFU,0x5A5E5B08U,0x571D7DD1U,0x53DC6066U,
        0x4D9B3063U,0x495A2DD4U,0x44190B0DU,0x40D816BAU,
        0xACA5C697U,0xA864DB20U,0xA527FDF9U,0xA1E6E04EU,
        0xBFA1B04BU,0xBB60ADFCU,0xB6238B25U,0xB2E29692U,
        0x8AAD2B2FU,0x8E6C3698U,0x832F1041U,0x87EE0DF6U,
        0x99A95DF3U,0x9D684044U,0x902B669DU,0x94EA7B2AU,
        0xE0B41DE7U,0xE4750050U,0xE9362689U,0xEDF73B3EU,
        0xF3B06B3BU,0xF771768CU,0xFA325055U,0xFEF34DE2U,
        0xC6BCF05FU,0xC27DEDE8U,0xCF3ECB31U,0xCBFFD686U,
        0xD5B88683U,0xD1799B34U,0xDC3ABDEDU,0xD8FBA05AU,
        0x690CE0EEU,0x6DCDFD59U,0x608EDB80U,0x644FC637U,
        0x7A089632U,0x7EC98B85U,0x738AAD5CU,0x774BB0EBU,
        0x4F040D56U,0x4BC510E1U,0x46863638U,0x42472B8FU,
        0x5C007B8AU,0x58C1663DU,0x558240E4U,0x51435D53U,
        0x251D3B9EU,0x21DC2629U,0x2C9F00F0U,0x285E1D47U,
        0x36194D42U,0x32D850F5U,0x3F9B762CU,0x3B5A6B9BU,
        0x0315D626U,0x07D4CB91U,0x0A97ED48U,0x0E56F0FFU,
        0x1011A0FAU,0x14D0BD4DU,0x19939B94U,0x1D528623U,
        0xF12F560EU,0xF5EE4BB9U,0xF8AD6D60U,0xFC6C70D7U,
        0xE22B20D2U,0xE6EA3D65U,0xEBA91BBCU,0xEF68060BU,
        0xD727BBB6U,0xD3E6A601U,0xDEA580D8U,0xDA649D6FU,
        0xC423CD6AU,0xC0E2D0DDU,0xCDA1F604U,0xC960EBB3U,
        0xBD3E8D7EU,0xB9FF90C9U,0xB4BCB610U,0xB07DABA7U,
        0xAE3AFBA2U,0xAAFBE615U,0xA7B8C0CCU,0xA379DD7BU,
        0x9B3660C6U,0x9FF77D71U,0x92B45BA8U,0x9675461FU,
        0x8832161AU,0x8CF30BADU,0x81B02D74U,0x857130C3U,
        0x5D8A9099U,0x594B8D2EU,0x5408ABF7U,0x50C9B640U,
        0x4E8EE645U,0x4A4FFBF2U,0x470CDD2BU,0x43CDC09CU,
        0x7B827D21U,0x7F436096U,0x7200464FU,0x76C15BF8U,
        0x68860BFDU,0x6C47164AU,0x61043093U,0x65C52D24U,
        0x119B4BE9U,0x155A565EU,0x18197087U,0x1CD86D30U,
        0x029F3D35U,0x065E2082U,0x0B1D065BU,0x0FDC1BECU,
        0x3793A651U,0x3352BBE6U,0x3E119D3FU,0x3AD08088U,
        0x2497D08DU,0x2056CD3AU,0x2D15EBE3U,0x29D4F654U,
        0xC5A92679U,0xC1683BCEU,0xCC2B1D17U,0xC8EA00A0U,
        0xD6AD50A5U,0xD26C4D12U,0xDF2F6BCBU,0xDBEE767CU,
        0xE3A1CBC1U,0xE760D676U,0xEA23F0AFU,0xEEE2ED18U,
        0xF0A5BD1DU,0xF464A0AAU,0xF9278673U,0xFDE69BC4U,
        0x89B8FD09U,0x8D79E0BEU,0x803AC667U,0x84FBDBD0U,
        0x9ABC8BD5U,0x9E7D9662U,0x933EB0BBU,0x97FFAD0CU,
        0xAFB010B1U,0xAB710D06U,0xA6322BDFU,0xA2F33668U,
        0xBCB4666DU,0xB8757BDAU,0xB5365D03U,0xB1F740B4U,
    };
    while (len > 0)
    {
        crc = table[*data ^ ((crc >> 24) & 0xff)] ^ (crc << 8);
        data++;
        len--;
    }
    return crc;
}

unsigned int ELFHash(const char* str, unsigned int len, unsigned int csum)
{
   unsigned int hash = csum;
   unsigned int x    = 0;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash = (hash << 4) + (*str);
      if((x = hash & 0xF0000000L) != 0)
      {
         hash ^= (x >> 24);
      }
      hash &= ~x;
   }

   return hash;
}

unsigned long fnvob64l[1] =  {0xCBF29CE484222325UL};
char* fnvob64 = (char*)&fnvob64l;

inline unsigned long FNVHashfold64(const char* hash){
    unsigned long ret;
    asm volatile(
            "movq    %1,%%rdi\n\t"
            "movq    (%%rdi),%%rax\n\t"
           // "movabsq $0x100000000,%%rcx\n\t"
           // "xor     %%rdx, %%rdx\n\t"
           // "divq    %%rcx\n\t"
           // "movq    %%rdx,%%rax\n\t"
            "movq    %%rax,%0\n\t"
            : "=m"(ret)
            : "m"(hash)
            : "rsi", "rdi", "rdx"
            );
    return ret;
}

void FNVHash64loop(const char* str, char* csum, unsigned long size){
    asm volatile(
            "movq    %2,%%rdi\n\t"
            "movq    (%%rdi),%%rax\n\t"
            "movq    %0,%%rsi\n\t"
            "movabsq $0x0,%%rdi\n\t"
            "movq    %1,%%rcx\n\t"
            "movabsq $0x100000001B3,%%rbx\n\t"
            "1:\n\t"
            "cmpq    %%rcx,%%rdi\n\t"
            "jge     2f\n\t"
            "xorq    (%%rsi,%%rdi,1),%%rax\n\t"
            "mulq    %%rbx\n\t"
            "addq    $8,%%rdi\n\t"
            "jmp     1b\n\t"
            "2:\n\t"
            "movq    %2,%%rdi\n\t"
            "movq    %%rax,(%%rdi)\n\t"
            : 
            : "m"(str), "m"(size), "m"(csum)
            : "%rdi", "%rsi", "%rbx", "%rcx"
            );
}

inline void FNVHash64(const char* str, char* csum)
{
    asm volatile(
            "movq    %1,%%rdi\n\t"
            "movq    (%%rdi),%%rax\n\t"
            "movq    %0,%%rsi\n\t"
            "xorq    (%%rsi),%%rax\n\t"
            "movabsq $0x100000001B3,%%rbx\n\t"
            "mulq    %%rbx\n\t"
            "movq    %%rax,(%%rdi)\n\t"
            :
            : "m"(str), "m"(csum)
            : "rsi", "rdi", "rbx"
            );
}

unsigned long fnvprime128l[2] = {0x0000000001000000UL , 0x000000000000013BUL};
unsigned long fnvob128l[2] = {0x6C62272E07BB0142UL, 0x62B821756295C58DUL};

char* fnvob128 = (char*)&fnvob128l;
char* fnvprime128 = (char*)&fnvprime128l;

inline unsigned long FNVHashfold128(const char* hash){
    unsigned long ret;
    asm volatile(
            "movq    %1,%%rcx\n\t"
            "movups  (%%rcx), %%xmm0\n\t"
            "movhlps %%xmm0, %%xmm1\n\t"
            "movq    %%xmm0, %%rax\n\t"
            "movq    %%xmm1, %%rcx\n\t"
            "movabsq $0x1591aefa5e7e5a17,%%r8\n\t"
            "movabsq $0x2bb6863566c4e761, %%r9\n\t"
            "mul     %%r8\n\t"
            "imul    %%r9, %%rcx\n\t"
            "add     %%rdx, %%rcx\n\t"
            "xor     %%rcx, %%rax\n\t"
            "mul     %%r8\n\t"
            "imul    %%r9, %%rcx\n\t"
            "add     %%rdx, %%rcx\n\t"
            "xor     %%rcx, %%rax\n\t"
            "mul     %%r8\n\t"
            "imul    %%r9, %%rcx\n\t"
            "add     %%rdx, %%rcx\n\t"
            "xor     %%rcx, %%rax\n\t"
         //   "movabsq $0x100000000, %%rcx\n\t"
         //   "xor     %%rdx, %%rdx\n\t"
         //   "divq    %%rcx\n\t"
         //   "movq    %%rdx,%%rax\n\t"
            "movq    %%rax,%0\n\t"
            : "=m"(ret)
            : "m"(hash)
            :  "rcx", "rdx", "r8"
            );
    return ret;
}

void FNVHash128loop(const char* str, char* csum, unsigned long size){
    asm volatile(
            "movq    %2,%%rdi\n\t"
            "movups  (%%rdi), %%xmm0\n\t"
            "movq    %3, %%rdi\n\t"
            "movups  (%%rdi), %%xmm2\n\t"
            "movq    %0,%%rsi\n\t"
            "movabsq $0x0,%%rdi\n\t"
            "movq    %1,%%rcx\n\t"
            "1:\n\t"
            "cmpq    %%rcx,%%rdi\n\t"
            "jge     2f\n\t"
            "movups  (%%rsi,%%rdi,1), %%xmm1\n\t"
            "xorps    %%xmm1, %%xmm0\n\t"
            "mulps   %%xmm2, %%xmm0\n\t"
            "addq    $16,%%rdi\n\t"
            "jmp     1b\n\t"
            "2:\n\t"
            "movq    %2, %%rdi\n\t"
            "movups  %%xmm0, (%%rdi)\n\t"
            : 
            : "m"(str), "m"(size), "m"(csum), "m" (fnvprime128)
            : "%rdi", "%rsi", "%rcx"
            );
}

inline void FNVHash128(const char* str, char* csum){   
    asm volatile(
            "movq    %0, %%rcx\n\t"
            "movups  (%%rcx), %%xmm0\n\t"
            "movq    %1, %%rcx\n\t"
            "movups  (%%rcx), %%xmm1\n\t"
            "xorps   %%xmm1, %%xmm0\n\t"
            "movq    %2, %%rcx\n\t"
            "movups  (%%rcx), %%xmm1\n\t"
            "mulps   %%xmm1, %%xmm0\n\t"
            "movq    %1, %%rcx\n\t"
            "movups  %%xmm0, (%%rcx)\n\t"
            :
            : "m"(str), "m"(csum), "m" (fnvprime128)
            :  "rcx"
            );
}

unsigned int FNVHash(const char* str, unsigned int len, unsigned int csum)
{
   unsigned int hash      = csum;
   unsigned int i         = 0;
   
   for(i = 0; i < len; str++, i++)
   {
      hash = (hash * 0x811C9DC5) ^ (*str);
   }

   return hash;
}

unsigned int BKDRHash(const char* str, unsigned int len, unsigned int csum)
{
   unsigned int seed = 131; // 31 131 1313 13131 131313 etc.. 
   unsigned int hash = csum;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash = (hash * seed) + (*str);
   }

   return hash;
}


/*
unsigned int RSHash(char* str, unsigned int len)
{
   unsigned int b    = 378551;
   unsigned int a    = 63689;
   unsigned int hash = 0;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash = hash * a + (*str);
      a    = a * b;
   }

   return hash;
}


unsigned int JSHash(char* str, unsigned int len)
{
   unsigned int hash = 1315423911;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash ^= ((hash << 5) + (*str) + (hash >> 2));
   }

   return hash;
}


unsigned int PJWHash(char* str, unsigned int len)
{
   const unsigned int BitsInUnsignedInt = (unsigned int)(sizeof(unsigned int) * 8);
   const unsigned int ThreeQuarters     = (unsigned int)((BitsInUnsignedInt  * 3) / 4);
   const unsigned int OneEighth         = (unsigned int)(BitsInUnsignedInt / 8);
   const unsigned int HighBits          = (unsigned int)(0xFFFFFFFF) << (BitsInUnsignedInt - OneEighth);
   unsigned int hash              = 0;
   unsigned int test              = 0;
   unsigned int i                 = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash = (hash << OneEighth) + (*str);

      if((test = hash & HighBits)  != 0)
      {
         hash = (( hash ^ (test >> ThreeQuarters)) & (~HighBits));
      }
   }

   return hash;
}

unsigned int SDBMHash(char* str, unsigned int len)
{
   unsigned int hash = 0;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash = (*str) + (hash << 6) + (hash << 16) - hash;
   }

   return hash;
}


unsigned int DJBHash(char* str, unsigned int len)
{
   unsigned int hash = 5381;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash = ((hash << 5) + hash) + (*str);
   }

   return hash;
}


unsigned int BPHash(char* str, unsigned int len)
{
   unsigned int hash = 0;
   unsigned int i    = 0;
   for(i = 0; i < len; str++, i++)
   {
      hash = hash << 7 ^ (*str);
   }

   return hash;
}


unsigned int APHash(char* str, unsigned int len)
{
   unsigned int hash = 0xAAAAAAAA;
   unsigned int i    = 0;

   for(i = 0; i < len; str++, i++)
   {
      hash ^= ((i & 1) == 0) ? (  (hash <<  7) ^ (*str) * (hash >> 3)) :
                               (~((hash << 11) + ((*str) ^ (hash >> 5))));
   }

   return hash;
}

*/
