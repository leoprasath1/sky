#include "hash2.h"
#include <linux/hash.h>

//#define DF_HASH_TIME 1
#undef DF_HASH_TIME

//struct kmem_cache *hashtableEntry_cache = NULL;
//static DEFINE_SPINLOCK(hashtableEntry_cache_lock);

#ifdef MEMORY_ACCOUNTING
extern atomic_t hash_allocs, hash_frees;
#endif

//NOTE: use kmalloc because we are sharing hash tables across two modules with duplicated code 
//all memory state is shared except for ht_count ( all hash tables created in that module must be destroyed there)
void hash_init(void)
{
    /*
    spin_lock(&hashtableEntry_cache_lock);
    if(hashtableEntry_cache == NULL){
        hashtableEntry_cache = kmem_cache_create("htcache",sizeof(hashtableEntry),0,SLAB_RED_ZONE|SLAB_POISON, NULL);
    }
    if(hashtableEntry_cache == NULL)
        hpanic("ERROR : could not create kmem_cache\n");
    spin_unlock(&hashtableEntry_cache_lock);
    */
}

hashtableEntry* get_hashtableEntry()
{
    void *object = NULL;
    //spin_lock(&hashtableEntry_cache_lock);
    object = my_kmalloc(sizeof(hashtableEntry), GFP_ATOMIC);
    //object = kmem_cache_alloc( hashtableEntry_cache, GFP_ATOMIC);
    if(!object) hpanic("ERROR : could not allocate hash entry\n");
    //spin_unlock(&hashtableEntry_cache_lock);
#ifdef MEMORY_ACCOUNTING
    atomic_inc(&hash_allocs);
#endif
    return (hashtableEntry*)object;
}

void put_hashtableEntry(hashtableEntry* object)
{
    if(object){
        my_kfree(object,sizeof(hashtableEntry));
        //kmem_cache_free(hashtableEntry_cache, object);
    }
#ifdef MEMORY_ACCOUNTING
    atomic_inc(&hash_frees);
#endif
}

void hash_cleanup(void)
{
    /*
    spin_lock(&hashtableEntry_cache_lock);
    if(hashtableEntry_cache != NULL){
        kmem_cache_destroy(hashtableEntry_cache);
        hashtableEntry_cache = NULL;
    }
    spin_unlock(&hashtableEntry_cache_lock);
    */
}

int df_list_lookup(hashtableEntry *head, KEYDT key, VALDT *val)
{
    hashtableEntry *tmp = head;
    for(tmp = head; tmp; tmp = tmp->next) {
        if (tmp->key == key) {
            *val = tmp->data;
            return 1;
        }
    }
    return -1;
}

int df_list_add_sub(hashtableEntry *head, KEYDT key, VALDT val, VALDT *oldval)
{
    hashtableEntry *tmp = head;
    for(tmp = head; tmp; tmp = tmp->next) {
        if (tmp->key == key) {
            *oldval = tmp->data;
            tmp->data += val;
            return 1;
        }
    }
    return -1;
}

int df_list_update(hashtableEntry *head, KEYDT key, VALDT val, VALDT *oldval)
{
    hashtableEntry *tmp = head;
    for(tmp = head; tmp; tmp = tmp->next) {
        if (tmp->key == key) {
            *oldval = tmp->data;
            tmp->data = val;
            return 1;
        }
    }
    return -1;
}

int df_list_insert(hashtable *ht, hashtableEntry **head, KEYDT key, VALDT val)
{
    hashtableEntry *tmp = get_hashtableEntry();

    //hashtableEntry *tmp = df_get_hash_entry(ht);
    BUG_ON(!tmp);
    //if (!tmp)
    //	hpanic("ERROR : df_list_insert: kmalloc failed");
    tmp->key = key;
    tmp->data = val;
    tmp->next = *head;
    *head = tmp;
    return 1;
}

int df_list_delete(hashtableEntry **head, KEYDT key, VALDT *val)
{
    hashtableEntry *tmp = *head, *prev = *head;
    if (!tmp)
        return -1;

    if (tmp->key == key) {
        *head = tmp->next;
        *val = tmp->data;
        put_hashtableEntry(tmp);
        return 1;
    }
    for(; tmp; tmp = tmp->next) {
        if (tmp->key == key) {
            *val = tmp->data;
            prev->next = tmp->next;
            put_hashtableEntry(tmp);
            return 1;
        }
        prev = tmp;
    }
    return -1;
}


int df_list_free(hashtableEntry *head)
{
    hashtableEntry *tmp = head, *tmp1 = head;

    while(tmp) {
        tmp1 = tmp->next;
        put_hashtableEntry(tmp);
        tmp = tmp1;
    }
    return 1;
}

void df_list_print(hashtableEntry *head)
{
    hashtableEntry *tmp = head;
    for(; tmp; tmp = tmp->next)
        ht_printk(" %lu:%lu", tmp->key, tmp->data);
}

/* res will be 1 if the hashtable is empty */
int HTempty(hashtable * ht, int * res)
{
    if (ht == NULL)
        return STATUS_ERR;
    else {
        *res = (ht->size == 0);
        return STATUS_OK;
    }
}

int HTsize(hashtable *ht)
{
    return ht->size;
}

errorCode createHTscan(hashtable * ht)
{
    ht->cur_entry = ht->table[0];
    ht->cur_row = 0;
    ht->scan_active = 1;
    return 1;
}

int destroyHTscan(hashtable *ht)
{
    ht->cur_entry = NULL;
    ht->scan_active = 0;
    return 1;
}

int HTadvanceScanVal(hashtable * ht, KEYDT* key, VALDT* val)
{
    if (ht->busy)
        hpanic("ERROR : RACE CONDITION IN HASH\n");
    ht->busy = 1;

    if (!ht->cur_entry) {
        ht->cur_row++;
        while ((ht->cur_row < ht->nr_rows) && !ht->table[ht->cur_row])
            ht->cur_row++;

        if (ht->cur_row >= ht->nr_rows) {
            ht->busy = 0;
            return -1;
        }
        *key = (ht->table[ht->cur_row]->key);
        *val = (ht->table[ht->cur_row]->data);
        ht->cur_entry = ht->table[ht->cur_row]->next;
        ht->busy = 0;
        return 1;
    }

    *key = (ht->cur_entry->key);
    *val = (ht->cur_entry->data);
    ht->cur_entry = ht->cur_entry->next;
    ht->busy = 0;
    return 1;
}

int HTpopVal(hashtable *ht, KEYDT* key, VALDT* val)
{
    int cur_row = 0;
    while ((cur_row < ht->nr_rows) && !ht->table[cur_row])
        cur_row++;

    if (cur_row < ht->nr_rows) {
        *key = (ht->table[cur_row]->key);
        if(df_list_delete(&ht->table[cur_row], (*key), val) > 0){
            ht->size--;
            return 0;
        }
    }
    return -1;
}

/* How to distinguish between a null pointer and block number 0? */
errorCode compareBlockNumberNoPointer(void * k1, void * k2, int * result)
{
    unsigned long key1 = (unsigned long)k1;
    unsigned long key2 = (unsigned long)k2;

    if (key1 == key2)
        *result = 0;
    else
        *result = 1;
    return STATUS_OK;
}


errorCode createHashtable(int numbuckets, keyCompareFunction compare, deallocKeyFunction deallocK, deallocDataFunction deallocD, hashtable ** tmp_ht)
{
    int i = 0;
    unsigned long kmalloc_size = 0;
    hashtable *ht;

    if (!(ht = my_kmalloc(sizeof(hashtable), GFP_ATOMIC)))
        return STATUS_ERR;

    ht->size = 0;
    ht->scan_active = 0;
    ht->cur_entry = NULL;
    ht->next_free = 0;

    ht->nr_rows = numbuckets;
    ht->busy = 0;
    ht->customhash = NULL;

#define VMALLOC_THRESH (4096*10)
    
    //Leo: huge tables are not allocated in interrupt contexts 
    kmalloc_size = sizeof(hashtableEntry *) * ht->nr_rows;

    if(kmalloc_size < VMALLOC_THRESH){
        if (!(ht->table = my_kmalloc(sizeof(hashtableEntry *) * ht->nr_rows, GFP_ATOMIC)))
            ht_printk("ERROR: kmalloc failed !!!!!!!!!!!\n");
    }else{
        if (!(ht->table = my_vmalloc(sizeof(hashtableEntry *) * ht->nr_rows)))
            ht_printk("ERROR: vmalloc failed !!!!!!!!!!!\n");
    }

    for(i = 0; i < ht->nr_rows; i++)
        ht->table[i] = NULL;

    *tmp_ht = ht;
    return STATUS_OK;
}


/* deallocator is a function that knows how to deallocate data and keys */
errorCode deleteHashtable(hashtable ** tmp_ht)
{
    hashtable *ht = *tmp_ht;
    int i;
    unsigned long kmalloc_size = 0;

    if (!ht || !ht->table) {
        ht_printk("ERROR!! Hash table CORRUPT\n");
        return -1;
    }

    for(i = 0; i< ht->nr_rows; i++) {
        if (ht->table[i])
            df_list_free(ht->table[i]);
    }

    kmalloc_size = sizeof(hashtableEntry *) * ht->nr_rows;

    if(kmalloc_size < VMALLOC_THRESH){
        my_kfree(ht->table,sizeof(hashtableEntry*)*ht->nr_rows);
    }else{
        my_vfree(ht->table,sizeof(hashtableEntry*)*ht->nr_rows);
    }
    my_kfree(ht,sizeof(hashtable));
    *tmp_ht = NULL;
    return 1;
}

unsigned long inline default_hash(KEYDT a)
{
    return (((unsigned long)a)*GOLDEN_RATIO_PRIME);
}

unsigned int inline compute_hash(hashtable * ht, KEYDT key)
{
    if(ht->customhash){
        return ht->customhash(key)%(ht->nr_rows);
    }
    return (default_hash(key)%(ht->nr_rows));
}

static errorCode HTinsert_helper(hashtable * ht, KEYDT key, VALDT data, bool checkdup)
{
    VALDT val;
    int hash = compute_hash(ht, key);

#ifdef DF_HASH_TIME
    struct timeval s_tv, e_tv;
    do_gettimeofday(&s_tv);
#endif

    if (ht->busy)
        hpanic("ERROR : RACE CONDITION IN HASH\n");
    ht->busy = 1;

    if (hash < 0)
        hpanic("ERROR : Incorrect hash\n");

    //ht_printk("HTinsert: key = %d\n", (int)key);
    if(checkdup){
        if (df_list_lookup(ht->table[hash], (KEYDT)key, &val) >= 0) {
            ht->busy = 0;
            return STATUS_DUPL_ENTRY;
        }
    }

    df_list_insert(ht, &ht->table[hash], (KEYDT)key, (VALDT)data);
    ht->size++;
    ht->busy = 0;
#ifdef DF_HASH_TIME
    do_gettimeofday(&e_tv);
    ht_printk("HTinsert: time %d\n", diff_time(s_tv, e_tv));
#endif
    return 1;
}

errorCode HTinsertNoDup(hashtable * ht, KEYDT key, VALDT data){
    return HTinsert_helper(ht,key,data,false);
}

errorCode HTinsert(hashtable * ht, KEYDT key, VALDT data){
    return HTinsert_helper(ht,key,data,true);
}

errorCode HTextract(hashtable * ht, KEYDT key, VALDT* data)
{
    int hash = compute_hash(ht, key);
    int ret = -1;

#ifdef DF_HASH_TIME
    struct timeval s_tv, e_tv;
    do_gettimeofday(&s_tv);
#endif

    if (hash < 0)
        hpanic("ERROR : Incorrect hash\n");

    //ht_printk("HTextract: key = %d\n", (int)key);
    if (ht->scan_active && ht->cur_entry && (ht->cur_entry->key == (KEYDT)key)) {
        if (ht->cur_row != hash)
            hpanic("ERROR : duplicate entries detected in HTextract\n");
        ht->cur_entry = ht->cur_entry->next;
    }

    if (df_list_delete(&ht->table[hash], (KEYDT)key, (VALDT*)data) > 0) {
        ht->size--;
        ht->busy = 0;
        ret = 1;
    }
#ifdef DF_HASH_TIME
    do_gettimeofday(&e_tv);
    ht_printk("HTextract: time %d\n", diff_time(s_tv, e_tv));
#endif

    return ret;
}

errorCode HTlookup_first(hashtable * ht, KEYDT key, VALDT* data)
{
    int hash = compute_hash(ht, key);
    int ret = -1;

#ifdef DF_HASH_TIME
    struct timeval s_tv, e_tv;
    do_gettimeofday(&s_tv);
#endif


    if (hash < 0)
        hpanic("ERROR :Incorrect hash\n"); 

    //ht_printk("HTlookup: hash = %d, key = %d\n", hash, (int)key);
    if (df_list_lookup(ht->table[hash], (KEYDT)key, (VALDT*)data) > 0)
        ret = 1;

    //do_gettimeofday(&e_tv);
    //ht_printk("Time - HTlookup = %ld usec\n", diff_time(s_tv, e_tv));
#ifdef DF_HASH_TIME
    do_gettimeofday(&e_tv);
    ht_printk("HTlookup: time %d\n", diff_time(s_tv, e_tv));
#endif

    return ret;
}


errorCode HTlookup(hashtable * ht, KEYDT key, VALDT* data)
{
    int hash = compute_hash(ht, key);
    int ret = -1;

#ifdef DF_HASH_TIME
    struct timeval s_tv, e_tv;
    do_gettimeofday(&s_tv);
#endif


    if (hash < 0)
        hpanic("ERROR : Incorrect hash\n");

    //ht_printk("HTlookup: hash = %d, key = %d\n", hash, (int)key);
    if (df_list_lookup(ht->table[hash], (KEYDT)key, (VALDT*)data) > 0)
        ret = 1;

    //do_gettimeofday(&e_tv);
    //ht_printk("Time - HTlookup = %ld usec\n", diff_time(s_tv, e_tv));
#ifdef DF_HASH_TIME
    do_gettimeofday(&e_tv);
    ht_printk("HTlookup: time %d\n", diff_time(s_tv, e_tv));
#endif

    return ret;
}

int HTaddsub(hashtable * ht, KEYDT key, VALDT data, VALDT* old_data)
{
    int hash = compute_hash(ht, key);

#ifdef DF_HASH_TIME
    struct timeval s_tv, e_tv;
    do_gettimeofday(&s_tv);
#endif

    if (hash < 0)
        hpanic("ERROR : Incorrect hash\n");
    
    if (df_list_add_sub(ht->table[hash], key,data, old_data) > 0) {
        return 0;
    }else{
        df_list_insert(ht, &ht->table[hash], key, data);
        ht->size++;
        return 0;
    }

#ifdef DF_HASH_TIME
    do_gettimeofday(&e_tv);
    ht_printk("HTupdate: time %d\n", diff_time(s_tv, e_tv));
#endif
    return -1;
}

int HTupdate(hashtable * ht, KEYDT key, VALDT data, VALDT* old_data)
{
    int hash = compute_hash(ht, key);

#ifdef DF_HASH_TIME
    struct timeval s_tv, e_tv;
    do_gettimeofday(&s_tv);
#endif

    if (hash < 0)
        hpanic("ERROR : Incorrect hash\n");
    
    if (df_list_update(ht->table[hash], key,data, old_data) > 0) {
        return STATUS_DUPL_ENTRY;
    }else{
        df_list_insert(ht, &ht->table[hash], key, data);
        ht->size++;
        return 0;
    }

#ifdef DF_HASH_TIME
    do_gettimeofday(&e_tv);
    ht_printk("HTupdate: time %d\n", diff_time(s_tv, e_tv));
#endif

    return -1;
}

void printHashtableContent(hashtable * ht)
{
    int i;
    ht_printk("Printing HT. ht->nr_rows = %d\n", ht->nr_rows);

    for(i = 0; i< ht->nr_rows; i++)
        df_list_print(ht->table[i]);
    ht_printk("\n");
}
