#include "ar_array.h"
#include "linux/string.h"
#include "linux/slab.h"
#define CHUNK_SIZE (4096)

#define diff(a,b) ((b.tv_sec - a.tv_sec) * 1000000 + (b.tv_usec - a.tv_usec))

int EMPTY;

int ar_create_with_size(array_table *ar, char *name, int size){
    int i;
    strcpy(ar->name, name);
    ar->name[29] = '\0';
    ar->bucket_size = (CHUNK_SIZE/sizeof(VALDT));
    ar->buckets = (size/ar->bucket_size) + 1;
    ar->array = (VALDT**) my_kmalloc(sizeof(VALDT*) * ar->buckets, GFP_KERNEL);
    BUG_ON(!ar->array);
    for(i=0;i<ar->buckets;i++){
        ar->array[i] = (VALDT*)my_kmalloc(CHUNK_SIZE , GFP_KERNEL);
        if(ar->array[i] == NULL){
            apanic("Error : could not allocate hash memroy\n");
        }
        memset(ar->array[i],0,CHUNK_SIZE/sizeof(char));
    }
    EMPTY = ar->array[0][0];
    ar->size = 0;
    ar->time = 0;
    return 1;
}

void ar_reset(array_table *ar){
    int i;
    for(i=0;i<ar->buckets;i++){
        memset(ar->array[i],0,CHUNK_SIZE/sizeof(char));
    }
    ar->size = 0;
    ar->time = 0;
}

int ar_destroy(array_table *ar){
    int i;
    for(i=0;i<ar->buckets;i++){
        my_kfree(ar->array[i],CHUNK_SIZE);
        ar->array[i] = NULL;
    }
    my_kfree(ar->array,sizeof(VALDT*)*ar->buckets);
    ar->buckets = 0;
    ar->bucket_size = 0;
    ar->size = 0;
    return 1;
}

int ar_add(array_table *ar, KEYDT key){
    struct timeval s,e;
    do_gettimeofday(&s);
    if(ar->array[key/ar->bucket_size][key%ar->bucket_size] == EMPTY) ar->size++;
    ar->array[key/ar->bucket_size][key%ar->bucket_size] = 1;
    do_gettimeofday(&e);
    ar->time += diff(s,e);
    return 1;
}

int ar_add_val(array_table *ar, KEYDT key, VALDT val){
    struct timeval s,e;
    do_gettimeofday(&s);
    if(ar->array[key/ar->bucket_size][key%ar->bucket_size] == EMPTY) ar->size++;
    ar->array[key/ar->bucket_size][key%ar->bucket_size] = val;
    do_gettimeofday(&e);
    ar->time += diff(s,e);
    return 1;
}

int ar_lookup(array_table *ar, KEYDT key){
    int ret;
    struct timeval s,e;
    do_gettimeofday(&s);
    ret = ar->array[key/ar->bucket_size][key%ar->bucket_size];
    do_gettimeofday(&e);
    ar->time += diff(s,e);
    return ret;
}

void ar_increment(array_table *ar, KEYDT key){
    struct timeval s,e;
    do_gettimeofday(&s);
    ar->array[key/ar->bucket_size][key%ar->bucket_size]++;
    do_gettimeofday(&e);
    ar->time += diff(s,e);
}

int ar_lookup_val(array_table *ar, KEYDT key, VALDT *val){
    struct timeval s,e;
    int ret;
    do_gettimeofday(&s);
    *val = 1;
    *val = ar->array[key/ar->bucket_size][key%ar->bucket_size];
    if (*val == EMPTY)
        ret = 0;
    else
        ret = 1;
    do_gettimeofday(&e);
    ar->time += diff(s,e);
    return ret;
}

int ar_remove(array_table *ar, KEYDT key){
    struct timeval s,e;
    do_gettimeofday(&s);
    if(ar->array[key/ar->bucket_size][key%ar->bucket_size] != EMPTY){
        ar->array[key/ar->bucket_size][key%ar->bucket_size] = EMPTY;
        ar->size--;
    }
    do_gettimeofday(&e);
    ar->time += diff(s,e);
    return 1;
}

int ar_remove_val(array_table *ar, KEYDT key, VALDT *val){
    struct timeval s,e;
    do_gettimeofday(&s);
    *val = ar->array[key/ar->bucket_size][key%ar->bucket_size];
    if(ar->array[key/ar->bucket_size][key%ar->bucket_size] != EMPTY){
        ar->array[key/ar->bucket_size][key%ar->bucket_size] = EMPTY;
        ar->size--;
    }
    do_gettimeofday(&e);
    ar->time += diff(s,e);
    return (*val);
}

int ar_get_size(array_table *ar){
    return ar->size;	
}
