#ifndef __HASHTABLE_H
#define __HASHTABLE_H

#include "utils.h"

#undef hash_init

#define ht_printk(x,y...) do { printk( KERN_ERR "%s:%d - %d %s %d - " x,__FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##y); } while (0);

#define hpanic(x...) printk( KERN_ERR "ERROR " x)

typedef errorCode (*keyCompareFunction)(void *, void *, int *);
typedef errorCode (*deallocKeyFunction)(void *);
typedef errorCode (*deallocDataFunction)(void *);
//typedef errorCode (*deallocValuesFunction)(void *);

typedef struct hashtableEntry_tag {
	KEYDT key;
    VALDT data;
	int busy;
	struct hashtableEntry_tag *next;
} hashtableEntry;


typedef struct hashtable_tag {
	/* number of elements stored */
	int size;
	int nr_rows;
	int busy;

	hashtableEntry *cur_entry;
	int scan_active;
	int cur_row;
	int next_free;
    unsigned long (*customhash)(KEYDT);

	hashtableEntry **table;
} hashtable;

/* application specific */
errorCode compareBlockNumber(void * k1, void * k2, int * result);
errorCode compareBlockNumberNoPointer(void * k1, void * k2, int * result);
/* should be specified by the application and be part of the hashtable function */

errorCode createHashtable(int numberEntries, keyCompareFunction compare, deallocKeyFunction deallocK, deallocDataFunction deallocD, hashtable ** ht /* return value */);
errorCode deleteHashtable(hashtable ** ht);
errorCode deleteHashtableAndData(hashtable ** ht);

errorCode HTinsertNoDup(hashtable * ht, KEYDT key, VALDT data);
errorCode HTinsert(hashtable * ht, VALDT key, VALDT data);
errorCode HTextract(hashtable * ht, VALDT key, VALDT* data);
errorCode HTlookup(hashtable * ht, VALDT key, VALDT* data);
errorCode HTupdate(hashtable * ht, VALDT key, VALDT data, VALDT* old_data);
errorCode HTaddsub(hashtable * ht, VALDT key, VALDT data, VALDT* old_data);

errorCode createHTscan(hashtable * ht);
errorCode HTadvanceScanVal(hashtable * ht, KEYDT* key, VALDT* val);
errorCode HTpopVal(hashtable *ht, KEYDT* key, VALDT* val);
errorCode destroyHTscan(hashtable * ht);

errorCode HTempty(hashtable* ht, int * res);

int HTsize(hashtable*);
void hash_init(void);
void hash_cleanup(void);
hashtableEntry* get_hashtableEntry(void);
void put_hashtableEntry(hashtableEntry*);
void printHashtableContent(hashtable * ht);
#endif
