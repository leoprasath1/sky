#include "ht_at_wrappers.h"
#include <linux/delay.h>
#include <linux/kthread.h>

int NUM_TESTERS = 8;
int NUM_TABLES = 3;
int NUM_ELEMS = 1000000;

hash_table** hts = NULL;
struct task_struct** testers = NULL;
atomic_t test_done;

int test_func(void* data){
    int id = (int)((unsigned long)data);
    int updateid = (id+1)%NUM_TESTERS;
    int deleteid = (id+2)%NUM_TESTERS;
    int i;
    const int num = NUM_ELEMS;
    
    ht_printk("starting tester id %d updateid %d deleteid %d \n", id, updateid, deleteid);

    for(i=0;i<num;i++){
        unsigned long key = ((i<<8) | id);
        unsigned long val = ((1<<8) | id);
        ht_add_val(hts[i%NUM_TABLES],key,val);
        if(i%100 == 0){
            msleep(1);
        }
    }
    
    ht_printk("finished adding tester id %d updateid %d deleteid %d \n", id, updateid, deleteid);

    for(i=0;i<num;i++){
        unsigned long key = ((i<<8) | updateid);
        unsigned long val;
        unsigned long expval = ((1<<8) | updateid);
        unsigned long newval = ((2<<8) | updateid);
        unsigned long sleeptime = 0;
        while(ht_lookup_val(hts[i%NUM_TABLES],key,&val) == 0){
            msleep(10);
            sleeptime += 10;
            if(sleeptime > 30000){
                ht_printk("ERROR too much sleep key %lu not yet found in ht %s \n", key , hts[i%NUM_TABLES]->name);
            }
        }
        if(val != expval){
            ht_printk("ERROR unexpected updateid expected %lu found %lu \n", expval, val);
        }
        ht_update_val(hts[i%NUM_TABLES],key,newval);
        if(i%100 == 0){
            msleep(1);
        }
    }
    
    ht_printk("finished updating tester id %d updateid %d deleteid %d \n", id, updateid, deleteid);
    
    for(i=0;i<num;i++){
        unsigned long key = ((i<<8) | deleteid);
        unsigned long val=0;
        unsigned long expval = ((2<<8) | deleteid);
        unsigned long sleeptime = 0;
        while((ht_lookup_val(hts[i%NUM_TABLES],key,&val) == 0) || (val!=expval)){
            msleep(10);
            sleeptime += 10;
            if(sleeptime > 30000){
                ht_printk("ERROR too much sleep expected %lu found %lu \n", expval, val);
            }
            val = 0;
        }
        ht_remove(hts[i%NUM_TABLES],key);
        if(i%100 == 0){
            msleep(1);
        }
    }
    
    atomic_inc(&test_done);
    
    ht_printk("finishing tester id %d updateid %d deleteid %d \n", id, updateid, deleteid);
    do_exit(0);
}

void test_ht_at(void){
    int i;
    char name[1024];
    atomic_set(&test_done,0);

    ht_printk("starting test\n");

    hts = (struct hash_table**)kmalloc(sizeof(struct hash_table*) * NUM_TABLES,GFP_KERNEL);
    testers = (struct task_struct**)kmalloc(sizeof(struct task_struct*) * NUM_TESTERS,GFP_KERNEL);

    for(i=0;i<NUM_TABLES;i++){
        sprintf(name,"testht%d",i);
        ht_create_with_size(&hts[i],name,(NUM_TESTERS*NUM_ELEMS)/100);
    }

    for(i=0;i<NUM_TESTERS;i++){
        testers[i] = kthread_run(test_func,(void*)((unsigned long)i),"tester%d",i);
    }

    while(atomic_read(&test_done) < NUM_TESTERS){
        msleep(2000);
    }

    for(i=0;i<NUM_TABLES;i++){
        if(ht_get_size(hts[i]) != 0){
            ht_printk("ERROR non-zero ht name %s size %d \n", hts[i]->name, ht_get_size(hts[i]));
        }
        ht_destroy(hts[i]);
    }
    
    ht_printk("finishing test\n");

    kfree(hts);
    kfree(testers);
    return;
}
