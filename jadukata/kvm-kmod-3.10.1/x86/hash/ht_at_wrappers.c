#include "ht_at_wrappers.h"

#define HASH_TABLE_ENTRIES      2565
//#define HASH_TABLE_ENTRIES	1237

/*Hash table wrappers */
inline int normal_compare(void *key1, void *key2, int *eq)
{
	*eq = ((KEYDT)key1 == (KEYDT)key2) ? 0 : 1;
	return *eq;
}

inline int ht_create_with_size(hash_table **ht, char *name, int size)
{
	hash_table *tmp = my_kmalloc(sizeof(hash_table), GFP_ATOMIC);
	HT_AT_lock_init(&tmp->lock);
	sema_init(&tmp->scan_sem,1);
	tmp->past_size = 0;
    tmp->debug = false;
	strcpy(tmp->name, name);

	createHashtable(size, compareBlockNumberNoPointer, NULL, NULL, &tmp->table);
	*ht = tmp;
	return 1;
}

inline int ht_create(hash_table **ht, char *name)
{
	ht_create_with_size(ht,name,HASH_TABLE_ENTRIES);
	return 1;
}

inline int ht_create_with_customhash(hash_table **ht, char *name, int size, unsigned long (*customhashfn)(KEYDT))
{
    int ret = ht_create_with_size(ht,name,size);
    hash_table *tmp = *ht;
    tmp->table->customhash = customhashfn;
    return ret;
}

inline int ht_destroy(hash_table *ht)
{
	if(!ht) {
		return 1;
	}
	deleteHashtable(&ht->table);
	my_kfree(ht,sizeof(hash_table));
	return 1;
}

inline int ht_clear(hash_table *ht)
{
	int i;
	HT_AT_lock(&(ht->lock));
	for(i = 0; i< ht->table->nr_rows; i++) {
		if (ht->table->table[i]) {
			hashtableEntry *tmp = ht->table->table[i], *tmp1;
			while(tmp) {
				tmp1 = tmp->next;
				put_hashtableEntry(tmp);
				ht->table->size--;
				tmp = tmp1;
			}
			ht->table->table[i] = NULL;
		}
	}
	if(ht->table->size != 0) {
		ht_printk("ERROR: hashtable size not 0 when empty\n");
	}
	HT_AT_unlock(&(ht->lock));
	return 0;
}

static inline void ht_display_mem_footprint(hash_table *ht)
{
	int diff_size = HTsize(ht->table) - ht->past_size;
	if ((diff_size > 25) || (diff_size < -25)) {
		struct timeval st;
		ht->past_size = HTsize(ht->table);
		do_gettimeofday(&st);
		ht_printk("HT %s: %ld.%ld : %d\n", ht->name, st.tv_sec, st.tv_usec, ht->past_size);
	}
}

inline int ht_get_size(hash_table *ht)
{
    if(ht && ht->table){
	    return HTsize(ht->table);
    }
    return -1;
}

inline void ht_set_debug(hash_table *ht){
    if(ht){
        ht->debug = true;
    }
}

inline void ht_unset_debug(hash_table *ht){
    if(ht){
        ht->debug = false;
    }
}

static inline int ht_add_val_helper(hash_table *ht, KEYDT key, VALDT val,bool checkdup)
{
	int st;
    
	HT_AT_lock(&ht->lock);
    if(checkdup){
	    st = HTinsert(ht->table, key, val);
    }else{
	    st = HTinsertNoDup(ht->table, key, val);
    }

#ifdef MEMORY_FOOTPRINT
	ht_display_mem_footprint(ht);
#endif

	HT_AT_unlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx dup: %d ret %d \n", ht->name, key, val,st==STATUS_DUPL_ENTRY,st);
    }

	return st;
}

inline int ht_add_val_no_dup(hash_table *ht, KEYDT key, VALDT val){
    return ht_add_val_helper(ht,key,val,false);
}

inline int ht_add_val(hash_table *ht, KEYDT key, VALDT val){
    return ht_add_val_helper(ht,key,val,true);
}

inline int ht_add(hash_table *ht, KEYDT key)
{
	VALDT val = 1;
    return ht_add_val(ht,key,val);
}

inline int ht_add_sub_val(hash_table *ht, KEYDT key, VALDT val)
{
	int st;
    VALDT oldval;
    
	HT_AT_lock(&ht->lock);
	st = HTaddsub(ht->table, key, val,&oldval);

#ifdef MEMORY_FOOTPRINT
	ht_display_mem_footprint(ht);
#endif

	HT_AT_unlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, key, val,st);
    }
    
	return st;
}

inline int ht_update_val(hash_table *ht, KEYDT key, VALDT val)
{
	int st;
    VALDT oldval;
    
	HT_AT_lock(&ht->lock);
	st = HTupdate(ht->table, key, val,&oldval);

#ifdef MEMORY_FOOTPRINT
	ht_display_mem_footprint(ht);
#endif

	HT_AT_unlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, key, val,st);
    }
    
	return st;
}

inline int htn_update_val(hash_table *ht, KEYDT key, VALDT val)
{
    return ht_update_val(ht,key,val);
}

inline int ht_lookup_val(hash_table *ht, KEYDT key, VALDT *val)
{
	int st;

	HT_AT_readlock(&ht->lock);
	st = HTlookup(ht->table, key, val);
	if (st < 0)
		st = 0;
	HT_AT_readunlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, key, *val, st);
    }

	return st;
}

inline int ht_lookup(hash_table *ht, KEYDT key)
{
	VALDT val;
    return ht_lookup_val(ht,key,&val);
}

inline int htn_remove_val(hash_table *ht, KEYDT key, VALDT *val)
{
	int st;
    
	HT_AT_lock(&ht->lock);
	st = HTextract(ht->table, key, val);
#ifdef MEMORY_FOOTPRINT
	ht_display_mem_footprint(ht);
#endif
	HT_AT_unlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, key, *val, st);
    }

	return st;
}

inline int htn_remove(hash_table *ht, KEYDT key)
{
    VALDT val;
    return htn_remove_val(ht,key,&val);
}

inline int ht_remove_val(hash_table *ht, KEYDT key, VALDT *val)
{
	int st;
    
	HT_AT_lock(&ht->lock);
	st = HTextract(ht->table, key, val);
#ifdef MEMORY_FOOTPRINT
	ht_display_mem_footprint(ht);
#endif
	HT_AT_unlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, key, *val,st);
    }

	return st;
}

inline int ht_remove(hash_table *ht, KEYDT key)
{
    VALDT val;
    return ht_remove_val(ht,key,&val);
}

inline void ht_print(hash_table *ht)
{
	HT_AT_readlock(&ht->lock);
	ht_printk("Contents of %s\n", ht->name);
	printHashtableContent(ht->table);
	HT_AT_readunlock(&ht->lock);
}

inline int ht_open_scan(hash_table *ht)
{
	int st;
    while(down_interruptible(&ht->scan_sem) != 0){
    };
    
    {
        HT_AT_lock(&ht->lock);
        st = createHTscan(ht->table);
        HT_AT_unlock(&ht->lock);
    }
	return st;
}

inline int ht_close_scan(hash_table *ht)
{
	int st;
	HT_AT_lock(&ht->lock);
	st = destroyHTscan(ht->table);
	HT_AT_unlock(&ht->lock);
    up(&ht->scan_sem);
	return st;
}

inline int ht_scan_val(hash_table *ht, KEYDT *key,VALDT *val)
{
	int st = -1;
	HT_AT_lock(&ht->lock);
	if (ht->table->size)
		st = HTadvanceScanVal(ht->table, key, val);
	HT_AT_unlock(&ht->lock);

    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, *key, *val,st);
    }

	return st;
}

inline int ht_scan(hash_table *ht, KEYDT *key)
{
    VALDT val;
    return ht_scan_val(ht,key,&val);
}

inline int ht_pop_val(hash_table *ht, KEYDT *key, VALDT *val)
{
	int st;

	HT_AT_lock(&ht->lock);
	st = HTpopVal(ht->table, key, val);
#ifdef MEMORY_FOOTPRINT
	ht_display_mem_footprint(ht);
#endif
	HT_AT_unlock(&ht->lock);
    
    if(unlikely(ht->debug)){
        ht_printk("%s : %lx -> %lx ret %d\n", ht->name, *key, *val,st);
    }

	return st;
}

inline int ht_pop(hash_table *ht, KEYDT* key)
{
    VALDT val;
	return ht_pop_val(ht,key,&val);
}

inline int at_create(avl_tree **t, char *name)
{
	avl_tree *tmp = my_kmalloc(sizeof(avl_tree), GFP_ATOMIC);
	graid_avl_init(&tmp->tree);
	HT_AT_lock_init(&tmp->lock);

	strcpy(tmp->name, name);
	tmp->num  = 0;
	*t = tmp;
	return 1;
}

inline int at_destroy(avl_tree *t)
{
	struct tree *node;
	HT_AT_lock(&t->lock);
	while((node = graid_avl_find_next(t->tree, -1))) {
		graid_avl_remove(&t->tree, node);
		my_kfree(node,sizeof(struct tree));
		if (!t->tree)
			break;
	}

	HT_AT_unlock(&t->lock);
	my_kfree(t,sizeof(avl_tree));
	return 1;
}

inline int at_removeall(avl_tree *t)
{
	struct tree *node;
	HT_AT_lock(&t->lock);
	while((node = graid_avl_find_next(t->tree, -1))) {
		graid_avl_remove(&t->tree, node);
        t->num--;
		my_kfree(node,sizeof(struct tree));
		if (!t->tree)
			panic("Empty tree");
	}
	HT_AT_unlock(&t->lock);
	return 1;
}

inline int at_add_val(avl_tree *t,  unsigned long key, unsigned long val)
{
	struct tree *node;

	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find(t->tree, key))) {
		ht_printk("ERROR: Duplicate insertion in add_val in at tree %s: key %ld node key %lu node val %d \n",t->name, key,node->key, node->val);
		dump_stack();
		HT_AT_unlock(&t->lock);
		return -1;
	}

	node = my_kmalloc(sizeof(struct tree), GFP_ATOMIC);
	if (!node)
		panic("ERROR: mem alloc failure in at_add_val\n");
	node->key = key;
	node->val = val;
	node->tree_avl_left = node->tree_avl_right = NULL;

	if (graid_avl_insert(&t->tree, node) > 0)
		t->num++;

	HT_AT_unlock(&t->lock);

	return 1;
}

inline int at_lookup(avl_tree *t, unsigned long key)
{
	int retval;
	
    HT_AT_lock(&t->lock);
	if (graid_avl_find(t->tree, key))
		retval = 1;
	else
		retval = 0;
	HT_AT_unlock(&t->lock);

	return retval;
}

inline int at_increment_val(avl_tree *t, unsigned long key)
{
	int retval;
	struct tree *node;

	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find(t->tree, key))) {
		node->val++;
		retval = 1;
	} else {
		retval = 0;
    }
	HT_AT_unlock(&t->lock);

	return retval;
}

inline int at_lookup_val(avl_tree *t, unsigned long key, unsigned long *val)
{
	int retval;
	struct tree *node;

	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find(t->tree, key))) {
		*val = node->val;
		retval = 1;
	} else {
		retval = 0;
    }
	HT_AT_unlock(&t->lock);

	return retval;
}

inline int at_remove(avl_tree *t, unsigned long key)
{
	struct tree *node;
	int retval = -1;
	HT_AT_lock(&t->lock);
	if ((node = graid_avl_find(t->tree, key))) {
		if (graid_avl_remove(&t->tree, node) > 0) {
			t->num--;
			my_kfree(node,sizeof(struct tree));
		}
		retval = 1;
	}

	HT_AT_unlock(&t->lock);
	return retval;
}

inline int at_find_closest_before(avl_tree *t, unsigned long key, unsigned long *res_key, unsigned long *res_val)
{
	int retval = -1;
	struct tree *node;
	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find_previous(t->tree, key)) && (node->key >= 0)) {
		*res_key = node->key;
		*res_val = node->val;
		retval = 1;
	}

	HT_AT_unlock(&t->lock);
	return retval;
}

inline int at_find_closest_after(avl_tree *t, unsigned long key, unsigned long *res_key, unsigned long *res_val)
{
	int retval = -1;
	struct tree *node;
	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find_next(t->tree, key)) && (node->key >= 0)) {
		*res_key = node->key;
		*res_val = node->val;
		retval = 1;
	}

	HT_AT_unlock(&t->lock);
	return retval;
}

inline int at_num_elements(avl_tree *t)
{
	return t->num;
}

inline int at_open_scan(avl_tree *t)
{
	t->cur_key_val = -1;
	return 1;
}

inline int at_scan_val(avl_tree *t, unsigned long *key,unsigned long *val)
{
	struct tree *node;
	unsigned long retval;
	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find_next(t->tree, t->cur_key_val))) {
		if (node->key < 0)
			retval = 0;
		else {
			*key = node->key;
            *val = node->val;
			t->cur_key_val = node->key;
			retval = 1;
		}
	} else
		retval = 0;
	HT_AT_unlock(&t->lock);
	return retval;
}

inline int at_scan(avl_tree *t, unsigned long *key)
{
	struct tree *node;
	unsigned long retval;
	HT_AT_lock(&t->lock);

	if ((node = graid_avl_find_next(t->tree, t->cur_key_val))) {
		if (node->key < 0)
			retval = 0;
		else {
			*key = node->key;
			t->cur_key_val = node->key;
			retval = 1;
		}
	} else
		retval = 0;
	HT_AT_unlock(&t->lock);
	return retval;
}

static inline void add_to_list(struct list_head* head, unsigned long key, unsigned long val,int tail)
{
	struct keyval* entry = (struct keyval*)my_kmalloc(sizeof(struct keyval),GFP_KERNEL);
	if(entry) {
		entry->key = key;
		entry->val = val;
		if(tail)
			list_add_tail(&entry->list,head);
		else
			list_add(&entry->list,head);
	}
}

inline void update_interval(avl_tree * tree, array_table* ht, unsigned long key, unsigned long val)
{
	int log = 0;
	LIST_HEAD(intervals);
	unsigned long key1,key2;
	unsigned long val1,val2;
	struct keyval *entry, *prev;
	//compressing
	unsigned long prev_sectornr;
	unsigned long prev_size, prev_remap, remap;

	if(log)
		ht_printk("key %lu val %lu\n",key,val);
	key1 = key;
	if(at_lookup_val(tree,key1,&val1)) {
		if(log)
			ht_printk("found equal key %lu val %lu\n",key1,val1);
		add_to_list(&intervals,key1,val1,0);
	} else {
		while(1) {
			if(at_find_closest_before(tree,key1,&key2,&val2) > 0) {
				if(log)
					ht_printk("found before key %lu val %lu\n",key2,val2);
				if( (key1 >= key2) && (key1 < (key2+val2)) ) {
					add_to_list(&intervals,key2,val2,0);
					key1 = key2;
				} else {
					break;
				}
			} else {
				break;
			}
		}
	}

	if(log)
		ht_printk("processing right \n");

	key1 = key;
	while(1) {
		if(at_find_closest_after(tree,key1,&key2,&val2) > 0) {
			if(log)
				ht_printk("found after key %lu val %lu\n",key2,val2);
			if(key2 < (key+val)) {
				add_to_list(&intervals,key2,val2,1);
				key1 = key2;
				val1 = val2;
			} else {
				break;
			}
		} else {
			break;
		}
	}

	if(log)
		ht_printk("processing list\n");

	list_for_each_entry_safe(entry,prev,&intervals,list) {
		if(log)
			ht_printk("processing entry key %lu val %lu\n",entry->key,entry->val);
		if( (entry->key >= key) ) {
			at_remove(tree,entry->key);
			if((entry->key + entry->val) <= (key+val)) {
				ar_remove(ht,entry->key);
			} else {
				at_add_val(tree, key+val, entry->key+entry->val-key-val);
				ar_remove_val(ht,entry->key,&val1);
				ar_add_val(ht,key+val,val1+(key+val-entry->key));
			}
		} else {
			at_remove(tree,entry->key);
			at_add_val(tree, entry->key, key-entry->key);
		}
		list_del(&(entry->list));
		my_kfree(entry,sizeof(struct keyval));
	}
	at_add_val(tree,key,val);

	//compress the entries
	if(at_find_closest_before(tree,key,&prev_sectornr,&prev_size) > 0) {
		ar_lookup_val(ht, prev_sectornr, &prev_remap);
		ar_lookup_val(ht, key, &remap);
		if( ((prev_sectornr + prev_size) == key) && ((prev_remap + prev_size) == remap) ) {
			if(log)
				ht_printk("Compressing prev_sectornr %lu prev_size %lu key %lu prev_remap %lu remap %lu\n", prev_sectornr, prev_size, key, prev_remap, remap);
			at_remove(tree,prev_sectornr);
			at_remove(tree,key);
			at_add_val(tree, prev_sectornr, prev_size + val);
			ar_remove(ht,key);
		}
	}
}

inline void remove_interval(avl_tree *tree, array_table *ht, unsigned long key, unsigned long val)
{
	update_interval(tree,ht,key,val);
	at_remove(tree,key);
	ar_remove(ht,key);
}

inline int lookup_interval(avl_tree *tree, array_table * ht, unsigned long sectornr,int sectors, unsigned long* ret_key, unsigned long* ret_offset)
{
	int metadata = 0;
	unsigned long offset = 0, val, remap_val;
	int log = 0;
	unsigned long key,temp;

	if(at_lookup_val(tree,sectornr,&val)) {
		if(log) ht_printk("found exact match sectornr %lu val %lu\n",sectornr,val);
		key = sectornr;
		metadata = 1;
		offset = 0;
		if( sectors > val) {
			for(temp = 0; temp < sectors; temp+=remap_val) {
				ar_lookup_val(ht, key + temp, &remap_val);
				ht_printk("ERROR : need multiple bios key %lu remap val %lu\n", key+temp, remap_val);
			}
		}
	} else {
		if(at_find_closest_before(tree, sectornr,&key,&val) > 0) {
			if(log) ht_printk("closest before match sectornr %lu key %lu val %lu\n",sectornr,key, val);
			if((key + val) > sectornr ) {
				metadata = 1;
				offset = sectornr - key;
				if( sectors > (val-offset)) {
					for(temp = 0; temp < sectors; temp+=remap_val) {
						ar_lookup_val(ht, key + temp, &remap_val);
						ht_printk("ERROR : need multiple bios key %lu remap val %lu\n", key+temp, remap_val);
					}
				}
			}
		}
	}
	if(log) ht_printk("returning key %lu offset %lu\n",key, offset);
	*ret_key = key;
	*ret_offset = offset;
	return metadata;
}

inline void print_interval_tree(avl_tree * tree, array_table *ht)
{
	unsigned long key,val;
	at_open_scan(tree);
	while(at_scan_val(tree,&key,&val)) {
		ht_printk("%lu -> %lu\n",key,val);
	}
	ht_printk("\n");
	//ht_print(ht);
}
