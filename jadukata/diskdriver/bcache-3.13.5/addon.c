#include "bcache.h"
#include "btree.h"
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)

struct btree_addon_op {
    struct cache_set *c;
    btree_map_keys_fn *fn;
    struct bkey *from;
    struct btree_op op;
    //for update priority
    unsigned long sector;
    uint16_t prio;
    //for cache_contents
    hash_table* ht_contents;
    unsigned long count;
    //for cache_distribution
    int prios[IOCLASS_MAX+2];
    int* stats;
};

extern unsigned bcache_sb_offset;
extern struct bcache_device *update_priority_d;

static inline int btree_addon_helper(struct btree_addon_op *op)
{
	int ret;
    do{
        ret  = bch_btree_map_keys(&op->op, op->c, op->from, op->fn, MAP_END_KEY);
    }while(ret == -EAGAIN);
    return ret;
}

static int update_priority_fn(struct btree_op *b_op, struct btree *b, struct bkey *k){
	struct btree_addon_op *op = container_of(b_op, struct btree_addon_op, op);
    dprinttrace("update prio leaf node b->level %d keys %llu keye %llu bucketnr %lu \n",b->level, KEY_START(k),KEY_OFFSET(k), PTR_BUCKET_NR(b->c,k,0));
    if( (KEY_INODE(k) == KEY_INODE(op->from)) && (op->sector >= KEY_START(k) && op->sector <= KEY_OFFSET(k)) ){
        //if(	PTR_BUCKET(b->c, k, 0)->prio < op->write_prio){
        PTR_BUCKET(b->c, k, 0)->prio = op->prio;
        PTR_BUCKET(b->c, k, 0)->org_prio = op->prio;
        dprinttrace("update priority setting priority sector %lu bucketnr %lu priority %d \n", op->sector - bcache_sb_offset, PTR_BUCKET_NR(b->c,k,0), op->prio);
        return MAP_DONE;
        //}
    }

    return MAP_CONTINUE;
}

//struct cached_dev* dc = (struct cached_dev*)(bdev->bd_holder); 
//struct bcache_device *d = &(dc->disk); 
//
#define COMMONDEFS \
    struct btree_addon_op addonop; \
    struct btree_op *op = &(addonop.op); \
    struct bcache_device *d = update_priority_d; \
    memset(&addonop,0,sizeof(struct btree_addon_op)); \
	bch_btree_op_init(op, -1); \
    addonop.c = d->c;

//Leo 
//update the priority of the sector
//TODO optimize by collating sectors
//TODO tie diskdriver and bcache more closely
// cannot be called under atomic
void bcache_update_priority(struct block_device* bdev, unsigned long sector, uint16_t ioclass){
    COMMONDEFS
    addonop.from = &KEY(d->id, sector, 0);
    addonop.fn = update_priority_fn; 
    addonop.prio = IOCLASS_TO_PRIO(ioclass);
    dprinttrace("trying to update priority setting priority sector %lu ioclass %u prio %d d %p op %p \n",sector, ioclass, addonop.prio, d, op);
    //find the bucket and update the priority
    btree_addon_helper(&addonop);
}

static int cache_contents_fn(struct btree_op *b_op, struct btree *b, struct bkey *k){
	struct btree_addon_op *op = container_of(b_op, struct btree_addon_op, op);
    dprinttrace("cache_contents leaf node b->level %d keys %llu keye %llu bucketnr %lu \n",b->level, KEY_START(k),KEY_OFFSET(k), PTR_BUCKET_NR(b->c,k,0));
    ht_add_val_no_dup(op->ht_contents,KEY_START(k),KEY_SIZE(k));
    op->count += KEY_SIZE(k);
	return MAP_CONTINUE;
}

void get_cached_sectors_bcache(char* ptr){
    unsigned long sector, size, prev, count;
    char *tptr = ptr, *pptr = ptr;
    hash_table* ht_contents = NULL;
    COMMONDEFS
    ht_create_with_size(&ht_contents,"cachecontents",1);
    dprinte(1,"getting cache contents d %p op %p \n", d, op);
    addonop.from = &KEY(d->id, 0, 0);
    addonop.fn = cache_contents_fn;
    addonop.ht_contents = ht_contents;
    //get contents 
    btree_addon_helper(&addonop);
    dprinte(1,"cache contents count %lu \n",addonop.count);
    prev = ~0UL;
    count = 0;
    while(ht_pop_val(ht_contents, &sector,&size) != -1){

        //adjust for bcache super block
        sector -= bcache_sb_offset; 
        if(prev == ~0UL || prev == sector){
            prev = sector+size;
            count += size;
        }else{
            if((prev-count) == (sector + size)){
                count += size;
            }else{
                tptr += sprintf(tptr,"%lu-%lu,",prev-count,count);
                if(tracing_enabled){
                    if(tptr-pptr > 300){
                        dprintk(1,"%300s\n",pptr);
                        pptr = tptr;
                    }
                }
                prev = sector + size;
                count = size;
            }
        }
    }
    if(prev != ~0UL){
        tptr += sprintf(tptr,"%lu-%lu,",prev-count,count);
    }
    if(tracing_enabled){
        dprintk(1,"%s\n",pptr);
    }
    ht_destroy(ht_contents);
}

static int cache_distribution_fn(struct btree_op *b_op, struct btree *b, struct bkey *k){
    struct btree_addon_op *op = container_of(b_op, struct btree_addon_op, op);
    int* btreestats = op->stats;
    int prio = PTR_BUCKET(b->c,k,0)->prio;
    int i;
    if(prio != HOARD_PRIO){
        for(i=0;i<IOCLASS_MAX+2;i++){
            if(prio <= op->prios[i]){
                btreestats[i]+=KEY_SIZE(k);
                break;
            }
        }
        op->count +=  KEY_SIZE(k);
    }
    return MAP_CONTINUE;
}

void get_cache_distribution_bcache(char* ptr){
    int i;
    char *tptr = ptr;
	struct cache *ca;
	struct bucket *b;
    int org_sum = 0 , sum = 0;
    unsigned long btreesum = 0;
    int org_stats[IOCLASS_MAX+2];
    int stats[IOCLASS_MAX+2];
    int btreestats[IOCLASS_MAX+2];
    int prios[IOCLASS_MAX+2];
    short bucketsectors;
    COMMONDEFS
    bucketsectors = d->c->sb.bucket_size;
    //struct cached_dev* dc = (struct cached_dev*)(bdev->bd_holder);
    //struct bcache_device *d = &(dc->disk);

    dprinte(1,"getting cache distribution d %p \n", d);
    
    for(i=0;i<IOCLASS_MAX+2;i++){
        stats[i] = 0;
        org_stats[i] = 0;
        btreestats[i] = 0;
        prios[i] = IOCLASS_TO_PRIO(i); 
    }

    prios[IOCLASS_MAX+1] = USHRT_MAX;

    for_each_cache(ca, d->c, i){
        for_each_bucket(b, ca){
           if(b->org_prio == HOARD_PRIO){
               continue;
           }
           for(i=0;i<IOCLASS_MAX+2;i++){
               if(b->org_prio <= prios[i]){
                   org_stats[i]+=bucketsectors;
                   break;
               }
           }

           dprinte( (i>IOCLASS_MAX+1), "ERROR bucket org prio does not fit %d \n", b->org_prio);
           for(i=0;i<IOCLASS_MAX+2;i++){
               if(b->prio <= prios[i]){
                   stats[i]+=bucketsectors;
                   break;
               }
           }
           
           dprinte( (i>IOCLASS_MAX+1), "ERROR bucket current prio does not fit %d \n", b->prio);

           org_sum+=bucketsectors;
           sum+=bucketsectors;
        }
    }

    tptr += sprintf(tptr,"cache distrubution org \n");
    for(i=0;i<IOCLASS_MAX+2;i++){
        char org_p[20];
        jadu_div_emu(org_p,org_stats[i]*100,org_sum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %d \n",i>5?"high":(i==5?"meta":"data"),i,org_stats[i],org_p,org_sum); 
    }
    
    tptr += sprintf(tptr,"cache distrubution current \n");
    for(i=0;i<IOCLASS_MAX+2;i++){
        char p[20];
        jadu_div_emu(p,stats[i]*100,sum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %d \n",i>5?"high":(i==5?"meta":"data"),i,stats[i],p,sum); 
    }

    addonop.from = &KEY(d->id, 0, 0);
    addonop.fn = cache_distribution_fn;
    addonop.stats = btreestats;
    for(i=0;i<IOCLASS_MAX+2;i++){
        addonop.prios[i] = IOCLASS_TO_PRIO(i); 
    }
    addonop.prios[IOCLASS_MAX+1] = USHRT_MAX;
    //get contents distribution 
    btree_addon_helper(&addonop);

    btreesum = addonop.count;
    
    tptr += sprintf(tptr,"cache distrubution btree \n");
    for(i=0;i<IOCLASS_MAX+2;i++){
        char p[20];
        jadu_div_emu(p,btreestats[i]*100,btreesum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %ld \n",i>5?"high":(i==5?"meta":"data"),i,btreestats[i],p,btreesum); 
    }

    dprinte(1,"%s", ptr);

}

void resize_cache_helper(struct cache* ca , unsigned long percent){
    DECLARE_FIFO(long, temp);
    size_t p = (((size_t)ca->sb.nbuckets * percent) / 100);
    size_t np = ((size_t)ca->sb.nbuckets - p);
    size_t orgp = ((size_t)ca->sb.nbuckets - fifo_used(&ca->resize_hoard));
    long i;
    int start = jiffies;
    int sleep_count = 0;
    int sleep_time = 10;
    int last_free_amount = 0;
           
    dprinte(1,"START resize cache percent %lu p %ld np %ld hoardsize %ld hoard used %lu hoard free %lu \n ", percent, p,  np, ca->resize_hoard.size, fifo_used(&ca->resize_hoard), fifo_free(&ca->resize_hoard));
    
    if(ca->resize_hoard.size < np){
        if(tracing_enabled)
            dprinte(1,"resizing the resize hoard fifo ");
		if (!init_fifo_exact(&temp, np, GFP_KERNEL)){
            dprinte(1,"ERROR: could not init temp fifo with size %ld \n", np);
			return;
        }
		fifo_move(&temp, &ca->resize_hoard);
		fifo_swap(&temp, &ca->resize_hoard);
		while (fifo_pop(&temp, i)){
            dprinte(1,"ERROR: still entries left during resize cache \
             %lu %lu %lu \n",p,np,ca->resize_hoard.size);
		    atomic_dec(&ca->buckets[i].pin);
        }
		free_fifo(&temp);
    }

    while(fifo_used(&ca->resize_hoard) >= np){
       if(fifo_pop(&ca->resize_hoard,i)){
           atomic_dec(&ca->buckets[i].pin);
       }else{
           dprinte(1,"ERROR: cannot pop from resize hoard \
           p %ld np %ld size %lu used %lu \n",p,np,ca->resize_hoard.size,fifo_used(&ca->resize_hoard));
       }
    }

    while(fifo_used(&ca->resize_hoard) < np){
       if(fifo_pop(&ca->free,i)){
		   struct bucket *b = ca->buckets + i;
           b->org_prio = HOARD_PRIO;
           fifo_push(&ca->resize_hoard,i);
       }else{
           sleep_count++;
           if(tracing_enabled && sleep_count %10){
               dprinte(1,"sleeping for more free entries to show up resize hoard size %ld free size %ld \n", fifo_used(&ca->resize_hoard), fifo_used(&ca->free));
           }
           while (1) {
               set_current_state(TASK_INTERRUPTIBLE);
               if (fifo_used(&ca->free) > 10){
                   break;	
               }
               wake_up_process(ca->alloc_thread);
               schedule_timeout(msecs_to_jiffies(sleep_time));

               if(fifo_used(&ca->free) > last_free_amount){
                   last_free_amount = fifo_used(&ca->free);
                   if(sleep_time < 3000){
                       sleep_time +=100;
                       if(tracing_enabled)
                           dprinte(1,"updating sleep time to %d last_free_amount %d \n", sleep_time, last_free_amount);
                   }
               }
           }
           __set_current_state(TASK_RUNNING);
       }
    }
    
    dprinte(1,"END time %d msecs resize cache percent %lu p %ld np %ld hoardsize %ld hoard used %lu hoard free %lu \n ", jiffies_to_msecs(jiffies-start) , percent, p,  np, ca->resize_hoard.size, fifo_used(&ca->resize_hoard), fifo_free(&ca->resize_hoard));
    dprinte(1,"org cache size %ld MB resized cache size %ld MB",((orgp*bucket_bytes(ca))/1024/1024) , ((p*bucket_bytes(ca))/1024/1024));
}

void resize_cache_bcache(unsigned long percent){
    unsigned i;
    struct bcache_device *d = update_priority_d;
	for (i = 0; i < d->c->sb.nr_in_set; i++){
        if(d->c->cache[i]){
            resize_cache_helper(d->c->cache[i],percent);
        }
    }
}

#endif

