/*
 * bcache stats code
 *
 * Copyright 2012 Google, Inc.
 */

#include "bcache.h"
#include "stats.h"
#include "btree.h"
#include "sysfs.h"

/*
 * We keep absolute totals of various statistics, and addionally a set of three
 * rolling averages.
 *
 * Every so often, a timer goes off and rescales the rolling averages.
 * accounting_rescale[] is how many times the timer has to go off before we
 * rescale each set of numbers; that gets us half lives of 5 minutes, one hour,
 * and one day.
 *
 * accounting_delay is how often the timer goes off - 22 times in 5 minutes,
 * and accounting_weight is what we use to rescale:
 *
 * pow(31 / 32, 22) ~= 1/2
 *
 * So that we don't have to increment each set of numbers every time we (say)
 * get a cache hit, we increment a single atomic_t in acc->collector, and when
 * the rescale function runs it resets the atomic counter to 0 and adds its
 * old value to each of the exported numbers.
 *
 * To reduce rounding error, the numbers in struct cache_stats are all
 * stored left shifted by 16, and scaled back in the sysfs show() function.
 */

static const unsigned DAY_RESCALE		= 288;
static const unsigned HOUR_RESCALE		= 12;
static const unsigned FIVE_MINUTE_RESCALE	= 1;
static const unsigned accounting_delay		= (HZ * 300) / 22;
static const unsigned accounting_weight		= 32;
#define SCALE_FACTOR (0)

/* sysfs reading/writing */

read_attribute(sectors_cache_hits_ioclass);
read_attribute(sectors_cache_hits);
read_attribute(sectors_cache_misses);
read_attribute(sectors_cache_hit_ratio);
read_attribute(cache_hits);
read_attribute(cache_misses);
read_attribute(cache_bypass_hits);
read_attribute(cache_bypass_misses);
read_attribute(cache_hit_ratio);
read_attribute(cache_readaheads);
read_attribute(cache_miss_collisions);
read_attribute(bypassed);

static void move_stats_accounting(struct cache_accounting* acc);

SHOW(bch_stats)
{
	struct cache_stats *s =	container_of(kobj, struct cache_stats, kobj);
    struct cached_dev *dc = container_of(kobj->parent, struct cached_dev, disk.kobj);
    char result[100];
    char output[1024], *outptr = output;
    int i;
    unsigned long n;

    //Leo
    //do move stats to add accumulated stats before printing
    move_stats_accounting(&(dc->accounting));
    //dprinte(1,"statspointer leo %p %p \n", &(dc->accounting.total), s);

#define var(stat)		(s->stat >> 16)
    //Leo

    for(i=0;i<IOCLASS_MAX+1;i++){
        n = (var(sectors_cache_hits_ioclass[i]) * 100);
        jadu_div_emu(result, n, var(sectors_cache_hits), 3);
    	outptr += sprintf(outptr,"ioclass %d hit %lu per %s total hits/miss(%lu/%lu)\n",i, var(sectors_cache_hits_ioclass[i]),result,var(sectors_cache_hits),var(sectors_cache_misses));
    }
    sysfs_print(sectors_cache_hits_ioclass,(const char*)output );

	var_print(sectors_cache_hits);
	var_print(sectors_cache_misses);

    n = (var(sectors_cache_hits) * 100);
    jadu_div_emu(result, n, (var(sectors_cache_hits)+var(sectors_cache_misses)), 3);
	sysfs_print(sectors_cache_hit_ratio,(const char*)result);

	var_print(cache_hits);
	var_print(cache_misses);
	var_print(cache_bypass_hits);
	var_print(cache_bypass_misses);

	sysfs_print(cache_hit_ratio,
		    DIV_SAFE(var(cache_hits) * 100,
			     var(cache_hits) + var(cache_misses)));

	var_print(cache_readaheads);
	var_print(cache_miss_collisions);
	sysfs_hprint(bypassed,	var(sectors_bypassed) << 9);
#undef var
	return 0;
}

STORE(bch_stats)
{
	return size;
}

static void bch_stats_release(struct kobject *k)
{
}

static struct attribute *bch_stats_files[] = {
	&sysfs_sectors_cache_hits_ioclass,
	&sysfs_sectors_cache_hits,
	&sysfs_sectors_cache_misses,
	&sysfs_sectors_cache_hit_ratio,
	&sysfs_cache_hits,
	&sysfs_cache_misses,
	&sysfs_cache_bypass_hits,
	&sysfs_cache_bypass_misses,
	&sysfs_cache_hit_ratio,
	&sysfs_cache_readaheads,
	&sysfs_cache_miss_collisions,
	&sysfs_bypassed,
	NULL
};
static KTYPE(bch_stats);

int bch_cache_accounting_add_kobjs(struct cache_accounting *acc,
				   struct kobject *parent)
{
	int ret = kobject_add(&acc->total.kobj, parent,
			      "stats_total");
	ret = ret ?: kobject_add(&acc->five_minute.kobj, parent,
				 "stats_five_minute");
	ret = ret ?: kobject_add(&acc->hour.kobj, parent,
				 "stats_hour");
	ret = ret ?: kobject_add(&acc->day.kobj, parent,
				 "stats_day");
	return ret;
}

void bch_cache_accounting_clear(struct cache_accounting *acc)
{
	memset(&acc->total.sectors_cache_hits_ioclass[0],
	       0,
	       sizeof(unsigned long) * (7+IOCLASS_MAX+3) );
}

void bch_cache_accounting_destroy(struct cache_accounting *acc)
{
	kobject_put(&acc->total.kobj);
	kobject_put(&acc->five_minute.kobj);
	kobject_put(&acc->hour.kobj);
	kobject_put(&acc->day.kobj);

	atomic_set(&acc->closing, 1);
	if (del_timer_sync(&acc->timer))
		closure_return(&acc->cl);
}

/* EWMA scaling */

static void scale_stat(unsigned long *stat)
{
	*stat =  ewma_add(*stat, 0, accounting_weight, 0);
}

static void scale_stats(struct cache_stats *stats, unsigned long rescale_at)
{
    int i;
	if (++stats->rescale == rescale_at) {
		stats->rescale = 0;
		scale_stat(&stats->sectors_cache_hits);
        for(i=0;i<IOCLASS_MAX+1;i++){
		    scale_stat(&(stats->sectors_cache_hits_ioclass[i]));
        }
		scale_stat(&stats->sectors_cache_misses);
		scale_stat(&stats->cache_hits);
		scale_stat(&stats->cache_misses);
		scale_stat(&stats->cache_bypass_hits);
		scale_stat(&stats->cache_bypass_misses);
		scale_stat(&stats->cache_readaheads);
		scale_stat(&stats->cache_miss_collisions);
		scale_stat(&stats->sectors_bypassed);
	}
}

//Leo
static void move_stats_accounting(struct cache_accounting* acc){
    int i;
//Leo changed move stat to divide by (weight-1) for modified moving average stats
// TODO: separate out the moving average and collection into two variables
#define move_stat(name) do {						\
	unsigned v = atomic_xchg(&(acc->collector.name), 0);		\
	unsigned long t = v << SCALE_FACTOR;							\
	unsigned long t1 = v / (accounting_weight -1);							\
	acc->five_minute.name += t1;					\
	acc->hour.name += t1;						\
	acc->day.name += t1;						\
	acc->total.name += t;						\
} while (0)
/*    printk(KERN_ERR "1 reset stats t %lu total %lu \n", t, acc->total.name); \ */
    for(i=0;i<IOCLASS_MAX+1;i++){
        move_stat(sectors_cache_hits_ioclass[i]);
    }
	move_stat(sectors_cache_hits);
	move_stat(sectors_cache_misses);
	move_stat(cache_hits);
	move_stat(cache_misses);
	move_stat(cache_bypass_hits);
	move_stat(cache_bypass_misses);
	move_stat(cache_readaheads);
	move_stat(cache_miss_collisions);
	move_stat(sectors_bypassed);
}

static void scale_accounting(unsigned long data)
{
	struct cache_accounting *acc = (struct cache_accounting *) data;

//Leo
move_stats_accounting(acc);

	scale_stats(&acc->total, 0);
	scale_stats(&acc->day, DAY_RESCALE);
	scale_stats(&acc->hour, HOUR_RESCALE);
	scale_stats(&acc->five_minute, FIVE_MINUTE_RESCALE);
/*    
 *  int i;
    printk(KERN_ERR "2 reset stats total %lu \n", acc->total.sectors_cache_misses);
    printk(KERN_ERR "2 reset stats total %lu \n", acc->total.sectors_cache_hits);
    for(i=0;i<IOCLASS_MAX+1;i++){
        printk(KERN_ERR "2 reset stats total %lu \n", acc->total.sectors_cache_hits_ioclass[i]);
    }
*/
	acc->timer.expires += accounting_delay;

	if (!atomic_read(&acc->closing))
		add_timer(&acc->timer);
	else
		closure_return(&acc->cl);
}

static void mark_sectors_cache_stats(struct cache_stat_collector *stats, bool hit, int sectors, int prio)
{
    if (hit){
        atomic_add(sectors,&(stats->sectors_cache_hits_ioclass[PRIO_TO_IOCLASS(prio)]));
        atomic_add(sectors,&stats->sectors_cache_hits);
    }
    else{
        atomic_add(sectors,&stats->sectors_cache_misses);
    }
}

static void mark_cache_stats(struct cache_stat_collector *stats,
			     bool hit, bool bypass)
{
	if (!bypass)
		if (hit)
			atomic_inc(&stats->cache_hits);
		else
			atomic_inc(&stats->cache_misses);
	else
		if (hit)
			atomic_inc(&stats->cache_bypass_hits);
		else
			atomic_inc(&stats->cache_bypass_misses);
}

void bch_mark_sectors_cache_accounting(struct cache_set *c, struct bcache_device* d, bool hit, int sectors,int prio)
{
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	mark_sectors_cache_stats(&dc->accounting.collector, hit, sectors,prio);
    if(tracing_enabled)
        dprintk(1," hits %d misses %d \n", atomic_read(&dc->accounting.collector.sectors_cache_hits), atomic_read(&dc->accounting.collector.sectors_cache_misses));
	mark_sectors_cache_stats(&c->accounting.collector, hit, sectors,prio);
#ifdef CONFIG_CGROUP_BCACHE
	mark_sectors_cache_stats(&(bch_bio_to_cgroup(s->orig_bio)->stats), hit, sectors,prio);
#endif
}

void bch_mark_cache_accounting(struct cache_set *c, struct bcache_device *d,
			       bool hit, bool bypass)
{
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	mark_cache_stats(&dc->accounting.collector, hit, bypass);
	mark_cache_stats(&c->accounting.collector, hit, bypass);
#ifdef CONFIG_CGROUP_BCACHE
	mark_cache_stats(&(bch_bio_to_cgroup(s->orig_bio)->stats), hit, bypass);
#endif
}

void bch_mark_cache_readahead(struct cache_set *c, struct bcache_device *d)
{
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	atomic_inc(&dc->accounting.collector.cache_readaheads);
	atomic_inc(&c->accounting.collector.cache_readaheads);
}

void bch_mark_cache_miss_collision(struct cache_set *c, struct bcache_device *d)
{
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	atomic_inc(&dc->accounting.collector.cache_miss_collisions);
	atomic_inc(&c->accounting.collector.cache_miss_collisions);
}

void bch_mark_sectors_bypassed(struct cache_set *c, struct cached_dev *dc,
			       int sectors)
{
	atomic_add(sectors, &dc->accounting.collector.sectors_bypassed);
	atomic_add(sectors, &c->accounting.collector.sectors_bypassed);
}

void bch_cache_accounting_init(struct cache_accounting *acc,
			       struct closure *parent)
{
	kobject_init(&acc->total.kobj,		&bch_stats_ktype);
	kobject_init(&acc->five_minute.kobj,	&bch_stats_ktype);
	kobject_init(&acc->hour.kobj,		&bch_stats_ktype);
	kobject_init(&acc->day.kobj,		&bch_stats_ktype);

	closure_init(&acc->cl, parent);
	init_timer(&acc->timer);
	acc->timer.expires	= jiffies + accounting_delay;
	acc->timer.data		= (unsigned long) acc;
	acc->timer.function	= scale_accounting;
	add_timer(&acc->timer);
}
