#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/genhd.h>
#include <linux/interrupt.h>
#include <linux/completion.h>
#include <linux/buffer_head.h>
#include <linux/bio.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/blkdev.h>
#include <linux/hdreg.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/stat.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/fcntl.h>
#include <linux/random.h>
#include <linux/jbd.h>
#include <linux/workqueue.h>
#include <linux/kthread.h>
#include <linux/bitmap.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kallsyms.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include <asm/checksum.h>
#include <asm/percpu.h>
#include <asm/stacktrace.h>
#include <linux/kallsyms.h>


#include <linux/kdev_t.h>
#include <linux/bio.h>
#include <linux/blkdev.h>
#include <linux/blk_types.h>
#include <linux/rwlock.h>

#define POW2(y) (1<<y)

// HIGH means upper layer of the stacked storge system
// LOW ( or rather undefinition of HIGH )means lower layer of the stacked storage system

//#define HIGH

#define LOW

//#define DISKDRIVER_KERN_ASSIST

#include "../kvm-kmod-3.10.1/include/linux/jadu.h"

//#define REMUS

#define BCACHE
//#define SMARTCACHE
//#define EXTCACHE // use faster external disk(eg. ssd) for items that do not fit in mem
#define STATS
//#define LIFETIMES

#ifdef LIFETIMES
#define LIFETIME_SERIES_UNIT_MSECS (100)
#define LIFETIME_SERIES_MAX (360) // in unit above
#endif


#ifdef HIGH
#define LAYER ("hi")
#else
#define LAYER ("lo")
#endif

#ifdef HIGH
#undef SMARTCACHE
#undef BCACHE
#undef JADUKATA_CHECKSUM
#undef STATS
#endif

#if defined(SMARTCACHE) && defined(BCACHE)
can have both bcache and smartcache at same time
#endif

#ifndef SMARTCACHE
#undef EXTCACHE
#endif

#ifndef BCACHE
#undef BCACHEFILLONREADS
#endif

#ifdef JADUKATA_VMM_COMM
#ifdef HIGH
#define DISKDRIVER_KERN_ASSIST
#endif
#endif

#define DISKDRIVER_BLKSIZE	(4096) /* block size old*/
#define DISKDRIVER_SECSIZE  (4096)
#define NR_SEC_PER_BLK      (1)   

#define DISKDRIVER_BLKSIZE_BITS	    (12)		/* no of bits */
#define DISKDRIVER_HARDSECT		    (512)		/* sector size*/
#define DISKDRIVER_HARDSECT_BITS	(9)		/* no of bits - HARDSECT*/

#define DISKDRIVER_SECTOR_TO_BLOCK(a)	((a) >> (DISKDRIVER_BLKSIZE_BITS - DISKDRIVER_HARDSECT_BITS))
#define DISKDRIVER_BLOCK_TO_SECTOR(a)	((a) << (DISKDRIVER_BLKSIZE_BITS - DISKDRIVER_HARDSECT_BITS))

#define DISKDRIVER_SECTORS_TO_BLOCKS(a)	((a % NR_SEC_PER_BLK == 0)? (a  >> (DISKDRIVER_BLKSIZE_BITS - DISKDRIVER_HARDSECT_BITS)):(a  >> (DISKDRIVER_BLKSIZE_BITS - DISKDRIVER_HARDSECT_BITS))+1)
#define DISKDRIVER_BLOCKS_TO_SECTORS(a)	((a) << (DISKDRIVER_BLKSIZE_BITS - DISKDRIVER_HARDSECT_BITS))

#define diff(a,b) ((b.tv_sec - a.tv_sec) * 1000000 + (b.tv_usec - a.tv_usec))
#define conv(a) (a.tv_sec*1000000 + a.tv_usec)

#define ABSDIFF(a,b)  ((a>b)?a-b:b-a)

// Define debugging macros //
#define DISKDRIVER_DEBUG

#define dprinte(n, f, a...)   \
    do {    \
        if (n) {        \
            printk(KERN_ERR "%s %s:%d - %d %s %d - " f,LAYER,__FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##a); \
        }       \
    } while (0);

//#ifdef DISKDRIVER_DEBUG

#define dprintk(n, f, a...)   \
    do {    \
        extern int dprint_enabled; \
        if (dprint_enabled && n) {        \
            printk(KERN_ERR "%s %s:%d - %d %s %d - " f,LAYER,__FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##a); \
        }       \
    } while (0);

//#else
//#define dprintk(n, f, a...)   
//#endif

extern void msleep(unsigned int);
//extern void print_bio(char* id, struct bio*);

#define print_bio(id,diskdriver_bio) \
    do { \
        if(diskdriver_bio != NULL){ \
            dprintk(1,"diskdriver %s pid %d cmd %s sector %ld size %u block %lu rw %lu bdev %p \n", id, current->pid,current->comm, diskdriver_bio->bi_sector, bio_sectors(diskdriver_bio),DISKDRIVER_SECTOR_TO_BLOCK(diskdriver_bio->bi_sector), diskdriver_bio->bi_rw, diskdriver_bio->bi_bdev); \
        } \
    } while (0); 
