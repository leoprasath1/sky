/*
 * Main bcache entry point - handle a read or a write request and decide what to
 * do with it; the make_request functions are called by the block layer.
 *
 * Copyright 2010, 2011 Kent Overstreet <kent.overstreet@gmail.com>
 * Copyright 2012 Google, Inc.
 */

#include "bcache.h"
#include "btree.h"
#include "debug.h"
#include "request.h"
#include "writeback.h"

#include <linux/cgroup.h>
#include <linux/module.h>
#include <linux/hash.h>
#include <linux/random.h>
#include "blk-cgroup.h"

#include <trace/events/bcache.h>

#define CUTOFF_CACHE_ADD	95
#define CUTOFF_CACHE_READA	90

struct kmem_cache *bch_search_cache;

static void check_should_skip(struct cached_dev *, struct search *);

/* Cgroup interface */

#ifdef CONFIG_CGROUP_BCACHE
static struct bch_cgroup bcache_default_cgroup = { .cache_mode = -1 };

static struct bch_cgroup *cgroup_to_bcache(struct cgroup *cgroup)
{
	struct cgroup_subsys_state *css;
	return cgroup &&
		(css = cgroup_subsys_state(cgroup, bcache_subsys_id))
		? container_of(css, struct bch_cgroup, css)
		: &bcache_default_cgroup;
}

struct bch_cgroup *bch_bio_to_cgroup(struct bio *bio)
{
	struct cgroup_subsys_state *css = bio->bi_css
		? cgroup_subsys_state(bio->bi_css->cgroup, bcache_subsys_id)
		: task_subsys_state(current, bcache_subsys_id);

	return css
		? container_of(css, struct bch_cgroup, css)
		: &bcache_default_cgroup;
}

static ssize_t cache_mode_read(struct cgroup *cgrp, struct cftype *cft,
			struct file *file,
			char __user *buf, size_t nbytes, loff_t *ppos)
{
	char tmp[1024];
	int len = bch_snprint_string_list(tmp, PAGE_SIZE, bch_cache_modes,
					  cgroup_to_bcache(cgrp)->cache_mode + 1);

	if (len < 0)
		return len;

	return simple_read_from_buffer(buf, nbytes, ppos, tmp, len);
}

static int cache_mode_write(struct cgroup *cgrp, struct cftype *cft,
			    const char *buf)
{
	int v = bch_read_string_list(buf, bch_cache_modes);
	if (v < 0)
		return v;

	cgroup_to_bcache(cgrp)->cache_mode = v - 1;
	return 0;
}

static u64 bch_verify_read(struct cgroup *cgrp, struct cftype *cft)
{
	return cgroup_to_bcache(cgrp)->verify;
}

static int bch_verify_write(struct cgroup *cgrp, struct cftype *cft, u64 val)
{
	cgroup_to_bcache(cgrp)->verify = val;
	return 0;
}

//Leo
static u64 bch_sectors_cache_hits_read(struct cgroup *cgrp, struct cftype *cft)
{
	struct bch_cgroup *bcachecg = cgroup_to_bcache(cgrp);
	return atomic_read(&bcachecg->stats.sectors_cache_hits);
}

static u64 bch_sectors_cache_misses_read(struct cgroup *cgrp, struct cftype *cft)
{
	struct bch_cgroup *bcachecg = cgroup_to_bcache(cgrp);
	return atomic_read(&bcachecg->stats.sectors_cache_misses);
}

static u64 bch_cache_hits_read(struct cgroup *cgrp, struct cftype *cft)
{
	struct bch_cgroup *bcachecg = cgroup_to_bcache(cgrp);
	return atomic_read(&bcachecg->stats.cache_hits);
}

static u64 bch_cache_misses_read(struct cgroup *cgrp, struct cftype *cft)
{
	struct bch_cgroup *bcachecg = cgroup_to_bcache(cgrp);
	return atomic_read(&bcachecg->stats.cache_misses);
}

static u64 bch_cache_bypass_hits_read(struct cgroup *cgrp,
					 struct cftype *cft)
{
	struct bch_cgroup *bcachecg = cgroup_to_bcache(cgrp);
	return atomic_read(&bcachecg->stats.cache_bypass_hits);
}

static u64 bch_cache_bypass_misses_read(struct cgroup *cgrp,
					   struct cftype *cft)
{
	struct bch_cgroup *bcachecg = cgroup_to_bcache(cgrp);
	return atomic_read(&bcachecg->stats.cache_bypass_misses);
}

static struct cftype bch_files[] = {
	{
		.name		= "cache_mode",
		.read		= cache_mode_read,
		.write_string	= cache_mode_write,
	},
	{
		.name		= "verify",
		.read_u64	= bch_verify_read,
		.write_u64	= bch_verify_write,
	},
	{
		.name		= "sectors_cache_hits",
		.read_u64	= bch_sectors_cache_hits_read,
	},
	{
		.name		= "sectors_cache_misses",
		.read_u64	= bch_sectors_cache_misses_read,
	},
	{
		.name		= "cache_hits",
		.read_u64	= bch_cache_hits_read,
	},
	{
		.name		= "cache_misses",
		.read_u64	= bch_cache_misses_read,
	},
	{
		.name		= "cache_bypass_hits",
		.read_u64	= bch_cache_bypass_hits_read,
	},
	{
		.name		= "cache_bypass_misses",
		.read_u64	= bch_cache_bypass_misses_read,
	},
	{ }	/* terminate */
};

static void init_bch_cgroup(struct bch_cgroup *cg)
{
	cg->cache_mode = -1;
}

static struct cgroup_subsys_state *bcachecg_create(struct cgroup *cgroup)
{
	struct bch_cgroup *cg;

	cg = kzalloc(sizeof(*cg), GFP_KERNEL);
	if (!cg)
		return ERR_PTR(-ENOMEM);
	init_bch_cgroup(cg);
	return &cg->css;
}

static void bcachecg_destroy(struct cgroup *cgroup)
{
	struct bch_cgroup *cg = cgroup_to_bcache(cgroup);
	free_css_id(&bcache_subsys, &cg->css);
	kfree(cg);
}

struct cgroup_subsys bcache_subsys = {
	.create		= bcachecg_create,
	.destroy	= bcachecg_destroy,
	.subsys_id	= bcache_subsys_id,
	.name		= "bcache",
	.module		= THIS_MODULE,
};
EXPORT_SYMBOL_GPL(bcache_subsys);
#endif

static unsigned cache_mode(struct cached_dev *dc, struct bio *bio)
{
#ifdef CONFIG_CGROUP_BCACHE
	int r = bch_bio_to_cgroup(bio)->cache_mode;
	if (r >= 0)
		return r;
#endif
	return BDEV_CACHE_MODE(&dc->sb);
}

static bool verify(struct cached_dev *dc, struct bio *bio)
{
#ifdef CONFIG_CGROUP_BCACHE
	if (bch_bio_to_cgroup(bio)->verify)
		return true;
#endif
	return dc->verify;
}

static void bio_csum(struct bio *bio, struct bkey *k)
{
	struct bio_vec *bv;
	uint64_t csum = 0;
	int i;

	bio_for_each_segment(bv, bio, i) {
		void *d = kmap(bv->bv_page) + bv->bv_offset;
		csum = bch_crc64_update(csum, d, bv->bv_len);
		kunmap(bv->bv_page);
	}

	k->ptr[KEY_PTRS(k)] = csum & (~0ULL >> 1);
}

/* Insert data into cache */

static void bio_invalidate(struct closure *cl)
{
	struct btree_op *op = container_of(cl, struct btree_op, cl);
	struct bio *bio = op->cache_bio;

	pr_debug("invalidating %i sectors from %llu",
		 bio_sectors(bio), (uint64_t) bio->bi_sector);

	while (bio_sectors(bio)) {
		unsigned len = min(bio_sectors(bio), 1U << 14);

		if (bch_keylist_realloc(&op->keys, 0, op->c))
			goto out;

		bio->bi_sector	+= len;
		bio->bi_size	-= len << 9;

		bch_keylist_add(&op->keys,
				&KEY(op->inode, bio->bi_sector, len));
	}

	op->insert_data_done = true;
	bio_put(bio);
out:
	continue_at(cl, bch_journal, bcache_wq);
}

struct open_bucket {
	struct list_head	list;
	struct task_struct	*last;
	unsigned		sectors_free;
	BKEY_PADDED(key);
};

void bch_open_buckets_free(struct cache_set *c)
{
	struct open_bucket *b;

	while (!list_empty(&c->data_buckets)) {
		b = list_first_entry(&c->data_buckets,
				     struct open_bucket, list);
		list_del(&b->list);
		kfree(b);
	}
}

int bch_open_buckets_alloc(struct cache_set *c)
{
	int i;

	spin_lock_init(&c->data_bucket_lock);

	for (i = 0; i < 6; i++) {
		struct open_bucket *b = kzalloc(sizeof(*b), GFP_KERNEL);
		if (!b)
			return -ENOMEM;

		list_add(&b->list, &c->data_buckets);
	}

	return 0;
}

/*
 * We keep multiple buckets open for writes, and try to segregate different
 * write streams for better cache utilization: first we look for a bucket where
 * the last write to it was sequential with the current write, and failing that
 * we look for a bucket that was last used by the same task.
 *
 * The ideas is if you've got multiple tasks pulling data into the cache at the
 * same time, you'll get better cache utilization if you try to segregate their
 * data and preserve locality.
 *
 * For example, say you've starting Firefox at the same time you're copying a
 * bunch of files. Firefox will likely end up being fairly hot and stay in the
 * cache awhile, but the data you copied might not be; if you wrote all that
 * data to the same buckets it'd get invalidated at the same time.
 *
 * Both of those tasks will be doing fairly random IO so we can't rely on
 * detecting sequential IO to segregate their data, but going off of the task
 * should be a sane heuristic.
 */
/*
static struct open_bucket *pick_data_bucket(struct cache_set *c,
					    const struct bkey *search,
					    struct task_struct *task,
					    struct bkey *alloc)
{
	struct open_bucket *ret, *ret_task = NULL;

    list_for_each_entry_reverse(ret, &c->data_buckets, list){
        if (!bkey_cmp(&ret->key, search)){
            goto found;
        }
        else{
            if (ret->last == task){
                ret_task = ret;
            }
        }
    }

	ret = ret_task ?: list_first_entry(&c->data_buckets,
					   struct open_bucket, list);
	
found:
	if (!ret->sectors_free && KEY_PTRS(alloc)) {
		ret->sectors_free = c->sb.bucket_size;
		bkey_copy(&ret->key, alloc);
		bkey_init(alloc);
	}

	if (!ret->sectors_free)
		ret = NULL;

	return ret;
}
*/
/*
 * Allocates some space in the cache to write to, and k to point to the newly
 * allocated space, and updates KEY_SIZE(k) and KEY_OFFSET(k) (to point to the
 * end of the newly allocated space).
 *
 * May allocate fewer sectors than @sectors, KEY_SIZE(k) indicates how many
 * sectors were actually allocated.
 *
 * If s->writeback is true, will not fail.
 */
/*
static bool bch_alloc_sectors_old(struct bkey *k, unsigned sectors,struct search *s)
{
	struct cache_set *c = s->op.c;
	struct open_bucket *b;
	BKEY_PADDED(key) alloc;
	struct closure cl, *w = NULL;
	unsigned i;

	if (s->writeback) {
		closure_init_stack(&cl);
		w = &cl;
	}

	
	//We might have to allocate a new bucket, which we can't do with a
	//spinlock held. So if we have to allocate, we drop the lock, allocate
	//and then retry. KEY_PTRS() indicates whether alloc points to
	//allocated bucket(s).
	

	bkey_init(&alloc.key);
	spin_lock(&c->data_bucket_lock);

	while (!(b = pick_data_bucket(c, k, s->task, &alloc.key))) {
		unsigned watermark = s->op.write_prio
			? WATERMARK_MOVINGGC
			: WATERMARK_NONE;

		spin_unlock(&c->data_bucket_lock);

		if (bch_bucket_alloc_set(c, watermark, &alloc.key, 1, w))
			return false;

		spin_lock(&c->data_bucket_lock);
	}

	
	//If we had to allocate, we might race and not need to allocate the
	//second time we call find_data_bucket(). If we allocated a bucket but
	//didn't use it, drop the refcount bch_bucket_alloc_set() took:
	
	if (KEY_PTRS(&alloc.key))
		__bkey_put(c, &alloc.key);

	for (i = 0; i < KEY_PTRS(&b->key); i++)
		EBUG_ON(ptr_stale(c, &b->key, i));

	// Set up the pointer to the space we're allocating:

	for (i = 0; i < KEY_PTRS(&b->key); i++)
		k->ptr[i] = b->key.ptr[i];

	sectors = min(sectors, b->sectors_free);

	SET_KEY_OFFSET(k, KEY_OFFSET(k) + sectors);
	SET_KEY_SIZE(k, sectors);
	SET_KEY_PTRS(k, KEY_PTRS(&b->key));

	
	//Move b to the end of the lru, and keep track of what this bucket was
	//last used for:
	
	list_move_tail(&b->list, &c->data_buckets);
	bkey_copy_key(&b->key, k);
	b->last = s->task;

	b->sectors_free	-= sectors;

	for (i = 0; i < KEY_PTRS(&b->key); i++) {
		SET_PTR_OFFSET(&b->key, i, PTR_OFFSET(&b->key, i) + sectors);

		atomic_long_add(sectors,
				&PTR_CACHE(c, &b->key, i)->sectors_written);
	}

	if (b->sectors_free < c->sb.block_size)
		b->sectors_free = 0;

	
	//k takes refcounts on the buckets it points to until it's inserted
	//into the btree, but if we're done with this bucket we just transfer
	//get_data_bucket()'s refcount.
	
	if (b->sectors_free)
		for (i = 0; i < KEY_PTRS(&b->key); i++)
			atomic_inc(&PTR_BUCKET(c, &b->key, i)->pin);

	spin_unlock(&c->data_bucket_lock);
	return true;
}
*/

static bool bch_alloc_sectors_new(struct bkey *k, unsigned sectors, struct search *s)
{
	struct cache_set *c = s->op.c;
	BKEY_PADDED(key) alloc;
	struct closure cl, *w = NULL;
	unsigned i;
    
    unsigned watermark = s->op.write_prio
        ? WATERMARK_MOVINGGC
        : WATERMARK_NONE;

	if (s->writeback) {
		closure_init_stack(&cl);
		w = &cl;
	}

	
	//We might have to allocate a new bucket, which we can't do with a
	//spinlock held. So if we have to allocate, we drop the lock, allocate
	//and then retry. KEY_PTRS() indicates whether alloc points to
	//allocated bucket(s).
	

    bkey_init(&alloc.key);

    if (bch_bucket_alloc_set(c, watermark, &alloc.key, 1, w))
        return false;
		
    spin_lock(&c->data_bucket_lock);

	// Set up the pointer to the space we're allocating:
	for (i = 0; i < KEY_PTRS(&alloc.key); i++)
		k->ptr[i] = alloc.key.ptr[i];

	sectors = min(sectors,  (unsigned)c->sb.bucket_size);

	SET_KEY_OFFSET(k, KEY_OFFSET(k) + sectors);
	SET_KEY_SIZE(k, sectors);
	SET_KEY_PTRS(k, KEY_PTRS(&alloc.key));

	for (i = 0; i < KEY_PTRS(&alloc.key); i++) {
		//SET_PTR_OFFSET(&alloc.key, i, PTR_OFFSET(&alloc.key, i) + sectors);

		atomic_long_add(sectors,
				&PTR_CACHE(c, &alloc.key, i)->sectors_written);
	}

    //for (i = 0; i < KEY_PTRS(&alloc.key); i++)
    //    atomic_inc(&PTR_BUCKET(c, &alloc.key, i)->pin);

	spin_unlock(&c->data_bucket_lock);
	return true;
}

static bool bch_alloc_sectors(struct bkey *k, unsigned sectors, struct search *s){
    return bch_alloc_sectors_new(k,sectors,s);
}

static void bch_insert_data_error(struct closure *cl)
{
	struct btree_op *op = container_of(cl, struct btree_op, cl);

	/*
	 * Our data write just errored, which means we've got a bunch of keys to
	 * insert that point to data that wasn't succesfully written.
	 *
	 * We don't have to insert those keys but we still have to invalidate
	 * that region of the cache - so, if we just strip off all the pointers
	 * from the keys we'll accomplish just that.
	 */

	struct bkey *src = op->keys.bottom, *dst = op->keys.bottom;

	while (src != op->keys.top) {
		struct bkey *n = bkey_next(src);

		SET_KEY_PTRS(src, 0);
		bkey_copy(dst, src);

		dst = bkey_next(dst);
		src = n;
	}

	op->keys.top = dst;

	bch_journal(cl);
}

static void bch_insert_data_endio(struct bio *bio, int error)
{
	struct closure *cl = bio->bi_private;
	struct btree_op *op = container_of(cl, struct btree_op, cl);
	struct search *s = container_of(op, struct search, op);

	if (error) {
		/* TODO: We could try to recover from this. */
		if (s->writeback)
			s->error = error;
		else if (s->write)
			set_closure_fn(cl, bch_insert_data_error, bcache_wq);
		else
			set_closure_fn(cl, NULL, NULL);
	}

	bch_bbio_endio(op->c, bio, error, "writing data to cache");
}

struct timeval hit_time = {.tv_sec =0, .tv_usec =0}, miss_time = {.tv_sec =0, .tv_usec =0};
atomic_t hit_sum, hit_count;
atomic_t miss_sum, miss_count;

void bio_timing_clear(void){
    atomic_set(&hit_sum,0);
    atomic_set(&hit_count,0);
    atomic_set(&miss_sum,0);
    atomic_set(&miss_count,0);
    memset(&hit_time, 0, sizeof(struct timeval));
    memset(&miss_time, 0, sizeof(struct timeval));
}

void bio_timing(char** outptrptr, bool hit){
    int r, count;
    char*outptr = *outptrptr;
    struct timeval *tt = hit?&hit_time:&miss_time;
    atomic_t *at = hit?&hit_sum:&miss_sum;
    atomic_t *ct = hit?&hit_count:&miss_count;
	do {
		r = atomic_read(at);
	} while (atomic_cmpxchg(at, r, 0) != r);

    tt->tv_sec += r/1000000;
    tt->tv_usec += r%1000000;
    
    count = atomic_read(ct);
    if(count ==0){
        count = 1;
    }
    outptr += sprintf(outptr,"%s time %lu.%lu count %d avg %lu ", hit?"hit":"miss",tt->tv_sec,tt->tv_usec,count,((tt->tv_sec*(1000000/count)) + tt->tv_usec/count));
    *outptrptr = outptr;
}

//Leo
#ifdef LOW
static void bio_account_time(struct bio* bio,bool hit){
    int r;
    unsigned long time = bio->bcache_returned - bio->bcache_issued;
    struct timeval *tt = hit?&hit_time:&miss_time;
    atomic_t *at = hit?&hit_sum:&miss_sum;
    atomic_t *ct = hit?&hit_count:&miss_count;

	atomic_inc(ct);
	atomic_add(time, at);
	do {
		r = atomic_read(at);

		if (r <= 1000000)
			return;
	} while (atomic_cmpxchg(at, r, 0) != r);

    tt->tv_sec += r/1000000;
    tt->tv_usec += r%1000000;
}
#endif

extern int cache_smart_mode;
struct bcache_device *update_priority_d = NULL;
struct cached_dev* update_delay_dc = NULL;
unsigned bcache_sb_offset = 0;
#if defined(DEDUP_WAR_PREFETCH) || defined(DEDUP_W_PREFETCH)
extern unsigned dmdedup_bcache_sb_offset;
#endif

#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)

//adjust details
//insights are in guest os sectors
//btree and elsewhere in bcache is in bcache sectors

/*
#ifdef STRICT_APP_PRIORITY
#undef METADATA_IOCLASS
#define METADATA_IOCLASS (0)
#endif
*/

uint16_t get_priority(struct bio* pbio){
    uint16_t ret_class = 0;
    uint16_t ret_prio = 0;
    //adjust for bcache super block
    pbio->bi_sector -= bcache_sb_offset; 
    if(is_metadata(pbio)){
        ret_class = METADATA_IOCLASS;
    }else{
        ret_class = is_smallfile(pbio);
    }
    ret_prio = IOCLASS_TO_PRIO(ret_class);
    dprinttrace("got priority %d ioclass %d for sector %ld block %ld size %u \n", ret_prio, ret_class, pbio->bi_sector, pbio->bi_sector/8, bio_sectors(pbio));

    //adjust for bcache super block
    pbio->bi_sector += bcache_sb_offset; 
    return ret_prio;
}
#endif

static void bch_insert_data_loop(struct closure *cl)
{
	struct btree_op *op = container_of(cl, struct btree_op, cl);
	struct search *s = container_of(op, struct search, op);
	struct bio *bio = op->cache_bio, *n;
    //Leo
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
    uint16_t prio = get_priority(bio);
    if(bio_data_dir(bio) == READ){
        if(prio <= READ_TEMP_PRIO){
            prio = READ_TEMP_PRIO;
        }
    }
#else
    uint16_t prio = INITIAL_PRIO;
#endif
    if(tracing_enabled)
        dprinte(1,"bch insert data loop sector %ld size %d\n",s->org_bio?s->org_bio->bi_sector:0, s->org_bio?bio_sectors(s->org_bio):0);

	if (op->skip){
        if(tracing_enabled)
            dprinte(1,"bch insert data loop skipping\n");
		return bio_invalidate(cl);
    }

	if (atomic_sub_return(bio_sectors(bio), &op->c->sectors_to_gc) < 0) {
		set_gc_sectors(op->c);
		bch_queue_gc(op->c);
	}

	/*
	 * Journal writes are marked REQ_FLUSH; if the original write was a
	 * flush, it'll wait on the journal write.
	 */
	bio->bi_rw &= ~(REQ_FLUSH|REQ_FUA);

	do {
		unsigned i;
		struct bkey *k;
		struct bio_set *split = s->d
			? s->d->bio_split : op->c->bio_split;

		/* 1 for the device pointer and 1 for the chksum */
		if (bch_keylist_realloc(&op->keys,
					1 + (op->csum ? 1 : 0),
					op->c)){
            if(tracing_enabled)
                dprinte(1,"bch insert data loop keylist realloc skip\n");
			continue_at(cl, bch_journal, bcache_wq);
        }

		k = op->keys.top;
		bkey_init(k);
		SET_KEY_INODE(k, op->inode);
		SET_KEY_OFFSET(k, bio->bi_sector);

		if (!bch_alloc_sectors(k, bio_sectors(bio), s)){
            if(tracing_enabled)
                dprinte(1,"bch_alloc_sectors failed \n");
			goto err;
        }

		n = bch_bio_split(bio, KEY_SIZE(k), GFP_NOIO, split);
		n->bi_end_io	= bch_insert_data_endio;
		n->bi_private	= cl;

        if(tracing_enabled && (n == bio)){
            dprinte(1,"bio split ending loop n %p bio %p \n",n,bio);
        }

            
        //Leo
        for (i = 0; i < KEY_PTRS(k); i++){
            //Leo leo >0 here but >=0 elsewhere : for 0, we just dont want prio to be updated in 0 mode
            PTR_BUCKET(op->c, k,i)->prio = cache_smart_mode>0?prio:INITIAL_PRIO;
            PTR_BUCKET(op->c, k,i)->org_prio = prio;
            if(tracing_enabled)
                dprinte(1,"insert setting priority bio sector %lu bio size %u nsector %lu nsize %u bucketnr %lu priority %d \n",bio->bi_sector,bio_sectors(bio),n->bi_sector,bio_sectors(n), PTR_BUCKET_NR(op->c,k,i), prio);
        }

		if (s->writeback) {
			SET_KEY_DIRTY(k, true);

			for (i = 0; i < KEY_PTRS(k); i++){
				SET_GC_MARK(PTR_BUCKET(op->c, k, i),
					    GC_MARK_DIRTY);
			}
		}

		SET_KEY_CSUM(k, op->csum);
		if (KEY_CSUM(k))
			bio_csum(n, k);

		trace_bcache_cache_insert(k);
		bch_keylist_push(&op->keys);

		n->bi_rw |= REQ_WRITE;
		bch_submit_bbio(n, op->c, k, 0);
	} while (n != bio);

	op->insert_data_done = true;
	continue_at(cl, bch_journal, bcache_wq);
err:
    if(tracing_enabled)
        dprinte(1,"bch_insert_data_loop error \n");
	/* bch_alloc_sectors() blocks if s->writeback = true */
	BUG_ON(s->writeback);

	/*
	 * But if it's not a writeback write we'd rather just bail out if
	 * there aren't any buckets ready to write to - it might take awhile and
	 * we might be starving btree writes for gc or something.
	 */

	if (s->write) {
		/*
		 * Writethrough write: We can't complete the write until we've
		 * updated the index. But we don't want to delay the write while
		 * we wait for buckets to be freed up, so just invalidate the
		 * rest of the write.
		 */
		op->skip = true;
		return bio_invalidate(cl);
	} else {
		/*
		 * From a cache miss, we can just insert the keys for the data
		 * we have written or bail out if we didn't do anything.
		 */
		op->insert_data_done = true;
		bio_put(bio);

		if (!bch_keylist_empty(&op->keys))
			continue_at(cl, bch_journal, bcache_wq);
		else
			closure_return(cl);
	}
}

/**
 * bch_insert_data - stick some data in the cache
 *
 * This is the starting point for any data to end up in a cache device; it could
 * be from a normal write, or a writeback write, or a write to a flash only
 * volume - it's also used by the moving garbage collector to compact data in
 * mostly empty buckets.
 *
 * It first writes the data to the cache, creating a list of keys to be inserted
 * (if the data had to be fragmented there will be multiple keys); after the
 * data is written it calls bch_journal, and after the keys have been added to
 * the next journal write they're inserted into the btree.
 *
 * It inserts the data in op->cache_bio; bi_sector is used for the key offset,
 * and op->inode is used for the key inode.
 *
 * If op->skip is true, instead of inserting the data it invalidates the region
 * of the cache represented by op->cache_bio and op->inode.
 */
void bch_insert_data(struct closure *cl)
{
	struct btree_op *op = container_of(cl, struct btree_op, cl);

	bch_keylist_init(&op->keys);
	bio_get(op->cache_bio);
	bch_insert_data_loop(cl);
}

void bch_btree_insert_async(struct closure *cl)
{
	struct btree_op *op = container_of(cl, struct btree_op, cl);
	struct search *s = container_of(op, struct search, op);
    
    if(tracing_enabled)
        dprinte(1,"bch_btree_insert_async sector %ld size %d insert_data_done %d\n", s->org_bio?s->org_bio->bi_sector:0, s->org_bio?bio_sectors(s->org_bio):0, op->insert_data_done);

	if (bch_btree_insert(op, op->c)) {
		s->error		= -ENOMEM;
		op->insert_data_done	= true;
	}

	if (op->insert_data_done) {
		bch_keylist_free(&op->keys);
		closure_return(cl);
	} else
		continue_at(cl, bch_insert_data_loop, bcache_wq);
}

/* Common code for the make_request functions */

static void request_endio(struct bio *bio, int error)
{
	struct closure *cl = bio->bi_private;

	if (error) {
		struct search *s = container_of(cl, struct search, cl);
		s->error = error;
		/* Only cache read errors are recoverable */
		s->recoverable = false;
	}

	bio_put(bio);
	closure_put(cl);
}

static void acc_request_endio(struct bio *bio, int error){
#ifdef LOW
    {
        struct timeval now;
        do_gettimeofday(&now);
        bio->bcache_returned = conv(now);
        bio_account_time(bio,false);
    }
    request_endio(bio,error);
#endif
}

void bch_cache_read_endio(struct bio *bio, int error)
{
	struct bbio *b = container_of(bio, struct bbio, bio);
	struct closure *cl = bio->bi_private;
	struct search *s = container_of(cl, struct search, cl);
    
#ifdef LOW
    //Leo leo
    {
        struct timeval now;
        do_gettimeofday(&now);
        bio->bcache_returned = conv(now);
        bio_account_time(bio,true);
    }
#endif

	/*
	 * If the bucket was reused while our bio was in flight, we might have
	 * read the wrong data. Set s->error but not error so it doesn't get
	 * counted against the cache device, but we'll still reread the data
	 * from the backing device.
	 */

	if (error)
		s->error = error;
	else if (ptr_stale(s->op.c, &b->key, 0)) {
		atomic_long_inc(&s->op.c->cache_read_races);
		s->error = -EINTR;
	}
    
    //tried process each of the split bios but ended up going for just processing
    // the orig bio approach because looks like not all partial misses in a request     
    // end up read into a cache because s->op.cache_bio and s->cache_miss can just 
    // hold one bio. So, if we just process orig_bio its backing address space mappings
    // are also mapped into split bios.
    /*
    if(!s->error){
        //reads from bcache hits are handled at bch_cache_read_endio
        //because they could be split from orig_bio
        //Leo
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
        {
            sector_t tsector;
            unsigned int tsize;
            tsector = bio->bi_sector;
            tsize = bio->bi_size;
            bio->bi_sector = b->org_sector;
            bio->bi_size = b->org_size;
            process_jadu_reads("cache_read",bio);
            bio->bi_sector = tsector;
            bio->bi_size = tsize;
        }
#endif
    }else{
        dprinte(1,"ERROR reading from cache hit error %d s->error %d\n", error, s->error);
    }
    */

	bch_bbio_endio(s->op.c, bio, error, "reading from cache");
}

static void bio_complete(struct search *s)
{
	if (s->orig_bio) {
		int cpu, rw = bio_data_dir(s->orig_bio);
		unsigned long duration = jiffies - s->start_time;

		cpu = part_stat_lock();
		part_round_stats(cpu, &s->d->disk->part0);
		part_stat_add(cpu, &s->d->disk->part0, ticks[rw], duration);
		part_stat_unlock();
#ifdef LOW
		trace_bcache_request_end(s->d, s->orig_bio);
#endif
		bio_endio(s->orig_bio, s->error);
		s->orig_bio = NULL;
	}
}

static void do_bio_hook(struct search *s)
{
	struct bio *bio = &s->bio.bio;
	memcpy(bio, s->orig_bio, sizeof(struct bio));

	bio->bi_end_io		= request_endio;
	bio->bi_private		= &s->cl;
	atomic_set(&bio->bi_cnt, 3);
}

static void search_free(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);
	bio_complete(s);
   
    //Leo
    if(s->org_bio)
        bio_put(s->org_bio);
    
	if (s->op.cache_bio)
		bio_put(s->op.cache_bio);

	if (s->unaligned_bvec)
		mempool_free(s->bio.bio.bi_io_vec, s->d->unaligned_bvec);

	closure_debug_destroy(cl);
	mempool_free(s, s->d->c->search);
}

static struct search *search_alloc(struct bio *bio, struct bcache_device *d)
{
	struct bio_vec *bv;
	struct search *s = mempool_alloc(d->c->search, GFP_NOIO);
	memset(s, 0, offsetof(struct search, op.keys));

	__closure_init(&s->cl, NULL);

	s->op.inode		= d->id;
	s->op.c			= d->c;
	s->d			= d;
	s->op.lock		= -1;
	s->task			= current;
	s->orig_bio		= bio;
	s->write		= (bio->bi_rw & REQ_WRITE) != 0;
	s->op.flush_journal	= (bio->bi_rw & (REQ_FLUSH|REQ_FUA)) != 0;
	s->op.skip		= (bio->bi_rw & REQ_DISCARD) != 0;
	s->recoverable		= 1;
	s->start_time		= jiffies;
	do_bio_hook(s);
    
    //Leo
    // cloning after bcache sb offset addition
    s->org_bio = NULL;
    if(!s->write)
    	s->org_bio = bio_clone_bioset(bio, GFP_NOWAIT,d->bio_split);

	if (bio->bi_size != bio_segments(bio) * PAGE_SIZE) {
		bv = mempool_alloc(d->unaligned_bvec, GFP_NOIO);
		memcpy(bv, bio_iovec(bio),
		       sizeof(struct bio_vec) * bio_segments(bio));

		s->bio.bio.bi_io_vec	= bv;
		s->unaligned_bvec	= 1;
	}

	return s;
}


#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)

void bcache_set_write_delay(int delay){
#if defined(BCACHE)
    update_delay_dc->writeback_delay = delay;    
    dprintk(1, "updating writeback_delay to %d\n", delay);
#endif
}
EXPORT_SYMBOL(bcache_set_write_delay);

//Leo 
//update the priority of the sector
//TODO optimize by collating sectors
//TODO tie diskdriver and bcache more closely
// cannot be called under atomic
int bcache_update_priority(struct block_device* bdev, unsigned long sector, uint16_t ioclass){
    int size;
    struct btree_op operation, *op = &operation;
    //struct cached_dev* dc = (struct cached_dev*)(bdev->bd_holder);
    //struct bcache_device *d = &(dc->disk);
    struct bcache_device *d = update_priority_d;
	memset(op, 0, sizeof(struct btree_op));
    op->lookup_done = false;
    op->inode		= d->id;
    op->c			= d->c;
    op->lock		= -1;
    op->cache_bio   = 0; // used for how many sectors
    op->write_prio = IOCLASS_TO_PRIO(ioclass);
    //adjust for bcache super block
    op->journal = (atomic_t*)(sector + bcache_sb_offset);
    //find the bucket and update the priority
    btree_root(update_priority, op->c, op);
    size = (int)((long)op->cache_bio);
    if(size > d->c->sb.bucket_size){
        dprinte(1, "ERROR incorrect number of sectors reported by update priority %d bucket size %d\n", size, d->c->sb.bucket_size);
        return 0;
    }
    return size;
    /*
	int _r = -EINTR;						
	do {								
		struct btree *_b = (op->c)->root;				
		bool _w = insert_lock(op, _b);				
		rw_lock(_w, _b, _b->level);				
		if (_b == (op->c)->root &&					
		    _w == insert_lock(op, _b))				
			_r = bch_btree_update_priority(_b, op);	
		rw_unlock(_w, _b);					
		bch_cannibalize_unlock(op->c, &(op->cl));		
	} while (_r == -EINTR);						
    */
}

void resize_cache_helper(struct cache* ca , unsigned long percent){
    DECLARE_FIFO(long, temp);
    size_t p = (((size_t)ca->sb.nbuckets * percent) / 100);
    size_t np = ((size_t)ca->sb.nbuckets - p);
    size_t orgp = ((size_t)ca->sb.nbuckets - fifo_used(&ca->resize_hoard));
    long i;
    int count = 0;
    int start = jiffies;
    int sleep_count = 0;
    int sleep_time = 10;
    int last_free_amount = 0;
           
    dprinte(1,"START resize cache percent %lu p %ld np %ld hoardsize %ld hoard used %lu hoard free %lu \n ", percent, p,  np, ca->resize_hoard.size, fifo_used(&ca->resize_hoard), fifo_free(&ca->resize_hoard));
    
    if(ca->resize_hoard.size < np){
        if(tracing_enabled)
            dprinte(1,"resizing the resize hoard fifo ");
		if (!init_fifo_exact(&temp, np, GFP_KERNEL)){
            dprinte(1,"ERROR: could not init temp fifo with size %ld \n", np);
			return;
        }
		fifo_move(&temp, &ca->resize_hoard);
		fifo_swap(&temp, &ca->resize_hoard);
		while (fifo_pop(&temp, i)){
            dprinte(1,"ERROR: still entries left during resize cache \
             %lu %lu %lu \n",p,np,ca->resize_hoard.size);
		    atomic_dec(&ca->buckets[i].pin);
        }
		free_fifo(&temp);
    }

    count = 0;
    while(fifo_used(&ca->resize_hoard) > np){
       count++;
       if(count % 100 == 0){
           dprinte(1,"unhoarded %d buckets\n",count);
           msleep(3);
       }
       if(fifo_pop(&ca->resize_hoard,i)){
           atomic_dec(&ca->buckets[i].pin);
       }else{
           dprinte(1,"ERROR: cannot pop from resize hoard \
           p %ld np %ld size %lu used %lu \n",p,np,ca->resize_hoard.size,fifo_used(&ca->resize_hoard));
       }
    }

    count = 0;
    while(fifo_used(&ca->resize_hoard) < np){
       count++;
       if(count % 100 == 0){
           dprinte(1,"hoarded %d buckets\n",count);
           msleep(3);
       }
       if( (fifo_used(&ca->free)>50) && fifo_pop(&ca->free,i) ){
		   struct bucket *b = ca->buckets + i;
           b->org_prio = HOARD_PRIO;
           fifo_push(&ca->resize_hoard,i);
       }else{
           while (1) {
               sleep_count++;
               set_current_state(TASK_INTERRUPTIBLE);
               if (fifo_used(&ca->free) > 200){
                   break;	
               }
               wake_up_process(ca->alloc_thread);
               if(tracing_enabled && (sleep_count %10)){
                   dprinte(1,"sleeping for more free entries to show up resize hoard size %ld free size %ld \n", fifo_used(&ca->resize_hoard), fifo_used(&ca->free));
               }
               schedule_timeout(msecs_to_jiffies(sleep_time));

               if(fifo_used(&ca->free) > last_free_amount){
                   last_free_amount = fifo_used(&ca->free);
                   if(sleep_time < 10000){
                       sleep_time +=100;
                       if(tracing_enabled)
                           dprinte(1,"updating sleep time to %d last_free_amount %d \n", sleep_time, last_free_amount);
                   }
               }
           }
           __set_current_state(TASK_RUNNING);
       }
    }
    
    dprinte(1,"END time %d msecs resize cache percent %lu p %ld np %ld hoardsize %ld hoard used %lu hoard free %lu \n ", jiffies_to_msecs(jiffies-start) , percent, p,  np, ca->resize_hoard.size, fifo_used(&ca->resize_hoard), fifo_free(&ca->resize_hoard));
    dprinte(1,"org cache size %ld MB resized cache size %ld MB",((orgp*bucket_bytes(ca))/1024/1024) , ((p*bucket_bytes(ca))/1024/1024));
}

void resize_cache_bcache(unsigned long percent){
    unsigned i;
    struct bcache_device *d = update_priority_d;
	for (i = 0; i < d->c->sb.nr_in_set; i++){
        if(d->c->cache[i]){
            resize_cache_helper(d->c->cache[i],percent);
        }
    }
}

void get_cache_distribution_bcache(char* ptr){
    char *tptr = ptr;
    struct bcache_device *d = update_priority_d;
	struct cache *ca;
	struct bucket *b;
    int i,ci;
    int count = 0;
    int org_sum = 0 , sum = 0;
    unsigned long btreesum = 0;
    int org_stats[IOCLASS_MAX+3];
    int stats[IOCLASS_MAX+3];
    int btreestats[(IOCLASS_MAX+3)*2];
    int prios[IOCLASS_MAX+3];
    int hoarded = 0;
    short bucketsectors = d->c->sb.bucket_size;
    struct btree_op operation, *op = &operation;
    //struct cached_dev* dc = (struct cached_dev*)(bdev->bd_holder);
    //struct bcache_device *d = &(dc->disk);

    dprinte(1,"getting cache distribution d %p \n", d);

    for(i=0;i<IOCLASS_MAX+3;i++){
        stats[i] = 0;
        org_stats[i] = 0;
        btreestats[i] = 0;
        btreestats[i+(IOCLASS_MAX+3)] = 0;
        prios[i] = IOCLASS_TO_PRIO(i); 
    }

    prios[IOCLASS_MAX+1] = USHRT_MAX;

    for_each_cache(ca, d->c, ci){
        for_each_bucket(b, ca){
           count++;
           //dprinte(1,"bucket nr %d prio %d  org_prio %d org_stats %d \n", count, b->prio, b->org_prio, org_stats[0]);
           if(b->org_prio == HOARD_PRIO){
               hoarded++;
               continue;
           }
           if(b->org_prio <= VERY_LOW_PRIO){
               org_stats[IOCLASS_MAX+2]+=bucketsectors;
           }
           for(i=0;i<IOCLASS_MAX+2;i++){
               if(b->org_prio <= prios[i]){
                   org_stats[i]+=bucketsectors;
                   break;
               }
           }

           dprinte( (i>IOCLASS_MAX+1), "ERROR bucket org prio does not fit %d \n", b->org_prio);
           if(b->prio <= VERY_LOW_PRIO){
               stats[IOCLASS_MAX+2]+=bucketsectors;
           }
           for(i=0;i<IOCLASS_MAX+2;i++){
               if(b->prio <= prios[i]){
                   stats[i]+=bucketsectors;
                   break;
               }
           }
           
           dprinte( (i>IOCLASS_MAX+1), "ERROR bucket current prio does not fit %d \n", b->prio);

           org_sum+=bucketsectors;
           sum+=bucketsectors;
        }
    }

    tptr += sprintf(tptr,"cache distribution org \n");
    for(i=0;i<IOCLASS_MAX+3;i++){
        char org_p[20];
        jadu_div_emu(org_p,org_stats[i]*100,org_sum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %d \n",i>IOCLASS_MAX?"outside":(i==IOCLASS_MAX?"meta":"data"),i,org_stats[i],org_p,org_sum); 
    }
    tptr += sprintf(tptr,"verylow %d notverylow %d hoarded %d\n", org_stats[IOCLASS_MAX+2], org_stats[0] - org_stats[IOCLASS_MAX+2], hoarded); 
    
    dprinte(1,"%s", ptr);
    ptr = tptr;
    
    tptr += sprintf(tptr,"cache distribution current \n");
    for(i=0;i<IOCLASS_MAX+3;i++){
        char p[20];
        jadu_div_emu(p,stats[i]*100,sum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %d \n",i>IOCLASS_MAX?"outside":(i==IOCLASS_MAX?"meta":"data"),i,stats[i],p,sum); 
    }
    tptr += sprintf(tptr,"verylow %d notverylow %d hoarded %d\n", stats[IOCLASS_MAX+2], stats[0] - stats[IOCLASS_MAX+2], hoarded); 
    
    dprinte(1,"%s", ptr);
    ptr = tptr;

	memset(op, 0, sizeof(struct btree_op));
    op->inode		= d->id;
    op->c			= d->c;
    //op->lock		= -1;
    op->lock		= d->c->root->level+10;
    op->cache_bio = 0;
    op->journal = (atomic_t*)btreestats;
    op->write_prio = 0; // num keys
    //get contents 
    btree_root(cache_distribution, op->c, op);

    btreesum = (unsigned long)op->cache_bio;
    
    tptr += sprintf(tptr,"cache distrubution btree org num_keys %u \n", op->write_prio);
    for(i=0;i<IOCLASS_MAX+3;i++){
        char p[20];
        jadu_div_emu(p,btreestats[i]*100,btreesum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %ld \n",i>IOCLASS_MAX?"outside":(i==IOCLASS_MAX?"meta":"data"),i,btreestats[i],p,btreesum); 
    }
    tptr += sprintf(tptr,"verylow diff %d \n",btreestats[0] - btreestats[IOCLASS_MAX+2]); 

    dprinte(1,"%s", ptr);
    
    tptr += sprintf(tptr,"cache distrubution btree current num_keys %u \n", op->write_prio);
    for(i=0;i<IOCLASS_MAX+3;i++){
        char p[20];
        jadu_div_emu(p,btreestats[i+(IOCLASS_MAX+3)]*100,btreesum,2);
        tptr += sprintf(tptr,"%s ioclass:%d %d p %s total %ld \n",i>IOCLASS_MAX?"outside":(i==IOCLASS_MAX?"meta":"data"),i,btreestats[i+(IOCLASS_MAX+3)],p,btreesum); 
    }
    tptr += sprintf(tptr,"verylow diff %d \n",btreestats[IOCLASS_MAX+3] - btreestats[IOCLASS_MAX+2+(IOCLASS_MAX+3)]); 

    dprinte(1,"%s", ptr);

}

void get_cached_sectors_bcache(char* ptr){
    struct btree_op operation, *op = &operation;
    //struct cached_dev* dc = (struct cached_dev*)(bdev->bd_holder);
    //struct bcache_device *d = &(dc->disk);
    struct bcache_device *d = update_priority_d;
    unsigned long sector, size, prev, count;
    char *tptr = ptr, *pptr = ptr;
    hash_table* ht_contents = NULL;
    ht_create_with_size(&ht_contents,"cachecontents",1);
	memset(op, 0, sizeof(struct btree_op));
    dprinte(1,"getting cache contents d %p op %p \n", d, op);
    op->inode		= d->id;
    op->c			= d->c;
    op->lock		= -1;
    op->cache_bio = 0;
    op->journal = (atomic_t*)ht_contents;
    //get contents 
    btree_root(cache_contents, op->c, op);
    dprinte(1,"cache contents count %lu \n", (unsigned long)op->cache_bio);
    prev = ~0UL;
    count = 0;
    while(ht_pop_val(ht_contents, &sector,&size) != -1){

        //adjust for bcache super block
        sector -= bcache_sb_offset; 
        if(prev == ~0UL || prev == sector){
            prev = sector+size;
            count += size;
        }else{
            if((prev-count) == (sector + size)){
                count += size;
            }else{
                tptr += sprintf(tptr,"%lu-%lu,",prev-count,count);
                if(tracing_enabled){
                    if(tptr-pptr > 300){
                        dprintk(1,"%300s\n",pptr);
                        pptr = tptr;
                    }
                }
                prev = sector + size;
                count = size;
            }
        }
    }
    if(prev != ~0UL){
        tptr += sprintf(tptr,"%lu-%lu,",prev-count,count);
    }
    if(tracing_enabled){
        dprintk(1,"%s\n",pptr);
    }
    ht_destroy(ht_contents);
}
#else
void bcache_update_priority(struct block_device* bdev, unsigned long sector, uint16_t ioclass){
}
void get_cache_distribution_bcache(char* ptr){
}
#endif

void bcache_splitter_helper(struct work_struct *wk){
    struct bcache_device *d = update_priority_d;
    struct delayed_work *dwork = to_delayed_work(wk);
    struct splitter_work_struct * swork = container_of(dwork, struct splitter_work_struct, work);
    if(d != NULL){
        struct btree_op operation, *op = &operation;
        bool split_happened = false;
        while(true){
            memset(op, 0, sizeof(struct btree_op));
            __closure_init(&op->cl, NULL);
            op->inode		= d->id;
            op->c			= d->c;
            op->lock		= d->c->root->level+10;
            op->flush_journal = false;
            op->cache_bio = 0;
            op->skip = false;
            bch_keylist_init(&op->keys);
            btree_root(splitter,op->c,op);
            if(tracing_enabled)
                dprinte(1,"splitter splits done %lu\n", (unsigned long)op->cache_bio);
            if(op->cache_bio == NULL){
                break;
            }else{
                split_happened = true;
            }
            msleep(500);
        }
        if(split_happened){
            swork->check_period = 500;
        }else{
            if(swork->check_period < 10000){
                swork->check_period += 500;
            }
        }
    }
}

void bcache_splitter_adhoc(struct work_struct *wk){
    extern struct splitter_work_struct splitter_work; 
    extern struct semaphore split_sem;
    struct delayed_work *dwork = to_delayed_work(wk);
    struct splitter_work_struct * swork = container_of(dwork, struct splitter_work_struct, work);
    splitter_work.check_period = 500;
    bcache_splitter_helper(wk);
    up(&split_sem);
    kfree(swork);
}

void bcache_splitter(struct work_struct *wk){
    struct delayed_work *dwork = to_delayed_work(wk);
    struct splitter_work_struct * swork = container_of(dwork, struct splitter_work_struct, work);
    bcache_splitter_helper(wk);
    schedule_delayed_work(dwork, msecs_to_jiffies(swork->check_period));
}

static void btree_read_async(struct closure *cl)
{
	struct btree_op *op = container_of(cl, struct btree_op, cl);

	int ret = btree_root(search_recurse, op->c, op);

	if (ret == -EAGAIN)
		continue_at(cl, btree_read_async, bcache_wq);

	closure_return(cl);
}

/* Cached devices */
static void cached_dev_bio_complete(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);
	struct cached_dev *dc = container_of(s->d, struct cached_dev, disk);

	search_free(cl);
	cached_dev_put(dc);
    
}

/* Process reads */
static void cached_dev_read_complete(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);

	if (s->op.insert_collision)
		bch_mark_cache_miss_collision(s);

	if (s->op.cache_bio) {
		int i;
		struct bio_vec *bv;

		__bio_for_each_segment(bv, s->op.cache_bio, i, 0)
			__free_page(bv->bv_page);
	}

	cached_dev_bio_complete(cl);
}

static void request_read_error(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);
	struct bio_vec *bv;
	int i;

	if (s->recoverable) {
		/* Retry from the backing device: */
		trace_bcache_read_retry(s->orig_bio);

		s->error = 0;
		bv = s->bio.bio.bi_io_vec;
		do_bio_hook(s);
		s->bio.bio.bi_io_vec = bv;

		if (!s->unaligned_bvec)
			bio_for_each_segment(bv, s->orig_bio, i)
				bv->bv_offset = 0, bv->bv_len = PAGE_SIZE;
		else
			memcpy(s->bio.bio.bi_io_vec,
			       bio_iovec(s->orig_bio),
			       sizeof(struct bio_vec) *
			       bio_segments(s->orig_bio));

		/* XXX: invalidate cache */

		closure_bio_submit(&s->bio.bio, &s->cl, s->d);
	}

	continue_at(cl, cached_dev_read_complete, NULL);
}

//Leo leo
extern int readfill;

static void request_read_done(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);
	struct cached_dev *dc = container_of(s->d, struct cached_dev, disk);

	/*
	 * s->cache_bio != NULL implies that we had a cache miss; cache_bio now
	 * contains data ready to be inserted into the cache.
	 *
	 * First, we copy the data we just read from cache_bio's bounce buffers
	 * to the buffers the original bio pointed to:
	 */

	if (s->op.cache_bio) {
		bio_reset(s->op.cache_bio);
		s->op.cache_bio->bi_sector	= s->cache_miss->bi_sector;
		s->op.cache_bio->bi_bdev	= s->cache_miss->bi_bdev;
		s->op.cache_bio->bi_size	= s->cache_bio_sectors << 9;
		bch_bio_map(s->op.cache_bio, NULL);

		bio_copy_data(s->cache_miss, s->op.cache_bio);

		bio_put(s->cache_miss);
		s->cache_miss = NULL;
	}

	if (verify(dc, &s->bio.bio) && s->recoverable)
		bch_data_verify(s);

    // we do this here because we want the data to be copied into     
    // original bio from the cache bio
    //Leo
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
    process_jadu_reads("request_read_done",s->org_bio);
#endif
    
	bio_complete(s);
    if(readfill){
        if (s->op.cache_bio &&
                !test_bit(CACHE_SET_STOPPING, &s->op.c->flags)) {
            s->op.type = BTREE_REPLACE;
            closure_call(&s->op.cl, bch_insert_data, NULL, cl);
        }
    }

	continue_at(cl, cached_dev_read_complete, NULL);
}

static void request_read_done_bh(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);
	struct cached_dev *dc = container_of(s->d, struct cached_dev, disk);

    //Leo : possibly incorrect stats when bio alloc fails for op.cache_bio in cacheddevmiss
	bch_mark_cache_accounting(s, !s->cache_miss, s->op.skip);
	trace_bcache_read(s->orig_bio, !s->cache_miss, s->op.skip);
    
    if(tracing_enabled)
        dprinte(1, "request read done sectors %lu size %d hit %d skip %d\n",s->orig_bio->bi_sector, bio_sectors(s->orig_bio), !s->cache_miss,s->op.skip);
    
	if (s->error){
		continue_at_nobarrier(cl, request_read_error, bcache_wq);
    }
	else if (s->op.cache_bio || verify(dc, &s->bio.bio)){
        //reads with atleast one cache miss are handled here
		continue_at_nobarrier(cl, request_read_done, bcache_wq);
    }
	else{
        //Leo
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
        process_jadu_reads("request_read_done_bh",s->org_bio);
#endif

/*
        {
            //some reads dont get cache_bio set up in cached_dev_cache_miss
            // due to should_split() true or more than one split bios missing in cache
            // so, try inserting them using the bio clone into the cache here
            // as if it were a write request 
    if(readfill){
            if (!test_bit(CACHE_SET_STOPPING, &s->op.c->flags)) {
                print_bio("newreadinsstart",s->org_bio);
                s->op.type = BTREE_REPLACE;
                s->op.cache_bio = s->org_bio;
                closure_call(&s->op.cl, bch_insert_data, NULL, cl);
                s->op.cache_bio = NULL;
                print_bio("newreadinsdone",s->org_bio);
            }
    }
        }
        */

		continue_at_nobarrier(cl, cached_dev_read_complete, NULL);
    }
}

static int cached_dev_cache_miss(struct btree *b, struct search *s,
				 struct bio *bio, unsigned sectors)
{
	int ret = 0;
	unsigned reada;
	struct cached_dev *dc = container_of(s->d, struct cached_dev, disk);
	struct bio *miss;
    char logdata[1000]; char* log = logdata;
        
	miss = bch_bio_split(bio, sectors, GFP_NOIO, s->d->bio_split);
	if (miss == bio)
		s->op.lookup_done = true;

    //Leo
    if(tracing_enabled)
        print_bio("cacheddevcachemiss",miss);

	miss->bi_end_io		= acc_request_endio;
	miss->bi_private	= &s->cl;
    
    log += sprintf(log,"1");

	if (s->cache_miss || s->op.skip)
		goto out_submit;
    
    log += sprintf(log,"2");


	if (miss != bio ||
	    (bio->bi_rw & REQ_RAHEAD) ||
	    (bio->bi_rw & REQ_META) ||
	    s->op.c->gc_stats.in_use >= CUTOFF_CACHE_READA)
		reada = 0;
	else {
		reada = min(dc->readahead >> 9,
			    sectors - bio_sectors(miss));

		if (bio_end_sector(miss) + reada > bdev_sectors(miss->bi_bdev))
			reada = bdev_sectors(miss->bi_bdev) -
				bio_end_sector(miss);
	}

	s->cache_bio_sectors = bio_sectors(miss) + reada;
	s->op.cache_bio = bio_alloc_bioset(GFP_NOWAIT,
			DIV_ROUND_UP(s->cache_bio_sectors, PAGE_SECTORS),
			dc->disk.bio_split);

	if (!s->op.cache_bio)
		goto out_submit;
    
    log += sprintf(log,"3");

	s->op.cache_bio->bi_sector	= miss->bi_sector;
	s->op.cache_bio->bi_bdev	= miss->bi_bdev;
	s->op.cache_bio->bi_size	= s->cache_bio_sectors << 9;

	s->op.cache_bio->bi_end_io	= acc_request_endio;
	s->op.cache_bio->bi_private	= &s->cl;

	/* btree_search_recurse()'s btree iterator is no good anymore */
	ret = -EINTR;
	if (!bch_btree_insert_check_key(b, &s->op, s->op.cache_bio))
		goto out_put;
    
    log += sprintf(log,"4");

	bch_bio_map(s->op.cache_bio, NULL);
	if (bio_alloc_pages(s->op.cache_bio, __GFP_NOWARN|GFP_NOIO))
		goto out_put;
    
    log += sprintf(log,"5");

	s->cache_miss = miss;
	bio_get(s->op.cache_bio);
    
    if(tracing_enabled)
        dprinte(1,"cacheddevcachemiss1 %p %d %p %d %s\n",s->cache_miss,s->op.skip,s->op.cache_bio,s->cache_bio_sectors,logdata);
#ifdef LOW
    {
        struct timeval now;
        do_gettimeofday(&now);
        s->op.cache_bio->bcache_issued = conv(now);
    }
#endif

	closure_bio_submit(s->op.cache_bio, &s->cl, s->d);

	return ret;
out_put:
	bio_put(s->op.cache_bio);
	s->op.cache_bio = NULL;
out_submit:
#ifdef LOW
    {
        struct timeval now;
        do_gettimeofday(&now);
        miss->bcache_issued = conv(now);
    }
#endif
	closure_bio_submit(miss, &s->cl, s->d);
    if(tracing_enabled)
        dprinte(1,"cacheddevcachemiss2 %p %d %p %d %s\n",s->cache_miss,s->op.skip,s->op.cache_bio,s->cache_bio_sectors,logdata);
	return ret;
}

static void request_read(struct cached_dev *dc, struct search *s)
{
	struct closure *cl = &s->cl;
    
    if(tracing_enabled)
        dprinte(1,"request read");

	check_should_skip(dc, s);
	closure_call(&s->op.cl, btree_read_async, NULL, cl);

	continue_at(cl, request_read_done_bh, NULL);
}

/* Process writes */

static void cached_dev_write_complete(struct closure *cl)
{
	struct search *s = container_of(cl, struct search, cl);
	struct cached_dev *dc = container_of(s->d, struct cached_dev, disk);

	up_read_non_owner(&dc->writeback_lock);
	cached_dev_bio_complete(cl);
}

static void request_write(struct cached_dev *dc, struct search *s)
{
	struct closure *cl = &s->cl;
	struct bio *bio = &s->bio.bio;
	struct bkey start, end;
	start = KEY(dc->disk.id, bio->bi_sector, 0);
	end = KEY(dc->disk.id, bio_end_sector(bio), 0);
    
    if(tracing_enabled)
        print_bio("request write",bio);

	bch_keybuf_check_overlapping(&s->op.c->moving_gc_keys, &start, &end);

	check_should_skip(dc, s);
	down_read_non_owner(&dc->writeback_lock);

	if (bch_keybuf_check_overlapping(&dc->writeback_keys, &start, &end)) {
		s->op.skip	= false;
		s->writeback	= true;
	}

	if (bio->bi_rw & REQ_DISCARD){
        if(tracing_enabled)
            dprintk(1,"skipping discard flag\n");
		goto skip;
    }
    
	if (should_writeback(dc, s->orig_bio,
			     cache_mode(dc, bio),
			     s->op.skip)) {
		s->op.skip = false;
		s->writeback = true;
	}
    
    if (s->op.skip){
        if(tracing_enabled)
            dprintk(1,"skipping op.skip\n");
        goto skip;
    }
   
	trace_bcache_write(s->orig_bio, s->writeback, s->op.skip);

	if (!s->writeback) {
		s->op.cache_bio = bio_clone_bioset(bio, GFP_NOIO,
						   dc->disk.bio_split);

#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
        //adjust for bcache super block
        bio->bi_sector -= bcache_sb_offset; 
        bio->isunique = is_unique(bio); 
        bio->bi_sector += bcache_sb_offset;
#endif
		
        closure_bio_submit(bio, cl, s->d);
	} else {
		bch_writeback_add(dc);
		s->op.cache_bio = bio;

		if (bio->bi_rw & REQ_FLUSH) {
			/* Also need to send a flush to the backing device */
			struct bio *flush = bio_alloc_bioset(GFP_NOIO, 0,
							     dc->disk.bio_split);

			flush->bi_rw	= WRITE_FLUSH;
			flush->bi_bdev	= bio->bi_bdev;
			flush->bi_end_io = request_endio;
			flush->bi_private = cl;

			closure_bio_submit(flush, cl, s->d);
		}
	}
out:
	closure_call(&s->op.cl, bch_insert_data, NULL, cl);
	continue_at(cl, cached_dev_write_complete, NULL);
skip:
    if(tracing_enabled)
        dprintk(1,"skipping write\n");
	s->op.skip = true;
	s->op.cache_bio = s->orig_bio;
	bio_get(s->op.cache_bio);

	if ((bio->bi_rw & REQ_DISCARD) &&
	    !blk_queue_discard(bdev_get_queue(dc->bdev)))
		goto out;

	closure_bio_submit(bio, cl, s->d);
	goto out;
}

static void request_nodata(struct cached_dev *dc, struct search *s)
{
	struct closure *cl = &s->cl;
	struct bio *bio = &s->bio.bio;

	if (bio->bi_rw & REQ_DISCARD) {
		request_write(dc, s);
		return;
	}

	if (s->op.flush_journal)
		bch_journal_meta(s->op.c, cl);

	closure_bio_submit(bio, cl, s->d);

	continue_at(cl, cached_dev_bio_complete, NULL);
}

/* Cached devices - read & write stuff */

unsigned bch_get_congested(struct cache_set *c)
{
	int i;
	long rand;

	if (!c->congested_read_threshold_us &&
	    !c->congested_write_threshold_us)
		return 0;

	i = (local_clock_us() - c->congested_last_us) / 1024;
	if (i < 0)
		return 0;

	i += atomic_read(&c->congested);
	if (i >= 0)
		return 0;

	i += CONGESTED_MAX;

	if (i > 0)
		i = fract_exp_two(i, 6);

	rand = get_random_int();
	i -= bitmap_weight(&rand, BITS_PER_LONG);

	return i > 0 ? i : 1;
}

static void add_sequential(struct task_struct *t)
{
	ewma_add(t->sequential_io_avg,
		 t->sequential_io, 8, 0);

	t->sequential_io = 0;
}

static struct hlist_head *iohash(struct cached_dev *dc, uint64_t k)
{
	return &dc->io_hash[hash_64(k, RECENT_IO_BITS)];
}

static void check_should_skip(struct cached_dev *dc, struct search *s)
{
	struct cache_set *c = s->op.c;
	struct bio *bio = &s->bio.bio;
	unsigned mode = cache_mode(dc, bio);
	unsigned sectors, congested = bch_get_congested(c);
    char logdata[1024], *log = logdata;
    
    if(tracing_enabled) log += sprintf(log, "congested %d detaching %d inuse %d > CUTOFF %d b %lu ",congested,atomic_read(&dc->disk.detaching),c->gc_stats.in_use,CUTOFF_CACHE_ADD, bio->bi_rw&REQ_DISCARD);

	if (atomic_read(&dc->disk.detaching) ||
	    (bio->bi_rw && c->gc_stats.in_use > CUTOFF_CACHE_ADD) ||
	    (bio->bi_rw & REQ_DISCARD))
		goto skip;

    if(tracing_enabled) log += sprintf(log,"1 ");

	if (mode == CACHE_MODE_NONE ||
	    (mode == CACHE_MODE_WRITEAROUND &&
	     (bio->bi_rw & REQ_WRITE)))
		goto skip;
    
    if(tracing_enabled) log += sprintf(log,"2 ");

	if (bio->bi_sector   & (c->sb.block_size - 1) ||
	    bio_sectors(bio) & (c->sb.block_size - 1)) {
		pr_debug("skipping unaligned io");
		goto skip;
	}
    
    if(tracing_enabled) log += sprintf(log,"3 ");

	if (!congested && !dc->sequential_cutoff)
		goto rescale;
    
    if(tracing_enabled) log += sprintf(log,"4 ");

	if (!congested &&
	    mode == CACHE_MODE_WRITEBACK &&
	    (bio->bi_rw & REQ_WRITE) &&
	    (bio->bi_rw & REQ_SYNC))
		goto rescale;
    
    if(tracing_enabled) log += sprintf(log,"5 ");

	if (dc->sequential_merge) {
		struct io *i;

		spin_lock(&dc->io_lock);

		hlist_for_each_entry(i, iohash(dc, bio->bi_sector), hash)
			if (i->last == bio->bi_sector &&
			    time_before(jiffies, i->jiffies))
				goto found;

		i = list_first_entry(&dc->io_lru, struct io, lru);

		add_sequential(s->task);
		i->sequential = 0;
found:
		if (i->sequential + bio->bi_size > i->sequential)
			i->sequential	+= bio->bi_size;

		i->last			 = bio_end_sector(bio);
		i->jiffies		 = jiffies + msecs_to_jiffies(5000);
		s->task->sequential_io	 = i->sequential;

		hlist_del(&i->hash);
		hlist_add_head(&i->hash, iohash(dc, i->last));
		list_move_tail(&i->lru, &dc->io_lru);

		spin_unlock(&dc->io_lock);
	} else {
		s->task->sequential_io = bio->bi_size;

		add_sequential(s->task);
	}
    
    if(tracing_enabled) log += sprintf(log,"6 ");

	sectors = max(s->task->sequential_io,
		      s->task->sequential_io_avg) >> 9;

	if (dc->sequential_cutoff &&
	    sectors >= dc->sequential_cutoff >> 9) {
		trace_bcache_bypass_sequential(s->orig_bio);
		goto skip;
	}
    
    if(tracing_enabled) log += sprintf(log,"7 ");

	if (congested && sectors >= congested) {
		trace_bcache_bypass_congested(s->orig_bio);
		goto skip;
	}
    
    if(tracing_enabled) log += sprintf(log,"8 ");

rescale:
	bch_rescale_priorities(c, bio_sectors(bio));
    if(tracing_enabled) log += sprintf(log,"9 ");
    if(tracing_enabled) dprinte(1,"%s\n",logdata);
	return;
skip:
	bch_mark_sectors_bypassed(s, bio_sectors(bio));
	s->op.skip = true;
    if(tracing_enabled) log += sprintf(log,"10 ");
    if(tracing_enabled) dprinte(1,"%s\n",logdata);
}

static void cached_dev_make_request(struct request_queue *q, struct bio *bio)
{
	struct search *s;
	struct bcache_device *d = bio->bi_bdev->bd_disk->private_data;
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	int cpu, rw = bio_data_dir(bio);
    bool skip = false;

	cpu = part_stat_lock();
	part_stat_inc(cpu, &d->disk->part0, ios[rw]);
	part_stat_add(cpu, &d->disk->part0, sectors[rw], bio_sectors(bio));
	part_stat_unlock();

	bio->bi_bdev = dc->bdev;
	bio->bi_sector += dc->sb.data_offset;
    
    //Leo
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
    if(unlikely(update_priority_d == NULL)){
        update_priority_d = d;
        update_delay_dc = dc;
        bcache_sb_offset = dc->sb.data_offset;
#if defined(DEDUP_WAR_PREFETCH) || defined(DEDUP_W_PREFETCH)
        dmdedup_bcache_sb_offset = dc->sb.data_offset;
#endif
    }
    
    if(tracing_enabled) print_bio("cacheddev make request",bio);

    diskdriver_account_bio(bio);
    {
        //int numzero;
        //numzero = process_jadu_writes("writes1", bio);
        process_jadu_writes("writes1", bio);
        //if(rw && (numzero == (bio_sectors(bio)))){
            //skip = true;
        //}
    }

    if(!skip && rw && should_ignore_cache(bio)){
        if(tracing_enabled)
            dprintk(1,"skipping due to ignore cache\n");
        skip = true;
    }
    
    if(tracing_enabled){
        if(skip){
            print_bio("cacheddev skipping",bio);
        }else{
            print_bio("cacheddev",bio);
        }
    }
#endif

	if (cached_dev_get(dc)) {
		s = search_alloc(bio, d);
        //Leo
        s->op.skip = skip;
#ifdef LOW
		trace_bcache_request_start(s->d, bio);
#endif

		if (!bio_has_data(bio))
			request_nodata(dc, s);
		else if (rw)
			request_write(dc, s);
		else
			request_read(dc, s);
	} else {
		if ((bio->bi_rw & REQ_DISCARD) &&
		    !blk_queue_discard(bdev_get_queue(dc->bdev)))
			bio_endio(bio, 0);
		else
			bch_generic_make_request(bio, &d->bio_split_hook);
	}
}

static int cached_dev_ioctl(struct bcache_device *d, fmode_t mode,
			    unsigned int cmd, unsigned long arg)
{
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	return __blkdev_driver_ioctl(dc->bdev, mode, cmd, arg);
}

static int cached_dev_congested(void *data, int bits)
{
	struct bcache_device *d = data;
	struct cached_dev *dc = container_of(d, struct cached_dev, disk);
	struct request_queue *q = bdev_get_queue(dc->bdev);
	int ret = 0;

	if (bdi_congested(&q->backing_dev_info, bits))
		return 1;

	if (cached_dev_get(dc)) {
		unsigned i;
		struct cache *ca;

		for_each_cache(ca, d->c, i) {
			q = bdev_get_queue(ca->bdev);
			ret |= bdi_congested(&q->backing_dev_info, bits);
		}

		cached_dev_put(dc);
	}

	return ret;
}

void bch_cached_dev_request_init(struct cached_dev *dc)
{
	struct gendisk *g = dc->disk.disk;

	g->queue->make_request_fn		= cached_dev_make_request;
	g->queue->backing_dev_info.congested_fn = cached_dev_congested;
	dc->disk.cache_miss			= cached_dev_cache_miss;
	dc->disk.ioctl				= cached_dev_ioctl;
}

/* Flash backed devices */

static int flash_dev_cache_miss(struct btree *b, struct search *s,
				struct bio *bio, unsigned sectors)
{
	struct bio_vec *bv;
	int i;
    
    if(tracing_enabled)
        print_bio("flashdev cache miss",bio);

	/* Zero fill bio */

	bio_for_each_segment(bv, bio, i) {
		unsigned j = min(bv->bv_len >> 9, sectors);

		void *p = kmap(bv->bv_page);
		memset(p + bv->bv_offset, 0, j << 9);
		kunmap(bv->bv_page);

		sectors	-= j;
	}

	bio_advance(bio, min(sectors << 9, bio->bi_size));

	if (!bio->bi_size)
		s->op.lookup_done = true;

	return 0;
}

static void flash_dev_make_request(struct request_queue *q, struct bio *bio)
{
	struct search *s;
	struct closure *cl;
	struct bcache_device *d = bio->bi_bdev->bd_disk->private_data;
	int cpu, rw = bio_data_dir(bio);

	cpu = part_stat_lock();
	part_stat_inc(cpu, &d->disk->part0, ios[rw]);
	part_stat_add(cpu, &d->disk->part0, sectors[rw], bio_sectors(bio));
	part_stat_unlock();

	s = search_alloc(bio, d);
	cl = &s->cl;
	bio = &s->bio.bio;

    if(tracing_enabled)
        print_bio("flashdev",bio);

#ifdef LOW
	trace_bcache_request_start(s->d, bio);
#endif

	if (bio_has_data(bio) && !rw) {
		closure_call(&s->op.cl, btree_read_async, NULL, cl);
	} else if (bio_has_data(bio) || s->op.skip) {
		bch_keybuf_check_overlapping(&s->op.c->moving_gc_keys,
					&KEY(d->id, bio->bi_sector, 0),
					&KEY(d->id, bio_end_sector(bio), 0));

		s->writeback	= true;
		s->op.cache_bio	= bio;

		closure_call(&s->op.cl, bch_insert_data, NULL, cl);
	} else {
		/* No data - probably a cache flush */
		if (s->op.flush_journal)
			bch_journal_meta(s->op.c, cl);
	}

	continue_at(cl, search_free, NULL);
}

static int flash_dev_ioctl(struct bcache_device *d, fmode_t mode,
			   unsigned int cmd, unsigned long arg)
{
	return -ENOTTY;
}

static int flash_dev_congested(void *data, int bits)
{
	struct bcache_device *d = data;
	struct request_queue *q;
	struct cache *ca;
	unsigned i;
	int ret = 0;

	for_each_cache(ca, d->c, i) {
		q = bdev_get_queue(ca->bdev);
		ret |= bdi_congested(&q->backing_dev_info, bits);
	}

	return ret;
}

void bch_flash_dev_request_init(struct bcache_device *d)
{
	struct gendisk *g = d->disk;

	g->queue->make_request_fn		= flash_dev_make_request;
	g->queue->backing_dev_info.congested_fn = flash_dev_congested;
	d->cache_miss				= flash_dev_cache_miss;
	d->ioctl				= flash_dev_ioctl;
}

void bch_request_exit(void)
{
#ifdef CONFIG_CGROUP_BCACHE
	cgroup_unload_subsys(&bcache_subsys);
#endif
	if (bch_search_cache)
		kmem_cache_destroy(bch_search_cache);
}

//int __init bch_request_init(void)
int bch_request_init(void)
{
	bch_search_cache = KMEM_CACHE(search, 0);
	if (!bch_search_cache)
		return -ENOMEM;

#ifdef CONFIG_CGROUP_BCACHE
	cgroup_load_subsys(&bcache_subsys);
	init_bch_cgroup(&bcache_default_cgroup);

	cgroup_add_cftypes(&bcache_subsys, bch_files);
#endif
	return 0;
}
