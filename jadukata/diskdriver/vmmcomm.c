#include "includes.h"
#include "common.h"

#ifdef JADUKATA_VMM_COMM

struct vmmcomm_entry{
    unsigned long addr;
    bool flag;
};

#define JADUKATA_ARRAY_MAX (4000/sizeof(struct vmmcomm_entry))
#define VMMCOMM_MAGIC (0x987654)
#define VMMCOMM_NOTPRESENT (0x0)

#define MAXBUF (1000)
#define MAXBUFPRINT (900)

#ifdef HIGH
void* a_vmmcomm_base = NULL;
#endif

struct vmmcomm_entry* a_vmmcomm = NULL;
long *a_vmmcomm_magic = NULL, *a_vmmcomm_start = NULL, *a_vmmcomm_end = NULL, *a_vmmcomm_num = NULL, *a_vmmcomm_adds = NULL , *a_vmmcomm_dels = NULL;
int *a_vmmcomm_lock = NULL;

int vmmcomm_started = 0;
int vmmcomm_debug = 1;

#define A_VMMCOMM_HEADER_SIZE ((sizeof(long)*6) + sizeof(int))
#define A_VMMCOMM_SIZE ((sizeof(struct vmmcomm_entry)*JADUKATA_ARRAY_MAX) + A_VMMCOMM_HEADER_SIZE)

void vmmcomm_lock_init(int* lock){
    barrier();
    *lock = 0;
    smp_wmb();
    dprintk(1,"vmmcomm lock init done %lx\n",(unsigned long)lock);
}

void vmmcomm_lock(int* lock,unsigned long* flags){
    while(1){
        if(cmpxchg(lock,0,1)==0){
            //dprintk(1,"vmmcomm lock acquired %lx\n",(unsigned long)lock);
            return;
        }else{
            msleep(1000);
            //dprintk(1,"vmmcomm lock acquisition failed retrying %lx \n",(unsigned long)lock);
        }
    };
}

void vmmcomm_unlock(int* lock, unsigned long* flags){
    barrier();
    *lock = 0;
    smp_wmb();
    //dprintk(1,"vmmcomm lock released %lx\n",(unsigned long)lock);
}

int vmmcomm_print(char* msg){
    char *msgptr = msg;
    long start = *a_vmmcomm_start;
    long end = *a_vmmcomm_end;
    long index = start;
	msgptr += sprintf(msgptr, "vmmcomm adds %ld dels %ld diff %ld start %ld end %ld num %ld \n",*a_vmmcomm_adds, *a_vmmcomm_dels, (*a_vmmcomm_adds - *a_vmmcomm_dels), *a_vmmcomm_start, *a_vmmcomm_end, *a_vmmcomm_num);
    msgptr += sprintf(msgptr, "{");
    while(index<=end){
        struct vmmcomm_entry* e = &(a_vmmcomm[index%JADUKATA_ARRAY_MAX]);
        if(e->addr != VMMCOMM_NOTPRESENT){
            msgptr += sprintf(msgptr,"%lu-%d,",e->addr,e->flag);
        }else{
            msgptr += sprintf(msgptr,"N,");
        }
        index++;
        if((msgptr-msg)>MAXBUFPRINT){
            msgptr += sprintf(msgptr,"...");
            break;
        }
    }
    msgptr += sprintf(msgptr, "}");
    return (msgptr - msg);
}


void vmmcomm_init_from_base(unsigned long a_vmmcomm_base){
    char* base = (char*)a_vmmcomm_base;

    a_vmmcomm_lock = (int*)base;
    base += sizeof(int);

    a_vmmcomm_magic = (long*)base;
    base += sizeof(long);

    if(*a_vmmcomm_magic != VMMCOMM_MAGIC){
        dprinte(1,"ERROR vmmcomm magic incorrect %lx \n",*a_vmmcomm_magic);
    }else{
        dprintk(1,"correct vmmcomm magic %lx \n", *a_vmmcomm_magic);
    }

    a_vmmcomm_start = (long*)base;
    base += sizeof(long);

    a_vmmcomm_end = (long*)base;
    base += sizeof(long);

    a_vmmcomm_num = (long*)base;
    base += sizeof(long);

    a_vmmcomm_adds = (long*)base;
    base += sizeof(long);

    a_vmmcomm_dels = (long*)base;
    base += sizeof(long);
    
    a_vmmcomm = (struct vmmcomm_entry*)(base);

#ifdef HIGH
    vmmcomm_lock_init(a_vmmcomm_lock);  
#endif

}

void vmmcomm_unbase(void){
    a_vmmcomm_lock = NULL;
    a_vmmcomm_magic = NULL;
    a_vmmcomm_start = NULL;
    a_vmmcomm_end = NULL;
    a_vmmcomm_num = NULL;
    a_vmmcomm_adds = NULL;
    a_vmmcomm_dels = NULL;
    a_vmmcomm = NULL;
}

void reset_vmmcomm(void){
    unsigned long flags;
    dprintk(1, "cleared vmmcomm hashtable\n");
    vmmcomm_lock(a_vmmcomm_lock,&flags);
    *a_vmmcomm_adds = 0;
    *a_vmmcomm_dels = 0;
    *a_vmmcomm_start = 0;
    *a_vmmcomm_end = -1;
    *a_vmmcomm_num = 0;
    vmmcomm_unlock(a_vmmcomm_lock,&flags);
}

#ifdef HIGH
unsigned long vmmcomm_get_basephy(void){
    return (unsigned long)(a_vmmcomm_base);
}
#endif

#ifdef LOW
void vmmcomm_set_base(unsigned long base){
    dprintk(1, "setting vmmcomm base %lx \n",base);
    vmmcomm_init_from_base(base);
}
EXPORT_SYMBOL(vmmcomm_set_base);

void vmmcomm_unset_base(void){
    dprintk(1, "unsetting vmmcomm base \n");
    vmmcomm_unbase();
}
EXPORT_SYMBOL(vmmcomm_unset_base);

unsigned long vmmcomm_translate_fn = 0x0;
EXPORT_SYMBOL(vmmcomm_translate_fn);

#endif

void vmmcomm_start(void){
    dprintk(1, "starting vmmcomm\n");
    vmmcomm_started = 1;
}
EXPORT_SYMBOL(vmmcomm_start);

void vmmcomm_stop(void){
    dprintk(1, "stopping vmmcomm\n");
    vmmcomm_started = 0;
}
EXPORT_SYMBOL(vmmcomm_stop);

void vmmcomm_set_magic(void* base){
    long* basel = (long*)((char*)base + sizeof(int));
    *basel = VMMCOMM_MAGIC;
}

void vmmcomm_lock_test(void);

#ifdef HIGH
void vmmcomm_init(void){
    dprintk(1, "vmmcomm init in kernel size %d bytes of which header is %d bytes \n", A_VMMCOMM_SIZE, A_VMMCOMM_HEADER_SIZE);
    if(a_vmmcomm_base == NULL){
        a_vmmcomm_base = myd_kzalloc(A_VMMCOMM_SIZE,GFP_ATOMIC);
        if(a_vmmcomm_base != NULL){
            vmmcomm_set_magic(a_vmmcomm_base);
            vmmcomm_init_from_base((unsigned long)a_vmmcomm_base);
            reset_vmmcomm();
            dprintk(1, "vmmcomm base gva is %lx \n", a_vmmcomm_base);
        }else{
            dprintk(1,  "could not allocate memory for array data table \n");
        }
    }

    //vmmcomm_lock_test();
}

void vmmcomm_destroy(void){
    dprintk(1, "vmmcomm destroy in kernel");
    if(a_vmmcomm_base != NULL){
        myd_kfree(a_vmmcomm_base,A_VMMCOMM_SIZE);
        a_vmmcomm_base = NULL;
    }
}
#endif

const int reset_threshold = (INT_MAX-1000);

void vmmcomm_add(unsigned long value, bool converted){
    unsigned long flags;
    long index,size;
    char msg[MAXBUF];
    char *msgptr = msg;
    struct vmmcomm_entry *e = NULL;

    if(value == VMMCOMM_NOTPRESENT){
        return;
    }
    msgptr += sprintf(msgptr, "vmmcomm adding %lx converted %s \n", value, converted?"true":"false");
    vmmcomm_lock(a_vmmcomm_lock,&flags);
    index = (*a_vmmcomm_end)+1;
    size = (*a_vmmcomm_num)+1;
    *a_vmmcomm_end = index;
    *a_vmmcomm_num = size;
    *a_vmmcomm_adds = *a_vmmcomm_adds + 1;
    if(index >= reset_threshold){
        *a_vmmcomm_start = (*a_vmmcomm_start%JADUKATA_ARRAY_MAX);
        *a_vmmcomm_end = (*a_vmmcomm_start + size);
    }
    e = &(a_vmmcomm[index%JADUKATA_ARRAY_MAX]);
    e->addr = value;
    e->flag = converted;
    if(size > JADUKATA_ARRAY_MAX){
        msgptr += sprintf(msgptr, "ERROR array full start %ld end %ld size %ld\n", *a_vmmcomm_start, *a_vmmcomm_end, size);
    }

    if(vmmcomm_debug){
         msgptr += vmmcomm_print(msgptr);
    }
    vmmcomm_unlock(a_vmmcomm_lock,&flags);
    dprintk(1,"%s\n",msg);
}

int vmmcomm_remove(unsigned long value){
    long index;
    int ret = 0;
    unsigned long flags;
    char msg[MAXBUF];
    char *msgptr = msg;

    if(value == VMMCOMM_NOTPRESENT){
        return 0;
    }
    msgptr += sprintf(msgptr,"vmmcomm removing %lx \n", value);
    vmmcomm_lock(a_vmmcomm_lock,&flags);
    if(*a_vmmcomm_num <= 0){
        msgptr += sprintf(msgptr,"ERROR empty data start %ld end %ld size %ld\n", *a_vmmcomm_start, *a_vmmcomm_end, *a_vmmcomm_num);
    }else{
        index = *a_vmmcomm_start;
        while(index<=*a_vmmcomm_end){
            struct vmmcomm_entry *e = &(a_vmmcomm[index%JADUKATA_ARRAY_MAX]);
            if(!e->flag){
                e->addr = ((unsigned long(*)(unsigned long))vmmcomm_translate_fn)(e->addr);
                e->flag = true;
            }
            if(e->addr == value){
                e->addr = VMMCOMM_NOTPRESENT;
                e->flag = false;
                *a_vmmcomm_num = *a_vmmcomm_num - 1;
                *a_vmmcomm_dels = *a_vmmcomm_dels + 1;
                ret = 1;
                break;
            }
            index++;
        }
        //skip empty slots in beginning
        index = *a_vmmcomm_start;
        do{
            struct vmmcomm_entry *e = &(a_vmmcomm[index%JADUKATA_ARRAY_MAX]);
            if((e->addr == VMMCOMM_NOTPRESENT) && (index<=(*a_vmmcomm_end))){
                 index++;
            }else{
                break;
            }
        }while(true);
        *a_vmmcomm_start = index;
    }
    if(vmmcomm_debug){
        msgptr += vmmcomm_print(msgptr);
    }
    vmmcomm_unlock(a_vmmcomm_lock,&flags);
    msgptr += sprintf(msgptr,"ret value : %d \n", ret);
    dprintk(1,"%s\n",msg);
    return ret;
}

/*
int vmmcomm_lookup(unsigned long value){
    int ret = 0;
    long index,start,end;
    unsigned long flags;

    if(value == VMMCOMM_NOTPRESENT){
        return 0;
    }
    vmmcomm_lock(a_vmmcomm_lock,&flags);
    start = *a_vmmcomm_start;
    end = *a_vmmcomm_end;
    vmmcomm_unlock(a_vmmcomm_lock,&flags);

    index = start;
    while(index<=end){
        if(a_vmmcomm[index%JADUKATA_ARRAY_MAX] == value){
            ret = 1;
            break;
        }
        index++;
    }
    return ret;
}
*/

static void vmmcomm_lock_test_helper(struct work_struct *work){
    int i=0;
    int mode = smp_processor_id()%2 ;
    struct diskdriver_work_t *w = container_of(work,struct diskdriver_work_t,wk);
    for(i=0;i<100;i++){
        if(mode == 0){
            vmmcomm_add(0x1234 + i,true);
        }else{
            vmmcomm_remove(0x1234 + i);
        }
        msleep(1);
    }
    myd_kfree(w,sizeof(struct diskdriver_work_t));
} 
    
void vmmcomm_lock_test(){
    int i=0;
    for(i=0;i<2;i++){
        struct diskdriver_work_t *w = (struct diskdriver_work_t*)myd_kmalloc(sizeof(struct diskdriver_work_t),GFP_ATOMIC);
        if (w == NULL) {
            dpanic("ERROR: mem allocation failure for freeslot swork struct\n");
        }
        INIT_WORK(&w->wk,vmmcomm_lock_test_helper);
        schedule_work_on(i,&w->wk);
    }
}

#endif

