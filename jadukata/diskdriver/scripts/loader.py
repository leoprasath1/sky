#!/usr/bin/python -u

import os
import sys
import subprocess
import shlex
from time import sleep,time
import re
import sys
import datetime
import random
import shelve
import signal

if(len(sys.argv) < 2):
	print "usage loader emulated_disk_size ramcache_size diskcache_size zeroout diskdriver mkfs"
	sys.exit(1)

device="DISKDRIVER"
mode="664"
SRCDIR="/home/leoa/cerny/jadukata/diskdriver/"
DMDEDUPSRCDIR="/home/leoa/cerny/jadukata/dm-dedup/"
group="root"
size1=int(sys.argv[1])
args = " DISKDRIVER_SIZE=%d "%(size1)
size2 = 0
size3 = 0
zero = 1
dd = 1
mkfs = 1
dedup = 0;
bcache = 1;

if(len(sys.argv) >= 3):
    size2=int(sys.argv[2])
    args += " RCACHE_SIZE=%d "%(size2)
if(len(sys.argv) >= 4):
    size3=int(sys.argv[3])
    args += " DCACHE_SIZE=%d "%(size3)

if(len(sys.argv) >= 5):
    zero=int(sys.argv[4])

if(len(sys.argv) >= 6):
    dd=int(sys.argv[5])

if(len(sys.argv) >= 7):
    mkfs=int(sys.argv[6])

if(len(sys.argv) >= 8):
    dedup=int(sys.argv[7])

if(len(sys.argv) >= 9):
    dedupmeta=int(sys.argv[8])

if(len(sys.argv) >= 10):
    bcache=int(sys.argv[9])

print "loader.py zero %s dd %s mkfs %s dedup %s dedupmeta %s bcache %d args %s \n" % (zero, dd,mkfs,dedup,dedupmeta,bcache, args)

#subprocess.call("sudo modprobe netconsole",shell=True)
def getmyprint(stmt):
    return "%s : %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M-%S"),stmt)
    
def myprint(stmt):
    print "%s" % getmyprint(stmt)

def localexec(cmd,sudo=1,ignore=0, log=1):
    if sudo == 1:
        cmd = "sudo " + cmd
    #myprint("running command " + cmd)
    out = ""
    err = ""
    ret = 0
    t1 = t2 = 0
    try:
        t1 = t2 = time()
        out = subprocess.check_output(shlex.split(cmd), stderr=subprocess.STDOUT)
        t2 = time()
    except subprocess.CalledProcessError as e:
        ret = e.returncode
        out = e.output
        if (ignore==0):
            err = "ERROR RETURN CODE"
    if(log):
        myprint('cmd %s : %s' % (cmd,out))
    sys.stdout.flush()
    return out

def loader():
    localexec("rmmod %s"%(device))
    localexec("/sbin/insmod %s/%s.ko %s "%(SRCDIR,device,args))
    major = subprocess.check_output("grep '%s%d' /proc/devices | tr -s ' ' | cut -d' ' -f1"%(device,0),shell=True).strip()
    localexec("rm -f /dev/%s%d"%(device,0))
    localexec("mknod /dev/%s%d b %s 0"%(device,0,major))
    localexec("chgrp %s /dev/%s%d"%(group,device,0))
    localexec("chmod %s /dev/%s%d"%(mode,device,0))

def ioctl():
    return subprocess.call("sudo %s/scripts/ioctl diskdriver_marker echo"%(SRCDIR),shell=True)

def load_dd():
    retrycount=0
    loader()
    while True:
        retrycount+=1;
        if(retrycount > 1):
            break
        ret = ioctl()
        if (ret != 0):
            print "ioctl failed with return code %d.. reloading .." % (ret)
        else:
            break
        loader()

def dedup_pdev():
    config=localexec("grep 'pdev' /home/arulraj/pdev.h").strip()
    return config.find("253") != -1

#umount
subprocess.call("%s/scripts/bcachemount.py 0" %(SRCDIR),shell=True);

subprocess.call("%s/scripts/dmdedupmount.py 0" %(DMDEDUPSRCDIR),shell=True);

if(dedup):
    if dedup_pdev():
        subprocess.call("%s/scripts/dmdedupmount.py 1 %d " %(DMDEDUPSRCDIR,dedupmeta),shell=True);   
#        subprocess.call("%s/scripts/ioctl dmdedup_debug 1" %(DMDEDUPSRCDIR),shell=True);
    else:
        print "pdev.h does not seem set up for dedup but dedup arg passed\n"
        exit(1);

#subprocess.call("sudo modprobe netconsole",shell=True)
# invoke insmod with all arguments we got
# and use a pathname, as newer modutils don't look in . by default

if(dd):
    load_dd()

#localexec("/home/leoa/queuenoop.py")

#mount
#subprocess.call("sudo /home/arulraj/cerny/jadukata/diskdriver/scripts/ioctl trace",shell=True)
if(bcache):
    subprocess.call("%s/scripts/bcachemount.py 1 %s %s %s" %(SRCDIR,zero,dd,mkfs),shell=True);


