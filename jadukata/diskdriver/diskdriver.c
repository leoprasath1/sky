#include "diskdriver.h"
#include "common.h"
#include "../../../pdev.h"

#include "diskcache.h"
#include "vmmcomm.h"

int module_init = 0;

int diskdriver_major[DISKDRIVER_DEVS];       /* 0 - let the system assign the major no */
extern int kernel_diskdriver_major;
int cache_smart_mode = -1;
bool unique_hints = false;
int diskdriver_first_major;

/*This holds the diskdriver device related info*/
diskdriver_dev diskdriver_device;
struct block_device * diskdriver_blk_dev[DISKDRIVER_DEVS];
struct workqueue_struct *diskdriver_wkq = NULL;

/* Our request queue */
static struct request_queue *diskdriver_queue;
unsigned long DISKDRIVER_SIZE;
module_param(DISKDRIVER_SIZE,ulong,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(DISKDRIVER_SIZE,"fake DISKDRIVER size in SECTORS integer");

#ifdef LOW
unsigned long DCACHE_SIZE;
module_param(DCACHE_SIZE,ulong,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(DCACHE_SIZE,"external disk cache size in SECTORS integer");
unsigned long CACHE_DISK_OFFSET = 0;

unsigned long RCACHE_SIZE;
module_param(RCACHE_SIZE,ulong,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(RCACHE_SIZE,"ram cache size in SECTORS integer");
#endif

#ifdef REMUS 
//temporary remus simulation measurement
static void remus_measurements(struct work_struct *work);
struct delayed_work remus_work;
int remus_last_jiffies = 0;
int remus_avg_interval = 0;
int remus_num_intervals = 0;
int remus_interval = 0; //msecs
#endif

int readfill = 0;

int tracing_enabled = 0;
int dprint_enabled = 0;

#ifdef DISKDRIVER_KERN_ASSIST
extern int cdata_start;
extern dev_t cdata_bd_dev;
extern int cdata_log;
extern void print_cdata(void);
extern void clear_cdata(void);
extern void cdata_init(void);
extern void cdata_destroy(void);
#endif

#ifdef BCACHE
extern void bcache_exit(void);
extern int bcache_init(void);
#if defined(JADUKATA) && defined(JADUKATA_CHECKSUM) && defined(LOW)
extern int get_cache_distribution_bcache(char* ptr);
extern int get_cached_sectors_bcache(char* ptr);
extern int resize_cache_bcache(unsigned long);
#endif
#endif

#ifdef MEM_TRACE

int sci_memory_used_max = 0;
EXPORT_SYMBOL(sci_memory_used_max);
atomic_t sci_memory_used = ATOMIC_INIT(0);
EXPORT_SYMBOL(sci_memory_used);

static inline void sci_mem_alloc_update(int size){
    int val = atomic_add_return(size,&sci_memory_used);
    if(val > sci_memory_used_max){
        sci_memory_used_max = val;
    }
}

inline void* sci_mem_kalloc(int source, int version, size_t size, gfp_t flags){
    void* mem = (version == 1?kmalloc(size,flags):kzalloc(size,flags));
    if(mem!=NULL) sci_mem_alloc_update(size);
    return mem;
}
EXPORT_SYMBOL(sci_mem_kalloc);

inline void* sci_mem_valloc(int source, int version, unsigned long size){
    void* mem = (version == 2?vmalloc(size):vzalloc(size));
    if(mem!=NULL) sci_mem_alloc_update(size);
    return mem;
}
EXPORT_SYMBOL(sci_mem_valloc);

inline void sci_mem_kfree(int source ,int version, const void* ptr, unsigned long size){
    if(ptr != NULL) atomic_sub(size,&sci_memory_used);
    kfree(ptr);
}
EXPORT_SYMBOL(sci_mem_kfree);

inline void sci_mem_vfree(int source, int version, const void* ptr, unsigned long size){
    if(ptr != NULL) atomic_sub(size,&sci_memory_used);
    vfree(ptr);
}
EXPORT_SYMBOL(sci_mem_vfree);
#endif


static char ioctl_msg_buf[MAX_UBUF_SIZE];

int major2index(int major)
{
    int i;
    for(i=0; (diskdriver_major[i]!=major)&&(i<DISKDRIVER_DEVS); i++) ;
    return i;
}

int get_major_number(struct bio *diskdriver_bio)
{
    int majornumber =  (diskdriver_bio ? diskdriver_bio->bi_bdev ? diskdriver_bio->bi_bdev->bd_disk ? diskdriver_bio->bi_bdev->bd_disk->major : -1 : -1 : -1);
    if(majornumber < 0 ) {
        dprintk(1,"major number is negative \n");
    }
    return majornumber;
}

/* Open the device. Increment the usage count */
int diskdriver_open(struct block_device *bdev,fmode_t mode)
{
    int num = iminor(bdev->bd_inode);
    dprintk(0,"DISKDRIVER request to open minor number %d .. \n",num);
    if (num >= DISKDRIVER_DEVS) {
        return -ENODEV;
    }

    dprintk(0,"DISKDRIVER Opened .. \n");
    diskdriver_device.usage ++;
    return 0;
}

/*Close the device. Decrement the usage count.*/
void diskdriver_release(struct gendisk* gdisk,fmode_t mode)
{
    struct block_device *bd = bdget(disk_devt(gdisk));
    diskdriver_device.usage --;

    if (!diskdriver_device.usage) { 
        fsync_bdev(bd);
    }
}

int print_metadata_stats(char* ptr);
void print_cache_stats(char **msg);

unsigned long taskptrs[5];

long print_current_task_ptr(void* buf){
    unsigned long addr = (unsigned long)this_cpu_ptr(&current_task);
    unsigned long addr1 = *((unsigned long*)addr);
    int pid = ((struct task_struct*)addr1)->pid;
    dprintk(1,"%d print_current_task_ptr current_task %lx pid %d taskstr %lx \n",smp_processor_id(),(unsigned long)this_cpu_ptr(&current_task),pid,addr1);
    taskptrs[smp_processor_id()] = (unsigned long)this_cpu_ptr(&current_task);
    return sprintf((char*)buf," %lx ",(unsigned long)this_cpu_ptr(&current_task));
}

int diskdriver_ioctl(struct block_device *blkdev,fmode_t mode,
        unsigned int cmd,unsigned long arg)
{
    int ret = 0;
    unsigned long target_pid;
    struct task_struct *target_task = NULL;
    struct pid* tpid = NULL;
    char msg[1024];
    char *tmsg = msg;
    int cpu;
    register unsigned long current_stack_pointer asm("esp");
    struct thread_info * ti;
    struct task_struct * ts;
    int i;
#ifdef JADUKATA
    bool debugi = false;
    extern hash_table* ht_data_checksums_read;
    extern hash_table* ht_data_checksums_write;
    extern hash_table* ht_metadata_sectors;
    extern hash_table* ht_data_smallfile_sectors;
#ifdef DEDUP_WAR_PREFETCH
    extern hash_table* ht_dedup_war_prefetch_detect;
#endif
#endif


    dprinte(1,"ioctl issued cmd %d \n",cmd);
        
    //dprinte(1,"s_deva %p \n",(blkdev));
    //dprinte(1,"s_devb %p \n",(blkdev)->bd_disk);
    //dprinte(1,"s_devc %p \n",(blkdev)->bd_disk->queue);
    //dprinte(1,"s_devd %d \n",(blkdev)->bd_disk->queue->limits.logical_block_size);
    //dprinte(1,"s_deve %d \n",(blkdev)->bd_disk->queue->limits.physical_block_size);


    switch (cmd) {
/*
        case BLKSSZGET:
            *((unsigned long*)arg) = DISKDRIVER_SECSIZE;
            dprinte(1,"ioctl issued BLKSSZGET\n");
            break;
        case BLKPBSZGET:
            *((unsigned long*)arg) = DISKDRIVER_SECSIZE;
            dprinte(1,"ioctl issued BLKPBSZGET\n");
            break;
        case BLKGETSIZE64:
            *((unsigned long*)arg) = (DISKDRIVER_SIZE*DISKDRIVER_SECSIZE);
            dprinte(1,"ioctl issued BLKGETSIZE64\n");
            break;
*/
        case READ_FILL:
#if defined(SMARTCACHE) || defined(BCACHE)
            readfill = arg;
#endif
            break;
        case DISKDRIVER_TRACE:
            if((arg & POW2(4))){
                tracing_enabled = 1;
            }else{
                tracing_enabled = 0;
            }

            if((arg & POW2(5))){
                dprint_enabled = 1;
            }else{
                dprint_enabled = 0;
            }

#ifdef JADUKATA
            if((arg & POW2(6))){
                debugi = true;
                ht_set_debug(ht_data_checksums_read);
            }else{
                ht_unset_debug(ht_data_checksums_read);
            }
            
            if((arg & POW2(7))){
                debugi = true;
                ht_set_debug(ht_data_checksums_write);
            }else{
                ht_unset_debug(ht_data_checksums_write);
            }
            
            if((arg & POW2(8))){
                debugi = true;
                ht_set_debug(ht_metadata_sectors);
            }else{
                ht_unset_debug(ht_metadata_sectors);
            }
            
            if((arg & POW2(9))){
                debugi = true;
                ht_set_debug(ht_data_smallfile_sectors);
            }else{
                ht_unset_debug(ht_data_smallfile_sectors);
            }
            
            
            if((arg & POW2(11))){
                ht_set_debug(ht_data_checksums_read);
                ht_set_debug(ht_data_checksums_write);
                ht_set_debug(ht_metadata_sectors);
                ht_set_debug(ht_data_smallfile_sectors);
            }else{
                if(!debugi){
                    ht_unset_debug(ht_data_checksums_read);
                    ht_unset_debug(ht_data_checksums_write);
                    ht_unset_debug(ht_metadata_sectors);
                    ht_unset_debug(ht_data_smallfile_sectors);
                }
            }
            
#ifdef DEDUP_WAR_PREFETCH
            if((arg & POW2(12))){
                ht_set_debug(ht_dedup_war_prefetch_detect);
            }else{
                ht_unset_debug(ht_dedup_war_prefetch_detect);
            }
#endif            
#define getdebug(x) ((x==NULL)?-1:x->debug)
            dprinte(1,"debug flags ht_data_checksums_read %d ht_data_checksums_write %d ht_metadata_sectors %d ht_data_smallfile_sectors %d \n",getdebug(ht_data_checksums_read), getdebug(ht_data_checksums_write), getdebug(ht_metadata_sectors), getdebug(ht_data_smallfile_sectors));

#endif 
            
            
            dprinte(1,"ioctl issued arg %lu  DISKDRIVER_TRACE tracing_enabled = %d dprint_eanbled = %d\n",arg,tracing_enabled, dprint_enabled);
            break;
        case DISKDRIVER_GET_STATS:
            dprinte(1,"ioctl issued DISKDRIVER_GET_STATS\n");
            {
                char* kptr = (char*)myd_vmalloc(MAX_UBUF_SIZE);
                char* ptr = kptr;
                ptr += print_metadata_stats(ptr);
                ptr += print_stats(ptr);
                if (copy_to_user((void __user *) arg,kptr,MAX_UBUF_SIZE))
                    ret = -EFAULT;
                myd_vfree(kptr,MAX_UBUF_SIZE);
            }
            break;
        case DISKDRIVER_RESET_STATS:
            dprinte(1,"ioctl issued DISKDRIVER_RESET_STATS\n");
            reset_stats();
            break;
        case CHANGE_PHASE:
            dprinte(1,"ioctl issued CHANGE_PHASE\n");
            print_stats(ioctl_msg_buf);
            reset_stats();
            break;
        case START_DISKDRIVER:
#ifdef DISKDRIVER_KERN_ASSIST
            cdata_bd_dev = blkdev->bd_dev;
            cdata_start = 1;
            cdata_log = 1;
#endif
            dprinte(1,"ioctl issued START_DISKDRIVER\n");
            break;
        case STOP_DISKDRIVER:
#ifdef DISKDRIVER_KERN_ASSIST
            cdata_start = 0;
            cdata_log = 0;
#endif
            dprinte(1,"ioctl issued STOP_DISKDRIVER\n");
            break;
        case IGNORE_CACHE:
            dprinte(1,"ioctl issued IGNORE_CACHE %s\n",ioctl_msg_buf);
            if(copy_from_user(ioctl_msg_buf,__user (char*) arg,MAX_UBUF_SIZE)) {
                dprinte(1,"ERROR: could not copy ioctl arg from userspace");
                dpanic("ERROR: could not copy ioctl arg from userspace");
            }
            ignore_cache_handle(ioctl_msg_buf);
            break;
        case GET_CACHED_SECTORS:
            dprinte(1,"ioctl issued GET_CACHED_SECTORS\n");
            {
                char* kptr = (char*)myd_vmalloc(MAX_UBUF_SIZE);
                get_cached_sectors(kptr);
                if (copy_to_user((void __user *) arg,kptr,MAX_UBUF_SIZE))
                    ret = -EFAULT;
                myd_vfree(kptr,MAX_UBUF_SIZE);
            }
            break;
        case GET_CACHE_DISTRIBUTION:
            dprinte(1,"ioctl issued GET_CACHE_DISTRIBUTION\n");
            {
                char* kptr = (char*)myd_vmalloc(MAX_UBUF_SIZE);
                get_cache_distribution(kptr);
                if (copy_to_user((void __user *) arg,kptr,MAX_UBUF_SIZE))
                    ret = -EFAULT;
                myd_vfree(kptr,MAX_UBUF_SIZE);
            }
            break;
        case RESIZE_CACHE:
            dprinte(1,"resizing the cache to %lu percent \n", (unsigned long)arg);
            resize_cache((unsigned long)arg);
            break;
        case CACHE_SMARTDUMB:
            dprinte(1,"changing cache smartdumb mode to %d \n", (int)arg);
            cache_smart_mode = (int)arg;
            break;
        case DISKDRIVER_MARKER:
            if(copy_from_user(ioctl_msg_buf,__user (char*) arg,MAX_UBUF_SIZE)) {
                dprinte(1,"ERROR: could not copy ioctl arg from userspace\n");
                dpanic("ERROR: could not copy ioctl arg from userspace\n");
                ret = -1;
            }
            dprinte(1,"ioctl issued DISKDRIVER_MARKER %s\n",ioctl_msg_buf);
#ifdef SMARTCACHE
            print_metadata_stats();
            print_cache_stats(NULL);
#endif
            break;
        case DISKDRIVER_CACHE_RESET:
#ifdef SMARTCACHE
            print_metadata_stats();
            print_cache_stats(NULL);
#endif
            break;
#ifdef HIGH
#ifdef JADUKATA_VMM_COMM
        case GETVMMCOMMBASE:
            sprintf(tmsg,"%lx\n",vmmcomm_get_basephy());
            dprinte(1,"ioctl getvmmcommbase from userspace %s\n",msg);
            if (copy_to_user((void __user *) arg,msg,1024))
                ret = -EFAULT;
            break;
        case STARTVMMCOMM:
            dprinte(1,"ioctl startvmmcomm from userspace\n");
            vmmcomm_init();
            vmmcomm_start();
            break;
        case STOPVMMCOMM:
            dprinte(1,"ioctl startvmmcomm from userspace\n");
            vmmcomm_stop();
            vmmcomm_destroy();
            break;
#endif
#endif
        case GETCR3:
            if(copy_from_user(msg,(void __user*) arg,100)) {
                dprinte(1,"ERROR: could not copy ioctl arg from userspace");
                dpanic("ERROR: could not copy ioctl arg from userspace");
            }

            dprinte(1,"ioctl getcr3 info from userspace %s\n",msg);
            sscanf(msg,"%lu\n",&target_pid);
            tpid = find_get_pid(target_pid);
            if(tpid == NULL){
                dprinte(1,"could not find struct pid for pid %lu\n",target_pid);
                ret  = -1;
                break;
            }
            target_task = get_pid_task(tpid,PIDTYPE_PID);
            if(target_task == NULL){
                dprinte(1,"could not find target task for pid %lu\n",target_pid);
                ret = -1;
                break;
            }
            dprinte(1," target %lx pid %d commd %s pgd virt %lx phy %lx active_pgd virt %lx phy %lx \n",(unsigned long)target_task,target_task->pid,target_task->comm,target_task->mm !=0 ? (unsigned long)target_task->mm->pgd: 0,target_task->mm !=0 ? (unsigned long)__pa(target_task->mm->pgd): 0,target_task->active_mm !=0?(unsigned long)target_task->active_mm->pgd:0,target_task->active_mm !=0?(unsigned long)__pa(target_task->active_mm->pgd):0);

            tmsg += sprintf(tmsg,"%lx ",__pa(target_task->mm->pgd));

            tmsg += sprintf(tmsg,"\n");

            dprinte(1,"returning to user getcr3 %s \n",msg);

            if (copy_to_user((void __user *) arg,msg,1024))
                ret = -EFAULT;
            ret = 0;
            break;
        case UNIQUE_HINTS:
            unique_hints = (arg!=0);
            dprinte(1,"set unique hints to new value %d\n",unique_hints);
            break;
        case GETTASKPTR:
            dprinte(1,"gettaskptr ioctl\n");
            print_current_task_ptr(tmsg);
            for_each_online_cpu(cpu){
                tmsg += work_on_cpu(cpu,print_current_task_ptr,tmsg);
                //dprinte(1,"per cpu ptr cpu %d ptr %lx \n",cpu,per_cpu_ptr(current_task,cpu));
            }
            tmsg += sprintf(tmsg,"\n");
            dprinte(1,"returning to user gettaskptr %s \n",msg);

            if (copy_to_user((void __user *) arg,msg,1024))
                ret = -EFAULT;
            ret = 0;
            break;
#ifdef REMUS
        case REMUS_IOCTL:
            {
                int interval = (int) arg;
                remus_last_jiffies = 0;
                remus_avg_interval = 0;
                remus_num_intervals = 0;
                if(remus_interval){
                    cancel_delayed_work_sync(&remus_work);
                }
                if(interval){
                    dprinte(1,"setting remus with interval %d \n",interval);
                    INIT_DELAYED_WORK(&remus_work,remus_measurements);
                    schedule_delayed_work(&remus_work, msecs_to_jiffies(interval));
                }else{
                    dprinte(1,"disabling remus with interval %d \n",interval);
                }
                remus_interval = interval;
            }
            break;
#endif
        case TEST:
            {
                extern int leo_debug;
                leo_debug = (int)arg;
                dprinte(1,"setting leo_debug to %d \n", leo_debug);
            }
            break;
            ti = ((struct thread_info *)(current_stack_pointer & ~(THREAD_SIZE - 1)));
            ts = ti->task;
            dprinte(1,"test ioctl...");
            dprinte(1,"leo test esp %lx ti %lx ts %lx pid %d stack %lx %lu %lu %lu %lu\n",current_stack_pointer,(unsigned long)ti ,(unsigned long)ts,ts->pid,(unsigned long)ts->stack,offsetof(struct thread_info,task),offsetof(struct task_struct,pid),offsetof(struct task_struct,stack),offsetof(struct task_struct,comm));
            dprinte(1,"current process comm %s pid %d cr3 %lx \n",current->comm,current->pid,(unsigned long)current->mm->pgd);
            dprinte(1,"current pgd virt %lx phy %lx active_pgd virt %lx phy %lx \n",current->mm !=0 ? (unsigned long)current->mm->pgd: 0,current->mm !=0 ?(unsigned long)__pa(current->mm->pgd): 0,current->active_mm !=0?(unsigned long)current->active_mm->pgd:0,current->active_mm !=0?(unsigned long)__pa(current->active_mm->pgd):0);

            for(i=0;i<4;i++){
                unsigned long addr1 = *((unsigned long*)taskptrs[i]);
                int pid = ((struct task_struct*)addr1)->pid;
                char* comm = ((struct task_struct*)addr1)->comm;
                dprinte(1,"%d print_current_task_ptr taskptr %lx current_task %lx pid %d comm %s \n",smp_processor_id(),taskptrs[i],addr1,pid,comm);
            }

            dprinte(1,"returning to user test %s \n",msg);

            ret = 0;
            break;
        default:
            dprinte(1,"unknown ioctl %u issued\n",cmd);
            break;
    }
    return ret;
}

int diskdriver_media_changed(struct gendisk *gd)
{
    return 1;
}

int diskdriver_revalidate_disk(struct gendisk *gd)
{
    return 0;
}

struct block_device_operations diskdriver_bdops = {
    .owner = THIS_MODULE,
    .open = diskdriver_open,
    .release = diskdriver_release,
    .ioctl = diskdriver_ioctl,
    .media_changed = diskdriver_media_changed,
    .revalidate_disk = diskdriver_revalidate_disk
};

#if defined(JADUKATA) || defined (SMARTCACHE)
static struct kmem_cache *diskdriver_wk_cache = NULL;
#endif

extern atomic_t stat_totr,stat_totw;
extern atomic_t stat_bcache_totr,stat_bcache_totw;

extern atomic_t stat_journal_totr,stat_journal_totw;
extern atomic_t stat_nonjournal_totr,stat_nonjournal_totw;

#ifdef REMUS
//temporary measurement for remus workloads
extern atomic_t num_checkpoints;
extern atomic_t curr_journal_totr, curr_journal_totw;
extern atomic_t curr_nonjournal_totr, curr_nonjournal_totw;
extern atomic_t cum_mov_avg_journal_totr, cum_mov_avg_journal_totw;
extern atomic_t cum_mov_avg_nonjournal_totr, cum_mov_avg_nonjournal_totw;
#endif

#if defined(SMARTCACHE) || defined(BCACHE)
extern hash_table* ht_metadata_sectors;
extern hash_table* ht_data_smallfile_sectors;
avl_tree* at_ignore_cache = NULL;
#endif

#ifdef SMARTCACHE
int replace_slot = 0;
char* smartcache = NULL;
unsigned long *smartcache_header = NULL;
extern hash_table* ht_smartcache;
extern hash_table* ht_free_smartslots;
extern hash_table* ht_tofree_smartslots;
#ifdef TESTMODE
extern hash_table* ht_test_free_smartslots;
#endif
extern unsigned long SMARTCACHE_SIZE;
extern unsigned long smartcache_hits;
atomic_t cached_reads = ATOMIC_INIT(0);

static struct kmem_cache *smart_tracker_cache = NULL;
static struct kmem_cache *cache_helper_cache = NULL;

void print_cache_stats(char **msg){
    if(msg != NULL){
        char* output = *msg;
        output += sprintf(output,"cached reads %d \n",atomic_read(&cached_reads));
        *msg = output;
    }else{
        dprinte(1,"cached reads %d \n",atomic_read(&cached_reads));
    }
    atomic_set(&cached_reads,0);
}

#ifdef EXTCACHE
extern hash_table* ht_diskcache;
unsigned long* smartcache_dirty_bitmap = NULL;

int smartcache_set_dirty(int sectornr) {
    int ret;
    dprintk(0,"S %d %p\n ",sectornr,smartcache_dirty_bitmap);
#ifdef TESTMODE
    if(sectornr >= SMARTCACHE_SIZE){
        dprinte(1,"ERROR : beyond smartcache bitmap size %d \n",sectornr);
    }
#endif
    ret = test_and_set_bit(sectornr,smartcache_dirty_bitmap);
    /*
    if(unlikely(ret)) {
        dprinte(1,"ERROR: Trying to set an allocated bit %d for  bit %d \n",ret,sectornr);
        return -1;
    }
    */
    return 0;
}

int smartcache_unset_dirty(int sectornr) {
    int ret;
    dprintk(0, "S %d %p\n ", sectornr, smartcache_dirty_bitmap);
    if(sectornr >= SMARTCACHE_SIZE){
        dprinte(1,"ERROR : beyond smartcache bitmap size %d \n",sectornr);
    }
    ret = test_and_clear_bit(sectornr,smartcache_dirty_bitmap);
    /*
    if(unlikely(!ret)) {
        dprinte(1,"ERROR: Trying to unset an unallocated bit %d for  bit %d \n",ret,sectornr);
        return -1;
    }
    */
    return 0;
}

int smartcache_is_dirty(int sectornr) {
    if(sectornr >= SMARTCACHE_SIZE){
        dprinte(1,"ERROR : beyond smartcache bitmap size %d \n",sectornr);
        return 0;
    }
    return test_bit(sectornr,smartcache_dirty_bitmap);
}
#endif

struct cache_helper_t{
    unsigned long smartslot;
    unsigned status;
    struct bio* tbio;
    struct completion event;
};

struct smart_tracker {
    unsigned long sector;
    int size,dir;
    bio_end_io_t* end_io;
};

struct bio *alloc_bio_page(struct block_device *dev,int sector,bio_end_io_t end_io_func,int rw,struct page *page,unsigned offset,int size)
{
    struct bio *tbio = NULL;

    tbio = bio_alloc(GFP_ATOMIC,1);
    if (!tbio) {
        dpanic("ERROR: mem allocation\n");
        return NULL;
    }

    tbio->bi_io_vec[0].bv_page = page;
    tbio->bi_io_vec[0].bv_len = size;
    tbio->bi_io_vec[0].bv_offset = offset;
    tbio->bi_vcnt = 1;
    tbio->bi_idx = 0;
    tbio->bi_size = size;
    tbio->bi_bdev = dev;
    tbio->bi_sector = sector;
    tbio->bi_end_io = end_io_func;
    tbio->bi_rw = rw;

    BIO_BUG_ON(!tbio->bi_size);
    BIO_BUG_ON(!tbio->bi_io_vec);

    return tbio;
}

struct bio *alloc_bio_size(struct block_device *dev,int sector,bio_end_io_t end_io_func,int rw,int size){
    struct page *tpage = NULL;
    tpage = alloc_page(GFP_ATOMIC);
    if (!tpage || (size > PAGE_SIZE)) {
        dprinte(1,"ERROR: mem allocation tpage %p size %d \n",tpage,size);
        return NULL;
    }
    return alloc_bio_page(dev,sector,end_io_func,rw,tpage,0,size);
}

struct bio *alloc_bio_buffer(struct block_device *dev,int sector,bio_end_io_t end_io_func,int rw,char *buffer,int size){
    struct page *tpage;
    int offset;
    tpage = virt_to_page(buffer);
    offset = ((unsigned long)buffer & ~PAGE_MASK);
#ifdef TESTMODE
    if(page_address(tpage) != (buffer-offset)){
        dprinte(1,"ERROR: page incorrect from buffer \n");
        dprintk(1,"buffer %lx page %lx offset %x page->virt %lx \n",(unsigned long)buffer,(unsigned long)tpage,offset,(unsigned long)page_address(tpage));
    }
#endif
    return alloc_bio_page(dev,sector,end_io_func,rw,tpage,offset,size);
}

void free_smartslot(unsigned long slot){
    ht_add(ht_tofree_smartslots,slot);
}

#ifdef EXTCACHE
void cache_disk_write_end_io(struct bio *tbio,int error)
{
    int uptodate = bio_flagged(tbio,BIO_UPTODATE);
    unsigned long slot = (unsigned long)tbio->bi_private1;
   
    //sectors here reflect adjustment for hard disk partition
    unsigned long actual_sector = tbio->bi_sector - CACHE_DISK_OFFSET - tbio->bi_size;
    
    dprintk(1,"Success: cache_disk_write_end_io %lu %lu \n",actual_sector,slot);
    print_bio("Success: cache_disk_write_end_io",tbio);

    if (!uptodate) {
        dprintk(1," cache write not fully written %d uptodate %d sector %lu \n",tbio->bi_size,uptodate,actual_sector);
        diskdriver_cache_free(smartcache_header[slot],actual_sector);
    }else{
        //add to metadata_sectors about new faster cache disk location
        diskdriver_cache_confirm(smartcache_header[slot],actual_sector);
    }
    ht_remove(ht_smartcache,smartcache_header[slot]);
    smartcache_header[slot] = DISKDRIVER_SIZE + 1;
    ht_add_val(ht_free_smartslots,slot,jiffies); 
    smartcache_unset_dirty(slot);

    bio_put(tbio);

    return;
}

void cache_disk_read_end_io(struct bio *tbio,int error)
{
    int uptodate = bio_flagged(tbio,BIO_UPTODATE);
    struct cache_helper_t *metae = (struct cache_helper_t*)tbio->bi_private1;

    if (!uptodate) {
        dprinte(1,"ERROR: cache read not fully read %d uptodate %d\n",tbio->bi_size,uptodate);
        metae->status = 1;
    }else{
        metae->status = 0;
    }
    complete(&metae->event);
    dprintk(0,"Success: read sec %ld \n",tbio->bi_sector-CACHE_DISK_OFFSET-tbio->bi_size);

    bio_put(tbio);
    return;
}
#endif

static DEFINE_SPINLOCK(fh_lock);
//static DEFINE_SPINLOCK(sm_lock);

static void freeslots_helper(struct work_struct *work){
    int i,level;
    unsigned long slot;
    int start_slot;
    //unsigned long flags;

#ifdef EXTCACHE
    #define BATCH_SIZE (32)
    int j;
    struct bio *tbios[BATCH_SIZE];
#endif
    struct diskdriver_work_t *w = container_of(work,struct diskdriver_work_t,wk);
    unsigned long numslots = w->payload;
    
    //local irq save to prevent interrupts  
    spin_lock(&fh_lock);
    start_slot = replace_slot;
    replace_slot += numslots;
    if(replace_slot > 300000){
        replace_slot %= SMARTCACHE_SIZE;
    }
    spin_unlock(&fh_lock);

    for(i=0;i<numslots;i++){
        free_smartslot((start_slot+i)%SMARTCACHE_SIZE);
    }

    dprintk(1," starting freeslots_helper\n");
    
    //actual bouncing to disk cache and free
    //to keep this logic simple,we just send 
    // sector size i/os one after another
    // and let the scheduler merge them
   
    //if there are too many to free,then skip writing non-dirty read in-mem cache entries to disk 
    level = 0;
    if(ht_get_size(ht_tofree_smartslots) > 64){
        level = 1;
        if(ht_get_size(ht_tofree_smartslots) > 128){
            level = 2;
        }
    }
   
    i=0; 
    while( ht_pop(ht_tofree_smartslots,&slot) != -1 ){
#ifdef EXTCACHE
        tbios[i] = NULL;
#endif
        //skip already free entries ( eg. bootstrap case )
        if(smartcache_header[slot] == (DISKDRIVER_SIZE +1)){
#ifdef TESTMODE
            if(ht_lookup(ht_smartcache,smartcache_header[slot]) != 0){
                dprinte(1,"ERROR : inconsistent free state in ht_smartcache %lu \n",slot);
            }
#ifdef EXTCACHE
            if(smartcache_is_dirty(slot)){
                dprinte(1,"ERROR : inconsistent free state but dirty %lu \n",slot);
            }
#endif
#endif
            if(ht_update_val(ht_free_smartslots,slot,jiffies) != STATUS_DUPL_ENTRY){
#ifdef TESTMODE
                if(ht_update_val(ht_test_free_smartslots,slot,jiffies) == STATUS_DUPL_ENTRY){
                    dprinte(1,"ERROR: already free slot not found in ht_free_smartslots %lu \n",slot);
                }
#endif
            }
            continue;
        }
#ifdef EXTCACHE
        //if slot is not dirty and does not have just free it
        if(level>0 && !smartcache_is_dirty(slot)){
            unsigned long cache_sector;
            if((level==2) || (diskdriver_cache_lookup(smartcache_header[slot],&cache_sector) == 0)){
                ht_remove(ht_smartcache,smartcache_header[slot]);
                smartcache_header[slot] = DISKDRIVER_SIZE + 1;
                ht_add_val(ht_free_smartslots,slot,jiffies); 
                continue;
            }
        }

        tbios[i] = alloc_bio_buffer(diskdriver_device.c_dev,smartcache_header[slot],cache_disk_write_end_io,WRITE,(char*)(smartcache+(slot*512)),512);

        if(tbios[i] == NULL){
            dprinte(1,"ERROR: could not allocate disk cache space\n");
            break;
        }

        if(diskdriver_cache_write(tbios[i]) == -1){
            dprinte(1,"ERROR: could not allocate disk cache space\n");
            bio_put(tbios[i]);
            tbios[i] = NULL;
            break;
        }

        tbios[i]->bi_private1 = (void*)slot;
        i++;
        if(i>=BATCH_SIZE){
            for(j=0;j<i;j++){
                if(tbios[j] != NULL){
                    print_bio("diskcache-bio1",tbios[j]);
                    generic_make_request(tbios[j]);
                }
            }
            i=0;
        }
#else
        if(slot >= SMARTCACHE_SIZE){
            dprinte(1,"ERROR slot too big\n");
        }
        ht_remove(ht_smartcache,smartcache_header[slot]);
        smartcache_header[slot] = DISKDRIVER_SIZE + 1;
        ht_add_val(ht_free_smartslots,slot,jiffies); 
#endif
    }

#ifdef EXTCACHE
    for(j=0;j<i;j++){
        if(tbios[j] != NULL){
            print_bio("diskcache-bio2",tbios[j]);
            generic_make_request(tbios[j]);
        }
    }
#endif
    //local_irq_restore(flags);
    dprintk(1," ending freeslots_helper\n");
    kmem_cache_free(diskdriver_wk_cache,w);
}

void free_cache_space(void){
    struct diskdriver_work_t *w = (struct diskdriver_work_t*)kmem_cache_alloc(diskdriver_wk_cache,GFP_ATOMIC);
    if (w == NULL) {
        dpanic("ERROR: mem allocation failure for freeslot swork struct\n");
    }
    INIT_WORK(&w->wk,freeslots_helper);
    w->payload = 512;
    queue_work(diskdriver_wkq,&w->wk);
}

//can fail to allocate smartslot
unsigned long alloc_smartslot(unsigned long sector){
   //unsigned long flags;
   unsigned long freeslot = SMARTCACHE_SIZE + 1;

   //local_irq_save(flags);
   //spin_lock_irqsave(&sm_lock,flags);
   
   if(ht_pop(ht_free_smartslots,&freeslot) != 0){
       //we failed to find a free slot
       /*
       unsigned long count=0;
       dprinte(1,"ERROR: searching for slots\n");
       while(count < SMARTCACHE_SIZE){
           freeslot = (count++)%SMARTCACHE_SIZE;
           if(smartcache_header[freeslot] > DISKDRIVER_SIZE){
#ifdef EXTCACHE
               if(!smartcache_is_dirty(freeslot)) break;
#else
               break;
#endif
           }
       }
       dprinte(1,"ERROR: searching for slots ended freeslot %lu count %lu \n",freeslot,count);
       if(count >= SMARTCACHE_SIZE){
       */
           dprintk(1,"ERROR: no freeslots in alloc smartslot\n");
           free_cache_space();
           /*
           freeslot = (SMARTCACHE_SIZE+1);
            * old code to sleep until free slots are available
           while(ht_get_size(ht_free_smartslots) == 0){
               dprinte(1,"ERROR: no freeslots sleeping\n");
               ssleep(2);
           }
           ht_open_scan(ht_free_smartslots);
           if( ht_pop(ht_free_smartslots,&freeslot) == -1 ){
               dprinte(1,"ERROR: no freeslots should not happen\n");
           }
           */
       //}
   }

   if(freeslot < SMARTCACHE_SIZE){
       smartcache_header[freeslot] = sector;
       ht_add_val(ht_smartcache,sector,freeslot);
   }
   if(ht_get_size(ht_free_smartslots) < 128){
       free_cache_space();
   }

   //local_irq_restore(flags);
   //spin_unlock_irqrestore(&sm_lock,flags);

   return freeslot;
}
#endif

int jadukata_process_bio(char*,struct bio *);

#ifdef SMARTCACHE

#ifdef TESTMODE

/*This method is called once the request is over.*/
void completion_event_end_io(struct bio *tbio,int error)
{
    int uptodate = bio_flagged(tbio,BIO_UPTODATE);
    dprintk(1," completion_even_end_io called %lu \n",tbio->bi_sector);
    if(tbio->bi_private1 != NULL){
        struct completion *event = (struct completion *)(tbio->bi_private1);

        if (!uptodate){
            dprinte(1,"ERROR: Read sec %ld\n",tbio->bi_sector);
        }else{
            dprintk(0,"Success: Read sec %ld\n",tbio->bi_sector);
        }

        complete_all(event);
    }
    return;
}

static void read_block_helper(struct work_struct *work){
    struct diskdriver_work_t * w = container_of(work,struct diskdriver_work_t,wk);
    struct bio* tbio = (struct bio*)w->payload;
    dprintk(1,"About to read sector %lu for testing cache\n",tbio->bi_sector);
    generic_make_request(tbio);
    //blk_run_queue(dev_get_queue(tbio->bi_bdev));
}

//NOTE : only one make_request function is active at a time
// since we need the data while processing make request,we 
// use a kernel delayed work task to do the actual io
// and we just wait for completion
char *read_block(int sector)
{
    char *ret = NULL;
    int* retint;
    struct bio *tbio = NULL;
    struct diskdriver_work_t wst;
    struct diskdriver_work_t *w = &wst;
    //this is used to wait for the completion of the io
    DECLARE_COMPLETION_ONSTACK(eventst);
    struct completion *event = &eventst;

    dprintk(1,"Reading sector %d for testing cache \n",sector);

    INIT_WORK_ONSTACK(&w->wk,read_block_helper);

    if (!(tbio = alloc_bio_size(diskdriver_device.f_dev,sector,completion_event_end_io,READ_SYNC,4096))){
        return NULL;
    }
    tbio->bi_private1 = event;
    ret = bio_data(tbio);

    w->payload = (unsigned long)tbio;
    queue_work(diskdriver_wkq,&w->wk);

    if(wait_for_completion_interruptible_timeout(event,msecs_to_jiffies(1000)) <= 0){
        free_page((unsigned long)ret);
        ret = NULL;
    }else{
        retint = (int*)ret;
        tbio->bi_private1 = NULL;
        if (!bio_flagged(tbio,BIO_UPTODATE)) {
            dprinte(1,"ERROR: IO error reading sector %d uptodate %d \n",sector,test_bit(BIO_UPTODATE,&tbio->bi_flags));
            free_page((unsigned long)ret);
            ret = NULL;
        }
    }
    bio_put(tbio);

    return ret;
}
#endif

void cache_end_helper(struct bio *diskdriver_bio)
{
    char jadumsg[1000];
    char* jmsg = jadumsg;
    struct smart_tracker* t = (struct smart_tracker*) diskdriver_bio->bi_private1;
    if(t->dir == READ){
        jmsg += sprintf(jmsg,"endre {");
        if(readfill){
            if( t->size<= 32){
                int i;
                unsigned long smartslot;
                bool should_process = false;

                if(cache_smart_mode >=0){
                    for(i=SMALLFILE_QUOTIENT;i<t->size;i+=SMALLFILE_DIVIDER){
                        if( (ht_lookup(ht_metadata_sectors,t->sector+i) != 0) || (ht_lookup(ht_data_smallfile_sectors,t->sector+i) != 0) ){
                            should_process = true;
                            break;
                        }
                    }
                }else{
                    should_process = true;
                }
                if(should_process){
                    // check the cache
                    for(i=0;i<t->size;i++){
                        jmsg += sprintf(jmsg, "%lu ",t->sector+i);
                        smartslot = SMARTCACHE_SIZE + 2;
                        ht_lookup_val(ht_smartcache,t->sector+i,&(smartslot));
                        if(smartslot >= SMARTCACHE_SIZE){
                            smartslot = alloc_smartslot(t->sector+i);
                            jmsg += sprintf(jmsg, "a");
                            if(smartslot >= SMARTCACHE_SIZE){
                                jmsg += sprintf(jmsg, "f");
                            }
                        }
                        jmsg += sprintf(jmsg, "%lu,",smartslot);
                        if(smartslot < SMARTCACHE_SIZE){
                            memcpy((void*)(smartcache + (smartslot*512)),(void*)(((char*)bio_data(diskdriver_bio))+(512*i)),512);
                        }
                    }
                }
            }
        }
        jmsg += sprintf(jmsg,"}");
        print_bio(jadumsg,diskdriver_bio);
    }
}
#endif

#ifdef REMUS
//simulates checkpoints for temporary remus measurements
static void remus_measurements(struct work_struct *work){

    int now = get_jiffies();
    remus_num_intervals++;
    if(remus_last_jiffies){
        int interval = now - remus_last_jiffies;
        remus_avg_interval = remus_avg_interval + ((interval - remus_avg_interval)/remus_num_intervals);       
    }
    remus_last_jiffies = now;

    {
        int numchk = atomic_add_return(1,&num_checkpoints);

        int ctotr = atomic_xchg(&curr_journal_totr,0);
        int ctotw = atomic_xchg(&curr_journal_totw,0);
        int cntotr =  atomic_xchg(&curr_nonjournal_totr,0);
        int cntotw = atomic_xchg(&curr_nonjournal_totw,0);

        int ajtotr = atomic_read(&cum_mov_avg_journal_totr);
        int ajtotw = atomic_read(&cum_mov_avg_journal_totw);
        int anjtotr = atomic_read(&cum_mov_avg_nonjournal_totr);
        int anjtotw = atomic_read(&cum_mov_avg_nonjournal_totw);

        atomic_add(ctotr,&stat_journal_totr);
        atomic_add(ctotw,&stat_journal_totw);
        atomic_add(cntotr,&stat_nonjournal_totr);
        atomic_add(cntotw,&stat_nonjournal_totw);

        ajtotr = ajtotr + ((ctotr - ajtotr)/numchk);
        ajtotw = ajtotw + ((ctotw - ajtotw)/numchk);
        anjtotr = anjtotr + ((cntotr - anjtotr)/numchk);
        anjtotw = anjtotw + ((cntotw - anjtotw)/numchk);

        atomic_set(&cum_mov_avg_journal_totr,ajtotr);
        atomic_set(&cum_mov_avg_journal_totw,ajtotw);
        atomic_set(&cum_mov_avg_nonjournal_totr,anjtotr);
        atomic_set(&cum_mov_avg_nonjournal_totw,anjtotw);

    }
    schedule_delayed_work(&remus_work, msecs_to_jiffies(remus_interval));

}
#endif

#if defined(SMARTCACHE) || defined(BCACHE)
int should_ignore_cache(struct bio* bio);
#endif
        
void  diskdriver_account_bio(struct bio* bio){
#ifdef REMUS
    int journal = should_ignore_cache(bio);
#endif

    if(bio_data_dir(bio) == WRITE){
        atomic_add(bio_sectors(bio),&stat_bcache_totw);
#ifdef REMUS
        if(journal){
            atomic_add(bio_sectors(bio),&curr_journal_totw);
        }else{
            atomic_add(bio_sectors(bio),&curr_nonjournal_totw);
        }
#endif
    }else{
        atomic_add(bio_sectors(bio),&stat_bcache_totr);
#ifdef REMUS
        if(journal){
            atomic_add(bio_sectors(bio),&curr_journal_totr);
        }else{
            atomic_add(bio_sectors(bio),&curr_nonjournal_totr);
        }
#endif
    }
}

int process_jadu_writes(char* src, struct bio* bio){
#if defined(JADUKATA) && defined(LOW)
    if(tracing_enabled) dprintk(1,"diskdriver %s-%s pid %d cmd %s sector %lu sectors %d\n", "jaduwrite", src , current->pid, current->comm, bio->bi_sector, bio_sectors(bio));
    if(cache_smart_mode>=0){
        if(bio_data_dir(bio) == WRITE){
            int ret = jadukata_process_bio("jaduwrite",bio);
            dprintk(1,"diskdriver %s-%s pid %d cmd %s sector %lu sectors %d\n", "jaduwritefinish", src , current->pid, current->comm, bio->bi_sector, bio_sectors(bio));
            return ret;
        }
    }
#endif
    return 0;
}

int process_jadu_reads(char* src, struct bio* bio){
#if defined(JADUKATA) && defined(LOW)
    if(cache_smart_mode>=0){
        if(bio_data_dir(bio) == READ){
            struct bio_vec *bv;
            int nbytes;
            int ret, old_idx, new_idx;
            old_idx = bio->bi_idx;

            //find the right idx based on size
            nbytes = bio->bi_size;
            for (bv = bio_iovec_idx((bio), (bio->bi_vcnt-1)), new_idx = bio->bi_vcnt-1;  new_idx>0 ; bv--, new_idx--){
                nbytes -= bv->bv_len;
                if(nbytes <= 0){
                    break;
                }
            }

            bio->bi_idx = new_idx;
            ret = jadukata_process_bio("jaduread1",bio);
            bio->bi_idx = old_idx;
            dprintk(1,"diskdriver %s-%s pid %d cmd %s sector %lu sectors %d bio->bi_idx %d bio->bi_vcnt %d bio->bi_size %d \n", "jadureadfinish", src, current->pid, current->comm, bio->bi_sector, bio_sectors(bio), bio->bi_idx, bio->bi_vcnt, bio->bi_size);
            return ret;
        }
    }
#endif
    return 0;
}

void get_cached_sectors(char* ptr){
#if defined(JADUKATA) && defined(JADUKATA_CHECKSUM) && defined(LOW)
#if defined(SMARTCACHE) || defined(BCACHE)
#if defined(BCACHE)
    get_cached_sectors_bcache(ptr);
#endif
#endif
#endif
}

void get_cache_distribution(char* ptr){
#if defined(JADUKATA) && defined(JADUKATA_CHECKSUM) && defined(LOW)
#if defined(SMARTCACHE) || defined(BCACHE)
#if defined(BCACHE)
    get_cache_distribution_bcache(ptr);
#endif
#endif
#endif
}

void resize_cache(unsigned long percent){
#if defined(JADUKATA) && defined(JADUKATA_CHECKSUM) && defined(LOW)
#if defined(SMARTCACHE) || defined(BCACHE)
#if defined(BCACHE)
    resize_cache_bcache(percent);
#endif
#endif
#endif
}

//int counter = 0;
void ignore_cache_handle(char* buf){
#if defined(JADUKATA) && defined(JADUKATA_CHECKSUM) && defined(LOW)
#if defined(SMARTCACHE) || defined(BCACHE)
    char* ptr = buf;
    int s, e;
    dprinte(1,"ignore cache handle \n");
    at_removeall(at_ignore_cache);
    while(ptr){
        sscanf(ptr,"%d-%d",&s,&e);
        //sectors
        dprinte(1,"adding interval %d:%d \n",s*8, ((e-s)*8)+1);
        at_add_val(at_ignore_cache,s*8,((e-s)*8)+1);
        ptr = strchr(ptr,',');
        if(ptr){
            ptr++;
        }
    }
#endif
#endif
}

#if defined(SMARTCACHE) || defined(BCACHE)
extern unsigned bcache_sb_offset;
int should_ignore_cache(struct bio* bio){
    bool ret = false;
    unsigned long key,val,sector;
    sector = bio->bi_sector;
    //adjust for bcache super block
    sector -= bcache_sb_offset; 
    if(at_ignore_cache != NULL){
        if(at_lookup_val(at_ignore_cache,sector,&val)) {
            ret = true;
        }else{
            if(at_find_closest_before(at_ignore_cache,sector,&key,&val) > 0) {
                if((key + val) > sector ) {
                    ret = true;
                }
            }
        }
    }
    return ret;
}

bool is_unique(struct bio* bio){
   bool ret = false;
   if(unique_hints){
       int s = 0;
       //for(s = 0; (s<bio_sectors(bio)) && !ret; s++){
           ret = ht_lookup(ht_metadata_sectors,bio->bi_sector+s);
           //dprintk(1,"is_unique sector %lu val %d\n",bio->bi_sector,ret);
       //}
       /*
          counter++;
          if(bio->bi_sector < 120000 && counter%2==0 ) {
          ret = true;
          }
          */
       //print_bio(ret?"meta true":"meta false",bio);

       //also classify all one number ioclass data as unique for strict app priority
#if defined(DEDUP_APP_UNIQUE) || defined(DEDUP_U_HINT) //app supplied unique hints using the ioclass 1
       if(!ret){
           unsigned long val;
           if(ht_lookup_val(ht_data_smallfile_sectors,bio->bi_sector+s,&val) != 0){
               ret = (pack_info_get_z(val) & DEDUP_U_IOCLASS_MASK);
           }
       }
#endif
   }
   return ret;
}

bool is_metadata(struct bio* bio){
   bool ret = false;
   int s = 0;
   //for(s = 0; s <bio_sectors(bio); s++){
       //return unlikely(ht_lookup(ht_metadata_sectors,bio->bi_sector));
       ret |= (ht_remove(ht_metadata_sectors,bio->bi_sector+s) > 0);
   //}
   /*
   counter++;
   if(bio->bi_sector < 120000 && counter%2==0 ) {
       ret = true;
   }
   */
   //print_bio(ret?"meta true":"meta false",bio);
   return ret;
}

//return 1 to 4 with 4 as highest priority
char is_smallfile(struct bio* bio){
   short ret = 0;
   char ioclass;
   unsigned long val = 0;
   int s = 0;
   //for(s = SMALLFILE_QUOTIENT; s<bio_sectors(bio); s+=SMALLFILE_DIVIDER){
       //return unlikely(ht_lookup(ht_data_smallfile_sectors,bio->bi_sector));
       ht_remove_val(ht_data_smallfile_sectors,bio->bi_sector+s,&val);
       ioclass = pack_info_get_z(val);
       if(ioclass>ret){
           ret = ioclass;
       }
   //}
   /*
   if(bio->bi_sector < 120000) {
       ret = true;
   }
   */
   //print_bio(ret?"small true":"small false",bio);
   if(ret <0 || ret >SMALLFILE_IOCLASS_MAX || (ret == METADATA_IOCLASS && METADATA_IOCLASS!=0)){
#ifndef DEDUP_U_HINT
       dprinte(1, "ERROR incorrect small file class %u \n " , ret);
#endif
       ret = 0;
   }
   return ret;
}
#endif

#if defined(JADUKATA) || defined(SMARTCACHE)
void diskdriver_end_io_helper(struct work_struct *work){
    struct diskdriver_work_t * w = container_of(work,struct diskdriver_work_t,wk);
    struct bio *tbio = (struct bio*)w->payload;

#ifdef SMARTCACHE
    struct smart_tracker* t = (struct smart_tracker*) tbio->bi_private1;
    int numzero = process_jadu_reads("ddendio",tbio);
    if(numzero == 0){
        cache_end_helper(tbio);
    }
    tbio->bi_end_io = t->end_io;
    kmem_cache_free(smart_tracker_cache,t);
#else
    process_jadu_reads("ddendio",tbio);
    handler_finishio(tbio);
#endif
    bio_endio(tbio, 0);
    kmem_cache_free(diskdriver_wk_cache,w);
}
#endif

void diskdriver_end_io(struct bio *diskdriver_bio,int error)
{

#if defined(HIGH)
    handler_finishio(diskdriver_bio);
    bio_endio(diskdriver_bio,0);
#else
#if (defined(JADUKATA) || defined(SMARTCACHE)) && !defined(BCACHE)
    struct diskdriver_work_t *w = NULL;
#endif

    if(!bio_flagged(diskdriver_bio,BIO_UPTODATE)){
        dprinte(1,"ERROR: diskdriver end io\n");
    }

#if (defined(JADUKATA) || defined(SMARTCACHE)) && !defined(BCACHE)
#ifndef SMARTCACHE
    if(bio_data_dir(diskdriver_bio) == READ){
#endif
        w = (struct diskdriver_work_t*)kmem_cache_alloc(diskdriver_wk_cache,GFP_ATOMIC);
        if (w == NULL) {
            dpanic("ERROR: mem allocation failure for diskdriver endio work struct\n");
        }
        INIT_WORK(&w->wk,diskdriver_end_io_helper);
        w->payload = (unsigned long)diskdriver_bio;
        queue_work(diskdriver_wkq,&w->wk);
#ifndef SMARTCACHE
    }else{
        handler_finishio(diskdriver_bio);
        bio_endio(diskdriver_bio,0);
    }
#endif

#else
    //#ifndef SMARTCACHE
    handler_finishio(diskdriver_bio);
    bio_endio(diskdriver_bio,0);
#endif
#endif
}

#ifdef SMARTCACHE

int cache_helper(struct bio *diskdriver_bio){
    int i=0,k=0,l=0;
    struct cache_helper_t * meta = NULL;
    struct bio_vec *bv;
    struct smart_tracker* t = NULL;
    int incache = 1;
    int diskcache = 0;
    char jadumsg[1000];
    char* jmsg = jadumsg;

    meta = (struct cache_helper_t*)kmem_cache_alloc(cache_helper_cache,GFP_ATOMIC);
    if(meta == NULL){
        dprinte(1,"ERROR: cannot allocate memory for meta in cache helper\n");
        return -1;
    }

    if(bio_sectors(diskdriver_bio) <= 32){
        bool should_process = false;
        
        if(cache_smart_mode>=0){
            if( (ht_lookup(ht_metadata_sectors,diskdriver_bio->bi_sector) != 0) || (ht_lookup(ht_data_smallfile_sectors,diskdriver_bio->bi_sector) != 0) ){
                should_process = true;
            }
        }else{
            should_process = true;
        }

        if(should_process){
            // check the cache
            for(i=0;i<bio_sectors(diskdriver_bio);i++){
                meta[i].status = 2;
                meta[i].tbio = NULL;
                meta[i].smartslot = SMARTCACHE_SIZE + 2;
                ht_lookup_val(ht_smartcache,diskdriver_bio->bi_sector+i,&(meta[i].smartslot));
                if(meta[i].smartslot >= SMARTCACHE_SIZE){
#ifndef EXTCACHE
                    incache=0;
#else
                    ht_lookup_val(ht_diskcache,diskdriver_bio->bi_sector+i,&(meta[i].smartslot));
                    if(meta[i].smartslot >= SMARTCACHE_SIZE){
                        incache = 0;
                    }else{
                        unsigned long cache_sector = meta[i].smartslot;
                        diskcache++;
                        meta[i].smartslot = alloc_smartslot(diskdriver_bio->bi_sector+i);

                        if(meta[i].smartslot >= SMARTCACHE_SIZE){
                            diskdriver_cache_free(diskdriver_bio->bi_sector+i,cache_sector);
                            incache = 0;
                            meta[i].smartslot = SMARTCACHE_SIZE + 2;
                            continue;
                        }

                        if(bio_data_dir(diskdriver_bio) == READ){
                            init_completion(&meta[i].event);

                            meta[i].tbio = alloc_bio_buffer(diskdriver_device.c_dev,cache_sector,cache_disk_read_end_io,WRITE,(char*)(smartcache+(meta[i].smartslot*512)),512);
                            if(meta[i].tbio == NULL){
                                dprinte(1,"ERROR: could not allocate disk cache space\n");
                                free_smartslot(meta[i].smartslot);
                                incache = 0;
                                meta[i].smartslot = SMARTCACHE_SIZE + 2;
                                continue;
                            }
                            meta[i].tbio->bi_private1 = &meta[i];
                            generic_make_request(meta[i].tbio);
                        }
                    }
#endif
                }
            }
            
            jmsg += sprintf(jmsg,"%s {",(incache?(diskcache?"ind":"in"):"no"));
           
#ifdef EXTCACHE
           //TODO: 1) handle smartslot allocation failure during diskcache==1 and write to make sure 
           // diskcache version are forgotten.
           // 2) wait outside make request function
            if(diskcache){
                if(bio_data_dir(diskdriver_bio) == READ){
                    for(i=0;i<bio_sectors(diskdriver_bio);i++){
                        if(meta[i].tbio != NULL){
                            wait_for_completion(&meta[i].event);
                            if(meta[i].status == 1){
                                dprinte(1,"ERROR: failed diskcache read status %d smartslot %lu \n",meta[i].status,meta[i].smartslot);
                                free_smartslot(meta[i].smartslot);
                                incache = 0;
                                meta[i].smartslot = SMARTCACHE_SIZE + 2;
                            }
                        }
                    }
                }
            }
#endif

            if(incache){
                if(bio_data_dir(diskdriver_bio) == WRITE){
                    l =0;
                    bio_for_each_segment(bv,diskdriver_bio,i){
                        void *kaddr = kmap_atomic(bv->bv_page) + bv->bv_offset;
                        for(k=0;k<(bv->bv_len/512);k++){
                            jmsg += sprintf(jmsg,"%lu %lu,",diskdriver_bio->bi_sector+l,meta[l].smartslot);
                            memcpy((void*)(smartcache + (meta[l].smartslot*512)),(void*)(((char*)kaddr)+(512*k)),512);
#ifdef EXTCACHE
                            smartcache_set_dirty(meta[l].smartslot); 
#endif
                            l++;
                        }
                        kunmap_atomic(kaddr);
                    }
                }else{
#ifdef TESTMODE
                    int j=0;
                    for(j=0;j<=(bio_sectors(diskdriver_bio)/8);j++){
                        int sectors = bio_sectors(diskdriver_bio) - (j*8);
                        if(sectors){
                            char *ret = read_block(diskdriver_bio->bi_sector+(j*8));
                            if(ret != NULL){
                                if(sectors > 8) sectors = 8;
                                for(l=0;l<sectors;l++){
                                    if(memcmp(ret+(512*l),smartcache + (meta[k].smartslot*512),512) != 0){
                                        dprinte(1," ERROR : mismatch in cached read sector %lu smartslot %lu \n",diskdriver_bio->bi_sector+k,meta[k].smartslot);
                                    }else{
                                        dprintk(1," MATCHES : read sector %lu smartslot %lu \n",diskdriver_bio->bi_sector+k,meta[k].smartslot);
                                    }
                                    k++;
                                }
                                free_page((unsigned long)ret); 
                            }else{
                                dprinte(1," ERROR : could not read block for comparision sector %lu num sectors %d \n",diskdriver_bio->bi_sector,bio_sectors(diskdriver_bio));
                            }
                        }
                    }
#endif
                    l =0;
                    bio_for_each_segment(bv,diskdriver_bio,i){
                        void *kaddr = kmap_atomic(bv->bv_page) + bv->bv_offset;
                        for(k=0;k<(bv->bv_len/512);k++){
                            memcpy((void*)((char*)kaddr+(512*k)),(void*)(smartcache + (meta[l].smartslot*512)),512);
                            jmsg += sprintf(jmsg,"%lu %lu,",diskdriver_bio->bi_sector+l,meta[l].smartslot);
                            l++;
                        }
                        kunmap_atomic(kaddr);
                    }
                    smartcache_hits+=bio_sectors(diskdriver_bio);
                    jmsg += sprintf(jmsg,"}");
                    print_bio(jadumsg,diskdriver_bio);
                    kmem_cache_free(cache_helper_cache,meta);
#ifdef LOW
#ifdef JADUKATA
                    jadukata_process_bio("jaduread2",diskdriver_bio);
#endif
#endif
                    //for cached reads bail out early
                    return 1;
                }
            }else{
                if(bio_data_dir(diskdriver_bio) == WRITE){
                    l =0;
                    bio_for_each_segment(bv,diskdriver_bio,i){
                        void *kaddr = kmap_atomic(bv->bv_page) + bv->bv_offset;
                        for(k=0;k<(bv->bv_len/512);k++){
                            jmsg += sprintf(jmsg, "%lu ", diskdriver_bio->bi_sector+l);
                            if(meta[l].smartslot >= SMARTCACHE_SIZE){
                                meta[l].smartslot = alloc_smartslot(diskdriver_bio->bi_sector+l);
                                jmsg += sprintf(jmsg, "a");
                                if(meta[l].smartslot >= SMARTCACHE_SIZE){
                                    jmsg += sprintf(jmsg, "f");
                                }
                            }
                            jmsg += sprintf(jmsg, "%lu,", meta[l].smartslot);
                            if(meta[l].smartslot < SMARTCACHE_SIZE){
                                memcpy((void*)(smartcache + (meta[l].smartslot*512)),(void*)(((char*)kaddr)+(512*k)),512);
#ifdef EXTCACHE
                                smartcache_set_dirty(meta[l].smartslot); 
#endif
                            }
                            l++;
                        }
                        kunmap_atomic(kaddr);
                    }
                }else{
                    for(i=0;i<bio_sectors(diskdriver_bio);i++){
                        if(meta[i].smartslot < SMARTCACHE_SIZE){
                            jmsg += sprintf(jmsg, "%lu f%lu,", diskdriver_bio->bi_sector+i, meta[i].smartslot);
                            free_smartslot(meta[i].smartslot);
                        }
                    }
                }
            }
        }else{
            if(cache_smart_mode>=0){
                jmsg += sprintf(jmsg,"notmeta {");
            }
        }
    }
    jmsg += sprintf(jmsg,"}");
    print_bio(jadumsg,diskdriver_bio);

    t = (struct smart_tracker*) kmem_cache_alloc(smart_tracker_cache,GFP_ATOMIC);
    t->sector = diskdriver_bio->bi_sector;
    t->dir = bio_data_dir(diskdriver_bio);
    t->size = bio_sectors(diskdriver_bio);
    t->end_io = diskdriver_bio->bi_end_io;
    diskdriver_bio->bi_private1 = t; 
    diskdriver_bio->bi_end_io = diskdriver_end_io;
    kmem_cache_free(cache_helper_cache,meta);
    return 0;
}
#endif

#if (!defined(BCACHE) && defined(LOW)) || (defined(HIGH) && defined(JADUKATA_VMM_COMM))
void diskdriver_new_request(struct request_queue *queue,struct bio *diskdriver_bio)
{
    int numzero = 0;
    if(bio_data_dir(diskdriver_bio) == WRITE){
        atomic_add(bio_sectors(diskdriver_bio),&stat_totw);
    }else{
        atomic_add(bio_sectors(diskdriver_bio),&stat_totr);
    }

    //dont allow i/o to outside the configured size
    if(diskdriver_bio->bi_sector > DISKDRIVER_SIZE){
     // print_bio("ignoring outside disk i/o",diskdriver_bio);
        bio_endio(diskdriver_bio,0);
        return;
    }
    
    print_bio("jadu",diskdriver_bio);
    diskdriver_bio->bi_private1 = NULL;

    // act as pass through until the module is actual inited
    if(likely(module_init != 0)){
        numzero = process_jadu_writes("ddnewreq", diskdriver_bio);

#ifdef SMARTCACHE
        if(numzero == 0){
            if(cache_helper(diskdriver_bio) == 1){
                atomic_add(bio_sectors(diskdriver_bio),&cached_reads);
                //for cached reads alone we are already done
                // so bail out
                bio_endio(diskdriver_bio,0);
                return;
            }
        }
#else
        handler_startio(diskdriver_bio);
        ((handler*)diskdriver_bio->bi_private1)->end_io = diskdriver_bio->bi_end_io;
        diskdriver_bio->bi_end_io = diskdriver_end_io;
#endif
    }

    diskdriver_bio->bi_bdev = diskdriver_device.f_dev;
    generic_make_request(diskdriver_bio);
    return;
}
#elif (!defined(SMARTCACHE) && defined(BCACHE)) || defined(HIGH)
void diskdriver_new_request(struct request_queue *queue,struct bio *diskdriver_bio)
{
    if(bio_data_dir(diskdriver_bio) == WRITE){
        atomic_add(bio_sectors(diskdriver_bio),&stat_totw);
    }else{
        atomic_add(bio_sectors(diskdriver_bio),&stat_totr);
    }
    //dont allow i/o to outside the configured size
    if(diskdriver_bio->bi_sector > DISKDRIVER_SIZE){
     // print_bio("ignoring outside disk i/o",diskdriver_bio);
        bio_endio(diskdriver_bio,0);
        return;
    }
    dprintk(1,"diskdriver: leo temp bio %p uniq %d\n",diskdriver_bio, diskdriver_bio->isunique);
#ifndef HIGH
    print_bio("jadu",diskdriver_bio);
#endif
#ifdef STATS
    handler_startio(diskdriver_bio);
    ((handler*)diskdriver_bio->bi_private1)->end_io = diskdriver_bio->bi_end_io;
    diskdriver_bio->bi_end_io = diskdriver_end_io;
#else
    diskdriver_bio->bi_private1 = NULL;
#endif
    diskdriver_bio->bi_bdev = diskdriver_device.f_dev;
    generic_make_request(diskdriver_bio);
    return;
}
#else
adfadf bad dont compile
#endif

/* This method will initialize the device specific structures */
int __init diskdriver_init(void)
{
    int nsectors,i,j;
    char temp_name[20];
    
    dprinte(1,"DISKDRIVER init starting ...\n");

    hash_init();

#ifdef SMARTCACHE
    SMARTCACHE_SIZE = RCACHE_SIZE;
    replace_slot = SMARTCACHE_SIZE/2;
    if(smartcache == NULL){
        smartcache = (char*)myd_vmalloc(SMARTCACHE_SIZE*512);
        if(smartcache == NULL){
            dprintk(1,"smart cache alloc failed size %lu bytes \n",SMARTCACHE_SIZE*512);
            dpanic( "smart cache alloc failed\n");
        }
    }

    if(smartcache_header == NULL){
        smartcache_header = (unsigned long*)myd_vmalloc(SMARTCACHE_SIZE*sizeof(unsigned long));
        if(smartcache_header == NULL){
            dprintk(1,"smart cache header alloc failed %lu bytes \n",SMARTCACHE_SIZE*sizeof(unsigned long));
            dpanic( "smart cache header alloc failed\n");
        }
        for(i=0;i<SMARTCACHE_SIZE;i++){
            smartcache_header[i] = DISKDRIVER_SIZE+1;
        }
    }
    if(smart_tracker_cache == NULL){
        smart_tracker_cache = kmem_cache_create("smtrack",sizeof(struct smart_tracker),0,SLAB_POISON,NULL);
    }
    if(cache_helper_cache == NULL){
        cache_helper_cache = kmem_cache_create("chcache",64*sizeof(struct cache_helper_t),0,SLAB_POISON,NULL);
    }
#ifdef EXTCACHE
    if(smartcache_dirty_bitmap == NULL){
        smartcache_dirty_bitmap = (unsigned long*)myd_vzalloc(SMARTCACHE_SIZE>>3);
    }
#endif
#endif

#ifdef BCACHE
    bcache_init();
    if(at_ignore_cache == NULL){
        at_create(&at_ignore_cache,"ignorecache");
    }
#endif


#ifdef DISKDRIVER_KERN_ASSIST
	cdata_init();
	clear_cdata();

	cdata_start = 0;
	cdata_log = 0;
#endif

    spin_lock_init(&(diskdriver_device.lock));
    
    //diskdriver_device.f_dev = open_by_devnum(pdev[0],FMODE_READ | FMODE_WRITE);
    diskdriver_device.f_dev = blkdev_get_by_dev(pdev[0],FMODE_READ | FMODE_WRITE,NULL);
    //diskdriver_device.f_dev = blkdev_get_by_path(pdev_path,FMODE_READ | FMODE_WRITE,NULL);
    
    if (IS_ERR(diskdriver_device.f_dev)) {
        dprintk(1,"diskdriver dev f_dev error\n");
        return PTR_ERR(diskdriver_device.f_dev);
    }else{
        dprinte(1,"diskdriver dev f_dev inode %p max sector %llu  \n", diskdriver_device.f_dev->bd_inode, i_size_read(diskdriver_device.f_dev->bd_inode) );
    }

    diskdriver_device.size = DISKDRIVER_SIZE;
    nsectors = diskdriver_device.size;

    diskdriver_queue = blk_alloc_queue(GFP_KERNEL);
    if (!diskdriver_queue) {
        dprintk(1,"diskdriver: queue mem alloc fails\n");
        goto out;
    }
    
    blk_queue_make_request(diskdriver_queue,diskdriver_new_request);

    /* Register */
    for(i=0;i<DISKDRIVER_DEVS;i++){
        sprintf(temp_name,"%s%d",DEVICE_NAME,i);
        diskdriver_major[i] = register_blkdev(0,temp_name);

        if (diskdriver_major[i] <= 0) {
            dprintk(1,"diskdriver: can't get major i: %d\n",i);
            goto out_unregister;
        }
        if(i==0) diskdriver_first_major = diskdriver_major[i];

        /* Add the gendisk structure */
        diskdriver_device.gd[i] = alloc_disk(1);
        if (!diskdriver_device.gd[i]) {
            goto out_unregister;
        }
        diskdriver_device.gd[i]->major = diskdriver_major[i];
        diskdriver_device.gd[i]->first_minor = 0;
        diskdriver_device.gd[i]->fops = &diskdriver_bdops;
        diskdriver_device.gd[i]->private_data = &diskdriver_device;
        diskdriver_device.gd[i]->queue = diskdriver_queue;
        strcpy(diskdriver_device.gd[i]->disk_name,temp_name);
        set_capacity(diskdriver_device.gd[i],nsectors);

        add_disk(diskdriver_device.gd[i]);
       
        //to self  
        diskdriver_device.s_dev[i] = bdget(disk_devt(diskdriver_device.gd[i]));
       
        if(diskdriver_device.s_dev[i]) bdput(diskdriver_device.s_dev[i]);
    }
    
    //dprinte(1,"s_devc %p \n",diskdriver_queue);
    //dprinte(1,"s_devd %d \n",diskdriver_queue->limits.logical_block_size);
    //dprinte(1,"s_deve %d \n",diskdriver_queue->limits.physical_block_size);
    
    blk_queue_logical_block_size(diskdriver_queue, DISKDRIVER_SECSIZE);
    blk_queue_physical_block_size(diskdriver_queue,DISKDRIVER_SECSIZE);
    

#ifdef EXTCACHE
    diskdriver_device.c_dev = blkdev_get_by_dev(cdev[0],FMODE_READ | FMODE_WRITE,NULL);

    if (IS_ERR(diskdriver_device.c_dev)) {
        dprintk(1,"diskdriver dev c_dev error\n");
        return PTR_ERR(diskdriver_device.c_dev);
    }

    if(diskdriver_device.c_dev != diskdriver_device.c_dev->bd_contains){
        CACHE_DISK_OFFSET = diskdriver_device.c_dev->bd_part->start_sect;
    }

    diskdriver_init_bitmaps();
#endif

    handler_init();

#if defined(JADUKATA) || defined (SMARTCACHE)
    if(diskdriver_wkq == NULL){
        diskdriver_wkq = create_workqueue("diskdriver_wkq");
    }

    if(diskdriver_wk_cache == NULL){
        diskdriver_wk_cache = kmem_cache_create("ddwkq",sizeof(struct diskdriver_work_t),0,SLAB_POISON,NULL);
    }
#endif

    dprintk(1,"DISKDRIVER init over ... successfully added the driver ( total secs em_disk : %lu ) \n",DISKDRIVER_SIZE);
#ifdef LOW
    dprintk(1,"DISKDRIVER cache config ( rcache: %lu dcache: %lu ) \n",RCACHE_SIZE,DCACHE_SIZE);
#endif

#ifdef SMARTCACHE
    module_init = 2;
#else    
    module_init = 1;
#endif
    return 0;

out_unregister:
    for(j=0;j<i;j++){
        sprintf(temp_name,"%s%d",DEVICE_NAME,j);
        unregister_blkdev(diskdriver_major[j],temp_name);
    }
out:
    dprintk(1,"Unable to load the device\n");
    return -ENOMEM;
}

/*
 * Free up the allocated memory and cleanup.
 */
static __exit void diskdriver_cleanup(void)
{

    int i;
    const int TEMP_NAME_SIZE = 4096*32;
    char *temp_name = (char*) myd_kmalloc(TEMP_NAME_SIZE,GFP_KERNEL);
    dprinte(1,"DISKDRIVER cleanup starting ...\n");

#if defined(JADUKATA) || defined (SMARTCACHE)
    if(diskdriver_wkq != NULL){
        flush_workqueue(diskdriver_wkq);
        destroy_workqueue(diskdriver_wkq);
        diskdriver_wkq = NULL;
    }
    if(diskdriver_wk_cache != NULL){
        kmem_cache_destroy(diskdriver_wk_cache);
        diskdriver_wk_cache = NULL;
    }
#endif

#ifdef DISKDRIVER_KERN_ASSIST
	cdata_log = 0;

	cdata_start = 0;
	clear_cdata();
	cdata_destroy();
#endif

    print_stats(temp_name);

    for(i=0;i<DISKDRIVER_DEVS;i++){
        sprintf(temp_name,"%s%d",DEVICE_NAME,i);
        del_gendisk(diskdriver_device.gd[i]);
        put_disk(diskdriver_device.gd[i]);
        unregister_blkdev(diskdriver_major[i],temp_name);
        diskdriver_device.s_dev[i] = NULL;
    }

    blk_cleanup_queue(diskdriver_queue);

    if(diskdriver_device.f_dev) blkdev_put(diskdriver_device.f_dev,FMODE_READ | FMODE_WRITE );
    diskdriver_device.f_dev = NULL;

#ifdef EXTCACHE
    if(diskdriver_device.c_dev) blkdev_put(diskdriver_device.c_dev,FMODE_READ | FMODE_WRITE );
    diskdriver_device.c_dev = NULL;
    diskdriver_destroy_bitmaps();
#endif

    for (i= 0;i<DISKDRIVER_DEVS;i++){
        if(diskdriver_blk_dev[i]) blkdev_put(diskdriver_blk_dev[i],FMODE_READ | FMODE_WRITE );
        diskdriver_blk_dev[i] = NULL;
    }

    myd_kfree(temp_name,TEMP_NAME_SIZE);
    
#ifdef SMARTCACHE
    if(smartcache != NULL){
        myd_vfree(smartcache,SMARTCACHE_SIZE*512);
        smartcache = NULL;
    }

    if(smartcache_header != NULL){
        myd_vfree(smartcache_header,SMARTCACHE_SIZE*sizeof(unsigned long));
        smartcache_header = NULL;
    }
    if(smart_tracker_cache != NULL){
        kmem_cache_destroy(smart_tracker_cache);
        smart_tracker_cache = NULL;
    }
    if(cache_helper_cache != NULL){
        kmem_cache_destroy(cache_helper_cache);
        cache_helper_cache = NULL;
    }
#ifdef EXTCACHE
    if(smartcache_dirty_bitmap != NULL){
        myd_vfree(smartcache_dirty_bitmap,SMARTCACHE_SIZE>>3);
        smartcache_dirty_bitmap = NULL;
    }
#endif
#endif
#ifdef BCACHE
    bcache_exit();
    if(at_ignore_cache != NULL){
        at_destroy(at_ignore_cache);
        at_ignore_cache = NULL;
    }
#endif
    handler_cleanup();

    /*
    {
        extern void test_ht_at(void);    
        test_ht_at();
    }
    */

    hash_cleanup();
    
    module_init = 0;

    dprinte(1,"DISKDRIVER cleanup over ... exiting");
}

module_init(diskdriver_init);
module_exit(diskdriver_cleanup);

MODULE_LICENSE("GPL");
