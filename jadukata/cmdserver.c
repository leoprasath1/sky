#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#include <linux/netlink.h>

#define NETLINK_USER 31
#define IOCTL_BASEDIR "/home/arulraj/cerny/jadukata/diskdriver/scripts/"
#define DMDEDUP_IOCTL_BASEDIR "/home/arulraj/cerny/jadukata/dm-dedup/scripts/"
#include "expts/testfwklib.c"

struct sockaddr_nl src_addr, dest_addr;
struct nlmsghdr *nlh = NULL;
struct iovec iov;
struct msghdr msg;

int mystrcmp(char* a, char* b){
    return strncmp(a,b,strlen(b));
}

int send_msg_helper(int sock_fd,char*msgdata, int msglen){
    int ret;
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid(); /* self pid */

    bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr));

    memset(&dest_addr, 0, sizeof(dest_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0; /* For Linux Kernel */
    dest_addr.nl_groups = 0; /* unicast */

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD_KERNEL));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD_KERNEL));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD_KERNEL);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;

    memcpy(NLMSG_DATA(nlh),msgdata,msglen);

    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    ret = sendmsg(sock_fd,&msg,0);
    if(ret == -1){
        perror("netlink send msg to kernel failed");
        printf("ERROR send msg ret %d \n" , ret);
    }
    return ret;
}
                
void increment(khash_t(32) *h, int k){
    khint_t i;
    khiter_t it;
    int ret;
    int v = 0;
    it = kh_get(32,h,k);
    if(it == kh_end(h)){
        v = 1;
    }else{
        v = kh_val(h,it) + 1;
        kh_del(32,h,it);
    }

    it = kh_put(32,h,k,&ret);
    if(!ret){
        printf("ERROR inserting %d into hash table : ret %d \n",k, ret);
        exit(1);
    }
    kh_value(h,it) = v;
}

int journal_handle(char* map1){
    int MAX_BUF_SIZE = 4096*3000;
    char cmd[CMD_LEN];
    char *buffer = (char*)malloc(MAX_BUF_SIZE);
    char *ptr;
    char disk[100];
    int k1,v1,k2,v2;
    int ret;
    khint_t i;
    khiter_t it;

    khash_t(32) *h1 = NULL;
    if(h1!=NULL){
        kh_destroy(32,h1);
        h1 = NULL;
    }
    h1 = kh_init(32);

    khash_t(32) *h2 = NULL;
    if(h2!=NULL){
        kh_destroy(32,h2);
        h2 = NULL;
    }
    h2 = kh_init(32);

    khash_t(32) *h3 = NULL;
    if(h3!=NULL){
        kh_destroy(32,h3);
        h3 = NULL;
    }
    h3 = kh_init(32);

    map_to_hash(map1,h1);

    sprintf(cmd,"mount | grep jadu");
    run_cmd(cmd,buffer);

    sscanf(buffer,"%s on",disk);

    sprintf(cmd,"sudo debugfs -R 'stat disk.raw' %s 2>/dev/null",disk);
    run_cmd_core_helper(cmd,buffer,debug,MAX_BUF_SIZE);
    
    map_to_hash(buffer,h2);

    my_foreach(h1, i, k1, v1){
        it = kh_get(32,h2,v1);
        if(it == kh_end(h2)){
            printf("ERROR key not found in h2 %d \n", v1);
            exit(1);
        }else{
            v2 = kh_val(h2,it);
            kh_put(32,h3,v2,&ret);   
            if(!ret){
                printf("ERROR inserting key into h3 %d \n",v2);
                exit(1);
            }else{
                //printf("inserting key into h3 %d \n",v2);
            }
        }
    }

    int prev=-10;
    int count = 0;
    int j = 0,k =0;
    int * array = malloc(h3->size * sizeof(int));
    printf("total blocks %d \n", h3->size);
    my_foreach(h3, i, k1, v1){
        array[j++] = k1;
    }

    {
        int compare_ints(const void *a, const void *b){
            return (*((int*)a) - *((int*)b));
        }
        qsort(array,j,sizeof(int),compare_ints);
    }

    ptr = buffer;
    for(k=0;k<j;k++){
        k1 = array[k];
        if(k1 == (prev+count+1)){
            count++;
        }else{
            if(prev >=0){
                ptr += sprintf(ptr,"%d-%d,",prev,prev+count);
            }
            prev = k1;
            count = 0;
        }
        //printf("block %d \n",k1);
    }
    if(prev>=0){
        ptr += sprintf(ptr,"%d-%d,",prev,prev+count);
    }
    //remove last comma
    *(ptr-1) = '\0';
    sprintf(cmd,"sudo %s/ioctl ignore_cache '%s' ",IOCTL_BASEDIR,buffer);
    run_cmd_core_helper(cmd,buffer,debug,MAX_BUF_SIZE);

    free(array);
}

void get_cached_sectors(char* outbuff){
    int MAX_BUF_SIZE = 4096*3000;
    char cmd[CMD_LEN];
    char *buffer = (char*)malloc(MAX_BUF_SIZE);
    char *inptr = buffer, *outptr = outbuff;
    char disk[100];
    int ret;
    int k1,v1,k2,v2;
    khint_t i; 
    khiter_t it;
    
    khash_t(32) *h2 = NULL;
    if(h2!=NULL){
        kh_destroy(32,h2);
        h2 = NULL;
    }
    h2 = kh_init(32);
    
    khash_t(32) *h2rev = NULL;
    if(h2rev!=NULL){
        kh_destroy(32,h2rev);
        h2rev = NULL;
    }
    h2rev = kh_init(32);
    
    khash_t(32) *hres = NULL;
    if(hres!=NULL){
        kh_destroy(32,hres);
        hres = NULL;
    }
    hres = kh_init(32);
    
    sprintf(cmd,"mount | grep jadu");
    run_cmd(cmd,buffer);

    sscanf(buffer,"%s on",disk);

    sprintf(cmd,"sudo debugfs -R 'stat disk.raw' %s 2>/dev/null",disk);
    run_cmd_core_helper(cmd,buffer,debug,MAX_BUF_SIZE);

    printf("buffer %s \n", buffer);

    //h2 virtdisk -> phydisk
    //h2rev phydisk -> virtdisk
    
    map_to_hash(buffer,h2);

    my_foreach(h2, i, k1, v1){
        it = kh_get(32,h2rev,v1);
        if(it == kh_end(h2rev)){
            it = kh_put(32,h2rev,v1,&ret);
            if(!ret){
                printf("ERROR inserting %d into hash table h2rev : ret %d \n",v1, ret);
                exit(1);
            }
            kh_value(h2rev,it) = k1;
            if(debug){
                printf("h2rev %d:%d\n",v1,k1);
            }
        }else{
            printf("ERROR key already found in h2rev %d \n", v1);
            exit(1);
        }
    }

    buffer[0] = 0;
    outptr[0] = 0;
    //get list of sectors in cache
    sprintf(cmd,"sudo %s/ioctl get_cached_sectors ",IOCTL_BASEDIR);
    run_cmd_core_helper(cmd,buffer,debug,MAX_BUF_SIZE);

    if(buffer[0] == 0){
        return;
    }
    
    //remove last comma
    inptr = buffer;
    inptr[strlen(inptr)-1] = '\0';

    printf("get_cached_sectors result %s \n",inptr);

    while(1){
        unsigned long s,len,e;
        if(!inptr){
            break;
        }
        if(strchr(inptr, '-') == NULL){
            break;
        }
        sscanf(inptr,"%lu-%lu,",&s,&len);
        e = s+ len;
        inptr = strchr(inptr,',');
        if(inptr){
            inptr++;
        }
        while(s<e){
            k2 = s/8;
            it = kh_get(32,h2rev,k2);
            if(it == kh_end(h2rev)){
                increment(hres,-1);
            }else{
                v2 = kh_val(h2rev,it);
                increment(hres,v2);
            }
            s++;
        }
    }

    my_foreach(hres, i, k1, v1){
        outptr += sprintf(outptr, "%d-%d,", k1, v1);
    }
    
    printf("get_cached_sectors output %s \n",outbuff);
    free(buffer);

    return;
}

int main(int argc, char *argv[])
{
    int listenfd = 0, connfd = 0, sock_fd = 0;
    int n, txfr;
    struct sockaddr_in serv_addr; 
    int nokvm = 0;
    int id = 0; 
    int netconsole = 0;
    int unmodified = 0;

    char* sendBuff = malloc(MAX_PAYLOAD);
    char* recvBuff = malloc(MAX_PAYLOAD);
    char* buffer = malloc(MAX_PAYLOAD);

    char cmd[CMD_LEN];
    char *buff;

    if((argc>1) && strcmp(argv[1],"netconsole") == 0){
        myprintf("\n netconsole will never be turned off\n");
        netconsole = 1;
    }
    
    if((argc>1) && strcmp(argv[1],"unmod-bare") == 0){
        myprintf("\n netconsole will never be turned off\n");
        unmodified = 1;
    }

    signal(SIGPIPE, SIG_IGN);

    sprintf(cmd,"lsmod | grep kvm");
    nokvm = run_cmd(cmd,buffer);

    sock_fd=socket(PF_NETLINK, SOCK_RAW, NETLINK_USERSOCK);

    if(sock_fd<0){
        myprintf("\n ERROR : Could not create netlink socket \n");
        return 1;
    }

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, 0, sizeof(serv_addr));
    memset(sendBuff, 0, sizeof(sendBuff)); 
    memset(recvBuff, 0, sizeof(recvBuff)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(5000); 

    //set recv timeout 
    struct timeval timeout;      
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    /*
       if (setsockopt (listenfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
       perror("setsockopt failed\n");

       if (setsockopt (listenfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
       perror("setsockopt failed\n");
       */

    int optval = 1;
    if (setsockopt (listenfd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval) ) < 0)
        perror("setsockopt failed\n");

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

    listen(listenfd, 10); 

    fflush(stdout);

    while(1)
    {

        id++;

        connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 

        txfr = MAX_PAYLOAD;
        do{
            n = read(connfd, recvBuff + (MAX_PAYLOAD-txfr), txfr);

            if(n<0){
                myprintf(" ERROR : receiving command failed %d\n",n);
                return 1;
            }
            txfr -= n;
        }while(txfr || !n);

        int dontkvm = 0;

        myprintf("%d command %s\n",id,recvBuff);
        sendBuff[0]='\0';

        if(unmodified){
            sprintf(sendBuff,"%d unmodified \n", 0);
            txfr = MAX_PAYLOAD;
            do{
                n = write(connfd, sendBuff + (MAX_PAYLOAD-txfr), txfr); 
                if(n<0){
                    myprintf(" ERROR : unmod sending output ret %d\n",n);
                    return 1;
                }
                txfr -= n;
            }while(txfr);

            close(connfd);
            fflush(stdout);
            sleep(1);
            continue;
        }

        if( mystrcmp(recvBuff,"clear") == 0 || mystrcmp(recvBuff,"status")==0 ){
            int n, index; char buffertmp[MAX_PAYLOAD];
            
            buff = buffer;
            sprintf(cmd,"sudo %s/ioctl get_stats ", IOCTL_BASEDIR);
            run_cmd(cmd,buff);

            buff += strlen(buff);
            
            sprintf(cmd,"tail -n +1 /sys/block/bcache0/bcache/stats_total/* 2>/dev/null");
            run_cmd(cmd,buff);
            
            buff += strlen(buff);
            
            if(mystrcmp(recvBuff,"clear") == 0){
                sprintf(cmd,"sudo %s/ioctl unique_stats_clear",DMDEDUP_IOCTL_BASEDIR);
                run_cmd(cmd,buff);
            }else{
                sprintf(cmd,"sudo %s/ioctl unique_stats_print",DMDEDUP_IOCTL_BASEDIR);
                run_cmd(cmd,buff);
            }

            if(mystrcmp(recvBuff,"clear") == 0){
                sprintf(cmd,"sudo bash -c 'ls /sys/block/ | grep bcache'");
                run_cmd(cmd,buffertmp);
                index=0;
                for(n=0;n<strlen(buffertmp); n++){
                    if(buffertmp[n] == '\n'){
                        buffertmp[n] = '\0';
                        sprintf(cmd,"sudo bash -c 'echo 1 > /sys/block/%s/bcache/clear_stats'",buffertmp+index);
                        run_cmd_o(cmd);
                        if(mystrcmp(recvBuff,"clear_stats") != 0){
                            sprintf(cmd,"sudo bash -c 'echo 1 > /sys/block/%s/bcache/reset_cache'",buffertmp+index);
                            run_cmd_o(cmd);
                        }
                        index=n+1;
                    }
                }
            }
            
            /*            
            if(mystrcmp(recvBuff,"clear_stats") == 0){
                dontkvm = 1;
            }
            */

            sprintf(sendBuff,"%d \n %s", 0,buffer);
        }


        if( mystrcmp(recvBuff,"wkld_trace") == 0 ){
            sprintf(sendBuff,"%d \n ", 0);
        }

        if(mystrcmp(recvBuff,"dropcaches") == 0){
            sprintf(cmd,"sync ");
            run_cmd(cmd,buffer);
            sprintf(cmd,"sudo bash -c 'echo 3 > /proc/sys/vm/drop_caches' ");
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"logrotate") == 0){
            sprintf(cmd,"sudo cp /var/log/syslog /home/arulraj/syslog.%ld ", time(NULL));
            run_cmd(cmd,buffer);
            sprintf(cmd,"sudo bash -c '>/var/log/syslog' ");
            run_cmd(cmd,buffer);
            sprintf(cmd,"sudo bash -c '>/var/log/kern.log' ");
            run_cmd(cmd,buffer);
            sprintf(cmd,"sudo bash -c '>/var/log/messages' ");
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"journal_handle") == 0){
            journal_handle(recvBuff);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"get_cached_sectors") == 0){
            get_cached_sectors(sendBuff);
            dontkvm = 1;
        }
        
        if(mystrcmp(recvBuff,"get_cache_distribution") == 0){
            char* ptr = sendBuff;
            ptr += sprintf(ptr,"%d \n", 0);
            sprintf(cmd,"sudo %s/ioctl get_cache_distribution ",IOCTL_BASEDIR);
            run_cmd_core_helper(cmd,ptr,1,MAX_PAYLOAD-(sendBuff-ptr));
            dontkvm = 1;
        }
        
        if(mystrcmp(recvBuff,"remus") == 0){
            int remus_interval;
            sscanf(recvBuff,"remus %d",&remus_interval);
            sprintf(cmd,"sudo %s/ioctl remus %d ",IOCTL_BASEDIR,remus_interval);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"diskreadcache") == 0){
            int mode;
            sscanf(recvBuff,"diskreadcache %d",&mode);
            sprintf(cmd,"sudo hdparm -A %d /dev/sdb",mode);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"readahead") == 0){
            int ra;
            sscanf(recvBuff,"readahead %d",&ra);

            sprintf(cmd,"sudo blockdev --setra %d /dev/bcache0 ",ra);
            run_cmd(cmd,buffer);

            sprintf(cmd,"sudo blockdev --setra %d /dev/DISKDRIVER0 ",ra);
            run_cmd(cmd,buffer);

            sprintf(cmd,"sudo blockdev --setra %d /dev/sdb ",ra);
            run_cmd(cmd,buffer);
            
            buff = buffer;
            sprintf(cmd,"sudo blockdev --getra /dev/bcache0 /dev/DISKDRIVER0 /dev/sdb ");
            run_cmd(cmd,buffer);

            dontkvm = 1;
            sprintf(sendBuff,"%d \n %s", 0,buffer);
        }
        
        if(mystrcmp(recvBuff,"resize_cache") == 0){
            unsigned long percent;
            sscanf(recvBuff,"resize_cache %lu",&percent);
            sprintf(cmd,"sudo %s/ioctl resize_cache %lu ",IOCTL_BASEDIR,percent);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"perfcmd start") == 0){
            char filename[256];
            sscanf(recvBuff,"perfcmd start %s",filename);
            sprintf(cmd,"sudo /mnt/disk/perf-analysis/perf_start.sh %s & ",filename);
            system(cmd);
            sleep(2);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"perfcmd stop") == 0){
            sprintf(cmd,"sudo /mnt/disk/perf-analysis/perf_stop.sh ");
            system(cmd);
            sleep(2);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"unique_hints") == 0){
            int hints = 0;
            sscanf(recvBuff,"unique_hints %d",&hints);
            sprintf(cmd,"sudo %s/ioctl unique_hints %d ",IOCTL_BASEDIR,hints);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"unique_stats_clear") == 0){
            int hints = 0;
            sscanf(recvBuff,"clearing unique hints");
            sprintf(cmd,"sudo %s/ioctl unique_stats_clear",DMDEDUP_IOCTL_BASEDIR);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"0 %s", buffer);
        }
        
        if(mystrcmp(recvBuff,"unique_stats_print") == 0){
            int hints = 0;
            sscanf(recvBuff,"unique hints print");
            sprintf(cmd,"sudo %s/ioctl unique_stats_print",DMDEDUP_IOCTL_BASEDIR);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"0 %s", buffer);
        }
        
        if(mystrcmp(recvBuff,"trace") == 0){
            int level;
            int flag = 0;
            sscanf(recvBuff,"trace %d",&level);

            if(level == 222){
                level = 0;
                flag = 1;
            }

            sprintf(cmd,"sudo %s/ioctl trace %d ",IOCTL_BASEDIR,level);
            run_cmd(cmd,buffer);

            if(flag){
                level = 222;
            }

            if(level ==  0){
                sprintf(cmd,"sudo modprobe netconsole ");
                run_cmd(cmd,buffer);
            }else{
                if(!netconsole){
                    sprintf(cmd,"sudo rmmod netconsole ");
                    run_cmd(cmd,buffer);
                }
            }
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"readfill") == 0){
            int yesno;
            sscanf(recvBuff,"readfill %d",&yesno);
            sprintf(cmd,"sudo %s/ioctl readfill %d ",IOCTL_BASEDIR,yesno);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }
        
        if(mystrcmp(recvBuff,"cache_smartdumb") == 0){
            unsigned long mode = 3;
            sscanf(recvBuff,"cache_smartdumb %lu",&mode);
            sprintf(cmd,"sudo %s/ioctl cache_smartdumb %lu ",IOCTL_BASEDIR,mode);
            run_cmd(cmd,buffer);
            dontkvm = 1;
            sprintf(sendBuff,"%d \n", 0);
        }

        if(!nokvm && !dontkvm){
            int copy = 0;
            char *buff = sendBuff;
            if(mystrcmp(recvBuff,"status") == 0 || mystrcmp(recvBuff,"clear") == 0 || mystrcmp(recvBuff,"readahead") == 0 || mystrcmp(recvBuff,"wkld_trace") == 0){
                copy = 1 ;
            }
            //myprintf("%d before netlink %s\n",id,buff);
            if(send_msg_helper(sock_fd, recvBuff, MAX_PAYLOAD_KERNEL) != -1){
                recvmsg(sock_fd, &msg, MAX_PAYLOAD_KERNEL);
                memcpy(buffer,NLMSG_DATA(nlh),MAX_PAYLOAD_KERNEL);
                //myprintf("%d netlink got %s\n",id,buffer);
                //if(copy){
                    buff = buff + strlen(buff);
                    buff += sprintf(buff,"%s ", buffer);
                //}
            }
            free(nlh);
            myprintf("%d netlink response %s\n",id,buffer);
        }

        txfr = MAX_PAYLOAD;
        do{
            n = write(connfd, sendBuff + (MAX_PAYLOAD-txfr), txfr); 
            if(n<0){
                perror("write to socket  error\n");
                myprintf(" ERROR : sending output ret %d\n",n);
                return 1;
            }
            txfr -= n;
        }while(txfr);

        close(connfd);
        fflush(stdout);
        sleep(1);
    }
    
    free(sendBuff);
    free(recvBuff);
    free(buffer);

    close(sock_fd);
    return 0;    
}
