#!/bin/bash


logfiles=("/tmp/glog" "/tmp/hlog" "/tmp/nw" "/tmp/cw" "/tmp/aw" "/tmp/nr" "/tmp/cr" "/tmp/ar")
cmpfiles=("/tmp/nw" "/tmp/aw" "/tmp/nr" "/tmp/ar" "/tmp/cw" "/tmp/aw" "/tmp/cr" "/tmp/ar")

for file in ${logfiles[*]}
do
    echo "rm $file" | bash
done

#dmesg > /tmp/hlog
s=`grep -n "Processing netlink command addcr3" /var/log/syslog | tail -n 1 | cut -d":" -f 1`
e=`wc -l /var/log/syslog | cut -d" " -f1`
let l=e-s
tail -n $l /var/log/syslog > /tmp/hlog
scp -P 2222 sura@localhost:/tmp/a /tmp/glog

grep "nitro WRITE" /tmp/hlog | sed -e 's/.*checksum //g' | sort > /tmp/nw
grep "cerny WRITE" /tmp/hlog | sed -e 's/.*checksum //g' | sort > /tmp/cw
grep "nitro READ" /tmp/hlog | sed -e 's/.*checksum //g' | sort > /tmp/nr
grep "cerny READ" /tmp/hlog | sed -e 's/.*checksum //g' | sort > /tmp/cr

grep -v "bytes" /tmp/glog | grep "w" | tr -s " " | cut -d" " -f 2 | sort > /tmp/aw
grep -v "bytes" /tmp/glog | grep "r" | tr -s " " | cut -d" " -f 2 | sort > /tmp/ar

for file in ${logfiles[*]}
do
    c=`wc -l $file`
    echo "lines $c "
done

index=0
while [ $index -lt ${#cmpfiles[*]} ]
do
    let index2=index+1
    c=`comm -12 ${cmpfiles[$index]} ${cmpfiles[$index2]} | wc -l`
    echo "comm ${cmpfiles[$index]} ${cmpfiles[$index2]}  $c"
    (( index += 2 ))
done

