#include <linux/module.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <linux/skbuff.h>

#include "../include/linux/nitro.h"

extern bool ndbg;

#define MSG_SIZE (1024*10)

struct sock *nl_sk = NULL;

static DEFINE_SPINLOCK(netlink_lock);

static void netlink_nl_recv_msg(struct sk_buff *skb) {
    struct nlmsghdr *nlh;
    int pid,cr3pid;
    struct sk_buff *skb_out;
    int res;
    int code = -1;
    int ret = 0;
    char *bufptr;
    char cmd[100];
    unsigned long cr3;
    unsigned long testinput;
    unsigned long flags;
        
    nlh=(struct nlmsghdr*)skb->data;
    bufptr = (char*)nlmsg_data(nlh);
    pid = nlh->nlmsg_pid; /*pid of sending process */

    //output buffer
    skb_out = nlmsg_new(MSG_SIZE,0);

    if(!skb_out)
    {
        jprinte("ERROR Failed to allocate new skb\n");
        return;
    }

    nlh=nlmsg_put(skb_out,0,0,NLMSG_DONE,MSG_SIZE,0);
    NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */

    jprintk("Received netlink command %s \n", bufptr);
    jprintk("Processing netlink command %s \n", bufptr);
    
    sscanf(bufptr,"%s ", cmd);
    bufptr += strlen(cmd) + 1;

    spin_lock_irqsave(&netlink_lock,flags);
    if(strcmp(cmd,"addcr3") == 0){
        sscanf(bufptr,"%lx %d\n",&cr3,&cr3pid);
        jprintk("%d %s going to add cr3 0x%lx pid %d to monitored processes\n", current->pid, current->comm,cr3,cr3pid);
        ret = add_targeted_process(cr3,0,cr3pid);
    }else if(strcmp(cmd,"delcr3") == 0){
        sscanf(bufptr,"%lx\n",&cr3);
        jprintk("%d %s going to remove cr3 0x%lx to monitored processes\n", current->pid, current->comm,cr3);
        ret = remove_targeted_process(cr3);
    }else if(strcmp(cmd,"settaskptr") == 0){
        jprintk("%d %s going to settaskptr %s\n", current->pid, current->comm,bufptr);
        ret = set_task_ptrptr(bufptr);
    }else if(strcmp(cmd,"setvmmcommbase") == 0){
        jprintk("%d %s going to setvmmcommbase %s\n", current->pid, current->comm,bufptr);
        ret = set_vmmcomm_base(bufptr);
    }else if(strcmp(cmd,"unsetvmmcommbase") == 0){
        jprintk("%d %s going to unsetvmmcommbase\n", current->pid, current->comm);
        ret = unset_vmmcomm_base();
    }else if(strcmp(cmd,"clear")==0){
        jprintk("%d %s going to call clear command\n", current->pid, current->comm);
        ret = clear_command();
    }else if(strcmp(cmd,"status")==0){
        jprintk("%d %s going to call status command\n", current->pid, current->comm);
        ret = status_command(bufptr);
    }else if(strcmp(cmd,"test") == 0){
        jprintk("%d %s going to test %s\n", current->pid, current->comm,bufptr);
        ret = nitro_test(bufptr);
    }
    spin_unlock_irqrestore(&netlink_lock,flags);

    sprintf(nlmsg_data(nlh),"%d\n",ret);

    res=nlmsg_unicast(nl_sk,skb_out,pid);

    if(res<0)
        jprinte("ERROR while sending back to user\n");
}

int netlink_init(void) {
    jprintk("jadukata netlink init\n");
    struct netlink_kernel_cfg cfg = {
        .input = netlink_nl_recv_msg,
        .groups = 3,
    };

    nl_sk=netlink_kernel_create(&init_net, NETLINK_USERSOCK, THIS_MODULE,&cfg);
    if(!nl_sk)
    {
        jprinte("ERROR creating socket.\n");
        return -10;
    }
    return 0;
}

void netlink_exit(void) {
    jprintk("jadukata netlink exit\n");
    netlink_kernel_release(nl_sk);
}

