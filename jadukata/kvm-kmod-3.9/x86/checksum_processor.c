#include "checksum_processor.h"

unsigned long pack_info(int time, int sector,short bigfile){
    unsigned long ret;
    unsigned* reta = (unsigned*)&ret;
    reta[0] = time;
    reta[1] = sector<<1;
    if(bigfile){
        reta[1] |= 0x1;
    }
    return ret;
}

int pack_info_get_jiffies(unsigned long val){
    unsigned* reta = (unsigned*)&val;
    return reta[0];
}

int pack_info_get_sector(unsigned long val){
    unsigned* reta = (unsigned*)&val;
    return reta[1]>>1;
}

short pack_info_get_bigfile(unsigned long val){
    unsigned* reta = (unsigned*)&val;
    return (reta[1] & 0x1);
}

#ifdef JADUKATA_CHECKSUM
#ifdef JADUKATA_VERBOSE
void checksum_processor1(struct pair* pairs, int numpairs)
{
    char codeddata[1800];
    int i,j;
    char* source;
    unsigned int bytes;
    char* dst=codeddata;
    int count=~0;
    int cur = -1;

    for(j=0;j<numpairs;j++){
        source = (char*)pairs[j].addr;
        bytes = pairs[j].size;
        
        if( cur!=~0 ){
            if( ((char)cur == source[0]) ){
                count++;
            }else{
                dst += sprintf(dst,"%dx%u,",count+1,(char)cur);
                count= 0;
            }
        }
        
        for(i=0;i<bytes-1;i++){
            if(source[i] == source[i+1]){
                count++;
            }else {
                dst += sprintf(dst,"%dx%u,",count+1,source[i]);
                count = 0;
            }
            if( (dst-codeddata)>1700 ) {
                count = -1;
                dst += sprintf(dst,"...\n");
                break;
            }
        }
        cur = source[bytes-1];
    }

    bytes= (dst-codeddata);
    dst = codeddata;
    for(i=0;i<=(bytes/100);i++){
        jprintk("nitro coded %d %*s\n",i,(i==(bytes/100))?bytes-(i*100):100,dst);
        dst += 100;
    }
}
#endif

extern int process_checksum(void* pargs, int chunknum, unsigned long checksum);

unsigned long custom_checksum(char* ptr, int size,unsigned long csumpartial){
    //jprintk("ptr %lx size %d csumpartial %lx \n", ptr, size, csumpartial);
    return csum_partial(ptr,size,csumpartial);
}

int checksum_processor2(void* pargs, struct pair* pairs, int numpairs)
{
    int j; 
    unsigned bytes;
    int remaining = 0;
    unsigned long checksum = 0;
    int chunknum = 0;
    char* source;
    int ret = 0;

    for(j=0;j<numpairs;j++){
        source = (char*)pairs[j].addr;
        bytes = pairs[j].size;
        if(remaining < 0){
            if( (bytes + remaining) >=0 ){
                checksum = custom_checksum((char*)(source),-remaining,checksum);
                source += (-remaining);
                ret += process_checksum(pargs, chunknum++,checksum);
                checksum = 0;
            }else{
                checksum = custom_checksum((char*)(source),bytes,checksum);
                source += bytes;
            }
        }
        remaining = bytes+remaining;
        while(remaining>=512){
            checksum = custom_checksum((char*)(source),512,0);
            source += 512;
            ret += process_checksum(pargs, chunknum++,checksum);
            checksum = 0;
            remaining -= 512;
        }
        if(remaining > 0){
            checksum = custom_checksum((char*)(source),remaining,0);
            source += remaining;
            remaining -= 512;
        }
    }
    return ret;
}
#endif
