/*
 * list of nitro monitored syscalls for linux 
 *
#define __NR_read 0
#define __NR_write 1
#define __NR_open 2
#define __NR_close 3

#define __NR_stat 4
#define __NR_fstat 5
#define __NR_lstat 6
#define __NR_lseek 8

#define __NR_mmap 9
#define __NR_munmap 11
#define __NR_pread64 17
#define __NR_pwrite64 18
#define __NR_readv 19
#define __NR_writev 20

#define __NR_msync 26
#define __NR_madvise 28

#define __NR_clone 56
#define __NR_fork 57
#define __NR_vfork 58
#define __NR_exit 60
#define __NR_kill 62

#define __NR_fsync 74
#define __NR_fdatasync 75
#define __NR_creat 85
#define __NR_sync 162

#define __NR_io_setup 206
#define __NR_io_destroy 207
#define __NR_io_getevents 208
#define __NR_io_submit 209
#define __NR_io_cancel 210

#define __NR_remap_file_pages 216
#define __NR_sync_file_range 277

#define __NR_preadv 295
#define __NR_pwritev 296

#define __NR_syncfs 306

arguments order :

c version        %rdi, %rsi, %rdx, %rcx, %r8, %r9
syscall version  %rdi, %rsi, %rdx, %r10, %r8, %r9 

 */
#ifdef HANDLERS
static int (*const HNAME(handlers)[])(char prefix, struct kvm_vcpu* vcpu, struct nitrodatau *nu) = {
#else
char* handler_names[] = {
#undef HNAME
#define HNAME(x) #x
#endif
    [0] = HNAME(read), 
    [1] = HNAME(write),
    [2] = HNAME(open),
    [3] = HNAME(close),
    [4] = HNAME(stat), 
    [5] = HNAME(fstat), 
    [6] = HNAME(lstat), 
    [8] = HNAME(lseek), 
    [9] = HNAME(mmap), 
    [11] = HNAME(munmap),
    [17] = HNAME(pread64), 
    [18] = HNAME(pwrite64), 
    [19] = HNAME(readv), 
    [20] = HNAME(writev), 
    [26] = HNAME(msync), 
    [28] = HNAME(madvise), 
    [56] = HNAME(clone),
    [57] = HNAME(fork),
    [58] = HNAME(vfork),
    [60] = HNAME(exit),
    [62] = HNAME(kill),
    [74] = HNAME(fsync), 
    [75] = HNAME(fdatasync), 
    [85] = HNAME(creat), 
    [162] = HNAME(sync), 
    [206] = HNAME(io_setup),
    [207] = HNAME(io_destroy), 
    [208] = HNAME(io_getevents),
    [209] = HNAME(io_submit),
    [210] = HNAME(io_cancel), 
    [216] = HNAME(remap_file_pages), 
    [277] = HNAME(sync_file_range), 
    [295] = HNAME(preadv), 
    [296] = HNAME(pwritev), 
    [306] = HNAME(syncfs), 
};

#ifndef HANDLERS
const int MAX_SYSCALLS = ARRAY_SIZE(handler_names);
char* syscall_name(int scallnr){
    if(scallnr < MAX_SYSCALLS){
        return handler_names[scallnr];
    }
    return "UNKNOWN";
}
#undef HNAME
#endif
