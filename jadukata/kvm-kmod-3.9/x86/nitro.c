#include <linux/nitro.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <asm/io.h>
#include <asm/checksum.h>
#include "kvm_cache_regs.h"
#include "x86.h"
#include "mmu.h"
#include "tss.h"

#ifdef JADUKATA
extern bool ndbg;

#define NO_SCALL (0xfffffff) // no syscall yet
#define UNMON_SCALL (0xffffffe) //denotes unmonitored scall

#undef HANDLERS
#include "handlers.h"

#ifdef JADUKATA_CHECKSUM
extern hash_table *ht_data_hpas;
#endif
//mmap
hash_table *ht_mmap_process = NULL;
hash_table *ht_mmap_gpas = NULL;
hash_table *ht_mmap_gpas_ptep = NULL;
hash_table *ht_rw_gpas = NULL;
hash_table *ht_rw_gpas_ptep = NULL;
hash_table *ht_tracked_gpas = NULL;
hash_table *ht_tracked_gfns = NULL;
hash_table *ht_new_pids = NULL;
hash_table *ht_file_sizes = NULL;
hash_table *ht_file_names = NULL;
hash_table *ht_file_chunksums = NULL;

struct kmem_cache *nucache = NULL;

//to store split system call procedures in guest context switch
hash_table *ht_splitcalls = NULL;

#define emul_to_vcpu(ctxt) container_of(ctxt, struct kvm_vcpu, arch.emulate_ctxt)

hash_table *ht_mon_process = NULL;
hash_table *ht_mon_pids = NULL;
unsigned long process_cr3s_bloom = 0;

#define ones_match(val,bloom) ((~(val & bloom) & val) == 0)

unsigned long monitored_syscalls[] = {0,1,2,3,4,5,6,8,9,11,17,18,19,20,26,28,56,57,58,60,62,74,75,85,162,206,207,208,209,210,216,277,295,296,306};

#define NR_SYSCALLS (ARRAY_SIZE(monitored_syscalls))
unsigned long syscall_bloom = 0;

void recalc_syscall_bloom(void){
    int i =0;
    syscall_bloom = 0;
    for(i=0;i<NR_SYSCALLS;i++){
        syscall_bloom |= monitored_syscalls[i];
    }
} 

/*
extern int is_sysenter_sysreturn(struct kvm_vcpu *vcpu);
extern int is_int(struct kvm_vcpu *vcpu);
*/

#ifdef NO_ON_OFF
int start_nitro(struct kvm_vcpu *vcpu, unsigned long);
int stop_nitro(struct kvm_vcpu *vcpu, unsigned long);
#endif

void nu_init(struct nitrodatau *nu){
    memset(nu,0,sizeof(struct nitrodatau));
    nu->out_syscall = NO_SCALL;
}

void nitro_vcpu_init(struct kvm_vcpu *vcpu){
    nu_init(&vcpu->nitrodata.nu);
}

int nitro_kvm_init(struct kvm_vcpu *vcpu){
    int i;
	vcpu->nitrodata.running = 0;
    vcpu->nitrodata.task_ptrptr = 0;
	vcpu->nitrodata.id[0] = '\0';
	vcpu->nitrodata.sysenter_cs_val = 0;
	vcpu->nitrodata.efer_val = 0;
	vcpu->nitrodata.idt_int_offset = 0;
	vcpu->nitrodata.idt_replaced_offset = 0;
	//vcpu->nitrodata.pae = 0;
	vcpu->nitrodata.mode = UNDEF;
	vcpu->nitrodata.idt_entry_size = 0;
	vcpu->nitrodata.no_int = 0;
	vcpu->nitrodata.syscall_reg = VCPU_REGS_RAX;
    vcpu->nitrodata.scall_count = 0;
    vcpu->nitrodata.sret_count = 0;
    vcpu->nitrodata.scall_time = 0;
    vcpu->nitrodata.sret_time = 0;
    vcpu->nitrodata.pnetlink = 0;
#ifdef SHADOW_IDT
	vcpu->nitrodata.shadow_idt.base = 0;
	vcpu->nitrodata.shadow_idt.limit = 0;
	vcpu->nitrodata.shadow_idt.table = 0;
#endif
    nitro_vcpu_init(vcpu);

	return 0;
}

int nitro_kvm_exit(struct kvm_vcpu *vcpu){

#ifdef SHADOW_IDT
	if(vcpu->nitrodata.shadow_idt.table != 0){
		kfree(vcpu->nitrodata.shadow_idt.table);
	}
#endif

	return 0;
}

int nitro_mod_init(void){
    process_cr3s_bloom = 0;
    recalc_syscall_bloom();

    hash_init();

    if(ht_mmap_process == NULL){
        ht_create_with_size(&ht_mmap_process,"mmap_process",10);
    }
    if(ht_mon_process == NULL){
        ht_create_with_size(&ht_mon_process,"mon_process",10);
    }
    if(ht_mon_pids == NULL){
        ht_create_with_size(&ht_mon_pids,"mon_pids",10);
    }
    if(ht_mmap_gpas == NULL){
        ht_create_with_size(&ht_mmap_gpas,"mmap_gpas",1000);
    }
    if(ht_mmap_gpas_ptep == NULL){
        ht_create_with_size(&ht_mmap_gpas_ptep,"mmap_gpas_ptep",1000);
    }
    if(ht_rw_gpas == NULL){
        ht_create_with_size(&ht_rw_gpas,"rw_gpas",1000);
    }
    if(ht_rw_gpas_ptep == NULL){
        ht_create_with_size(&ht_rw_gpas_ptep,"rw_gpas_ptep",1000);
    }
    if(ht_tracked_gpas == NULL){
        ht_create_with_size(&ht_tracked_gpas,"tracked_gpas",1000);
    }
    if(ht_tracked_gfns == NULL){
        ht_create_with_size(&ht_tracked_gfns,"tracked_gfns",1000);
    }
    if(ht_new_pids == NULL){
        ht_create_with_size(&ht_new_pids,"newpids",100);
    }
    if(ht_file_sizes == NULL){
        ht_create_with_size(&ht_file_sizes,"fsizes",100);
    }
    if(ht_file_names == NULL){
        ht_create_with_size(&ht_file_names,"fnames",100);
    }
    if(ht_file_chunksums == NULL){
        ht_create_with_size(&ht_file_chunksums,"fchksums",1000);
    }
    if(ht_splitcalls == NULL){
        ht_create_with_size(&ht_splitcalls,"splitcalls",10);
    }
    if(nucache == NULL){
        nucache = kmem_cache_create("nucache",sizeof(struct nitrodatau),0,SLAB_RED_ZONE, NULL);
        if(!nucache){
            panic("ERROR : could not allocate nitrodata kmem cache alloc \n");
        }
    }

	return 0;
}

int nitro_mod_exit(void){

    if(ht_mmap_process != NULL){
        ht_destroy(ht_mmap_process);
        ht_mmap_process = NULL;
    }
    if(ht_mon_process != NULL){
        ht_destroy(ht_mon_process);
        ht_mon_process = NULL;
    }
    if(ht_mon_pids != NULL){
        ht_destroy(ht_mon_pids);
        ht_mon_pids = NULL;
    }
    if(ht_mmap_gpas != NULL){
        ht_destroy(ht_mmap_gpas);
        ht_mmap_gpas = NULL;
    }
    if(ht_mmap_gpas_ptep != NULL){
        ht_destroy(ht_mmap_gpas_ptep);
        ht_mmap_gpas_ptep = NULL;
    }
    if(ht_rw_gpas != NULL){
        ht_destroy(ht_rw_gpas);
        ht_rw_gpas = NULL;
    }
    if(ht_rw_gpas_ptep != NULL){
        ht_destroy(ht_rw_gpas_ptep);
        ht_rw_gpas_ptep = NULL;
    }
    if(ht_tracked_gpas != NULL){
        ht_destroy(ht_tracked_gpas);
        ht_tracked_gpas = NULL;
    }
    if(ht_tracked_gfns != NULL){
        ht_destroy(ht_tracked_gfns);
        ht_tracked_gfns = NULL;
    }
    if(ht_new_pids != NULL){
        ht_destroy(ht_new_pids);
        ht_new_pids = NULL;
    }
    if(ht_file_sizes != NULL){
        ht_destroy(ht_file_sizes);
        ht_file_sizes = NULL;
    }
    if(ht_file_names != NULL){
        unsigned long hash,path;
        while(ht_pop_val(ht_file_names,&hash,&path) != -1){
            vfree((void*)path);
        }
        ht_destroy(ht_file_names);
        ht_file_names = NULL;
    }
    if(ht_file_chunksums != NULL){
        ht_destroy(ht_file_chunksums);
        ht_file_chunksums = NULL;
    }
    if(ht_splitcalls != NULL){
        unsigned long tid;
        struct nitrodatau *nu = NULL;
        while(ht_pop_val(ht_splitcalls,&tid, (unsigned long*)&nu) != -1){
            jprinte("ERROR unmatched syscall id %lx syscall %ld \n", tid, nu, nu->out_syscall);
            kmem_cache_free(nucache,nu);
        }
        ht_destroy(ht_splitcalls);
        ht_splitcalls = NULL;
    }
    
    if(nucache != NULL){
       kmem_cache_destroy(nucache);
       nucache = NULL;
    }

    hash_cleanup();

	return 0;
}

static DEFINE_SPINLOCK(nitro_lock);

struct mylock{
    spinlock_t* lock;
    unsigned long* flags;
    int op;
};

long my_helper(void* vm){
    struct mylock* m = (struct mylock*)vm;
    if(m->op==1){
        spin_lock_irqsave(m->lock,(*(m->flags)));
    }else if(m->op ==2){
        spin_unlock_irqrestore(m->lock,(*(m->flags)));
    }else{
        return -1;
    }
    return 0;
}

void my_lock(spinlock_t* lock,unsigned long* flags){
    struct mylock l;
    l.lock = lock;
    l.flags = flags;
    l.op = 1;
    my_helper(&l);
}

void my_unlock(spinlock_t* lock,unsigned long* flags){
    struct mylock l;
    l.lock = lock;
    l.flags = flags;
    l.op = 2;
    my_helper(&l);
}

/*
int start_nitro_one(struct kvm_vcpu* vcpu){
    int ret = 0;
    unsigned long flags;

    my_lock(&nitro_lock,&flags);
    ret = start_nitro(vcpu);
    my_unlock(&nitro_lock,&flags);

    return ret;
}

int stop_nitro_one(struct kvm_vcpu* vcpu){
    int ret = 0;
    unsigned long flags;

    my_lock(&nitro_lock,&flags);
    ret = stop_nitro(vcpu);
    my_unlock(&nitro_lock,&flags);

    return ret;
}
*/

int start_nitro(struct kvm_vcpu *vcpu, unsigned long newcr3){
	struct kvm_sregs sregs;
	//u8 *idt;
	//u64 idt_base,efer;
	//u32 error;
	//unsigned long cr4,cr0;
    //struct x86_exception exception;
    int64_t idt_index = 128;

    //tracing_on();
    if(vcpu->nitrodata.running == 1)
        return 0;
    
    jprintk("starting nitro vcpuid %d currentcr3 %lx newcr3 %lx CPU %d \n",vcpu->vcpu_id, vcpu->arch.cr3, newcr3, smp_processor_id());

	//printk( KERN_ERR "%d %s starting nitro on CPU %d vcpuid %d cr3 %lx\n", current->pid, current->comm, smp_processor_id(), vcpu->vcpu_id, newcr3);

	if(!is_protmode(vcpu)){
	//	printk( KERN_ERR "kvm:start_syscall_trace: ERROR: guest is running in real mode, nitro can not function.\n");
		return 3;
	}

	vcpu->nitrodata.mode = PROT;
	vcpu->nitrodata.idt_entry_size=8;

	if(is_pae(vcpu)){
		vcpu->nitrodata.mode = PAE;
		//printk( KERN_ERR "kvm:start_syscall_trace: system running in PAE mode\n");
	}
	if(is_long_mode(vcpu)){
		vcpu->nitrodata.mode = LONG;
		vcpu->nitrodata.idt_entry_size=16;
		//printk( KERN_ERR "kvm:start_syscall_trace: system running in long mode (x86_64)\n");
	}

	//vcpu_load(vcpu);
	kvm_arch_vcpu_ioctl_get_sregs(vcpu,&sregs);
	//vcpu_put(vcpu);

	if(idt_index == 0){
		vcpu->nitrodata.no_int = 1;
	}
	else if(idt_index<32 || idt_index>(sregs.idt.limit+1)/vcpu->nitrodata.idt_entry_size){
		jprinte("kvm:start_syscall_trace: ERROR: invalid idt_index passed, start will be aborted.\n");
		return 2;
	}
	else{
		vcpu->nitrodata.idt_int_offset = (u8) idt_index;
	}

	// set syscall_reg
    vcpu->nitrodata.syscall_reg = VCPU_REGS_RAX;
	//printk( KERN_ERR "kvm:start_syscall_trace: starting syscall trace with syscall_reg = %d, name='%s'\n", vcpu->nitrodata.syscall_reg, syscall_reg);

    vcpu->nitrodata.running = 1;
    //printk( KERN_ERR "kvm:nitro enabled on %d cpus.\n",vcpu->nitrodata.running);

/*
	//code to set #GP trap
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
	//kvm_for_each_vcpu(i, vcpu, kvm){
	//	vcpu_load(vcpu);
	//	kvm_x86_ops->set_gp_trap(vcpu);
	//	printk( KERN_ERR "kvm:start_syscall_trace: cpu%d: GP trap set\n",i);
	//	vcpu_put(vcpu);

		//i++;
	//}


	//code to cause sysenter to cause #GP
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
	kvm_for_each_vcpu(i, vcpu, kvm){
		vcpu_load(vcpu);
		kvm_x86_ops->get_msr(vcpu, MSR_IA32_SYSENTER_CS, &(vcpu->nitrodata.sysenter_cs_val));
		kvm_x86_ops->set_msr(vcpu, MSR_IA32_SYSENTER_CS, 0);
		vcpu_put(vcpu);

		//i++;
	}
*/

	//code to cause syscall to cause #UD (64 bit ubuntu)
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
    

	//kvm_for_each_vcpu(i, vcpu, kvm){
	//	vcpu_load(vcpu);
		kvm_get_msr_common(vcpu, MSR_EFER, &(vcpu->nitrodata.efer_val));
        struct msr_data efer_info;
	    efer_info.host_initiated = true;
        efer_info.index = MSR_EFER;
        efer_info.data = (vcpu->nitrodata.efer_val & ~EFER_SCE);
		kvm_set_msr_common(vcpu, &efer_info);
	//	vcpu_put(vcpu);
	//}

/*

#ifdef SHADOW_IDT
	//extern int emulator_read_emulated(unsigned long addr, void *val, unsigned int bytes, unsigned int *error_code, struct kvm_vcpu *vcpu)

	if(!vcpu->nitrodata.no_int){
		kvm_for_each_vcpu(i, vcpu, kvm){
			kvm_arch_vcpu_ioctl_get_sregs(vcpu,&sregs);
			if(vcpu->nitrodata.shadow_idt.base == 0){
				vcpu->nitrodata.shadow_idt.base = sregs.idt.base;
				vcpu->nitrodata.shadow_idt.limit = sregs.idt.limit;
				vcpu->nitrodata.shadow_idt.table = kmalloc(sregs.idt.limit + 1,GFP_KERNEL);
				memset(vcpu->nitrodata.shadow_idt.table,0,sregs.idt.limit + 1);
				kvm_read_guest_virt_system(sregs.idt.base,vcpu->nitrodata.shadow_idt.table,(unsigned int)(sregs.idt.limit + 1),vcpu,&error);
			}
			sregs.idt.limit=32*vcpu->nitrodata.idt_entry_size-1;
			kvm_arch_vcpu_ioctl_set_sregs(vcpu,&sregs);
		}
	}
#else

	//old code to cause int x to cause #GP/#NP
	//i=0;

	idt_base = 0;

	if(!vcpu->nitrodata.no_int){

		//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
		kvm_for_each_vcpu(i, vcpu, kvm){
			kvm_arch_vcpu_ioctl_get_sregs(vcpu,&sregs);

			if(sregs.idt.base != idt_base){
				idt_base = sregs.idt.base;

				idt = kmalloc(sregs.idt.limit + 1,GFP_KERNEL);
				memset(idt,0,sregs.idt.limit + 1);
				//kvm_read_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit + 1));
				kvm_read_guest_virt(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				//printk( KERN_ERR "kvm:start_syscall_trace: idt size: %d\n", (unsigned int)(sregs.idt.limit + 1));

				vcpu->nitrodata.idt_replaced_offset = 0x81;

				//for(j=32;j<(sregs.idt.limit + 1)/vcpu->nitrodata.idt_entry_size;j++){
				for(j=((sregs.idt.limit + 1)/vcpu->nitrodata.idt_entry_size) - 1; j>=32;j--){
					//printk( KERN_ERR "kvm:start_syscall_trace: checking IDT gate 0x%hX, p=0x%X, seg. sel.=%hu\n",j,(idt[(j*vcpu->nitrodata.idt_entry_size)+5] & 0x80),*((u16*) (idt +  (INT_OFFSET*vcpu->nitrodata.idt_entry_size) + 2)));
					if((idt[(j*vcpu->nitrodata.idt_entry_size)+5] & 0x80) == 0){
						vcpu->nitrodata.idt_replaced_offset = (u8)j;
						break;
					}
				}

				printk( KERN_ERR "kvm:start_syscall_trace: using empty gate 0x%hX\n",vcpu->nitrodata.idt_replaced_offset);

				memcpy(idt + (vcpu->nitrodata.idt_replaced_offset*vcpu->nitrodata.idt_entry_size), idt + (vcpu->nitrodata.idt_int_offset*vcpu->nitrodata.idt_entry_size), vcpu->nitrodata.idt_entry_size);

				*((u16*) (idt +  (vcpu->nitrodata.idt_int_offset*vcpu->nitrodata.idt_entry_size) + 2)) = DUM_SEG_SELECT;  //set selector
				//idt[(INT_OFFSET*vcpu->nitrodata.idt_entry_size) + 5] &= 0x7F;  //unset present bit

				//kvm_write_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit));
				kvm_write_guest_virt_system(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				kfree(idt);
			}
		}

	}
#endif
    */

	return 0;
}

int stop_nitro(struct kvm_vcpu* vcpu, unsigned long newcr3){
	//struct kvm_sregs sregs;
	//u8 *idt;
	//u64 idt_base;
	//u32 error;
    //struct x86_exception exception;
    
    //tracing_off();

	if(!vcpu->nitrodata.running){
		//printk( KERN_ERR "kvm:stop_syscall_trace: WARNING: nitro is not started, stop will be aborted.\n");
		return 0;
	}
    
    jprintk("stopping nitro vcpuid %d currentcr3 %lx newcr3 %lx CPU %d \n",vcpu->vcpu_id, vcpu->arch.cr3, newcr3, smp_processor_id());
	
    //printk( KERN_ERR "%d %s stopping nitro on CPU %d vcpuid %d cr3 %lx\n", current->pid, current->comm, smp_processor_id(), vcpu->vcpu_id, newcr3);
/*
	//code to unset #GP trap
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
	//	vcpu_load(kvm->vcpus[i]);
	//	kvm_x86_ops->unset_gp_trap(kvm->vcpus[i]);
	//	printk( KERN_ERR "kvm:start_syscall_trace: cpu%d: GP trap unset\n",i);
	//	vcpu_put(kvm->vcpus[i]);

	//	i++;
	//}


	//code to cause sysenter not to cause #GP
	i=0;
	while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
		vcpu_load(kvm->vcpus[i]);
		kvm_x86_ops->set_msr(kvm->vcpus[i], MSR_IA32_SYSENTER_CS, vcpu->nitrodata.sysenter_cs_val);
		vcpu_put(kvm->vcpus[i]);

		i++;
	}
*/

	//code to cause syscall not to cause #UD (64 bit ubuntu)
	//kvm_for_each_vcpu(i, vcpu, kvm){
	//	vcpu_load(vcpu);
        struct msr_data efer_info;
	    efer_info.host_initiated = true;
        efer_info.index = MSR_EFER;
        efer_info.data = (vcpu->nitrodata.efer_val);
		kvm_set_msr_common(vcpu, &efer_info);
	//	vcpu_put(vcpu);
	//}
	
    
    vcpu->nitrodata.running = 0;

    

/*
#ifdef SHADOW_IDT

#else

	//old code to cause int x not to cause #GP/#NP

	i=0;
	idt_base = 0;

	if(!vcpu->nitrodata.no_int){

		while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
			kvm_arch_vcpu_ioctl_get_sregs(kvm->vcpus[i],&sregs);

			if(sregs.idt.base != idt_base){
				idt_base = sregs.idt.base;

				idt = kmalloc(sregs.idt.limit + 1,GFP_KERNEL);
				memset(idt,0,sregs.idt.limit + 1);
				//kvm_read_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit + 1));
				kvm_read_guest_virt(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				memcpy(idt + (vcpu->nitrodata.idt_int_offset*vcpu->nitrodata.idt_entry_size), idt + (vcpu->nitrodata.idt_replaced_offset*vcpu->nitrodata.idt_entry_size), vcpu->nitrodata.idt_entry_size);

				//kvm_write_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit));
				kvm_write_guest_virt_system(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				kfree(idt);
			}

			i++;
		}

	}
#endif
*/

	return 0;
}

int start_nitro_all(void){
    struct kvm* kvm;
    struct kvm_vcpu* vcpu;
    int ret = 0,i;
    unsigned long flags;

    list_for_each_entry(kvm, &vm_list, vm_list){
        my_lock(&nitro_lock,&flags);
	    kvm_for_each_vcpu(i, vcpu, kvm){
            ret |= start_nitro(vcpu,0);
        }
        my_unlock(&nitro_lock,&flags);
    }
    
    return ret;
}

int stop_nitro_all(void){
    struct kvm* kvm;
    struct kvm_vcpu* vcpu;
    int ret = 0,i;
    unsigned long flags;

    list_for_each_entry(kvm, &vm_list, vm_list){
        my_lock(&nitro_lock,&flags);
        kvm_for_each_vcpu(i, vcpu, kvm){
            ret |= stop_nitro(vcpu,0);
        }
        my_unlock(&nitro_lock,&flags);
    }

    return ret;
}

int kvm_read_guest_virt_system(struct x86_emulate_ctxt *ctxt, gva_t addr, void *val, unsigned int bytes,struct x86_exception *exception);

int get_guest_pid(struct kvm_vcpu* vcpu){
    struct x86_exception ex;
    unsigned long taskptr;
    int pid = -1;
    if(vcpu->nitrodata.task_ptrptr){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, vcpu->nitrodata.task_ptrptr, &taskptr, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + 676, &pid, sizeof(int),&ex);
    }
    return pid;
}

void get_guest_comm(struct kvm_vcpu* vcpu, char* comm){
    struct x86_exception ex;
    unsigned long taskptr;
    if(vcpu->nitrodata.task_ptrptr){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, vcpu->nitrodata.task_ptrptr, &taskptr, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + 1104, comm, sizeof(char)*16,&ex);
    }else{
        comm[0] = '\0';
    }
}

void print_all_guest_pids(struct kvm_vcpu* vcpu){
    int i;
    int pid;
    char comm[64];
    unsigned long taskptr;
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    struct x86_exception ex;
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            if(tvcpu->nitrodata.task_ptrptr){
                kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, tvcpu->nitrodata.task_ptrptr, &taskptr, sizeof(unsigned long),&ex);
                kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + 676, &pid, sizeof(int),&ex);
	            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr+1104, comm, sizeof(char)*16,&ex);
                jprintk("guest pid vcpu %d taskptrptr %lx taskptr %lx pid %d comm %s \n",tvcpu->vcpu_id,tvcpu->nitrodata.task_ptrptr,taskptr,pid,comm);
            }
        }
    }
}

void get_process_hardware_id(struct kvm_vcpu *vcpu, unsigned long *cr3, u32 *verifier, unsigned long *pde){
	unsigned long dir_base;
	u32 i;
	u32 pde_32;

	*verifier = 0;
	*cr3 = vcpu->arch.cr3;

	if (vcpu->nitrodata.mode == PAE){//PAE
		dir_base = (*cr3) & 0xFFFFFFFFFFFFFFE0;	//see section 4.3 in intel manual

		for (i=0;i<4*8;i+=8){
			kvm_read_guest(vcpu->kvm, dir_base+i, pde, 8);
			//printk( KERN_ERR "kvm:handle_gp: kvm_read_guest_virt_system error: %u\n",error);
			if(((*pde) & PT_PRESENT_MASK)){//  &&  !(pde & PT_WRITABLE_MASK)){
				*verifier=i;
				goto FOUND;
			}
		}
	}
	else if (vcpu->nitrodata.mode == LONG){//IA-32E
		dir_base = (*cr3) & 0x000FFFFFFFFFF000;	//see section 4.3 in intel manual

		for (i=0;i<512*8;i+=8){
			kvm_read_guest(vcpu->kvm, dir_base+i, pde, 8);
			//printk( KERN_ERR "kvm:handle_gp: kvm_read_guest_virt_system error: %u\n",error);
			if(((*pde) & PT_PRESENT_MASK)){//  &&  !(pde & PT_WRITABLE_MASK)){
				*verifier=i;
				goto FOUND;
			}
		}
	}
	else{//32-bit Protected
		dir_base = (*cr3) & 0xFFFFFFFFFFFFF000;  	//see section 4.3 in intel manual

		for (i=0;i<1024*4;i+=4){
			kvm_read_guest(vcpu->kvm, dir_base+i, &pde_32, 4);
			//printk( KERN_ERR "kvm:handle_gp: kvm_read_guest_virt_system error: %u\n",error);
			if((pde_32 & PT_PRESENT_MASK)){//  &&  !(pde & PT_WRITABLE_MASK)){
				*verifier=i;
				*pde = (unsigned long)pde_32;
				goto FOUND;
			}
		}
	}
	*pde=0;

FOUND:
	/* end copy and paste */
	return;
}

/*
int handle_gp(struct kvm_vcpu *vcpu, struct kvm_run *kvm_run){
	int er;

	printk( KERN_ERR "kvm:handle_gp: #GP trapped\n");

	if(!nitrodata.running)
		return 1;

	if(is_sysenter_sysreturn(vcpu)){//sysenter/sysreturn
		print_trace_proxy('f',vcpu);
		er = emulate_instruction(vcpu, 0, 0, 0);
		if (er != EMULATE_DONE){
			kvm_clear_exception_queue(vcpu);
			kvm_clear_interrupt_queue(vcpu);
			kvm_queue_exception_e(vcpu,GP_VECTOR,kvm_run->ex.error_code);
		}
	}
#ifdef SHADOW_IDT
	else if(vcpu->arch.interrupt.pending && vcpu->arch.interrupt.nr > 31){//int shadow_idt
		printk( KERN_ERR "trapped int 0x%X\n", vcpu->arch.interrupt.nr);
		DEBUG_PRINT("begin_int_handling: EIP is now 0x%08lX.\n", kvm_register_read(vcpu, VCPU_REGS_RIP))
		if (is_int(vcpu)) {
			er = emulate_instruction(vcpu, 0, 0, 0);
		} else {
			DEBUG_PRINT("Asynchronous interrupt detected.\n")
			er = handle_asynchronous_interrupt(vcpu);
		}
		kvm_clear_exception_queue(vcpu);
		kvm_clear_interrupt_queue(vcpu);
		if (er != EMULATE_DONE) {
			kvm_queue_exception_e(vcpu, GP_VECTOR, kvm_run->ex.error_code);
		}
	}
#else
	else if(((DUM_SEG_SELECT & 0xFFF8) == (kvm_run->ex.error_code & 0xFFF8)) && !nitrodata.no_int){ //int no shadow_idt
																		 //check if its our expected error code for int handling
																	     //(disregard bottom 3 bits as these are status)

		print_trace_proxy('i',vcpu);

		kvm_clear_exception_queue(vcpu);
		kvm_clear_interrupt_queue(vcpu);
		kvm_queue_interrupt(vcpu,nitrodata.idt_replaced_offset,true);
	}
#endif
	else{
		//printk( KERN_ERR "kvm:handle_gp: natural #GP trapped, EC=%u\n",kvm_run->ex.error_code);
		kvm_clear_exception_queue(vcpu);
		kvm_clear_interrupt_queue(vcpu);
		kvm_queue_exception_e(vcpu,GP_VECTOR,kvm_run->ex.error_code);
	}
	return 1;
}
EXPORT_SYMBOL(handle_gp);
*/

extern bool rmap_write_protect(struct kvm *kvm, u64 gfn);

extern void jadu_write_protect(struct kvm_vcpu *vcpu, gpa_t gpa);

unsigned long taddr = 0, tcr3 = 0, tpa = 0;

/*
int nitro_test_helper(struct kvm_vcpu* vcpu){
    gpa_t pa;
    gfn_t fn;
    struct x86_exception ex;
    unsigned long val;
    //struct kvm_vcpu* vcpu;
    //unsigned long flags;
    leo_debug =1 ;
    if(tcr3 > 1 && vcpu->arch.cr3 == tcr3){ 
	    u32 access = (kvm_x86_ops->get_cpl(vcpu) == 3) ? PFERR_USER_MASK : 0;
		pa = vcpu->arch.walk_mmu->gva_to_gpa(vcpu, taddr, access,&ex);
        tpa = pa;
        fn = pa >> PAGE_SHIFT;
        kvm_read_guest_virt(&vcpu->arch.emulate_ctxt, taddr, &val, sizeof(unsigned long),&ex);
        printk( KERN_ERR "fn test input tcr3 %lx 0x%lx pa %lx fn %lx val %lx \n " , tcr3, taddr, pa , fn, val);
        jadu_write_protect(vcpu, pa);
        tcr3 = 1;
    }
    leo_debug = 0;
    return 0;
}

int nitro_test_helper1(struct kvm_vcpu* vcpu, unsigned long pa){
    gfn_t fn = pa >> PAGE_SHIFT;
    gfn_t tfn = tpa >> PAGE_SHIFT;
    if(leo_debug) printk( KERN_ERR " test_helper %s %s %d : 1 pa %lx tpa %lx fn %lx tfn %lx \n",  __FILE__ , __FUNCTION__ ,__LINE__,pa,tpa,fn,tfn);
    if(tcr3 == 1){
        if(leo_debug) printk( KERN_ERR "%s %s %d : 2 pa %lx tpa %lx fn %lx tfn %lx \n",  __FILE__ , __FUNCTION__ ,__LINE__,pa,tpa,fn,tfn);
        if(fn == tfn){ 
            printk( KERN_ERR "fn test helper trapped write protection taddr %lx  va %lx \n" , tpa, pa);
        }
        //printk( KERN_ERR "fn test helper trapped write protection taddr %lx  va %lx \n" , taddr, va);
        tcr3 = 0;
    }
    return 0;
}
*/

int nitro_test(char* input){
    jprintk("fn nitro_test %s \n ", input);
    sscanf(input, "%lx %lx\n",&tcr3,&taddr);
    //add_targeted_process(tcr3,0,0);
    return 0;
}

int status_command(void* buf){
    //TODO: print the status
    char* data = (char*)buf;
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i;
    unsigned long total_scall=0, total_sret=0, total_scall_time, total_sret_time;
    list_for_each_entry(kvm, &vm_list, vm_list){
        total_scall = 0, total_sret = 0, total_scall_time = 0, total_sret_time = 0;
        kvm_for_each_vcpu(i, tvcpu, kvm){
            data += sprintf(data,"syscall/sysret counts vcpu %d count scall - %lu, sret - %lu time scall - %lu sret - %lu \n", tvcpu->vcpu_id, tvcpu->nitrodata.scall_count, tvcpu->nitrodata.sret_count, tvcpu->nitrodata.scall_time, tvcpu->nitrodata.sret_time);
            total_scall += tvcpu->nitrodata.scall_count;
            total_sret += tvcpu->nitrodata.sret_count;
            total_scall_time += tvcpu->nitrodata.scall_time;
            total_sret_time += tvcpu->nitrodata.sret_time;
        }
        data += sprintf(data,"total syscall/sysret counts %lu %lu time %lu %lu\n", total_scall, total_sret, total_scall_time, total_sret_time);
    }

    jprinte("status command: %s \n",buf);
    buf = data;

    data += sprintf(data,"mmap process size %d\n", ht_get_size(ht_mmap_process));
    data += sprintf(data,"mon process size %d\n", ht_get_size(ht_mon_process));
    data += sprintf(data,"mon pids size %d\n", ht_get_size(ht_mon_pids));
    data += sprintf(data,"mmap gpas size %d\n", ht_get_size(ht_mmap_gpas));
    data += sprintf(data,"mmap gpas ptep size %d\n", ht_get_size(ht_mmap_gpas_ptep));
    data += sprintf(data,"rw gpas size %d\n", ht_get_size(ht_rw_gpas));
    data += sprintf(data,"rw gpas ptep size %d\n", ht_get_size(ht_rw_gpas_ptep));
    data += sprintf(data,"tracked gpas size %d\n", ht_get_size(ht_tracked_gpas));
    data += sprintf(data,"tracked gfns size %d\n", ht_get_size(ht_tracked_gfns));
    data += sprintf(data,"new pids size %d\n", ht_get_size(ht_new_pids));
    data += sprintf(data,"file sizes size %d\n", ht_get_size(ht_file_sizes));
    data += sprintf(data,"file names size %d\n", ht_get_size(ht_file_names));
    data += sprintf(data,"file chunksums size %d\n", ht_get_size(ht_file_chunksums));
    data += sprintf(data,"splitcalls size %d\n", ht_get_size(ht_splitcalls));
    
    jprinte("status command: %s \n",buf);
    buf = data;

    return 0;
}

int clear_command(void){
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i;
    //TODO: clear cache, reset counters and set things up to start the experiments 
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            tvcpu->nitrodata.scall_count = 0;
            tvcpu->nitrodata.sret_count = 0;
            tvcpu->nitrodata.scall_time = 0;
            tvcpu->nitrodata.sret_time = 0;
        }
    }
    return 0;
}

int set_task_ptrptr(char* buf){
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i;
    //TODO: clear cache, reset counters and set things up to start the experiments 
    jprintk("settaskptr %s \n",buf);
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            sscanf(buf,"%lx",&(tvcpu->nitrodata.task_ptrptr));
            while(*buf != ' '){
                buf++;
            }
            buf++;
            jprintk("vcpu id %d , task ptr %lx \n",tvcpu->vcpu_id,tvcpu->nitrodata.task_ptrptr);
        }
   }
   return 0;
}

extern void vmmcomm_set_base(unsigned long base);
extern void vmmcomm_unset_base(void);
extern void vmmcomm_start(void);
extern void vmmcomm_stop(void);

#ifdef JADUKATA_VMM_COMM
void set_vmmcomm_base_helper(struct kvm_vcpu* tvcpu, gva_t basegva){
    struct x86_exception ex;
    hva_t basehva = 0;
    unsigned long magic = 0L;
    basehva = jadukata_gva_to_hva(tvcpu,basegva);
    jprintk("going to set_vmmcomm_base basegva %lx basehva %lx \n", basegva,basehva );
    if(basehva){
        vmmcomm_set_base(basehva);
        vmmcomm_start();
    }
}
#endif

int set_vmmcomm_base(char* buf){
#ifdef JADUKATA_VMM_COMM
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    gva_t basegva = 0;
    int i;

    sscanf(buf,"%lx\n",(unsigned long*)&basegva);
    //TODO: clear cache, reset counters and set things up to start the experiments 
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            if(i==0){
                tvcpu->nitrodata.pnetlink = basegva;
            }
        }
    }
#endif
    return 0;
}

int unset_vmmcomm_base(void){
#ifdef JADUKATA_VMM_COMM
        vmmcomm_stop();
        vmmcomm_unset_base();
#endif
    return 0;
}
static DEFINE_SPINLOCK(mmap_lock);

struct process_data* get_process_data(unsigned long cr3){
    struct process_data* m = NULL;
    int i;
    if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) == 0){
        spin_lock(&mmap_lock);
        if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) == 0){
            m = (struct process_data*) kmalloc(sizeof(struct process_data),GFP_KERNEL);
            if(m == NULL){
                panic("kmalloc failure in mmap process\n");
            }
            spin_lock_init(&m->lock);
            m->cr3 = cr3;
            for(i=0;i<MAX_THREADS_SMALL_IO;i++){
                m->buffer[i] = 0;
                m->bufferuse[i] = false;
            }
            ht_create_with_size(&m->v2p,"v2p",100);
            ht_create_with_size(&m->p2v,"p2v",100);
            ht_create_with_size(&m->pending,"pending",10);
            ht_create_with_size(&m->vbe,"vbe",10);
            ht_create_with_size(&m->mfds,"mfds",10);
            ht_create_with_size(&m->fdoffsets,"fdoffsets",10);
            ht_add_val(ht_mmap_process,cr3,(unsigned long)m);
        }
        spin_unlock(&mmap_lock);
    }
    if(m==NULL){
        jprinte("ERROR process_data NULL for cr3 %lx \n" , cr3);
        panic(" process_data NULL\n");
    }
    return m;
}

void drop_process_data(unsigned long cr3){
    struct process_data* m = NULL;
    spin_lock(&mmap_lock);
    if(ht_remove_val(ht_mmap_process,cr3,(unsigned long*)(&m)) != -1){
        if(m->v2p){
            ht_destroy(m->v2p);
        }
        if(m->p2v){
            ht_destroy(m->p2v);
        }
        if(m->pending){
            ht_destroy(m->pending);
        }
        if(m->vbe){
            ht_destroy(m->vbe);
        }
        if(m->mfds){
            ht_destroy(m->mfds);
        }
        if(m->fdoffsets){
            ht_destroy(m->fdoffsets);
        }
        kfree(m);
    }
    spin_unlock(&mmap_lock);
}

unsigned long get_new_file_key(char* path){
    int length = strlen(path);
    unsigned long hash = csum_partial(path,length,0);

    if(ht_lookup(ht_file_names,hash) == 0){
        char* newpath = (char*)vmalloc(length);
        memcpy(newpath,path,length);
        ht_add_val(ht_file_names,hash,(unsigned long)newpath);
        ht_add_val(ht_file_sizes,hash,0);
    }
    return hash;
}

int get_filehash_fd(unsigned long cr3, unsigned long fd){
    unsigned long hash = 0;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->mfds,fd,&hash) ==0){
        jprinte("ERROR: missing file hash for fd %d \n",fd);
    }
    return hash;
}

int get_filesize_fd(unsigned long cr3, unsigned long fd){
    unsigned long hash,size;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->mfds,fd,&hash) !=0){
        if(ht_lookup_val(ht_file_sizes,hash,&size) != 0){
            return size;
        }else{
            jprinte("ERROR: missing file size for file hash %lx \n",hash);
        }
    }
    return 0;
}

unsigned long get_offset_fd(unsigned long cr3, unsigned long fd){
    unsigned long hash;
    unsigned long offset=~0;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->fdoffsets,fd,&offset) ==0){
        jprinte("ERROR: missing file offset for fd %lu \n",fd);
    }
    return offset;
}

unsigned long update_fd_offsets(unsigned long cr3, unsigned long fd, unsigned long newsize){
    unsigned long hash,size = 0;
    struct process_data* m = get_process_data(cr3);
    ht_update_val(m->fdoffsets,fd,newsize);
    if(ht_lookup_val(m->mfds,fd,&hash) !=0){
        if(ht_lookup_val(ht_file_sizes,hash,&size) == 0){
            jprinte("ERROR: missing file size for file hash %lx \n",hash);
        }else{
            if(size<BIG_FILE_SIZE && newsize > BIG_FILE_SIZE){
                jprintk("bigfile transition size %lu newsize %lu fd %lu hash %lx \n", size,newsize,fd,hash);
            }
            ht_update_val(ht_file_sizes,hash,newsize);
        }
    }
    return newsize;
}

unsigned long increment_fd_offsets(unsigned long cr3, unsigned long fd, unsigned long increment){
    unsigned long offset = 0;
    unsigned long newsize = 0;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->fdoffsets,fd,&offset) == 0){
        jprinte("ERROR: cannot update offsets for fd %ld \n", fd);
        return 1;
    }
    newsize = offset + increment;
    return update_fd_offsets(cr3,fd,newsize);
}

void add_monitored_fd(unsigned long cr3, unsigned long fd,unsigned long hash){
    struct process_data* m = get_process_data(cr3);
    spin_lock(&m->lock);
    if(ht_remove(m->mfds,fd) != -1){
        jprinte("ERROR already monitored fd remonitored\n");
    }
    ht_add_val(m->mfds,fd,hash);
    ht_add_val(m->fdoffsets,fd,0);
    jprintk( "added fd %d to monitored fds\n",fd);
    spin_unlock(&m->lock);
}

void del_monitored_fd(unsigned long cr3, unsigned long fd){
    struct process_data* m = get_process_data(cr3);
    unsigned long del_hash=0,fhash;
    if(is_monitored_fd(cr3,fd)){
#ifdef JADUKATA_CHECKSUM
        fhash = get_filehash_fd(cr3,fd);
        if(fhash != 0){
            ht_open_scan(ht_file_chunksums);
            while( ht_scan(ht_file_chunksums,&del_hash) != -1 ){
                if( UNPACK_INTS_FROM_LONG_X(del_hash) == fhash ){
                    htn_remove(ht_file_chunksums,del_hash);
                }
            }
            ht_close_scan(ht_file_chunksums);
        }
#endif
        spin_lock(&m->lock);
        if(ht_remove(m->mfds,fd) == -1){
            jprinte("ERROR no fd found to unmonitor\n");
        }else{
            jprintk( "removed fd %d from monitored fds\n",fd);
        }
        if(ht_remove(m->fdoffsets,fd) == -1){
            jprinte("ERROR no fd found to unmonitor\n");
        }else{
            jprintk( "removed fd %d from monitored offsets\n",fd);
        }
        spin_unlock(&m->lock);
    }
}

int is_monitored_fd(unsigned long cr3, unsigned long fd){
    struct process_data* m = get_process_data(cr3);
    return ht_lookup(m->mfds,fd);
}

static DEFINE_SPINLOCK(mon_lock);

void print_targeted_processes(void){
    int i = 1, j = 1;
    unsigned long cr3;
    unsigned long pid;
    hash_table *ht_threads = NULL;
    jprintk("leo %d processes are monitored \n",ht_mon_process->table->size);
    ht_open_scan(ht_mon_process);
    while(ht_scan(ht_mon_process, &cr3) != -1){
        if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
            ht_open_scan(ht_threads);
            j = 1;
            while(ht_scan(ht_threads, &pid) != -1){
                jprintk("leo monitored cr3 #%d is %lx pid #%d is %lu : ",i,cr3,j,pid);
                j++;
            }
            ht_close_scan(ht_threads);
        }else{
            jprinte( "ERROR no pids associated with targeted process cr3 %lx \n", cr3);
        }
        i++;
    }
    ht_close_scan(ht_mon_process);
}

unsigned long targeted_process_pid_to_cr3(unsigned long tpid){
    unsigned long cr3 = 0;
    unsigned long pid;
    bool found = false;
    hash_table *ht_threads = NULL;
    ht_open_scan(ht_mon_process);
    while((found == false) && (ht_scan(ht_mon_process, &cr3) != -1)){
        if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
            ht_open_scan(ht_threads);
            while(ht_scan(ht_threads, &pid) != -1){
                if(pid == tpid){
                    found = true;
                    break;
                }
            }
            ht_close_scan(ht_threads);
        }else{
            jprinte( "ERROR no pids associated with targeted process cr3 %lx \n", cr3);
        }
    }
    ht_close_scan(ht_mon_process);
    
    jprinte( "ERROR no process cr3 found for pid %d \n", tpid);

    return (found==true)?cr3:0;
}

int is_targeted_process(unsigned long cr3){
    if( ones_match(cr3,process_cr3s_bloom) ){
        return ht_lookup(ht_mon_process,cr3);
    }
    return 0;
}

void recalc_process_cr3s_bloom(void){
    unsigned long cr3;
    process_cr3s_bloom = 0;
    ht_open_scan(ht_mon_process);
    while(ht_scan(ht_mon_process, &cr3) != -1){
        process_cr3s_bloom |= cr3;
    }
    ht_close_scan(ht_mon_process);
}

int add_targeted_process(unsigned long cr3,unsigned long parentcr3, unsigned long pid){
    spin_lock(&mon_lock);
    if(cr3 != parentcr3){
        if(ht_add_val(ht_mon_process,cr3,parentcr3) ==1){
            recalc_process_cr3s_bloom();
        }
    }

    hash_table *ht_threads = NULL;
    if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) == 0){
        ht_create_with_size(&ht_threads, "ht_threads", 2);
        ht_add_val(ht_mon_pids,cr3,(unsigned long)ht_threads);
    }
    
    ht_add(ht_threads,pid);

    jprintk("process cr3 %lx pid %lu added to monitored processes\n",cr3,pid );
    get_process_data(cr3);

#ifdef NO_ON_OFF
    start_nitro_all();
#endif
    spin_unlock(&mon_lock);

    return 0;
}

int remove_targeted_process_core(unsigned long cr3, int pid){
    spin_lock(&mon_lock);
    hash_table *ht_threads = NULL;
    if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
        if(pid != -1){
            //just remove one thread
            ht_remove(ht_threads,pid);
        }

        // if no more threads or if we want to remove whole process 
        if(ht_get_size(ht_threads) == 0 || pid == -1){
            if(ht_remove(ht_mon_process,cr3) != -1){
                recalc_process_cr3s_bloom();
            }
            ht_remove(ht_mon_pids,cr3);
            ht_destroy(ht_threads);
            drop_process_data(cr3);
        }
        jprintk("process cr3 %lx removed from monitored processes\n",cr3);
    }else{
        jprinte("ERROR: no pids associated with process cr3 %lx \n", cr3);
    }

#ifdef NO_ON_OFF
    if(ht_get_size(ht_mon_process) == 0){
        stop_nitro_all();
    }
#endif
    spin_unlock(&mon_lock);
    return 0;
}

int remove_targeted_process(unsigned long cr3){
    unsigned long childcr3, parentcr3;
    
    hash_table *ht_children = NULL;
    ht_create_with_size(&ht_children, "ht_children", 10); 

    //find all children
    ht_open_scan(ht_mon_process);
    while(ht_scan_val(ht_mon_process, &childcr3,&parentcr3) != -1){
        if(parentcr3 == cr3){
            ht_add(ht_children,childcr3);
            htn_remove(ht_mon_process,childcr3);
        }
    }
    ht_close_scan(ht_mon_process);

    //recursively remove children and their children if any
    ht_open_scan(ht_children);
    while(ht_scan(ht_children, &childcr3) != -1){
        remove_targeted_process(childcr3);
    }
    ht_close_scan(ht_children);

    //finally remove self
    remove_targeted_process_core(cr3,-1);
    
    ht_destroy(ht_children);
    return 0;
}

int del_mmap_gpa_helper(struct kvm_vcpu *vcpu, unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    unsigned long gpa;
    hva_t hva;
    hpa_t hpa;
    if(ht_lookup_val(ht_mmap_gpas_ptep,ptep, &gpa) !=0){
        ht_remove(ht_mmap_gpas,gpa);
        ht_remove(ht_mmap_gpas_ptep,ptep);
        hva = gfn_to_hva(vcpu->kvm,gpa_to_gfn(gpa)) | (gpa & ~PAGE_MASK);
        hpa = pfn_to_hpa(gfn_to_pfn(vcpu->kvm,gpa_to_gfn(gpa))) | (gpa & ~PAGE_MASK);
        ht_remove(ht_data_hpas,hpa);
        jprintk("removing gpa from mmap gpas %lx ptep %lx \n", gpa,ptep); 
    }
#endif
    return 0;
}

int add_mmap_gpa_helper(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    gpa_t gpa = page_boundary(gpas);
    unsigned long bigfile = 0;
    hva_t hva;
    hpa_t hpa;
    hva = gfn_to_hva(vcpu->kvm,gpa_to_gfn(gpa)) | (gpa & ~PAGE_MASK);
    hpa = pfn_to_hpa(gfn_to_pfn(vcpu->kvm,gpa_to_gfn(gpa))) | (gpa & ~PAGE_MASK);
    
    if(vcpu->nitrodata.nu.out_syscall == MMAP_SCALL_NR && vcpu->nitrodata.nu.u.mm.mmap_len){
        bigfile = (vcpu->nitrodata.nu.u.mm.mmap_len > BIG_FILE_SIZE);
    }else{
        //try to lookup old bigfile info
        ht_lookup_val(ht_data_hpas,hpa,&bigfile);
    }

    //remove old
    del_mmap_gpa_helper(vcpu,ptep);
    //add new
    ht_add_val(ht_mmap_gpas,gpa,ptep);
    ht_add_val(ht_mmap_gpas_ptep,ptep,gpa);
    ht_add_val(ht_data_hpas,hpa,bigfile);
    jprintk( " adding gpa to mmap gpas %lx hva %lx hphys %lx ptep %lx \n", gpa,hva,hpa,ptep); 
#endif
    return 0;
}

int del_rw_gpa_helper(struct kvm_vcpu *vcpu,unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    unsigned long gpa;
    hva_t hva;
    hpa_t hpa;
    hash_table *ht_list = NULL;
    if(ht_lookup_val(ht_rw_gpas_ptep,ptep, &gpa) !=0 ){
        ht_remove(ht_rw_gpas,gpa);
        ht_remove(ht_rw_gpas_ptep,ptep);
        jprintk("removing gpa from rw gpas %lx ptep %lx \n", gpa,ptep);
        //untrack the gpa
        ht_remove(ht_tracked_gpas,gpa);

        ht_lookup_val(ht_tracked_gfns, gpa_to_gfn(gpa), (unsigned long*)&ht_list);
        if(ht_list != NULL){
            //remove all ranges
            ht_destroy(ht_list);
        }else{
            jprinte("ERROR: empty list for ht_tracked_gfns\n");
        }
        ht_remove(ht_tracked_gfns,gpa_to_gfn(gpa));
    }
#endif
    return 0;
}

int add_rw_gpa_helper(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    unsigned long val;
    hash_table *ht_list = NULL;
    struct process_data* m = get_process_data(cr3);
    gpa_t gpa = page_boundary(gpas);
    //remove old
    del_rw_gpa_helper(vcpu,ptep);
    //add new
    ht_add_val(ht_rw_gpas,gpa,ptep);
    ht_add_val(ht_rw_gpas_ptep,ptep,gpa);
    //write protect the actual virtual buffer gpa base
    ht_add_val(ht_tracked_gpas, gpa,GPA_ENCODE(cr3,VBUF_GPA));
    //for VBUF_GPA, ht_tracked_gfns has a list of actual start offset and end offsets that the virt buffers occupy in this page
    val = PACK_INTS_TO_LONG((gpas-gpa),(gpae-gpa));

    ht_lookup_val(ht_tracked_gfns, gpa_to_gfn(gpa), (unsigned long*)&ht_list);
    if(ht_list == NULL){
        ht_create_with_size(&ht_list,"vbeofs",1);
        ht_update_val(ht_tracked_gfns, gpa_to_gfn(gpa),(unsigned long)ht_list);
    }
    ht_add_val(ht_list, val,vaddr);

    jadu_write_protect(vcpu,gpa);
    jprintk("adding gpa to rw gpas %lx ptep %lx \n", gpa,ptep); 
#endif
    return 0;
}

int add_virt_range_gpa(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep, int code){
    if(code == MMAP_GPA){
        add_mmap_gpa_helper(vcpu,cr3,vaddr,gpas,gpae,ptep);
    }else{
        add_rw_gpa_helper(vcpu,cr3,vaddr,gpas,gpae,ptep);
    }
    return 0;
}

int del_virt_range_gpa(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long ignore0, unsigned long ignore1, unsigned long ignore2, unsigned long ptep, int code){
    if(code == MMAP_GPA){
        del_mmap_gpa_helper(vcpu,ptep);
    }else{
        del_rw_gpa_helper(vcpu,ptep);
    }
    return 0;
}

int is_mmap_gpa(gpa_t gpa){
    int ret = 0;
#ifdef JADUKATA_CHECKSUM
    ret = ht_lookup(ht_mmap_gpas,gpa);
#endif
    return ret;
}

int add_tracked_pte(gpa_t cr3, gpa_t ptep, int code){
    unsigned long val = 0;
    gpa_t ptepfn = gpa_to_gfn(ptep);
    if(ht_lookup(ht_tracked_gpas,ptep) == 0){
        ht_add_val(ht_tracked_gpas, ptep,GPA_ENCODE(cr3,code));
        if(ht_lookup_val(ht_tracked_gfns,ptepfn,&val) != 0){
            ht_remove(ht_tracked_gfns, ptepfn);
        }
        //we just increment the val without regards to collisions in the ptep within ptepfn
        //because the fact that we are here means there is a ptep that is not write protected
        ht_add_val(ht_tracked_gfns, ptepfn, val+1);
        if(val == 0){
            return 1;
        }
    }else{
        jprinte("ERROR : already being tracked ptep %lx to be added\n", ptep);
    }
    return 0;
}

int del_tracked_pte(gpa_t ptep){
    unsigned long val = 0;
    gpa_t ptepfn = gpa_to_gfn(ptep);
    if(ht_lookup(ht_tracked_gpas,ptep) != 0){
        ht_remove(ht_tracked_gpas, ptep);
        if(ht_lookup_val(ht_tracked_gfns,ptepfn,&val) != 0){
            ht_remove(ht_tracked_gfns,ptepfn);
            if(val > 1){
                ht_add_val(ht_tracked_gfns, ptepfn, val-1);
            }else{
                //actual untracking of step happens automatically during the next page fault handling 
                jprintk( " untracking gfn %lx last ptep %lx \n", ptepfn, ptep);
            }
        }else{
            jprinte("ERROR : no ht_tracked_gfns entry for tracked ptep %lx to be deleted \n", ptep);
        }
        return 1;
    }else{
        jprinte("ERROR : del untracked ptep %lx \n", ptep);
    }
    return 0;
}

int process_pending_remaps(struct kvm_vcpu* vcpu,struct process_data *m, int code);

int handle_tracked_pte_helper(struct kvm_vcpu *vcpu, struct process_data* m, gpa_t addr,int code){
    unsigned long vaddr,vaddrs;
    unsigned long ptep,tptep;
    int count = 0;

    if( ht_lookup_val(m->p2v,addr,&vaddr) !=0 ){
        jprintk( " leo handle fault : ptep %lx vaddr %lx found in cr3 %lx \n", addr, vaddr, m->cr3);
        ptep = addr;
        vaddrs = vaddr;
        count = 1;
        //Leo commented this out so that we process each remap within the same page table entries page as and when it is overwritten
        /*
        vaddr_start = vaddr;
        while(ht_lookup_val(m->v2p,vaddr,&tptep) != 0){
            if(ptep==tptep){
                count++;
                vaddr += PAGE_SIZE;
            }else{
                break;
            }
        }
        if(count<=0){
            jprinte("ERROR : inconsistency between v2p and p2v: addr %lx vaddr_start %lx tptep %lx vaddr %lx \n", addr, vaddr_start, tptep, vaddr );
        }else{
        */
            jprintk( " leo handle fault : ptep %lx vaddrs %lx len %d\n", addr, vaddrs, count);
            add_remap_task(m,vaddrs,count);
        //}

        process_pending_remaps(vcpu,m,code);
        return 1;
    }
    return 0;
}

// TODO add bloom filter here
int handle_tracked_pte(struct kvm_vcpu *vcpu, gpa_t gpa,unsigned long cr3code){
    int i,ret=0;
    unsigned long tgfn;
    int code = GPA_DECODE_VAL(cr3code);

    jprintk( " page fault due to jadu ptep  %lx\n", gpa);
    unsigned long cr3;
    struct process_data* m = get_process_data(GPA_DECODE_CR3(cr3code));
    return handle_tracked_pte_helper(vcpu,m,gpa,code);
}

int handle_tracked_rw(struct kvm_vcpu *vcpu, gpa_t gpa,unsigned long vaddr){
    lock_virt_buffer(vcpu,vaddr,PAGE_SIZE - (gpa & ~PAGE_MASK),false);
    return 0;
}

unsigned long is_jadu_write_protected(gpa_t gpa,gva_t *gva){
    unsigned long val = 0;
    if(ht_lookup_val(ht_tracked_gpas,gpa,&val) == 0){
        gpa_t gpab = page_boundary(gpa);
        //check if this is due to vbuf write protect
        if(ht_lookup_val(ht_tracked_gpas,page_boundary(gpab),&val) != 0){
            hash_table *ht_list = NULL;
            unsigned long offsets,vaddr;
            bool found = false;

            ht_lookup_val(ht_tracked_gfns,gpa_to_gfn(gpa),(unsigned long*)&ht_list);
            ht_open_scan(ht_list);
            while(ht_scan_val(ht_list, &offsets,&vaddr) != -1){
                if( (gpa >= (gpab + UNPACK_INTS_FROM_LONG_X(offsets))) && (gpa <= (gpab + UNPACK_INTS_FROM_LONG_Y(offsets))) ){
                    jprintk( "tracked VBUF_GPA gpa %lx gfn %lx val %lu offsets %lx \n", gpa, gpa_to_gfn(gpa), val, offsets);
                    found = true;
                    *gva = vaddr + (gpa-(gpab+UNPACK_INTS_FROM_LONG_X(offsets)));
                    break;
                }
            }
            ht_close_scan(ht_list);
            if(found == false){
                val = 0;
            }
        }
    }
    if(val){
        jprintk("tracked gpa %lx gfn %lx cr3 %lx code %d \n", gpa, gpa_to_gfn(gpa), GPA_DECODE_CR3(val), GPA_DECODE_VAL(val));
    }
    return val;
}

int add_remap_task(struct process_data* m, unsigned long vaddrs, unsigned long numpages){
    unsigned long vaddre = vaddrs + (numpages* PAGE_SIZE);
    ht_add_val(m->pending,vaddrs,vaddre);
    return 0;
}

int process_virt_range_ptep(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long ignore0, unsigned long ignore1, unsigned long ignore2, unsigned long ptep,int code){
    //for page table entries due to both mmap and virt buff write protection, we want to write protect
    if(add_tracked_pte(cr3,ptep,code)){
        jadu_write_protect(vcpu,ptep);
    }
    return 0;
}

int process_pending_remaps(struct kvm_vcpu* vcpu,struct process_data *m, int code){
    unsigned long vaddrs, vaddre;
    while(ht_pop_val(m->pending, &vaddrs, &vaddre) != -1){
        if(monitor_virt_range(vcpu,vaddrs,(vaddre-vaddrs),code)){
            jprintk("remapped vaddr %lx \n", vaddrs); 
        }
    }
    return 0;
}

static DEFINE_SPINLOCK(lock_virt_buff_lock);

extern void msleep(unsigned int msecs);

bool overlaps(unsigned long s1, unsigned long e1, unsigned long s2, unsigned long e2){
    return !((e1<s2)||(e2<s1));
}

void lock_virt_buffer(struct kvm_vcpu *vcpu, unsigned long vaddr_start, unsigned long len, bool acquire){
#ifdef LOCK_UNLOCK_BUFFER
    unsigned long vaddr_end = vaddr_start + len;
    unsigned long start,sizewho;
    struct process_data *m = get_process_data(vcpu->arch.cr3);
    int pid = get_guest_pid(vcpu);
    int who,size;
    bool monitor = true;
    bool remoteflush = false;

    while(true){
        bool locked = false;
        spin_lock(&lock_virt_buff_lock);
        ht_open_scan(m->vbe);
        while(ht_scan_val(m->vbe, &start,&sizewho) != -1){
            size = UNPACK_INTS_FROM_LONG_X(sizewho);
            who = UNPACK_INTS_FROM_LONG_Y(sizewho);
            if(overlaps(start,start+size,vaddr_start,vaddr_end)){
                bool exactoverlap = ((start == vaddr_start) && ((start+size)==vaddr_end));
                if(who>0){
                    if(pid != who){
                        locked = true;
                        break;
                    }
                }else{
                    if(pid != -who){
                        remoteflush = true;
                    }
                }
                if(exactoverlap == false){
                    unmonitor_virt_range(vcpu,start,size,VBUF_GPA);
                    htn_remove(m->vbe, start);
                }else{
                    monitor = false;
                    break;
                }
            }
        }
        ht_close_scan(m->vbe);
        if(locked == false){
            if(acquire == true){
                ht_update_val(m->vbe,vaddr_start,PACK_INTS_TO_LONG(len,pid));
            }else{
                //to make sure next lock does a remote tlb flush
                ht_update_val(m->vbe,vaddr_start,PACK_INTS_TO_LONG(len,-1));
            }
        }
        spin_unlock(&lock_virt_buff_lock);
        if(locked == true){
            msleep(3000);
        }else{
            break;
        }
    }

    if(monitor == true){
        monitor_virt_range(vcpu,vaddr_start,len,VBUF_GPA);
    }
    if(remoteflush == true){
        kvm_flush_remote_tlbs(vcpu->kvm);
    }
#endif
}

void unlock_virt_buffer(struct kvm_vcpu *vcpu, unsigned long vaddr_start, unsigned long len){
#ifdef LOCK_UNLOCK_BUFFER
    unsigned long vaddr_end = vaddr_start + len;
    unsigned long start,sizewho;
    struct process_data *m = get_process_data(vcpu->arch.cr3);
    int pid = get_guest_pid(vcpu);
    int who,size;

    ht_update_val(m->vbe,vaddr_start,PACK_INTS_TO_LONG(len,-pid));
#endif
}

int is_monitored_syscall(unsigned long nr){
    int i =0;
    if(ones_match(nr,syscall_bloom)){
        for(i=0;i<NR_SYSCALLS;i++){
            if(monitored_syscalls[i] == nr){
                return 1;
            }
        } 
    }
    return 0;
}

static DEFINE_SPINLOCK(splitcalls_lock);
static DEFINE_SPINLOCK(newprocess_lock);

int check_process_reassignments(struct kvm_vcpu *vcpu){
    unsigned long cpid = get_guest_pid(vcpu);
    unsigned long pid,spid=0;
    int valid = 0;
    hash_table *ht_threads = NULL;
    if( ht_lookup_val(ht_mon_pids,vcpu->arch.cr3,(unsigned long*)&ht_threads) != 0 ){
        ht_open_scan(ht_threads);
        while(ht_scan(ht_threads, &pid) != -1){
            if(pid == cpid){
                valid = 1;
                break;
            }
        }
        ht_close_scan(ht_threads);

        if(valid == 0){
            jprintk("a targeted process page table base has been reused for another unmonitored process old pid %lu new pid %lu cr3 %lx \n", pid, cpid, vcpu->arch.cr3);
            remove_targeted_process(vcpu->arch.cr3);
            return 1;
        }
    }else{
        jprinte("ERROR could not find pid for a targeted process cr3 %lx \n", vcpu->arch.cr3);
    }
    return 0;
}

int new_process_tracking(struct kvm_vcpu *vcpu){
    int ret = 0;
    if(ht_get_size(ht_new_pids) != 0){
        unsigned long pid;
        unsigned long cpid = get_guest_pid(vcpu);
        unsigned long parentcr3;
        ht_open_scan(ht_new_pids);
        while(ht_scan_val(ht_new_pids, &pid,&parentcr3) != -1){
            if(pid == cpid){
                char comm[16];
                get_guest_comm(vcpu,comm);
                jprintk("adding newly monitored process with pid %d comm %s ,cr3 %lx and parentcr3 %lx \n", pid,comm, vcpu->arch.cr3, parentcr3);
                add_targeted_process(vcpu->arch.cr3,parentcr3,pid); 
                htn_remove(ht_new_pids,pid);
                ret = 1;
                break;
            }
        }
        ht_close_scan(ht_new_pids);
    }
    return ret;
}

void nitro_handle_guest_task_switch(struct kvm_vcpu* vcpu, unsigned long newcr3){
    //leo
    int otar = is_targeted_process(vcpu->arch.cr3);
    int ntar = is_targeted_process(newcr3);

    if(otar | ntar ){
        jprintk("CR3 write on cpu %d vcpuid %d oldcr3 %lx newcr3 %lx otar %d ntar %d \n", vcpu->cpu, vcpu->vcpu_id, vcpu->arch.cr3, newcr3, otar, ntar);
    }

#ifndef NO_SYSCALL_HANDLING
    //if old process is targeted and in between a syscall processing
    if(otar){
        spin_lock(&splitcalls_lock);
        if(is_monitored_syscall(vcpu->nitrodata.nu.out_syscall)){
            // we use the userspace stack pointer before syscall saved at the syscall intercept time 
            // to later match this outstanding syscall with its sysret that will happen in a different
            //  or same vcpu after guest context switch
            //  struct nitrodatau *nu  = kmalloc(sizeof(struct nitrodatau), GFP_ATOMIC);
            struct nitrodatau *nul = NULL;
            struct nitrodatau *nu = (struct nitrodatau*) kmem_cache_alloc(nucache, GFP_ATOMIC);
            if(!nu){
                panic("ERROR : could not allocate nitrodata entry \n");
            }

            memcpy(nu,&vcpu->nitrodata.nu,sizeof(struct nitrodatau));
            jprintk("stashing split call with id %lx nu %lx scall %lu \n",nu->id,nu,nu->out_syscall);
            if(ht_add_val(ht_splitcalls,nu->id,(unsigned long)nu) == STATUS_DUPL_ENTRY){
                jprinte( "ERROR duplicate split call info id %lx \n", nu->id);
                if(ht_remove_val(ht_splitcalls,nu->id,(unsigned long*)&nul) != -1){
                    jprintk("deleting split call with id %lx nu %lx scall %lu\n",nul->id,nul,nul->out_syscall);
                    kmem_cache_free(nucache,nul);
                }
                if(ht_add_val(ht_splitcalls,nu->id,(unsigned long)nu) != 1){
                    jprinte("ERROR inconsistent hash table entry duplicate but not deletable and final add did not succeed\n");
                }else{
                    jprintk("added split call with id %lx nu %lx scall %lu\n",nu->id,nu,nu->out_syscall);
                }
            }
        }
        spin_unlock(&splitcalls_lock);
    }
#endif

    //clear nitrodata in vcpu, because if ntar we want to start afresh and if otar we have already stashed
    if(ntar || otar){
        //protects case when splitcall matching searches other vcpus
        spin_lock(&splitcalls_lock);
        nu_init(&vcpu->nitrodata.nu);
        spin_unlock(&splitcalls_lock);
    }

#ifndef NO_ON_OFF
    // if it is not tlb flush or thread context switch
    // enable / disable syscall/sysret interception
    if(ht_get_size(ht_new_pids) == 0){
        if(ntar && !otar){
            start_nitro(vcpu,newcr3);
        }else if(!ntar && otar){
            stop_nitro(vcpu,newcr3);
        }
    }else{
        start_nitro_all();
    }
    vcpu->nitrodata.first_scall = 1;
    //leo end
#endif
}



// this distance threshold avoid matching split syscalls between:
// 1) different processes because cr3 is different
// 2) different threads with closely laid userspace stacks because rip is different
// in 4KB pages
#define SCALL_ID_DIST (512UL) // allow 2 MB distance
#define ID_WITH_TID
//filter out the lower 32 bits of rip
//const unsigned long LOW32_MASK = ((1UL<<32)-1);

//generate id for this system call using the page table base cr3, 
//userspace stack pointer rsp and userspace inst rip right after syscall
#ifdef ID_WITH_TID
unsigned long get_id(struct kvm_vcpu* vcpu){
    return get_guest_pid(vcpu);
}
#else
unsigned long get_id(struct kvm_vcpu* vcpu, unsigned long cr3, unsigned long rip, unsigned long rsp){
    // higher order 32 bits made of cr3 and rip making mismatch in any avoid matches
    // lower bits are the stack page ( allowed difference is SCALL_ID_DIST)
    return ( ((cr3 ^ (rip>>8)) << 32) | (rsp >> 12));
}
#endif

int handle_split_syscall(struct kvm_vcpu* vcpu, char prefix, unsigned long id){
    int done = 0;
    struct nitrodatau *nu = NULL;
    struct kvm_vcpu* tvcpu = NULL;
    bool unlocked = false;
    spin_lock(&splitcalls_lock);
    //first priority check stashed split calls
    if(ht_remove_val(ht_splitcalls,id,(unsigned long*)&nu) != -1){
        done = 1;
        jprintk("sysret_hook splitcall prefix %c vcpuid %d cr3 %lx scallnr %lu %s nu %lx \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall), nu);
    }else{
        //second check on other vcpus ( because guest os can always reschedule in any order and therefore may lead to a monitored process being moved to a difference cpu before another process is scheduled on the current cpu and hence triggering stashing )
        int i;
        kvm_for_each_vcpu(i, tvcpu, vcpu->kvm){
            if( tvcpu->arch.cr3 == vcpu->arch.cr3 && tvcpu->vcpu_id != vcpu->vcpu_id && is_monitored_syscall(tvcpu->nitrodata.nu.out_syscall) && (ABSDIFF(tvcpu->nitrodata.nu.id,id) <SCALL_ID_DIST) ){
                nu = &tvcpu->nitrodata.nu;
                jprintk("sysret_hook splitcallvcpu prefix %c vcpuid %d realcpu %d tvcpuid %d tvcpurealcpu %d vcpucr3 %lx tvcpucr3 %lx scallnr %lu %s id %lx nuid %lx diff %lu nu %lx \n", prefix, vcpu->vcpu_id, vcpu->cpu, tvcpu->vcpu_id, tvcpu->cpu, vcpu->arch.cr3, tvcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall),id,nu->id, ABSDIFF(tvcpu->nitrodata.nu.id,id), nu);
                done = 2;
                break;
            }
        }
    }
    if(!done){
        spin_unlock(&splitcalls_lock);
        unlocked = true;
        // finally try matching the closest esp from stashed calls in same process
        // the 2MB diff limit avoids mathing against other processes because the 
        // cr3 is moved to higher order 32 bits in the id
        unsigned long lowest = ~0L, diff,nuldiff,tid;
        struct nitrodatau *nul = NULL;
        ht_open_scan(ht_splitcalls);
        while(ht_scan_val(ht_splitcalls,&tid, (unsigned long*)&nu) != -1){
            diff = ABSDIFF(id,nu->id);
            if(diff < lowest){
                lowest = diff;
                if(diff < SCALL_ID_DIST){
                    nul = nu;
                    nuldiff = diff;
                }
            }
        }
        ht_close_scan(ht_splitcalls);
        if(nul != NULL){
            jprintk("sysret_hook splitcallclosest prefix %c vcpuid %d cr3 %lx scallnr %lu %s id %lx nulid %lx diff %ld nul %lx \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, nul->out_syscall,syscall_name(nu->out_syscall),id,nul->id,nuldiff,nul);
            nu = nul;
            done = 1;
            ht_remove(ht_splitcalls,nul->id);
        }
    }

    if(done){
        //memcpy to handle chained state of fake syscalls
        memcpy(&vcpu->nitrodata.nu,nu,sizeof(struct nitrodatau));
        if(done == 1){
            kmem_cache_free(nucache,nu);
        }else if(done == 2){
            nu_init(&tvcpu->nitrodata.nu);
            //[linux scheduler sometimes schedules without cr3 overwrites]
            tvcpu->nitrodata.first_scall = 1;
        }else{
            jprinte("ERROR : invalid done mode %d  \n", done);
        }
        if(unlocked == false){
            spin_unlock(&splitcalls_lock);
        }
    }
    return done;
}

//TODO: handle int80 and sysenter with correct arg to register mapping
int syscall_hook(char prefix, struct x86_emulate_ctxt *ctxt){
    unsigned long scallnr, rsp, rip, id;
    struct timespec ts, te;
    struct kvm_vcpu* vcpu = emul_to_vcpu(ctxt);
    
    if( unlikely(!is_targeted_process(vcpu->arch.cr3) && vcpu->nitrodata.first_scall == 0) ){
        jprinte("ERROR: syscall called for non targeted process cr3 %lx \n",vcpu->arch.cr3);
    }
    
    scallnr = kvm_register_read(vcpu,VCPU_REGS_RAX);
    rsp = kvm_register_read(vcpu,VCPU_REGS_RSP);
    //rip = kvm_register_read(vcpu,VCPU_REGS_RIP);
    rip = vcpu->arch.emulate_ctxt._eip;

#ifdef ID_WITH_TID
    id = get_id(vcpu);
#else
    id = get_id(vcpu,vcpu->arch.cr3,rip,rsp);
#endif
  
    if(unlikely(vcpu->nitrodata.first_scall!=0)){
        vcpu->nitrodata.first_scall = 0;
#ifdef JADUKATA_VMM_COMM
        if(vcpu->nitrodata.pnetlink){
            set_vmmcomm_base_helper(vcpu,vcpu->nitrodata.pnetlink);
            vcpu->nitrodata.pnetlink=0;
        }
#endif
        if(is_targeted_process(vcpu->arch.cr3)){
            if(check_process_reassignments(vcpu) == 0){
                //valid monitored thread
                //to handle fake syscalls chain, try to match against stashed splitcalls
                handle_split_syscall(vcpu,'F',id);
            }
        }

        if(!is_targeted_process(vcpu->arch.cr3)){
            new_process_tracking(vcpu);
        }
    }

    if(!is_targeted_process(vcpu->arch.cr3)){
        stop_nitro(vcpu,vcpu->arch.cr3);
        return 0;
    }

    jprintk("scall cr3 %lx scallnr %lu %s rsp %lx rip1 %lx \n",vcpu->arch.cr3, scallnr, syscall_name(scallnr), rsp, rip);
    getnstimeofday(&ts);
    vcpu->nitrodata.scall_count++;
#ifdef TEST_MODE
    if(!vcpu->nitrodata.running){
        jprinte("ERROR : sysret interception without nitro running");
    }

    // close for some reason does not have a sysret
    // for now, we dont print error for close 
    if(vcpu->nitrodata.nu.out_syscall != NO_SCALL && vcpu->nitrodata.nu.out_syscall != 3){
        jprinte("ERROR : syscall interception while out_syscall is not NO_SCALL but %d vcpuid %d scallnr %lu \n",vcpu->nitrodata.nu.out_syscall,vcpu->vcpu_id, scallnr);
    }
#endif

#ifdef TEST_MODE
    //vcpu->nitrodata.nu.temp = rsp;
#endif

    vcpu->nitrodata.nu.out_syscall = scallnr ;
    vcpu->nitrodata.nu.id = id;

    if(is_monitored_syscall(scallnr)){
        jprintk("syscall_hook prefix %c vcpuid %d cr3 %lx scallnr %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall) , rsp);
        jadukata_syscall(prefix,vcpu,&(vcpu->nitrodata.nu));
        //return print_trace_proxy(prefix,vcpu);
    }else{
        jprintk("syscall_hook prefix %c vcpuid %d cr3 %lx scallnr UNMON %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall), rsp);
        vcpu->nitrodata.nu.out_syscall = UNMON_SCALL;
    }
    smp_wmb();
    getnstimeofday(&te);
    vcpu->nitrodata.scall_time += TIMEDIFF(te,ts);
    return 0;
}

//TODO: think about hooking into host context switches to avoid the barriers in each
// system call /return 
int sysret_hook(char prefix, struct x86_emulate_ctxt *ctxt){
    //long val = 0;
    struct x86_exception ex;
    struct timespec ts, te;
    struct kvm_vcpu* vcpu = emul_to_vcpu(ctxt);
    unsigned long rsp, rip;

    rsp = kvm_register_read(vcpu, VCPU_REGS_RSP);
    rip = vcpu->arch.emulate_ctxt._eip; // VCPU_REGS_RIP updated only after emulation of sysret
    //unsigned long rip1 = kvm_register_read(vcpu,VCPU_REGS_RIP);
    jprintk("sret cr3 %lx rsp %lx rip1 %lx \n",vcpu->arch.cr3, rsp, rip);
    if(!is_targeted_process(vcpu->arch.cr3)){
        //jprinte("ERROR: sysret called for non targeted process cr3 %lx \n",vcpu->arch.cr3);
        return 0;
    }
    getnstimeofday(&ts);
    //jprintk("sret cr3 %lx rsp %lx rip %lx rip1 %lx \n",vcpu->arch.cr3, rsp, rip, rip1 );
    //print_all_guest_pids(vcpu);
    smp_rmb();
    vcpu->nitrodata.sret_count++;
#ifdef TEST_MODE
    if(!vcpu->nitrodata.running){
        jprinte("ERROR : sysret interception without nitro running");
    }
    /*
    val = ABSDIFF(vcpu->nitrodata.nu.temp,rsp);
    if(val > vcpu->nitrodata.nu.max){
        if(vcpu->nitrodata.nu.temp == 0){
            val = 0;
        }
        vcpu->nitrodata.nu.max = val;
        jprintk("max offset %lu scall %lx sret %lx nr %d \n",vcpu->nitrodata.nu.max, vcpu->nitrodata.nu.temp, rsp, vcpu->nitrodata.nu.out_syscall);
    }
    */
#endif
    if(vcpu->nitrodata.nu.out_syscall == NO_SCALL){
        // this is a syscall call whose processing is split across vcpus due to guest level context switch
        // try to match with the saved split syscalls during context switch of targeted processes
#ifdef ID_WITH_TID
        unsigned long id = get_id(vcpu);
#else
        unsigned long id = get_id(vcpu,vcpu->arch.cr3, rip,rsp);
#endif
        if(handle_split_syscall(vcpu,prefix,id) == 0){
            jprinte("ERROR : unmatched syscall id %lx cr3 %lx rsp %lx rip %lx \n", id, vcpu->arch.cr3, rsp, rip);
        }
        jadukata_sysret(prefix-32,vcpu,&(vcpu->nitrodata.nu));
    }else if(vcpu->nitrodata.nu.out_syscall == UNMON_SCALL){
        //skip unmonitored sysrets
        vcpu->nitrodata.nu.out_syscall = NO_SCALL;
        jprintk("sysret_hook prefix %c vcpuid %d cr3 %lx scallnr UNMON %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3,vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall), rsp);
    }else{
        jprintk("sysret_hook prefix %c vcpuid %d cr3 %lx scallnr %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall),rsp);
        jadukata_sysret(prefix,vcpu,&(vcpu->nitrodata.nu));
    }
    getnstimeofday(&te);
    if(!vcpu->nitrodata.nu.fake_syscall){
        nu_init(&vcpu->nitrodata.nu);
    }
    vcpu->nitrodata.sret_time += TIMEDIFF(te,ts);
    return 0;
}

#endif
