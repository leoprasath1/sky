#include "../include/linux/jadu.h"
#include <linux/kvm_types.h>
#include <asm/checksum.h>

struct pair {
    unsigned long addr;
    unsigned int size;
};

int checksum_processor2(void* pargs, struct pair* pairs, int numpairs);
void checksum_processor1(struct pair* pairs, int numpairs);

unsigned long pack_info(int time, int sector,short bigfile);
int pack_info_get_jiffies(unsigned long val);
int pack_info_get_sector(unsigned long val);
unsigned long pack_info_get_filechunk(unsigned long val);

short pack_info_get_bigfile(unsigned long val);
