#include <linux/nitro.h>
#include "kvm_cache_regs.h"
#include "x86.h"
#include "mmu.h"
#include "checksum_processor.h"

#ifdef JADUKATA

extern void jadu_write_protect(struct kvm_vcpu *vcpu, gpa_t gpa);

extern bool ndbg;

#ifdef JADUKATA_CHECKSUM
//extern int leo_debug;

int ARGSMAP[MAXFAKESCALLARGS] = {VCPU_REGS_RDI, VCPU_REGS_RSI, VCPU_REGS_RDX, VCPU_REGS_R10, VCPU_REGS_R8, VCPU_REGS_R9};

extern hash_table *ht_file_chunksums;
extern hash_table *ht_data_checksums_read;
extern hash_table *ht_data_checksums_write;
extern hash_table *ht_metadata_sectors;
extern hash_table *ht_data_bigfile_sectors;
extern hash_table *ht_outstanding;

struct outstanding{
    struct pair outpairs[128];
    int outnumpairs;
    unsigned long outstanding_gva;
    int outstanding_size;
    int call_outstanding;
};

hva_t jadukata_gva_to_hva(struct kvm_vcpu *vcpu, gva_t gva){
    struct x86_exception error;
    gpa_t gpa;
    gfn_t gfn;
    hva_t hva;
    hpa_t hpa;
    pfn_t pfn;
    unsigned long kaddr;
    unsigned offset = 0;

	gpa = kvm_mmu_gva_to_gpa_system(vcpu, gva, &error);

    if (gpa == UNMAPPED_GVA) {
        jprinte("ERROR unmapped gpa %lx \n",gpa); //fault address %lx\n",gva, vcpu->arch.fault.address);
        return 0;
    }

    offset = (gpa & ~PAGE_MASK);
    gfn = gpa_to_gfn(gpa);
    hva = gfn_to_hva(vcpu->kvm,gfn) | offset;
    pfn = gfn_to_pfn(vcpu->kvm,gfn);
    hpa = pfn_to_hpa(pfn) | offset;
    kaddr = ((unsigned long)pfn_to_kaddr(pfn)) | offset;
    
    jprintk("gva %lx gpa %lx gfn %lx hva %lx pfn %lx hpa %lx kaddr %lx virt %lx pg %lx \n",gva, gpa, gfn, hva, pfn, hpa, kaddr, __va(hpa), virt_to_page(kaddr));

    return kaddr;
}

extern atomic_t jadu_rmetadata;
extern atomic_t jadu_rdata;
extern unsigned long data_checksums_start_jiffies;

struct pchksum_args {
    int rw;
    unsigned long fhash;
    int chks;
    short bigfile;
};

#define CLEANUP_TIME (30000) //30 seconds
unsigned long last_scan_data_checksum_jiffies = 0;
static DEFINE_SPINLOCK(checksum_cleaner_lock);

int process_checksum(void* pargs, int chunknum, unsigned long checksum){
    struct pchksum_args* args = (struct pchksum_args*)pargs;
    int rw = args->rw;
    int ret = 0;
    unsigned long fhash, oldchecksum, flags, info, del_checksum;
    jprintk("nitro %s checksums #%d : checksum %lx bigfile %d\n", rw==1 ? "WRITE" : "READ",  chunknum, checksum,args->bigfile);
    if(rw == 1) {
        ht_update_val(ht_data_checksums_write,checksum, pack_info(0,~0,args->bigfile) );
        fhash = PACK_INTS_TO_LONG(args->fhash,(args->chks+chunknum));
        if(ht_lookup_val(ht_file_chunksums,fhash,&oldchecksum) != 0){
            ht_update_val(ht_data_checksums_write,oldchecksum, pack_info(jiffies,0,args->bigfile));
        }
        ht_update_val(ht_file_chunksums,fhash,checksum);
    }else{
        unsigned long packedinfo;
        if(ht_remove_val(ht_data_checksums_read,checksum,&packedinfo) > 0){
            if(args->bigfile){
                ht_update_val(ht_data_bigfile_sectors,pack_info_get_sector(packedinfo),jiffies);
            }
            atomic_inc(&jadu_rdata);
        }else{
            atomic_inc(&jadu_rmetadata);
            ret = 1;
        }
    }
    
    //remove checksums with nonzero jiffies due to overwrites
    if(jiffies_to_msecs(jiffies-last_scan_data_checksum_jiffies) > CLEANUP_TIME){
        spin_lock_irqsave(&checksum_cleaner_lock,flags);
        ht_open_scan(ht_data_checksums_write);
        while(ht_scan_val(ht_data_checksums_write,&del_checksum,&info) != -1){
            if(jiffies_to_msecs(jiffies-pack_info_get_jiffies(info)) > CLEANUP_TIME){
                htn_remove(ht_data_checksums_write,del_checksum);
            }
        }
        ht_close_scan(ht_data_checksums_write);
        spin_unlock_irqrestore(&checksum_cleaner_lock,flags);
        last_scan_data_checksum_jiffies = jiffies;
    }

    return ret;
}

int jadukata_translate(struct kvm_vcpu *vcpu, gva_t gva, int size, struct pair* pairs, int *totpairs){
    unsigned bytes;
    unsigned offset;
    unsigned toread;
    int i,r=0;
    hva_t hva;

    char c;

    i = *totpairs;
    bytes = size;
    
    while (bytes) {
        offset = gva & (PAGE_SIZE-1);
        toread = min(bytes, (unsigned)PAGE_SIZE - offset);

        hva = jadukata_gva_to_hva(vcpu, gva);
        if(hva == 0){
            jprinte("ERROR invalid hva\n");
            r = 1;
            break;
        }

        pairs[*totpairs].addr = hva;
        pairs[*totpairs].size = toread; 
        
        bytes -= toread;
        gva += toread;

        *totpairs = *totpairs + 1;
        if(*totpairs>=96){
            jprinte("ERROR need more address pairs\n");
            r = 1;
            break;
        }
    }
    
    for(;i<*totpairs;i++){
        jprintk("gva %lx pair #%d : addr %lx size %lu \n", gva, i, pairs[i].addr, pairs[i].size);
    }
    return r;
}

void init_pchksum_args(struct kvm_vcpu *vcpu, struct pchksum_args *pargs, int rw, int fd, unsigned long fhash, int chks){
    pargs->rw = rw;
    pargs->bigfile = get_filesize_fd(vcpu->arch.cr3,fd) > (BIG_FILE_SIZE);
    pargs->fhash = fhash;
    pargs->chks = chks;
}
    
void checksum_caller(struct kvm_vcpu *vcpu, int rw, int fd, struct pair *pairs, int numpairs, struct pchksum_args* pargs){
#ifdef JADUKATA_CHECKSUM
#ifdef JADUKATA_VERBOSE
    checksum_processor1(pairs,numpairs); 
#endif
    checksum_processor2((void*)pargs,pairs,numpairs);
#endif
}

int jadukata_fake_syscall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, void (*chainfn)(struct kvm_vcpu*,struct nitrodatau*),unsigned long syscallnr, int num, ...){
    int i;
    unsigned long v;
    va_list args;
    struct fakescall *fk = &nu->u.rw.fk;
    jprintk("fake syscall scall nr %lu %s numargs %d chainfn %p \n",syscallnr,syscall_name(syscallnr),num,chainfn);
    nu->fake_syscall = 1;
    fk->last = false;
    fk->chainfn = chainfn;
    fk->numargs = num;
    va_start(args,num);
    for(i=0;i<num;i++){
        v = va_arg(args,unsigned long);
        fk->regs[i] = kvm_register_read(vcpu, ARGSMAP[i]);
        kvm_register_write(vcpu, ARGSMAP[i], v);
    }
    va_end(args);
    kvm_register_write(vcpu,VCPU_REGS_RAX,syscallnr);
    nu->out_syscall = syscallnr;
    return 0;
}

int jadukata_fake_sysret(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    int i;
    struct fakescall *fk = &nu->u.rw.fk;
    int fake_ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintk("fake sysret chainfn %p fake ret %d \n",nu->u.rw.fk.chainfn,fake_ret);
    for(i=0;i<fk->numargs;i++){
        kvm_register_write(vcpu, ARGSMAP[i], fk->regs[i]);
    }
    nu->u.rw.fk.chainfn(vcpu,nu);
    if(fk->last == false){
        //redo syscall 
        vcpu->arch.emulate_ctxt._eip-=2;
        jprintk("fake sysret changing rcx to %lx \n",vcpu->arch.emulate_ctxt._eip);
    }else{
        nu->fake_syscall = 0;
        jprintk("fake sysret last in chain unfake\n");
    }
    return 0;
}

int alloc_process_buffer_firsthalf(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    int i, inuse=0;
    struct fakescall *fk = &nu->u.rw.fk;
    struct process_data * pd = get_process_data(vcpu->arch.cr3);

    jprintk("alloc buffer first half \n");
    if(fk->buffer == 0){
        fk->bufferuse = -1;
        spin_lock(&pd->lock);
        for(i=0;i<MAX_THREADS_SMALL_IO;i++){
            if(pd->bufferuse[i] == false){
                if(pd->buffer[i] != 0){
                    fk->buffer = pd->buffer[i];
                    fk->bufferuse = i;
                    pd->bufferuse[i] = true;
                    break;
                }else{
                    fk->bufferuse = i;
                }
            }else{
                inuse++;
            }
        }
        spin_unlock(&pd->lock);

        if(i>=MAX_THREADS_SMALL_IO){
            if(inuse == MAX_THREADS_SMALL_IO){
                jprinte("ERROR: too many threads doing small / unaligned i/o concurrently\n");
            }else{
                return 1;
            }
        }
    }

    return 0;
}

int jadukata_sysret_readv_writev(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadvwritev);

void free_process_buffer(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct fakescall *fk = &nu->u.rw.fk;
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    jprintk("free process buffer \n");
    spin_lock(&pd->lock);
    pd->bufferuse[fk->bufferuse] = false;
    fk->buffer = 0;
    spin_unlock(&pd->lock);
}

void last_fake_chain(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    jprintk("last fake chain \n");
    nu->u.rw.fk.last = true;
}

void padded_lseek(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    free_process_buffer(vcpu,nu);
    //fake calls are always preadvpwritev
    jadukata_sysret_readv_writev('f', vcpu, nu,true);
    //SEEK_CUR 1
    jadukata_fake_syscall(vcpu, nu, last_fake_chain, (unsigned long)LSEEK_SCALL_NR, 3, nu->u.rw.fd,nu->u.rw.size,1UL);
}

void alloc_process_buffer_secondhalf(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct fakescall *fk = &nu->u.rw.fk;
    if(fk->buffer == 0){
        struct process_data *pd = get_process_data(vcpu->arch.cr3);
        //copy registers
        fk->buffer = kvm_register_read(vcpu,VCPU_REGS_RAX);
        pd->buffer[fk->bufferuse] = fk->buffer;
        jprintk("alloc buffer second half buffer ptr %lx \n",fk->buffer);
    }
}

void padd_helper(struct kvm_vcpu *vcpu, unsigned long numbytes, unsigned long* iovec_ptr, unsigned long buf_ptr, unsigned long* iovec_cnt){
    struct x86_exception ex;
    int ret;
    
    unsigned long ptr, i=0, addr, size;
    ptr = *iovec_ptr;

    //kvm_write_guest_virt_system cribs on the fake mmap buffer with write access
    unsigned long* hva = (unsigned long*) jadukata_gva_to_hva(vcpu, *iovec_ptr);

    jprintk("padd helper ptr %lx addr %lx size %lu hva %lx \n", *iovec_ptr, buf_ptr, numbytes,hva);
    if(hva != 0){
        //iovec buffer, numbytes
        *hva = buf_ptr;
        hva += 1;
        *hva = numbytes;
         
        *iovec_ptr = *iovec_ptr + 16; 
        *iovec_cnt = *iovec_cnt + 1;
    }
}
    
int jadukata_syscall_readv_writev_helper(char, struct kvm_vcpu*, struct nitrodatau*);

void perform_padded_read_write(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    unsigned long shortoff = nu->u.rw.offset%(512);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);

    unsigned long iovec_ptr = 0, buf_ptr = 0;
    unsigned long iovec_cnt = 0;
    unsigned long orgsize;
    
    jprintk("perform padded read write \n");
    //first two sectors for buffers
    buf_ptr = nu->u.rw.fk.buffer;
    iovec_ptr = nu->u.rw.fk.buffer + 1024;
    iovec_cnt = 0;

    if(shortoff){
        padd_helper(vcpu,shortoff,&iovec_ptr,buf_ptr,&iovec_cnt);
    }

    //copy over the iovec from original i/o in between shortoff / excess
    guestmemcpy(vcpu, iovec_ptr,nu->u.rw.iov_ptr,16*nu->u.rw.iov_cnt);
    iovec_ptr += 16*nu->u.rw.iov_cnt;
    iovec_cnt += nu->u.rw.iov_cnt;

    if(excess){
        padd_helper(vcpu,(512-excess),&iovec_ptr,buf_ptr+512,&iovec_cnt);
    }
    
    nu->u.rw.iov_ptr = nu->u.rw.fk.buffer+1024;
    nu->u.rw.iov_cnt = iovec_cnt;
    orgsize = nu->u.rw.size;
    jadukata_syscall_readv_writev_helper('f', vcpu, nu);
    nu->u.rw.size = orgsize;

    jadukata_fake_syscall(vcpu, nu, padded_lseek, (unsigned long)(nu->u.rw.rw?PWRITEV_SCALL_NR:PREADV_SCALL_NR), 4, nu->u.rw.fd,nu->u.rw.iov_ptr,nu->u.rw.iov_cnt,nu->u.rw.offset-shortoff);
}

void read_padded_excess(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    unsigned long shortoff = nu->u.rw.offset%(512);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);
            
    jprintk("read padded excess %lu \n", (512-excess));
    if(excess){
        jadukata_fake_syscall(vcpu, nu, perform_padded_read_write, (unsigned long)PREAD_SCALL_NR, 4, nu->u.rw.fd,(nu->u.rw.fk.buffer+512),(512UL-excess), nu->u.rw.offset + nu->u.rw.size);
    }else{
        perform_padded_read_write(vcpu,nu);
    }
}

void read_padded_shortoff(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    unsigned long shortoff = nu->u.rw.offset%(512);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);

    jprintk("read padded shortoff %lu \n", shortoff);
    if(shortoff){
        jadukata_fake_syscall(vcpu, nu, read_padded_excess, (unsigned long)PREAD_SCALL_NR, 3, nu->u.rw.fd,nu->u.rw.fk.buffer,shortoff, nu->u.rw.offset-shortoff);
    }else{
        read_padded_excess(vcpu,nu);
    }
}

void read_padded_data(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    unsigned long shortoff = nu->u.rw.offset%(512);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);
    jprintk("read padded data excess %lu shortoff %lu \n", excess, shortoff);
    if(shortoff || excess){
        //read the padd data first for writes
        read_padded_shortoff(vcpu,nu);
    }
}

void alloc_process_buffer_sysret_handler_rwv(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    jprintk("alloc process buffer sysret handler rwv\n");
    alloc_process_buffer_secondhalf(vcpu,nu);
    read_padded_data(vcpu,nu);
}

void alloc_process_buffer_sysret_handler_rw(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    unsigned long iov_ptr;
    jprintk("alloc process buffer sysret handler rw\n");
    alloc_process_buffer_secondhalf(vcpu,nu);
    
    //tranform this read/write that needs padding to readv/write that needs padding
    //by creatging a iovec in end of buffer to make code common
    nu->u.rw.iov_ptr = nu->u.rw.fk.buffer + 4000;
    iov_ptr = nu->u.rw.iov_ptr;
    padd_helper(vcpu,nu->u.rw.size,&iov_ptr,nu->u.rw.ptr,&nu->u.rw.iov_cnt);
    
    read_padded_data(vcpu,nu); 
}

void padd_rw_helper(struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool rwv){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    unsigned long shortoff = nu->u.rw.offset%(512);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);
    void (*fn)(struct kvm_vcpu*,struct nitrodatau*) = rwv?alloc_process_buffer_sysret_handler_rwv:alloc_process_buffer_sysret_handler_rw;

    jprintk("padd %s excess %lu shortoff %lu \n", rwv?"rwv":"rw", excess, shortoff);
    if(shortoff || excess){
        short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu);
        if(mmap_needed){
            //mmap anonymous shared to allocate buffer space
            jadukata_fake_syscall(vcpu, nu, fn, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, 4096UL, 3UL, 40994UL, (unsigned long)-1, 0UL);
        }else{
            fn(vcpu,nu);
        }
    }
}

void padd_readv_writev(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    padd_rw_helper(vcpu,nu,true);
}

void padd_read_write(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    padd_rw_helper(vcpu,nu,false);
}
#endif

int jadukata_process_read_write(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu){
    int rc = 0;
#ifdef JADUKATA_CHECKSUM
    struct pchksum_args pargs;
    struct pair pairs[96];
    int numpairs=0;

    jprintk("syscall rw %s ptr %lx size %lu \n", nu->u.rw.rw?"WRITE":"READ", nu->u.rw.ptr, nu->u.rw.size);
    rc = jadukata_translate(vcpu, nu->u.rw.ptr, nu->u.rw.size, pairs, &numpairs);
    if(rc == 0) {
        init_pchksum_args(vcpu,&pargs,nu->u.rw.rw, nu->u.rw.fd,get_filehash_fd(vcpu->arch.cr3,nu->u.rw.fd),nu->u.rw.offset%512);
        checksum_caller(vcpu,nu->u.rw.rw,nu->u.rw.fd,pairs,numpairs,&pargs);
    }
#endif
    return rc;
}

int jadukata_syscall_read_write(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadwrite){
    int ret = 0;
    nu->u.rw.fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    jprintk("syscall read write %d \n", nu->u.rw.fd);
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
        nu->u.rw.ptr = kvm_register_read(vcpu, VCPU_REGS_RSI);
        nu->u.rw.size = kvm_register_read(vcpu, VCPU_REGS_RDX);
        if(preadwrite == false){
            nu->u.rw.offset = get_offset_fd(vcpu->arch.cr3, nu->u.rw.fd);
        }else{
            nu->u.rw.offset = kvm_register_read(vcpu, VCPU_REGS_R10);
        }
        nu->u.rw.rw = nu->out_syscall;
        jprintk("syscall fd %d rw %s ptr %lx size %lu offset %lu \n", nu->u.rw.fd, nu->u.rw.rw?"WRITE":"READ", nu->u.rw.ptr, nu->u.rw.size, nu->u.rw.offset);
#ifdef JADUKATA_CHECKSUM 
        unsigned long shortoff = nu->u.rw.offset%(512);
        unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);
        
        lock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size,true);
            
        if(shortoff || excess){
            //checksum calculation is handled as part of final readv, writev for unaligned i/o
            padd_read_write(vcpu,nu);
        }else{
            //only for writes
            if(nu->u.rw.rw == 0x1){
                ret = jadukata_process_read_write(vcpu,prefix,nu);
            }
        }
#endif
    }
    return ret;
}

int jadukata_sysret_read_write(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau* nu, bool preadwrite){
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(ret != -1){
        ret = 0;
        if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
            unlock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size);
            //only for reads
            if(nu->u.rw.rw == 0x0){
                ret = jadukata_process_read_write(vcpu,prefix,nu);
            }
            //update current offsets 
            if(preadwrite == false){
                increment_fd_offsets(vcpu->arch.cr3, nu->u.rw.fd, nu->u.rw.size);
            }
        }
    }
    return ret;
}

int jadukata_syscall_read(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu,nu,false);
}

int jadukata_sysret_read(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu, nu,false);
}

int jadukata_syscall_write(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu, nu,false);
}

int jadukata_sysret_write(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu, nu,false);
}
    
int jadukata_syscall_open(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    #define PATH_PATTERN "/mnt/disk"
    struct x86_exception ex;
    char path[100];
    unsigned long pathaddr = kvm_register_read(vcpu, VCPU_REGS_RDI);
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,pathaddr, &path, sizeof(path),&ex);
    jprintk("syscall open path %s \n", path);
    if(strncmp(path, PATH_PATTERN, strlen(PATH_PATTERN)) == 0){
        nu->u.op.match = 1;
        nu->u.op.hash = get_new_file_key(path);
    }
    return 0;
}

int jadukata_sysret_open(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    if(nu->u.op.match == 1){
        int fd = kvm_register_read(vcpu, VCPU_REGS_RAX);
        if(fd != -1){
            jprintk("sysret open adding monitored fd %d \n", fd);
            add_monitored_fd(vcpu->arch.cr3,fd,nu->u.op.hash);
        }
        //nu->u.op.match = 0;
    }
    return 0;
}

/*
 * close for some reason does not have a sysret trap ( TODO : look into it)
 * for now, we assume all close syscalls are successful
 */
int jadukata_syscall_close(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.cl.close_fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    del_monitored_fd(vcpu->arch.cr3,nu->u.cl.close_fd);
    jprintk("syscall close fd %d \n", nu->u.cl.close_fd);
    return 0;
}

int jadukata_sysret_close(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(ret != 0){
        jprinte("ERROR : assumed close successful while it was not sysret close ret %d fd %d \n", ret, nu->u.cl.close_fd);
    }
    //nu->u.cl.close_fd = 0;
    return 0;
}

int jadukata_syscall_stat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_stat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_fstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_lstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_lstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_lseek(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.ls.fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    return 0;
}

int jadukata_sysret_lseek(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.ls.fd)){
        int offset = kvm_register_read(vcpu, VCPU_REGS_RAX);
        if(offset != -1){
            update_fd_offsets(vcpu->arch.cr3,nu->u.ls.fd,offset);
        }
    }
    return 0;
}

int print_mmap(struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    struct x86_exception ex;
	unsigned long gpa = kvm_mmu_gva_to_gpa_system(vcpu, (gva_t)nu->u.mm.mmap_ptr, &ex);
    jprintk("syscall io_mmap gva cr3 %lx ptr %lx len %lx gpa %lx \n", vcpu->arch.cr3,nu->u.mm.mmap_ptr, nu->u.mm.mmap_len, gpa);
    return 0;
}

int jadukata_syscall_mmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.mm.mmap_len = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.mm.mmap_fd = kvm_register_read(vcpu, VCPU_REGS_R8);
    jprintk("syscall mmap len %lu fd %d \n", nu->u.mm.mmap_len, nu->u.mm.mmap_fd);
    return 0;
}

int jadukata_sysret_mmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.mm.mmap_fd)){
        nu->u.mm.mmap_ptr = (void*)kvm_register_read(vcpu, VCPU_REGS_RAX);
        if(nu->u.mm.mmap_ptr != ((void*)-1)){
            //for mmap code is positive
            monitor_virt_range(vcpu,(unsigned long)nu->u.mm.mmap_ptr,nu->u.mm.mmap_len,MMAP_GPA);
        }
    }
    jprintk("sysret mmap ptr %lx len %lu fd %d \n", nu->u.mm.mmap_ptr, nu->u.mm.mmap_len, nu->u.mm.mmap_fd);
    //nu->u.mm.mmap_ptr = 0;
    //nu->u.mm.mmap_len = 0;
    //nu->u.mm.mmap_fd = 0;
    return 0;
}

int jadukata_syscall_munmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.um.munmap_ptr = (void*)kvm_register_read(vcpu, VCPU_REGS_RDI);
    jprintk("syscall munmap ptr %lx \n", nu->u.um.munmap_ptr);
    return 0;
}

int jadukata_sysret_munmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(ret == 0){
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        unsigned long vaddrs = (unsigned long)nu->u.um.munmap_ptr;
        unsigned long vaddre = vaddrs;
        while(ht_lookup(m->v2p,vaddre) != 0){
            vaddre += PAGE_SIZE;
        }
        jprintk("sysret munmap ptr ret %d vaddrs %lx vaddre %lx \n", ret, vaddrs,vaddre);
        unmonitor_virt_range(vcpu,vaddrs,(vaddre-vaddrs),MMAP_GPA);
    }
    //nu->u.um.munmap_ptr = 0;
    return 0;
}

int jadukata_syscall_pread64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu,nu,true);
}

int jadukata_sysret_pread64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu,nu,true);
}

int jadukata_syscall_pwrite64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu,nu,true);
}

int jadukata_sysret_pwrite64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu,nu,true);
}

int jadukata_process_readv_writev(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu){
    int rc = 0;
#ifdef JADUKATA_CHECKSUM
    int i;
    struct pchksum_args pargs;
    struct pair pairs[96];
    int numpairs=0;
    gva_t addr;
    unsigned long size;
    int shortoff, excess, chks = ~0, chke = ~0;
    struct x86_exception ex;

    unsigned long ptr = (unsigned long)nu->u.rw.iov_ptr;
    for(i=0;i<nu->u.rw.iov_cnt;i++){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, ptr, &addr, sizeof(void*),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, ptr+8, &size, sizeof(unsigned long),&ex);
        rc |= jadukata_translate(vcpu, addr, size, pairs, &numpairs);
        ptr += sizeof(struct iovec);
    }
    if(rc == 0){
        init_pchksum_args(vcpu,&pargs,nu->u.rw.rw, nu->u.rw.fd,get_filehash_fd(vcpu->arch.cr3,nu->u.rw.fd),nu->u.rw.offset%512);
        checksum_caller(vcpu,nu->u.rw.rw,nu->u.rw.fd,pairs,numpairs,&pargs);
    }
#endif
    return rc;
}

int jadukata_syscall_readv_writev_helper(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    int i,ret =0;
#ifdef JADUKATA_CHECKSUM 
    unsigned long ptr;
    struct x86_exception ex;
    unsigned long addr;
    unsigned long size;

    ptr = (unsigned long)nu->u.rw.iov_ptr;
    nu->u.rw.size = 0; 
    
    for(i=0;i<nu->u.rw.iov_cnt;i++){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)ptr, &addr, sizeof(void*),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)(ptr+8), &size, sizeof(unsigned long),&ex);
        jprintk("iov %d ptr %lx addr %lx size %lu \n", i,ptr,addr,size);
        ptr += 16;
        nu->u.rw.size += size;
        lock_virt_buffer(vcpu,addr,size,true);
    }

    unsigned long shortoff = nu->u.rw.offset%(512);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(512);
    
    jprintk("syscall rwv %s ptr %lx count %lu size %lu offset %lu \n", nu->u.rw.rw?"WRITE":"READ", nu->u.rw.iov_ptr, nu->u.rw.iov_cnt, nu->u.rw.size, nu->u.rw.offset);
    jprintk("syscall readv writev helper size %d excess %lu shortoff %lu \n", nu->u.rw.size, excess, shortoff);

    if(shortoff || excess){
        padd_readv_writev(vcpu,nu);
    }else{
        //only for writes
        if(nu->u.rw.rw == 0x1){
            ret = jadukata_process_readv_writev(vcpu,prefix,nu);
        }
    }
#endif
    return ret;
}

int jadukata_syscall_readv_writev(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadvwritev){
    int ret = 0;
    nu->u.rw.rw = (nu->out_syscall == WRITEV_SCALL_NR);
    nu->u.rw.fd = (int)kvm_register_read(vcpu, VCPU_REGS_RDI);
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
        nu->u.rw.iov_ptr = (unsigned long)kvm_register_read(vcpu, VCPU_REGS_RSI);
        nu->u.rw.iov_cnt = (unsigned long)kvm_register_read(vcpu, VCPU_REGS_RDX);
        if(preadvwritev == false){
            nu->u.rw.offset = get_offset_fd(vcpu->arch.cr3, nu->u.rw.fd);
        }else{
            nu->u.rw.offset = (unsigned long)kvm_register_read(vcpu, VCPU_REGS_R10);
        }
        ret = jadukata_syscall_readv_writev_helper(prefix,vcpu,nu);
    }
    return ret;
}

int jadukata_sysret_readv_writev(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadvwritev){
    unsigned long addr;
    unsigned long size;
    int i;
    struct x86_exception ex;

    int ret = (int)kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(ret != -1){
        if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
            char* ptr = (char*)nu->u.rw.iov_ptr;
            //only for reads
            if(nu->u.rw.rw == 0x0){
                ret = jadukata_process_readv_writev(vcpu,prefix,nu);
            }
            for(i=0;i<nu->u.rw.iov_cnt;i++){
                kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)ptr, &addr, sizeof(void*),&ex);
                kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)(ptr+8), &size, sizeof(unsigned long),&ex);
                unlock_virt_buffer(vcpu,addr,size);
                ptr += sizeof(struct iovec);
            }
            if(preadvwritev == false){
                //update current offsets 
                increment_fd_offsets(vcpu->arch.cr3, nu->u.rw.fd, nu->u.rw.size);
            }
        }
    }
    return ret;
}

int jadukata_syscall_readv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_sysret_readv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_syscall_writev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_sysret_writev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_syscall_msync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_msync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_madvise(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_madvise(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

extern spinlock_t newprocess_lock;
extern hash_table *ht_new_pids;

void process_helper(struct kvm_vcpu* vcpu, int sharedvm){
        int pid = kvm_register_read(vcpu, VCPU_REGS_RAX);
        if(pid != -1 && pid != 0){
            jprintk("new child process created with pid %d parent cr3 %lx sharedvm %d \n", pid, vcpu->arch.cr3, sharedvm);
            if(sharedvm == 0){
                ht_add_val(ht_new_pids,pid,vcpu->arch.cr3);
            }else{
                add_targeted_process(vcpu->arch.cr3,vcpu->arch.cr3,pid);
            }
        }else{
            jprinte("ERROR failed new child process creation pid %d parent cr3 %lx sharedvm %d \n", pid, vcpu->arch.cr3, sharedvm);
        }
}

int jadukata_syscall_clone(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int flags = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.cn.sharedvm = 0;
    if(flags & CLONE_VM){
        nu->u.cn.sharedvm = 1;
    }
    nu->u.cn.ptid = kvm_register_read(vcpu, VCPU_REGS_RDX);
    nu->u.cn.ctid = kvm_register_read(vcpu, VCPU_REGS_R10);
    return 0;
}

int jadukata_sysret_clone(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    process_helper(vcpu,nu->u.cn.sharedvm);
    return 0;
}

int jadukata_syscall_fork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    process_helper(vcpu,0);
    return 0;
}

int jadukata_syscall_vfork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_vfork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    process_helper(vcpu,0);
    return 0;
}

int jadukata_syscall_exit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int pid = get_guest_pid(vcpu);
    remove_targeted_process_core(vcpu->arch.cr3,pid);  
    return 0;
}

int jadukata_sysret_exit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_kill(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.kl.pid = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.kl.signal = kvm_register_read(vcpu, VCPU_REGS_RSI);
    return 0;
}

int jadukata_sysret_kill(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long cr3;
    if(ret == 0){
        switch(nu->u.kl.signal){
            case 1:
            case 2:
            case 3:
            case 9:
            case 15:
                cr3 = targeted_process_pid_to_cr3(nu->u.kl.pid);
                if(cr3 != 0){
                    remove_targeted_process_core(cr3,nu->u.kl.pid);
                }
                break;
            default:
                jprinte("ERROR uncaptured kill signal %d for pid %d\n", nu->u.kl.signal, nu->u.kl.pid);
        }
    }
    return 0;
}

int jadukata_syscall_fsync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fsync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_fdatasync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fdatasync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_creat(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_creat(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_sync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_sync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_io_setup(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_io_setup(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_io_destroy(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_io_destroy(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_io_getevents(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {

    nu->u.ig.iogetevents_minnr = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.ig.iogetevents_nr = kvm_register_read(vcpu, VCPU_REGS_RDX);
    nu->u.ig.iogetevents_ptr = kvm_register_read(vcpu, VCPU_REGS_R10);
    jprintk("syscall io_getevents minnr %ld nr %ld ptr %lx \n", nu->u.ig.iogetevents_minnr, nu->u.ig.iogetevents_nr, nu->u.ig.iogetevents_ptr);
    return 0;
}

//TODO: handle async i/o small unaligned writes, reads
int jadukata_process_reada_writea(struct kvm_vcpu *vcpu, char prefix, int rw, int fd, unsigned long ptr, unsigned long size, long long offset){
    int rc = 0;
#ifdef JADUKATA_CHECKSUM
    struct pchksum_args pargs;
    struct pair pairs[96];
    int numpairs=0;

    jprintk("syscall rwa %s ptr %lx size %lu offset %lld \n", rw?"WRITE":"READ", ptr, size, offset);
    rc = jadukata_translate(vcpu, ptr, size, pairs, &numpairs);
    if(rc == 0) {
        init_pchksum_args(vcpu,&pargs,rw, fd,get_filehash_fd(vcpu->arch.cr3,fd),offset%512);
        checksum_caller(vcpu,rw,fd,pairs,numpairs,&pargs);
    }
#endif
    return rc;
}

int jadukata_sysret_io_getevents(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    long i = 0;
    struct x86_exception ex;
    unsigned long eventptr;
    unsigned long iocb_ptrptr;
    unsigned long iocb_ptr;
    unsigned long res_ptr;
    unsigned long res;
    unsigned long res2_ptr;
    unsigned long res2;
    unsigned long buf_addr;
    unsigned long nbytes;
    long long offset;
    short opcode = 1111;
    int fd;
    long nr;
    int ret = 0;

    nr = kvm_register_read(vcpu, VCPU_REGS_RAX);
    
    for(i =0; i < nr; i++){
        eventptr = nu->u.ig.iogetevents_ptr+(i*32);
        iocb_ptrptr = eventptr + 8;
        res_ptr = eventptr + 16; 
        res2_ptr = eventptr + 24;
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,iocb_ptrptr, &iocb_ptr, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, res_ptr, &res, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, res2_ptr, &res2, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 16, &opcode, sizeof(short),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 20, &fd, sizeof(int),&ex);
        if(is_monitored_fd(vcpu->arch.cr3,fd)){
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 24, &buf_addr, sizeof(unsigned long),&ex);
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 32, &nbytes, sizeof(unsigned long),&ex);
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 40, &offset, sizeof(long long),&ex);
            jprintk("iogetevents process iocb %lx res %lu res2 %lu buf %lx opcode %hd fd %d bytes %lu offset %lld \n", iocb_ptr, res, res2, buf_addr, opcode, fd, nbytes, offset);
            unlock_virt_buffer(vcpu,buf_addr,nbytes);

            //only for reads
            if(opcode == 0){
                ret |= jadukata_process_reada_writea(vcpu,prefix,0,fd,buf_addr,nbytes,offset);
            }
        }
    }
    
    //nu->u.ig.iogetevents_minnr = 0;
    //nu->u.ig.iogetevents_nr = 0;
    //nu->u.ig.iogetevents_ptr = 0;
    return ret;
}

//async logic outline:

// for async writes, we calculate checksums during io_submit in the syscall ( if we were to do this in sysret it would add unlikely risk that the i/o might finish before we note down the checksum, but with doing this ins syscall we might do more than one checksum calculations with failures and retries but that is fine )

// for asycn reads, we calculate checksums during the io_getevents sysret ( to work on only successfully finished i/os that the disk has seen )

//TODO : handle small i/o for async
int jadukata_syscall_io_submit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    long i = 0;
    struct x86_exception ex;
    unsigned long iocb_ptr;
    short opcode = 1111;
    int fd;
    unsigned long buf_addr;
    unsigned long nbytes;
    long long offset;
    int ret =0;

    nu->u.is.iosubmit_nr = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.is.iosubmit_ptr = kvm_register_read(vcpu, VCPU_REGS_RDX);
    jprintk("syscall iosubmit nr %lu ptr %lx \n", nu->u.is.iosubmit_nr, nu->u.is.iosubmit_ptr);
    
    for(i =0; i < nu->u.is.iosubmit_nr; i++){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.is.iosubmit_ptr+(i*sizeof(unsigned long)), &iocb_ptr, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 16, &opcode, sizeof(short),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 20, &fd, sizeof(int),&ex);
        if(is_monitored_fd(vcpu->arch.cr3,fd)){
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 24, &buf_addr, sizeof(unsigned long),&ex);
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 32, &nbytes, sizeof(unsigned long),&ex);
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, iocb_ptr + 40, &offset, sizeof(long long),&ex);
            jprintk("iosubmit process iocb %lx opcode %hd fd %d buf %lx nbytes %lu offset %lld \n", iocb_ptr, opcode, fd, buf_addr, nbytes, offset);
            lock_virt_buffer(vcpu,buf_addr,nbytes,true);

            //only for writes
            if(opcode == 1){
                ret |= jadukata_process_reada_writea(vcpu,prefix,1,fd,buf_addr,nbytes,offset);
            }
        }
    }
    return 0;
}

int jadukata_sysret_io_submit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
//    long nr = kvm_register_read(vcpu, VCPU_REGS_RAX);

    jprintk("sysret iosubmit nr %ld syscall nr %lu ptr %lx \n", nu->u.is.iosubmit_nr, nu->u.is.iosubmit_nr, nu->u.is.iosubmit_ptr);

    //nu->u.is.iosubmit_nr = 0;
    //nu->u.is.iosubmit_ptr = 0;
    return 0;
}

int jadukata_syscall_io_cancel(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_io_cancel(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_remap_file_pages(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_remap_file_pages(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_sync_file_range(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_sync_file_range(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_preadv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_sysret_preadv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_syscall_pwritev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_sysret_pwritev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_syscall_syncfs(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_syncfs(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

#define HANDLERS
#define HNAME(name) jadukata_syscall_##name
#include "handlers.h"
#undef HNAME

#define HNAME(name) jadukata_sysret_##name
#include "handlers.h"
#undef HNAME

int jadukata_syscall(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
#ifndef NO_SYSCALL_HANDLING
    nu->prefix = prefix;
    if(!nu->fake_syscall){
        if(nu->out_syscall < MAX_SYSCALLS && jadukata_syscall_handlers[nu->out_syscall]){
            return jadukata_syscall_handlers[nu->out_syscall](prefix,vcpu,nu);
        }
    }
#endif
    return 1;
}

int jadukata_sysret(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifndef NO_SYSCALL_HANDLING
    nu->prefix = prefix;
    if(!nu->fake_syscall){
        if(nu->out_syscall < MAX_SYSCALLS){
            if(jadukata_sysret_handlers[nu->out_syscall]){
                return jadukata_sysret_handlers[nu->out_syscall](prefix,vcpu,nu);
            }
        }
    }else{
        jadukata_fake_sysret(vcpu,nu);
    }
#endif
    return 1;
}

#endif    
