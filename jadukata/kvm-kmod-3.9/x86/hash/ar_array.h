#define KEYDT unsigned long
#define VALDT unsigned long

typedef struct {
    char name[30];
    int buckets;
    int bucket_size;
    VALDT** array;
    int** ifEmptyArray;
    int size;
    int time;
} array_table;

int ar_add(array_table *ar, KEYDT key);
int ar_add_val(array_table *ar, KEYDT key, VALDT val);
int ar_lookup(array_table *ar, KEYDT key);
void ar_increment(array_table *ar, KEYDT key);
int ar_lookup_val(array_table *ar, KEYDT key, VALDT *val);
int ar_lookup_val_blm(array_table *ar, KEYDT key, VALDT *val);
int ar_remove(array_table *ar, KEYDT key);
int ar_remove_val(array_table *ar, KEYDT key, VALDT *val);
int ar_create_with_size(array_table *ar, char *name, int size);
int ar_destroy(array_table *ar);
int ar_get_size(array_table *ar);
void ar_reset(array_table *ar);

