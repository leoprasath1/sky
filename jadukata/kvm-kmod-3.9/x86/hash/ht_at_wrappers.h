#ifndef __INCLUDE_HT_AT_WRAPPERS__
#define __INCLUDE_HT_AT_WRAPPERS__

#include "hash2.h"
#include "avl_tree.h"
#include "ar_array.h"
struct keyval {
	unsigned long key;
	unsigned long val;
	struct list_head list;
};

typedef struct avl_tree {
	struct tree *tree;
	spinlock_t lock;
	int num;
	char name[1024];
	int cur_key_val;
} avl_tree;

typedef struct hash_table {
	char name[1024];
	hashtable *table;
	spinlock_t lock;
	struct semaphore scan_sem;
	int past_size;
    bool debug;
} hash_table;

#define HT_AT_lock(a)           unsigned long HTATflags; spin_lock_irqsave(a,HTATflags);
#define HT_AT_unlock(a)         spin_unlock_irqrestore(a,HTATflags)
#define HT_AT_lock_init(a)      spin_lock_init(a)

int ht_add(hash_table *ht, KEYDT key);
int ht_add_val(hash_table *ht, KEYDT key, VALDT val);
int ht_update_val(hash_table *ht, KEYDT key, VALDT val);
int ht_lookup(hash_table *ht, KEYDT key);
int ht_lookup_val(hash_table *ht, KEYDT key, VALDT *val);
int htn_remove(hash_table *ht, KEYDT key);
int htn_remove_val(hash_table *ht, KEYDT key, VALDT *val);
int ht_remove(hash_table *ht, KEYDT key);
int ht_remove_val(hash_table *ht, KEYDT key, VALDT *val);
int ht_open_scan(hash_table *ht);
int ht_scan(hash_table *ht, KEYDT *blk);
int ht_scan_val(hash_table *ht, KEYDT *blk,VALDT *val);
int ht_close_scan(hash_table *ht);
int ht_pop(hash_table *ht, KEYDT *key);
int ht_pop_val(hash_table *ht, KEYDT *key, VALDT* val);
int ht_create_with_size(hash_table **ht, char *name, int size);
int ht_create_with_customhash(hash_table **ht, char *name, int size,unsigned long(*customhashfn)(KEYDT));
int ht_create(hash_table **ht, char *name);
int ht_destroy(hash_table *ht);
void ht_print(hash_table *ht);
int ht_clear(hash_table *ht);
int ht_get_size(hash_table *ht);
void ht_set_debug(hash_table *ht);
void ht_unset_debug(hash_table *ht);

int at_create(avl_tree **t, char *name);
int at_find_closest_before(avl_tree *t, unsigned long key, unsigned long* res_key, unsigned long* res_val);
int at_find_closest_after(avl_tree *t, unsigned long key, unsigned long* res_key, unsigned long* res_val);
int at_destroy(avl_tree *t);
int at_removeall(avl_tree *t);
int at_num_elements(avl_tree *t);
int at_add_val(avl_tree *t,  unsigned long key, unsigned long val);
int at_remove(avl_tree *t, unsigned long key);
int at_lookup(avl_tree *t, unsigned long key);
int at_lookup_val(avl_tree *t, unsigned long key, unsigned long *val);
int at_increment_val(avl_tree *t, unsigned long key);
int at_open_scan(avl_tree *t);
int at_scan(avl_tree *t, unsigned long *key);
int at_scan_val(avl_tree *t, unsigned long *key, unsigned long *val);

void remove_interval(avl_tree*, array_table*, unsigned long key, unsigned long val);
void update_interval(avl_tree*, array_table*, unsigned long key, unsigned long val);
int lookup_interval(avl_tree *tree, array_table*, unsigned long,int, unsigned long*, unsigned long*);
void print_interval_tree(avl_tree *,array_table*);

#endif
