/*
 * nitro.h
 *
 *  Created on: Nov 6, 2009
 *      Author: pfoh
 */


/************************************** Global options for nitro ****************************************/

#define DUM_SEG_SELECT 		0xFFFF			/* Value to be used to simulate the absence 	*/
							/* of IDT entries */
#undef SHADOW_IDT					/* Whether to use the shadow idt technique 	*/
//#define DEBUG_INTERRUPTS 	1			/* Switch for debugging interrupt emulation 	*/
//#define USE_NETLINK		1			/* Switch between netlink and dmesg (printk) 	*/

/************* Nothing below this line should be changed, configure only the macros above! **************/
/********************************************************************************************************/

#include "kvm_host.h"

#define ABS(x) (x>=0?x:-x)
#define ABSDIFF(x,y) (((x)>(y))?(x-y):(y-x))
#define TIMEDIFF(e,s) (((e.tv_sec-s.tv_sec)*1000000000) + (e.tv_nsec-s.tv_nsec))

#define READ_SCALL_NR (0)
#define WRITE_SCALL_NR (1)
#define LSEEK_SCALL_NR (8)
#define MMAP_SCALL_NR (9)
#define PREAD_SCALL_NR (17)
#define PWRITE_SCALL_NR (18)
#define READV_SCALL_NR (19)
#define WRITEV_SCALL_NR (20)
#define PREADV_SCALL_NR (295)
#define PWRITEV_SCALL_NR (296)

#define page_boundary(x) ((x/PAGE_SIZE)*PAGE_SIZE)

#define MMAP_GPA (1)
#define VBUF_GPA (2)
#define GPA_CODE_MASK (0xC000000000000000)
#define GPA_ENCODE(cr3,x) ((((unsigned long)x)<<62) | cr3)
#define GPA_DECODE_VAL(x) ((((unsigned long)x)&GPA_CODE_MASK)>>62)
#define GPA_DECODE_CR3(x) (((unsigned long)x)&~GPA_CODE_MASK)

#define PACK_INTS_TO_LONG(x,y) ((((unsigned long)x)<<32)|y)
#define UNPACK_INTS_FROM_LONG_X(l) (((unsigned long)l)>>32)
#define UNPACK_INTS_FROM_LONG_Y(l) (((unsigned long)l)&0xFFFFFFFF)

typedef int (*virt_range_process_t)(struct kvm_vcpu*,unsigned long,unsigned long, unsigned long, unsigned long, unsigned long, int);

void nu_init(struct nitrodatau *vcpu);
int nitro_kvm_init(struct kvm_vcpu *vcpu);
int nitro_kvm_exit(struct kvm_vcpu *vcpu);

unsigned long get_new_file_key(char* path);
unsigned long get_offset_fd(unsigned long cr3, unsigned long fd);
int get_filehash_fd(unsigned long cr3, unsigned long fd);
int get_filesize_fd(unsigned long cr3, unsigned long fd);
unsigned long update_fd_offsets(unsigned long cr3, unsigned long fd, unsigned long newsize);
unsigned long increment_fd_offsets(unsigned long cr3, unsigned long fd, unsigned long increment);
int is_monitored_fd(unsigned long cr3, unsigned long fd);
void del_monitored_fd(unsigned long cr3, unsigned long fd);
void add_monitored_fd(unsigned long cr3, unsigned long fd, unsigned long hash);
void drop_process_data(unsigned long cr3);
struct process_data* get_process_data(unsigned long cr3);

void nitro_handle_guest_task_switch(struct kvm_vcpu *vcpu, unsigned long newcr3);

int is_targeted_process(unsigned long cr3);
int remove_targeted_process(unsigned long cr3);
int remove_targeted_process_core(unsigned long cr3,int pid);
int add_targeted_process(unsigned long cr3,unsigned long parentcr3,unsigned long pid);
unsigned long targeted_process_pid_to_cr3(unsigned long pid);
int get_guest_pid(struct kvm_vcpu* vcpu);
void guestmemcpy(struct kvm_vcpu *vcpu, unsigned long dst, unsigned long src, int numbytes);

int add_tracked_pte(gpa_t cr3, gpa_t ptep,int code);
int del_tracked_pte(gpa_t ptep);

int clear_command(void);
int status_command(void* data);
int set_task_ptrptr(char* buf);
int set_vmmcomm_base(char* buf);
int unset_vmmcomm_base(void);

int add_virt_range_gpa(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep, int code);
int del_virt_range_gpa(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long ignore0, unsigned long ignore1, unsigned long ignore2, unsigned long ptep, int code);
int process_virt_range_ptep(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long ignore0, unsigned long ignore1, unsigned long ignore2, unsigned long ptep,int code);

int add_remap_task(struct process_data *m, unsigned long vaddr, unsigned long len);
unsigned long is_jadu_write_protected(gpa_t gpa,gva_t *gva);
int handle_tracked_pte(struct kvm_vcpu *vcpu, gpa_t gpa,unsigned long cr3);
int handle_tracked_rw(struct kvm_vcpu *vcpu, gpa_t gpa,unsigned long vaddr);

void lock_virt_buffer(struct kvm_vcpu *vcpu, unsigned long vaddr_start, unsigned long len, bool acquire);
void unlock_virt_buffer(struct kvm_vcpu *vcpu, unsigned long vaddr_start, unsigned long len);

int monitor_virt_range(struct kvm_vcpu *vcpu,unsigned long vaddrs, unsigned long len,int code);
int unmonitor_virt_range(struct kvm_vcpu *vcpu, unsigned long vaddrs, unsigned long len, int code);

int nitro_test(char* input);
int nitro_test_helper(struct kvm_vcpu* vcpu);
int nitro_test_helper1(struct kvm_vcpu* vcpu, unsigned long);

int nitro_mod_init(void);
int nitro_mod_exit(void);

int jadukata_syscall(char, struct kvm_vcpu*, struct nitrodatau *nu);
int jadukata_sysret(char, struct kvm_vcpu*, struct nitrodatau *nu);
hva_t jadukata_gva_to_hva(struct kvm_vcpu *vcpu, gva_t gva);

int handle_gp(struct kvm_vcpu *vcpu, struct kvm_run *kvm_run);
int handle_ud(struct kvm_vcpu *vcpu, struct kvm_run *kvm_run);
int syscall_hook(char prefix, struct x86_emulate_ctxt *ctxt);
int sysret_hook(char prefix, struct x86_emulate_ctxt *ctxt);

void get_process_hardware_id(struct kvm_vcpu *vcpu, unsigned long *cr3, u32 *verifier, unsigned long *pde);

int handle_asynchronous_interrupt(struct kvm_vcpu *vcpu);
int emulate_int_prot(struct x86_emulate_ctxt *ctxt, struct x86_emulate_ops *ops, int irq);
int kvm_read_guest_virt_system(struct x86_emulate_ctxt *ctxt,gva_t addr, void *val, unsigned int bytes,struct x86_exception *exception);
int kvm_write_guest_virt_system(struct x86_emulate_ctxt *ctxt,gva_t addr, void *val,unsigned int bytes,struct x86_exception *exception);

int netlink_init(void);
void netlink_exit(void);

extern char* syscall_name(int scallnr);
extern const int MAX_SYSCALLS;
