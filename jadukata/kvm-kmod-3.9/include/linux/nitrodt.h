#include "hash/ht_at_wrappers.h"
#include "jadu.h"

#ifdef SHADOW_IDT
struct shadow_idt {
	__u64 base;
	__u16 limit;
	u8 *table;
};
#endif

enum cpu_mode {
	UNDEF,
	PROT,
	PAE,
	LONG
};

//extern int ndbg;

struct process_data{
    spinlock_t lock;
    unsigned long cr3;
    bool bufferuse[MAX_THREADS_SMALL_IO];
    unsigned long buffer[MAX_THREADS_SMALL_IO];
    hash_table* v2p;
    hash_table* p2v;
    hash_table* pending; 
    hash_table* vbe;
    hash_table* mfds;
    hash_table* fdoffsets;
};

struct nitrodatau;

struct fakescall{
    bool last;
    int numargs;
    unsigned long regs[MAXFAKESCALLARGS];
    int bufferuse;
    unsigned long buffer;
    void (*chainfn)(struct kvm_vcpu*,struct nitrodatau*);
};

struct readwrite{
    unsigned long fd;
    short rw;
    unsigned long offset;
    unsigned long size;
    struct fakescall fk;
    //read/write
    unsigned long ptr;
    //readv/writev
    unsigned long iov_ptr;
    unsigned long iov_cnt;
};

struct iosubmit{
    //iosubmit
    long iosubmit_nr;
    unsigned long iosubmit_ptr;
};

struct iogetevents{
    //iogetevents
    long iogetevents_minnr;
    long iogetevents_nr;
    unsigned long iogetevents_ptr;
};

struct mmap{
    //mmap
    int mmap_fd;
    unsigned long mmap_len;
    void* mmap_ptr;
};

struct munmap{
    //munmap
    void* munmap_ptr;
};

struct open{
    //open 
    int match;
    unsigned long hash;
};

struct close{
    //close 
    int close_fd;
};

struct clone{
    //close 
    int sharedvm;
    unsigned long ptid,ctid;
};

struct kill{
    //kill
    int pid;
    int signal;
};

struct lseek{
    //lseek
    int fd;
};

struct nitrodatau{
    char prefix;
    short fake_syscall;
    unsigned long out_syscall; // outstanding syscall
    unsigned long id; // to match split syscalls
//  unsigned long temp, max;
    union {
        struct readwrite rw;
        struct iosubmit is;
        struct iogetevents ig;
        struct mmap mm;
        struct munmap um;
        struct open op;
        struct close cl;
        struct clone cn;
        struct kill kl;
        struct lseek ls;
    } u;
};

struct nitro_data{
	int running;
	char id[16];
	u64 sysenter_cs_val;
	u64 efer_val;
	u8 idt_int_offset;
	u8 idt_replaced_offset;
	//int pae;
	enum cpu_mode mode;
	int idt_entry_size;
	int syscall_reg;
	int no_int;
    unsigned long task_ptrptr;
    unsigned long scall_count, sret_count;
    unsigned long scall_time, sret_time;
    int first_scall; // whether next syscall is first after process scheduling
    unsigned long pnetlink;
#ifdef SHADOW_IDT
	struct shadow_idt shadow_idt;
#endif
    struct nitrodatau nu;
    char copybuf[4096];
};

struct gate_descriptor{
	u16 offset_low;
	u16 seg_selector;
	u16 flags;
	u16 offset_high;
};

