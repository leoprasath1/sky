#include <linux/smp.h>
#include <linux/percpu.h>
#include <linux/sched.h>

#define JADUKATA //syscall trapping fwk
//#define JADUKATA_VERBOSE
//#define JADUKATA_VMM_COMM
#define JADUKATA_CHECKSUM // metadata/data classification
//#define TEST_MODE //does more checks that are not needed in production
//#define NO_ON_OFF // keep syscall monitoring on at all times
//#define NO_SYSCALL_HANDLING
//#define LOCK_UNLOCK_BUFFER

#ifndef JADUKATA
#undef JADUKATA_VERBOSE
#undef JADUKATA_VMM_COMM
#undef JADUKATA_CHECKSUM
#endif

#define BIG_FILE_SIZE (10*1024*1024)

#define MAX_THREADS_SMALL_IO (10)
#define MAXFAKESCALLARGS (6)

//#define JPRINTK

#define jprinte(x,y...) do { printk( KERN_ERR "nitro %s:%d - %d %s %d - " x,__FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##y); } while (0);
#ifdef JPRINTK
//#define jprintk(x,y...) do { if(ndbg) printk( KERN_ERR "%s:%s %d - %d %s %d - " x,__FILE__,__FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(),y); } while (0)
#define jprintk(x,y...) do { printk( KERN_ERR "nitro %s:%d - %d %s %d - " x,__FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##y); } while (0);
#else
#define jprintk(x,y...) do { } while(0); 
#endif


