#!/bin/bash -x

pid=`ps auwx | grep bash | head -n 1 | tr -s ' ' | cut -d' ' -f2`

echo $pid

echo "ftrace init"
echo 'echo "function" >/sys/kernel/debug/tracing/current_tracer' | sudo bash -x
echo 'echo "409600" > /sys/kernel/debug/tracing/buffer_size_kb' | sudo bash -x
echo 'echo "" > /sys/kernel/debug/tracing/trace' | sudo bash -x
echo 'echo 1 > /sys/kernel/debug/tracing/options/func_stack_trace' | sudo bash -x
echo 'echo "kmalloc" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "process_checksum_success" > /sys/kernel/debug/tracing/set_graph_function' | sudo bash -x
#echo 'echo "*:mod:kvm" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "*:mod:kvm_intel" >> /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "*:mod:bcache" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "*:mod:DISKDRIVER" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x

#echo 'echo 1 > /sys/kernel/debug/tracing/tracing_on' | sudo bash -x
