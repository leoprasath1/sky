#!/bin/bash 

sudo rmmod -v kvm_intel
sudo rmmod -v kvm

#sudo modprobe kvm
#sudo modprobe kvm_intel

sudo insmod /home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/kvm.ko
sudo insmod /home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/kvm-intel.ko #ept=0

#sudo logrotate -f /etc/logrotate.d/rsyslog
sudo bash -c " echo 1 >  /sys/module/kvm/parameters/ndbg "

#cmdserver process
ccount=`ps auwx | grep cmdserver | wc -l`
if [ $ccount -gt 1 ]; then
    processes=`ps auwx | grep cmdserver`
    echo "stopping already running cmdserver processes.. $ccount"
    echo "$processes"
    `ps auwx | grep cmdserver | tr -s ' ' | cut -d' ' -f2 | xargs -i kill {}`
    sleep 5;
fi 

echo "starting cmdserver"
#start the cmdserver
`~/cerny/jadukata/cmdserver.o netconsole > ~/cerny/jadukata/cmdserver.log ` &
#`~/cerny/jadukata/cmdserver.o > ~/cerny/jadukata/cmdserver.log ` &

qemu_cmd="/home/arulraj/cerny/jadukata/qemu-2.5.0/x86_64-softmmu/qemu-system-x86_64"
#qemu_cmd="/home/arulraj/cerny/jadukata/qemu-1.6.1/x86_64-softmmu/qemu-system-x86_64"
#qemu_cmd="/home/arulraj/qemu-src/nitro/x86_64-softmmu/qemu-system-x86_64"
#qemu_cmd="/usr/bin/qemu-system-x86_64"
#qemu_cmd="/home/arulraj/qemu-src/qemu-1.5.0/x86_64-softmmu/qemu-system-x86_64"

#drive1=" -drive file=/mnt/disk/ubuntu.img,format=raw,if=none,id=ddisk0,aio=threads -device virtio-blk-pci,scsi=off,bus=pci.0,addr=0x5,drive=ddisk0,id=disk0  -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6 "
#drive1="-drive file=/mnt/disk/ubuntu.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"

drive1="-drive file=/mnt/disk/bsd.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"
#drive1="-drive file=/mnt/disk/images/backups/ubuntu.img.3rdApr2014,format=raw,if=virtio,cache=none,index=0,aio=threads"

#drive1=" -hda /mnt/disk/ubuntu.img "
drive2="-drive file=/mnt/jadu/disk.raw,format=raw,if=none,id=ddisk1,aio=threads,cache=none -device virtio-blk-pci,scsi=off,bus=pci.0,addr=0x7,drive=ddisk1,id=disk1,logical_block_size=4096,physical_block_size=512"
#drive2="-drive file=/mnt/jadu/disk.raw,format=raw,if=virtio,cache=none,index=1,aio=threads,logical_block_size=4096,physical_block_size=4096"
#drive2=" -hdb /mnt/jadu/disk.raw "

cpu="-smp 4,sockets=4,cores=1,threads=1 -cpu host,+x2apic "
#cpu="-smp 1"

#mem="-m 2048"
mem="-m 6144"
kvm="-enable-kvm"

net="-netdev user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1 -net nic,macaddr=00:16:3e:2f:92:40,netdev=mynet0 "

input="-device virtio-keyboard-pci -device virtio-tablet-pci"

debug=" -gdb tcp::9999 "
monitor="-monitor telnet:127.0.0.1:1234,server,nowait"
#serial="-serial tcp:127.0.0.1:9999"

#cmd=/usr/bin/kvm -name new -S -M pc-1.2 -enable-kvm -m 1024 -smp 4,sockets=4,cores=1,threads=1 -uuid a05e9dbb-7b27-e1c0-05ac-d755410a969f -no-user-config -nodefaults -chardev socket,id=charmonitor,path=/var/lib/libvirt/qemu/new.monitor,server,nowait -mon chardev=charmonitor,id=monitor,mode=control -rtc base=utc -no-reboot -no-shutdown -device piix3-usb-uhci,id=usb,bus=pci.0,addr=0x1.0x2 -drive file=/mnt/disk/new.cow,if=none,id=drive-virtio-disk0,format=raw -device virtio-blk-pci,scsi=off,bus=pci.0,addr=0x5,drive=drive-virtio-disk0,id=virtio-disk0,bootindex=2 -drive file=/home/arulraj/ubuntu-12.10-desktop-amd64.iso,if=none,id=drive-ide0-1-0,readonly=on,format=raw -device ide-cd,bus=ide.1,unit=0,drive=drive-ide0-1-0,id=ide0-1-0,bootindex=1 -netdev tap,fd=21,id=hostnet0,vhost=on,vhostfd=22 -device virtio-net-pci,netdev=hostnet0,id=net0,mac=52:54:00:5e:f1:5d,bus=pci.0,addr=0x3 -chardev pty,id=charserial0 -device isa-serial,chardev=charserial0,id=serial0 -vnc 127.0.0.1:0 -vga cirrus -device intel-hda,id=sound0,bus=pci.0,addr=0x4 -device hda-duplex,id=sound0-codec0,bus=sound0.0,cad=0 -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6"
cmd="$qemu_cmd $kvm $cpu $drive1 $drive2 -rtc base=utc $net $monitor $serial -redir tcp:2222::22 $mem -vga cirrus -vnc :1 -show-cursor $input $debug "
#cmd="$qemu_cmd -drive file=~/cerny/jadukata/ubuntu.cow,format=cow,if=ide,index=0 -drive file=/mnt/jadu/disk.raw,format=raw,if=ide,index=1 -net nic,macaddr=00:16:3e:2f:92:40,vlan=1 -net user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1,vlan=1 -monitor telnet:127.0.0.1:1234,server,nowait -redir tcp:2222::22 -m 1024 -localtime -smp 4 -enable-kvm -vga cirrus -vnc :1 -usbdevice tablet -cpu host"
#cmd="~/qemu-src/qemu-1.5.0/x86_64-linux-user/qemu-x86_64 -drive file=~/cerny/jadukata/ubuntu.cow,format=cow,if=ide,index=0,boot=on -drive file=/mnt/disks/tst200G.raw,format=raw,if=ide,index=1,boot=off,cache=none -net nic,macaddr=00:16:3e:2f:92:40,vlan=1 -net user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1,vlan=1 -curses -monitor telnet:127.0.0.1:1234,server,nowait -redir tcp:2222::22 -m 1024 -localtime -smp 4 -enable-kvm -vga cirrus -vnc :1 -usbdevice tablet -cpu host"
echo "$cmd"

if [ $# -eq 0 ] ; then 
    echo "default"
    echo "$cmd" | sudo bash
else
    if [ $1 -eq 1 ]; then
        echo "debug"
        sudo bash -c "gdb --args $cmd "
    elif [ $1 -eq 2 ]; then
        echo "ftrace"
        ` sudo $cmd ` &
        #pid=`ps auwx | grep qemu-system-x86_64 | tr -s " " | head -n 2 | tail -n 1 | cut -d" " -f2`
        count=`ps auwx | grep qemu-system-x86_64 | wc -l`
        #ps auwx | grep qemu-system-x86_64 | tr -s " " 

        #echo $pid
        #echo "echo \"$pid\" > /debug/tracing/set_ftrace_pid" | sudo bash
        #echo 'echo 1 > /debug/tracing/tracing_enabled' | sudo bash
        #echo 'echo 1 > /debug/tracing/tracing_on' | sudo bash

        sudo ./ftrace_init.sh
        
        while sleep 10; do
            count=`ps auwx | grep qemu-system-x86_64 | wc -l`
            if [ $count -le 1 ]; then
                break
            fi     # Break the loop when we see the process has gone away.
        done

        #echo 'echo 0 > /debug/tracing/tracing_on' | sudo bash
    elif [ $1 -eq 3 ]; then
        echo "strace"
        `sudo strace -f -o /tmp/a $cmd`

    elif [ $1 -eq 4 ]; then
        echo "valgrind"
        sudo bash -c " valgrind --tool=callgrind --collect-jumps=yes  $cmd  "
    elif [ $1 -eq 5 ]; then
        echo "printf"
        sudo bash -c "  $cmd 2>/tmp/a "
    else
        echo "default"
        echo "$cmd" | sudo bash
    fi
fi
