#!/bin/bash 

sudo insmod /home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/kvm.ko
sudo insmod /home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/kvm-intel.ko #ept=0

qemu_cmd="/home/arulraj/cerny/jadukata/qemu-2.5.0/x86_64-softmmu/qemu-system-x86_64"

#drive1="-drive file=/mnt/disk/images/FreeBSD-10.2-RELEASE-amd64.raw,format=raw,if=ide,cache=none,index=0"
#drive2="-drive file=/mnt/disk/PCBSD10.2-RELEASE-09-12-2015-x64-DVD-USB.iso,media=cdrom,index=0"
#drive2="-drive file=/mnt/disk/tmpdisk.raw,format=raw,if=virtio,cache=none,index=1,aio=threads"

drive1="-drive file=/mnt/disk/bsd.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"
#drive1="-drive file=/mnt/disk/solaris.qcow2,format=qcow2,if=virtio,cache=none,index=0,aio=threads"
#drive1="-drive file=/mnt/disk/ubuntu.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"
#drive2="-drive file=/home/arulraj/ubuntu-15.10-desktop-amd64.iso,media=cdrom,index=1,aio=threads"

cpu="-smp 4,sockets=4,cores=1,threads=1 -cpu host,+x2apic "
mem="-m 6144"
kvm="-enable-kvm"
input="-device virtio-keyboard-pci -device virtio-tablet-pci"
#input="-usb -usbdevice keyboard -usbdevice wacom-tablet"
net="-netdev user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1 -net nic,macaddr=00:16:3e:2f:92:40,netdev=mynet0 "
cmd="$qemu_cmd $kvm -m 1024 $cpu $drive1 $drive2 -rtc base=utc $net -monitor telnet:127.0.0.1:1234,server,nowait -redir tcp:2222::22 $mem -vga cirrus -vnc :1 -show-cursor $input $debug "

echo "$cmd" | sudo bash -x

