#!/bin/bash 

sudo /home/arulraj/cerny/jadukata/diskdriver/scripts/bcachemount.py 0 

sudo rmmod netconsole
sudo modprobe netconsole
sudo rmmod kvm-intel
sudo rmmod kvm

sudo insmod /home/arulraj/cerny/jadukata/unmod-kvm-kmod-3.10.1/x86/kvm.ko
sudo insmod /home/arulraj/cerny/jadukata/unmod-kvm-kmod-3.10.1/x86/kvm-intel.ko #ept=0

#sudo insmod /home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/kvm.ko
#sudo insmod /home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/kvm-intel.ko #ept=0

#sudo bash -c "echo noop > /sys/block/sdb/queue/scheduler"

#cmdserver process
ccount=`ps auwx | grep cmdserver | wc -l`
if [ $ccount -gt 1 ]; then
    processes=`ps auwx | grep cmdserver`
    echo "stopping already running cmdserver processes.. $ccount"
    echo "$processes"
    `ps auwx | grep cmdserver | tr -s ' ' | cut -d' ' -f2 | xargs -i echo "/bin/kill -9 -{};/bin/kill -9 {};" | bash -x`
    sleep 5;
fi 

echo "starting cmdserver"
#start the cmdserver
`~/cerny/jadukata/cmdserver.o unmod-bare > ~/cerny/jadukata/cmdserver.log ` &
#`~/cerny/jadukata/cmdserver.o > ~/cerny/jadukata/cmdserver.log ` &

qemu_cmd="/home/arulraj/cerny/jadukata/qemu-2.5.0/x86_64-softmmu/qemu-system-x86_64"
#qemu_cmd="/home/arulraj/cerny/jadukata/qemu-1.6.1/x86_64-softmmu/qemu-system-x86_64"
#qemu_cmd="/home/arulraj/qemu-src/nitro/x86_64-softmmu/qemu-system-x86_64"
#qemu_cmd="/usr/bin/qemu-system-x86_64"
#qemu_cmd="/home/arulraj/qemu-src/qemu-1.5.0/x86_64-softmmu/qemu-system-x86_64"

#drive1=" -drive file=/mnt/disk/ubuntu.img,format=raw,if=none,id=ddisk0,aio=threads -device virtio-blk-pci,scsi=off,bus=pci.0,addr=0x5,drive=ddisk0,id=disk0  -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6 "
#drive1="-drive file=/mnt/disk/ubuntu.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"

#drive1="-drive file=/mnt/disk/bsd.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"
drive1="-drive file=/mnt/disk/ubuntu.img.new,format=raw,if=virtio,cache=none,index=0,aio=threads"
#drive1="-drive file=/mnt/disk/images/backups/ubuntu.img.new.8thOct2015,format=raw,if=virtio,cache=none,index=0,aio=threads"
sudo umount /dev/sdb
sudo umount /mnt/jadu
sudo mkfs.ext3 -F /dev/sdb
sudo mount  /dev/sdb /mnt/jadu
qemuimgsize=12884901888
qemuimgsize4k=$((qemuimgsize / 4096))
sudo qemu-img create -f raw /mnt/jadu/disk.raw $qemuimgsize
#sudo dd if=/dev/zero of=/mnt/jadu/disk.raw bs=4096 count=$qemuimgsize4k
#drive1=" -hda /mnt/disk/ubuntu.img "
drive2="-drive file=/mnt/jadu/disk.raw,format=raw,if=none,id=ddisk1,aio=threads,cache=none -device virtio-blk-pci,scsi=on,bus=pci.0,addr=0x7,drive=ddisk1,id=disk1,logical_block_size=4096,physical_block_size=512"
#drive2="-drive file=/mnt/jadu/disk.raw,format=raw,if=virtio,cache=none,index=1,aio=threads,logical_block_size=4096,physical_block_size=4096"
#drive2=" -hdb /mnt/jadu/disk.raw "

cpu="-smp 4,sockets=4,cores=1,threads=1 -cpu host,+x2apic "
#cpu="-smp 1"

#mem="-m 2048"
mem="-m 6144"
kvm="-enable-kvm"

net="-netdev user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1 -net nic,macaddr=00:16:3e:2f:92:40,netdev=mynet0 "

input="-device virtio-keyboard-pci -device virtio-tablet-pci"

debug=" -gdb tcp::9999 "
monitor="-monitor telnet:127.0.0.1:1234,server,nowait"
#serial="-serial tcp:127.0.0.1:9999"

#cmd=/usr/bin/kvm -name new -S -M pc-1.2 -enable-kvm -m 1024 -smp 4,sockets=4,cores=1,threads=1 -uuid a05e9dbb-7b27-e1c0-05ac-d755410a969f -no-user-config -nodefaults -chardev socket,id=charmonitor,path=/var/lib/libvirt/qemu/new.monitor,server,nowait -mon chardev=charmonitor,id=monitor,mode=control -rtc base=utc -no-reboot -no-shutdown -device piix3-usb-uhci,id=usb,bus=pci.0,addr=0x1.0x2 -drive file=/mnt/disk/new.cow,if=none,id=drive-virtio-disk0,format=raw -device virtio-blk-pci,scsi=off,bus=pci.0,addr=0x5,drive=drive-virtio-disk0,id=virtio-disk0,bootindex=2 -drive file=/home/arulraj/ubuntu-12.10-desktop-amd64.iso,if=none,id=drive-ide0-1-0,readonly=on,format=raw -device ide-cd,bus=ide.1,unit=0,drive=drive-ide0-1-0,id=ide0-1-0,bootindex=1 -netdev tap,fd=21,id=hostnet0,vhost=on,vhostfd=22 -device virtio-net-pci,netdev=hostnet0,id=net0,mac=52:54:00:5e:f1:5d,bus=pci.0,addr=0x3 -chardev pty,id=charserial0 -device isa-serial,chardev=charserial0,id=serial0 -vnc 127.0.0.1:0 -vga cirrus -device intel-hda,id=sound0,bus=pci.0,addr=0x4 -device hda-duplex,id=sound0-codec0,bus=sound0.0,cad=0 -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6"
cmd="$qemu_cmd $kvm $cpu $drive1 $drive2 -rtc base=utc $net $monitor $serial -redir tcp:2222::22 $mem -vga cirrus -vnc :1 -show-cursor $input $debug "
#cmd="$qemu_cmd -drive file=~/cerny/jadukata/ubuntu.cow,format=cow,if=ide,index=0 -drive file=/mnt/jadu/disk.raw,format=raw,if=ide,index=1 -net nic,macaddr=00:16:3e:2f:92:40,vlan=1 -net user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1,vlan=1 -monitor telnet:127.0.0.1:1234,server,nowait -redir tcp:2222::22 -m 1024 -localtime -smp 4 -enable-kvm -vga cirrus -vnc :1 -usbdevice tablet -cpu host"
#cmd="~/qemu-src/qemu-1.5.0/x86_64-linux-user/qemu-x86_64 -drive file=~/cerny/jadukata/ubuntu.cow,format=cow,if=ide,index=0,boot=on -drive file=/mnt/disks/tst200G.raw,format=raw,if=ide,index=1,boot=off,cache=none -net nic,macaddr=00:16:3e:2f:92:40,vlan=1 -net user,id=mynet0,net=192.168.0.0/24,dhcpstart=192.168.0.1,vlan=1 -curses -monitor telnet:127.0.0.1:1234,server,nowait -redir tcp:2222::22 -m 1024 -localtime -smp 4 -enable-kvm -vga cirrus -vnc :1 -usbdevice tablet -cpu host"
echo "$cmd"

echo "$cmd" | sudo bash -x
