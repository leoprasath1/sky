#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include "../ioctl.h"

#define DEV "/dev/dm-0"

int myioctl(int fd, int req, ...)
{
    int retries = 3;
    int num = 0;
    int ret = 1;
    void* arg;
    va_list args;
    int args_ioctl = 0;
    if(req == UNIQUE_STATS_PRINT || req == UNIQUE_STATS_CLEAR || req == DMDEDUP_DEBUG){
        args_ioctl = 1;
    }
    if(args_ioctl){
        va_start(args,req);
        arg = va_arg(args,void*);
        va_end(args);
    }

    do {
        num++;
        if(args_ioctl)
            ret = ioctl(fd, req, arg);
        else
            ret = ioctl(fd,req);

        if(ret)
            fprintf(stderr,"retrying failed ioctl %d error : %d %s\n",num,errno,strerror(errno));
    }while(ret!=0 && num <=retries);

    return ret;
}

int main(int argc, char *argv[])
{
    int fd;
    int ret = 1;
    unsigned long ioctl_long;

    if (argc < 2) {
        printf("Usage: ioctl <unique_stats_print|unique_stats_clear|dmdedup_debug\n");
        return ret;
    }

    fd = open(DEV, O_RDONLY);

    if(fd < 0) {
        //perror("file open error\n");
        printf("dmdedup module not loaded \n");
        return ret;
    }

    if (strcmp(argv[1], "unique_stats_print") == 0) {
        fprintf(stderr, "printing unique stats ...\n");
        char *buff = (char *)malloc(STATS_BUFF_SIZE);
        if (!buff) {
            fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n",STATS_BUFF_SIZE);
            return 1;
        } else {
            memset(buff, '\0', STATS_BUFF_SIZE);
            ret = myioctl(fd, UNIQUE_STATS_PRINT, buff);
            printf("%s\n", buff);
        }
        free(buff);
        ret = 0;
    } else if (strcmp(argv[1], "unique_stats_clear") == 0) {
        fprintf(stderr, "clear unique stats ...\n");
        char *buff = (char *)malloc(STATS_BUFF_SIZE);
        if (!buff) {
            fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n",STATS_BUFF_SIZE);
            return 1;
        } else {
            memset(buff, '\0', STATS_BUFF_SIZE);
            ret = myioctl(fd, UNIQUE_STATS_CLEAR, buff);
            printf("%s\n", buff);
        }
        free(buff);
        ret = 0;
    } else if (strcmp(argv[1], "dmdedup_debug") == 0) {
        int debug = 0;
        if(argv[2])
            debug = atoi(argv[2]);
        printf("setting debug to %d\n", debug);
        ret = myioctl(fd, DMDEDUP_DEBUG, debug);
    }else {
        fprintf(stderr, "Invalid command %s \n", argv[1]);
    }

    close(fd);

    return ret;
}
