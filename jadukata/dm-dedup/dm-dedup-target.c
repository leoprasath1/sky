/*
 * Copyright (C) 2012-2014 Vasily Tarasov
 * Copyright (C) 2012-2014 Geoff Kuenning
 * Copyright (C) 2012-2014 Sonam Mandal
 * Copyright (C) 2012-2014 Karthikeyani Palanisami
 * Copyright (C) 2012-2014 Philip Shilane
 * Copyright (C) 2012-2014 Sagar Trehan
 * Copyright (C) 2012-2014 Erez Zadok
 *
 * This file is released under the GPL.
 */

#include <linux/vmalloc.h>

#include "dm-dedup-target.h"
#include "dm-dedup-rw.h"
#include "dm-dedup-hash.h"
#include "dm-dedup-backend.h"
#include "dm-dedup-ram.h"
#include "dm-dedup-cbt.h"
#include "dm-dedup-kvstore.h"

#include "/home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/hash/ht_at_wrappers.h"
#include "/home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/checksum_processor.h"

#include "ioctl.h"

#define MAX_DEV_NAME_LEN (64)

#define MIN_DATA_DEV_BLOCK_SIZE (4 * 1024)
#define MAX_DATA_DEV_BLOCK_SIZE (1024 * 1024)

bool dmdedup_debug_enabled = false;

struct on_disk_stats {
	uint64_t physical_block_counter;
	uint64_t logical_block_counter;
};

/*
 * All incoming requests are packed in the dedup_work structure
 * for further processing by the workqueue thread.
 */
struct dedup_work {
	struct work_struct worker;
	struct dedup_config *config;
	struct bio *bio;
};

enum backend {
	BKND_INRAM,
	BKND_COWBTREE
};

static void bio_zero_endio(struct bio *bio)
{
	zero_fill_bio(bio);
	bio_endio(bio, 0);
}

static uint64_t bio_lbn(struct dedup_config *dc, struct bio *bio)
{
	sector_t lbn = bio->bi_sector;

	sector_div(lbn, dc->sectors_per_block);

	return lbn;
}

static uint64_t sector_lbn(struct dedup_config *dc, sector_t sector)
{
	sector_t lbn = sector;

	sector_div(lbn, dc->sectors_per_block);

	return lbn;
}

static void do_io(struct dedup_config *dc, struct bio *bio, uint64_t pbn)
{
	int offset;

	offset = sector_div(bio->bi_sector, dc->sectors_per_block);
	bio->bi_sector = (sector_t)pbn * dc->sectors_per_block + offset;

	bio->bi_bdev = dc->data_dev->bdev;
    
    leoprintk( "do io write new bio %p\n", bio);
    print_bio("do_io",bio);

	generic_make_request(bio);
}

static int handle_read(struct dedup_config *dc, struct bio *bio)
{
	uint64_t lbn;
	uint32_t vsize;
	struct lbn_pbn_value lbnpbn_value;
	int r;
	lbn = bio_lbn(dc, bio);

	r = dc->kvs_lbn_pbn->kvs_lookup(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value, &vsize);
    if (r == 0){
        bio_zero_endio(bio);
    }
    else if (r == 1){
        do_io(dc, bio, lbnpbn_value.pbn);
    }
    else{
        return r;
    }

	return 0;
}

static int allocate_block(struct dedup_config *dc, uint64_t *pbn_new)
{
	int r;

	r = dc->mdops->alloc_data_block(dc->bmd, pbn_new);

	if (!r) {
		dc->logical_block_counter++;
		dc->physical_block_counter++;
	}

	return r;
}

static int write_to_new_block(struct dedup_config *dc, uint64_t *pbn_new,
			      struct bio *bio, uint64_t lbn)
{
	int r = 0;
	struct lbn_pbn_value lbnpbn_value;

	r = allocate_block(dc, pbn_new);
	if (r < 0) {
		r = -EIO;
		return r;
	}

	lbnpbn_value.pbn = *pbn_new;

	do_io(dc, bio, *pbn_new);

	r = dc->kvs_lbn_pbn->kvs_insert(dc->kvs_lbn_pbn, (void *)&lbn,
		sizeof(lbn), (void *)&lbnpbn_value, sizeof(lbnpbn_value));
	if (r < 0)
		dc->mdops->dec_refcount(dc->bmd, *pbn_new);

	return r;
}

static int handle_write_unique_hint(struct dedup_config *dc,
				struct bio *bio, uint64_t lbn)
{
	int r;
	uint32_t vsize;
	uint64_t pbn_new, pbn_old;
	struct lbn_pbn_value lbnpbn_value;

	dc->uniqwrites++;

	r = dc->kvs_lbn_pbn->kvs_lookup(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value, &vsize);
    leoprintk( "uniq hint handle write no hash r %d lbnpbn value %llu \n", r,lbnpbn_value.pbn);
	if (r == 0) {
		/* No LBN->PBN mapping entry */
		dc->newwrites++;
        leo_debug_print("uniq hint handle write no hash 1",bio);
		r = write_to_new_block(dc, &pbn_new, bio, lbn);
		if (r < 0)
			goto out_write_new_block_1;

		r = dc->mdops->inc_refcount(dc->bmd, pbn_new);
		if (r < 0)
			goto out_inc_refcount_1;

		goto out_1;

out_inc_refcount_1:
		dc->kvs_lbn_pbn->kvs_delete(dc->kvs_lbn_pbn,
				(void *)&lbn, sizeof(lbn));
		dc->mdops->dec_refcount(dc->bmd, pbn_new);
out_write_new_block_1:
		dc->newwrites--;
out_1:
		if (r < 0)
			dc->uniqwrites--;
		return r;
	} else if (r < 0)
		goto out_2;

	/* LBN->PBN mappings exist */
	dc->overwrites++;
	r = write_to_new_block(dc, &pbn_new, bio, lbn);
	if (r < 0)
		goto out_write_new_block_2;

	pbn_old = lbnpbn_value.pbn;
	r = dc->mdops->dec_refcount(dc->bmd, pbn_old);
	if (r < 0)
		goto out_dec_refcount_2;

	dc->logical_block_counter--;
	r = dc->mdops->inc_refcount(dc->bmd, pbn_new);
	if (r < 0)
		goto out_inc_refcount_2;

	goto out_2;

out_inc_refcount_2:
	dc->logical_block_counter++;
	dc->mdops->inc_refcount(dc->bmd, pbn_old);
out_dec_refcount_2:
	dc->kvs_lbn_pbn->kvs_insert(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value,
			sizeof(lbnpbn_value));
	dc->mdops->dec_refcount(dc->bmd, pbn_new);
out_write_new_block_2:
	dc->overwrites--;
out_2:
	if (r < 0)
		dc->uniqwrites--;
	return r;
}

static int handle_write_no_hash(struct dedup_config *dc,
				struct bio *bio, uint64_t lbn, u8 *hash)
{
	int r;
	uint32_t vsize;
	uint64_t pbn_new, pbn_old;
	struct lbn_pbn_value lbnpbn_value;
	struct hash_pbn_value hashpbn_value;

	dc->uniqwrites++;

	r = dc->kvs_lbn_pbn->kvs_lookup(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value, &vsize);
    leoprintk( "handle write no hash r %d lbnpbn value %llu \n", r,lbnpbn_value.pbn);
	if (r == 0) {
		/* No LBN->PBN mapping entry */
		dc->newwrites++;
        leo_debug_print("handle write no hash 1",bio);
		r = write_to_new_block(dc, &pbn_new, bio, lbn);
		if (r < 0)
			goto out_write_new_block_1;

		hashpbn_value.pbn = pbn_new;

		r = dc->kvs_hash_pbn->kvs_insert(dc->kvs_hash_pbn, (void *)hash,
				dc->crypto_key_size, (void *)&hashpbn_value,
				sizeof(hashpbn_value));
	    leo_debug_print("handle write no hash 2",bio);
        leoprintk( "handle write no hash inserting new pbn value r %d lbnpbn value %llu \n", r,hashpbn_value.pbn);
		if (r < 0)
			goto out_kvs_insert_1;

		r = dc->mdops->inc_refcount(dc->bmd, pbn_new);
		if (r < 0)
			goto out_inc_refcount_1;

		goto out_1;

out_inc_refcount_1:
		dc->kvs_hash_pbn->kvs_delete(dc->kvs_hash_pbn,
				(void *)hash, dc->crypto_key_size);
out_kvs_insert_1:
		dc->kvs_lbn_pbn->kvs_delete(dc->kvs_lbn_pbn,
				(void *)&lbn, sizeof(lbn));
		dc->mdops->dec_refcount(dc->bmd, pbn_new);
out_write_new_block_1:
		dc->newwrites--;
out_1:
		if (r < 0)
			dc->uniqwrites--;
		return r;
	} else if (r < 0)
		goto out_2;

	/* LBN->PBN mappings exist */
	dc->overwrites++;
	r = write_to_new_block(dc, &pbn_new, bio, lbn);
	if (r < 0)
		goto out_write_new_block_2;

	pbn_old = lbnpbn_value.pbn;
	r = dc->mdops->dec_refcount(dc->bmd, pbn_old);
	if (r < 0)
		goto out_dec_refcount_2;

	dc->logical_block_counter--;

	hashpbn_value.pbn = pbn_new;

	r = dc->kvs_hash_pbn->kvs_insert(dc->kvs_hash_pbn, (void *)hash,
				dc->crypto_key_size, (void *)&hashpbn_value,
				sizeof(hashpbn_value));
	if (r < 0)
		goto out_kvs_insert_2;

	r = dc->mdops->inc_refcount(dc->bmd, pbn_new);
	if (r < 0)
		goto out_inc_refcount_2;

	goto out_2;

out_inc_refcount_2:
	dc->kvs_hash_pbn->kvs_delete(dc->kvs_hash_pbn,
			(void *)hash, dc->crypto_key_size);
out_kvs_insert_2:
	dc->logical_block_counter++;
	dc->mdops->inc_refcount(dc->bmd, pbn_old);
out_dec_refcount_2:
	dc->kvs_lbn_pbn->kvs_insert(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value,
			sizeof(lbnpbn_value));
	dc->mdops->dec_refcount(dc->bmd, pbn_new);
out_write_new_block_2:
	dc->overwrites--;
out_2:
	if (r < 0)
		dc->uniqwrites--;
	return r;
}

static int handle_write_with_hash(struct dedup_config *dc, struct bio *bio,
				  uint64_t lbn, u8 *final_hash,
				  struct hash_pbn_value hashpbn_value)
{
	int r;
	uint32_t vsize;
	uint64_t pbn_new, pbn_old;
	struct lbn_pbn_value lbnpbn_value;
	struct lbn_pbn_value new_lbnpbn_value;

	dc->dupwrites++;

	pbn_new = hashpbn_value.pbn;
	r = dc->kvs_lbn_pbn->kvs_lookup(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value, &vsize);
    leoprintk( "handle write with hash r %d lbnpbn value %llu \n", r,lbnpbn_value.pbn);
	if (r == 0) {
		/* No LBN->PBN mapping entry */
		dc->newwrites++;

		r = dc->mdops->inc_refcount(dc->bmd, pbn_new);
		if (r < 0)
			goto out_inc_refcount_1;

		lbnpbn_value.pbn = pbn_new;

		r = dc->kvs_lbn_pbn->kvs_insert(dc->kvs_lbn_pbn, (void *)&lbn,
				sizeof(lbn), (void *)&lbnpbn_value,
				sizeof(lbnpbn_value));
        leoprintk( "handle write with hash inserting new pbn value r %d lbnpbn value %llu \n", r,lbnpbn_value.pbn);
		if (r < 0)
			goto out_kvs_insert_1;

		dc->logical_block_counter++;

		goto out_1;

out_kvs_insert_1:
		dc->mdops->dec_refcount(dc->bmd, pbn_new);
out_inc_refcount_1:
		dc->newwrites--;
out_1:
		if (r >= 0)
			bio_endio(bio, 0);
		else
			dc->dupwrites--;
		return r;
	} else if (r < 0)
		goto out_2;

	/* LBN->PBN mapping entry exists */
	dc->overwrites++;
	pbn_old = lbnpbn_value.pbn;
	if (pbn_new != pbn_old) {
		r = dc->mdops->inc_refcount(dc->bmd, pbn_new);
		if (r < 0)
			goto out_inc_refcount_2;

		new_lbnpbn_value.pbn = pbn_new;

		r = dc->kvs_lbn_pbn->kvs_insert(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&new_lbnpbn_value,
			sizeof(new_lbnpbn_value));
		if (r < 0)
			goto out_kvs_insert_2;

		r = dc->mdops->dec_refcount(dc->bmd, pbn_old);
		if (r < 0)
			goto out_dec_refcount_2;
	}

	/* Nothing to do if writing same data to same LBN */
	goto out_2;

out_dec_refcount_2:
	dc->kvs_lbn_pbn->kvs_insert(dc->kvs_lbn_pbn, (void *)&lbn,
			sizeof(lbn), (void *)&lbnpbn_value,
			sizeof(lbnpbn_value));
out_kvs_insert_2:
	dc->mdops->dec_refcount(dc->bmd, pbn_new);
out_inc_refcount_2:
	dc->overwrites--;
out_2:
	if (r >= 0)
		bio_endio(bio, 0);
	else
		dc->dupwrites--;
	return r;
}

inline bool is_unique_hint(struct bio* bio){
    return bio->isunique;
}

struct work_order_entry{
    struct timespec todo_add, todo_start, todo_end;
    unsigned long checksum;
    struct list_head list;
};

#ifdef DEDUP_ENABLED
hash_table* ht_cleaners = NULL;
EXPORT_SYMBOL(ht_cleaners);
#endif

struct kmem_cache *work_order_entries = NULL;

hash_table* ht_cache_hashpbn = NULL;
hash_table* ht_work_todo = NULL;
LIST_HEAD(work_order_prio);
LIST_HEAD(work_order);
static DEFINE_SPINLOCK(work_order_lock);
static DEFINE_SPINLOCK(work_order_prio_lock);
static DEFINE_SPINLOCK(stats_update_lock);


struct cleaner_data* cache_cleaner = NULL;
EXPORT_SYMBOL(cache_cleaner);

#define WORK_TODO_NUM_SERVICE (2)
struct delayed_work work_todo[WORK_TODO_NUM_SERVICE];
struct dedup_config *w_hint_dc = NULL;
unsigned dmdedup_bcache_sb_offset = 0;
EXPORT_SYMBOL(dmdedup_bcache_sb_offset);

atomic_t count = ATOMIC_INIT(0);
atomic_t time = ATOMIC_INIT(0);
atomic_t unique = ATOMIC_INIT(0);
atomic_t duplicate = ATOMIC_INIT(0);
long time1total[2] = {0,0}, time2total[2] = {0,0}, todocount[2]= {1,1};

atomic_t cache_hits_1 = ATOMIC_INIT(0);
atomic_t cache_hits_2 = ATOMIC_INIT(0);
atomic_t cache_hits_3 = ATOMIC_INIT(0);

atomic_t cache_inserts = ATOMIC_INIT(0);
atomic_t war_hints = ATOMIC_INIT(0);
atomic_t w_hints = ATOMIC_INIT(0);

#define CACHE_TIMEOUT (600000) //msecs
#define CACHE_TIMEOUT_INIT (60000) //msecs

//dummy function to resolve undefined symbols
int process_checksum(void* pargs, int chunknum, unsigned long checksum){
    return 0;
}

void dedup_hashpbn_cache_insert(unsigned long checksum, unsigned long sector){
    struct work_order_entry* entry = NULL;
    atomic_inc(&cache_inserts);
    ht_add_val(ht_work_todo,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),sector,1));
    entry = kmem_cache_alloc(work_order_entries, GFP_ATOMIC);
    entry->checksum = checksum;
    getnstimeofday(&entry->todo_add);
    spin_lock(&work_order_lock);
    list_add_tail(&(entry->list),&work_order);
    spin_unlock(&work_order_lock);
}
EXPORT_SYMBOL(dedup_hashpbn_cache_insert);

void dedup_hashpbn_cache_insert_work(unsigned long checksum, unsigned long sector, int source){
    unsigned long val;
    if(ht_lookup_val(ht_cache_hashpbn,checksum,&val) != 0){
        if(pack_info_get_x(val) <  (jiffies_to_jsecs(get_jiffies()+msecs_to_jiffies(CACHE_TIMEOUT_INIT)))){
            ht_update_val(ht_cache_hashpbn,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()+msecs_to_jiffies(CACHE_TIMEOUT_INIT)),pack_info_get_y(val),source));
        }
    }else{
        uint64_t lbn;
        struct lbn_pbn_value lbnpbn_value;
        uint32_t vsize;
        int r;

        lbn = sector_lbn(w_hint_dc,sector+dmdedup_bcache_sb_offset);

        r = w_hint_dc->kvs_lbn_pbn->kvs_lookup(w_hint_dc->kvs_lbn_pbn, (void *)&lbn,
                sizeof(lbn), (void *)&lbnpbn_value, &vsize);
        if (r != 1){
            printk(KERN_ERR "ERROR: ret = %d dmdedup cache insert no lbn pbn entry found for checksum %lx sector %ld \n",r,checksum,sector);
            return;
        }
        ht_add_val(ht_cache_hashpbn,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()+msecs_to_jiffies(CACHE_TIMEOUT_INIT)),lbnpbn_value.pbn,source));
    }
}

void dedup_war_hint(unsigned long checksum, unsigned long sector){
    unsigned long val;
    atomic_inc(&war_hints);
    if(ht_lookup_val(ht_cache_hashpbn,checksum,&val) != 0){
        ht_update_val(ht_cache_hashpbn,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()+msecs_to_jiffies(CACHE_TIMEOUT)),pack_info_get_y(val),2));
    }else{
        struct work_order_entry* entry = NULL;
        ht_update_val(ht_work_todo,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),sector,2));
        entry = kmem_cache_alloc(work_order_entries, GFP_ATOMIC);
        entry->checksum = checksum;
        getnstimeofday(&entry->todo_add);
        spin_lock(&work_order_prio_lock);
        list_add_tail(&(entry->list),&work_order_prio);
        spin_unlock(&work_order_prio_lock);
    }
}
EXPORT_SYMBOL(dedup_war_hint);

inline void dedup_w_hint_work(unsigned long checksum, int id)
{
	struct hash_pbn_value hashpbn_value;
	uint32_t vsize;
    int r;
    r = w_hint_dc->kvs_hash_pbn->kvs_lookup(w_hint_dc->kvs_hash_pbn, (u8*)(&checksum),
                w_hint_dc->crypto_key_size, &hashpbn_value, &vsize);
    if (r > 0){
        ht_add_val(ht_cache_hashpbn,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()+msecs_to_jiffies(CACHE_TIMEOUT)),hashpbn_value.pbn,3));
    }
}

void dedup_w_hint(unsigned long checksum){
    unsigned long val;
    struct work_order_entry* entry = NULL;
    int id = atomic_inc_return(&w_hints);
    
    if(ht_lookup_val(ht_cache_hashpbn,checksum,&val) != 0){
        ht_update_val(ht_cache_hashpbn,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()+msecs_to_jiffies(CACHE_TIMEOUT)),pack_info_get_y(val),3));
        return;
    }
    
    ht_add_val(ht_work_todo,checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),id,3));
    entry = kmem_cache_alloc(work_order_entries, GFP_ATOMIC);
    entry->checksum = checksum;
    getnstimeofday(&entry->todo_add);
    spin_lock(&work_order_lock);
    list_add_tail(&(entry->list),&work_order);
    spin_unlock(&work_order_lock);
}
EXPORT_SYMBOL(dedup_w_hint);

inline int dedup_todo_process(unsigned long checksum, unsigned long val){
    if(pack_info_get_z(val) == 1){
        dedup_hashpbn_cache_insert_work(checksum, pack_info_get_y(val),1);
    }else if(pack_info_get_z(val) == 2){
        dedup_hashpbn_cache_insert_work(checksum, pack_info_get_y(val),2);
    }else if(pack_info_get_z(val) == 3){        
        dedup_w_hint_work(checksum, pack_info_get_y(val));
    }else{
        printk(KERN_ERR "ERROR: unexpected value in todo processing key %lx val %lx \n",checksum,val);
    }
    return 0;
}

#define NEXT_MSECS (20)

#define TIMEDIFF(e,s) (((e.tv_sec-s.tv_sec)*1000000000) + (e.tv_nsec-s.tv_nsec))

static void do_work_todo_process(struct work_struct *ws)
{
    unsigned long key, val, count=0;
    struct work_order_entry* entry = NULL;
    struct delayed_work *dwork = to_delayed_work(ws);
    int loopcount = 0;

    while(true){
        while(true){
            bool prio = !list_empty(&work_order_prio);
            int index = prio? 0 : 1;
            if(prio){
                spin_lock(&work_order_prio_lock);
                entry = list_first_entry(&work_order_prio, struct work_order_entry, list);
                list_del(&(entry->list));
                getnstimeofday(&entry->todo_start);
                spin_unlock(&work_order_prio_lock);
            }else{
                bool normal = !list_empty(&work_order);
                if(!normal) break;
                spin_lock(&work_order_lock);
                entry = list_first_entry(&work_order, struct work_order_entry, list);
                list_del(&(entry->list));
                getnstimeofday(&entry->todo_start);
                spin_unlock(&work_order_lock);
            }

            key = entry->checksum;

            if(ht_remove_val(ht_work_todo,key,&val) != -1){
                dedup_todo_process(key,val);
                count++;
            }
            getnstimeofday(&entry->todo_end);

            spin_lock(&stats_update_lock);
            time1total[index] += TIMEDIFF(entry->todo_end,entry->todo_start);
            time2total[index] += TIMEDIFF(entry->todo_start,entry->todo_add);
            todocount[index]++;
            spin_unlock(&stats_update_lock);

            kfree(entry);
        }

        while(ht_pop_val(ht_work_todo, &key,&val) != -1){
            dedup_todo_process(key,val);
            count++;
        }

        loopcount++;
        if(loopcount > 5){
            loopcount = 0;
            count = 0;
        }
        if(count){
            msleep(1);
        }else{
            schedule_delayed_work(dwork, msecs_to_jiffies(count==0?NEXT_MSECS:5));
            break;
        }
    }
}


static int handle_write(struct dedup_config *dc, struct bio *bio)
{
	uint64_t lbn;
	u8 hash[MAX_DIGEST_SIZE];
	struct hash_pbn_value hashpbn_value;
	uint32_t vsize;
	struct bio *new_bio = NULL;
	int r;
    struct timeval time1, time2;

	dc->writes++;
    
    do_gettimeofday(&time1);

	/* Read-on-write handling */
	if (bio->bi_size < dc->block_size) {
		dc->reads_on_writes++;
		new_bio = prepare_bio_on_write(dc, bio);
		if (!new_bio || IS_ERR(new_bio))
			return -ENOMEM;
		bio = new_bio;
	}

	lbn = bio_lbn(dc, bio);
    
    atomic_inc(&count);
    //if(atomic_inc_return(&count) > 1000){
        //int time_taken = atomic_read(&time);
        //atomic_set(&count,0);
        //atomic_set(&time,0);
        //printk(KERN_ERR "handle write time %d uniq %d duplicate %d\n",time_taken,atomic_read(&unique),atomic_read(&duplicate));
    //}
    
    r = compute_hash_bio(dc->desc_table, bio, hash);
    if (r)
        return r;
    
    if(is_unique_hint(bio)){
        handle_write_unique_hint(dc, bio, lbn);
        atomic_add(bio_sectors(bio),&unique);
    }else{
        unsigned long val;
        atomic_add(bio_sectors(bio),&duplicate);

        if(ht_lookup_val(ht_cache_hashpbn,*((unsigned long*)hash),&val) != 0){
            if(pack_info_get_z(val) == 1){
                atomic_add(bio_sectors(bio),&cache_hits_1);
            }else if(pack_info_get_z(val) == 2){
                atomic_add(bio_sectors(bio),&cache_hits_2);
            }else if(pack_info_get_z(val) == 3){        
                atomic_add(bio_sectors(bio),&cache_hits_3);
            }
            hashpbn_value.pbn = pack_info_get_y(val); 
            r = handle_write_with_hash(dc, bio, lbn, hash,
                    hashpbn_value);
            if (r < 0)
                return r;
        }else{
    
            ht_remove(ht_work_todo,*((unsigned long*)hash));

            r = dc->kvs_hash_pbn->kvs_lookup(dc->kvs_hash_pbn, hash,
                    dc->crypto_key_size, &hashpbn_value, &vsize);
            if (r == 0)
                r = handle_write_no_hash(dc, bio, lbn, hash);
            else if (r > 0)
                r = handle_write_with_hash(dc, bio, lbn, hash,
                        hashpbn_value);
            if (r < 0)
                return r;
        }
    }
    
    leoprintk("handle write r %d new bio %p uniq %d possibleduplicate %d hash %llx\n", r,bio,atomic_read(&unique),atomic_read(&duplicate),*((u64*)&hash));

	dc->writes_after_flush++;
	if ((dc->flushrq && dc->writes_after_flush >= dc->flushrq) ||
			(bio->bi_rw & (REQ_FLUSH | REQ_FUA))) {
		r = dc->mdops->flush_meta(dc->bmd);
		if (r < 0)
			return r;
		dc->writes_after_flush = 0;
	}
   
    do_gettimeofday(&time2);

    atomic_add(diff(time1,time2),&time);

	return 0;
}

static void process_bio(struct dedup_config *dc, struct bio *bio)
{
	int r;

    print_bio("leo start process_bio ",bio);
	switch (bio_data_dir(bio)) {
	case READ:
		r = handle_read(dc, bio);
		break;
	case WRITE:
		r = handle_write(dc, bio);
	}
    
    print_bio("leo end process_bio ",bio);

	if (r < 0){
        leoprintk("process_bio calling endio bio %p r %d \n",bio,r);
		bio_endio(bio, r);
    }
}

static void do_work(struct work_struct *ws)
{
	struct dedup_work *data = container_of(ws, struct dedup_work, worker);
	struct dedup_config *dc = (struct dedup_config *)data->config;
	struct bio *bio = (struct bio *)data->bio;

	mempool_free(data, dc->dedup_work_pool);

	process_bio(dc, bio);
}

static void dedup_defer_bio(struct dedup_config *dc, struct bio *bio)
{
	struct dedup_work *data;

	data = mempool_alloc(dc->dedup_work_pool, GFP_NOIO);
	if (!data) {
		bio_endio(bio, -ENOMEM);
		return;
	}

	data->bio = bio;
	data->config = dc;

	INIT_WORK(&(data->worker), do_work);

	queue_work(dc->workqueue, &(data->worker));
}

static int dm_dedup_map(struct dm_target *ti, struct bio *bio)
{
	dedup_defer_bio(ti->private, bio);

	return DM_MAPIO_SUBMITTED;
}

struct dedup_args {
	struct dm_target *ti;

	struct dm_dev *meta_dev;

	struct dm_dev *data_dev;
	uint64_t data_size;

	uint32_t block_size;

	char hash_algo[CRYPTO_ALG_NAME_LEN];

	enum backend backend;
	char backend_str[MAX_BACKEND_NAME_LEN];

	uint32_t flushrq;
};

static int parse_meta_dev(struct dedup_args *da, struct dm_arg_set *as,
			  char **err)
{
	int r;

	r = dm_get_device(da->ti, dm_shift_arg(as),
			dm_table_get_mode(da->ti->table), &da->meta_dev);
	if (r)
		*err = "Error opening metadata device";

	return r;
}

static int parse_data_dev(struct dedup_args *da, struct dm_arg_set *as,
			  char **err)
{
	int r;

	r = dm_get_device(da->ti, dm_shift_arg(as),
			dm_table_get_mode(da->ti->table), &da->data_dev);
	if (r)
		*err = "Error opening data device";
    else
	    da->data_size = i_size_read(da->data_dev->bdev->bd_inode);

	return r;
}

static int parse_block_size(struct dedup_args *da, struct dm_arg_set *as,
			    char **err)
{
	uint32_t block_size;

	if (kstrtou32(dm_shift_arg(as), 10, &block_size) ||
		!block_size ||
		block_size < MIN_DATA_DEV_BLOCK_SIZE ||
		block_size > MAX_DATA_DEV_BLOCK_SIZE ||
		!is_power_of_2(block_size)) {
		*err = "Invalid data block size";
		return -EINVAL;
	}

	if (block_size > da->data_size) {
		*err = "Data block size is larger than the data device";
		return -EINVAL;
	}

	da->block_size = block_size;

	return 0;
}

static int parse_hash_algo(struct dedup_args *da, struct dm_arg_set *as,
			   char **err)
{
	strlcpy(da->hash_algo, dm_shift_arg(as), CRYPTO_ALG_NAME_LEN);

	if (!crypto_has_hash(da->hash_algo, 0, CRYPTO_ALG_ASYNC)) {
		*err = "Unrecognized hash algorithm";
		return -EINVAL;
	}

	return 0;
}

static int parse_backend(struct dedup_args *da, struct dm_arg_set *as,
			 char **err)
{
	char backend[MAX_BACKEND_NAME_LEN];

	strlcpy(backend, dm_shift_arg(as), MAX_BACKEND_NAME_LEN);

	if (!strcmp(backend, "inram"))
		da->backend = BKND_INRAM;
	else if (!strcmp(backend, "cowbtree"))
		da->backend = BKND_COWBTREE;
	else {
		*err = "Unsupported metadata backend";
		return -EINVAL;
	}

	strlcpy(da->backend_str, backend, MAX_BACKEND_NAME_LEN);

	return 0;
}

static int parse_flushrq(struct dedup_args *da, struct dm_arg_set *as,
			 char **err)
{
	if (kstrtou32(dm_shift_arg(as), 10, &da->flushrq)) {
		*err = "Invalid flushrq value";
		return -EINVAL;
	}

	return 0;
}

static int parse_dedup_args(struct dedup_args *da, int argc,
			    char **argv, char **err)
{
	struct dm_arg_set as;
	int r;

	if (argc < 6) {
		*err = "Insufficient args";
		return -EINVAL;
	}

	if (argc > 6) {
		*err = "Too many args";
		return -EINVAL;
	}

	as.argc = argc;
	as.argv = argv;

	r = parse_meta_dev(da, &as, err);
	if (r)
		return r;

	r = parse_data_dev(da, &as, err);
	if (r)
		return r;

	r = parse_block_size(da, &as, err);
	if (r)
		return r;

	r = parse_hash_algo(da, &as, err);
	if (r)
		return r;

	r = parse_backend(da, &as, err);
	if (r)
		return r;

	r = parse_flushrq(da, &as, err);
	if (r)
		return r;

	return 0;
}

static void destroy_dedup_args(struct dedup_args *da)
{
	if (da->meta_dev)
		dm_put_device(da->ti, da->meta_dev);

	if (da->data_dev)
		dm_put_device(da->ti, da->data_dev);
}

static int dm_dedup_ctr(struct dm_target *ti, unsigned int argc, char **argv)
{
	struct dedup_args da;
	struct dedup_config *dc;
	struct workqueue_struct *wq;

	struct init_param_inram iparam_inram;
	struct init_param_cowbtree iparam_cowbtree;
	void *iparam = NULL;
	struct metadata *md = NULL;

	sector_t data_size;
	int i,r;
	int crypto_key_size;

	struct on_disk_stats *data = NULL;
	uint64_t logical_block_counter = 0;
	uint64_t physical_block_counter = 0;
    struct work_order_entry* en = NULL;

	mempool_t *dedup_work_pool = NULL;

	bool unformatted;

    customfnv_mod_init(); 
    
#ifdef DEDUP_ENABLED
    if(ht_cleaners != NULL){
        ht_destroy(ht_cleaners);
    }
    ht_create_with_size(&ht_cleaners,"cleaners",100);
#endif
    
    work_order_entries = kmem_cache_create("wo_entries",sizeof(struct work_order_entry),0,SLAB_RED_ZONE, NULL);
    
    if(ht_cache_hashpbn != NULL){
        ht_destroy(ht_cache_hashpbn);
    }
    
    ht_create_with_size(&ht_cache_hashpbn,"hashpbn",500000);
    
    while(!list_empty(&work_order)){
        spin_lock(&work_order_lock);
        en = list_first_entry(&work_order, struct work_order_entry, list);
        list_del(&(en->list));
        spin_unlock(&work_order_lock);
        kfree(en);
    }
    
    if(ht_work_todo != NULL){
        ht_destroy(ht_work_todo);
    }
    ht_create_with_size(&ht_work_todo,"worktodo",500000);
    
    cache_cleaner = cleanup_register(ht_cache_hashpbn, NULL, 50, 5000);

	memset(&da, 0, sizeof(struct dedup_args));
	da.ti = ti;

	r = parse_dedup_args(&da, argc, argv, &ti->error);
	if (r)
		goto out;

	dc = kzalloc(sizeof(*dc), GFP_KERNEL);
	if (!dc) {
		ti->error = "Error allocating memory for dedup config";
		r = -ENOMEM;
		goto out;
	}

	wq = create_singlethread_workqueue("dm-dedup");
	if (!wq) {
		ti->error = "failed to create workqueue";
		r = -ENOMEM;
		goto bad_wq;
	}

	dedup_work_pool = mempool_create_kmalloc_pool(MIN_DEDUP_WORK_IO,
						sizeof(struct dedup_work));
	if (!dedup_work_pool) {
		ti->error = "failed to create mempool";
		r = -ENOMEM;
		goto bad_mempool;
	}

	dc->io_client = dm_io_client_create();
	if (IS_ERR(dc->io_client)) {
		ti->error = "failed to create dm_io_client";
		r = PTR_ERR(dc->io_client);
		goto bad_io_client;
	}

	dc->block_size = da.block_size;
	dc->sectors_per_block = to_sector(da.block_size);
	data_size = ti->len;
	(void) sector_div(data_size, dc->sectors_per_block);
	dc->lblocks = data_size;

	data_size = i_size_read(da.data_dev->bdev->bd_inode) >> SECTOR_SHIFT;
	(void) sector_div(data_size, dc->sectors_per_block);
	dc->pblocks = data_size;

	/* Meta-data backend specific part */
	switch(da.backend) {
	case BKND_INRAM:
		dc->mdops = &metadata_ops_inram;
		iparam_inram.blocks = dc->pblocks;
		iparam = &iparam_inram;
		break;
	case BKND_COWBTREE:
		dc->mdops = &metadata_ops_cowbtree;
		iparam_cowbtree.blocks = dc->pblocks;
		iparam_cowbtree.metadata_bdev = da.meta_dev->bdev;
		iparam = &iparam_cowbtree;
	}

	strcpy(dc->backend_str, da.backend_str);

	md = dc->mdops->init_meta(iparam, &unformatted);
	if (IS_ERR(md)) {
		ti->error = "failed to initialize backend metadata";
		r = PTR_ERR(md);
		goto bad_metadata_init;
	}

	dc->desc_table = desc_table_init(da.hash_algo);
	if (!dc->desc_table || IS_ERR(dc->desc_table)) {
		ti->error = "failed to initialize crypto API";
		r = PTR_ERR(dc->desc_table);
		goto bad_metadata_init;
	}

	crypto_key_size = get_hash_digestsize(dc->desc_table);

	dc->kvs_hash_pbn = dc->mdops->kvs_create_sparse(md, crypto_key_size,
				sizeof(struct hash_pbn_value),
				dc->pblocks, unformatted);
	if (IS_ERR(dc->kvs_hash_pbn)) {
		ti->error = "failed to create sparse KVS";
		r = PTR_ERR(dc->kvs_hash_pbn);
		goto bad_kvstore_init;
	}

	dc->kvs_lbn_pbn = dc->mdops->kvs_create_linear(md, 8,
			sizeof(struct lbn_pbn_value), dc->lblocks, unformatted);
	if (IS_ERR(dc->kvs_lbn_pbn)) {
		ti->error = "failed to create linear KVS";
		r = PTR_ERR(dc->kvs_lbn_pbn);
		goto bad_kvstore_init;
	}

	r = dc->mdops->flush_meta(md);
	if (r < 0) {
		ti->error = "failed to flush metadata";
		goto bad_kvstore_init;
	}

	if (!unformatted && dc->mdops->get_private_data) {
		r = dc->mdops->get_private_data(md, (void **)&data,
				sizeof(struct on_disk_stats));
		if (r < 0) {
			ti->error = "failed to get private data from superblock";
			goto bad_kvstore_init;
		}

		logical_block_counter = data->logical_block_counter;
		physical_block_counter = data->physical_block_counter;
	}

	dc->data_dev = da.data_dev;
	dc->metadata_dev = da.meta_dev;

	dc->workqueue = wq;
	dc->dedup_work_pool = dedup_work_pool;
    w_hint_dc = dc; 
	dc->bmd = md;

	dc->logical_block_counter = logical_block_counter;
	dc->physical_block_counter = physical_block_counter;

	dc->writes = 0;
	dc->dupwrites = 0;
	dc->uniqwrites = 0;
	dc->reads_on_writes = 0;
	dc->overwrites = 0;
	dc->newwrites = 0;

	strcpy(dc->crypto_alg, da.hash_algo);
	dc->crypto_key_size = crypto_key_size;

	dc->flushrq = da.flushrq;
	dc->writes_after_flush = 0;

	r = dm_set_target_max_io_len(ti, dc->sectors_per_block);
	if (r)
		goto bad_kvstore_init;

	ti->private = dc;
    
    for(i=0;i<WORK_TODO_NUM_SERVICE;i++){
        INIT_DELAYED_WORK(&work_todo[i],do_work_todo_process);
        schedule_delayed_work(&work_todo[i],msecs_to_jiffies(10));
    }

	return 0;

bad_kvstore_init:
	desc_table_deinit(dc->desc_table);
bad_metadata_init:
	if (md && !IS_ERR(md))
		dc->mdops->exit_meta(md);
	dm_io_client_destroy(dc->io_client);
bad_io_client:
	mempool_destroy(dedup_work_pool);
bad_mempool:
	destroy_workqueue(wq);
bad_wq:
	kfree(dc);
out:
	destroy_dedup_args(&da);
	return r;
}

static void dm_dedup_dtr(struct dm_target *ti)
{
	struct dedup_config *dc = ti->private;
	struct on_disk_stats data;
	int ret,i;

    for(i=0;i<WORK_TODO_NUM_SERVICE;i++){
	    cancel_delayed_work_sync(&work_todo[i]);
    }

	if (dc->mdops->set_private_data) {
		data.physical_block_counter = dc->physical_block_counter;
		data.logical_block_counter = dc->logical_block_counter;

		ret = dc->mdops->set_private_data(dc->bmd, &data,
				sizeof(struct on_disk_stats));
		if (ret < 0)
			DMERR("Failed to set the private data in superblock.");
	}

	ret = dc->mdops->flush_meta(dc->bmd);
	if (ret < 0)
		DMERR("Failed to flush the metadata to disk.");

	flush_workqueue(dc->workqueue);
	destroy_workqueue(dc->workqueue);

	mempool_destroy(dc->dedup_work_pool);

	dc->mdops->exit_meta(dc->bmd);

	dm_io_client_destroy(dc->io_client);

	dm_put_device(ti, dc->data_dev);
	dm_put_device(ti, dc->metadata_dev);
	desc_table_deinit(dc->desc_table);
    
	kfree(dc);
    
    customfnv_mod_fini(); 
    
    cleanup_unregister(cache_cleaner);
    cache_cleaner = NULL;
    
    if(ht_cache_hashpbn != NULL){
        ht_destroy(ht_cache_hashpbn);
    }
    ht_cache_hashpbn = NULL;
    
    if(ht_work_todo != NULL){
        ht_destroy(ht_work_todo);
    }
    ht_work_todo = NULL;

#ifdef DEDUP_ENABLED
    if(ht_cleaners != NULL){
        ht_destroy(ht_cleaners);
        ht_cleaners = NULL;
    }
#endif
    if(work_order_entries != NULL){
        kmem_cache_destroy(work_order_entries);
        work_order_entries = NULL;
    }
}

        
void prefetch_cache_remove(void* key, int32_t ksize){
    
}

static void dm_dedup_status(struct dm_target *ti, status_type_t status_type,
			    unsigned status_flags, char *result, unsigned maxlen)
{
	struct dedup_config *dc = ti->private;
	uint64_t data_total_block_count;
	uint64_t data_used_block_count;
	uint64_t data_free_block_count;
	uint64_t data_actual_block_count;
	int sz = 0;

	switch (status_type) {
	case STATUSTYPE_INFO:
		data_used_block_count = dc->physical_block_counter;
		data_actual_block_count = dc->logical_block_counter;
		data_total_block_count = dc->pblocks;

		data_free_block_count =
			data_total_block_count - data_used_block_count;

		DMEMIT("%llu %llu %llu %llu ",
			data_total_block_count, data_free_block_count,
			data_used_block_count, data_actual_block_count);

		DMEMIT("%u %s %s ", dc->block_size,
			dc->data_dev->name, dc->metadata_dev->name);

		DMEMIT("%llu %llu %llu %llu %llu %llu",
			dc->writes, dc->uniqwrites, dc->dupwrites,
			dc->reads_on_writes, dc->overwrites, dc->newwrites);
		break;
	case STATUSTYPE_TABLE:
		DMEMIT("%s %s %u %s %s %u",
			dc->metadata_dev->name, dc->data_dev->name, dc->block_size,
			dc->crypto_alg, dc->backend_str, dc->flushrq);
	}
}

#ifdef MARK_AND_SWEEP
struct mark_and_sweep_data {
	unsigned long *bitmap;
	uint64_t bitmap_len;
	uint64_t cleanup_count; /* number of hashes cleaned up */
	struct dedup_config *dc;
};

static int mark_lbn_pbn_bitmap(void *key, int32_t ksize,
			       void *value, int32_t vsize, void *data)
{
	int ret = 0;
	struct mark_and_sweep_data *ms_data =
		(struct mark_and_sweep_data *)data;
	uint64_t pbn_val = *((uint64_t *)value);

	BUG_ON(!data);
	BUG_ON(!ms_data->bitmap);
	BUG_ON(pbn_val > ms_data->bitmap_len);

	bitmap_set(ms_data->bitmap, pbn_val, 1);

	return ret;
}

static int cleanup_hash_pbn(void *key, int32_t ksize, void *value,
			    int32_t vsize, void *data)
{
	int ret = 0, r = 0;
	uint64_t pbn_val = 0;
	struct mark_and_sweep_data *ms_data =
		(struct mark_and_sweep_data *)data;
	struct hash_pbn_value hashpbn_value = *((struct hash_pbn_value *)value);
	struct dedup_config *dc = ms_data->dc;

	BUG_ON(!data);
	BUG_ON(!ms_data->bitmap);

	pbn_val = hashpbn_value.pbn;
	BUG_ON(pbn_val > ms_data->bitmap_len);

	if (test_bit(pbn_val, ms_data->bitmap) == 0) {

        //invalidate cache entry
        ht_remove(ht_cache_hashpbn, (*(unsigned long*)key));

		ret = dc->kvs_hash_pbn->kvs_delete(dc->kvs_hash_pbn,
							key, ksize);
		if (ret < 0)
			BUG();

		r = dc->mdops->dec_refcount(ms_data->dc->bmd, pbn_val);
		if (r < 0)
			BUG();

		ms_data->cleanup_count++;
	}

	return ret;
}

static int mark_and_sweep(struct dedup_config *dc)
{
	int err = 0;
	sector_t data_size = 0;
	uint64_t bitmap_size = 0;
	struct mark_and_sweep_data ms_data;

	BUG_ON(!dc);

	data_size = i_size_read(dc->data_dev->bdev->bd_inode) >> SECTOR_SHIFT;
	(void) sector_div(data_size, dc->sectors_per_block);
	bitmap_size = data_size;

	memset(&ms_data, 0, sizeof(struct mark_and_sweep_data));

	ms_data.bitmap = vmalloc(BITS_TO_LONGS(bitmap_size) *
			sizeof(unsigned long));
	if (!ms_data.bitmap) {
		DMERR("Could not vmalloc ms_data.bitmap");
		err = -ENOMEM;
		goto out;
	}
	bitmap_zero(ms_data.bitmap, bitmap_size);

	ms_data.bitmap_len = bitmap_size;
	ms_data.cleanup_count = 0;
	ms_data.dc = dc;

	/* Create bitmap of used pbn blocks */
	err = dc->kvs_lbn_pbn->kvs_iterate(dc->kvs_lbn_pbn,
			&mark_lbn_pbn_bitmap, (void *)&ms_data);
	if (err < 0)
		goto out_free;

	/* Cleanup hashes based on above bitmap of used pbn blocks */
	err = dc->kvs_hash_pbn->kvs_iterate(dc->kvs_hash_pbn,
			&cleanup_hash_pbn, (void *)&ms_data);
	if (err < 0)
		goto out_free;

	dc->physical_block_counter -= ms_data.cleanup_count;

out_free:
	vfree(ms_data.bitmap);
out:
	return err;
}

#else

static int cleanup_hash_pbn(void *key, int32_t ksize, void *value,
			    int32_t vsize, void *data)
{
	int r = 0;
	uint64_t pbn_val = 0;
	struct hash_pbn_value hashpbn_value = *((struct hash_pbn_value *)value);
	struct dedup_config *dc = (struct dedup_config *)data;

	BUG_ON(!data);

	pbn_val = hashpbn_value.pbn;

	if (dc->mdops->get_refcount(dc->bmd, pbn_val) == 1) {
		r = dc->kvs_hash_pbn->kvs_delete(dc->kvs_hash_pbn,
							key, ksize);
		if (r < 0)
			goto out;

		r = dc->mdops->dec_refcount(dc->bmd, pbn_val);
		if (r < 0)
			goto out_dec_refcount;

		dc->physical_block_counter -= 1;
	}

	goto out;

out_dec_refcount:
	dc->kvs_hash_pbn->kvs_insert(dc->kvs_hash_pbn, key,
			ksize, (void *)&hashpbn_value,
			sizeof(hashpbn_value));
out:
	return r;
}

static int garbage_collect(struct dedup_config *dc)
{
	int err = 0;

	BUG_ON(!dc);

	// Cleanup hashes if the refcount of block == 1 
	err = dc->kvs_hash_pbn->kvs_iterate(dc->kvs_hash_pbn,
			&cleanup_hash_pbn, (void *)dc);

	return err;
}
#endif

static int dm_dedup_message(struct dm_target *ti,
			    unsigned argc, char **argv)
{
	int r = 0;

	struct dedup_config *dc = ti->private;

	if (!strcasecmp(argv[0], "garbage_collect")) {
#ifdef MARK_AND_SWEEP
		r = mark_and_sweep(dc);
#else
		r = garbage_collect(dc);
#endif
		if (r < 0)
			DMERR("Error in performing garbage_collect: %d.", r);
	} else if (!strcasecmp(argv[0], "drop_bufio_cache")) {
		if (dc->mdops->flush_bufio_cache)
			dc->mdops->flush_bufio_cache(dc->bmd);
		else
			r = -ENOTSUPP;
	} else
		r = -EINVAL;

	return r;
}

void clear_stats(void){
    atomic_set(&unique,0);
    atomic_set(&duplicate,0);
    atomic_set(&count,0);
    atomic_set(&time,0);
    time1total[0] = 0;
    time1total[1] = 0;
    time2total[0] = 0;
    time2total[1] = 0;
    todocount[0] = 1;
    todocount[1] = 1;

    atomic_set(&cache_hits_1,0);
    atomic_set(&cache_hits_2,0);
    atomic_set(&cache_hits_3,0);
    
    atomic_set(&war_hints,0);
    atomic_set(&w_hints,0);
    atomic_set(&cache_inserts,0);
}

static char* get_stats(char* buff){
    buff += sprintf(buff,"unique %d duplicate %d hits[init-%d,war-%d,write-%d] inserts %d hints[war-%d,w-%d] count %d time %d (usecs) prio : time1total[0] %ld time1avg %ld (nsecs) time2total[0] %ld time2avg %ld (nsecs)  normal : time1total[1] %ld time1avg %ld (nsecs) time2total[1] %ld time2avg %ld (nsecs) cache ht_size %d work ht_size %d \n",atomic_read(&unique),atomic_read(&duplicate), atomic_read(&cache_hits_1),atomic_read(&cache_hits_2),atomic_read(&cache_hits_3), atomic_read(&cache_inserts), atomic_read(&war_hints), atomic_read(&w_hints), atomic_read(&count),atomic_read(&time), time1total[0], time1total[0]/todocount[0], time2total[0], time2total[0]/todocount[0], time1total[1], time1total[1]/todocount[1], time2total[1], time2total[1]/todocount[1], ht_get_size(ht_cache_hashpbn), ht_get_size(ht_work_todo));
    return buff;
}

static int dm_dedup_ioctl(struct dm_target *ti, unsigned int cmd,
            unsigned long arg)
{
	//struct dedup_config *dc = ti->private;
    int ret = 0;
    char *kptr = vmalloc(STATS_BUFF_SIZE);
    char *buff = kptr;

    printk(KERN_ERR "ioctl issued cmd %d \n",cmd);

    switch (cmd) {
        case UNIQUE_STATS_CLEAR:
            buff += sprintf(buff,"\nbefore: ");
            buff = get_stats(buff);
            clear_stats(); 
            buff += sprintf(buff,"after: ");
            buff = get_stats(buff);
            if (copy_to_user((void __user *) arg,kptr,STATS_BUFF_SIZE))
                ret = -EFAULT;
            break;
        case UNIQUE_STATS_PRINT:
            buff = get_stats(buff);
            if (copy_to_user((void __user *) arg,kptr,STATS_BUFF_SIZE))
                ret = -EFAULT;
            break;
        case DMDEDUP_DEBUG:
            dmdedup_debug_enabled = (arg > 0);
            printk(KERN_ERR "dmdedup_debug_enabled is %d \n",dmdedup_debug_enabled);
            break;
        default:
            printk(KERN_ERR "unknown ioctl %u issued\n",cmd);
            break;
    }
    if(kptr != NULL) vfree(kptr);
    return ret;
}

static struct target_type dm_dedup_target = {
	.name = "dedup",
	.version = {1, 0, 0},
	.module = THIS_MODULE,
	.ctr = dm_dedup_ctr,
	.dtr = dm_dedup_dtr,
	.map = dm_dedup_map,
	.ioctl = dm_dedup_ioctl,
	.message = dm_dedup_message,
	.status = dm_dedup_status,
};

static int __init dm_dedup_init(void)
{
	return dm_register_target(&dm_dedup_target);
}

static void __exit dm_dedup_exit(void)
{
	dm_unregister_target(&dm_dedup_target);
}

module_init(dm_dedup_init);
module_exit(dm_dedup_exit);

MODULE_DESCRIPTION(DM_NAME " target for data deduplication");
MODULE_LICENSE("GPL");
