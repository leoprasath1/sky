#include "testfwk.c"

int main_helper1(void){
    int fd,written,i,j,k;
    char *blk, c = 'A';
    char outblk[4096];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    printf("realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);

    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);

    if(mkdir("/mnt/disks/fsize/", 0777) == -1){
        perror("mkdir failed");
    }

    char fname[100];

    for(i=0;i<1;i++){
        int file_size = 4096*1024*120;
        sprintf(fname,"/mnt/disks/fsize/test%d",i);
        printf("creating file %s size %d \n", fname, file_size);
        fd = open(fname, O_CREAT | O_RDWR | O_LARGEFILE | O_SYNC | O_DIRECT, S_IRWXU |S_IRWXO | S_IRWXG );
        if(fd == -1) {
            perror("could not open file");
            return -1;
        }
        for(k=0;k<(file_size/4096);k++){
            /*
            for(j=0;j<8;j++){
                sprintf(blk+(j*512),"%6d%6d",i,k);
            }
            */
            written = write(fd,blk,4096);
            if(written == -1) {
                perror("write error");
            }
        }
        close(fd);
    }
    
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    sprintf(cmd,"echo '/bin/rm -rf /mnt/disks/fsize/' | sudo bash");
    run_cmd(cmd,buffer);
    main_helper1();
    sync();
}
