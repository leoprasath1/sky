#!/bin/bash -x

file="testfwklib.c"
fs="EXT3"
fssdir="fssdir"

#fss=(ZFS UFS)
fss=(EXT3 EXT4 XFS NILFS JFS REISERFS BTRFS)

uncomment ()
{
  output=$( grep "^//#define $1" $file )
  if [[ $? = 0 ]]; then
      sed -i "s/^\/\/#define $1/#define $1/g" $file
  fi
}

comment ()
{
  output=$( grep "^#define $1" $file )
  if [[ $? = 0 ]]; then
      sed -i "s/^#define $1/\/\/#define $1/g" $file
  fi
}

commentall ()
{
for i in ${fss[@]}; do
    comment $i
done
}

rm -rf $fssdir
mkdir $fssdir

for fs in ${fss[@]}; do
    commentall
    echo $fs
    uncomment $fs
    make clean
    make 
    mkdir $fssdir/$fs
    cp *.o $fssdir/$fs/
done
    
commentall
uncomment EXT3
make clean
make
