#!/bin/bash -x

sudo /bin/rm -rf /mnt/disks/*
cd /mnt/disks

test=2;

if [ "$test" -eq "0" ]
then 
cp /home/sura/linux.tar.gz /mnt/disks/ 
gzip -cd /mnt/disks/linux.tar.gz > /mnt/disks/linux.tar
for((num=0;num<2;num++)){
    START=$(date +%s);
    mkdir /mnt/disks/data$num/ 
    tar -C /mnt/disks/data$num/ -xf /mnt/disks/linux.tar 
    END=$(date +%s);
    echo "$num took $((END-START)) seconds"
}
fi

if [ "$test" -eq "1" ]
then 
for((num=0;num<2;num++)){
    START=$(date +%s);
    sudo cp /home/sura/random2G /mnt/disks/a$num.gz
    END=$(date +%s);
    echo "$num took $((END-START)) seconds"
}
fi

#/home/sura/cerny/jadukata/cmdclient.o trace 4195040 

if [ "$test" -eq "2" ]
then 
for((num=0;num<100;num++)){
    START=$(date +%s);
    sudo dd if=/dev/urandom of=/mnt/disks/1Mfile$num bs=4096 count=500 conv=sync
    END=$(date +%s);
    echo "$num took $((END-START)) seconds"
}
fi

#/home/sura/cerny/jadukata/cmdclient.o trace 0

/home/sura/cerny/jadukata/cmdclient.o get_cache_distribution
