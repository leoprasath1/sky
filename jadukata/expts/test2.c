#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>
#include <stdio.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main1(int argc, char *argv[])
{
    struct stat sb;
    char *linkname;
    ssize_t r;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("working on %s \n" , argv[1]);

    if (lstat(argv[1], &sb) == -1) {
        perror("lstat");
        exit(EXIT_FAILURE);
    }

    linkname = malloc(sb.st_size + 1);
    if (linkname == NULL) {
        fprintf(stderr, "insufficient memory\n");
        exit(EXIT_FAILURE);
    }

    r = readlink(argv[1], linkname, sb.st_size + 1);

    if (r == -1) {
        perror("lstat");
        exit(EXIT_FAILURE);
    }

    if (r > sb.st_size) {
        fprintf(stderr, "symlink increased in size "
                "between lstat() and readlink()\n");
        exit(EXIT_FAILURE);
    }

    linkname[r] = '\0';

    printf("'%s' points to '%s'\n", argv[1], linkname);

    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]){
    printf("PATH_MAX %d \n",PATH_MAX);
    printf("get pid test2 : %d \n", getpid());
    //sleep(10);
}
