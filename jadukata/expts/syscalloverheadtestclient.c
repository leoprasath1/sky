#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

int fd;
struct stat buf;
struct timespec req, rem;

int fd1;
char sysbuf[4096];
int err = 0;

extern int smartmode;

//#define SKIP

/*
int fake_syscall(long i){
    const char hello[] = "Hello World!\n";
    const int hello_size = sizeof(hello);
    int ret;  
    asm volatile
        (
         "movl $314, %%eax\n\t"
         "movl %1, %%edi\n\t"
         "movq $1, %%rsi\n\t"
         "movl $1, %%edx\n\t"
         "syscall\n\t"
         "sysret\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         : "=a"(ret)
         : "g"(i)
         : "%rdi", "%rsi", "%rdx", "%rcx", "%r11"
        );
}
*/

int mysyscall(long i){
   /* asm volatile
        (
         "movq $20, %%rax\n"
         "syscall\n"
         :
         :
         :
        );
    */
    
    char buff[1024];
    unsigned long* addr = (unsigned long*)&buff;
    unsigned long sleep_time[2] = {0,100000};
    int size = 1024*1024;
    char *buffer = malloc(size);
    //*addr = 10;
    //*(addr+8) = 0;

    unsigned long ret;

    //syscall version  %rdi, %rsi, %rdx, %r10, %r8, %r9 
    
    memcpy(addr,&sleep_time,sizeof(long)*2);

    //unsigned long scallnractual = 240;
    unsigned long scallnractual = 20;

    unsigned long scallnr = scallnractual;
    if(smartmode >= 0) scallnr = 258;

    asm volatile (
            "movq %2, %%rax\n"  /* first argument */
            "movq %1, %%rdi\n"  /* third argument */
            "movq %3, %%rsi\n"  /* third argument */
            "movq %4, %%rdx\n"  /* third argument */
            "movq %5, %%r10\n"  /* third argument */
            "syscall\n"
            "jge 1f\n"
            "movq $-1,%0\n"  /* first argument */
            "jmp 2f\n"
            "1:\n"  /* first argument */
            "movq %%rax,%0\n"  /* first argument */
            "2:\n"  /* first argument */
            : "=m"(ret)
            : "m"(addr), "m"(scallnr), "m"(buffer), "m"(size), "m"(scallnractual)
            : "rax", "rdi", "rsi" , "rdx", "r10"
            ); 

    /*
    fake_syscall(i);
    if(syscall(SYS_gettid) == 0){
        perror("ERROR gettid\n");
    }
    if(syscall(SYS_lseek,fd,0,SEEK_CUR) != 0){
        perror("ERROR lseek\n");
    }

    if(syscall(SYS_stat,"/home/sura/cerny/jadukata/syscalloverheadtestclient.c",&buf) != 0){
        perror("ERROR stat\n");
    }

    if(syscall(SYS_nanosleep,&req,&rem) != 0){
        perror("ERROR sleeping\n");
    }

    if(syscall(SYS_pwrite64,fd1,sysbuf,4096,0) == -1){
        perror("ERROR write\n");
    }
    
    if(syscall(SYS_pread64,fd1,sysbuf,4096,0) == -1){
        perror("ERROR read\n");
    }
    
    if(syscall(SYS_write,fd1,sysbuf,4096) == -1){
        perror("ERROR write\n");
    }
    
    if(syscall(SYS_read,fd1,sysbuf,4096) == -1){
        perror("ERROR read\n");
    }

    if(syscall(314,i) == -1){
        perror("ERROR cerny dummy\n");
    }
    */

    free(buffer);
}

/*
int fakesyscall(void){
    const char hello[] = "Hello World!\n";
    const size_t hello_size = sizeof(hello);
    ssize_t ret;
    asm volatile
        (
         "movl $1, %%eax\n\t"
         "movl $1, %%edi\n\t"
         "movq %1, %%rsi\n\t"
         "movl %2, %%edx\n\t"
         "syscall"
         : "=a"(ret)
         : "g"(hello), "g"(hello_size)
         : "%rdi", "%rsi", "%rdx", "%rcx", "%r11"
        );
    return 0;
}
*/

int mysyscallsetup(void){
    /*
    int i;
    fd = open("/home/sura/cerny/jadukata/syscalloverheadtestclient.c",O_RDONLY);
    if(fd == -1){
        perror("error opening file fd");
    }
    */
    req.tv_sec = 0;
    req.tv_nsec = 10000000; 
    
    /*
    fd1 = open("/mnt/disks/test",O_RDWR|O_CREAT , S_IRWXU | S_IRWXO | S_IRWXG);
    if(fd1 == -1){
        perror("error opening file fd1");
    }

    for(i=0;i<100000;i++){
        if(syscall(SYS_write,fd1,sysbuf,4096) == -1){
            perror("ERROR write");
        }
    }
    drop_caches();
    */
}

int mysyscallteardown(void){
    /*
    if(fd != -1){
        close(fd);
    }
    */
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0 ;
    long long numcalls;
    int j;
    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%lld",&numcalls);

    mysyscallsetup();
        
    sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
    run_cmd(cmd,buffer);
    
    for(j=0;j<NUM_ITERS;j++){

        syscall(SYS_lseek,fd1,0,SEEK_SET);
        drop_caches();

        err = 0;
        sprintf(tmp,"iterations %lld.%d \n",numcalls,j);
        i = numcalls;

        #ifdef SKIP
        sprintf(cmd,"sudo %s/cmdclient.o addskipcr3 %lx %d",homedir,cr3,pid);
        run_cmd(cmd,buffer);
        #endif

        TIMER_START;
        while(i--){
            mysyscall((long)i);
        }
        TIMER_END(tmp);
        
        #ifdef SKIP
        sprintf(cmd,"sudo %s/cmdclient.o delskipcr3 %lx ",homedir,cr3);
        run_cmd(cmd,buffer);
        #endif

        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs per syscall time %0.2f usecs \n",j,times[j], ((times[j]*1000000)/numcalls));
        
        sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
        run_cmd(cmd,buffer);
        
        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);
    }
    printf("sum %0.2f secs avg %0.2f secs per syscall time %0.2f usecs \n",total, (total/NUM_ITERS), (((total/NUM_ITERS)*1000000)/numcalls));
    
    mysyscallteardown();

    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(0);
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
