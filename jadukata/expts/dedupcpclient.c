#include "testfwk.c"

void setup(int argc, char** argv){
    setup_guest_disks(1);
    
    int i;
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    sprintf(cmd,"%s/expts/dedupcp_setup.sh",homedir);
    run_cmd_sys(cmd);
    
    drop_caches();
}

void runner(int argc, char** argv){
    unsigned long cr3;
    char* homedir = "/home/arulraj/cerny/jadukata";
    //char * space = (char*)malloc(4096*10);
    //unsigned long* addr = (unsigned long*)(space+(4096*2));
    //*addr = 0x1234;
    //pid_t pid = getpid();

    char buffer[MAX_PAYLOAD];
    char cmd[CMD_LEN];
    char tmp[4096];
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace %ld ",homedir,ipow(2,22) + ipow(2,5)  + ipow(2,12));
    //run_cmd(cmd,buffer);
        
    TIMER_START;

    sprintf(cmd,"%s/expts/dedupcp_wkld.sh",homedir);
    run_cmd_sys(cmd);
    
    TIMER_END(tmp);
        
    printf("time taken by dedupcpclient wkld total %0.2f secs \n",total_time);
    
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
}
