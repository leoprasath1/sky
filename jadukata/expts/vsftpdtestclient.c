#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

void vsftpd_stop(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/vsftpd/vsftpd_stop.sh",homedir);
    run_cmd_o(cmd);
}

void vsftpd_trace_echo(char* str){
}

void vsftpd_start(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/vsftpd/vsftpd_start.sh",homedir);
    run_cmd_sys(cmd);
}
        
void vsftpd_setup(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    vsftpd_trace_echo("starting setup");
    vsftpd_stop();

    sprintf(cmd,"%s/vsftpd/vsftpd_setup.sh",homedir);
    run_cmd_o(cmd);
    
    drop_caches();
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
}

void vsftpd_teardown(){
    vsftpd_trace_echo("starting teardown");
    vsftpd_stop();
    
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/vsftpd/vsftpd_log.sh ",homedir);
    run_cmd_o(cmd);
}

int vsftpdwkld(char *mode){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/vsftpd/vsftpd_wkld.sh > /mnt/disks/wkld.out",homedir);
    run_cmd_o(cmd);
}

void vsftpdwkld_clear(){
    vsftpd_trace_echo("starting clear");
    vsftpd_stop(); 
    drop_caches();
    vsftpd_start(); 
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    /*
    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%s ",mode);
    */

    vsftpd_setup();

    for(j=0;j<NUM_ITERS;j++){

        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);

        drop_caches();

        vsftpdwkld_clear();

        sprintf(tmp,"iterations %s.%d \n",mode,j);
        
        TIMER_START;
        vsftpdwkld(mode);
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n",j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
        run_cmd(cmd,buffer);
    }
        
    vsftpd_teardown();

    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
    //setup_guest_disks(0);
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
