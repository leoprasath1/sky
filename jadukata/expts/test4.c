#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    int i;

    for(i=0;i<300000;i++){
        write(-10,buffer,0);
        usleep(2000);
    }

    return 0;
}

void setup(int argc, char** argv){
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
