#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

int linuxbenchwkld(char* mode){
    run_cmd_o("tar -C /mnt/disks xJf linux-3.12.tar.xz");
    run_cmd_o("sync");
}

int linuxbenchsetup(void){
    run_cmd_o("cp linux-3.12.tar.xz /mnt/disks/");
    drop_caches();
    return 0;
}

int linuxbenchteardown(void){
    return 0;
}

#define NUM_ITERS (4)
//#define SKIP

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    linuxbenchsetup();
        
    sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
    run_cmd(cmd,buffer);
    
    for(j=0;j<NUM_ITERS;j++){
        drop_caches();

        sprintf(tmp,"iterations %s.%d \n",mode,j);

        #ifdef SKIP
        sprintf(cmd,"sudo %s/cmdclient.o addskipcr3 %lx %d",homedir,cr3,pid);
        run_cmd(cmd,buffer);
        #endif

        TIMER_START;
        linuxbenchwkld(mode);
        TIMER_END(tmp);
        
        #ifdef SKIP
        sprintf(cmd,"sudo %s/cmdclient.o delskipcr3 %lx ",homedir,cr3);
        run_cmd(cmd,buffer);
        #endif

        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n",j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
        run_cmd(cmd,buffer);
        
        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);
    }

    printf("sum %0.2f secs avg %0.2f secs\n",total, (total/NUM_ITERS));
    
    linuxbenchteardown();

    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
