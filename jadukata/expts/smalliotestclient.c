
#include "testfwk.c"

#define BUFFER_SIZE (8192)
#define BLK_SIZE (4096)

#define SRAND_SEED (12345)
long long NUM_IOS = 1;
int IO_SIZE = 4096;

//char* FILE_PATH = "/usr/home/arulraj/mntdisks/test";
char* FILE_PATH = "/mnt/disks/test";

//#define SYNC
//#define RAND
#define REP
//#define SEQ
//#define PREADWRITE
//#define DELCR3 //without jadukata
#define READS
//#define WRITES
//#define TESTMODE

//#define FASTSETUP
//#define SKIPSETUP

//int iosizes[] = {1,22,111,333,444,512,555,777,999,1024,1111,1444,1888,2222,2444,2777,3072,3333,3666,3999,4096,4333,4888,5120,5555,5999,6144,6444,6888,7333,7680,7777,7999,8192};
//int iosizes[] = {4333,4888,5120,5555,5999,6144,6444,6888,7333,7680,7777,7999,8192};
//int iosizes[] = {1,22,111,333,444,512,555,777,999,1024,1111,1444,1888,2222,2444,2777,3072,3333,3666,3999,4096};
//int iosizes[] = {2444,3072,3333,3666,3999,4096,4888,5555};
//int iosizes[] = {512,1024,3072,4096,5120,6144,7680,8192};
//int iosizes[] = {22,512,2444,3072,4096,6444,8192};
//int iosizes[] = {22,512};
int iosizes[] = {4096};
//int iosizes[] = {6144,8192};
//int iosizes[] = {1,11,111,254,512,786,1024,2048,2489,3072,3987,4096};
//int numios[] = {1000,10000,20000,30000,50000,80000,100000};
//int iosizes[] = {254,3987,254,3987,254,512,786,1024,2048,2489,3072,3987,4096,254,512,786,1024,2048,2489,3072,3987,4096,254,512,786,1024,2048,2489,3072,3987,4096,254,512,786,1024,2048,2489,3072,3987,4096,254,512,786,1024,2048,2489,3072,3987,4096,254,512,786,1024,2048,2489,3072,3987,4096};
//int numios[] = {10000000};
int numios[] = {1000000};
//int numios[] = {200000};
//int numios[] = {20000};
//int numios[] = {100};
//int numios[] = {10};
//int numios[] = {2000,1000,5000,3000,2000,1000,5000,3000,2000,1000,3000,2000,1000,5000,3000,2000,1000,5000,3000,2000,1000,3000,2000,1000,5000,3000,2000,1000,5000,3000,2000,1000,3000,2000,1000,5000,3000,2000,1000,5000,3000,2000,1000,3000,2000,1000,5000,3000,2000,1000,5000,3000,2000,1000,3000,2000,1000,5000,3000,2000,1000,5000,3000,2000,1000};
int NUM_ITERS = 1;
int CONST_OFF = 512*16;

int skip = 0, skipi = 7777, skipj = 500;
int count = 10000; 

int setup_helper(int max_num_ios){
    long fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[BUFFER_SIZE];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFFER_SIZE + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    printf(" setup: realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    memset(blk,0,BUFFER_SIZE);
    for(i=0;i<BUFFER_SIZE;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(SRAND_SEED);

    unsigned long exptid = getpid();

    fd = open(FILE_PATH, O_CREAT | O_RDWR | O_LARGEFILE , S_IRWXU |S_IRWXO | S_IRWXG );

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }

    printf("setup buffer %lx max_num_ios %d RAND_MAX %d MAXSIZE %d \n",(unsigned long)blk, max_num_ios, RAND_MAX, MAXSIZE);

    for(i=0;i<max_num_ios;i++){
#if defined(RAND)
        r = get_random_offset(BUFFER_SIZE,max_num_ios, MAXSIZE);
#elif defined(SEQ) || defined(REP)
        r = (i*BUFFER_SIZE);
#endif
 
#if defined(SEQ) || defined(REP) || defined(RAND)
//#if !defined(REP) || defined(RAND)
        for(j=0;j<(BUFFER_SIZE/512);j++){
            char* print = blk+(j*512);
            sprintf(print,"%6ld %6ld %12d %10d",exptid,i,myrand(64000),myrand(32000));
        }
#endif

        if(lseek(fd,r,SEEK_SET) == -1){
            char msg[1000];
            sprintf(msg,"lseek failed for %ld\n", r);
            perror(msg);
            exit(1);
        }
        written = write(fd,blk,BUFFER_SIZE);
        if(written == -1) {
            perror("write error");
            exit(1);
        }
    }

    fsync(fd);
    close(fd);
    sync();

    drop_caches(); 
    printf("setup done \n");
    extern int maxloopcount;
    extern float maxlooptime;
    printf("max loop count %d max loop time %f \n", maxloopcount, maxlooptime);
    return 0;
}

char cmd[CMD_LEN];
char buffer[MAX_PAYLOAD];
    
void sleeplib()
{
    char buff[1024];
    unsigned long* addr = (unsigned long*)&buff;

    unsigned long sleep_time[2] = {10,0};

    unsigned long ret;

    //syscall version  %rdi, %rsi, %rdx, %r10, %r8, %r9 

    memcpy(addr,&sleep_time,sizeof(long)*2);
    
    printf("sleeping now  \n");

    asm volatile (
            "movq $240, %%rax\n"  /* first argument */
            "movq %1, %%rdi\n"  /* third argument */
            "movq $0, %%rsi\n"  /* third argument */
            "syscall\n"
            "jge 1f\n"
            "movq $-1,%0\n"  /* first argument */
            "jmp 2f\n"
            "1:\n"  /* first argument */
            "movq %%rax,%0\n"  /* first argument */
            "2:\n"  /* first argument */
            : "=m"(ret)
            : "m"(addr)
            : "rax", "rdi", "rsi"
            ); 

    // ret = nanosleep(addr, NULL);
}

int write_2g_helper(){
    long fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[BUFFER_SIZE];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    int max_num_ios = 600000;
    
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFFER_SIZE + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;
    
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);

    printf(" write2g: realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    memset(blk,0,BUFFER_SIZE);
    for(i=0;i<BUFFER_SIZE;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(SRAND_SEED);

    fd = open("/mnt/disks/temp", O_CREAT | O_RDWR | O_LARGEFILE , S_IRWXU |S_IRWXO | S_IRWXG );

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }

    printf("write2g buffer %lx max_num_ios %d RAND_MAX %d MAXSIZE %d \n",(unsigned long)blk, max_num_ios, RAND_MAX, MAXSIZE);

    for(i=0;i<max_num_ios;i++){
        written = write(fd,blk,BUFFER_SIZE);
        if(written == -1) {
            perror("write error");
            exit(1);
        }
    }

    fsync(fd);
    close(fd);
    sync();

    drop_caches(); 
                
    printf("write2g done \n");
    return 0;
}


int main_helper(int mode, int log){
    long fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[BUFFER_SIZE];
    int flags = 0;

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFFER_SIZE + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 4194304 ",homedir);
    //sprintf(cmd,"sudo %s/cmdclient.o trace 4194352 ",homedir);
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);
    
    memset(blk,0,BUFFER_SIZE);
    for(i=0;i<BUFFER_SIZE;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }
    
    srand(SRAND_SEED);

    unsigned long exptid = getpid();
    
    //sleeplib();

    flags = O_CREAT | O_RDWR | O_LARGEFILE ;
#ifdef SYNC
    flags |= O_SYNC ;
#endif
    fd = open(FILE_PATH, flags , S_IRWXU |S_IRWXO | S_IRWXG );
    if( log ) {
        out = open("/tmp/a",O_CREAT | O_RDWR, S_IRWXU);
        if(out == -1) {
            perror("could not open log file");
            return -1;
        }
        if(lseek(out,0,SEEK_END) == -1){
            perror("lseek to end of log file failed ");
        }
    }

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }

    printf("buffer %lx IO_SIZE %d NUM_IOS %lld RAND_MAX %d MAXSIZE %d \n",(unsigned long)blk, IO_SIZE, NUM_IOS, RAND_MAX, MAXSIZE);
            
    //sleeplib();

    for(i=0;i<NUM_IOS;i++){
        //usleep(2000);
       
#if defined(REP)
        r = CONST_OFF;
#elif defined(SEQ)
        r = (i* (((IO_SIZE/BLK_SIZE)+(IO_SIZE%BLK_SIZE?1:0))*BLK_SIZE) );
        //r = (i*BUFFER_SIZE);
#elif defined(RAND)
        r = get_random_offset(BUFFER_SIZE,NUM_IOS, MAXSIZE);
#endif
 
#if defined(SEQ) || defined(REP) || defined(RAND)
//#if !defined(REP) || defined(RAND)
        for(j=0;j<(BUFFER_SIZE/512);j++){
            char* print = blk+(j*512);
            sprintf(print,"%6ld %6ld %12d %10d",exptid,i,myrand(64000),myrand(32000));
            if(log) {
                write(out,outblk,sprintf(outblk,"%c %lu %ld.%ld\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
            }
        }
#endif
    
#ifndef PREADWRITE
        if(lseek(fd,r,SEEK_SET) == -1){
            char msg[1000];
            sprintf(msg,"lseek failed for %ld\n", r);
            perror(msg);
            exit(1);
        }
#endif
        
        //myrandset(blk,IO_SIZE);

        if ( mode == 1  ) {
#ifndef PREADWRITE
            written = write(fd,blk,IO_SIZE);
#else
            written = pwrite(fd,blk,IO_SIZE,r);
#endif
            if(written == -1 || written == 0) {
                printf("write error %ld %ld \n", written, r);
                perror("write error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%ld bytes written at %ld \n",written,r));
            }
            //sleep(10);
            //fsync(fd);
        }else{
#ifndef PREADWRITE
            re = read(fd,blk,IO_SIZE);
#else
            re = pread(fd,blk,IO_SIZE,r);
#endif
            if(re == -1 || re == 0) {
                printf("read error %ld %ld \n", re , r);
                perror("read error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%ld bytes read from %ld \n",re,r));
            }
        }
#ifndef PREADWRITE
#ifdef TESTMODE
        unsigned long curoff = lseek(fd,0,SEEK_CUR);
        unsigned long expoff = r+IO_SIZE;
        if(curoff != expoff){
            perror("ERROR lseek offset mismatch\n");
            printf("ERROR lseek offset mismatch curoff %lu expoff %lu \n", curoff, expoff);
            exit(1);
        }
#endif
#endif
        //printf("r %ld \n",r);
        //if(i%100==0) printf("i %ld \n",i);
    }
            
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);

    close(fd);
    if(log){
        close(out);
    }
    printf("done \n");
    extern int maxloopcount;
    extern float maxlooptime;
    printf("max loop count %d max loop time %f i is %ld \n", maxloopcount, maxlooptime,i);
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);

#ifndef SKIPSETUP
    int i;
#ifdef FASTSETUP
    int max = 0;
#else
    int max = CONST_OFF*10;
#endif

#if defined(RAND) || defined(SEQ)
    for(i=0;i<ARRAY_SIZE(numios);i++){
        if(numios[i] > max){
            max = numios[i];
        }
    }
#endif
    setup_helper(max);
#endif
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[1024];

    /*
    if(argc != 3){
        printf("error: pass numios and iosize arg\n");
        return;
    }
    sscanf(argv[1],"%lld", &NUM_IOS);
    sscanf(argv[2],"%lld", &IO_SIZE);
    */
     
    /*           
    sprintf(cmd,"sudo %s/cmdclient.o test 0 ",homedir);
    run_cmd(cmd,buffer);
    */
    
    /*
    sprintf(cmd,"sudo %s/cmdclient.o trace 3",homedir);
    run_cmd(cmd,buffer);

    int pidd = getpid();
    sprintf(cmd,"sudo ltrace -TSLf -D071 -p %d -o /tmp/q &",pidd);
    run_cmd_sys(cmd);
    
    sprintf(cmd,"sudo %s/diskdriver/scripts/ioctl test %d ",homedir,pidd);
    run_cmd(cmd,buffer);
    */
    
#ifdef DELCR3
    sprintf(cmd,"sudo %s/cmdclient.o delcr3 %lx ",homedir,cr3);
    run_cmd(cmd,buffer);
#endif

    //temp
    /*
    sprintf(cmd,"sudo blockdev --setra  256 /dev/sdb");
    run_cmd(cmd,buffer);
    sprintf(cmd,"sudo blockdev --setra  256 /dev/DISKDRIVER0");
    run_cmd(cmd,buffer);
    */

    sprintf(cmd,"sudo dmesg -c > /dev/null ");
    run_cmd(cmd,buffer);
#ifdef CERNY
    sprintf(cmd,"sudo %s/scripts/ioctl trace ",homedir);
    run_cmd(cmd,buffer);
#endif
    
    //write_2g_helper();
    
    drop_caches();
                
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
            
    //sprintf(cmd,"sudo %s/cmdclient.o trace 4195040 ",homedir);
    //run_cmd(cmd,buffer);

    int i,j,k;
    for(i=0;i<ARRAY_SIZE(iosizes);i++){
        IO_SIZE = iosizes[i]; 
        for(k=0;k<ARRAY_SIZE(numios);k++){
            NUM_IOS = numios[k]; 

            if(skip && skipi == IO_SIZE && skipj == NUM_IOS){
                skip = 0;
            }

            if(skip) continue;
            count--;
            if(count <0) break;

            float times[NUM_ITERS];
            for(j=0;j<NUM_ITERS;j++){
                drop_caches();
#ifdef CERNY
                sprintf(cmd,"sudo %s/scripts/ioctl reset_stats ",homedir);
                run_cmd(cmd,buffer);
#endif
                
                sprintf(tmp,"smalliotest.%d.%lld.%d ",IO_SIZE,NUM_IOS,j);
    
                sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
                run_cmd(cmd,buffer);
    
                TIMER_START;
#ifdef READS
                main_helper(0,0);
#endif
#ifdef WRITES
                main_helper(1,0);
#endif
#if defined(RAND) || defined(SEQ)
                //sync();
                drop_caches();
#endif
                TIMER_END(tmp);
                times[j] = total_time;
#ifdef LOW 
                //sprintf(cmd,"sudo dmesg -c > /tmp/%s ",tmp);
                //run_cmd(cmd,buffer);
#ifdef CERNY
                sprintf(cmd,"sudo %s/scripts/ioctl get_stats ",homedir);
                run_cmd(cmd,buffer);
                sprintf(cmd,"sudo %s/scripts/ioctl reset_stats ",homedir);
                run_cmd(cmd,buffer);
#endif
#else
                sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
                run_cmd(cmd,buffer);

                sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
                run_cmd(cmd,buffer);
#endif
            }
                
            float sum = 0, t;
            for(j=0;j<NUM_ITERS;j++){
                printf("iter #%d for iosize %d numios %lld took %0.2f secs %0.2f msecs \n",j,iosizes[i],NUM_IOS,myroundf(times[j]),myroundf(times[j]*1000.0));
                sum += times[j];
            }
            t = sum/(float)NUM_ITERS;
            printf("average for iosize %d numios %lld took %0.2f secs %0.2f msecs \n",iosizes[i],NUM_IOS,myroundf(t),myroundf(t*1000.0));
        }
    }
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);
}
