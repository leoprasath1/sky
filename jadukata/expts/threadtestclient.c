#include "testfwk.c"
#include <sys/syscall.h>

#define NUM_THREADS (4)
pthread_t threads[NUM_THREADS];


void hog(int msecs){
    double total_time = msecs*1000000;
    struct timespec start, end;
    //while(total_time){
    //    clock_gettime(CLOCK_REALTIME_COARSE, &start); /* mark start time */
        for(long i= 20000; i > 0; i--){
        //for(long i= 5000000000; i > 0; i--){
            //for(long j= LONG_MAX; j > 0; j--){
            //    //for(long k= LONG_MAX; k > 0; k--){
            //    //    for(long l= LONG_MAX; l > 0; l--){

            //    //    }
            //    //}
            //}
        }
        //clock_gettime(CLOCK_REALTIME_COARSE, &end);   /* mark the end time */

        //double diff = (1000000000UL * (end.tv_sec - start.tv_sec)) + end.tv_nsec - start.tv_nsec;
        //total_time -= diff;
        //printf("time taken %g\n",diff);
    //}
}


int main_helper1(int mode, int log, int filenum){
#define NUM_IOS (5)
    int fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[MAX_PAYLOAD];
    char filename[100];

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);

    blk = alignedbuff;

    printf(" realbuff %p alignedbuff %p\n",realbuff, alignedbuff);

    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);
    sprintf(filename, "/mnt/disks/test%d",filenum);
    fd = open(filename, O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG );
    if( log ) {
        out = open("/tmp/a",O_CREAT | O_RDWR, S_IRWXU);
        if(out == -1) {
            perror("could not open log file");
            return -1;
        }
        if(lseek(out,0,SEEK_END) == -1){
            perror("lseek to end of log file failed ");
        }
    }

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }
    printf("buffer %p \n",blk);
    for(i=0;i<NUM_IOS;i++){
        printf("io num %d \n",i);
        for(j=0;j<8;j++){
            sprintf(blk+(j*512),"%6d",i);
            if(log) {
                //write(out,outblk,sprintf(outblk,"%c %lu %d.%d\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
            }
        }

        //r = get_random_offset(4096,NUM_IOS,MAXSIZE);
        r = i*4096;

        if(lseek(fd,r,SEEK_SET) == -1){
            perror("lseek failed ");
            exit(1);
        }
        if ( mode == 1  ) {
            written = write(fd,blk,4096);
            if(written == -1) {
                perror("write error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes written at %d \n",written,r));
            }
            //sleep(10);
            //fsync(fd);
        }else{
            re = read(fd,blk,4096);
            if(re == -1) {
                perror("read error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes read from %d \n",re,r));
            }
        }
        hog(100);
    }

    close(fd);
    if(log){
        close(out);
    }
    return 0;
}

void* main_helper(void* arg){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    pthread_t *t = (pthread_t*) arg;
    int threadnum = (((unsigned long)t-(unsigned long)threads) / sizeof(pthread_t));
    printf("thread number %d pid %d tid %ld \n", threadnum, getpid(), syscall(SYS_gettid));
	
    main_helper1(1,0,threadnum);        
    
    //pthread_exit(0);

    return NULL;    
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc,char** argv){
    //sprintf(cmd,"sudo %s/cmdclient.o test %lx %lx ",homedir,0,addr);
    //run_cmd(cmd,buffer);

    //main_helper(1);
    //main_helper(0);
    int i =0;
    for(i=0;i<NUM_THREADS;i++){
        pthread_create(&threads[i],NULL,(void * (*)(void *))main_helper,&threads[i]);
    }
    for(i=0;i<NUM_THREADS;i++){
        //while(pthread_tryjoin_np(threads[i], NULL))
        //    sleep(3);
        pthread_join(threads[i], NULL);
    }
    return;
}
