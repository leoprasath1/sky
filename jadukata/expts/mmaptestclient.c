#include "testfwk.c"

int main_helper(int mode){
	const int SIZE = 4096 * 5;
	char * mapped;
	unsigned long addr;
	int i;
	char *blk, c = 'A';
	char outblk[4096];
	int fd;

	int pagesize = getpagesize();

	char *realbuff = (char*)malloc(4096 + pagesize);
	char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);

	blk = alignedbuff;

	printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);

	memset(blk,0,4096);
	for(i=0;i<4096;i++){
		if(i%1024 == 1023){
			blk[i] = '\n';
		}else{
			blk[i] = c;
		}
	}

	/* Open the file for reading. */
    	fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE , S_IRWXU | S_IRWXO | S_IRWXG);
	if(fd == -1) {
		perror("could not open file");
		return -1;
	}
	//start_ftrace();
	/* Memory-map the file. */
	mapped = mmap (0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(mapped == MAP_FAILED){
		perror("mmap failed");
		return -1;
	}
	/* Now do something with the information. */
	for(i=0;i<(SIZE/4096);i++){
		addr = (unsigned long)mapped + (i*4096);
		printf(" mmap address %lx page %lx offset %lx mode %d \n",addr,addr/4096,addr%4096, mode);
		if(mode == 1){
			memcpy((char*)addr,blk,4096);
		}else{
			memcpy(blk,(char*)addr,4096);
		}
		if(msync((char*)addr,4096,MS_SYNC) != 0){
			printf(" error msync failed \n");
		}
	}

	//read(fd, blk, 4096);

    if(munmap(mapped,SIZE) != 0){
		perror("munmap failed\n");
		return -1;
	}
	//stop_ftrace();
	close(fd);

	return 0;    
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    main_helper(1);
    //main_helper(0);
}
