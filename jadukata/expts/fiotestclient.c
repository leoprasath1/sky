#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

int fiowkld(char* mode){
    char cmd[CMD_LEN];
    sprintf(cmd,"/home/leoa/software/fio-2.1.4/fio /home/sura/cerny/jadukata/expts/confs/fiojobs/%s",mode);
    run_cmd_o(cmd);
    //drop_caches();
    //sprintf(cmd,"sync ");
    //run_cmd_o(cmd);
}

int fiosetup(void){
    return 0;
}

int fioteardown(void){
    return 0;
}

#define NUM_ITERS (3)
//#define SKIP

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%s ",mode);

    fiosetup();
        
    sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
    run_cmd(cmd,buffer);
    
    for(j=0;j<NUM_ITERS;j++){

        setup_guest_disks(1);

        drop_caches();

        sprintf(tmp,"iterations %s.%d \n",mode,j);

        #ifdef SKIP
        sprintf(cmd,"sudo %s/cmdclient.o addskipcr3 %lx %d",homedir,cr3,pid);
        run_cmd(cmd,buffer);
        #endif

        TIMER_START;
        fiowkld(mode);
        TIMER_END(tmp);
        
        #ifdef SKIP
        sprintf(cmd,"sudo %s/cmdclient.o delskipcr3 %lx ",homedir,cr3);
        run_cmd(cmd,buffer);
        #endif

        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n", j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
        run_cmd(cmd,buffer);
        
        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);
    }

    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    fioteardown();

    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
