#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>

#include <sys/mman.h> 
#include <sched.h> 
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

//#define BSD
//#define ZFS

#define EXT3
//#define EXT4
//#define XFS
//#define NILFS
//#define JFS
//#define REISERFS
//#define BTRFS


#ifdef BSD
#define O_LARGEFILE (0)
#define BSDDISK  "/dev/vtbd1"
#define BSDMOUNTPOINT  "/mnt/disks"
#else
#define LINUXDISK  "/dev/vdb"
#define LINUXMOUNTPOINT  "/mnt/disks"
#endif

#include "../kvm-kmod-3.10.1/x86/checksums.c"


#ifdef CERNY
char* homedir = "/home/arulraj/cerny";
#else
char* homedir = "/home/arulraj/cerny/jadukata";
#endif

int debug = 0;

#include "khash.h"

#define TIMER_START  struct timeval start,end; float total_time; gettimeofday(&start,NULL);

#define TIMER_END_NO_PRINT gettimeofday(&end,NULL); total_time = (end.tv_sec - start.tv_sec) + (((float)(end.tv_usec - start.tv_usec))/1000000);

#define TIMER_END(name) gettimeofday(&end,NULL); total_time = (end.tv_sec - start.tv_sec) + (((float)(end.tv_usec - start.tv_usec))/1000000); printf("%s took %.2f secs = %.2f msecs \n",name,(ceilf(total_time*100.0)/100.0), (ceilf(total_time*100000.0)/100.0));

#define printf(f,a...)  do { time_t __t = time(NULL); printf("%s: " f, strtok(ctime(&__t),"\n") , ##a); } while(0);

#define myprintf(f,a...) do { time_t __t = time(NULL); printf("%s: " f, strtok(ctime(&__t),"\n") , ##a); } while(0);

#define my_foreach(h, i, kvar, vvar) \
    for(i = kh_begin(h); i != kh_end(h); ++i)     \
        if( kh_exist(h,i) && ((kvar = kh_key(h,i)) || !kvar) && ((vvar = kh_val(h,i)) || !vvar) ) \

#include "../kvm-kmod-3.10.1/x86/filesize_ioclass.c"
#include "../kvm-kmod-3.10.1/x86/all_common.h"
#define CMD_LEN (40960)
                
int run_cmd_sys(char* cmd){
    return system(cmd);
}

int run_cmd_core_helper(char* cmd, char* output, int log, int maxpayload){
    int ret = 1;
    int bufsize = (maxpayload);
    char* outbuff = (char*)malloc(bufsize);
    if(outbuff == NULL){
        printf("ERROR malloc failed %d \n ", maxpayload);
    }
    memset(outbuff,0,bufsize);
    char* outptr = outbuff;
    printf(" cmd %s \n",cmd);
    outptr[0] = '\0';
    FILE* pipe = popen(cmd, "r");
    if (pipe == NULL)
    {
        perror("popen ERROR\n");
        printf("popen ERROR\n");
        exit(1);
    }   
    while(!feof(pipe)) 
    {
        fgets(outptr, maxpayload, pipe);
        outptr+=strlen(outptr);
        if((outptr-outbuff)>=(bufsize)){
            printf("ERROR too much output %ld %d \n", (outptr-outbuff), bufsize);
            if(log){
                if(debug){
                    printf("output so far : %s \n", outbuff);
                }
            }
            outptr = outbuff;
        }
    }
    memset(output,0,bufsize);
    memcpy(output,outbuff,(outptr-outbuff));
    ret = pclose(pipe);
    if(log){
        if(ret == 0){
            printf(" cmd %s output %s \n",cmd, output);
        }else{
            printf(" cmd ERROR %s output %s \n",cmd, output);
        }
    }
    free(outbuff);
    return ret;
}

int run_cmd_core(char* cmd, char* output, int log){
    return run_cmd_core_helper(cmd, output, log, MAX_PAYLOAD);
}

int run_cmd(char* cmd, char* output){
    return run_cmd_core(cmd,output,1);
}

int run_cmd_s(char* cmd, char* output){
	char scmd[CMD_LEN];
	sprintf(scmd,"/usr/local/bin/sudo bash -c \"%s\" ",cmd);
	return run_cmd(scmd,output);
}

int run_cmd_o(char* cmd){
    char buffer[MAX_PAYLOAD];
    return run_cmd(cmd,buffer);
}

static unsigned long ipow(unsigned long base, unsigned long exp)
{
    unsigned long result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
} 

static inline float myroundf(float t){
    return ceilf(t*100.0)/100.0;
}

void drop_caches(void){    
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    sprintf(cmd,"sync ");
    run_cmd(cmd,buffer);


#ifndef BSD
    sprintf(cmd,"echo 'echo 3 > /proc/sys/vm/drop_caches' | /usr/local/bin/sudo bash");
    run_cmd(cmd,buffer);
#else
#ifndef ZFS
    sprintf(cmd,"/usr/local/bin/sudo umount %s",BSDMOUNTPOINT);
    run_cmd(cmd,buffer);

    sprintf(cmd,"/usr/local/bin/sudo mount %s %s", BSDDISK, BSDMOUNTPOINT);
    run_cmd(cmd,buffer);
#else
    sprintf(cmd,"/usr/local/bin/sudo zfs umount mypool/data ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo zfs umount mypool ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo zfs mount mypool ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo zfs mount mypool/data ");
    run_cmd(cmd,buffer);
#endif
#endif

#ifdef LOW
#else
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o dropcaches",homedir);
    run_cmd(cmd,buffer);
#endif
}

void perform_backup(void){    
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    sprintf(cmd,"tar cf /mnt/disks/backup.tar  /mnt/disks/* ");
    run_cmd(cmd,buffer);
}

KHASH_MAP_INIT_INT(32, int)
khash_t(32) *dupoffset = NULL;

int map_to_hash(char* map, khash_t(32) *h){
    char* blocks;
    khiter_t hiter;
    int ret;

    blocks = strstr(map,"BLOCKS:");

    if(blocks!= NULL){
        char *ignore = " \t\r\n";
        char *b,*t,*t1,*pa;
        int s,e,ls,le;
        blocks+=7;

        while(blocks != NULL){
            blocks += strspn(blocks,ignore);
            b = blocks;
            pa = index(blocks,'-');
            t = index(blocks,':');
            if( t == NULL){
                return 0;
            }
            t1 = index(t,'-');
            blocks = index(blocks,',');
            if(blocks){
                blocks++;
            }
            if( (pa && pa < t)){
                sscanf(t+1,"%d-%d,",&s,&e);
                //printf("|%s|",index(b,'('));
                sscanf(index(b,'('),"(%d-%d)",&ls,&le);
                //printf("*%d&%d*\n",ls,le);
                if(debug){
                    printf("(%d-%d):%d-%d\n",ls,le,s,e);
                }
                while(s<=e){
                    hiter = kh_get(32,h,ls);
                    if(hiter == kh_end(h)){
                        hiter = kh_put(32,h,ls,&ret);
                        if(!ret){
                            printf("ERROR inserting %d into hash table : ret %d \n",s, ret);
                            exit(1);
                        }
                        kh_value(h,hiter) = s;
                    }
                    ls++;
                    s++;
                }
            }else{
                char* p = index(b,'(');
                if(p && *(p+1)>= '0' && *(p+1) <= '9'){
                    sscanf(p+1,"%d",&ls);
                    sscanf(t+1,"%d",&s);
                    hiter = kh_get(32,h,ls);
                    if(hiter == kh_end(h)){
                        hiter = kh_put(32,h,ls,&ret);
                        if(!ret){
                            printf("ERROR inserting %d into hash table : ret %d \n",s, ret);
                            exit(1);
                        }
                        kh_value(h,hiter) = s;
                    }
                }
            }
        }
        return 0;
    }
    return 1;
}

int myrand(int num){
    float r = ((float)rand() /(float)RAND_MAX)*(float)(num-1) ;
    //printf("random %f %d num %d \n",r,(int)r,num);
    return r;
}

void myrandset(char* blk,int size){
    int i;
    for(i=0;i<size;i++){
        blk[i] = 'a' + myrand(25);
    }
}

int maxloopcount = 0;
float maxlooptime = 0;

int get_random_offset_new(int BUFFER_SIZE, int num_randoms, int max_random){
        khiter_t dupoffsetiter;
        int ret, r, max_blocks = (max_random/BUFFER_SIZE);
        int mult, dist = max_blocks / num_randoms;
        TIMER_START
        if(dist <= 0){
            dist = 1;
        }
        if(dist > 10000){
            dist = 10000;
        }
        mult = dist * BUFFER_SIZE;
        max_blocks /= dist;
        int loopcount = 0;
        while(1){
            r = myrand(max_blocks);
            r *= mult;
            r = (r/BUFFER_SIZE)*BUFFER_SIZE;
            dupoffsetiter = kh_get(32,dupoffset,r);
            if(dupoffsetiter == kh_end(dupoffset)){
                dupoffsetiter = kh_put(32,dupoffset,r,&ret);
                if(!ret){
                    printf("ERROR inserting %d into hash table : ret %d \n",r, ret);
                    exit(1);
                }
                break;
            }
            loopcount++;
            //printf("skipping duplicated offset %d \n", r);
        }
        if(loopcount > maxloopcount){
            maxloopcount = loopcount;
        }
        TIMER_END_NO_PRINT
        if(total_time > maxlooptime){
            maxlooptime = total_time;
        }
        return r;
}

int get_random_offset_old(int BUFFER_SIZE, int num_randoms, int max_random){
    khiter_t dupoffsetiter;
    int r, ret;
    while(1){
        r = rand()%max_random;
        r = (r/BUFFER_SIZE)*BUFFER_SIZE;
        dupoffsetiter = kh_get(32,dupoffset,r);
        if(dupoffsetiter == kh_end(dupoffset)){
            dupoffsetiter = kh_put(32,dupoffset,r,&ret);
            if(!ret){
                printf("ERROR inserting %d into hash table : ret %d \n",r, ret);
                exit(1);
            }
            break;
        }
        //printf("skipping duplicated offset %d \n", r);
    }
    return r;
}

int get_random_offset(int BUFFER_SIZE, int num_randoms, int max_random){
    return get_random_offset_new(BUFFER_SIZE, num_randoms, max_random);
}

