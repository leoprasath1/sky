#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE

#define MAXSIZE (2000000000)

//high
#define JADUKATA
//#define CERNY
//#define LOW
//#define JOURNAL_IGNORE
#define SMARTMODE

/*
//low
//#define JADUKATA
//#define CERNY
#define LOW
//#define JOURNAL_IGNORE
*/

#include "testfwklib.c"

#define ARRAY_SIZE(a)  (sizeof(a) / sizeof(*(a)))
    
static inline unsigned add32_with_carry(unsigned a, unsigned b)
{
    asm("addl %2,%0\n\t"
            "adcl $0,%0"
            : "=r" (a)
            : "0" (a), "r" (b));
    return a;
}

static inline unsigned short from32to16(unsigned a) 
{
    unsigned short b = a >> 16; 
    asm("addw %w2,%w0\n\t"
            "adcw $0,%w0\n" 
            : "=r" (b)
            : "0" (b), "r" (a));
    return b;
}

static unsigned do_csum(const unsigned char *buff, unsigned len)
{
    unsigned odd, count;
    unsigned long result = 0;

    if (len == 0)
        return result; 
    odd = 1 & (unsigned long) buff;
    if (odd) {
        result = *buff << 8;
        len--;
        buff++;
    }
    count = len >> 1;        /* nr of 16-bit words.. */
    if (count) {
        if (2 & (unsigned long) buff) {
            result += *(unsigned short *)buff;
            count--;
            len -= 2;
            buff += 2;
        }
        count >>= 1;        /* nr of 32-bit words.. */
        if (count) {
            unsigned long zero;
            unsigned count64;
            if (4 & (unsigned long) buff) {
                result += *(unsigned int *) buff;
                count--;
                len -= 4;
                buff += 4;
            }
            count >>= 1;    /* nr of 64-bit words.. */

            /* main loop using 64byte blocks */
            zero = 0;
            count64 = count >> 3;
            while (count64) { 
                asm("addq 0*8(%[src]),%[res]\n\t"
                        "adcq 1*8(%[src]),%[res]\n\t"
                        "adcq 2*8(%[src]),%[res]\n\t"
                        "adcq 3*8(%[src]),%[res]\n\t"
                        "adcq 4*8(%[src]),%[res]\n\t"
                        "adcq 5*8(%[src]),%[res]\n\t"
                        "adcq 6*8(%[src]),%[res]\n\t"
                        "adcq 7*8(%[src]),%[res]\n\t"
                        "adcq %[zero],%[res]"
                        : [res] "=r" (result)
                        : [src] "r" (buff), [zero] "r" (zero),
                        "[res]" (result));
                buff += 64;
                count64--;
            }

            /* last upto 7 8byte blocks */
            count %= 8; 
            while (count) { 
                asm("addq %1,%0\n\t"
                        "adcq %2,%0\n" 
                        : "=r" (result)
                        : "m" (*(unsigned long *)buff), 
                        "r" (zero),  "0" (result));
                --count; 
                buff += 8;
            }
            result = add32_with_carry(result>>32,
                    result&0xffffffff); 

            if (len & 4) {
                result += *(unsigned int *) buff;
                buff += 4;
            }
        }
        if (len & 2) {
            result += *(unsigned short *) buff;
            buff += 2;
        }
    }
    if (len & 1)
        result += *buff;
    result = add32_with_carry(result>>32, result & 0xffffffff); 
    if (odd) { 
        result = from32to16(result);
        result = ((result >> 8) & 0xff) | ((result & 0xff) << 8);
    }
    return result;
}

unsigned long csum_partial(const void *buff, int len, unsigned long sum)
{
    //return (unsigned long)add32_with_carry(do_csum(buff, len),
    //        ( unsigned )sum);
    return FNVHash(buff,len,sum);
}

#ifndef BSD
void start_ftrace(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    pid_t pid = getpid();
    sprintf(cmd,"mount -t debugfs nodev /debug ");
    run_cmd_s(cmd,buffer);

    sprintf(cmd,"echo 'function_graph' > /debug/tracing/current_tracer ");
    run_cmd_s(cmd,buffer);

    sprintf(cmd,"echo '40960' > /debug/tracing/buffer_size_kb ");
    run_cmd_s(cmd,buffer);

    sprintf(cmd,"echo '' > /debug/tracing/trace ");
    run_cmd_s(cmd,buffer);

    sprintf(cmd,"echo '%d' > /debug/tracing/set_ftrace_pid ",pid);
    run_cmd_s(cmd,buffer);
    
    sprintf(cmd,"echo 1 >/debug/tracing/tracing_on ");
    run_cmd_s(cmd,buffer);

    sprintf(cmd,"echo 1 >/debug/tracing/tracing_enabled ");
    run_cmd_s(cmd,buffer);
}

void stop_ftrace(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    sprintf(cmd,"echo 0 >/debug/tracing/tracing_enabled ");
    run_cmd_s(cmd,buffer);

    sprintf(cmd,"echo '' > /debug/tracing/set_ftrace_pid ");
    run_cmd_s(cmd,buffer);
}
#endif

int smartmode = -1;
unsigned long cr3;
pid_t pid;
char mountpath[100];
char externphydiskpath[100];
char externemdiskpath[100];

extern void runner(int argc,char** argv);
extern void setup(int argc,char** argv);

int emdisk_size=8388608;

int preexp_setup(int argc, char** argv){
    TIMER_START;
    setup(argc,argv);
    TIMER_END("setup"); 
    return 0;
}

int core(int argc, char** argv){
    printf("****************************CORE START********************\n");
    TIMER_START;
    runner(argc,argv);
    TIMER_END("runner"); 
    printf("****************************CORE END********************\n");
    return 0;
}

void setup_guest_disks(int format){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD], *ptr, *t;
    pid_t pid = getpid();
    struct timeval start,end;
    unsigned long total;
    
#ifndef BSD
    sprintf(cmd,"/usr/local/bin/sudo bash -c 'echo 8 > /proc/sys/kernel/printk'");
    run_cmd(cmd,buffer);
#endif

    sprintf(cmd,"/usr/local/bin/sudo /bin/rm /tmp/a");
    run_cmd(cmd,buffer);

    sprintf(cmd,"/usr/local/bin/sudo umount /mnt/disks ");
    run_cmd(cmd,buffer);
    
    char loaderargs[4096];
#ifdef CERNY
    char *emdisk_size="160836480";
    char *exptdiskpath = "/dev/sdb1";
    char *emdiskpath = "/dev/CERNY0";
    sprintf(loaderargs,"%s \n",emdisk_size);
#else

#ifndef BSD
    char *emdiskpath = LINUXDISK;
#else
    char *emdiskpath = BSDDISK;
#endif

#ifdef LOW
    //char *emdisk_size="160836480";
    char *emdisk_size="29296875";
    char *ramcache_size="16384";
    char *diskcache_size="13737984";

    char *exptdiskpath = "/dev/sdb1";
    sprintf(loaderargs," %s %s %s \n",emdisk_size,ramcache_size,diskcache_size);
#else
    //char *emdisk_size= "8388608";
    char *emdisk_size= "25165824";
#ifndef BSD
    char *exptdiskpath = LINUXDISK; // vda sdb
#else
    char *exptdiskpath = BSDDISK; // vda sdb
#endif
    sprintf(loaderargs,"%s \n",emdisk_size);
#endif
#endif

    sprintf(externphydiskpath,"%s",exptdiskpath);
    sprintf(externemdiskpath,"%s",emdiskpath);

    printf("loaderargs %s \n", loaderargs);
    
    
#ifndef BSD
    sprintf(cmd,"/usr/local/bin/sudo umount /dev/DISKDRIVER0");
    run_cmd(cmd,buffer);
    sprintf(cmd,"/usr/local/bin/sudo umount /dev/CERNY0");
    run_cmd(cmd,buffer);
#endif
    
    sprintf(cmd,"/usr/local/bin/sudo umount %s ",exptdiskpath);
    run_cmd(cmd,buffer);

#ifdef BSD 
    sprintf(cmd,"/usr/local/bin/sudo zfs umount mypool/data ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo zpool destroy mypool");
    run_cmd(cmd,buffer);
#endif    
    sprintf(cmd,"/usr/local/bin/sudo umount %s ",emdiskpath);
    run_cmd(cmd,buffer);

#ifdef LOW 
    sprintf(cmd,"/usr/local/bin/sudo rmmod kvm_intel ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo rmmod kvm ");
    run_cmd(cmd,buffer);
#endif
    
#ifndef BSD
    sprintf(cmd,"/usr/local/bin/sudo rmmod DISKDRIVER ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo rmmod CERNY ");
    run_cmd(cmd,buffer);
#endif
    
#ifndef LOW
    if(format){
        sprintf(cmd,"/usr/local/bin/sudo wipefs -fa %s ", exptdiskpath);
        run_cmd(cmd,buffer);
#ifndef BSD
#ifdef EXT3
        sprintf(cmd,"/usr/local/bin/sudo mkfs.ext3 -F %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#ifdef EXT4
        sprintf(cmd,"/usr/local/bin/sudo mkfs.ext4 -F %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#ifdef NILFS
        sprintf(cmd,"/usr/local/bin/sudo mkfs.nilfs2 -f %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#ifdef XFS
        sprintf(cmd,"/usr/local/bin/sudo mkfs.xfs -f %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#ifdef JFS
        sprintf(cmd,"/usr/local/bin/sudo mkfs.jfs -f %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#ifdef REISERFS
        sprintf(cmd,"/usr/local/bin/sudo mkfs.reiserfs -f %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#ifdef BTRFS
        sprintf(cmd,"/usr/local/bin/sudo mkfs.btrfs -f %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif
#else

#ifndef ZFS
        //sprintf(cmd,"/usr/local/bin/sudo newfs -b 8192 -f 1024 %s ",exptdiskpath);
        //sprintf(cmd,"/usr/local/bin/sudo newfs -U -b 4096 -f 4096 %s ",exptdiskpath);
        //sprintf(cmd,"/usr/local/bin/sudo newfs -b 4096 -f 4096 %s ",exptdiskpath);
        //sprintf(cmd,"/usr/local/bin/sudo newfs -U -b 65536 -f 32768 -g 1048576 -d 1048576 %s ",exptdiskpath);
        sprintf(cmd,"/usr/local/bin/sudo newfs %s ",exptdiskpath);
        run_cmd(cmd,buffer);
#else
        sprintf(cmd,"/usr/local/bin/sudo zpool create mypool %s",exptdiskpath);
        run_cmd(cmd,buffer);
        sprintf(cmd,"/usr/local/bin/sudo zfs create -o compression=off mypool/data ",exptdiskpath);
        run_cmd(cmd,buffer);
        sprintf(cmd,"/usr/local/bin/sudo zfs set mountpoint=/mnt/disks  mypool/data ",exptdiskpath);
        run_cmd(cmd,buffer);
#endif

#endif
        drop_caches();
#ifdef JOURNAL_IGNORE
        sprintf(cmd,"/usr/local/bin/sudo debugfs -R 'stat <8>' %s ",exptdiskpath);
        run_cmd(cmd,buffer);

        t = strstr(buffer,"BLOCKS");
        ptr = strchr(buffer,'\n');
        while(ptr!=NULL){
            *ptr = ' ';
            ptr = strchr(buffer,'\n');
        }
        sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o journal_handle '%s'",homedir,t);
        run_cmd(cmd,buffer);
#endif
    }
#endif

#ifdef LOW
//change the config to HIGH 
    sprintf(cmd,"sed -i 's/#define LOW/\\/\\/#define LOW/g' %s/diskdriver/includes.h",homedir);
    run_cmd(cmd,buffer);
    sprintf(cmd,"sed -i 's/\\/\\/#define HIGH/#define HIGH/g' %s/diskdriver/includes.h",homedir);
    run_cmd(cmd,buffer);
#endif
   
#ifdef CERNY
    sprintf(cmd,"/usr/local/bin/sudo %s/scripts/loader.py %s ",homedir,loaderargs);
    run_cmd(cmd,buffer);
#else
#ifndef BSD
    //sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/loader.py %s ",homedir,emdisk_size);
    //run_cmd(cmd,buffer);
#endif
#endif

#ifdef LOW
//restore it back to LOW
    sprintf(cmd,"sed -i 's/\\/\\/#define LOW/#define LOW/g' %s/diskdriver/includes.h",homedir);
    run_cmd(cmd,buffer);
    sprintf(cmd,"sed -i 's/#define HIGH/\\/\\/#define HIGH/g' %s/diskdriver/includes.h",homedir);
    run_cmd(cmd,buffer);
#endif

    //    return;   

#ifndef ZFS
    sprintf(cmd,"/usr/local/bin/sudo mount %s /mnt/disks", emdiskpath);
    run_cmd(cmd,buffer);
#endif
    sprintf(cmd,"/usr/local/bin/sudo mount");
    run_cmd(cmd,buffer);

    strcpy(mountpath,emdiskpath);
   
    /* 
    sprintf(cmd,"/usr/local/bin/sudo mount /dev/sdb1 /mnt/disks ");
    run_cmd(cmd,buffer);
    */
    
    sprintf(cmd,"/usr/local/bin/sudo chmod 777 /mnt/disks ");
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o clear",homedir);
    run_cmd(cmd,buffer);
}

void set_readahead(int onoroff){    
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    int ra = onoroff?256:0;

#ifndef BSD
    sprintf(cmd,"/usr/local/bin/sudo blockdev --setra %d %s ",ra,externphydiskpath);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo blockdev --setra %d %s ",ra,externemdiskpath);
    run_cmd(cmd,buffer);
#else
    sprintf(cmd,"/usr/local/bin/sudo sysctl vfs.read_max=%d",ra);
    run_cmd(cmd,buffer);
#endif

    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o readahead %d",homedir,ra);
    run_cmd(cmd,buffer);
}


int main(int argc, char** argv){
    unsigned long base;
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD], *ptr;
    struct timeval start,end;
    unsigned long total;
    pid = getpid();
    
    if(argc < 2){
        printf("pass smartmode as arg\n");
        return 1;
    }
    sscanf(argv[1],"%d ",&smartmode);
   
#ifndef LOW

#ifndef BSD
//    sprintf(cmd,"/usr/local/bin/sudo modprobe netconsole ");
//    run_cmd(cmd,buffer);
//    
//    sprintf(cmd,"/usr/local/bin/sudo bash -c 'echo 8 > /proc/sys/kernel/printk'");
//    run_cmd(cmd,buffer);
#endif

    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o logrotate ",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o trace 0 ",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o readfill 0 ",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    run_cmd(cmd,buffer);
#endif

    preexp_setup(argc,argv);

#ifdef LOW
//noop
#else
#ifndef BSD
#ifdef JADUKATA
   // sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl gettaskptr ",homedir);
   // run_cmd(cmd,buffer);

   // sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o settaskptr %s ",homedir,buffer);
   // run_cmd(cmd,buffer);
   // 
   // sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl startvmmcomm \n",homedir);
   // run_cmd(cmd,buffer);

   // sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl getvmmcommbase \n",homedir);
   // run_cmd(cmd,buffer);
   // sscanf(buffer, "%lx", &base);

   // sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o setvmmcommbase %lx \n",homedir,base);
   // run_cmd(cmd,buffer);
#endif
#endif
#endif

#ifndef BSD
#ifdef CERNY
    sprintf(cmd,"/usr/local/bin/sudo %s/scripts/ioctl start \n",homedir);
    run_cmd(cmd,buffer);
#else
    //sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl start \n",homedir);
    //run_cmd(cmd,buffer);
#endif
#endif

#ifdef LOW
//noop
#else
#ifdef JADUKATA
#ifndef BSD
    //sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl getcr3 %d ",homedir,pid);
    //run_cmd(cmd,buffer);
    //sscanf(buffer, "%lx", &cr3);
#else
    cr3 = 0;
#endif

    if(smartmode !=0 && smartmode !=1 && smartmode !=-1){
        printf("ERROR : invalid smartmode value %d defaulting to 1 \n", smartmode);
        smartmode = 1;
    }

    unsigned long log = ipow(2,22);
    //log += ipow(2,4);
    //log += ipow(2,5);
    log = 0;
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o trace %ld ",homedir,log);
    run_cmd(cmd,buffer);
        
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o cache_smartdumb %d \n",homedir,smartmode);
    run_cmd(cmd,buffer);

    if(smartmode >= 0){
        sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o addcr3 %lx %d\n",homedir,cr3,pid);
        run_cmd(cmd,buffer);
    }
#endif
    
    //int pid = fork();
    //
    //if(pid == 0){
    //    char* eargv[] = {"datefork","-R"};
    //    printf("child %d \n", getpid());
    //    //unsigned int i,j;
    //    //i = -1 ;
    //    //j = -1 ;
    //    //while(i-- > 0){
    //    //    //while(j-- > 0){
    //    //    //}
    //    //}
    //    printf("child %d exiting \n", getpid());
    //    execve("/bin/date",eargv,NULL);
    //    perror("execve failed\n");
    //    //_exit(0);
    //    exit(0);
    //}else{
    //    printf("parent pid %d got vfork ret as %d\n", getpid(),pid);
    //}

    //exit(0);

    //run_cmd("/usr/local/bin/sudo /bin/date -R ",buffer);
    //run_cmd("/bin/echo abcdabcdaab",buffer);
    //exit(0);

    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
    
#endif

    core(argc-1,argv+1);
    
#ifndef BSD
#ifdef CERNY
    sprintf(cmd,"/usr/local/bin/sudo %s/scripts/ioctl stop \n",homedir);
    run_cmd(cmd,buffer);
#else
    //sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl stop \n",homedir);
    //run_cmd(cmd,buffer);
#endif
#endif
    
    //log = ipow(2,22);
    //log += ipow(2,4);
    //log += ipow(2,5);
    //log = 0;
    
    //sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o trace %ld ",homedir,log);
    //run_cmd(cmd,buffer);

#ifdef LOW
    sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl get_stats ",homedir);
    run_cmd(cmd,buffer);
#else
#ifndef BSD
   // sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o unsetvmmcommbase \n",homedir);
   // run_cmd(cmd,buffer);
   // 
   // sprintf(cmd,"/usr/local/bin/sudo %s/diskdriver/scripts/ioctl stopvmmcomm \n",homedir);
   // run_cmd(cmd,buffer);
#endif

    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o delcr3 %lx ",homedir,cr3);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o cache_smartdumb %d \n",homedir,-1);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o status",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    
    //sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o get_cached_sectors",homedir);
    //run_cmd_core(cmd,buffer,1);
    
#endif

    return 0;
}
