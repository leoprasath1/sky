#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

int findbenchwkld(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    /*
    {
        TIMER_START;
        sprintf(cmd,"sudo fsck -n -f %s",mountpath);
        run_cmd(cmd,buffer);
        TIMER_END("fscktime");
        sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
        run_cmd(cmd,buffer);
    }
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    */

    //sprintf(cmd,"sudo %s/cmdclient.o trace 112 ",homedir);
    //run_cmd(cmd,buffer);

    /*
    {
        TIMER_START;
        run_cmd_o("sudo tar -C /mnt/disks -cRzf /mnt/disks/linuxnew.tar.gz /mnt/disks/data ");
        TIMER_END("tartime");
        sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
        run_cmd(cmd,buffer);
    }
    

    {
        TIMER_START;
        run_cmd_o("sudo find /mnt/disks/ -name notfindpattern");
        TIMER_END("findtime");
        sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
        run_cmd(cmd,buffer);
    }
    */
    
    return 0;
}

int findbenchsetup(void){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 3 ",homedir);
    //run_cmd(cmd,buffer);
    
    TIMER_START;
    run_cmd_o("mkdir -p /mnt/disks/data ");
    run_cmd_o("cp /home/sura/linux.tar.gz /mnt/disks/ ");
    {
        TIMER_START;
        run_cmd_o("ltrace -cTSLf gzip -cd /mnt/disks/linux.tar.gz > /mnt/disks/linux.tar");
        TIMER_END("gunzip");
    }
    {
        TIMER_START;
        run_cmd_o("ltrace -cTSLf tar -C /mnt/disks/data/ -xf /mnt/disks/linux.tar ");
        TIMER_END("untar");
    }
    //run_cmd_o("/bin/rm /mnt/disks/linux.tar.gz ");
    //run_cmd_o("cp /home/sura/linux.tar.gz /mnt/disks/a1 ");
    //run_cmd_o("cp /home/sura/linux.tar.gz /mnt/disks/a2 ");
    drop_caches();
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);
    
    TIMER_END("findsetuptime");
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 0 ",homedir);
    run_cmd(cmd,buffer);

    return 0;
}

int findbenchteardown(void){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    run_cmd(cmd,buffer);

    return 0;
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    int j;

    //sprintf(cmd,"sudo %s/cmdclient.o trace 112 ",homedir);
    //run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    findbenchsetup();

    for(j=0;j<NUM_ITERS;j++){
        sprintf(tmp,"iterations %d \n",j);

        TIMER_START;
        findbenchwkld();
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n", j,times[j]);
    }
        
    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    findbenchteardown();
    
    //sprintf(cmd,"sudo %s/cmdclient.o resize_cache 100 ",homedir);
    //run_cmd_core(cmd,buffer,1);

    return 0;
}

void setup(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    setup_guest_disks(1);
    
    //sprintf(cmd,"sudo %s/cmdclient.o resize_cache 8 ",homedir);
    //run_cmd_core(cmd,buffer,1);
    
    /*
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    //hack to bring fsck contents into cache at metadata before untar 
    sprintf(cmd,"sudo %s/cmdclient.o cache_smartdumb %d \n",homedir,smartmode);
    run_cmd(cmd,buffer);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 112 ",homedir);
    //run_cmd(cmd,buffer);

    sprintf(cmd,"sudo fsck -n -f %s",mountpath);
    run_cmd(cmd,buffer);
    sleep(10);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);
    */

}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
