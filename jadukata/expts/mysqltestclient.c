#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

void mysql_cmd(char* scmd){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    sprintf(cmd,"sudo bash -c ' %s | tee -a /mnt/disks/traces.txt ' ",scmd);
    run_cmd_core(cmd,buffer,1);
}

void mysqld_stop(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/mysql/mysql_stop.sh",homedir);
    run_cmd_sys(cmd);
}

void mysqld_trace_echo(char* str){
    char cmd[CMD_LEN];
    sprintf(cmd,"echo \"tracelog %s\" ",str);
    mysql_cmd(cmd);
}

void mysqld_start(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/mysql/mysql_start.sh",homedir);
    run_cmd_sys(cmd);
}

void mysqlwkld_clear(){
    mysqld_trace_echo("starting clear");
    mysqld_stop(); 
    drop_caches();
    mysqld_start(); 
}
        
void mysql_setup(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    mysqld_trace_echo("starting setup");
    mysqld_stop();

    {
        TIMER_START;
        sprintf(cmd,"%s/mysql/mysql_setup.sh",homedir);
        run_cmd_sys(cmd);
        TIMER_END("mysqlsetup");
    }
    
    mysqlwkld_clear();

    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    mysql_cmd(cmd);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution ",homedir);
    mysql_cmd(cmd);
    
    mysqld_trace_echo("starting dataload");
    {
        TIMER_START;
        sprintf(cmd,"%s/mysql/mysql_dataload.sh",homedir);
        run_cmd_sys(cmd);
        TIMER_END("mysqldataload");
    }

    //drop_caches();
    mysqlwkld_clear();
        
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    mysql_cmd(cmd);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution ",homedir);
    mysql_cmd(cmd);

    mysqld_trace_echo("starting index");
    {
        TIMER_START;
        sprintf(cmd,"%s/mysql/mysql_index.sh",homedir);
        run_cmd_sys(cmd);
        TIMER_END("mysqlindex");
    }
    
    mysqlwkld_clear();

    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    mysql_cmd(cmd);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution ",homedir);
    mysql_cmd(cmd);
}

void mysql_teardown(){
    mysqld_trace_echo("starting teardown");
    mysqld_stop();
}

int mysqlwkld(char *mode){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/mysql/mysql_wkld.sh",homedir);
    run_cmd_sys(cmd);
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    /*
    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%s ",mode);
    */

    mysql_setup();

    for(j=0;j<NUM_ITERS;j++){

        mysqld_trace_echo("starting workdload");

        sprintf(tmp,"iterations %s.%d \n",mode,j);
        
        TIMER_START;
        mysqlwkld(mode);
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n",j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
        run_cmd(cmd,buffer);
    }
        
    mysql_teardown();

    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
    //setup_guest_disks(0);
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    sprintf(cmd,"sudo %s/cmdclient.o readfill 0 ",homedir);
    run_cmd(cmd,buffer);
    
    set_readahead(1);
    
    sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    run_cmd(cmd,buffer);

    
    main_helper(argc,argv);
    
    set_readahead(1);

    sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o readfill 1 ",homedir);
    run_cmd(cmd,buffer);
    
}
