#include "testfwk.c"
#include <sys/syscall.h>

int main_helper1(int mode, int log){
#define NUM_IOS (5)
    int fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[4096];

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);

    blk = alignedbuff;

    printf(" realbuff %p alignedbuff %p\n",realbuff, alignedbuff);

    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);
    fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG );
    if( log ) {
        out = open("/tmp/a",O_CREAT | O_RDWR, S_IRWXU);
        if(out == -1) {
            perror("could not open log file");
            return -1;
        }
        if(lseek(out,0,SEEK_END) == -1){
            perror("lseek to end of log file failed ");
        }
    }

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }
    printf("buffer %p \n",blk);
    for(i=0;i<NUM_IOS;i++){
        for(j=0;j<8;j++){
            sprintf(blk+(j*512),"%6d",i);
            if(log) {
                //write(out,outblk,sprintf(outblk,"%c %lu %d.%d\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
            }
        }

        r = get_random_offset(4096,NUM_IOS,MAXSIZE);

        if(lseek(fd,r,SEEK_SET) == -1){
            perror("lseek failed ");
            exit(1);
        }
        if ( mode == 1  ) {
            written = write(fd,blk,4096);
            if(written == -1) {
                perror("write error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes written at %d \n",written,r));
            }
            //sleep(10);
            //fsync(fd);
        }else{
            re = read(fd,blk,4096);
            if(re == -1) {
                perror("read error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes read from %d \n",re,r));
            }
        }
    }

    close(fd);
    if(log){
        close(out);
    }
    return 0;
}

int main_helper(void){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    printf("pid %d tid %ld \n", getpid(), syscall(SYS_gettid));
    sprintf(cmd,"sudo %s/diskdriver/scripts/ioctl test\n",homedir);
    run_cmd(cmd,buffer);
    
    main_helper1(0,0);
    syscall(SYS_io_setup,0,0,0);
	return 0;    
}

void setup(int argc,char** argv){
    setup_guest_disks(1);
}

void runner(int argc,char** argv){
    //pthread_t f1_thread, f2_thread;
    //sprintf(cmd,"sudo %s/cmdclient.o test %lx %lx ",homedir,0,addr);
    //run_cmd(cmd,buffer);

    //main_helper(1);
    //main_helper(0);
    //pthread_create(&f1_thread,NULL,(void * (*)(void *))main_helper,NULL);
    //pthread_create(&f2_thread,NULL,(void * (*)(void *))main_helper,NULL);
    //pthread_join(f1_thread,NULL);
    //pthread_join(f2_thread,NULL);
    
    open("data",O_APPEND);
    char buf[4096];

    printf("%p %s \n", getcwd(buf,4096), buf);
    
    return;
}
