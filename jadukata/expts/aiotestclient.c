#include "testfwk.c"
#include "/home/arulraj/softwares/libaio-oracle-0.3.0/libaio-oracle.h"
#include <libaio.h>
//#include <aio.h>

//#define DELCR3
char* FILE_PATH = "/mnt/disks/blah.txt";
const int NUM_SLOTS = 10;

int setup_helper(int max_num_ios){
    #define BUFFER_SIZE (8192) 
    int fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[BUFFER_SIZE];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFFER_SIZE + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    memset(blk,0,BUFFER_SIZE);
    for(i=0;i<BUFFER_SIZE;i++){
        if(i==(BUFFER_SIZE-1)){
            blk[i] = '\n';
        }else{
            blk[i] = c;
        }
    }

    fd = open(FILE_PATH, O_CREAT | O_RDWR | O_LARGEFILE , S_IRWXU |S_IRWXO | S_IRWXG );

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }

    printf("setup buffer %lx max_num_ios %d RAND_MAX %d MAXSIZE %d \n",(unsigned long)blk, max_num_ios, RAND_MAX, MAXSIZE);

    for(i=0;i<max_num_ios;i++){
        r = (i*BUFFER_SIZE);
        if(lseek(fd,r,SEEK_SET) == -1){
            perror("lseek failed ");
            exit(1);
        }
        written = write(fd,blk,BUFFER_SIZE);
        if(written == -1) {
            perror("write error");
            exit(1);
        }
    }

    fsync(fd);
    close(fd);
    sync();

    drop_caches(); 
    printf("setup done \n");
    return 0;
}

void aio_test1(){
	int numBytes;
    const int SIZE_TO_READ = 500;
	// create the buffer
	char* buffer_orig = malloc(SIZE_TO_READ*10);
	char* buffer = buffer_orig + (SIZE_TO_READ *4);
	
	// create the control block structure
	struct aiocb cb;
	
	int file = open(FILE_PATH, O_RDWR | O_CREAT | O_ASYNC , S_IRWXU | S_IRWXG | S_IRWXO);
	
	if (file == -1)
	{
		printf("Unable to open file!\n");
		return;
	}

	printf("cb %p \n", buffer);
	
	memset(buffer, 'l', SIZE_TO_READ);
	memset(&cb, 0, sizeof(struct aiocb));
	cb.aio_nbytes = SIZE_TO_READ;
	cb.aio_fildes = file;
	cb.aio_offset = 0;
	cb.aio_buf = buffer;

	time(NULL);	
	// write!
	if(aio_write(&cb) == -1)
	{
		printf("Unable to create request!\n");
		close(file);
		return;
	}
	
	printf("Request enqueued!\n");
	
	// wait until the request has finished
	while(aio_error(&cb) == EINPROGRESS)
	{
		printf("Working...\n");
		sleep(3);
	}
	
	// success?
	numBytes = aio_return(&cb);
	
	if (numBytes != -1){
		printf("Success!\n");
    }else{
		printf("Error!\n");
    }
	
	time(NULL);	
	// read!
	if (aio_read(&cb) == -1)
	{
		printf("Unable to create request!\n");
		close(file);
		return;
	}
	
	printf("Request enqueued!\n");
	
	// wait until the request has finished
	while(aio_error(&cb) == EINPROGRESS)
	{
		printf("Working...\n");
		sleep(3);
	}
	
	// success?
	numBytes = aio_return(&cb);
	
	if (numBytes != -1){
		printf("Success!\n");
    }else{
		printf("Error!\n");
    }
	time(NULL);	
		
	close(file);
	free(buffer_orig);
}

void aio_test2(){
    int ret, count,i;
    char *buff;
    char vbuff[BUFFER_SIZE*NUM_SLOTS];
    char tvbuff[BUFFER_SIZE*NUM_SLOTS];
    io_context_t ctxt;
    struct iocb cbs[NUM_SLOTS];
    struct iocb* cbps[NUM_SLOTS];
    struct io_event events[NUM_SLOTS];
    const int NUM_IOS = 8;

    memset(&ctxt,0,sizeof(ctxt));
    memset(&cbs,0,sizeof(struct iocb)*NUM_SLOTS);
    memset(&events,0,sizeof(struct io_event)*NUM_SLOTS);

    for(i=0;i<NUM_SLOTS;i++){
        cbps[i] = &cbs[i];
    }
	
    int fd = open(FILE_PATH, O_RDWR | O_CREAT | O_ASYNC , S_IRWXU | S_IRWXG | S_IRWXO);
	
	if (fd == -1)
	{
		printf("Unable to open file!\n");
        perror("Unable to open file!\n");
		return;
	}
	
    // create the buffer
    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFFER_SIZE*NUM_SLOTS + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    buff = alignedbuff;

    char c = 'A';
	memset(vbuff, 0, BUFFER_SIZE * NUM_SLOTS);
    for(i=0;i<NUM_SLOTS;i++){
	    memset(vbuff+(i*BUFFER_SIZE), c, BUFFER_SIZE);
        vbuff[((i+1)*BUFFER_SIZE)-1] = '\n';
    }

    c = 'l';
    for(i=0;i<NUM_SLOTS;i++){
	    memset(buff+(i*BUFFER_SIZE), c+i, BUFFER_SIZE);
    }
    
    //verify
    memset(tvbuff,0,BUFFER_SIZE*NUM_SLOTS);
    pread(fd,tvbuff,BUFFER_SIZE*NUM_SLOTS,0);
    for(i=0;i<BUFFER_SIZE*NUM_SLOTS;i++){
        if(tvbuff[i] != vbuff[i]){
            printf("1 error exp %c found %c index %d slot %d \n", vbuff[i], tvbuff[i], i, i%BUFFER_SIZE);
        }
    }

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
	
    ret = io_setup(NUM_SLOTS,&ctxt);
    if(ret != 0){
        printf("ERROR io_setup ret %d \n", ret);
    }

    short code[] = {1,0,1,0,1,1,0,1};
    int sizes[] = {4096,500,1300,1024,4096,3500,512,1024};

    //setup cbs
    for(i=0;i<NUM_IOS;i++){
        struct iocb* cbp = &cbs[i];
        if(code[i]){
            io_prep_pwrite(&cbs[i],fd,buff+(BUFFER_SIZE*i),sizes[i],BUFFER_SIZE*i);
        }else{
            io_prep_pread(&cbs[i],fd,buff+(BUFFER_SIZE*i),sizes[i],BUFFER_SIZE*i);
        }
        printf("iocb submitted %s fd %d buf %p nbytes %lu offset %lld \n", code[i]?"write":"read", cbp->aio_fildes, cbp->u.c.buf, cbp->u.c.nbytes, cbp->u.c.offset);
    }
    
    ret = io_submit(ctxt,NUM_IOS,cbps);
    if(ret != NUM_IOS){
        printf("WARN io_submit ret %d \n", ret);
    }

    //wait for completion
    count = 0;
    while(count !=NUM_IOS){
        ret = io_getevents(ctxt, 1, NUM_IOS, events,NULL);
        if(ret>=0){
            int i;
            for(i=0;i<ret;i++){
                struct iocb* cbp = events[i].obj;
                if(events[i].res != cbp->u.c.nbytes){
                    printf("ERROR aio not complete res %lu %lu \n", events[i].res, cbp->u.c.nbytes);
                }else{
                    printf("iocb completed %s fd %d buf %p nbytes %lu offset %lld \n", cbp->aio_lio_opcode ==IO_CMD_PREAD?"read":"write", cbp->aio_fildes, cbp->u.c.buf, cbp->u.c.nbytes, cbp->u.c.offset);
                }
            }
            count += ret;
        }else{
            printf("ERROR iogetevents %d \n", ret);
        }
        printf("waiting \n");
        sleep(3);
    }
    
    for(i=0;i<NUM_IOS;i++){
        memcpy(vbuff+(BUFFER_SIZE*i),buff+(BUFFER_SIZE*i),sizes[i]);
    }

    //verify
    memset(tvbuff,0,BUFFER_SIZE*NUM_SLOTS);
    pread(fd,tvbuff,BUFFER_SIZE*NUM_SLOTS,0);
    for(i=0;i<BUFFER_SIZE*NUM_SLOTS;i++){
        if(tvbuff[i] != vbuff[i]){
            printf("1 error exp %c found %c index %d slot %d \n", vbuff[i], tvbuff[i], i, i%BUFFER_SIZE);
        }
    }

    printf("done\n");
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
    setup_helper(NUM_SLOTS); 
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
#ifdef DELCR3
    sprintf(cmd,"sudo %s/cmdclient.o delcr3 %lx ",homedir,cr3);
    run_cmd(cmd,buffer);
#endif
    aio_test2();
}
