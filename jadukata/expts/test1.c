#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

#include "testfwklib.c"

int main(){
    unsigned long cr3;
    char* homedir = "/home/arulraj/cerny/jadukata";
#define CMD_LEN (40960)
#define MAX_PAYLOAD (600*4096) /* maximum payload size*/
	char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    pid_t pid = getpid();

    sprintf(cmd,"sudo %s/diskdriver/scripts/ioctl getcr3 %d ",homedir,pid);
    run_cmd(cmd,buffer);
    sscanf(buffer, "%lx", &cr3);
        
    sprintf(cmd,"sudo %s/cmdclient.o addcr3 %lx %d\n",homedir,cr3,pid);
    run_cmd(cmd,buffer);

    syscall(SYS_getcwd,buffer,4096);

    return 0;
    printf("get pid test1 : %d \n", getpid());
    int ret = syscall(__NR_clone, CLONE_NEWNET | SIGCHLD, NULL);

    printf("clone ret %d %d\n", ret,errno);

    //sleep(10);
    char buf[4096];
    char buf1[4096];
    unsigned long addr = (unsigned long)getcwd(buf,4096);
    unsigned long addr1 = (unsigned long)syscall(SYS_getcwd,buf1,4096);

    printf("%lx %s %s \n",addr, (char*)addr , buf);
    printf("%lx %s %s \n",addr1, (char*)addr1 , buf1);
    //execve("./test2.o", NULL,NULL);
    return 0;
    //printf("%d\n",CLONE_VM);
}
