#define _GNU_SOURCE


#include "khash.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>

#include <sys/mman.h> 
#include <sched.h> 
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "khash.h"
#include <sys/syscall.h>

int main_helper1(int mode, int log){
#define NUM_IOS (5000)
    int fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[4096];

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);

    blk = alignedbuff;

    printf(" realbuff %p alignedbuff %p\n",realbuff, alignedbuff);

    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);
    fd = open("/mnt/jadu/test1", O_CREAT | O_RDWR , S_IRWXU |S_IRWXO | S_IRWXG );

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }
    printf("buffer %p \n",blk);
    for(i=0;i<NUM_IOS;i++){
        for(j=0;j<8;j++){
            sprintf(blk+(j*512),"%6d",i);
            if(log) {
                //write(out,outblk,sprintf(outblk,"%c %lu %d.%d\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
            }
        }

        r = get_random_offset(4096,NUM_IOS,MAXSIZE);

        if(lseek(fd,r,SEEK_SET) == -1){
            perror("lseek failed ");
            exit(1);
        }
        if ( mode == 1  ) {
            written = write(fd,blk,4096);
            if(written == -1) {
                perror("write error");
                exit(1);
            }
        }else{
            re = read(fd,blk,4096);
            if(re == -1) {
                perror("read error");
                exit(1);
            }
        }
    }

    close(fd);
    return 0;
}

int main_helper(void){
    main_helper1(1,0);
    main_helper1(0,0);
	return 0;    
}

void main(int argc,char** argv){
    sleep(5);
    main_helper();
    return;
}
