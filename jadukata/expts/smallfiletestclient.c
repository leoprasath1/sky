
#include "testfwk.c"

long long NUM_FILES;
long long NUM_IOS;
#define BUFSIZE (4096)
#define RAND_TEXT_SIZE (409600)
char randtext[RAND_TEXT_SIZE];

int main_helper(int mode, int log){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    int fd,written,re,i,j,k,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[4096];
    char fname[4096];

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFSIZE + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    for(i=0;i<RAND_TEXT_SIZE;i++) randtext[i] = myrand(26) + 'a';

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace %d ",homedir, 4194304 + 32); //4194304  + 48);
    //run_cmd(cmd,buffer);
    
    memset(blk,0,BUFSIZE);
    for(i=0;i<BUFSIZE;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);
    if( log ) {
        out = open("/tmp/a",O_CREAT | O_RDWR, S_IRWXU);
        if(out == -1) {
            perror("could not open log file");
            return -1;
        }
        if(lseek(out,0,SEEK_END) == -1){
            perror("lseek to end of log file failed ");
        }
    }

    float time_taken = 0;
    NUM_FILES = 1024;
    NUM_IOS = 125;
    //NUM_FILES = 2;
    //NUM_IOS = 20;
    printf("buffer %lx \n",(unsigned long)blk);
    for(i=0;i<NUM_FILES;i++){
        //usleep(2000);
        sprintf(fname,"/mnt/disks/test%d",i);
        fd = open(fname, O_CREAT | O_RDWR | O_LARGEFILE , S_IRWXU |S_IRWXO | S_IRWXG );
        if(fd == -1) {
            perror("could not open file");
            return -1;
        }
        TIMER_START;
        for(k=0;k<NUM_IOS;k++){
            for(j=0;j<(BUFSIZE/512);j++){
                sprintf(blk+(j*512),"%6d%6d%d%d",i,k,myrand(1000000),myrand(1000000));
                sprintf(blk+((j*512)+100),"%300.300s",randtext+myrand((RAND_TEXT_SIZE-300)));
                if(log) {
                    write(out,outblk,sprintf(outblk,"%c %lx %d.%d\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
                }
            }

            written = write(fd,blk,BUFSIZE);
            if(written == -1) {
                perror("write error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes written at %d \n",written,r));
            }
        }
        TIMER_END_NO_PRINT
        time_taken += total_time;
        close(fd);
    }

    sprintf(cmd,"sync ");
    run_cmd(cmd,buffer);

    if(log){
        close(out);
    }
    printf("done time taken %f\n",time_taken);
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    //sscanf(argv[1],"%lld", &NUM_FILES);
    
    //sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o trace 8",homedir);
    //run_cmd(cmd,buffer);
    
    main_helper(1,0);
}
