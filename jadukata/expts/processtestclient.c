#include "testfwk.c"

int childFunc(void* arg){
    printf("child pid from child %d \n" , getpid());
    exit(0);
}

int main_helper1(void){
    int pid = fork();
    
    if(pid != 0){
        printf("child pid fork %d \n" , pid);
        if(pid == -1){
            printf("child creation fork failed -1 \n");
        }
    }

    if(pid == 0){
        childFunc(NULL);
    }

    printf("mainhelper1 done \n");
    return 0;
}

int main_helper2(void){
    char *stack, *stackTop;
    int STACK_SIZE = 128*1024;
    stack = malloc(STACK_SIZE);
    if(stack == NULL){
        perror("malloc");
        return 0;
    }
    stackTop = stack + STACK_SIZE;  /* Assume stack grows downward */

    int pid = clone(childFunc, stackTop, CLONE_FILES, 0);

    if(pid != -1){
        printf("child pid clone from parent %d \n" , pid);
        int status;
        wait(&status);
    }else{
        perror("clone failed \n");
    }
    printf("mainhelper2 done \n");
    return 0;
}

void main_helper3(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/expts/process_test1.sh",homedir);
    run_cmd_sys(cmd);
    sprintf(cmd,"%s/expts/process_test2.sh",homedir);
    run_cmd_sys(cmd);
}


void setup(int argc, char** argv){
    setup_guest_disks(0);
}

void runner(int argc, char** argv){
    //main_helper1();
    //main_helper2();
    main_helper3();
}
