
#include "testfwk.c"

long long NUM_IOS = 0;

int main_helper(int mode, int log){
    int fd,written,re,i,j,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[4096];

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);

    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);
    fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG );
    close(fd);
    fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG );
    if( log ) {
        out = open("/tmp/a",O_CREAT | O_RDWR, S_IRWXU);
        if(out == -1) {
            perror("could not open log file");
            return -1;
        }
        if(lseek(out,0,SEEK_END) == -1){
            perror("lseek to end of log file failed ");
        }
    }

    if(fd == -1) {
        perror("could not open file");
        return -1;
    }
    printf("buffer %lx \n",(unsigned long)blk);
    for(i=0;i<NUM_IOS;i++){
        //usleep(2000);
        for(j=0;j<8;j++){
            sprintf(blk+(j*512),"%6d",i);
            if(log) {
                write(out,outblk,sprintf(outblk,"%c %lx %d.%d\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
            }
        }

        r = get_random_offset(4096,NUM_IOS,MAXSIZE);

        if(lseek(fd,r,SEEK_SET) == -1){
            perror("lseek failed ");
            exit(1);
        }
        if ( mode == 1  ) {
            written = write(fd,blk,4096);
            if(written == -1) {
                perror("write error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes written at %d \n",written,r));
            }
            //sleep(10);
            //fsync(fd);
        }else{
            re = read(fd,blk,4096);
            if(re == -1) {
                perror("read error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes read from %d \n",re,r));
            }
        }
    }

    close(fd);
    if(log){
        close(out);
    }
    printf("done \n");
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    if(argc != 2){
        printf("error: pass numios arg\n");
        return;
    }
    sscanf(argv[1],"%lld", &NUM_IOS);
    
    //sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o trace 8",homedir);
    //run_cmd(cmd,buffer);
    
    main_helper(1,0);
    //drop_caches();
    //main_helper(0,0);
}
