#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

int filebenchwkld(char* mode){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    //sprintf(cmd,"sudo %s/cmdclient.o trace 3 ",homedir);
    //run_cmd(cmd,buffer);
    
    //sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 0 ",homedir);
    //run_cmd(cmd,buffer);

    sprintf(cmd,"sudo /home/leoa/software/filebench/filebench -f /home/leoa/fb/%s.f 1>~/fbout%ld.txt",mode,time(NULL));
    run_cmd_sys(cmd);
    
    sprintf(cmd,"sync ");
    run_cmd_sys(cmd);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);

    drop_caches();
    
    //sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    //run_cmd(cmd,buffer);

}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%s ",mode);


    //sprintf(cmd,"sudo bash -c 'echo 0 > /proc/sys/kernel/randomize_va_space'");
    //run_cmd_sys(cmd);
    
    for(j=0;j<NUM_ITERS;j++){

        sprintf(cmd,"sudo /bin/rm -rf /mnt/disks/*");
        run_cmd_sys(cmd);
    
        drop_caches();
        
        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);

        sprintf(tmp,"iterations %s.%d \n",mode,j);

        TIMER_START;
        filebenchwkld(mode);
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n", j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
        run_cmd(cmd,buffer);
    }

    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    return 0;
}

void setup(int argc, char** argv){
    int i;
    char cmd[CMD_LEN];
    setup_guest_disks(1);
    //setup_guest_disks(0);
#ifdef BSD
    sprintf(cmd,"sudo find ~/ -maxdepth 1 -name \"fbou*txt\" -mmin +120 | xargs -J{} mv {} ~/fboutold/ ");
#else
    sprintf(cmd,"sudo find ~/ -maxdepth 1 -name \"fbou*txt\" -mmin +120 | xargs -i mv {} ~/fboutold/ ");
#endif
    run_cmd_sys(cmd);

    /*
    for(i=0;i<11000;i++){
        if(i%25 == 0){
            sprintf(cmd,"cp /home/sura/1Mfile /mnt/disks/s%d ",i+1);
        }else{
            sprintf(cmd,"cp /home/sura/1Mfile /mnt/disks/a%d ",i+1);
        }
        run_cmd_sys(cmd);
        if(i%1000 == 0){
            run_cmd_sys("sync");
            printf("old FS mimic files %d \n", i);
            run_cmd_sys("df -lh /mnt/disks");
        }
    }

    sprintf(cmd,"/bin/rm -f /mnt/disks/s* ");
    run_cmd_o(cmd);

    sprintf(cmd,"df -lh /mnt/disks ");
    run_cmd_o(cmd);

    drop_caches();
    */
}

void runner(int argc, char** argv){
        /*
        char cmd[CMD_LEN];
        char buffer[MAX_PAYLOAD];
        sprintf(cmd,"sudo mkfs.ext3 -F /mnt/disks/../../dev/sdb ");
        run_cmd(cmd,buffer);
        return;
        */
    main_helper(argc,argv);
}
