#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

void facetest_stop(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/facetest/facetest_stop.sh",homedir);
    run_cmd_sys(cmd);
}

void facetest_trace_echo(char* str){
}

void facetest_start(){

~/cerny/jadukata/face/Neurotec_Biometric_5_0_SDK_Trial/Tutorials/Biometrics/C/VerifyFace$ ls .obj/release/Linux_x86_64/VerifyFace/VerifyFace^C


~/cerny/jadukata/face/Neurotec_Biometric_5_0_SDK_Trial/Bin/Linux_x86/Activation/run_pgd.sh
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/facetest/facetest_start.sh",homedir);
    run_cmd_sys(cmd);
}
        
void facetest_setup(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    facetest_trace_echo("starting setup");
    facetest_stop();

    sprintf(cmd,"%s/facetest/facetest_setup.sh %d ",homedir, smartmode);
    run_cmd_sys(cmd);

    facetest_start();

    //set_readahead(0);
    
    /*    
    drop_caches();
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    */
}

void facetest_teardown(){
    facetest_trace_echo("starting teardown");
    facetest_stop();
}

int facetestwkld(char *mode){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/facetest/facetest_wkld.sh",homedir);
    run_cmd_sys(cmd);
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    /*
    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%s ",mode);
    */

    facetest_setup();

    for(j=0;j<NUM_ITERS;j++){

        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);

        sprintf(tmp,"iterations %s.%d \n",mode,j);
        
        TIMER_START;
        facetestwkld(mode);
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n",j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
        run_cmd(cmd,buffer);
    }
        
    facetest_teardown();

    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
    //setup_guest_disks(0);
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
