#include <stdio.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>

#include <sys/mman.h> 
#include <sched.h> 
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define TIMER_START  struct timeval start,end; float total_time; gettimeofday(&start,NULL);

#define TIMER_END(name) gettimeofday(&end,NULL); total_time = (end.tv_sec - start.tv_sec) + (((float)(end.tv_usec - start.tv_usec))/1000000); printf("%s took %.2f secs = %.2f msecs \n",name,(ceilf(total_time*100.0)/100.0), (ceilf(total_time*100000.0)/100.0));


static inline unsigned add32_with_carry(unsigned a, unsigned b)
{
    asm("addl %2,%0\n\t"
            "adcl $0,%0"
            : "=r" (a)
            : "0" (a), "r" (b));
    return a;
}

static inline unsigned short from32to16(unsigned a) 
{
    unsigned short b = a >> 16; 
    asm("addw %w2,%w0\n\t"
            "adcw $0,%w0\n" 
            : "=r" (b)
            : "0" (b), "r" (a));
    return b;
}

static unsigned do_csum(const unsigned char *buff, unsigned len)
{
    unsigned odd, count;
    unsigned long result = 0;

    if (len == 0)
        return result; 
    odd = 1 & (unsigned long) buff;
    if (odd) {
        result = *buff << 8;
        len--;
        buff++;
    }
    count = len >> 1;        /* nr of 16-bit words.. */
    if (count) {
        if (2 & (unsigned long) buff) {
            result += *(unsigned short *)buff;
            count--;
            len -= 2;
            buff += 2;
        }
        count >>= 1;        /* nr of 32-bit words.. */
        if (count) {
            unsigned long zero;
            unsigned count64;
            if (4 & (unsigned long) buff) {
                result += *(unsigned int *) buff;
                count--;
                len -= 4;
                buff += 4;
            }
            count >>= 1;    /* nr of 64-bit words.. */

            /* main loop using 64byte blocks */
            zero = 0;
            count64 = count >> 3;
            while (count64) { 
                asm("addq 0*8(%[src]),%[res]\n\t"
                        "adcq 1*8(%[src]),%[res]\n\t"
                        "adcq 2*8(%[src]),%[res]\n\t"
                        "adcq 3*8(%[src]),%[res]\n\t"
                        "adcq 4*8(%[src]),%[res]\n\t"
                        "adcq 5*8(%[src]),%[res]\n\t"
                        "adcq 6*8(%[src]),%[res]\n\t"
                        "adcq 7*8(%[src]),%[res]\n\t"
                        "adcq %[zero],%[res]"
                        : [res] "=r" (result)
                        : [src] "r" (buff), [zero] "r" (zero),
                        "[res]" (result));
                buff += 64;
                count64--;
            }

            /* last upto 7 8byte blocks */
            count %= 8; 
            while (count) { 
                asm("addq %1,%0\n\t"
                        "adcq %2,%0\n" 
                        : "=r" (result)
                        : "m" (*(unsigned long *)buff), 
                        "r" (zero),  "0" (result));
                --count; 
                buff += 8;
            }
            result = add32_with_carry(result>>32,
                    result&0xffffffff); 

            if (len & 4) {
                result += *(unsigned int *) buff;
                buff += 4;
            }
        }
        if (len & 2) {
            result += *(unsigned short *) buff;
            buff += 2;
        }
    }
    if (len & 1)
        result += *buff;
    result = add32_with_carry(result>>32, result & 0xffffffff); 
    if (odd) { 
        result = from32to16(result);
        result = ((result >> 8) & 0xff) | ((result & 0xff) << 8);
    }
    return result;
}

#include "../kvm-kmod-3.10.1/x86/checksums.c"

unsigned long csum_partial_old(const void *buff, int len, unsigned long sum)
{
    return (unsigned long)add32_with_carry(do_csum(buff, len),(unsigned)sum);
}

unsigned long csum_partial(const void *buff, int len, unsigned long sum)
{
    //return murmur_hash3(buff, len, sum);
    //return murmur_hash2(buff, len,sum);
    //return crc32(buff, len, sum);
    //return FNVHash(buff, len, sum);
    return BKDRHash(buff, len, sum);
    //return csum;
}

struct pair {
    unsigned long addr;
    unsigned int size;
};

void checksum_processor1(struct pair* pairs, int numpairs)
{
    char codeddata[1800];
    int i,j;
    char* source;
    int bytes;
    char* dst=codeddata;
    int count=0;
    int cur = ~0;

    for(j=0;j<numpairs;j++){
        source = (char*)pairs[j].addr;
        bytes = pairs[j].size;
        
        if( cur != ~0 ){
            if( ((char)cur == source[0]) ){
                count++;
            }else{
                dst += sprintf(dst,"%dx%u,",count+1,(char)cur);
                count= 0;
            }
        }
        
        for(i=0;i<bytes-1;i++){
            if(source[i] == source[i+1]){
                count++;
            }else {
                dst += sprintf(dst,"%dx%u,",count+1,source[i]);
                count = 0;
            }
            if( (dst-codeddata)>1700 ) {
                count = 0;
                dst += sprintf(dst,"...\n");
                break;
            }
        }
        cur = source[bytes-1];
    }
    
    dst += sprintf(dst,"%dx%u,",count+1,(char)cur);

    bytes= (dst-codeddata);
    dst = codeddata;
    for(i=0;i<=(bytes/100);i++){
        printf("nitro coded %d %*s\n",i,(i==(bytes/100))?bytes-(i*100):100,dst);
        dst += 100;
    }
}

int process_checksum(void* pargs, int chunknum, unsigned long checksum){
    printf("process pargs %p chunknum %d checksum %lx \n", pargs, chunknum, checksum);
}

unsigned long custom_checksum(char* ptr, int size,unsigned long csumpartial){
    unsigned long csum = csum_partial(ptr,size,csumpartial);
    //printf("custom ptr %lx size %d csumpartial inp %lx  out %lx \n", ptr, size, csumpartial, csum);
    return csum;
}

unsigned long custom_checksum_old(char* ptr, int size,unsigned long csumpartial){
    unsigned long csum = csum_partial_old(ptr,size,csumpartial);
    return csum;
}

int checksum_processor2(void* pargs, struct pair* pairs, int numpairs)
{
    int j; 
    int bytes;
    int remaining = 0;
    unsigned long checksum = 0;
    int chunknum = 0;
    char* source;
    int ret = 0;

    for(j=0;j<numpairs;j++){
        source = (char*)pairs[j].addr;
        bytes = pairs[j].size;
        printf("source %p bytes %d remaining %d %d \n",source ,bytes, remaining, ((bytes+remaining)>=0 ));
        if(remaining < 0){
            if( (bytes + remaining) >=0 ){
                checksum = custom_checksum((char*)(source),-remaining,checksum);
                source += (-remaining);
                ret += process_checksum(pargs, chunknum++,checksum);
                checksum = 0;
            }else{
                checksum = custom_checksum((char*)(source),bytes,checksum);
                source += bytes;
            }
        }
        remaining = bytes+remaining;
        while(remaining>=512){
            checksum = custom_checksum((char*)(source),512,0);
            source += 512;
            ret += process_checksum(pargs, chunknum++,checksum);
            checksum = 0;
            remaining -= 512;
        }
        if(remaining > 0){
            checksum = custom_checksum((char*)(source),remaining,0);
            source += remaining;
            remaining -= 512;
        }
    }
    return ret;
}

void speedtest(int NUM){
    int i;
    char* b3 = (char*)malloc(4096);
    for(i=0;i<4096;i++) b3[i] = i%20+65;
    {
        TIMER_START
            for(i=0;i<NUM;i++){
                custom_checksum_old(b3,4096,0);
            }
        TIMER_END("old")
    }

    {
        TIMER_START
            for(i=0;i<NUM;i++){
                custom_checksum(b3,4096,0);
            }
        TIMER_END("new")
    }
}

int main(){
    int i;
    void* a = NULL;
    char* b1 = (char*)malloc(400);
    char* b2 = (char*)malloc(112);
    char* b3 = (char*)malloc(4096);

    struct pair pairs[3];
    pairs[0].addr = (unsigned long)b1;
    pairs[1].addr = (unsigned long)b2;
    pairs[2].addr = (unsigned long)b3;

    pairs[0].size = 400;
    pairs[1].size = 112;
    pairs[2].size = 4096;

    for(i=0;i<400;i++) b1[i] = 65;
    for(i=0;i<112;i++) b2[i] = 0;

    for(i=0;i<4096;i++) b3[i] = 0;
    for(i=0;i<400;i++) b3[i] = 65;

    printf("%lx %lx %lx %lx \n", custom_checksum(b1,400,0),custom_checksum(b2,112,0),custom_checksum(b2,112,custom_checksum(b1,400,0)),custom_checksum(b3,512,0));

    checksum_processor1(pairs, 2);
    checksum_processor2(a, pairs, 2);
    
    //checksum_processor1(pairs+2, 1);
    //checksum_processor2(a, pairs+2, 1);
    speedtest(1000000);
    return 0;
}
