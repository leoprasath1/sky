#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

void simpleftpd_stop(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/simpleftpd/simpleftpd_stop.sh",homedir);
    run_cmd_sys(cmd);
}

void simpleftpd_trace_echo(char* str){
}

void simpleftpd_start(){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/simpleftpd/simpleftpd_start.sh",homedir);
    run_cmd_sys(cmd);
}
        
void simpleftpd_setup(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    simpleftpd_trace_echo("starting setup");
    simpleftpd_stop();

    sprintf(cmd,"%s/simpleftpd/simpleftpd_setup.sh %d ",homedir, smartmode);
    run_cmd_sys(cmd);

    simpleftpd_start();

    //set_readahead(0);
    
    /*    
    drop_caches();
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    */
}

void simpleftpd_teardown(){
    simpleftpd_trace_echo("starting teardown");
    simpleftpd_stop();
}

int simpleftpdwkld(char *mode){
    char cmd[CMD_LEN];
    sprintf(cmd,"%s/simpleftpd/simpleftpd_wkld.sh",homedir);
    run_cmd_sys(cmd);
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    char mode[1024];
    int j;

    /*
    if(argc !=2){
        printf("no arguments given \n");
        return 1;
    }
    sscanf(argv[1],"%s ",mode);
    */

    simpleftpd_setup();

    for(j=0;j<NUM_ITERS;j++){

        sprintf(cmd,"sudo %s/cmdclient.o clear",homedir);
        run_cmd(cmd,buffer);

        sprintf(tmp,"iterations %s.%d \n",mode,j);
        
        TIMER_START;
        simpleftpdwkld(mode);
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n",j,times[j]);
        
        sprintf(cmd,"sudo %s/cmdclient.o status",homedir);
        run_cmd(cmd,buffer);
    }
        
    simpleftpd_teardown();

    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
    //setup_guest_disks(0);
}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
