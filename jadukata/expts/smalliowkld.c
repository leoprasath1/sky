
#include "testfwk.c"

int main_helper(){
    int fd,written,re,i,j,r,out = 1,ret;
    char *blk, *blk1, c = 'A';
    char outblk[4096];

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;
    
    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    realbuff = (char*)malloc(10000 + pagesize);
    alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk1 = alignedbuff;

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);

    memset(blk1,0,10000);
    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        blk[i] = c;
    }

    srand(12345);
    fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG);
    //fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG);

    if(fd == -1){
        perror("could not open file");
        return -1;
    }

    printf("buffer %lx \n",(unsigned long)blk);

    for(i=0;i<20;i++){
        written = write(fd,blk,400);
        if(written != 400) {
            printf("ERROR ret %d \n", written);
            perror("write ERROR");
            exit(1);
        }
    }
    
    for(i=0;i<20;i++){
        if(lseek(fd, i*400, SEEK_SET) != (i*400)){
            perror("lseek ERROR");
            exit(1);
        }
        written = write(fd,"B",1);
        if(written != 1) {
            printf("ERROR ret %d \n", written);
            perror("write ERROR");
            exit(1);
        }
    }
    
    fsync(fd);
    close(fd);
    drop_caches(); 
    
    fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG);
    //fd = open("/mnt/disks/test", O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE | O_SYNC , S_IRWXU |S_IRWXO | S_IRWXG);

    if(fd == -1){
        perror("could not open file");
        return -1;
    }

    if(lseek(fd, 0, SEEK_SET) != 0){
        perror("lseek ERROR");
        exit(1);
    }

    //printf("before %8000s \n", blk1);
    for(i=0;i<20;i++){
        re = read(fd,blk1+(i*400),400);
        if(re != 400) {
            printf("ERROR ret %d \n", re);
            perror("read ERROR");
            exit(1);
        }
    }

    //printf("after %8000s \n", blk1);
    for(i=0;i<8000;i++){
        if((i%400) == 0){
            if(blk1[i] != 'B'){
                printf("ERROR i %d char %c \n",i, blk1[i]);
            }
        }else{
            if(blk1[i] != 'A'){
                printf("ERROR i %d char %c \n",i, blk1[i]);
            }
        }
    }

    printf("done \n");
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
        
    main_helper();
}
