#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  
#include "stdio.h"

int mysyscall(){
    long ret = 0, scallnr=222123213212321,error=0;
    //syscall version  %rdi, %rsi, %rdx, %r10, %r8, %r9 
    //unsigned long scallnractual = 240;
    asm volatile (
            "movq %1, %%rax\n"  /* syscall number */
            "syscall\n"
            "jge 1f\n"
            "movq $-1,%%rdi\n"  /* first argument */
            "imul %%rdi\n"  /* first argument */
            "1:\n"  /* first argument */
            "movq %%rax,%0\n"  /* first argument */
            : "=m"(ret)
            : "m"(scallnr)
            : "rax", "rdi" , "rdx"
            ); 

    printf("result is %ld \n", ret);
}

int main(int argc, char**argv){
    mysyscall();
    return 0;
}
