#include "testfwk.c"

long long NUM_FILES;
long long NUM_IOS;
#define BUFSIZE (4096)
#define RAND_TEXT_SIZE (409600)
char randtext[RAND_TEXT_SIZE];
int id = 1;

//syscall version  %rdi, %rsi, %rdx, %r10, %r8, %r9 
unsigned long mysyscall(unsigned long scallnr, unsigned long fd, unsigned long p_buf, unsigned long size, unsigned long offset, unsigned long ioclass){
    unsigned long retval = 0;
    unsigned long error = 0;

    asm volatile(
            "movq %2, %%rax\n"
            "movq %3, %%rdi\n"
            "movq %4, %%rsi\n"
            "movq %5, %%rdx\n"
            "movq %6, %%r10\n"
            "movq %7, %%r8\n"
            "movq %8, %%r9\n"
            "syscall\n"
            "jge 1f\n"
            "movq $1, %1\n"
            "1:\n"
            "movq %%rax, %0\n"
            "movq $0, %%r9\n"
            : "=m"(retval), "=m"(error) 
            : "m"(scallnr), "m"(fd), "m"(p_buf), "m"(size), "m"(offset), "i"(GUEST_APP_AIO_MAGIC), "m"(ioclass)
            : "rax", "rdi", "rsi", "rdx", "r10", "r8", "r9"
            );

    if(error) {
        errno = retval;
        printf( "leo: error %lu %lu bytes should have"
                " been io. %lu bytes io done",
                error, size,retval);
        retval = -1;
    }

    return retval;
}


int main_helper(int mode, int log){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    int fd,written,re,i,j,k,r,out = 1,ret;
    char *blk, c = 'A';
    char outblk[4096];
    char fname[4096];

    khiter_t dupoffsetiter;
    if(dupoffset!=NULL){
        kh_destroy(32,dupoffset);
        dupoffset = NULL;
    }
    dupoffset = kh_init(32);

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(BUFSIZE + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    if(id == 1){
        for(i=0;i<RAND_TEXT_SIZE;i++) randtext[i] = myrand(26) + 'a';
    }

    printf(" realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace %d ",homedir, 4194304 + 32); //4194304  + 48);
    //run_cmd(cmd,buffer);
    
    memset(blk,0,BUFSIZE);
    for(i=0;i<BUFSIZE;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    if(id == 1){
        srand(12345);
    }
    if( log ) {
        out = open("/tmp/a",O_CREAT | O_RDWR, S_IRWXU);
        if(out == -1) {
            perror("could not open log file");
            return -1;
        }
        if(lseek(out,0,SEEK_END) == -1){
            perror("lseek to end of log file failed ");
        }
    }

    id++;
    float time_taken = 0;
    NUM_FILES = 1;
    NUM_IOS = 100000;
    printf("buffer %lx \n",(unsigned long)blk);
    for(i=0;i<NUM_FILES;i++){
        //usleep(2000);
        sprintf(fname,"/mnt/disks/test_%d_%d",id,i);
        fd = open(fname, O_CREAT | O_RDWR | O_LARGEFILE , S_IRWXU |S_IRWXO | S_IRWXG );
        if(fd == -1) {
            perror("could not open file");
            return -1;
        }
        TIMER_START;
        for(k=0;k<NUM_IOS;k++){
            for(j=0;j<(BUFSIZE/512);j++){
                sprintf(blk+(j*512),"%6d%6d",i,k);
                sprintf(blk+(j*512)+100,"%6d%6d",i,k);
                sprintf(blk+(j*512)+220,"%6d%6d",i,k);
                sprintf(blk+(j*512)+380,"%6d%6d",i,k);
                if(log) {
                    write(out,outblk,sprintf(outblk,"%c %lx %d.%d\n",mode?'w':'r',csum_partial(blk+j*512,512,0),i,j));
                }
            }

            //pread = 475, pwrite = 476
            //written = pwrite(fd,blk,BUFSIZE,(k*BUFSIZE));
			written = mysyscall(476, fd, (unsigned long)blk, BUFSIZE, (k*BUFSIZE),1);
            if(written == -1) {
                perror("write error");
                exit(1);
            }
            if(log){
                write(out,outblk,sprintf(outblk,"%d bytes written at %d \n",written,r));
            }
        }
        TIMER_END_NO_PRINT
        time_taken += total_time;
        close(fd);
    }

    sprintf(cmd,"sync ");
    run_cmd(cmd,buffer);

    if(log){
        close(out);
    }
    printf("done time taken %f\n",time_taken);
    return 0;
}

void setup(int argc, char** argv){
    char buffer[MAX_PAYLOAD];
    char cmd[CMD_LEN];
    char tmp[4096];
    sprintf(tmp,"dedupuniqueapp");

    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    char* homedir = "/home/arulraj/cerny/jadukata";
    //char * space = (char*)malloc(4096*10);
    //unsigned long* addr = (unsigned long*)(space+(4096*2));
    //*addr = 0x1234;
    //pid_t pid = getpid();

    char buffer[MAX_PAYLOAD];
    char cmd[CMD_LEN];
    char tmp[4096];
    int hints;
    sprintf(tmp,"dedupuniqueapp");
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace %ld ",homedir,ipow(2,22) + ipow(2,5)  + ipow(2,12));
    //run_cmd(cmd,buffer);
    
    if(argc != 2){
        printf("error: pass hints option\n");
        return;
    }
    sscanf(argv[1],"%d", &hints);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o unique_hints %d",homedir,hints);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o unique_stats_clear ",homedir);
    run_cmd(cmd,buffer);
    
    TIMER_START;

    main_helper(1,0);
    
    TIMER_END(tmp);
        
    printf("time taken by dedupuniqueapp wkld total %0.2f secs \n",total_time);
    
    sprintf(cmd,"/usr/local/bin/sudo %s/cmdclient.o unique_hints 0",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
}
