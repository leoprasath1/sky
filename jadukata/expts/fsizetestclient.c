#include "testfwk.c"

//#define SYNC

#define num_files (1500)
#define big_files (8)
int fr = ((num_files-big_files)/big_files);
int big_file_size = (4096*1024*3); // 12 MB in bytes 
//int big_file_size = (1024*3); // 100 MB in bytes 
int small_file_size = (8192*10); // 80KB in bytes
//int read_bytes = 4096*10;
int read_bytes = 4096 * 1024 * 100; // 400MB

int main_helper1(void){
    int fd,written,i,j,k;
    char *blk, c = 'A';
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    int totalwrites = 0;
    int flags;

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;

    printf("start writephase\n");
    printf("realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);

    memset(blk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            blk[i] = '\n';
            c++;
        }else{
            blk[i] = c;
        }
    }

    srand(12345);

    if(mkdir("/mnt/disks/fsize/", 0777) == -1){
        perror("mkdir failed");
    }

    // create files 
    char fname[100];
    for(i=0;i<num_files;i++){
        int file_size = small_file_size;
        char* prefix = "smf";
        if((i % fr) == 0){
            file_size = big_file_size;
            prefix = "bgf";
        }
        sprintf(fname,"/mnt/disks/fsize/test%s%d",prefix,i);
        //printf("creating file %s size %d \n", fname, file_size);
        flags = O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE ;
#ifdef SYNC
        flags |= O_SYNC ;
#endif
        fd = open(fname, flags, S_IRWXU |S_IRWXO | S_IRWXG );
        if(fd == -1) {
            perror("could not open file");
            return -1;
        }
        if(lseek(fd,file_size,SEEK_SET) == -1){
            perror("lseek failed\n");
        }
        if(lseek(fd,0,SEEK_SET) == -1){
            perror("lseek failed\n");
        }
        for(k=0;k<(file_size/4096);k++){
            for(j=0;j<8;j++){
                sprintf(blk+(j*512),"%6d%6d",i,k);
            }
            written = write(fd,blk,4096);
            if(written == -1) {
                perror("write error");
            }
            totalwrites += 4096;
            if(((totalwrites/4096)%500)==0){
                sleep(1);
            }
        }
        fsync(fd);
        close(fd);
    }
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
    
    //printf("clear_stats %s\n",buffer);

    printf("end writephase: totalwritten bytes %d sectors %d \n",totalwrites,totalwrites/512);
    return 0;
}

int main_helper2(void){
    int fd,acc,offset;
    int i,j,k,y;
    char *blk, c = 'A';
    char *outblk;
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    int pagesize = getpagesize();

    char *realbuff = (char*)malloc(4096 + pagesize);
    char *alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    blk = alignedbuff;
    
    realbuff = (char*)malloc(4096 + pagesize);
    alignedbuff = (char*)((( (unsigned long)realbuff + pagesize - 1)/ pagesize) * pagesize);
    outblk = alignedbuff;

    printf("start readphase\n");
    printf("realbuff %lx alignedbuff %lx\n",(unsigned long)realbuff, (unsigned long)alignedbuff);
    
    memset(outblk,0,4096);
    for(i=0;i<4096;i++){
        if(i%1024 == 1023){
            outblk[i] = '\n';
            c++;
        }else{
            outblk[i] = c;
        }
    }

    srand(12345);

    // read files 
    int fds[num_files];
    short small_file[num_files];
    char fname[100];
    int bigfilereads = 0;
    int smallfilereads = 0;
    int start = 0;
    int numblocks = 2;
    int flags;

    for(i=0;i<num_files;i++){
        char* prefix = "smf";
        small_file[i] = 1;
        if((i % fr) == 0){
            prefix = "bgf";
            small_file[i] = 0;
        }
        sprintf(fname,"/mnt/disks/fsize/test%s%d",prefix,i);
        flags = O_CREAT | O_RDWR | O_DIRECT | O_LARGEFILE ;
#ifdef SYNC
        flags |= O_SYNC ;
#endif
        fds[i] = open(fname, flags, S_IRWXU |S_IRWXO | S_IRWXG );
        if(fds[i] == -1) {
            perror("could not open file");
            return -1;
        }
    }

    /*
    i=read_bytes;
    j = 0;
    while(i>0){
        int file_id = myrand(num_files);
        fd = fds[file_id];
        offset = lseek(fd,0,SEEK_END);
        if(offset == -1){
            perror("lseek failed\n");
        }
        if(lseek(fd,myrand((offset/4096)-numblocks)*4096,SEEK_SET) == -1){
            perror("lseek failed\n");
        }
        //printf("file %d size %d total read %d \n",file_id,offset,i);
        start = i;
        for(k=0;k<numblocks && i>0;k++){
            red = read(fd,blk,4096);
            if(red == -1) {
                perror("read error");
                break;
            }else if (red == 0){
                break;
            }else{
                i -= red;
            }
        }
        if(offset > small_file_size){
            bigfilereads += (start-i);
        }else{
            smallfilereads += (start-i);
        }
        if(j%10000==0){
            printf("iter %d file %d size %d this iteration read %d total read %d \n",j,file_id,offset,(start-i),i);
        }
        j++;
    }
    */
    for(y=0;y<1;y++){
        printf("iteration %d \n",y);
        for(i=0;i<num_files;i++){
            if(small_file[i] == 0){
                //continue;
            }
            fd = fds[i];
            offset = lseek(fd,0,SEEK_END);
            if(offset == -1){
                perror("lseek failed\n");
            }
            //printf("file %d size %d total read %d \n",file_id,offset,i);
            for(k=offset;k>0;k-=4096){
                if(lseek(fd,k,SEEK_SET) == -1){
                    perror("lseek2 failed\n");
                }
                if(small_file[i]){
                    acc = read(fd,blk,4096);
                }else{
                    acc = write(fd,outblk,4096);
                }
                if(acc == -1) {
                    perror("read error");
                }
                if(offset > small_file_size){
                    bigfilereads += 4096;
                }else{
                    smallfilereads += 4096;
                }
            }
        }
    }
    
    for(i=0;i<num_files;i++){
        close(fds[i]);
    }
            
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);

    //printf("clear_stats %s\n",buffer);

    printf("end readphase: bigfilereads %d smallfilereads %d totalreads %d \n", bigfilereads, smallfilereads, bigfilereads + smallfilereads);
    printf("end readphase: bigfilereads secs %d smallfilereads secs %d totalreads secs %d \n", bigfilereads/512, smallfilereads/512, (bigfilereads + smallfilereads)/512);
    return 0;
}

void setup(int argc, char** argv){
    setup_guest_disks(1);
}

void runner(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    
    sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 0 ",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"echo '/bin/rm -rf /mnt/disks/fsize/' | sudo bash");
    run_cmd(cmd,buffer);
    {
        TIMER_START;
        main_helper1();
        drop_caches();
        TIMER_END("writephase");
    }

    //getchar();
    {
        TIMER_START;
        main_helper2();
        TIMER_END("readphase");
    }
    
    sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    run_cmd(cmd,buffer);
    
}
