#include "testfwk.c"
#include "khash.h"
#include <unistd.h>
#include <sys/syscall.h>  
#include <sys/types.h>  

int fsutilsbenchwkld(){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    {
        TIMER_START;
        sprintf(cmd,"sudo fsck -t ufs %s",mountpath);
        run_cmd(cmd,buffer);
        TIMER_END("fscktime");
        sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
        run_cmd(cmd,buffer);
    }
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    return 0;

}

int fsutilsbenchsetup(void){
    int i;
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    //sprintf(cmd,"sudo %s/cmdclient.o trace 3 ",homedir);
    //run_cmd(cmd,buffer);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 4197088 ",homedir);
    //run_cmd(cmd,buffer);

    
    TIMER_START;
    run_cmd_o("mkdir -p /mnt/disks/data ");
    //run_cmd_o("cp /home/sura/linux.tar.gz /mnt/disks/ ");
    //run_cmd_o("tar -C /mnt/disks/data/ -xzf /mnt/disks/linux.tar.gz ");
    //run_cmd_o("/bin/rm /mnt/disks/linux.tar.gz ");
    
    /*for(i=0;i<40;i++){
        sprintf(cmd,"cp /home/sura/linux.tar.gz /mnt/disks/a%d ",i+1);
        run_cmd_o(cmd);
        run_cmd_o("sync");
    }*/
    run_cmd_o("cp /home/sura/linux.tar.gz /mnt/disks/ ");
    {
        TIMER_START;
        sprintf(cmd,"gzip -cd /mnt/disks/linux.tar.gz > /mnt/disks/linux.tar");
        run_cmd_o(cmd);
        TIMER_END("unziptime");
    }
    
    for(i=0;i<6;i++){

        {
            TIMER_START;
            sprintf(cmd,"mkdir /mnt/disks/data%d/ ",i);
            run_cmd_o(cmd);
            sprintf(cmd,"tar -C /mnt/disks/data%d/ -xf /mnt/disks/linux.tar ",i);
            run_cmd_o(cmd);
            TIMER_END("untartime");
        }

    }
    drop_caches();
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);
    
    TIMER_END("fsutilssetuptime");
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);
    
    sprintf(cmd,"sudo %s/cmdclient.o clear_stats",homedir);
    run_cmd(cmd,buffer);
    
    //sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 0 ",homedir);
    //run_cmd(cmd,buffer);
   
    /*
    // hack to make fsck reads as fast as smartmode > 0
    // TODO: investigate why ?
    if(smartmode < 0){
        sprintf(cmd,"sudo %s/cmdclient.o addcr3 %lx %d\n",homedir,cr3,pid);
        run_cmd(cmd,buffer);
    }
    */

    return 0;
}

int fsutilsbenchteardown(void){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];

    //sprintf(cmd,"sudo %s/cmdclient.o diskreadcache 1 ",homedir);
    //run_cmd(cmd,buffer);

    return 0;
}

#define NUM_ITERS (1)

int main_helper(int argc, char**argv){
    float total,times[NUM_ITERS];
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char tmp[4096];
    long long i =0;
    int j;

    //sprintf(cmd,"sudo %s/cmdclient.o trace 112 ",homedir);
    //run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    fsutilsbenchsetup();
    
    for(j=0;j<NUM_ITERS;j++){
        sprintf(tmp,"iterations %d \n",j);

        TIMER_START;
        fsutilsbenchwkld();
        TIMER_END(tmp);
        
        times[j] = total_time;
        total += times[j];
        printf("iter #%d total %0.2f secs \n",j,times[j]);
    }
        
    printf("sum %0.2f secs avg %0.2f secs\n", total, (total/NUM_ITERS));
    
    fsutilsbenchteardown();
    
    /*
    sprintf(cmd,"sudo %s/cmdclient.o resize_cache 100 ",homedir);
    run_cmd_core(cmd,buffer,1);
    */

    return 0;
}

void setup(int argc, char** argv){
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    setup_guest_disks(1);
    
    /*
    sprintf(cmd,"sudo %s/cmdclient.o resize_cache 8 ",homedir);
    run_cmd_core(cmd,buffer,1);
    */
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    //hack to bring fsck contents into cache at metadata before untar 
    sprintf(cmd,"sudo %s/cmdclient.o cache_smartdumb %d \n",homedir,smartmode);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o readfill 1 ",homedir);
    run_cmd(cmd,buffer);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 4197088 ",homedir);
    //run_cmd(cmd,buffer);

    sprintf(cmd,"sudo fsck -t ufs %s",mountpath);
    run_cmd(cmd,buffer);
    
    sleep(30);
    
    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o readfill 0 ",homedir);
    run_cmd(cmd,buffer);
    
    sprintf(cmd,"sudo %s/cmdclient.o get_cache_distribution",homedir);
    run_cmd_core(cmd,buffer,1);

    //sprintf(cmd,"sudo %s/cmdclient.o trace 0 ",homedir);
    //run_cmd(cmd,buffer);

    //exit(0);

}

void runner(int argc, char** argv){
    main_helper(argc,argv);
}
