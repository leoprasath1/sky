#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "expts/testfwklib.c"

int MAX_BUF_SIZE = 4096*100;

int main(int argc, char *argv[])
{
    char* ptr;
    char buffer[MAX_BUF_SIZE];
    int ret;
    int chunknum = 0;
    int remaining = 0;
    int flags = O_RDONLY | O_LARGEFILE;


    if(argc < 2){
        printf("usage ./a.out <filepath>\n");
        return 0;
    }

    int fd = open(argv[1], flags , 0);
    if(fd < 0){
        perror("Error opening file");
        printf("Error opening file %s \n", argv[1]);
        return -1;
    }

    while(1){
        int index = 0;
        memset(buffer+remaining, 0, MAX_BUF_SIZE -remaining);
        ret = read(fd,buffer+remaining,MAX_BUF_SIZE-remaining);
        if(ret < 0){
            perror("Error could not read from file");
            printf("Error could not read from file %s ", argv[1]);
        }
        if(ret == 0){
            break;
        }
        remaining += ret;
        index = 0;

        while(remaining >= 512){
            printf("chunk %d block %d checksum %x \n", chunknum++, chunknum/8, FNVHash(buffer+index,512,0));
            remaining -= 512;
            index += 512;
        }
        if(remaining){
            memcpy(buffer,buffer+index,remaining);
        }
    }

    if(remaining){
        printf("chunk %d block %d checksum %x \n", chunknum++, chunknum/8, FNVHash(buffer,512,0));
    }

    return 0;
}

