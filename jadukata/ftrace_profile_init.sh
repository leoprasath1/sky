#!/bin/bash -x 

pid=`ps auwx | grep bash | head -n 1 | tr -s ' ' | cut -d' ' -f2`

echo $pid

echo "ftrace init"
echo 'echo "nop" >/sys/kernel/debug/tracing/current_tracer' | sudo bash -x
echo 'echo "409600" > /sys/kernel/debug/tracing/buffer_size_kb' | sudo bash -x
echo 'echo "" > /sys/kernel/debug/tracing/trace' | sudo bash -x
echo 'echo 1 > /sys/kernel/debug/tracing/function_profile_enabled' | sudo bash -x
echo 'echo 0 > /sys/kernel/debug/tracing/options/graph-time' | sudo bash -x
echo "echo $pid > /sys/kernel/debug/tracing/set_ftrace_pid  " | sudo bash -x
#echo 'echo "*:mod:kvm" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "*:mod:kvm_intel" >> /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "*:mod:bcache" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x
#echo 'echo "*:mod:DISKDRIVER" > /sys/kernel/debug/tracing/set_ftrace_filter' | sudo bash -x

echo 'echo 1 > /sys/kernel/debug/tracing/tracing_on' | sudo bash -x
