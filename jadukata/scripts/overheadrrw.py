#!/usr/bin/python -u

import numpy as np
import numpy.random
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.ticker import NullLocator
from collections import defaultdict
import string
import re
from matplotlib.pyplot import step, legend, xlim, ylim, show, axvline


from optparse import OptionParser

def autoviv(levels=1, final=dict):
  return (defaultdict(final) if levels < 2 else
      defaultdict(lambda: autoviv(levels - 1, final)))

def dict2arr(d):
    maxkey = 0;
    for key,val in d.iteritems():
        if key > maxkey:
            maxkey = key
    maxkey+=1;
    vals = []
    for i in range(0,maxkey):
        vals.append(d[i])
    return vals

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-f", "--filepath", dest="f", help="datafile path", metavar="FILE")
    parser.add_option("-d", "--datacolumn", dest="d", help="the column with data", metavar="D")
    parser.add_option("-i", "--numdatacolumns", dest="i", help="number of columns with data", metavar="I")
    parser.add_option("-x", "--xlabelcolumn", dest="x", help="the column with x labels", metavar="X")
    parser.add_option("-y", "--ylabelcolumn", dest="y", help="the column with y labels", metavar="Y")
    parser.add_option("-t", "--title", dest="title", help="title for the graph", metavar="T")
    parser.add_option("-o", "--output", dest="output", help="output file", metavar="O")
    
    parser.add_option("-q", "--quiet", action="store_false", dest="verbose", default=True, help="don't print status messages to stdout")
    
    (options, args) = parser.parse_args()

    if options.f is None:
       print "Missing input data file\n"
       parser.print_help()
       exit(-1)
    #if options.d is None:
    #   print "Missing input data file\n"
    #   parser.print_help()
    #   exit(-1)
    #if options.x is None:
    #   print "Missing input data file\n"
    #   parser.print_help()
    #   exit(-1)
    #if options.y is None:
    #   print "Missing input data file\n"
    #   parser.print_help()
    #   exit(-1)
    #if options.a is None:
    #   options.a = "1"
    if options.i is None:
       options.i = "1"
    #skip the headers
    
    #options.a = int(options.a)

    # capitalize title
    if options.title is not None:
        options.title = string.capwords(options.title)

    d = autoviv(2,int)

    # convert to zero based indexing
    #options.d = int(options.d) - 1
    #options.x = int(options.x) - 1 
    #options.y = int(options.y) - 1
    options.i = int(options.i)
    
    dfile = open(options.f,'r')
    header = dfile.readline()
    j = 0
    for line in dfile.readlines():
        vals = line.strip().split()
        if(len(vals) < options.i):
            print "ERROR not enough columns"
            exit(1)
        for i in range(0,len(vals)):
            print "%d %d %s" % (i,j, vals[i].strip())
            d[i][j] = vals[i].strip()
        j+=1;

    print d[0]
    print d[1]
    print d[2]
    print dict2arr(d[0])
    print dict2arr(d[1])
    print dict2arr(d[2])

    print "leo"
       
    #mpl.rcParams['xtick.major.size'] = 1
    #mpl.rcParams['ytick.major.size'] = 1
    font = { 
        #'family' : 'normal',
        'weight' : 'bold',
        'size'   : 14
        }

    mpl.rc('font', **font)
    fig = plt.figure()
    xlabels = []
    ylabels = []
    xticks = []
    yticks = []

    #print xindex,yindex
        #xlabels.append()
        #xticks.append(i-0.2)
        #ylabels.append(num2name2(j))
        #yticks.append(j)

    ax = fig.add_subplot(1, 1, 1)
    ax.set_ymargin(0.1)
    ax.set_xmargin(0.1)
    ax.set_xlabel('Request Size(Bytes)',fontsize=14,weight='bold')
    ax.set_ylabel('Average Latency(usecs)',fontsize=14,weight='bold')
    xlim(0,9000)
    ylim(0,50)
    #ax.set_xticks(dict2arr(d[0]))
    #ax.set_yticks(yticks)
    #ax.set_xticklabels(dict2arr(d[0]))
    #ax.set_yticklabels(ylabels)
    plt.title('Repeated I/O to Same Block')
    p1, = plt.plot(dict2arr(d[0]),dict2arr(d[1]),'h',color='0.3')
    p2, = plt.plot(dict2arr(d[0]),dict2arr(d[2]),'^',color='0.75')
    legend([p1,p2],["Writes(w/intercept)","Reads(w/intercept)"])
    for i in range(0,len(d[0])):
        if float(dict2arr(d[0])[i])%512 == 0:
            ax.text(float(dict2arr(d[0])[i]),float(dict2arr(d[2])[i])-2,d[0][i],size='small',ha='center',rotation=33)
    if(options.output):
        plt.savefig(options.output)
    else:
        plt.show()
    
