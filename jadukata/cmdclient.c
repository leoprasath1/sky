#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

#define SERVER_IP "128.105.104.162"
//#define SERVER_IP "128.105.104.152"
#include "expts/testfwklib.c"
#define DEBUGFSINPUT "/tmp/debugfs"
int MAX_BUF_SIZE = 4096*1000*40;

void parse_output(float* stats, float data, float meta, float hmeta, float total){
    int i;
    for(i=0;i<5;i++){
        printf("data ioclass:%d %0.2f p %0.2f \n",i, stats[i], (stats[i]*100)/total);
    }
    printf("meta ioclass:%d %0.2f p %0.2f \n",5, meta, (meta*100)/total);
    printf("hmeta ioclass:%d %0.2f p %0.2f \n",5, hmeta, (hmeta*100)/total);
    printf("data %0.2f p %0.2f meta %0.2f p %0.2f total %0.2f \n", data, (data*100)/total, (meta+hmeta), ((meta+hmeta)*100)/total, total);
}

int parse_cached_sectors1(char* bufferin){
    char *ignore = " \t\r\n";
    int retrycount = 0;
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char *inptr = bufferin;
    char disk[100];
    int ret;
    int k1,v1,k2,v2,repeats;
    khint_t i; 
    khiter_t it;
    float stats[5] = {0,0,0,0,0}, hmeta = 0, meta = 0, data = 0 , total = 0;

    if(debug)
        printf("starting parse_cached_sectors1 %s \n",bufferin);
    
    //inode to size
    khash_t(32) *h1 = NULL;
    if(h1!=NULL){
        kh_destroy(32,h1);
        h1 = NULL;
    }
    h1 = kh_init(32);

    //virt disk sector to file inode    
    khash_t(32) *h2 = NULL;
    if(h2!=NULL){
        kh_destroy(32,h2);
        h2 = NULL;
    }
    h2 = kh_init(32);
    
    sprintf(cmd,"mount | grep disks");
    run_cmd(cmd,buffer);

    sscanf(buffer,"%s on",disk);

    {
        char *tptr, *ptr, *longbuffer;
        int tk, tv, tret;
        int inode,size;
        khint_t l;
        khiter_t tit;
        khash_t(32) *temph = NULL;
        int fd;
        //get file map for all files and update h2
        //find the file size and update h1
        {
            longbuffer = (char*)malloc(MAX_BUF_SIZE);
            sprintf(cmd,"sudo find /mnt/disks/ -type f ");
            run_cmd_core_helper(cmd,longbuffer,debug,MAX_BUF_SIZE);
            ptr = longbuffer;
            ptr = ptr + strspn(ptr,ignore);
        }
    
        sprintf(cmd,"rm %s ", DEBUGFSINPUT);
        run_cmd(cmd,buffer);

        fd = open(DEBUGFSINPUT, O_CREAT | O_RDWR , S_IRWXU |S_IRWXO | S_IRWXG );

        if(fd == -1){
            perror("ERROR could not open " DEBUGFSINPUT);
            return -1;
        }

        do{
            int ret;
            char tmp[100], *temp = tmp;
            tptr = ptr + strcspn(ptr,ignore);
            if(!tptr) break;
            strncpy(temp, ptr,tptr-ptr);
            temp[tptr-ptr] = '\n';
            temp[tptr-ptr+1] = '\0';
            // replace starting strlen("/mnt/disks/");
            strncpy(temp,"stat          ",11);
            if(debug){
                printf("file %s \n", temp);
            }
            ret = write(fd, temp, strlen(temp));
            if(ret == -1 || ret == 0){
                perror("ERROR writing to " DEBUGFSINPUT);
                return -1;
            }
            ptr = tptr + strspn(tptr,ignore);
        }while(*ptr);

        close(fd);

        {
            char *tptr;
            char buffer[MAX_PAYLOAD];
            char *tlongbuffer = longbuffer;
            char cmd[CMD_LEN];
            sprintf(cmd,"sudo debugfs -f %s %s",DEBUGFSINPUT,disk);
            run_cmd_core_helper(cmd,tlongbuffer,debug,MAX_BUF_SIZE);
            tptr = tlongbuffer;
            
            while(tptr){
                char* tempptr, *sptr;
                char oldvalue;
                if(temph != NULL){
                    kh_destroy(32,temph);
                    temph = NULL;
                }
                temph = kh_init(32);
               
                tptr = strstr(tptr, "debugfs:");
                sptr = tptr;
                
                if(tptr == NULL){
                    break;
                }
                
                tempptr = strstr(tptr + 1, "debugfs:");
                if(tempptr != NULL){
                    tempptr -= 3;
                    oldvalue = *tempptr;
                    *tempptr = '\0'; 
                }

                tptr = strstr(tptr,"Inode:");
                if(tptr == NULL){
                    break;
                }

                if(debug){
                    int chars =  strstr(sptr,"Inode:")-sptr;
                    if(chars <= 0){
                        printf("ERROR chars too few %d \n",chars);
                        chars = 1;
                    }
                    printf("processing file %.*s ", chars, sptr);
                }

                tptr += strlen("Inode:") + 1;
                sscanf(tptr,"%d",&inode);

                tptr = strstr(tptr,"Size:");
                if(tptr == NULL){
                    printf("could not find size ");
                    exit(1);
                }
                tptr += strlen("Size:") + 1;
                sscanf(tptr,"%d",&size);

                {
                    khiter_t tit;
                    tit = kh_put(32,h1,inode,&tret);   
                    if(!tret){
                        printf("ERROR inserting key into h1 %d \n",inode);
                        exit(1);
                    }else{
                        //printf("inserting key into h3 %d \n",v2);
                    }
                    kh_value(h1,tit) = size;
                }

                map_to_hash(tptr,temph);

                my_foreach(temph, l, tk, tv){
                    khiter_t tit;
                    tit = kh_put(32,h2,tv,&tret);   
                    if(!tret){
                        printf("ERROR inserting key into h2 %d \n",tv);
                        exit(1);
                    }else{
                        //printf("inserting key into h3 %d \n",v2);
                    }
                    kh_value(h2,tit) = inode;
                }

                kh_clear(32,temph);
                if(tempptr != NULL){
                    *tempptr = oldvalue; 
                }
            }
        }
        free(longbuffer);
    }
    //remove last comma
    inptr = bufferin;
    inptr[strlen(inptr)-1] = '\0';

    while(1){
        if(!inptr){
            break;
        }
        inptr  += strspn(inptr,ignore);
        repeats = 0;
        sscanf(inptr,"%d-%d,",&k1,&repeats);
        inptr = strchr(inptr,',');
        if(inptr){
            inptr++;
        }
        if(k1<0){
            hmeta += repeats;
            continue;
        }

        it = kh_get(32,h2,k1);
        if(it == kh_end(h2)){
            meta+=repeats;
            continue;
        }else{
            v1 = kh_val(h2,it);
            it = kh_get(32,h1,v1);
            if(it == kh_end(h1)){
                printf("ERROR key not found in h1 %d \n", v1);
                exit(1);
            }else{
                v2 = kh_val(h1,it);
                stats[filesize_to_ioclass(v2)]+=repeats;
            }
        }
    }
    
    for(i=0;i<5;i++){
        data += stats[i];
    }
    total = data + meta + hmeta;
    
    parse_output(stats,data,meta,hmeta,total);
     
    return 0;
}

int parse_cached_sectors(char* bufferin){
    int retrycount = 0;
    char cmd[CMD_LEN];
    char buffer[MAX_PAYLOAD];
    char *inptr = bufferin;
    char disk[100];
    int ret;
    int k1,v1,k2,v2, repeats;
    khint_t i; 
    khiter_t it;
    float stats[5] = {0,0,0,0,0}, hmeta = 0, meta = 0, data = 0 , total = 0;

    if(debug)
        printf("starting parse_cached_sectors %s \n",bufferin);
    
    //inode to size
    khash_t(32) *h1 = NULL;
    if(h1!=NULL){
        kh_destroy(32,h1);
        h1 = NULL;
    }
    h1 = kh_init(32);

    //guest disk sector to file inode    
    khash_t(32) *h2 = NULL;
    if(h2!=NULL){
        kh_destroy(32,h2);
        h2 = NULL;
    }
    h2 = kh_init(32);
    
    sprintf(cmd,"mount | grep disks");
    run_cmd(cmd,buffer);

    sscanf(buffer,"%s on",disk);
    
    //remove last comma
    inptr = bufferin;
    char *ignore = " \t\r\n";

    while(1){
        if(!inptr){
            break;
        }
        inptr  += strspn(inptr,ignore);
        repeats = 0;
        sscanf(inptr,"%d-%d,",&k1,&repeats);
        inptr = strchr(inptr,',');
        if(inptr){
            inptr++;
        }
        if(k1<0){
            meta+=repeats;
            continue;
        }
        retrycount = 0;
retry:
        if(retrycount > 1){
            meta+=repeats;
            continue;
        }
        it = kh_get(32,h2,k1);
        if(it == kh_end(h2)){
            char temp[100];
            char* longbuffer = (char*)malloc(MAX_BUF_SIZE);
            char *tptr;
            int tk, tv, tret;
            int inode,size;
            khint_t l;
            khiter_t tit;
            khash_t(32) *temph = NULL;
            temph = kh_init(32);
            //find the file for the missing guest disk sector
            //get file map and update h2
            //find the file size and update h1
            sprintf(cmd,"sudo debugfs -R 'icheck %d' %s",k1,disk);
            run_cmd_core(cmd,buffer,0);

            sprintf(temp, "%d", k1);
            tptr = strstr(buffer,temp);
            if(tptr == NULL){
                meta+=repeats;
                continue;
            }
            tptr += strlen(temp) + 1;
            if(*tptr <'0' || *tptr > '0'){
                meta+=repeats;
                continue;
            }
            sscanf(tptr,"%d",&inode);

            sprintf(cmd,"sudo debugfs -R 'stat <%d>' %s",inode,disk);
            run_cmd_core_helper(cmd,longbuffer,debug,MAX_BUF_SIZE);
            
            tptr = strstr(longbuffer,"Size:");
            if(tptr == NULL){
                printf("could not stat inode %d disk %s\n", inode , disk);
                exit(1);
            }
            tptr += strlen("Size:") + 1;
            sscanf(tptr,"%d",&size);

            {
                khiter_t tit;
                tit = kh_put(32,h1,inode,&tret);   
                if(!tret){
                    printf("ERROR inserting key into h1 %d \n",inode);
                    exit(1);
                }else{
                    //printf("inserting key into h3 %d \n",v2);
                }
                kh_value(h1,tit) = size;
            }
        
            map_to_hash(longbuffer,temph);
    
            my_foreach(temph, l, tk, tv){
                khiter_t tit;
                tit = kh_put(32,h2,tv,&tret);   
                if(!tret){
                    printf("ERROR inserting key into h2 %d \n",tv);
                    exit(1);
                }else{
                    //printf("inserting key into h3 %d \n",v2);
                }
                kh_value(h2,tit) = inode;
            }

            retrycount++;
            free(longbuffer);
            goto retry;
        }else{
            v1 = kh_val(h2,it);
            it = kh_get(32,h1,v1);
            if(it == kh_end(h1)){
                printf("ERROR key not found in h1 %d \n", v1);
                exit(1);
            }else{
                v2 = kh_val(h1,it);

                stats[filesize_to_ioclass(v2)]+=repeats;
            }
        }
    }

    
    for(i=0;i<5;i++){
        data += stats[i];
    }
    total = data + meta;
    
    parse_output(stats,data,meta,hmeta,total); 
    
    return 0;
}


int main(int argc, char *argv[])
{
    int sockfd = 0, n = 0, i;
    char recvBuff[MAX_PAYLOAD];
    char sendBuff[MAX_PAYLOAD];
    char* ptr;
    struct sockaddr_in serv_addr; 

    int ret = 1;
    if (argc < 2) {
        printf("Usage: netlink <addcr3 val |delcr3 val |test cr3 addr | clear | status | settaskptr | logrotate | journal_handle data | get_cached_sectors >\n");
        return 1;
    }

    //debugging purpose
    /*
    {
        char tbuf[4096];
        printf("cmdclient.o %d \n", getpid());
        int fd = open("/tmp/loga",O_RDWR | O_CREAT| O_APPEND, S_IRWXU); 
        sprintf(tbuf,"cmdclient.o cmd %s pid %d \n", argv[1] ,getpid());
        write(fd,tbuf,strlen(tbuf));
        close(fd);
    }
    */
    
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n ERROR : Could not create inet socket \n");
        return 1;
    } 

    memset(recvBuff, 0,sizeof(recvBuff));
    memset(sendBuff, 0,sizeof(sendBuff));
    memset(&serv_addr, 0, sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(5000); 

    if(inet_pton(AF_INET, SERVER_IP, &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    } 

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n ERROR : Connect Failed \n");
       return 1;
    }
    
    /*
    //set recv timeout 
    struct timeval timeout;      
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
        error("setsockopt failed\n");
    
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
        error("setsockopt failed\n");
    */

    int code;
    int txfr;
    int optval = 1;
    if (setsockopt (sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval) ) < 0)
        perror("setsockopt failed\n");

    ptr = sendBuff;
    for(i=1;i<argc;i++){
        ptr += sprintf(ptr,"%s ",argv[i]);
    }
    ptr += sprintf(ptr,"\n");
    
    //printf(" Trying to send command %s \n",sendBuff);
    if(strstr(sendBuff,"get_cached_sectors") != NULL){
        code = 1;
    }

    txfr = MAX_PAYLOAD;
    do{
        n = write(sockfd, sendBuff + (MAX_PAYLOAD-txfr), txfr);
        if(n<0){
            printf(" ERROR : sending command failed %d \n",n);
            return 1;
        }
        txfr-=n;
    }while(txfr);

    txfr = MAX_PAYLOAD;
    do{
        n = read(sockfd, recvBuff + (MAX_PAYLOAD-txfr), txfr);
        if(n<0){
            printf(" ERROR : receiving command result failed %d \n",n);
            return 1;
        }
        txfr -= n;
    }while(txfr || !n);

    sscanf(recvBuff,"%d",&ret);

    switch(code){
        case 1:
            ret = parse_cached_sectors1(recvBuff);
            if(debug)
                printf("result: %s \n",recvBuff);
            break;
        default:
            printf("result: %s \n",recvBuff);
            break;
    };
    
    if(ret == 0){
        printf("Sucessfully performed command %s\n",argv[1]);
    }else{
        printf("ERROR while performing command %s\n",argv[1]);
    }

    close(sockfd);
    
    return 0;
}

/*
int main(int argc, char *argv[])
{
    int i;
    char sendBuff[MAX_PAYLOAD];
    char* ptr = sendBuff;
    int fd  = open("/tmp/c", O_RDONLY);
    do{
       i = read(fd,ptr,4096);
       ptr += i;
    }while(i>0);
    ptr += sprintf(ptr,"\n");
    parse_cached_sectors1(sendBuff);
}
*/
