#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include "../ioctl.h"

#define DEV "/dev/DMDEDUP0"

int myioctl(int fd, int req, ...)
{
    int retries = 3;
    int num = 0;
    int ret = 1;
    void* arg;
    va_list args;
    int args_ioctl = 0;
    if(req == TEST){
        args_ioctl = 1;
    }
    if(args_ioctl){
        va_start(args,req);
        arg = va_arg(args,void*);
        va_end(args);
    }

    do {
        num++;
        if(args_ioctl)
            ret = ioctl(fd, req, arg);
        else
            ret = ioctl(fd,req);

        if(ret)
            fprintf(stderr,"retrying failed ioctl %d error : %d %s\n",num,errno,strerror(errno));
    }while(ret!=0 && num <=retries);

    return ret;
}

int main(int argc, char *argv[])
{
        int fd;
        int ret = 1;
        unsigned long ioctl_long;

        if (argc < 2) {
                printf("Usage: ioctl <start|stop>\n");
                return ret;
        }

        fd = open(DEV, O_RDONLY);

        if (strcmp(argv[1], "start") == 0) {
                fprintf(stderr, "Starting DISKDRIVER ...\n");
                ret = myioctl(fd, START_DISKDRIVER);
        } else if (strcmp(argv[1], "stop") == 0) {
                fprintf(stderr, "Stoping DISKDRIVER ...\n");
                ret = myioctl(fd, STOP_DISKDRIVER);
        } else {
            fprintf(stderr, "Invalid command %s \n", argv[1]);
        }
        close(fd);

        return ret;
}
