#!/usr/bin/python -u

import os
import sys
import subprocess
import shlex
from time import sleep,time
import re
import sys
import datetime
import random
import shelve
import signal

def getmyprint(stmt):
    return "%s : %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M-%S"),stmt)
    
def myprint(stmt):
    print "%s" % getmyprint(stmt)

def localexec(cmd,sudo=1,ignore=0, log=1):
    if sudo == 1:
        cmd = "sudo " + cmd
    #myprint("running command " + cmd)
    out = ""
    err = ""
    ret = 0
    t1 = t2 = 0
    try:
        t1 = t2 = time()
        out = subprocess.check_output(shlex.split(cmd), stderr=subprocess.STDOUT)
        t2 = time()
    except subprocess.CalledProcessError as e:
        ret = e.returncode
        out = e.output
        if (ignore==0):
            err = "ERROR RETURN CODE"
    if(log):
        myprint('cmd %s : %s' % (cmd,out))
    sys.stdout.flush()
    return out


SRCDIR="/home/leoa/cerny/jadukata/dm-dedup/"

mountpoint = "/mnt/dm-dedup"
devpath = "/dev/DMDEDUP0"

localexec("insmod %s/dm-dedup.ko " %(SRCDIR));
localexec("mount %s %s" %(devpath,mountpoint));

localexec("umount %s"%(mountpoint));

exit(0)

