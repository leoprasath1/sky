#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <aio.h>
#include <string.h>

#define RR 0
#define RW 1
#define SR 2
#define SW 3
#define WO 4
#define AC 5
#define MT 6
#define RT 7
#define SP 8
#define NS 9
#define SZ 10
#define RN 11
#define SE 12
#define DD 13

#define RR_S "rr" // random reads
#define RW_S "rw" // random writes
#define SR_S "sr" // sequential reads
#define SW_S "sw" // sequential writes
#define WO_S "wo" // write overhead detection
#define AC_S "ac" // access an offset
#define MT_S "mt" // mixed trace
#define RT_S "rt" // random trace
#define SP_S "sp" // seek profile
#define NS_S "ns" // number of read cache segments
#define SZ_S "sz" // read cache segment size
#define RN_S "rn" // rotation time
#define SE_S "se" // seek time
#define DD_S "dd" // dedup

#define diff(a,b) ((b.tv_sec - a.tv_sec) * 1000000 + (b.tv_usec - a.tv_usec))
#define conv(a) ( a.tv_sec*1000000 + a.tv_usec )

//#define ASYNC
#define COUNT (200)

int pcount = 1;
off64_t size = 600000; // in sectors
//off64_t size = 525168; // in sectors
int blksize = 512;
off64_t maxblocks;
int nr_sectors = 1;
char* blk[COUNT];
int count = 1;
off64_t offset = 0;
char *dev = "/dev/SBA0";
int should_write = 0;
int fd;
int verbose = 0;
int mode = -1;
long num = 10000;

int* pids;

clock_t start ,end;

char errmsg[200];
int err_num;

off64_t mylongrand(off64_t max){
	return((off64_t) (max * (rand() / (RAND_MAX + 1.0))) );
}

//allocate buffer
int allocate_buffer(int size, int fill){
	int tmp = getpagesize();
	char* blktmp;
	int i;
	for(i=0;i<COUNT;i++){
		blktmp = (char*)malloc(size+tmp); //unalligned buffer
		if(blktmp == NULL){
			err_num = errno;
			sprintf(errmsg, "could not alloc %d bytes",blksize);
			perror(errmsg);
			return err_num;
		}
		blk[i] = (char*)(( ((unsigned long)blktmp)/tmp +1)*tmp); //aligned buffer
		if(fill == 1){
			//fill buffer
			for(tmp =0 ;tmp<size;tmp++)
			{
				blk[i][tmp] = 'a' + mylongrand(26);
			}
		}
	}

	return 0;
}

//open a device
int open_dev(){
	fd = open(dev, O_RDWR | O_LARGEFILE | O_DIRECT);

	if(fd == -1){
		err_num = errno;
		sprintf(errmsg, "error could not open dev %s",dev);
		perror(errmsg);
		return err_num;
	}
	return 0;
}

#ifdef ASYNC			
struct aiocb aiodes[COUNT];
#endif

int my_access(off64_t offset, int count, int should_write){
			unsigned int tcount;
			unsigned int num_sectors = (count*blksize)/512;
			unsigned int tmp;
			int destcnt, done;
#ifdef ASYNC			
			int descnt = (num_sectors/nr_sectors ) + 1;
			if(descnt >= COUNT){
				sprintf(errmsg,"Error : allocate more buffer %d \n",descnt);
				perror(errmsg);
			}
			for(tmp=0;tmp<descnt;tmp++){
				memset(&aiodes[tmp], 0, sizeof(struct aiocb));
				aiodes[tmp].aio_fildes = fd;
				aiodes[tmp].aio_buf = blk[tmp];
				aiodes[tmp].aio_sigevent.sigev_notify = SIGEV_NONE;
			}
#endif
			off64_t pos = offset*blksize;
#ifndef ASYNC
			pos = lseek64(fd,offset * blksize,SEEK_SET);
			if(pos < 0){
				err_num = errno;
				sprintf(errmsg, "error could not seek to %lu",offset);
				perror(errmsg);
				return err_num;
			}
#endif
			int rwcount;

			if(verbose >1){
				printf("write? %d offset %lu blksize %d offsetinbytes %lu pos %lu count %d\n",should_write,offset,blksize,offset*blksize,pos,count);		
			}

			for(tmp=0,destcnt=0;tmp<num_sectors;tmp+=tcount,destcnt++){
				tcount = nr_sectors;
				if(tmp + nr_sectors > num_sectors)
					tcount = num_sectors - tmp;
				#ifdef ASYNC
				aiodes[destcnt].aio_nbytes = 512*tcount;
				aiodes[destcnt].aio_offset = pos + tmp*512;
				#endif
				if(should_write != 1){
				#ifndef ASYNC
					rwcount = read(fd,blk[0],512*tcount);
				#else
					rwcount = aio_read(&aiodes[destcnt]);
				#endif
				}else{
				#ifndef ASYNC
					rwcount = write(fd,blk[0],512*tcount);
				#else
					rwcount = aio_write(&aiodes[destcnt]);
				#endif
				}

				#ifndef ASYNC
				if(rwcount != (512*tcount)){
				#else
				if(rwcount != 0){
				#endif
					err_num = errno;
					sprintf(errmsg, "not read/write completely. expected %d but only %d bytes tcount %d num_Sectors %d errno %d",512*tcount,rwcount,tcount,num_sectors,err_num);
					perror(errmsg);
					return err_num;
				}
			}
			#ifdef ASYNC
			done =0;
			while(!done){
				done = 1;
				for(tmp=0;tmp<destcnt;tmp++){
					if(aio_error(&aiodes[tmp]) == EINPROGRESS){
						done =0;
						break;
					}
				}
				usleep(50000);
			}
			#endif

			return 0;
}

int rc_num_seg(){
    off64_t add[500];
    off64_t range = maxblocks / 500;
    int num_seg = 0;
    struct timeval s,e;
    unsigned long time = 0;
    int i;
    while(1){
        add[num_seg] = range*num_seg + mylongrand(range);
        num_seg++;
        //bring to cache
        if(verbose) printf("bring to cache\n");
        for(i=0;i<num_seg;i++){
            gettimeofday(&s,NULL);
            my_access(add[i],1,0);
            gettimeofday(&e,NULL);
            time = diff(s,e);
            if(verbose) printf("offset %lu diff %lu\n",add[i], time);
        }
        
        //test presence in cache
        if(verbose) printf("test presence in cache\n");
        for(i=0;i<num_seg;i++){
            gettimeofday(&s,NULL);
            my_access(add[i],1,0);
            gettimeofday(&e,NULL);
            time = diff(s,e);
            if(verbose) printf("offset %lu time %lu\n",add[i],time);
            if(time > 3000){
                printf("offset %lu not in cache time %lu \n",add[i], time);
                goto end;
            }
        }
    }
end:
    printf("number of read segments %d\n",num_seg);
    return 0;
}

int rc_seg_size(){
    int num_seg = 1;
    off64_t add[num_seg];
    off64_t range = maxblocks / num_seg;
    struct timeval s,e;
    unsigned long time = 0;
    int i;
    int seg_size=1;
    
    for(i=0;i<num_seg;i++){
        add[i] = range*i + mylongrand(range);
    }

    while(1){
        printf("segment size being tested %d \n",seg_size);
        //pollute cache
        for(i=0;i<100;i++){
            my_access(mylongrand(maxblocks),1,0);
        }
/*
        for(i=0;i<num_seg;i++){
            add[i] = range*i + mylongrand(range);
        }
*/
        
        //bring to cache
        if(verbose) printf("bring to cache\n");
        for(i=0;i<num_seg;i++){
            gettimeofday(&s,NULL);
            my_access(add[i],1,0);
            gettimeofday(&e,NULL);
            time = diff(s,e);
            if(verbose) printf("offset %lu diff %lu\n",add[i], time);
        }
        
        //test presence in cache
        if(verbose) printf("test presence in cache\n");
        for(i=0;i<num_seg;i++){
            gettimeofday(&s,NULL);
            my_access(add[i]+seg_size,1,0);
            gettimeofday(&e,NULL);
            time = diff(s,e);
            if(verbose) printf("offset %lu time %lu\n",add[i]+seg_size,time);
            if(time > 3000){
                printf("offset %lu not in cache time %lu \n",add[i],time);
                goto end;
            }
        }
        seg_size+=50;
    }
end:
    printf("segment size %d\n",seg_size);
    return 0;
}

int rotation_times(){
    off64_t add = mylongrand(1000);
    struct timeval s,e;
    unsigned long time;
    int i;
    for(i=0;i<8000;i++){
        usleep(0);
        gettimeofday(&s,NULL);
        my_access(add,1,0);
        gettimeofday(&e,NULL);
        time = diff(s,e);
        printf("%d %lu\n",i,time);
    }
    return 0;
}

int seek_times(){
    off64_t add = mylongrand(maxblocks-1);
    struct timeval s,e;
    unsigned long time;
    int i;
    for(i=0;i<10000;i++){
        usleep(i);
        gettimeofday(&s,NULL);
        my_access(add,1,0);
        gettimeofday(&e,NULL);
        time = diff(s,e);
        if(verbose) printf("%d %lu\n",i,time);
    }
    return 0;
}

int seq_wkd( int num, int should_write){
	int ret =0,tmp;
	unsigned long base = offset; //mylongrand(maxblocks - num -1);
	unsigned long obase = base;
	if(verbose)
		printf("base %lu at start\n",base);
	for(tmp=0;tmp<num && !ret;tmp++){
		ret = my_access(base,1,should_write);
        //usleep(3000);
		base++;
	}
	if(verbose)
		printf("base %lu at start total - %ld \n",base,base-obase);

	return ret;
}

int rand_wkd( int num, int should_write){
		int ret = 0,tmp;
		off64_t blknum = 0;
		for(tmp=0;tmp<num && !ret;tmp++){
			ret = my_access(mylongrand(maxblocks),count,should_write);
		}

		if(verbose)
			printf("count %d at end\n",tmp);

		return ret;
}

int rtrace( int num){
		off64_t off;
		int ret = 0,tmp,write;
		for(tmp=0;tmp<num && !ret;tmp++){
			if((rand()/(RAND_MAX+1.0)) < 0.66){
				write =0;
			}else{
				write =1;
			}
			off = mylongrand(maxblocks);
			ret = my_access(off,1,write);
		}

		if(verbose)
			printf("count %d at end\n",tmp);

		return ret;
}

int mtrace( int num){
		off64_t off=0;
		int ret = 0,tmp,write;
		for(tmp=0;tmp<num && !ret;tmp++){
			if((rand()/(RAND_MAX+1.0)) < 0.66){
				write =0;
			}else{
				write =1;
			}
			if((rand()/(RAND_MAX+1.0)) < 0.2){
				off = off +1 ;
			}else{
				if((rand()/(RAND_MAX+1.0)) < 0.3){
					off = off + mylongrand(200);
				}else{
					off = mylongrand(maxblocks);
				}
			}
			ret = my_access(off,1,write);
		}

		if(verbose)
			printf("count %d at end\n",tmp);

		return ret;
}

int dedup_trace( int num){
		off64_t off=0;
		int ret = 0,tmp,write;
		if(verbose)
			printf("starting dedup_trace\n");
		for(tmp=0;tmp<num && !ret;tmp++){
            write =1;
			if((rand()/(RAND_MAX+1.0)) < 0.5){
                sprintf(blk[0],"%10lu",off);
                sprintf(blk[0]+512,"%10lu",off);
                sprintf(blk[0]+1024,"%10lu",off);
                sprintf(blk[0]+2048,"%10lu",off);
            }else{
                sprintf(blk[0],"%10ld",9999999999);
            }
            off = off +1 ;
			ret = my_access(off,1,write);
		}

		if(verbose)
			printf("count %d at end\n",tmp);

		return ret;
}

int seek_profile(int delay){
    unsigned long offset;
    FILE *fp;
        fp = fopen("address_1tb_small_random","r");
        while(fscanf(fp,"%lu\n",&offset) != EOF){
			if( my_access(offset,1,0) ){
                return -1;
            }
            usleep(delay);
        }
        fclose(fp);
        return 0;
}

int main(int argc, char** argv){

	int c,status;

	opterr = 0;

	//srand(time(NULL));
	srand(2345);

	while ((c = getopt (argc, argv, "vwc:b:o:d:m:n:s:t:")) != -1){
		switch (c)
		{
			case 't':
				sscanf(optarg,"%d",&pcount);
				break;
			case 'n':
				sscanf(optarg,"%ld",&num);
				break;
			case 'm':
				if(strncmp(RW_S,optarg,2) == 0) mode = RW;
				else if(strncmp(SW_S,optarg,2) == 0) mode = SW;
				else if(strncmp(RR_S,optarg,2) == 0) mode = RR;
				else if(strncmp(SR_S,optarg,2) == 0) mode = SR;
				else if(strncmp(WO_S,optarg,2) == 0) mode = WO;
				else if(strncmp(RT_S,optarg,2) == 0) mode = RT;
				else if(strncmp(MT_S,optarg,2) == 0) mode = MT;
				else if(strncmp(SP_S,optarg,2) == 0) mode = SP;
				else if(strncmp(SZ_S,optarg,2) == 0) mode = SZ;
				else if(strncmp(NS_S,optarg,2) == 0) mode = NS;
				else if(strncmp(RN_S,optarg,2) == 0) mode = RN;
				else if(strncmp(SE_S,optarg,2) == 0) mode = SE;
				else if(strncmp(DD_S,optarg,2) == 0) mode = DD;
				else {
                    fprintf(stderr,"error mode is %s \n", optarg);
                    exit(1);
                }
				break;
			case 'v':
				verbose ++ ;
				break;
			case 'w':
				should_write = 1;
				break;
			case 'c':
				sscanf(optarg,"%d",&count);
				break;
			case 'b':
				sscanf(optarg,"%d",&blksize);
				break;
			case 'o':
				sscanf(optarg,"%lu",&offset);
				break;
			case 's':
				sscanf(optarg,"%d",&nr_sectors);
				break;
			case 'd':
				dev = optarg;
				break;
			case '?':
				if (optopt == 'c' || optopt == 'b' || optopt == 'o' || optopt == 'd' || optopt == 'w')
					fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				else if (isprint (optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr,"Unknown option character `\\x%x'.\n",	optopt);
				return 1;
			default:
                {
                    fprintf(stderr,"error option is %c \n", c);
                    exit(1);
                }
		}
	}

	maxblocks = size/(blksize/512);

	if(verbose)
		printf ("mode = %d dev %s blocksize = %d, count = %d, offset = %lu, write = %d num = %ld maxblocks %lu nr_sectors %d \n",mode,dev, blksize, count, offset, should_write, num,maxblocks,nr_sectors);

	int ret = allocate_buffer(blksize*nr_sectors,1);

	if(ret){
		perror("could not allocate buffer");
		return ret;
	}

	ret = open_dev();

	if(ret){
		perror("could not open device");
		return ret;
	}

	num /= pcount;
	int pi = 0;
	pids = (int*) malloc(sizeof(int) * pcount);
	for(pi=0;pi<pcount-1;pi++){
		pids[pi] = fork();
		if(pids[pi] == 0) break;
	}

	start = clock();
	switch(mode){
		case RT:
			ret = rtrace(num);
			break;
		case MT:
			ret = mtrace(num);
			break;
		case SW:
			ret = seq_wkd(num,1);
			break;
		case SR:
			ret = seq_wkd(num,0);
			break;
		case RW:
			ret = rand_wkd(num,1);
			break;
		case RR:
			ret = rand_wkd(num,0);
			break;
        case SP:
            ret = seek_profile(num);
            break;
        case SZ:
            ret = rc_seg_size();
            break;
        case NS:
            ret = rc_num_seg();
            break;
        case RN:
            ret = rotation_times();
            break;
        case SE:
            ret = seek_times();
            break;
        case DD:
            ret = dedup_trace(num);
            break;
		default:
			ret = my_access(offset,count,should_write);
			break;
	}
	end = clock();

	if(ret){
 		perror("wkd error ");
 		return ret;
 	}                                                          

	close(fd);
	if(verbose) 	
		printf("time %ld %ld\n",(end-start),CLOCKS_PER_SEC);
	
	for(pi=0;pi<pcount-1;pi++){
		waitpid(pids[pi],&status,0);
	}
    free(pids);

	return ret;
}

