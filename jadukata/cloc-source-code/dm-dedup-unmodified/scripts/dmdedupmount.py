#!/usr/bin/python -u

import os
import sys
import subprocess
import shlex
from time import sleep,time
import re
import sys
import datetime
import random
import shelve
import signal

def getmyprint(stmt):
    return "%s : %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M-%S"),stmt)
    
def myprint(stmt):
    print "%s" % getmyprint(stmt)

def localexec(cmd,sudo=1,ignore=0, log=1):
    if sudo == 1:
        cmd = "sudo " + cmd
    #myprint("running command " + cmd)
    out = ""
    err = ""
    ret = 0
    t1 = t2 = 0
    try:
        t1 = t2 = time()
        out = subprocess.check_output(shlex.split(cmd), stderr=subprocess.STDOUT)
        t2 = time()
    except subprocess.CalledProcessError as e:
        ret = e.returncode
        out = e.output
        if (ignore==0):
            err = "ERROR RETURN CODE"
    if(log):
        myprint('cmd %s : %s' % (cmd,out))
    sys.stdout.flush()
    return out


if(len(sys.argv) < 2):
    print "usage dmdedupmount.py mount(1)/umount(0)\n"
    exit(1)

mode = int(sys.argv[1])

if(mode == 0):
    print "dmdedupmount.py mode %d \n" %(mode)
else:
    print "dmdedupmount.py mode %d \n" % (mode)

SRCDIR="/home/leoa/cerny/jadukata/dm-dedup/"

#Decide on metadata and data devices:
META_DEV="/dev/sdb1"
DATA_DEV="/dev/sdb2"

#Compute target size assuming 1.5 dedup ratio:
DATA_DEV_SIZE=int(localexec("blockdev --getsz %s"%(DATA_DEV)))
TARGET_SIZE=DATA_DEV_SIZE * 15 / 10;

#Reset metadata device:
localexec("dd if=/dev/zero of=%s bs=4096 count=1" %(META_DEV));

#Setup a target:
configstr="0 %s dedup %s %s 4096 md5 cowbtree 100"%(TARGET_SIZE,META_DEV,DATA_DEV)

print configstr

def mount():
    localexec("insmod %s/dmbufio.ko " %(SRCDIR));
    localexec("insmod %s/dmdedup.ko " %(SRCDIR));
    subprocess.check_output("echo %s | sudo dmsetup create mydedup"%(configstr),stderr=subprocess.STDOUT,shell=True).strip()
    localexec("dmsetup ls ");

def umount():
    localexec("dmsetup ls ");
    localexec("sudo dmsetup remove mydedup");
    localexec("dmsetup ls ");
    localexec("rmmod dmdedup ");
    localexec("rmmod dmbufio ");


localexec("sudo rmmod netconsole ");
localexec("sudo modprobe netconsole ");
if(mode):
    mount()
else:
    umount()

exit(0)

