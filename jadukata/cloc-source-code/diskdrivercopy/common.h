#define DISKDRIVER_DEVS 	(1)
#include "../kvm-kmod-3.10.1/x86/hash/ht_at_wrappers.h"
#include "../kvm-kmod-3.10.1/x86/checksum_processor.h"

struct diskdriver_work_t {
    struct work_struct wk;
    unsigned long payload;
};

typedef struct _diskdriver_dev {
    struct block_device *f_dev;
    struct block_device *c_dev;
    unsigned long size;
    spinlock_t lock;
    struct gendisk *gd[DISKDRIVER_DEVS];
    struct block_device *s_dev[DISKDRIVER_DEVS];
    int usage;
}diskdriver_dev;

int major2index(int major);
int get_major_number(struct bio *diskdriver_bio);

void handler_finishio(struct bio* diskdriver_bio);
void handler_startio(struct bio* diskdriver_bio);

