#include "includes.h"

//based on number of outstanding i/o requests
#define POOL_SIZE (1000)

typedef struct {
    unsigned int seq_id; // sequence number for this i/o request
    unsigned int outstanding; // outstanding i/os
    struct timeval issued,received; // timestamp at which i/o was issued and received ( jiffies )
    unsigned int lba_dist; // lba distance between previous i/o request and this i/o request
    unsigned int size; // size of the i/o request
    unsigned int inter_arrival; // interarrival time between past two requests ( jiffies )
    unsigned int latency; // time taken for the request
    unsigned int queuetime; // time spent in the i/o queue
    bio_end_io_t* end_io;
    unsigned int pid;
    unsigned int dd,md;
    char name[TASK_COMM_LEN];
    unsigned long checksum;
} handler;

void handler_init(void);
void handler_cleanup(void);

void handler_startio(struct bio* diskdriver_bio);
void handler_finishio(struct bio* diskdriver_bio);

int print_stats(char*msg);
void reset_stats(void);

extern int cdata_lookup(void*);
extern void cdata_remove(void*);

void print_cache_stats(char**);

