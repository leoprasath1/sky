#include "handler.h"
#include "common.h"
#include "vmmcomm.h"

#define RANDOM_LBA_THRESHOLD (1000)

atomic_t issued = ATOMIC_INIT(0);
atomic_t received = ATOMIC_INIT(0);

atomic_t stat_totr = ATOMIC_INIT(0);
atomic_t stat_totw = ATOMIC_INIT(0);
atomic_t stat_bcache_totr = ATOMIC_INIT(0);
atomic_t stat_bcache_totw = ATOMIC_INIT(0);

atomic_t stat_journal_totr = ATOMIC_INIT(0) ,stat_journal_totw = ATOMIC_INIT(0);
atomic_t stat_nonjournal_totr =  ATOMIC_INIT(0) ,stat_nonjournal_totw = ATOMIC_INIT(0);

#ifdef REMUS
//temporary measurement for remus workloads
extern int remus_last_jiffies;
extern int remus_avg_interval;
extern int remus_num_intervals;
atomic_t num_checkpoints = ATOMIC_INIT(0);
atomic_t curr_journal_totr = ATOMIC_INIT(0), curr_journal_totw = ATOMIC_INIT(0);
atomic_t curr_nonjournal_totr = ATOMIC_INIT(0), curr_nonjournal_totw = ATOMIC_INIT(0);
atomic_t cum_mov_avg_journal_totr = ATOMIC_INIT(0), cum_mov_avg_journal_totw = ATOMIC_INIT(0);
atomic_t cum_mov_avg_nonjournal_totr = ATOMIC_INIT(0), cum_mov_avg_nonjournal_totw = ATOMIC_INIT(0);
#endif

struct kmem_cache* handler_pool = NULL; 
extern int tracing_enabled;
extern int cdata_log;
extern int cache_smart_mode;

hash_table* ht_metadata_sectors = NULL;
EXPORT_SYMBOL(ht_metadata_sectors);
hash_table* ht_data_smallfile_sectors = NULL;
EXPORT_SYMBOL(ht_data_smallfile_sectors);

#ifdef DETECT_OVERWRITES
hash_table *ht_file_chunksums = NULL;
EXPORT_SYMBOL(ht_file_chunksums);
#endif

#ifdef SMARTCACHE

unsigned long SMARTCACHE_SIZE = 0;
unsigned long smartcache_hits = 0;
hash_table* ht_free_smartslots = NULL;
hash_table* ht_tofree_smartslots = NULL;
#ifdef TESTMODE
hash_table* ht_test_free_smartslots = NULL;
#endif
hash_table* ht_smartcache = NULL;
#ifdef EXTCACHE
hash_table* ht_diskcache = NULL;
#endif
#endif

#ifdef LIFETIMES
hash_table *ht_series_lifetimes;
hash_table *ht_lifetimes;
#endif

#ifdef JADUKATA

struct cleaner_data* write_cleaner = NULL;
EXPORT_SYMBOL(write_cleaner);
struct cleaner_data *read_cleaner = NULL;
#if defined(SMARTCACHE) || defined(BCACHE)
struct cleaner_data *small_cleaner = NULL;
struct cleaner_data *meta_cleaner = NULL;
#endif
#ifdef DETECT_OVERWRITES
struct cleaner_data* fhash_cleaner = NULL;
EXPORT_SYMBOL(fhash_cleaner);
#endif

#ifdef DEDUP_WAR_PREFETCH
extern void dedup_hashpbn_cache_insert(unsigned long checksum, unsigned long sector);
struct cleaner_data* dedup_war_prefetch_detect_cleaner = NULL;
EXPORT_SYMBOL(dedup_war_prefetch_detect_cleaner);
hash_table* ht_dedup_war_prefetch_detect = NULL;
EXPORT_SYMBOL(ht_dedup_war_prefetch_detect);
#endif

static DEFINE_SPINLOCK(read_coll_lock);
EXPORT_SYMBOL(read_coll_lock);

#ifndef DEDUP_ENABLED
hash_table* ht_cleaners = NULL;
EXPORT_SYMBOL(ht_cleaners);
#else
extern hash_table* ht_cleaners;
#endif

hash_table* ht_data_checksums_read = NULL;
EXPORT_SYMBOL(ht_data_checksums_read);
hash_table* ht_data_checksums_read_coll = NULL;
EXPORT_SYMBOL(ht_data_checksums_read_coll);
hash_table* ht_data_checksums_write = NULL;
EXPORT_SYMBOL(ht_data_checksums_write);
hash_table* ht_data_hpas = NULL;
EXPORT_SYMBOL(ht_data_hpas);

atomic_t jadu_rmetadata = ATOMIC_INIT(0);
EXPORT_SYMBOL(jadu_rmetadata);
atomic_t jadu_rdata = ATOMIC_INIT(0);
EXPORT_SYMBOL(jadu_rdata);
atomic_t jadu_rsmf = ATOMIC_INIT(0);
EXPORT_SYMBOL(jadu_rsmf);
atomic_t jadu_rbgf = ATOMIC_INIT(0);
EXPORT_SYMBOL(jadu_rbgf);
atomic_t jadu_wsmf = ATOMIC_INIT(0);
EXPORT_SYMBOL(jadu_wsmf);
atomic_t jadu_wioclass[6] = {ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0)};
EXPORT_SYMBOL(jadu_wioclass);
atomic_t jadu_rioclass[6] = {ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0),ATOMIC_INIT(0)};
EXPORT_SYMBOL(jadu_rioclass);
atomic_t jadu_wbgf = ATOMIC_INIT(0);
EXPORT_SYMBOL(jadu_wbgf);

atomic_t jadu_wmetadata = ATOMIC_INIT(0);
atomic_t jadu_wdata = ATOMIC_INIT(0);

void read_cleaner_process_fn(hash_table* ht, unsigned long key, unsigned long val){

/*
#ifdef STRICT_APP_PRIORITY
#undef METADATA_IOCLASS
#define METADATA_IOCLASS (0)
#endif
*/

#if (defined(SMARTCACHE) || defined(BCACHE))
    if(cache_smart_mode >= 0){
        if(pack_info_get_z(val) != 0){
            hash_table* ht_temp = NULL;
            unsigned long packinfo;
            unsigned long flags;
            spin_lock_irqsave(&read_coll_lock,flags);
            if(ht_remove_val(ht_data_checksums_read_coll, key, (unsigned long*)&ht_temp) != -1){
                spin_unlock_irqrestore(&read_coll_lock,flags);
                while(ht_pop(ht_temp, &packinfo) != -1){
#ifdef SMARTCACHE
                    ht_add_val(ht_metadata_sectors,pack_info_get_y(packinfo), pack_info_xyz(jiffies_to_jsecs(get_jiffies()),SMARTCACHE_SIZE+1,METADATA_IOCLASS));
#else
                    ht_add_val(ht_metadata_sectors,pack_info_get_y(packinfo), pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,METADATA_IOCLASS) );
#endif
                }
                ht_destroy(ht_temp);
            }else{
                spin_unlock_irqrestore(&read_coll_lock,flags);
                dprinte(1,"ERROR: hash table not found for collisions in read checksum %lx \n", key);
            }
        }else{
#ifdef SMARTCACHE
            ht_add_val(ht_metadata_sectors,pack_info_get_y(val), pack_info_xyz(jiffies_to_jsecs(get_jiffies()),SMARTCACHE_SIZE+1,METADATA_IOCLASS));
#else
            ht_add_val(ht_metadata_sectors,pack_info_get_y(val), pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,METADATA_IOCLASS));
#endif
        }
    }
#endif
}

void small_meta_cleaner_process_fn(hash_table* ht, unsigned long key, unsigned long val){
#ifdef JADUKATA_CHECKSUM
#if (defined(SMARTCACHE) || defined(BCACHE))
    extern unsigned long DISKDRIVER_SIZE; 
    if(key > DISKDRIVER_SIZE){
        dprinte(1, "ERROR sector outside disk boundary sector %lu boundary %lu \n", key ,DISKDRIVER_SIZE);
    }
    if(cache_smart_mode >=0){
        //only for bcache
        //later for smartcache
#ifdef BCACHE
        extern diskdriver_dev diskdriver_device;
        extern int bcache_update_priority(struct block_device*, unsigned long sector, uint16_t ioclass);
        //z stores io class metadata is class 5
        int ioclass = pack_info_get_z(val);
        if(ioclass){
            int ret_size = bcache_update_priority(diskdriver_device.s_dev[0],key,ioclass);
            unsigned int size = ret_size<0?-ret_size:ret_size;
            unsigned int i =0, count = 0;
            for(i=1;i<size;i++){
                if(htn_remove(ht,key+i) != -1){
                    count++;
                }else{
                    break;
                }
            }
            if(ret_size>0){
                count++;
                dprintk(1,"updated priority sector %lu ioclass %d size %d\n",key, ioclass, count);
            }else{
                dprintk(1,"failed update priority sector %lu ioclass %d size %d\n",key, ioclass, ret_size );
            }
        }
#endif
    }
#endif
#endif
}

#endif

unsigned long previous_lba=0;
unsigned long previous_time;
struct timeval loadtime;

unsigned long rr=0,rw=0,sr=0,sw=0;
unsigned long dd=0,md=0;
unsigned long ddtime=0,mdtime=0;

void reset_stats(void){
    rr=0;
    rw=0;
    sr=0;
    sw=0;
    dd=0;
    md=0;
    ddtime=0;
    mdtime=0;

    atomic_set(&issued,0);
    atomic_set(&received,0);
#ifdef LIFETIMES
    {
        unsigned long key;
        while(ht_pop(ht_lifetimes,&key)!=-1);
        while(ht_pop(ht_series_lifetimes,&key)!=-1);
    }
#endif
}

#ifdef LIFETIMES
void get_min_max_sum(hash_table* table, unsigned long* min, unsigned long* max, unsigned long* sum){
    unsigned long key, val;
    *min = ULONG_MAX; *max =0; *sum =0;
    ht_open_scan(table);
    while(ht_scan_val(table, &key,&val) != -1){
        if(key < *min) *min = key;
        if(key > *max) *max = key;
        (*sum) = (*sum) + val;
    }
    if(*min == ULONG_MAX) *min = 0;
    if(*sum == 0) *sum = 1;
    ht_close_scan(table);
}


void add_to_series(hash_table* table, unsigned long val, unsigned long units){
    dprintk(1,"add_to_series: htname %s val %lu units %lu \n", table->name, val ,units);
    ht_add_sub_val(table,val/units,1);
}

void flush_lifetimes(void){
    unsigned long key, val, count = 0;
    //10000 seconds in future to denote non overwritten blocks
    unsigned long j = get_jiffies() + msecs_to_jiffies(10000*1000);
    while(ht_pop_val(ht_lifetimes,&key, &val) != -1){
        add_to_series(ht_series_lifetimes,jiffies_to_msecs( j - jsecs_to_jiffies(val)),LIFETIME_SERIES_UNIT_MSECS);
        count++;
        if(count %200 == 0){
            msleep(50);
        }
    }
}

inline char* divhelper(char* buff, unsigned long a, unsigned long b){
    if(b == 0) b = 1;
    sprintf(buff,"%3ld.%05ld",a/b,((a%b)*100000)/b);
    return buff;
}       
#endif

char* print_lifetime_stats(char* output){
    char* data = output;
#ifdef LIFETIMES
    unsigned long i = 0, key, val = 0;
    unsigned long min,max,sum,tsum=0;
    char buff[200];
    flush_lifetimes();
    data += sprintf(data,"\ndiskdriver block lifetimes:\n");
    get_min_max_sum(ht_series_lifetimes, &min, &max, &sum);
    dprintk(1,"\nprint_lifetime_stats: min %lu max %lu sum %lu\n",min,max,sum);

    for(i=min;i<=max;i++){
        val = 0;
        ht_lookup_val(ht_series_lifetimes,i,&val);
        if( (val*100*100000) >= sum){
            data += sprintf(data,"%6ld:%s;\n",i,divhelper(buff,(val*100),sum));
            tsum += val*100;
        }
    }
    data += sprintf(data,"\ntotal percentage: %s\n",divhelper(buff,tsum,sum));
    while(ht_pop(ht_series_lifetimes, &key) != -1);
#endif
    return data;
}

int print_stats(char* msg){
    char *output = msg;
    unsigned long tsum = 0;

#ifdef SMARTCACHE
    print_cache_stats(&output);
#endif

    tsum = rr + rw + sr + sw;
    
    output+= sprintf(output,"spread : %lu rr %lu - %lu rw %lu - %lu sr %lu - %lu sw %lu - %lu ; r %lu - %lu w %lu - %lu\n",tsum,rr,tsum?(rr*100)/tsum:0,rw,tsum?(rw*100)/tsum:0,sr,tsum?(sr*100)/tsum:0,sw,tsum?(sw*100)/tsum:0,(rr+sr),tsum?((rr+sr)*100)/tsum:0,(rw+sw),tsum?((rw+sw)*100)/tsum:0);

    if(dd+md > 0){
        dd = dd/DISKDRIVER_HARDSECT;
        md = md/DISKDRIVER_HARDSECT;

        output+= sprintf(output,"meta(data) : dd %lu - %lu md %lu - %lu \n",dd,(dd*100)/(dd+md),md,(md*100)/(dd+md));
        output+= sprintf(output,"meta(data)time : ddtime %lu - %lu mdtime %lu - %lu ddatimeavgpersector - %lu mdtimeavgpersector - %lu\n",ddtime,(ddtime*100)/(ddtime+mdtime),mdtime,(mdtime*100)/(ddtime+mdtime),dd?ddtime/dd:0,md?mdtime/md:0);
    }

    output = print_lifetime_stats(output);

    dprinte(1,"%s", msg);

    reset_stats();

    return (output-msg);
}

#define INTEGERHASHFN integerhashfn2

unsigned long nohashfn(KEYDT key){
    return (unsigned long)key;
}

unsigned long integerhashfn1(KEYDT key){
    key = key*GOLDEN_RATIO_PRIME;
    key -= (key<<6);
    key ^= (key>>17);
    key -= (key<<9);
    key ^= (key<<4);
    key -= (key<<3);
    key ^= (key<<10);
    key ^= (key>>15);
    return key;
}

unsigned long integerhashfn2(KEYDT key){
    return (key*GOLDEN_RATIO_PRIME) >> 16;
}
    
void handler_init(void){

    handler_pool = kmem_cache_create("diskdriver-handler-pool", sizeof(handler), 0, SLAB_HWCACHE_ALIGN,NULL);
    if(handler_pool == NULL){
        dpanic("Error : diskdriver_handler could not allocate memory");
    }
    do_gettimeofday(&loadtime);
    previous_time = conv(loadtime);
    
#ifdef LIFETIMES
    if(ht_lifetimes != NULL){
        ht_destroy(ht_lifetimes);
    }
    ht_create_with_size(&ht_lifetimes,"lifetimes",500000);
    
    if(ht_series_lifetimes != NULL){
        ht_destroy(ht_series_lifetimes);
    }
    ht_create_with_size(&ht_series_lifetimes,"serieslifetimes",500000);
#endif

#if defined(SMARTCACHE) || defined(BCACHE)
    if(ht_metadata_sectors != NULL){
        ht_destroy(ht_metadata_sectors);
    }
    //ht_create_with_customhash(&ht_metadata_sectors,"metadatasecs",500000,INTEGERHASHFN);
    ht_create_with_size(&ht_metadata_sectors,"metadatasecs",500000);
    
    if(ht_data_smallfile_sectors != NULL){
        ht_destroy(ht_data_smallfile_sectors);
    }
    //ht_create_with_customhash(&ht_data_smallfile_sectors,"datasmflsecs",500000,INTEGERHASHFN);
    ht_create_with_size(&ht_data_smallfile_sectors,"datasmflsecs",500000);
    
#ifdef DETECT_OVERWRITES
    if(ht_file_chunksums != NULL){
        ht_destroy(ht_file_chunksums);
    }
    ht_create_with_size(&ht_file_chunksums,"fchksums",500000);
    //ht_set_debug(ht_file_chunksums);
#endif

#ifdef SMARTCACHE    
    if(ht_free_smartslots != NULL){
        ht_destroy(ht_free_smartslots);
    }
    ht_create_with_customhash(&ht_free_smartslots,"freeslots",1000,INTEGERHASHFN);

    //ht_set_debug(ht_free_smartslots);

    if(ht_tofree_smartslots != NULL){
        ht_destroy(ht_tofree_smartslots);
    }
    ht_create_with_size(&ht_tofree_smartslots,"tofreeslots",500);

#ifdef TESTMODE
    if(ht_test_free_smartslots != NULL){
        ht_destroy(ht_test_free_smartslots);
    }
    ht_create_with_size(&ht_test_free_smartslots,"testfreeslots",500);
#endif
    if(ht_smartcache != NULL){
        ht_destroy(ht_smartcache);
    }
    ht_create_with_size(&ht_smartcache,"smartcache",10000);

#ifdef EXTCACHE
    if(ht_diskcache != NULL){
        ht_destroy(ht_diskcache);
    }
    ht_create_with_size(&ht_diskcache,"diskcache",10000);

#endif
#endif
#endif

#ifdef JADUKATA    

#ifndef DEDUP_ENABLED
    if(ht_cleaners != NULL){
        ht_destroy(ht_cleaners);
    }
    ht_create_with_size(&ht_cleaners,"cleaners",100);
#endif

    if(ht_data_checksums_read != NULL){
        ht_destroy(ht_data_checksums_read);
    }
    ht_create_with_size(&ht_data_checksums_read,"data_chkr",500000);
    
    if(ht_data_checksums_read_coll != NULL){
        ht_destroy(ht_data_checksums_read_coll);
    }
    ht_create_with_size(&ht_data_checksums_read_coll,"data_rd_coll",10000);

    //ht_set_debug(ht_data_checksums_read);
    
    if(ht_data_checksums_write != NULL){
        ht_destroy(ht_data_checksums_write);
    }
    ht_create_with_size(&ht_data_checksums_write,"data_chkw",100000);

    //ht_set_debug(ht_data_checksums_write);
    
    if(ht_data_hpas != NULL){
        ht_destroy(ht_data_hpas);
    }
    ht_create_with_size(&ht_data_hpas,"data_hpas",10000);

#ifdef DEDUP_WAR_PREFETCH
    if(ht_dedup_war_prefetch_detect != NULL){
        ht_destroy(ht_dedup_war_prefetch_detect);
    }
    ht_create_with_size(&ht_dedup_war_prefetch_detect,"war_prefetch",50000);
#endif

#ifdef JADUKATA_CHECKSUM
    read_cleaner = cleanup_register(ht_data_checksums_read, read_cleaner_process_fn, READ_CLEANER_MSECS, 500);
    write_cleaner = cleanup_register(ht_data_checksums_write, NULL,WRITE_CLEANER_MSECS,500);
    
#if defined(SMARTCACHE) || defined(BCACHE)
    //3 seconds so that for writes the data can get into the bcache and use get_priority to find ioclass
    small_cleaner = cleanup_register(ht_data_smallfile_sectors, small_meta_cleaner_process_fn,SMALLFILE_CLEANER_MSECS, 10000);
    meta_cleaner = cleanup_register(ht_metadata_sectors, small_meta_cleaner_process_fn, METADATA_CLEANER_MSECS, 10000);
    small_cleaner->might_sleep = true;
    meta_cleaner->might_sleep = true;

#endif
#endif
    
#ifdef JADUKATA_CHECKSUM
#ifdef DETECT_OVERWRITES
    //fhash_cleaner = cleanup_register(ht_file_chunksums, NULL,2000,50);
    fhash_cleaner = cleanup_register(ht_file_chunksums, NULL,FILE_CHUNKSUMS_CLEANER_MSECS,50000);
    //just to disallow cleaning by i/o handlers but only by the scheduled cleanup
    fhash_cleaner->might_sleep = true;
#endif
#endif
    
#ifdef DEDUP_WAR_PREFETCH
    dedup_war_prefetch_detect_cleaner = cleanup_register(ht_dedup_war_prefetch_detect, NULL,WARPREFETCH_CLEANER_MSECS,100);
#endif

#endif

}

void handler_cleanup(void){
#ifdef TEST_MODE
    int chksize;
#endif
    kmem_cache_destroy(handler_pool);

#ifdef JADUKATA
#ifdef JADUKATA_CHECKSUM
    cleanup_unregister(write_cleaner);
    write_cleaner = NULL;
    cleanup_unregister(read_cleaner);
    read_cleaner = NULL;
#ifdef DETECT_OVERWRITES
    cleanup_unregister(fhash_cleaner);
    fhash_cleaner = NULL;
#endif
#ifdef DEDUP_WAR_PREFETCH
    cleanup_unregister(dedup_war_prefetch_detect_cleaner);
    dedup_war_prefetch_detect_cleaner = NULL;
#endif

#if defined(SMARTCACHE) || defined(BCACHE)
    cleanup_unregister(small_cleaner);
    small_cleaner = NULL;
    cleanup_unregister(meta_cleaner);
    meta_cleaner = NULL;
#endif
#endif
#endif

#if defined(SMARTCACHE) || defined(BCACHE)
    if(ht_metadata_sectors != NULL){
        ht_destroy(ht_metadata_sectors);
        ht_metadata_sectors = NULL;
    }
    if(ht_data_smallfile_sectors != NULL){
        ht_destroy(ht_data_smallfile_sectors);
        ht_data_smallfile_sectors = NULL;
    }

#ifdef DETECT_OVERWRITES
    if(ht_file_chunksums != NULL){
        ht_destroy(ht_file_chunksums);
        ht_file_chunksums = NULL;
    }
#endif

#ifdef SMARTCACHE
    if(ht_free_smartslots != NULL){
        ht_destroy(ht_free_smartslots);
        ht_free_smartslots = NULL;
    }
    if(ht_tofree_smartslots != NULL){
        ht_destroy(ht_tofree_smartslots);
        ht_tofree_smartslots = NULL;
    }
#ifdef TESTMODE
    if(ht_test_free_smartslots != NULL){
        ht_destroy(ht_test_free_smartslots);
        ht_test_free_smartslots = NULL;
    }
#endif
    if(ht_smartcache != NULL){
        ht_destroy(ht_smartcache);
        ht_smartcache = NULL;
    }
#ifdef EXTCACHE
    if(ht_diskcache != NULL){
        ht_destroy(ht_diskcache);
        ht_diskcache = NULL;
    }
#endif
#ifdef LIFETIMES
    if(ht_lifetimes != NULL){
        ht_destroy(ht_lifetimes);
        ht_lifetimes = NULL;
    }
    if(ht_series_lifetimes != NULL){
        ht_destroy(ht_series_lifetimes);
        ht_series_lifetimes = NULL;
    }
#endif
     
#endif
#endif

#ifdef JADUKATA   

#ifndef DEDUP_ENABLED
    if(ht_cleaners != NULL){
        ht_destroy(ht_cleaners);
        ht_cleaners = NULL;
    }
#endif

    if(ht_data_checksums_read != NULL){
        ht_destroy(ht_data_checksums_read);
        ht_data_checksums_read = NULL;
    }
    
    if(ht_data_checksums_read_coll != NULL){
        ht_destroy(ht_data_checksums_read_coll);
        ht_data_checksums_read_coll = NULL;
    }

#ifdef TEST_MODE
    chksize = ht_get_size(ht_data_checksums_write);
    if(chksize != 0){
        dprinte(1,"ERROR: data_checksums in write path is not empty %d\n",chksize);
    }
#endif
    if(ht_data_checksums_write != NULL){
        ht_destroy(ht_data_checksums_write);
        ht_data_checksums_write = NULL;
    }

    if(ht_data_hpas != NULL){
        ht_destroy(ht_data_hpas);
        ht_data_hpas = NULL;
    }
#endif
        
}
    
int print_metadata_stats(char* ptr){
    int metadata_size = 0, freeslots = 0, datasmfl_size = 0, war_prefetch_size = 0;
    int sm_size =0,dsk_size=0;
    unsigned long hits = 0;
    int checksums_size_r =0, checksums_size_rc = 0, checksums_size_w = 0, hpas_size =0, chunksums_size = 0, cleaners_size = 0;
    char* ptrorg = ptr;
#if defined(SMARTCACHE) || defined(BCACHE)
#ifdef SMARTCACHE
    extern int replace_slot;
    sm_size = ht_get_size(ht_smartcache);
#ifdef EXTCACHE
    dsk_size = ht_get_size(ht_diskcache);
#endif
    freeslots = ht_get_size(ht_free_smartslots);
    hits = smartcache_hits;
#else
    const int replace_slot = -1;
#endif
    cleaners_size = ht_get_size(ht_cleaners);
    metadata_size = ht_get_size(ht_metadata_sectors);
    datasmfl_size = ht_get_size(ht_data_smallfile_sectors);
#else
    const int replace_slot = -1;
#endif

#ifdef JADUKATA
    checksums_size_r = ht_get_size(ht_data_checksums_read);
    checksums_size_rc = ht_get_size(ht_data_checksums_read_coll);
    checksums_size_w = ht_get_size(ht_data_checksums_write);
    hpas_size = ht_get_size(ht_data_hpas);
#ifdef DETECT_OVERWRITES
    chunksums_size = ht_get_size(ht_file_chunksums);
#endif
#ifdef DEDUP_WAR_PREFETCH
    war_prefetch_size = ht_get_size(ht_dedup_war_prefetch_detect);
#endif
#endif
    
    #ifdef REMUS
    ptr += sprintf(ptr, "metadata stats checksums_read htsize %d checksums_read_coll %d checksums_write htsize %d fchunks htsize %d hpas htsize %d warprefetch htsize %d scache_size %d scache replace %d dskcache size %d md_secs htsize %d datasmfl_secs %d cleaners %d smart hits %lu free smartslots %d totbcachereads %d totbcachewrites %d totreads %d totwrites %d tjreads %d tjwrites %d tnjreads %d tnjwrites %d checkpoints %d ajreads %d ajwrites %d anjreads %d anjwrites %d remus chks %d avg_interval %d \n", checksums_size_r, checksums_size_rc, checksums_size_w, chunksums_size, hpas_size, war_prefetch_size,sm_size, replace_slot,dsk_size, metadata_size, datasmfl_size, cleaners_size, hits,freeslots, atomic_read(&stat_bcache_totr), atomic_read(&stat_bcache_totw), atomic_read(&stat_totr), atomic_read(&stat_totw), atomic_read(&stat_journal_totr), atomic_read(&stat_journal_totw), atomic_read(&stat_nonjournal_totr), atomic_read(&stat_nonjournal_totw), atomic_read(&num_checkpoints), atomic_read(&cum_mov_avg_journal_totr), atomic_read(&cum_mov_avg_journal_totw), atomic_read(&cum_mov_avg_nonjournal_totr), atomic_read(&cum_mov_avg_nonjournal_totw),remus_num_intervals,jiffies_to_msecs(remus_avg_interval)); 
    #else
    ptr += sprintf(ptr, "metadata stats checksums_read htsize %d checksums_read_coll %d checksums_write htsize %d chunks size %d hpas htsize %d warprefetch htsize %d scache size %d scache replace %d dskcache size %d md_secs htsize %d datasmfl_secs %d cleaners_size %d smart hits %lu free smartslots %d totbcachereads %d totbcachewrites %d totreads %d totwrites %d \n", checksums_size_r, checksums_size_rc, checksums_size_w, chunksums_size, hpas_size,war_prefetch_size,sm_size, replace_slot,dsk_size, metadata_size, datasmfl_size, cleaners_size, hits,freeslots, atomic_read(&stat_bcache_totr), atomic_read(&stat_bcache_totw), atomic_read(&stat_totr), atomic_read(&stat_totw)); 
    #endif
  
#ifdef REMUS
    remus_last_jiffies = 0;
    remus_avg_interval = 0;
    remus_num_intervals = 0;
#endif

    atomic_set(&stat_totr,0);
    atomic_set(&stat_totw,0);
    atomic_set(&stat_bcache_totr,0);
    atomic_set(&stat_bcache_totw,0);

    atomic_set(&stat_journal_totr,0);
    atomic_set(&stat_journal_totw,0);
    atomic_set(&stat_nonjournal_totr,0);
    atomic_set(&stat_nonjournal_totw,0);
    
#ifdef REMUS
    //temporary measurement for remus workloads
    atomic_set(&num_checkpoints,0);
    atomic_set(&curr_journal_totr,0);
    atomic_set(&curr_journal_totw,0);
    atomic_set(&curr_nonjournal_totr,0);
    atomic_set(&curr_nonjournal_totw,0);
    atomic_set(&cum_mov_avg_journal_totr,0);
    atomic_set(&cum_mov_avg_journal_totw,0);
    atomic_set(&cum_mov_avg_nonjournal_totr,0);
    atomic_set(&cum_mov_avg_nonjournal_totw,0);
#endif

#if defined(SMARTCACHE) 
    smartcache_hits = 0;
#endif    

#ifdef JADUKATA
    ptr += sprintf(ptr, "jadu_wmetadata %d jadu_wdata %d jadu_rmetadata %d jadu_rdata %d jadu_rsmf %d jadu_rbgf %d jadu_wsmf %d jadu_wbgf %d jadu_rioclass[0:%d,1:%d,2:%d,3:%d,4:%d,5:%d] jadu_wioclass[0:%d,1:%d,2:%d,3:%d,4:%d,5:%d]\n",atomic_read(&jadu_wmetadata),atomic_read(&jadu_wdata), atomic_read(&jadu_rmetadata),atomic_read(&jadu_rdata),atomic_read(&jadu_rsmf),atomic_read(&jadu_rbgf),atomic_read(&jadu_wsmf),atomic_read(&jadu_wbgf), atomic_read(&jadu_rioclass[0]),atomic_read(&jadu_rioclass[1]),atomic_read(&jadu_rioclass[2]),atomic_read(&jadu_rioclass[3]),atomic_read(&jadu_rioclass[4]),atomic_read(&jadu_rioclass[5]),atomic_read(&jadu_wioclass[0]),atomic_read(&jadu_wioclass[1]),atomic_read(&jadu_wioclass[2]),atomic_read(&jadu_wioclass[3]),atomic_read(&jadu_wioclass[4]),atomic_read(&jadu_wioclass[5]));
    atomic_set(&jadu_rsmf,0);
    atomic_set(&jadu_rbgf,0);
    atomic_set(&jadu_rioclass[0],0);
    atomic_set(&jadu_rioclass[1],0);
    atomic_set(&jadu_rioclass[2],0);
    atomic_set(&jadu_rioclass[3],0);
    atomic_set(&jadu_rioclass[4],0);
    atomic_set(&jadu_rioclass[5],0);
    atomic_set(&jadu_wsmf,0);
    atomic_set(&jadu_wioclass[0],0);
    atomic_set(&jadu_wioclass[1],0);
    atomic_set(&jadu_wioclass[2],0);
    atomic_set(&jadu_wioclass[3],0);
    atomic_set(&jadu_wioclass[4],0);
    atomic_set(&jadu_wioclass[5],0);
    atomic_set(&jadu_wbgf,0);
    atomic_set(&jadu_wmetadata,0);
    atomic_set(&jadu_wdata,0);
    atomic_set(&jadu_rmetadata,0);
    atomic_set(&jadu_rdata,0);
#endif
    dprinte(1,"%s\n",ptrorg);
    return (ptr-ptrorg);
        
}

#if defined(JADUKATA) || defined(JADUKATA_VMM_COMM)

#ifdef JADUKATA_VMM_COMM
extern int vmmcomm_started;
#endif

#ifdef DISKDRIVER_KERN_ASSIST
extern int cdata_start;
#endif

int process_checksum(void* bioptr, int chunknum, unsigned long checksum)
{
    struct bio* diskdriver_bio = (struct bio*)bioptr;
    short ioclass = 0; // only for printing purpose
    int ret = 0;
    int data = 0;
#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
    extern unsigned bcache_sb_offset;
    //adjust for bcache super block
    diskdriver_bio->bi_sector -= bcache_sb_offset; 
#endif
    dprintk(1,"process chunknum %d checksum %lx pid %d cmd %s sector %lu \n", chunknum, checksum, current->pid, current->comm, diskdriver_bio->bi_sector);
    if(checksum != 0){
#ifdef LOW
        if(bio_data_dir(diskdriver_bio) == WRITE){
            unsigned long packedinfo;

#if defined(LIFETIMES)
            unsigned long oldval;
            int sect = diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK);
            if(ht_lookup_val(ht_lifetimes,sect/8,&oldval) != 0){
                dprintk(1,"jiffies %lu get_jiffies %lu jsecs_to_jiffies(oldval) %lu jiffies %lu \n", jiffies, get_jiffies(), jsecs_to_jiffies(oldval),(get_jiffies() - jsecs_to_jiffies(oldval)));
                add_to_series(ht_series_lifetimes,jiffies_to_msecs(get_jiffies() - jsecs_to_jiffies(oldval)),LIFETIME_SERIES_UNIT_MSECS);
            }
            ht_update_val(ht_lifetimes,sect/8,jiffies_to_jsecs(get_jiffies()));
#endif

            if(ht_lookup_val(ht_data_checksums_write,checksum,&packedinfo) != 0){
                data = 1;
                if(pack_info_get_z(packedinfo)){
#if (defined(SMARTCACHE) || defined(BCACHE))
                    if(cache_smart_mode>=0){
                        int sect = diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK);
                        dprintk(1,"added %d to ht_smf \n", sect);
                        if(sect % SMALLFILE_DIVIDER == SMALLFILE_QUOTIENT){
                            ht_update_val(ht_data_smallfile_sectors,sect,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,pack_info_get_z(packedinfo)));
                        }
                    }
#endif
                    ioclass = pack_info_get_z(packedinfo);
                    atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wsmf);
                    ioclass = ((ioclass<0)?0:((ioclass>IOCLASS_MAX)?METADATA_IOCLASS:ioclass));
                    atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wioclass[ioclass]);
                }else{
                    atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wioclass[0]);
                    atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wbgf);
                }
                if(pack_info_get_x(packedinfo) == 0){
                    ht_remove(ht_data_checksums_write,checksum);
                }
                atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wdata);
            }else{
                unsigned long val = 0;
                if(ht_lookup_val(ht_data_hpas,page_to_phys(bio_page(diskdriver_bio)),&val) == 0){
                    ioclass = METADATA_IOCLASS;
#if (defined(SMARTCACHE) || defined(BCACHE))
                    if(cache_smart_mode>=0){
#ifdef SMARTCACHE
                        ht_add_val(ht_metadata_sectors,diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK), pack_info_xyz(jiffies_to_jsecs(get_jiffies()),SMARTCACHE_SIZE+1,ioclass));
#else
                        ht_add_val(ht_metadata_sectors,diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK), pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,ioclass));
#endif
                    }
#endif
                    atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wioclass[METADATA_IOCLASS]);
                    atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_wmetadata);
                }else{
#if (defined(SMARTCACHE) || defined(BCACHE))
                    if(cache_smart_mode>=0){
                        if(val){
                            int sect = diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK);
                            ioclass = val;
                            dprintk(1,"added %d to ht_smf \n", sect);
                            if(sect % SMALLFILE_DIVIDER == SMALLFILE_QUOTIENT){
                                ht_update_val(ht_data_smallfile_sectors,sect,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,ioclass));
                            }
                        }
                    }
#endif
                    data = 1;
                }
            }
        }else{
            //if this is mmaped data read, don't add so that later this would not end up into metadata_sectors after cleanup timeout
            unsigned long val = 0;
            if(ht_lookup_val(ht_data_hpas,page_to_phys(bio_page(diskdriver_bio)),&val) == 0){
                unsigned long oldpackinfo;
                //everyting other than mmapped data reads
                if(ht_lookup_val(ht_data_checksums_read, checksum, &oldpackinfo) != 0){
                    unsigned long flags;
                    bool del_tempnew = false;
                    hash_table *ht_temp = NULL;
                    hash_table *ht_tempnew = NULL;
                    //if there is collision z char is set to 1
                    ht_update_val(ht_data_checksums_read, checksum, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,1));
                    
                    if(ht_lookup_val(ht_data_checksums_read_coll, checksum, (unsigned long*)&ht_temp) == 0){
               newht_retry_again:
                        ht_create_with_size(&ht_tempnew, "ht_temp", 100);
                        if(pack_info_get_z(oldpackinfo) == 0){
                            ht_add(ht_tempnew, oldpackinfo);
                        }
                        ht_add(ht_tempnew, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK),0) );
                    }
                    spin_lock_irqsave(&read_coll_lock,flags);
                    if(ht_lookup_val(ht_data_checksums_read_coll, checksum, (unsigned long*)&ht_temp) == 0){
                        if(ht_tempnew != NULL){
                            ht_add_val(ht_data_checksums_read_coll,checksum,(unsigned long)ht_tempnew);
                        }else{
                            spin_unlock_irqrestore(&read_coll_lock,flags);
                            goto newht_retry_again;
                        }
                    }else{
                        del_tempnew = true;
                        if(pack_info_get_z(oldpackinfo) == 0){
                            ht_add(ht_temp, oldpackinfo);
                        }
                        ht_add(ht_temp, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK),0) );
                    }
                    spin_unlock_irqrestore(&read_coll_lock,flags);

                    if(del_tempnew == true && ht_tempnew != NULL){
                        ht_destroy(ht_tempnew);
                    }
                }else{
                    //printk(KERN_ERR " jiffies %ld jiffies_to_jsecs %u\n",jiffies, jiffies_to_jsecs(get_jiffies()));
                    ht_update_val(ht_data_checksums_read, checksum, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK),0) );
                }
#ifdef DEDUP_WAR_PREFETCH
                dedup_hashpbn_cache_insert(checksum,diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK));
#endif
            }else{
                //mmaped data reads
#if (defined(SMARTCACHE) || defined(BCACHE))
                if(cache_smart_mode>=0){
                    if(val){
                        int sect = diskdriver_bio->bi_sector+(chunknum*JADU_NR_HARDSECT_PER_BLK);
                        ioclass = val;
                        dprintk(1,"added %d to ht_smf \n", sect);
                        if(sect % SMALLFILE_DIVIDER == SMALLFILE_QUOTIENT){
                            ht_update_val(ht_data_smallfile_sectors,sect,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,ioclass));
                        }
                    }
                }
#endif
            }
        }
#endif
    }else{
        ret = 1;
    }

#ifdef JADUKATA_CHECKSUM
#ifdef LOW
        //add whatever has timed out in read path data checksums to metadata sectors ( mostly due to read paths )
        do_cleanup(read_cleaner);
        //call small meta cleaner in write path only
        //cannot be called in atomic/interrupt context
#if defined(SMARTCACHE) || defined(BCACHE)
        if(bio_data_dir(diskdriver_bio) == WRITE){
            do_cleanup(small_cleaner);
            do_cleanup(meta_cleaner);
        }
#endif
#endif
#endif

    dprintk(1," %d %s : diskdriver %lu %s checksums #%d : checksum %lx data.ioclass %d.%d\n", current->pid, current->comm, diskdriver_bio->bi_sector , bio_data_dir(diskdriver_bio) == WRITE ? "WRITE" : "READ",  chunknum, checksum,data,ioclass);

#if defined(JADUKATA) && defined(LOW) && defined(BCACHE)
    //adjust for bcache super block
    diskdriver_bio->bi_sector += bcache_sb_offset; 
#endif
    return ret;
}

int jadukata_process_bio(char* id, struct bio *diskdriver_bio){
    struct bio_vec *bv;
    int bv_index;
    int ret = 0;
    
    dprintk(1,"diskdriver %s pid %d cmd %s sector %lu sectors %d \n", id, current->pid, current->comm, diskdriver_bio->bi_sector, bio_sectors(diskdriver_bio));

#ifdef JADUKATA_CHECKSUM
    {
#define PNUM (96)
        int pairsnum = 0;
        int TPAIR_PAGES_ORDER = 0;
        int maxpairs = bio_segments(diskdriver_bio) + 1;
        struct pair tpairs[PNUM];
        struct pair *pairs = NULL;

        //if(bio_sectors(diskdriver_bio) < 32){
        //process only those that have enough data for calculating a checksum
        if(bio_sectors(diskdriver_bio) >= 1){
            int maxpairssize = 0;
            if(maxpairs > PNUM){
                maxpairssize = (maxpairs*sizeof(struct pair)) + 256;
                TPAIR_PAGES_ORDER = get_order(maxpairssize); 
                //pairs = (struct pair*)__get_free_pages(GFP_KERNEL,TPAIR_PAGES_ORDER);
                pairs = (struct pair*)myd_kmalloc(maxpairssize,GFP_ATOMIC);
            }else{
                pairs = tpairs;
                maxpairs = PNUM;
            }

            bio_for_each_segment(bv,diskdriver_bio,bv_index) {
                pairs[pairsnum].addr = (unsigned long)page_address(bv->bv_page) + bv->bv_offset;
                pairs[pairsnum].size = bv->bv_len;
                //dprinte(1,"diskdriver addr %lx size %u\n",pairs[pairsnum].addr,pairs[pairsnum].size);
                pairsnum++;
                if(pairsnum > maxpairs){
                    dump_stack();
                    dprinte(1,"diskdriver ERROR bio %s pid %d cmd %s sector %ld size %u block %lu rw %lu bdev %p segs %d vcnt %d idx %d bv_index %d \n", id, current->pid,current->comm, diskdriver_bio->bi_sector, bio_sectors(diskdriver_bio),DISKDRIVER_SECTOR_TO_BLOCK(diskdriver_bio->bi_sector), diskdriver_bio->bi_rw, diskdriver_bio->bi_bdev, bio_segments(diskdriver_bio), diskdriver_bio->bi_vcnt, diskdriver_bio->bi_idx, bv_index); \
                    dprinte(1," ERROR : more segments than expected in jadukata_process_bio pairsnum %d  maxpairs %d\n", pairsnum, maxpairs);
                }
            }

#ifdef JADUKATA_VERBOSE
            checksum_processor1(pairs, pairsnum);
#endif
            ret = checksum_processor2((void*)diskdriver_bio, pairs, pairsnum);

            if(maxpairs > PNUM){
                //free_pages((unsigned long)pairs,TPAIR_PAGES_ORDER);
                myd_kfree(pairs,maxpairssize);
            }
        }
    }
#endif

#ifdef JADUKATA_VMM_COMM
    //in high mode, add buffer pointers to vmmcomm
    if(vmmcomm_started){
        bio_for_each_segment(bv,diskdriver_bio,bv_index) {
#ifdef HIGH
            vmmcomm_add((unsigned long)page_to_phys(bv->bv_page));
#else
            int lookup1 = vmmcomm_remove((unsigned long)page_to_phys(bv->bv_page));
#ifdef JADUKATA_VMM_COMM_VERIFY
            int lookup2 = (ht_lookup(ht_metadata_sectors,diskdriver_bio->bi_sector) == 0);
            if(lookup1 != lookup2){
                dprinte(1,"ERROR vmmcomm verify mismatch biosec %lu lookup1 %d lookup2 %d\n", diskdriver_bio->bi_sector, lookup1, lookup2);
            }
#else
            if(lookup1 ==0){
                int i;
                //metadata
                for(i=0;i<(diskdriver_bio->bi_size/DISKDRIVER_HARDSECT);i++){
#if (defined(SMARTCACHE) || defined(BCACHE))
                    if(cache_smart_mode>=0){
#ifdef SMARTCACHE
                        ht_add_val(ht_metadata_sectors,diskdriver_bio->bi_sector+i, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),SMARTCACHE_SIZE+1,METADATA_IOCLASS));
#else
                        ht_add_val(ht_metadata_sectors,diskdriver_bio->bi_sector+i, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,METADATA_IOCLASS));
#endif
                    }
#endif
                }
                
            }
#endif
#endif
        }
    }
#endif

    return ret;
}
#endif

//dprinte(1,"diskdriver %s pid %d cmd %s sector %ld size %u block %lu rw %lu address %lx phys %llx private %lu \n", id, current->pid,current->comm, diskdriver_bio->bi_sector, bio_sectors(diskdriver_bio),DISKDRIVER_SECTOR_TO_BLOCK(diskdriver_bio->bi_sector), diskdriver_bio->bi_rw, (unsigned long)bio_data(diskdriver_bio), page_to_phys(bio_page(diskdriver_bio)),(unsigned long)diskdriver_bio->bi_private1); 
//void inline print_bio(char* id, struct bio *diskdriver_bio){

void print_trace(struct bio* diskdriver_bio){
    handler *t = (handler*)(diskdriver_bio->bi_private1);
    unsigned long diffa = diff(loadtime,t->issued);
    unsigned long diffb = diff(loadtime,t->received);
    if(diskdriver_bio != NULL){
        dprintk(1,"%ld %lu %u issued %lu received %lu outstanding %u lba_dist %u size %u inter_arrival %u latency %u queuetime %u pid %d name %s\n",diskdriver_bio->bi_sector-t->size,bio_data_dir(diskdriver_bio),t->seq_id, diffa, diffb, t->outstanding, t->lba_dist,t->size,t->inter_arrival, t->latency,t->queuetime,t->pid, t->name);
    }
}

void handler_startio(struct bio* diskdriver_bio){
    char premsg[100];
    char *premsgt = premsg;
    handler* t = NULL ;
#ifdef DISKDRIVER_KERN_ASSIST
    struct bio_vec * bv;
    int bv_index;
#endif

    t = (handler*)kmem_cache_alloc(handler_pool, GFP_ATOMIC);
    if(t == NULL){
        dpanic("Error : kmem cache alloc failed");
    }
    diskdriver_bio->bi_private1 = t;

    t->seq_id = atomic_inc_return(&issued);

    t->outstanding = issued.counter - received.counter ;

    do_gettimeofday(&t->issued);

    t->lba_dist = ABSDIFF(diskdriver_bio->bi_sector,previous_lba);
    t->size = diskdriver_bio->bi_size/DISKDRIVER_HARDSECT;
    t->inter_arrival = conv(t->issued) - previous_time;

    if(t->lba_dist > RANDOM_LBA_THRESHOLD){
        if(bio_data_dir(diskdriver_bio) == WRITE)
            rw+=t->size;
        else
            rr+=t->size;
    }else{
        if(bio_data_dir(diskdriver_bio) == WRITE)
            sw+=t->size;
        else
            sr+=t->size;
    }

    previous_time = conv(t->issued);
    previous_lba = diskdriver_bio->bi_sector;

    t->pid = current->pid;
    strncpy(t->name,current->comm,TASK_COMM_LEN);
    
    t->md = 0;
    t->dd = 0;

#ifdef DISKDRIVER_KERN_ASSIST
    if(cdata_start){
        bio_for_each_segment(bv,diskdriver_bio,bv_index) {
            void* addr = bv->bv_page;
            int lookup = cdata_lookup(addr);
            dprintk(1,"first diskdriver cdata %p %p source %d len %d \n",bv->bv_page,addr,lookup,bv->bv_len);
            if(!lookup){
                addr = page_address(bv->bv_page);
                lookup = cdata_lookup(addr);
            }
            //remove implicit tracing addr capture
            dprintk(1,"second diskdriver cdata %p %p source %d len %d \n",bv->bv_page,addr,lookup,bv->bv_len);
            if(lookup){
                t->dd += bv->bv_len;
            }else{
                t->md += bv->bv_len;
            }
            if(lookup){
#ifdef HIGH
#ifdef JADUKATA_VMM_COMM
                jadukata_process_bio("vmmcomm",diskdriver_bio);
#endif
#endif
                cdata_remove(addr);
            }
        }
        if(t->md != 0 && t->dd != 0 ){
            dprinte(1,"ERROR both md and dd md %d dd %d\n",t->md,t->dd);
        }
    }
    dd += t->dd;
    md += t->md;
#endif
    //testing stack analysis
    if(0){
    //if(tracing_enabled){
        premsgt += sprintf(premsgt,"%d %s - %p; ",t->pid,t->name,(void*)(t->checksum));
#ifdef DISKDRIVER_KERN_ASSIST
        premsgt += sprintf(premsgt,"%s",t->dd != 0 ? "data" : "meta");
#endif
        print_bio(premsg,diskdriver_bio); 
    }
}

void handler_finishio(struct bio* diskdriver_bio){

    handler* t = (handler*)diskdriver_bio->bi_private1;

    atomic_inc(&received);

    do_gettimeofday(&t->received);
    t->latency = (conv(t->received) - diskdriver_bio->ata_issued);
    t->queuetime = (diskdriver_bio->ata_issued - conv(t->issued));

#ifdef DISKDRIVER_KERN_ASSIST
    if(cdata_start){
        if(t->dd + t->md > 0){
            ddtime += ((t->latency)*t->dd)/(t->dd+t->md);
            mdtime += ((t->latency)*t->md)/(t->dd+t->md);
        }else{
            dprinte(1,"ERROR i/o request neither meta nor data %d %d \n",t->dd,t->md);
        }
    }
#endif

    //print_trace(diskdriver_bio);
    diskdriver_bio->bi_end_io = t->end_io;
    kmem_cache_free(handler_pool,t);
}
