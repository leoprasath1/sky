#include "includes.h"

#ifdef EXTCACHE

#include "diskcache.h"
#include "common.h"

extern diskdriver_dev diskdriver_device;
extern unsigned long DCACHE_SIZE;
unsigned long MAX_DISK_CACHE_SECTORS;
unsigned long REMAP_BITMAP_SIZE;

extern hash_table* ht_diskcache;
unsigned long* diskcache_bitmap = NULL;

/*
#define ADDR (*(volatile long *) addr)

int my_test_bit(int nr, const volatile unsigned long * addr)
{
    int oldbit;

    __asm__ __volatile__(
            "btl %2,%1\n\tsbbl %0,%0"
            :"=r" (oldbit)
            :"m" (ADDR),"Ir" (nr));
    return oldbit;
}

int my_test_and_set_bit(int nr, volatile unsigned long * addr)
{
    int oldbit;

    __asm__ __volatile__( LOCK_PREFIX
            "btsl %2,%1\n\tsbbl %0,%0"
            :"=r" (oldbit),"=m" (ADDR)
            :"Ir" (nr) : "memory");
    return oldbit;
}

int my_test_and_clear_bit(int nr, volatile unsigned long * addr)
{
    int oldbit;

    __asm__ __volatile__( LOCK_PREFIX
            "btrl %2,%1\n\tsbbl %0,%0"
            :"=r" (oldbit),"=m" (ADDR)
            :"Ir" (nr) : "memory");
    return oldbit;
}
*/

int diskdriver_cache_lookup(unsigned long sector, unsigned long* cache_sector){
    int enable_logging = 0;
    
    if(ht_lookup_val(ht_diskcache, sector, cache_sector)){
        if(enable_logging) dprintk(1, "Remap entry exists key: %lu val: %lu\n",sector,*cache_sector);
        return 1;
    }
    return 0;
}

void diskdriver_cache_read(struct bio* diskdriver_bio){
	unsigned long cache_sector;
    if(diskdriver_cache_lookup(diskdriver_bio->bi_sector,&cache_sector) != 0){
		diskdriver_bio->bi_sector = cache_sector;
        diskdriver_bio->bi_bdev = diskdriver_device.c_dev;
    }else{
		dprinte(1,"ERROR sector %ld not in disk cache \n", diskdriver_bio->bi_sector);
	}
	return;
}

static DEFINE_SPINLOCK(diskcache_bitmap_alloc_lock);

int diskcache_bitmap_weight(void){
    int weight = 0;
    spin_lock(&diskcache_bitmap_alloc_lock);
    weight = bitmap_weight((void*)diskcache_bitmap, REMAP_BITMAP_SIZE*8);
    spin_unlock(&diskcache_bitmap_alloc_lock);
    return weight;
}

int diskdriver_set_diskcache_bitmap(int sectornr, int length) {
    int ret, i=0;
    dprintk(1, "S %d %d %p\n ", sectornr, length,diskcache_bitmap);
#ifdef TESTMODE
    if((sectornr+length-1) > MAX_DISK_CACHE_SECTORS){
        dprinte(1,"ERROR : beyond bitmap size %d \n",sectornr);
    }
#endif
    for (i= 0; i< length; i++) {
        ret = test_and_set_bit(sectornr+i, (unsigned long*)diskcache_bitmap);
        if(unlikely(ret)) {
            dprinte(1,"ERROR: Trying to set an allocated bit %d for  bit %d length %d \n", ret, sectornr+i, length);
            return -1;
        }
    }
    return 0;
}

int diskdriver_clear_diskcache_bitmap(int sectornr, int length) {
    int ret,i=0;
    dprintk(1, "S %d %d %p\n ", sectornr, length,diskcache_bitmap);
#ifdef TESTMODE
    if((sectornr+length-1) > MAX_DISK_CACHE_SECTORS){
        dprinte(1,"ERROR : beyond bitmap size %d \n",sectornr);
    }
#endif
    for (i= 0; i< length; i++) {
        ret = test_and_clear_bit(sectornr, (unsigned long*)diskcache_bitmap);
        if(unlikely(!ret)) {
            dprinte(1,"ERROR: Trying to unset an unallocated bit %d for  bit %d length %d \n", ret, sectornr, length);
            return -1;
        }
    }
    return 0;
}

int possible_fab = -1;
int find_first_available_sector(int sectors){
    int fab;
    //find unset bits
    if((sectors == 1) && (possible_fab != -1) && !test_bit(possible_fab, (void*)diskcache_bitmap)){
        fab = possible_fab++;
    }else{
        dprintk(1,"numblock %d order %d\n",sectors,fls(sectors));

        fab = bitmap_find_next_zero_area( (void*)diskcache_bitmap, REMAP_BITMAP_SIZE*8 , (possible_fab >=0 ? possible_fab : 0) , sectors, 0);
        if(fab > MAX_DISK_CACHE_SECTORS){
            fab = bitmap_find_next_zero_area( (void*)diskcache_bitmap, REMAP_BITMAP_SIZE*8 , 0 , sectors, 0);
        }
        if(fab > (REMAP_BITMAP_SIZE*8))
            dprinte(1,"ERROR fab %d outside size %lu possible fab %d \n",fab,(REMAP_BITMAP_SIZE*8),possible_fab);
        possible_fab = fab + sectors;
    }
    //set the bits
    if(sectors > 1){
        bitmap_set((void*)diskcache_bitmap, fab, sectors);
    }else{
        diskdriver_set_diskcache_bitmap(fab, sectors);
    }
    dprintk(1,"find_first_available_block : sectors %d fab %d \n", sectors, fab);
    return fab;
}


/* Bitmaps of blocks returns first free block -- forget range allocation for now 
   the bitmap includes the journal blocks as well .. just a simple list from 0 to SBA_SIZE
   just accepts a alloc of one block for now .. maybe hints later on
   Take care of atomic ops and locks and such when reading/writing to bitmap*/
int return_first_available_sector(int sectors){
	int first_available_sector, num_times=0;

    spin_lock(&diskcache_bitmap_alloc_lock);

    first_available_sector = find_first_available_sector(sectors);

    if(first_available_sector >= 0) {
		dprintk(1, "First available block = %d times = %d sectors %d \n", \
				first_available_sector, num_times, sectors);
            
		num_times+= sectors;
	}
	else {
		dprinte(1, "ERROR: No available bit? !!\n");
        first_available_sector = -1;
	}

	if(first_available_sector > MAX_DISK_CACHE_SECTORS) {
		dprinte(1, "ERROR: Trying to remap outside device: %d max %lu\n", first_available_sector, MAX_DISK_CACHE_SECTORS);
		// remap fails
		first_available_sector = -1;
	}

    spin_unlock(&diskcache_bitmap_alloc_lock);
   
	return first_available_sector;
}

void diskdriver_cache_free(unsigned long sector, unsigned long cache_sector){
    ht_remove(ht_diskcache,sector);
    diskdriver_clear_diskcache_bitmap(cache_sector,1);
}

void diskdriver_cache_confirm(unsigned long sector, unsigned long cache_sector){
    ht_add_val(ht_diskcache, sector, cache_sector);
}
        
//TODO: improve replacement policy
void diskdriver_cache_makespace(void){
    int i =0;
    unsigned long orig_sector, cache_sector;
    ht_open_scan(ht_diskcache);
    while( ht_scan_val(ht_diskcache,&orig_sector, &cache_sector) != -1 ){
        diskdriver_cache_free(orig_sector,cache_sector);
        i++;
        if(i>(1024*1024)){
            break;
        }
    }
    ht_close_scan(ht_diskcache);
}

int diskdriver_cache_write(struct bio* diskdriver_bio){
	unsigned long remap_sector = diskdriver_bio->bi_sector;
	unsigned long first_available_sector = MAX_DISK_CACHE_SECTORS + 2 ;
    int sectors = bio_sectors(diskdriver_bio);

    if(sectors != 1){
        dprinte(1,"ERROR more than one sector in i/o to disk cache\n");
    }

    diskdriver_cache_lookup(remap_sector,&first_available_sector);
    if(first_available_sector <= MAX_DISK_CACHE_SECTORS) {
		dprintk(1, "Checking remap table if block %lu is already remapped yes remapped to %lu\n", remap_sector, first_available_sector);
        diskdriver_bio->bi_sector = first_available_sector;
        return 0;
    }

	dprintk(1, "Asked for remap of %d blocks starting %lu\n", sectors, remap_sector);
	first_available_sector = return_first_available_sector(sectors);  

	if(first_available_sector >= 0 ) {
		dprintk(1, "Remap Success: adding %lu to %lu RW: %ld\n", \
				remap_sector, first_available_sector, diskdriver_bio->bi_rw);
		diskdriver_bio->bi_sector = first_available_sector;
        return 0;
	} 
	else {
        diskdriver_cache_makespace();
		dprinte(1, "ERROR: Remap Fails %lu for %lu\n", first_available_sector, remap_sector);
        return -1;
	}
    return -1;
}

int diskdriver_init_bitmaps(void) {
    MAX_DISK_CACHE_SECTORS = DCACHE_SIZE;
    REMAP_BITMAP_SIZE = ((MAX_DISK_CACHE_SECTORS >> 3) + 8);

    // this is 4096 bytes = 4096*8 = 32768 bits = 128 MB worth of metadata blocks
    //diskcache_bitmap  = myd_kmalloc(REMAP_BITMAP_SIZE, GFP_KERNEL); 
    diskcache_bitmap  = (unsigned long *) myd_vzalloc(REMAP_BITMAP_SIZE); 
    if(!diskcache_bitmap) {
        dprinte(1, "*********************ERROR: alloc of diskcache_bitmap failed! setting remap_logic to 0 \n");
        dpanic("*********************ERROR: alloc of diskcache_bitmap failed! setting remap_logic to 0 \n");
        //todo turn off remapping
        return -1;
    }
    //memset(diskcache_bitmap, 0 , REMAP_BITMAP_SIZE);
    dprintk(1, "Initializing diskcache_bitmap 0x%p %lu \n", diskcache_bitmap, REMAP_BITMAP_SIZE);
    return 1;
}

int diskdriver_destroy_bitmaps(void) {
    //kfree(diskcache_bitmap);
    if(diskcache_bitmap) {
        myd_vfree((void*)diskcache_bitmap,REMAP_BITMAP_SIZE);
        dprintk(1, "Success: Trying to free a valid remap bitmap\n"); 
        return 1;
    }
    else {
        dprinte(1, "ERROR: Trying to free a NULL remap bitmap\n"); 
        return 0;
    }
}

int dump_diskcache_bitmap(int size) {
    int i,set;
    dprintk(1, "Dump contents of the bitmap:\n");
    dprintk(1,"Remap bitmap weight diskcache_bitmap_weight %d \n", diskcache_bitmap_weight());
    
    for (i =0 ; i < size; i++) {
        set = test_bit(i, diskcache_bitmap);
        //dprintk(1, "%d ", set >=0?set:-set);
        dprintk(1, "bit %d: %d\n", i, set >=0?set:-set);
    }
    return 1;
}

void print_diskcache_bitmap_weight(void){
    dprintk(1,"Remap bitmap weight diskcache_bitmap_weight %d \n", diskcache_bitmap_weight());
}

#endif
