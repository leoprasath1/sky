#include "includes.h"
#include "ioctl.h"
#include "handler.h"

#define DEVICE_NAME 	"DISKDRIVER"

#define DISKDRIVER_MAJOR		(0)
#define DISKDRIVER_PHY_DEVS	(1)	/*these are the # underlying phy devices*/

/* fwd declarations */
void diskdriver_release(struct gendisk* gdisk, fmode_t mode);
int diskdriver_open(struct block_device *bdev, fmode_t mode);
int diskdriver_ioctl(struct block_device *blkdev, fmode_t mode,
        unsigned int cmd, unsigned long arg);
int diskdriver_check_change(struct gendisk *gd);
int diskdriver_revalidate(struct gendisk *gd);
int diskdriver_get_f_device_number(void);
int diskdriver_get_device_number(void);
bio_end_io_t diskdriver_end_io;

int is_reserved_data(struct bio* diskdriver_bio, char* p, int plen);

