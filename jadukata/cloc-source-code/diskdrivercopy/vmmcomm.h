
void vmmcomm_init_from_base(unsigned long a_vmmcomm_base);
void reset_vmmcomm(void);
unsigned long vmmcomm_get_basephy(void);
void vmmcomm_set_base(unsigned long base);
void vmmcomm_start(void);
void vmmcomm_stop(void);
void vmmcomm_init(void);
void vmmcomm_destroy(void);
void vmmcomm_add(unsigned long address);
int vmmcomm_remove(unsigned long address);
void vmmcomm_print(void);
