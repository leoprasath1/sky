#!/usr/bin/python -u

import os
import sys
import subprocess
import shlex
from time import sleep,time
import re
import sys
import datetime
import random
import shelve
import signal

def getmyprint(stmt):
    return "%s : %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M-%S"),stmt)
    
def myprint(stmt):
    print "%s" % getmyprint(stmt)

def localexec(cmd,sudo=1,ignore=0, log=1):
    if sudo == 1:
        cmd = "sudo " + cmd
    #myprint("running command " + cmd)
    out = ""
    err = ""
    ret = 0
    t1 = t2 = 0
    try:
        t1 = t2 = time()
        out = subprocess.check_output(shlex.split(cmd), stderr=subprocess.STDOUT)
        t2 = time()
    except subprocess.CalledProcessError as e:
        ret = e.returncode
        out = e.output
        if (ignore==0):
            err = "ERROR RETURN CODE"
    if(log):
        myprint('cmd %s : %s' % (cmd,out))
    sys.stdout.flush()
    return out


zero = 1
dd = 1
mkfs = 1

if(len(sys.argv) < 2):
    print "usage bcachemount.py mount(1)/umount(0)\n"
    exit(1)

mode = int(sys.argv[1])

if(len(sys.argv) > 2):
    zero = int(sys.argv[2])
if(len(sys.argv) > 3):
    dd = int(sys.argv[3])
if(len(sys.argv) > 4):
    mkfs = int(sys.argv[4])

if(mode == 0):
    print "bcachemount.py mode %d \n" %(mode)
else:
    print "bcachemount.py mode %d zero %s dd %s mkfs %s \n" % (mode, zero, dd,mkfs)

bcachetoolsdir = "/home/arulraj/cerny/jadukata/bcache/bcache-tools/"
SRCDIR="/home/leoa/cerny/jadukata/diskdriver/"

mountpoint = "/mnt/jadu"
blocksize="4096"
#blocksize="512"
#bucketsize="4096"
bucketsize="8192"
#bucketsize="131072"
#bucketsize="32768"
policy="lru"

writedelay=5
cachemode="writethrough"
#cachemode="writeback"

if(dd):
    diskdev = "/dev/DISKDRIVER0"
    diskdevblock = "DISKDRIVER0"
else:
    #diskdev = "/dev/sdb1"
    #diskdevblock = "sdb/sdb1"
    diskdev = "/dev/vdb"
    diskdevblock = "vdb"

#ssddev = "/dev/sdc4"
#ssddev = "/dev/sdd2"
#ssddev = "/dev/sdd1"
ssddev = "/dev/MEMSTORE0"
#ssddev = "/dev/sdc5"
qemuimgsize = "12G"

def getcachesetuuid():
    cachesetuuidout=localexec("ls /sys/fs/bcache/ ")
    for line in cachesetuuidout.splitlines():
       if(line.find("register") != -1):
          continue
       else:
           return line
    return "nonedevice"

def getcachedev():
    cachedevuuidout=localexec("ls /dev/",log=0)
    for line in cachedevuuidout.splitlines():
        if line.startswith("bcache"):
            return "/dev/" + line.strip();
    return "nonedevice"

def hi():
    config=localexec("grep 'define HIGH$' /home/arulraj/cerny/jadukata/diskdriver/includes.h").strip()
    return not config.startswith("//") 

#check macro 
def bcachedisabled():
    if not hi():
        config=localexec("grep 'define BCACHE$' /home/arulraj/cerny/jadukata/diskdriver/includes.h").strip()
        return config.startswith("//")
    return True

def mount():
    if(bcachedisabled()):
        mountdev = diskdev
    else:
        #recreate bcache disks and register devices
        localexec("%s/make-bcache -C %s -b %s -w %s --cache_replacement_policy=%s --wipe-bcache " %(bcachetoolsdir,ssddev,bucketsize,blocksize,policy));
        localexec("wipefs -a %s " %(diskdev));
        localexec("%s/make-bcache -B %s -b %s -w %s --cache_replacement_policy=%s --wipe-bcache " %(bcachetoolsdir,diskdev,bucketsize,blocksize,policy));
        localexec("bash -c 'echo %s > /sys/fs/bcache/register' " % (ssddev));
        localexec("bash -c 'echo %s > /sys/fs/bcache/register' "%(diskdev));
        
        #attach ssddev
        cachesetuuid = getcachesetuuid()
        localexec("bash -c 'echo %s > /sys/block/%s/bcache/attach' " %(cachesetuuid,diskdevblock));
        #writeback,fifo
        localexec("bash -c 'echo %s > /sys/block/%s/bcache/cache_mode'" %(cachemode,diskdevblock));
        localexec("bash -c 'echo %s > /sys/block/%s/bcache/cache/cache0/cache_replacement_policy'" %(policy,diskdevblock))
        mountdev = getcachedev()  
        localexec("bash -c 'echo 0 > /sys/block/%s/bcache/sequential_cutoff'" %(mountdev.split("/")[2]))
        localexec("bash -c 'echo 0 > /sys/block/%s/bcache/sequential_merge'" %(mountdev.split("/")[2]))
        localexec("bash -c 'echo 0 > /sys/block/%s/bcache/cache/congested_read_threshold_us'" %(mountdev.split("/")[2]))
        localexec("bash -c 'echo 0 > /sys/block/%s/bcache/cache/congested_write_threshold_us'" %(mountdev.split("/")[2]))
        localexec("bash -c 'echo %s > /sys/block/%s/bcache/writeback_delay' " %(writedelay,mountdev.split("/")[2]))
        localexec("cat /sys/block/%s/bcache/writeback_delay"%(mountdev.split("/")[2]))
        
    if(mkfs): 
        if not hi():
            localexec("echo starting_mkfs");
            localexec("mkfs.ext3 -F %s" % (mountdev));
            localexec("echo finished_mkfs");
        
            localexec("mount %s %s " % (mountdev,mountpoint));
            localexec("chmod -R 777 %s" %(mountpoint));
            
            localexec("sudo qemu-img create -f raw %s/disk.raw %s"%(mountpoint,qemuimgsize));
            
            if(zero):
                localexec("echo 'dd start' ")
                localexec("dd if=/dev/zero of=%s/disk.raw bs=4096 count=%d"%(mountpoint,(1048576*3)))
                localexec("echo 'dd sync' ")
                localexec("sync")
                localexec("sudo bash -c 'echo 3 > /proc/sys/vm/drop_caches'");
                localexec("echo 'dd done' ")
            
            localexec("sudo chmod 777 %s/disk.raw"%(mountpoint)) 

def umount():
    localexec("umount %s"%(mountpoint));
    localexec("umount %s" % (diskdev));
    localexec("umount %s" % (diskdev));
    localexec("umount %s" % (getcachedev()));
    
    localexec("rmmod kvm_intel");
    localexec("rmmod kvm")
    
    #detach and unregister ssd device
    #remove bcache dev
    cachesetuuid = getcachesetuuid();
    localexec("bash -c 'echo 1 > /sys/block/%s/bcache/detach' " %(diskdevblock));
    localexec("bash -c 'echo 1 > /sys/block/%s/bcache/stop' " %(diskdevblock));
    localexec("bash -c 'echo 1 > /sys/fs/bcache/%s/unregister' " % (cachesetuuid));
    
    sleep(5)
    
    localexec("wipefs -a %s " %(diskdev));
    while(localexec("rmmod DISKDRIVER").find("use") != -1):
        sleep(2);

if(mode):
    mount()
else:
    umount()

exit(0)

