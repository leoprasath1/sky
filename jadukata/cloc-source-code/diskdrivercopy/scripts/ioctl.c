#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include "../ioctl.h"

#define DEV "/dev/DISKDRIVER0"

int myioctl(int fd, int req, ...)
{
    int retries = 3;
    int num = 0;
    int ret = 1;
    void* arg;
    va_list args;
    int args_ioctl = 0;
    if(req == DISKDRIVER_GET_STATS || req == BTRFS || req == DISKDRIVER_MARKER || req == GETVMMCOMMBASE || req == GETCR3 || req == GETTASKPTR || req == IGNORE_CACHE || req == GET_CACHED_SECTORS || req == GET_CACHE_DISTRIBUTION || req == RESIZE_CACHE || req == CACHE_SMARTDUMB || req == DISKDRIVER_TRACE || req == READ_FILL || req == REMUS_IOCTL || req == UNIQUE_HINTS || req == TEST){
        args_ioctl = 1;
    }
    if(args_ioctl){
        va_start(args,req);
        arg = va_arg(args,void*);
        va_end(args);
    }

    do {
        num++;
        if(args_ioctl)
            ret = ioctl(fd, req, arg);
        else
            ret = ioctl(fd,req);

        if(ret)
            fprintf(stderr,"retrying failed ioctl %d error : %d %s\n",num,errno,strerror(errno));
    }while(ret!=0 && num <=retries);

    return ret;
}

int main(int argc, char *argv[])
{
        int fd;
        int ret = 1;
        unsigned long ioctl_long;

        if (argc < 2) {
                printf("Usage: ioctl <start|stop|trace|get_stats|reset_stats|btrfs_wkld_on|btrfs_wkld_off|diskdriver_marker|ignore_cache|resize_cache|get_cached_sectors|get_cache_distribution|getvmmcommbase pid|startvmmcomm|stopvmmcomm|gettaskptr|getcr3|remus|test>\n");
                return ret;
        }

        fd = open(DEV, O_RDONLY);

        if (strcmp(argv[1], "start") == 0) {
                fprintf(stderr, "Starting DISKDRIVER ...\n");
                ret = myioctl(fd, START_DISKDRIVER);
        }else if (strcmp(argv[1], "trace") == 0) {
                int level = 0;
                sscanf(argv[2],"%d",&level);
                fprintf(stderr, "Setting trace level to %d \n", level);
                ret = myioctl(fd, DISKDRIVER_TRACE, level);
        }else if (strcmp(argv[1], "readfill") == 0) {
                int yesno = 0;
                sscanf(argv[2],"%d",&yesno);
                fprintf(stderr, "Setting readfill to %d \n", yesno);
                ret = myioctl(fd, READ_FILL, yesno);
        } else if (strcmp(argv[1], "stop") == 0) {
                fprintf(stderr, "Stoping DISKDRIVER ...\n");
                ret = myioctl(fd, STOP_DISKDRIVER);
        } else if (strcmp(argv[1], "get_stats") == 0) {
                fprintf(stderr, "printing the statistics ...\n");
                char *buf = (char *)malloc(MAX_UBUF_SIZE);
                if (!buf) {
                        fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", MAX_UBUF_SIZE);
                        return 1;
                } else {
                    memset(buf, '\0', MAX_UBUF_SIZE);
                    ret = myioctl(fd, DISKDRIVER_GET_STATS, buf);
                    printf("%s\n", buf);
                }
                free(buf);
        } else if (strcmp(argv[1], "reset_stats") == 0) {
                fprintf(stderr, "resetting the statistics ...\n");
                ret = myioctl(fd, DISKDRIVER_RESET_STATS);
        } else if (strcmp(argv[1], "change_phase") == 0) {
                fprintf(stderr, "changing the phase ...\n");
                ret = myioctl(fd, CHANGE_PHASE);
        } else if (strcmp(argv[1], "btrfs_wkld_on") == 0) {
                ioctl_long = 1;
                fprintf(stderr, "enabling btrfs wkld ...\n");
                ret = myioctl(fd, BTRFS,&ioctl_long);
        } else if (strcmp(argv[1], "btrfs_wkld_off") == 0) {
                ioctl_long = 0;
                fprintf(stderr, "disabling btrfs wkld ...\n");
                ret = myioctl(fd, BTRFS,&ioctl_long);
        } else if (strcmp(argv[1], "diskdriver_marker") == 0) {
                char *buf = (char *)malloc(MAX_UBUF_SIZE);
                fprintf(stderr, "sending marker %s...\n",argv[2]);
                if (!buf) {
                        fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", MAX_UBUF_SIZE);
                        return 1;
                } else {
                    memset(buf, '\0', MAX_UBUF_SIZE);
                    strcpy(buf,argv[2]);
                    ret = myioctl(fd, DISKDRIVER_MARKER,buf);
                }
                free(buf);
        } else if (strcmp(argv[1], "ignore_cache") == 0) {
                char *buf = (char *)malloc(MAX_UBUF_SIZE);
                char *ptr = buf;
                int i;
                fprintf(stderr, "sending ignore_cache...\n");
                if (!buf) {
                        fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", MAX_UBUF_SIZE);
                        return 1;
                } else {
                    memset(buf, 0, MAX_UBUF_SIZE);
                    for(i=2;i<argc;i++){
                        ptr += sprintf(ptr,"%s ",argv[i]);
                    }
                    fprintf(stderr, "buf %s \n", buf);
                    
                    ret = myioctl(fd,IGNORE_CACHE,buf);
                }
                free(buf);
        } else if (strcmp(argv[1], "resize_cache") == 0) {
                unsigned long percent = 0;
                sscanf(argv[2],"%lu",&percent);
                fprintf(stderr, "resizing the cache to %lu percent ...\n", percent);
                ret = myioctl(fd, RESIZE_CACHE,percent);
        } else if (strcmp(argv[1], "remus") == 0) {
                int remus_interval = 0;
                sscanf(argv[2],"%d",&remus_interval);
                fprintf(stderr, "setting remus interval to %d ...\n", remus_interval);
                ret = myioctl(fd, REMUS_IOCTL, remus_interval);
        } else if (strcmp(argv[1], "cache_smartdumb") == 0) {
                int mode = 3;
                sscanf(argv[2],"%d",&mode);
                fprintf(stderr, "changing cache mode to %d ...\n", mode);
                ret = myioctl(fd, CACHE_SMARTDUMB,mode);
        } else if (strcmp(argv[1], "get_cached_sectors") == 0) {
                char *buf = (char *)malloc(MAX_UBUF_SIZE);
                fprintf(stderr, "sending get_cached_sectors...\n");
                if (!buf) {
                        fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", MAX_UBUF_SIZE);
                        return 1;
                } else {
                    memset(buf, 0, MAX_UBUF_SIZE);
                    ret = myioctl(fd,GET_CACHED_SECTORS,buf);
                    printf("%s\n", buf);
                }
                free(buf);
        } else if (strcmp(argv[1], "get_cache_distribution") == 0) {
                char *buf = (char *)malloc(MAX_UBUF_SIZE);
                fprintf(stderr, "sending get_cache_distribution..\n");
                if (!buf) {
                        fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", MAX_UBUF_SIZE);
                        return 1;
                } else {
                    memset(buf, 0, MAX_UBUF_SIZE);
                    ret = myioctl(fd,GET_CACHE_DISTRIBUTION,buf);
                    printf("%s\n", buf);
                }
                free(buf);
        }else if (strcmp(argv[1], "getvmmcommbase") == 0) {
            fprintf(stderr, "getting vmmcomm base\n");
            char *buf = (char *)malloc(1024);
            if (!buf) {
                fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", 1024);
                return 1;
            } else {
                memset(buf, '\0', 1024);
                sprintf(buf,"%s\n",argv[2]);
                ret = myioctl(fd, GETVMMCOMMBASE, buf);
                printf("%s\n", buf);
            }
            free(buf);
        }else if (strcmp(argv[1], "startvmmcomm") == 0) {
            fprintf(stderr, "starting vmmcomm ..\n");
            ret = myioctl(fd, STARTVMMCOMM);
        }else if (strcmp(argv[1], "stopvmmcomm") == 0) {
            fprintf(stderr, "stopping vmmcomm ..\n");
            ret = myioctl(fd, STOPVMMCOMM);
        }else if (strcmp(argv[1], "getcr3") == 0) {
            if(argc != 3){
                fprintf(stderr, "needs pid argument\n");
                return 1;
            }
            fprintf(stderr, "getting cr3 for pid %s...\n", argv[2]);
            char *buf = (char *)malloc(1024);
            if (!buf) {
                fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", 1024);
                return 1;
            } else {
                memset(buf, '\0', 1024);
                sprintf(buf,"%s\n",argv[2]);
                ret = myioctl(fd, GETCR3, buf);
                printf("%s\n", buf);
            }
            free(buf);
        }else if (strcmp(argv[1], "gettaskptr") == 0) {
            fprintf(stderr, "getting taskptrs...\n");
            char *buf = (char *)malloc(1024);
            if (!buf) {
                fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", 1024);
                return 1;
            } else {
                memset(buf, '\0', 1024);
                ret = myioctl(fd, GETTASKPTR, buf);
                printf("%s\n", buf);
            }
            free(buf);
        }else if (strcmp(argv[1], "unique_hints") == 0) {
            int level = 0;
            sscanf(argv[2],"%d",&level);
            fprintf(stderr, "setting unique_hints to %d\n", level);
            ret = myioctl(fd, UNIQUE_HINTS, level);
        }else if (strcmp(argv[1], "test") == 0) {
            fprintf(stderr, "test ioctl...\n");
            /*
            char *buf = (char *)malloc(1024);
            if (!buf) {
                fprintf(stderr, "unable to allocate  mem for user buffer %d bytes\n", 1024);
                return 1;
            } else {
                memset(buf, '\0', 1024);
                ret = myioctl(fd, TEST, buf);
                printf("%s\n", buf);
            }
            free(buf);
            */
                int level = 0;
                sscanf(argv[2],"%d",&level);
                ret = myioctl(fd, TEST, level);
        }else if(strcmp(argv[1], "dummy") == 0) {
            fprintf(stderr, "dummy ioctl...\n");
            ret = myioctl(fd, 1234);

        }else {
            fprintf(stderr, "Invalid command %s \n", argv[1]);
        }

        close(fd);

        return ret;
}
