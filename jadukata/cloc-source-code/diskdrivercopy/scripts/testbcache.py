#!/usr/bin/python -u

import os
import sys
import subprocess
import shlex
from time import sleep,time
import re
import sys
import datetime
import random
import shelve
import signal

bcachetoolsdir = "/home/arulraj/cerny/jadukata/bcache/bcache-tools/"
SRCDIR="/home/leoa/cerny/jadukata/diskdriver/"

mountpoint = "/mnt/jadu"
blocksize="512"
bucketsize="4096"
readcount=18000
writecount=35000
policy="lru"

diskdev = "/dev/DISKDRIVER0"
diskdevblock = "DISKDRIVER0"

#ssddev = "/dev/sdc4"
ssddev = "/dev/sdc5"

def getmyprint(stmt):
    return "%s : %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M-%S"),stmt)
    
def myprint(stmt):
    print "%s" % getmyprint(stmt)

def localexec(cmd,sudo=1,ignore=0, log=1):
    if sudo == 1:
        cmd = "sudo " + cmd
    #myprint("running command " + cmd)
    out = ""
    err = ""
    ret = 0
    t1 = t2 = 0
    try:
        t1 = t2 = time()
        out = subprocess.check_output(shlex.split(cmd), stderr=subprocess.STDOUT)
        t2 = time()
    except subprocess.CalledProcessError as e:
        ret = e.returncode
        out = e.output
        if (ignore==0):
            err = "ERROR RETURN CODE"
    if(log):
        myprint('cmd %s : %s' % (cmd,out))
    sys.stdout.flush()
    return out

def getcachesetuuid():
    cachesetuuidout=localexec("ls /sys/fs/bcache/ ")
    for line in cachesetuuidout.splitlines():
       if(line.find("register") != -1):
          continue
       else:
           return line
    return "nonedevice"

def getcachedev():
    cachedevuuidout=localexec("ls /dev/",log=0)
    for line in cachedevuuidout.splitlines():
        if line.startswith("bcache"):
            return "/dev/" + line.strip();
    return "nonedevice"

#check macro 
def bcachedisabled():
    config=localexec("grep 'define BCACHE$' /home/arulraj/cerny/jadukata/diskdriver/includes.h").strip()
    return config.startswith("//")

localexec("sudo bash -c 'echo 1 > /sys/block/bcache0/bcache/clear_wbq'")
localexec("sudo bash -c 'echo 1 > /sys/block/bcache0/bcache/reset_bcache'")
localexec("sudo bash -c 'echo 1 > /sys/block/bcache0/bcache/clear_stats'")
subprocess.call("tail -n +1 /sys/block/bcache0/bcache/stats_total/*",shell=True)
subprocess.call("tail -n +1 /sys/block/bcache0/bcache/dirty_data",shell=True)
subprocess.call("sudo /home/arulraj/cerny/jadukata/diskdriver/scripts/ioctl get_stats",shell=True)

localexec("dd if=/dev/urandom of=%s count=%s bs=4096 obs=4096 oflag=direct,sync,noatime iflag=fullblock"%(getcachedev(),writecount))

localexec("sudo bash -c 'echo 1 > /sys/block/bcache0/bcache/clear_wbq'")
subprocess.call("tail -n +1 /sys/block/bcache0/bcache/stats_total/*",shell=True)
subprocess.call("tail -n +1 /sys/block/bcache0/bcache/dirty_data",shell=True)
subprocess.call("sudo /home/arulraj/cerny/jadukata/diskdriver/scripts/ioctl get_stats",shell=True)
localexec("sudo bash -c 'echo 1 > /sys/block/bcache0/bcache/clear_stats'")

localexec("dd if=%s of=/dev/null count=%s bs=4096 ibs=4096 iflag=sync,fullblock,direct,sync"%(getcachedev(),readcount))
subprocess.call("sudo /home/arulraj/cerny/jadukata/diskdriver/scripts/ioctl get_stats",shell=True)

subprocess.call("tail -n +1 /sys/block/bcache0/bcache/stats_total/*",shell=True)
subprocess.call("tail -n +1 /sys/block/bcache0/bcache/dirty_data",shell=True)

exit(0)

