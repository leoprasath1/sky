#include <linux/module.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <linux/skbuff.h>

#include "checksum_processor.h"
#include "../include/linux/nitro.h"
#include "all_common.h"

extern bool ndbg;

extern short int nitro_shutdown;

extern atomic_t in_new_pid_mode;

#ifdef JADUKATA
extern bool new_process;
#endif

int jprint_enabled = 0;

int toggle_process_tracking(bool val){
#ifdef JADUKATA
    new_process = val;
#endif
    return 0;
}

int nitro_test(char* input){
    //int val;
    //sscanf(buf,"%d\n",&val);
    //toggle_process_tracking(val);
    return 0;
}

#ifdef JADUKATA
char* cleanup_stats(char* buff);
extern atomic_t debug_time;
#else
atomic_t debug_time;
#endif

int wkld_trace_command(void *bufptr);

int status_command(void* bufptr){
    char* buf = (char*)bufptr;
    char* data = (char*)bufptr;
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i;
    unsigned long total_scall=0, total_sret=0, total_scallm_time, total_sretm_time, total_scallu_time, total_sretu_time, total_scalli_time, total_sreti_time;

    wkld_trace_command(data);
    data+=strlen(data);
    
    jprinte("status command start: \n");

    list_for_each_entry(kvm, &vm_list, vm_list){
        total_scall = 0, total_sret = 0, total_scallm_time = 0, total_sretm_time = 0, total_scallu_time = 0, total_sretu_time = 0, total_scalli_time = 0, total_sreti_time = 0;
        kvm_for_each_vcpu(i, tvcpu, kvm){
            data += sprintf(data,"\nsyscall/sysret counts vcpu %d count scall - %lu, sret - %lu time scallm - %llu sretm - %llu scallu - %llu sretu - %llu scalli - %llu sreti - %llu ", tvcpu->vcpu_id, tvcpu->nitrodata.scall_count, tvcpu->nitrodata.sret_count, tvcpu->nitrodata.scallm_time, tvcpu->nitrodata.sretm_time, tvcpu->nitrodata.scallu_time, tvcpu->nitrodata.sretu_time, tvcpu->nitrodata.scalli_time, tvcpu->nitrodata.sreti_time);
#ifdef JADU_SKIP_SYSCALLS
            data += sprintf(data,"\nskipped syscall count %d ",tvcpu->nitrodata.skipped_syscalls);
#endif
            total_scall += tvcpu->nitrodata.scall_count;
            total_sret += tvcpu->nitrodata.sret_count;
            total_scallm_time += tvcpu->nitrodata.scallm_time;
            total_sretm_time += tvcpu->nitrodata.sretm_time;
            total_scallu_time += tvcpu->nitrodata.scallu_time;
            total_sretu_time += tvcpu->nitrodata.sretu_time;
            total_scalli_time += tvcpu->nitrodata.scalli_time;
            total_sreti_time += tvcpu->nitrodata.sreti_time;
        }
        data += sprintf(data,"\ntotal syscall/sysret counts %lu %lu time monitored(nsecs) %lu %lu time insights(nsecs) %lu %lu time unmonitored(nsecs) %lu %lu", total_scall, total_sret, total_scallm_time, total_sretm_time, total_scalli_time, total_sreti_time, total_scallu_time, total_sretu_time);
    }
    data += sprintf(data,"\ndebug time %d ", atomic_read(&debug_time));

    jprinte("%s\n",buf);
#ifdef JADUKATA
    buf = data;

    extern hash_table *ht_mmap_process;
    extern hash_table *ht_mmap_gpas;
    extern hash_table *ht_mmap_gpas_ptep;
    extern hash_table *ht_rw_gpas;
    extern hash_table *ht_rw_gpas_ptep;
    extern hash_table *ht_tracked_gpas;
    extern hash_table *ht_tracked_gfns;
    extern hash_table *ht_new_pids;
    extern hash_table *ht_file_sizes;
#ifdef DETECT_OVERWRITES
    extern hash_table *ht_file_chunksums;
#endif
    extern hash_table *ht_mon_pids;
    extern hash_table *ht_mon_process;
    extern hash_table *ht_splitcalls;
    extern hash_table *ht_file_eof_guess;
    extern hash_table *ht_cr3_to_pid;
    extern hash_table *ht_fork_parent_pids;
    extern hash_table *ht_exec_cr3s;
    extern hash_table *ht_new_process_tracking;
    extern hash_table *ht_sig_nu_stash;

#ifdef MEM_TRACE
    extern int sci_memory_used_max;
    extern atomic_t sci_memory_used;
    data += sprintf(data,"SCI dynamic memory stats: now = %d bytes peak = %d bytes\n\n", atomic_read(&sci_memory_used), sci_memory_used_max);
#endif
    
    data += sprintf(data,"mmap process size %d ", ht_get_size(ht_mmap_process));
    data += sprintf(data,"mon process size %d ", ht_get_size(ht_mon_process));
    data += sprintf(data,"mon pids size %d ", ht_get_size(ht_mon_pids));
    data += sprintf(data,"mmap gpas size %d ", ht_get_size(ht_mmap_gpas));
    data += sprintf(data,"mmap gpas ptep size %d ", ht_get_size(ht_mmap_gpas_ptep));
    data += sprintf(data,"rw gpas size %d ", ht_get_size(ht_rw_gpas));
    data += sprintf(data,"rw gpas ptep size %d ", ht_get_size(ht_rw_gpas_ptep));
    data += sprintf(data,"tracked gpas size %d ", ht_get_size(ht_tracked_gpas));
    data += sprintf(data,"tracked gfns size %d ", ht_get_size(ht_tracked_gfns));
    data += sprintf(data,"new pids size %d ", ht_get_size(ht_new_pids));
    data += sprintf(data,"for_parent_pids size %d ", ht_get_size(ht_fork_parent_pids));
    data += sprintf(data,"exec_cr3s size %d ", ht_get_size(ht_exec_cr3s));
    data += sprintf(data,"cr3_to_pid size %d ", ht_get_size(ht_cr3_to_pid));
    data += sprintf(data,"new process tracking size %d ", ht_get_size(ht_new_process_tracking));
    data += sprintf(data,"file sizes size %d ", ht_get_size(ht_file_sizes));
#ifdef DETECT_OVERWRITES
    data += sprintf(data,"file chunksums size %d ", ht_get_size(ht_file_chunksums));
#endif
    data += sprintf(data,"file eof guess size %d ", ht_get_size(ht_file_eof_guess));
    data += sprintf(data,"splitcalls size %d ", ht_get_size(ht_splitcalls));
    data += sprintf(data,"signustash size %d ", ht_get_size(ht_sig_nu_stash));
   
    jprinte("%s\n",buf);
    buf = data;

    data = cleanup_stats(data);

#ifdef SYS_STATS
    struct sysstats *ps = (struct sysstats*)my_kzalloc(sizeof(struct sysstats)*MAX_STATS, GFP_KERNEL);
    int j;
    //memset(ps,0,sizeof(struct sysstats)*MAX_STATS);
    for(i=0;i<MAX_STATS;i++){
        ps[i].mintime = INT_MAX;
    }
    for(i=0;i<MAX_STATS;i++){
       //jprintk("sysstats processing i%d \n", i);
       char* name = syscall_name(i);
       if(name != NULL){
           list_for_each_entry(kvm, &vm_list, vm_list){
               kvm_for_each_vcpu(j, tvcpu, kvm){
                   ps[i].count += tvcpu->nitrodata.ss[i].count;
                   ps[i].fkcount += tvcpu->nitrodata.ss[i].fkcount;
                   ps[i].timesecs += tvcpu->nitrodata.ss[i].timesecs;
                   ps[i].timeusecs += tvcpu->nitrodata.ss[i].timeusecs;
                   if(ps[i].maxtime < tvcpu->nitrodata.ss[i].maxtime){
                        ps[i].maxtime = tvcpu->nitrodata.ss[i].maxtime;
                   }
                   if(ps[i].mintime > tvcpu->nitrodata.ss[i].mintime){
                        ps[i].mintime = tvcpu->nitrodata.ss[i].mintime;
                   }
                   ps[i].timesecs+=(ps[i].timeusecs/1000000);
                   ps[i].timeusecs%=1000000;
               }
           }
           if(ps[i].count != 0 || ps[i].fkcount != 0){
               data += sprintf(data,"\n%s - count %lu fkcount %lu time(secs.usecs) %lu.%lu avg %ld max %d min %d", name, ps[i].count, ps[i].fkcount, ps[i].timesecs, ps[i].timeusecs, ((ps[i].timesecs*1000000)+ps[i].timeusecs)/(ps[i].count + ps[i].fkcount) , ps[i].maxtime, ps[i].mintime);
               if( (data-buf) > 200){
                   jprinte("%s\n",buf);
                   buf = data;
               }
               //jprintk("%s - count %lu fkcount %lu time %lu.%lu avg %ld max %d min %d \n", name, ps[i].count, ps[i].fkcount, ps[i].timesecs, ps[i].timeusecs, ((ps[i].timesecs*1000000)+ps[i].timeusecs)/(ps[i].count + ps[i].fkcount), ps[i].maxtime, ps[i].mintime);
           }else{
               //jprintk("empty sysstats i%d %s \n", i, name);
           }
       }else{
           //jprintk("null name sysstats i%d %s \n", i, name);
       }
    }
    jprinte("%s\n",buf);
    buf = data;
    jprinte("status command end: \n");
    my_kfree(ps,sizeof(struct sysstats)*MAX_STATS);
#endif
#endif
    return 0;
}

int clear_command(void *bufptr){
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i,j;
    jprinte("clear command: \n");
    status_command(bufptr);
    
    atomic_set(&debug_time,0);
            
#if defined(WRITE_DELAY)
    extern atomic_t writedelay_sum;
    extern atomic_t writedelay_count;

    atomic_set(&writedelay_sum,0);
    atomic_set(&writedelay_count,0);
#endif

#ifdef MEM_TRACE
    extern int sci_memory_used_max;
    extern atomic_t sci_memory_used;
    sci_memory_used_max = 0;
    atomic_set(&sci_memory_used,0);
#endif
    
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            tvcpu->nitrodata.scall_count = 0;
            tvcpu->nitrodata.sret_count = 0;
            tvcpu->nitrodata.scallm_time = 0;
            tvcpu->nitrodata.sretm_time = 0;
            tvcpu->nitrodata.scallu_time = 0;
            tvcpu->nitrodata.sretu_time = 0;
            tvcpu->nitrodata.scalli_time = 0;
            tvcpu->nitrodata.sreti_time = 0;
#ifdef JADU_SKIP_SYSCALLS  
            tvcpu->nitrodata.skipped_syscalls = 0;
#endif
#ifdef COMPUTE_BETWEEN_IO
            tvcpu->nitrodata.io_amount = 0;
            tvcpu->nitrodata.compute_time = 0;
            getnstimeofday(&tvcpu->nitrodata.compute_time_start);
#endif
#ifdef JADUKATA
#ifdef SYS_STATS
            memset(tvcpu->nitrodata.ss,0,sizeof(struct sysstats)*MAX_STATS);
            for(j=0;j<MAX_STATS;j++){
                tvcpu->nitrodata.ss[j].mintime = 1000000;
            }
#endif
#endif
        }
    }
    return 0;
}

#ifdef BLOCK_LIFETIME

extern hash_table *ht_series_blocklifetime;
extern hash_table *ht_blocklifetime;

void get_min_max_sum(hash_table* table, unsigned long* min, unsigned long* max, unsigned long* sum){
    unsigned long key, val;
    *min = ULONG_MAX; *max =0; *sum =0;
    ht_open_scan(table);
    while(ht_scan_val(table, &key,&val) != -1){
        if(key < *min) *min = key;
        if(key > *max) *max = key;
        (*sum) = (*sum) + val;
    }
    if(*min == ULONG_MAX) *min = 0;
    if(*sum == 0) *sum = 1;
    ht_close_scan(table);
}

void add_to_series(hash_table* table, unsigned long val, unsigned long units);

void flush_block_lifetimes(void){
    unsigned long key, val, count = 0;
    //10000 seconds in future to denote non overwritten blocks
    unsigned long j = get_jiffies() + msecs_to_jiffies(10000*1000);
    while(ht_pop_val(ht_blocklifetime,&key, &val) != -1){
        add_to_series(ht_series_blocklifetime,jiffies_to_msecs( j - jsecs_to_jiffies(val)),BLOCK_LIFETIME_SERIES_UNIT_MSECS);
        count++;
        if(count %200 == 0){
            msleep(50);
        }
    }
}

#endif

inline char* divhelper(char* buff, unsigned long a, unsigned long b){
    if(b == 0) b = 1;
    sprintf(buff,"%3ld.%05ld",a/b,((a%b)*100000)/b);
    return buff;
}
        
int wkld_trace_command(void *bufptr){
    
#ifdef WKLD_TRACE
    char* buf = (char*)bufptr;
    char* data = (char*)bufptr;
    unsigned long i = 0, j =0, key, val = 0;
    unsigned long min,max,sum,tsum=0;
    char buff[200];
    char buff1[200];
    jprinte("\nwkld_trace command: \n");
    data += sprintf(data,"\nwkld_trace_command:\n");

#ifdef BLOCK_LIFETIME
    flush_block_lifetimes();
    data += sprintf(data,"\nblocklifetime:\n");
    get_min_max_sum(ht_series_blocklifetime, &min, &max, &sum);
    jprintk("\nwkld_trace command: min %lu max %lu sum %lu\n",min,max,sum);

    for(i=min;i<=max;i++){
        val = 0;
        ht_lookup_val(ht_series_blocklifetime,i,&val);
        if( (val*100*100000) >= sum){
            data += sprintf(data,"%6d:%s;\n",i,divhelper(buff,(val*100),sum));
            tsum += val*100;
        }
    }
    data += sprintf(data,"\ntotal percentage: %s\n",divhelper(buff,tsum,sum));
    while(ht_pop(ht_series_blocklifetime, &key) != -1);
#endif

#ifdef COMPUTE_BETWEEN_IO
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    char* units[] = {"KB","MB","GB","TB","PB","EB","ZB"};
    int k = 0;
    unsigned long compute_time = 0, io_amount = 0, io_amount1 = 0;
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(k, tvcpu, kvm){
            io_amount += tvcpu->nitrodata.io_amount;
            compute_time += tvcpu->nitrodata.compute_time;
        }
    }
    io_amount /= (BYTES_PER_KB / COMPUTE_IOAMOUNT_UNIT_BYTES);
    compute_time /= (NSECS_PER_MSEC / COMPUTE_TIME_UNIT_NSECS);
    j = 0;
    io_amount1 = io_amount;
    while(io_amount1 > compute_time){
        io_amount1/=1024;
        j++;
    }
    data += sprintf(data,"compute time: %lu ; io_amount : %lu(%s) %lu(%s) ; avg compute time: %s (Per %s) %s (Per %s);\n",compute_time, io_amount, units[0], io_amount1, units[j] , divhelper(buff,compute_time,io_amount), units[0],divhelper(buff1,compute_time,io_amount1),units[j]);
#endif

#endif
    return 0;
}

int clear_wkld_trace_command(void *bufptr){
#ifdef WKLD_TRACE
    char* buf = (char*)bufptr;
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i;
    jprinte("\nwkld_trace_clear command: \n");
    buf += sprintf(buf,"\nwkld_trace_clear:\nbefore");

    wkld_trace_command(buf);
    buf = bufptr + strlen(bufptr);

#ifdef BLOCK_LIFETIME
    unsigned long key, count = 0;
    while(ht_pop(ht_blocklifetime, &key) != -1){
        count++;
        if(count %500 == 0){
            msleep(50);
        }
    }
    count = 0;
    while(ht_pop(ht_series_blocklifetime, &key) != -1){
        count++;
        if(count %500 == 0){
            msleep(50);
        }
    }
#endif

#ifdef COMPUTE_BETWEEN_IO
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            tvcpu->nitrodata.io_amount = 0;
            tvcpu->nitrodata.compute_time = 0;
        }
    }
#endif
    
    buf += sprintf(buf,"\nafter:");
    wkld_trace_command(buf);
    
    buf = bufptr + strlen(bufptr);
    buf += sprintf(buf,"\n");

#endif    
    return 0;
}

#ifdef JADUKATA
/*
int set_task_ptrptr(char* buf){
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    int i;
    jprinte("settaskptr %s \n",buf);
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            sscanf(buf,"%lx",&(tvcpu->nitrodata.task_ptrptr));
            while(*buf != ' '){
                buf++;
            }
            buf++;
            jprinte("vcpu id %d , task ptr %lx \n",tvcpu->vcpu_id,tvcpu->nitrodata.task_ptrptr);
        }
   }
   return 0;
}
*/

extern void vmmcomm_set_base(unsigned long base);
extern void vmmcomm_unset_base(void);
extern void vmmcomm_start(void);
extern void vmmcomm_stop(void);
extern struct kvm* vmmcomm_kvm;
extern unsigned long vmmcomm_translate_fn;
extern unsigned long gpa2hpa(unsigned long);

#ifdef JADUKATA_VMM_COMM
void set_vmmcomm_base_helper(struct kvm_vcpu* tvcpu, gva_t basegva){
    struct x86_exception ex;
    hva_t basehva = 0;
    unsigned long magic = 0L;
    basehva = jadukata_gva_to_hva(tvcpu,basegva);
    jprinte("going to set_vmmcomm_base basegva %lx basehva %lx \n", basegva,basehva );
    if(basehva){
        vmmcomm_kvm = tvcpu->kvm;
        vmmcomm_translate_fn = (unsigned long)gpa2hpa;
        vmmcomm_set_base(basehva);
        vmmcomm_start();
    }
}
#endif

int set_vmmcomm_base(char* buf){
#ifdef JADUKATA_VMM_COMM
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    gva_t basegva = 0;
    int i;

    sscanf(buf,"%lx\n",(unsigned long*)&basegva);
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            if(i==0){
                tvcpu->nitrodata.pnetlink = basegva;
            }
        }
    }
#endif
    return 0;
}

int unset_vmmcomm_base(void){
#ifdef JADUKATA_VMM_COMM
        vmmcomm_stop();
        vmmcomm_unset_base();
        vmmcomm_kvm = NULL;
        vmmcomm_translate_fn = 0x0;
#endif
    return 0;
}

#endif

struct sock *nl_sk = NULL;

static DEFINE_SEMAPHORE(netlink_lock);

static void netlink_nl_recv_msg(struct sk_buff *skb) {
    struct nlmsghdr *nlh;
    int pid,cr3pid;
    struct sk_buff *skb_out;
    int res;
    int code = -1;
    int ret = 0;
    char *buf, *bufptr;
    char cmd[100];
    unsigned long cr3;
    unsigned long testinput;
    unsigned long flags;
        
    nlh=(struct nlmsghdr*)skb->data;
    bufptr = (char*)nlmsg_data(nlh);
    buf = bufptr;
    pid = nlh->nlmsg_pid; /*pid of sending process */

    //output buffer
    skb_out = nlmsg_new(MAX_PAYLOAD_KERNEL,0);

    if(!skb_out)
    {
        jprinte("ERROR Failed to allocate new skb\n");
        return;
    }

    nlh=nlmsg_put(skb_out,0,0,NLMSG_DONE,MAX_PAYLOAD_KERNEL,0);
    NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */

    jprinte("Received netlink command %s \n", bufptr);
    jprinte("Processing netlink command %s \n", bufptr);
    
    sscanf(bufptr,"%s ", cmd);
    bufptr += strlen(cmd) + 1;

    while(down_interruptible(&netlink_lock) != 0){
    };

    if(strncmp(cmd,"clear",5)==0){
        jprinte("%d %s going to call clear command\n", current->pid, current->comm);
        ret = clear_command(bufptr);
    }else if(strncmp(cmd,"wkld_trace_clear",16)==0){
        jprinte("%d %s going to call clear_wkld_trace command\n", current->pid, current->comm);
        ret = clear_wkld_trace_command(bufptr);
    }else if(strcmp(cmd,"status")==0){
        jprinte("%d %s going to call status command\n", current->pid, current->comm);
        ret = status_command(bufptr);
    }else if(strcmp(cmd,"wkld_trace")==0){
        jprinte("%d %s going to call wkld_trace command\n", current->pid, current->comm);
        ret = wkld_trace_command(bufptr);
    }else if(strcmp(cmd,"test") == 0){
        jprinte("%d %s going to test %s\n", current->pid, current->comm,bufptr);
        ret = nitro_test(bufptr);
#ifdef JADUKATA
    }else if(strcmp(cmd,"trace") == 0){
        int level;
        sscanf(bufptr,"%d\n",&level);
        if((level & POW2(20))){
            jprint_enabled = 1;
        }else{
            jprint_enabled = 0;
        }
        
        if((level & POW2(21))){
            jprint_enabled = 2 ;
        }
        
        if((level & POW2(22))){
            jprint_enabled = 3 ;
        }
        
#ifdef DETECT_OVERWRITES
        extern hash_table *ht_file_chunksums;
        if((level & POW2(10))){
            ht_set_debug(ht_file_chunksums);
        }else{
            ht_unset_debug(ht_file_chunksums);
        }
#endif
        
        jprinte("%d %s got level %d setting jprint_enabled = %d \n", current->pid, current->comm, level,jprint_enabled);
        ret = 0;
    }else if(strcmp(cmd,"addcr3") == 0){
        nitro_shutdown = 0;
        sscanf(bufptr,"%lx %d\n",&cr3,&cr3pid);
        jprinte("%d %s going to add cr3 0x%lx pid %d to monitored processes\n", current->pid, current->comm,cr3,cr3pid);
        //leo added this line to clear all targeted processes before adding a new one in Dec 2015
        remove_all_targeted_processes();
        clear_splitcalls();
        unsigned long key;
        //ret = add_targeted_process(cr3,0,cr3pid);
        //ret = add_targeted_process(cr3,0,cr3pid);
        extern hash_table *ht_new_pids;
        extern hash_table *ht_cr3_to_pid;
        extern hash_table *ht_fork_parent_pids;
        extern hash_table *ht_exec_cr3s;
        extern hash_table *ht_new_process_tracking;
        atomic_set(&in_new_pid_mode,0);
        while(ht_pop(ht_new_pids, &key) != -1);
        ht_add_val(ht_new_pids,cr3pid,0);
        while(ht_pop(ht_cr3_to_pid, &key) != -1);
        while(ht_pop(ht_fork_parent_pids, &key) != -1);
        while(ht_pop(ht_exec_cr3s, &key) != -1);
        while(ht_pop(ht_new_process_tracking, &key) != -1);
#ifdef NO_ON_OFF
        start_nitro_all('a');
#endif
        ret = 0;
    }else if(strcmp(cmd,"delcr3") == 0){
        sscanf(bufptr,"%lx\n",&cr3);
        jprinte("%d %s going to stop nitro cr3 %d \n", current->pid, current->comm,cr3);
        //ret = remove_targeted_process(cr3);
        
        //TODO: remove the need for this
        // I think the process relationships are not 
        // perfectly capture , so there remain some monitored
        // process at the end of experiments after calling delcr3
        // for now, clear them off
        ret = 0;
        nitro_shutdown = 1;
        unsigned long key;
        //ret = add_targeted_process(cr3,0,cr3pid);
        //ret = add_targeted_process(cr3,0,cr3pid);
    }else if(strcmp(cmd,"addskipcr3") == 0){
        sscanf(bufptr,"%lx %d\n",&cr3,&cr3pid);
        jprinte("%d %s going to add skip cr3 0x%lx pid %d to skipped processes\n", current->pid, current->comm,cr3,cr3pid);
        add_skipped_process(cr3);
        ret = 0;
    }else if(strcmp(cmd,"delskipcr3") == 0){
        sscanf(bufptr,"%lx\n",&cr3);
        jprinte("%d %s going to remove skip cr3 0x%lx from skipped processes\n", current->pid, current->comm,cr3);
        remove_skipped_process(cr3);
        ret = 0;
    }else if(strcmp(cmd,"settaskptr") == 0){
        /*
        jprinte("%d %s going to settaskptr %s\n", current->pid, current->comm,bufptr);
        ret = set_task_ptrptr(bufptr);
        */
    }else if(strcmp(cmd,"setvmmcommbase") == 0){
        jprinte("%d %s going to setvmmcommbase %s\n", current->pid, current->comm,bufptr);
        ret = set_vmmcomm_base(bufptr);
    }else if(strcmp(cmd,"unsetvmmcommbase") == 0){
        jprinte("%d %s going to unsetvmmcommbase\n", current->pid, current->comm);
        ret = unset_vmmcomm_base();
    }
#else
    }
#endif
    up(&netlink_lock);

    sprintf(nlmsg_data(nlh),"%d %s\n",ret,buf);

    res=nlmsg_unicast(nl_sk,skb_out,pid);

    if(res<0)
        jprinte("ERROR while sending back to user\n");
}

int netlink_init(void) {

    jprinte("jadukata netlink init\n");
    struct netlink_kernel_cfg cfg = {
        .input = netlink_nl_recv_msg,
        .groups = 3,
    };

    nl_sk=netlink_kernel_create(&init_net, NETLINK_USERSOCK, &cfg);
    if(!nl_sk)
    {
        jprinte("ERROR creating socket.\n");
        return -10;
    }
    return 0;
}

void netlink_exit(void) {
    jprinte("jadukata netlink exit\n");
    netlink_kernel_release(nl_sk);
}

