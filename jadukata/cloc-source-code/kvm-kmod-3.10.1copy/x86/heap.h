#ifndef _BCACHE_UTIL_H

#define DECLARE_HEAP(type, name)					\
	struct {							\
		size_t size, used;					\
		type *data;						\
	} name

#define init_heap(heap, _size, gfp)					\
({									\
	size_t _bytes;							\
	(heap)->used = 0;						\
	(heap)->size = (_size);						\
	_bytes = (heap)->size * sizeof(*(heap)->data);			\
	(heap)->data = NULL;						\
	if (_bytes < KMALLOC_MAX_SIZE)					\
		(heap)->data = my_kmalloc(_bytes, (gfp));			\
	if ((!(heap)->data) && ((gfp) & GFP_KERNEL))			\
		(heap)->data = my_vmalloc(_bytes);				\
	(heap)->data;							\
})

#define free_heap(heap)							\
do {									\
	if (is_vmalloc_addr((heap)->data))				\
		my_vfree((heap)->data,((heap)->size * sizeof(*(heap)->data)));					\
	else								\
		my_kfree((heap)->data,((heap)->size * sizeof(*(heap)->data)));					\
	(heap)->data = NULL;						\
} while (0)

#define heap_swap(h, i, j)	swap((h)->data[i], (h)->data[j])

#define heap_sift(h, i, cmp)						\
do {									\
	size_t _r, _j = i;						\
									\
	for (; _j * 2 + 1 < (h)->used; _j = _r) {			\
		_r = _j * 2 + 1;					\
		if (_r + 1 < (h)->used &&				\
		    cmp((h)->data[_r], (h)->data[_r + 1]))		\
			_r++;						\
									\
		if (cmp((h)->data[_r], (h)->data[_j]))			\
			break;						\
		heap_swap(h, _r, _j);					\
	}								\
} while (0)

#define heap_sift_down(h, i, cmp)					\
do {									\
	while (i) {							\
		size_t p = (i - 1) / 2;					\
		if (cmp((h)->data[i], (h)->data[p]))			\
			break;						\
		heap_swap(h, i, p);					\
		i = p;							\
	}								\
} while (0)

#define heap_add(h, d, cmp)						\
({									\
	bool _r = !heap_full(h);					\
	if (_r) {							\
		size_t _i = (h)->used++;				\
		(h)->data[_i] = d;					\
									\
		heap_sift_down(h, _i, cmp);				\
		heap_sift(h, _i, cmp);					\
	}								\
	_r;								\
})

#define heap_pop(h, d, cmp)						\
({									\
	bool _r = (h)->used;						\
	if (_r) {							\
		(d) = (h)->data[0];					\
		(h)->used--;						\
		heap_swap(h, 0, (h)->used);				\
		heap_sift(h, 0, cmp);					\
	}								\
	_r;								\
})

#define heap_peek_new(h)	((h)->used ? (h)->data[0] : NULL)

#define heap_peek(h)	((h)->size ? (h)->data[0] : NULL)

#define heap_full(h)	((h)->used == (h)->size)

#endif
