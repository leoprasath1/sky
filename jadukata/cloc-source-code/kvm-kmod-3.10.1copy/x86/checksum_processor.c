#include "checksum_processor.h"
#include "filesize_ioclass.c"

#include "checksums.c"

#include <crypto/internal/hash.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/cryptohash.h>
#include <asm/byteorder.h>

int debug = 0;

unsigned long pack_info_xyz(int time, int sector,char bigfile){
    unsigned long ret;
    unsigned* reta = (unsigned*)&ret;
    reta[0] = time << 8;
    reta[1] = sector;
    if(bigfile){
        reta[0] |= (char)bigfile;
    }
    return ret;
}

int pack_info_get_x(unsigned long val){
    unsigned* reta = (unsigned*)&val;
    return reta[0]>>8;
}

int pack_info_get_y(unsigned long val){
    unsigned* reta = (unsigned*)&val;
    return reta[1];
}

char pack_info_get_z(unsigned long val){
    unsigned* reta = (unsigned*)&val;
    return (reta[0] & 0xFF);
}


#ifdef JADUKATA_CHECKSUM
#ifdef JADUKATA_VERBOSE
void checksum_processor1(struct pair* pairs, int numpairs)
{
    char codeddata[1800];
    int i,j;
    char* source;
    int bytes;
    char* dst=codeddata;
    int count=0;
    int cur = ~0;

    for(j=0;j<numpairs;j++){
        source = (char*)pairs[j].addr;
        bytes = pairs[j].size;
        
        if( cur != ~0 ){
            if( ((char)cur == source[0]) ){
                count++;
            }else{
                dst += sprintf(dst,"%dx%u,",count+1,(char)cur);
                count= 0;
            }
        }
        
        for(i=0;i<bytes-1;i++){
            if(source[i] == source[i+1]){
                count++;
            }else {
                dst += sprintf(dst,"%dx%u,",count+1,source[i]);
                count = 0;
            }
            if( (dst-codeddata)>1700 ) {
                count = 0;
                dst += sprintf(dst,"...\n");
                break;
            }
        }
        cur = source[bytes-1];
    }
    
    dst += sprintf(dst,"%dx%u,",count+1,(char)cur);

    bytes= (dst-codeddata);
    dst = codeddata;
    for(i=0;i<=(bytes/100);i++){
        jprintk("nitro coded %d %*s\n",i,(i==(bytes/100))?bytes-(i*100):100,dst);
        dst += 100;
    }
}
#endif

int do_cleanup_helper(struct cleaner_data* cl, bool scheduled){
#define log (0)
    //unsigned long flags;
    int count = 0;
    int min_jiffies = 0;
    int now;

    if(cl->might_sleep && !scheduled ){
        return 0;
    }
    
    now = get_jiffies();

    // NOTE 1: down_trylock has different return value semantics 
    // than spin_trylock.
    // NOTE 2: additional trylock sem apart from open_scan's to 
    // not keep i/o threads wating if there is already one 
    // thread cleaning up
    //if(spin_trylock(&cl->lock)){
    //if(!down_trylock(&cl->busy_sem)){
    // we rely on the open_scan's sema lock to allow only one cleaner at a time
    
    if(!scheduled){
        if(!heap_peek_new(&cl->heap))
            return min_jiffies;
        if(now < (jsecs_to_jiffies(heap_peek_new(&cl->heap)->jsecs) + msecs_to_jiffies(cl->period))){
            return min_jiffies;
        }
    }
            
    if(log) jprintk("cleaner %s %d \n", cl->table->name, now);
    
    if(!down_trylock(&cl->busy_sem)){
        struct cleaner_heap_entry* entry = NULL;
        unsigned long val;
        unsigned long time_diff = 0;
        struct timeval start,end;
        min_jiffies = now + msecs_to_jiffies(cl->period);
        
        do_gettimeofday(&start);
		
        while(count < cl->cleanup_rate || scheduled){
            int entry_jiffies;
            entry = heap_peek_new(&cl->heap);
            if(entry == NULL){
                if(log) jprintk("heap empty %ld \n", cl->heap.used);
                break;
            }
            if(log) jprintk("heap peek entry key %lu jsecs %d \n", entry->key, entry->jsecs);
            entry_jiffies = (jsecs_to_jiffies(entry->jsecs) + msecs_to_jiffies(cl->period));
            if( now >= entry_jiffies ){
                heap_pop(&cl->heap, entry, cleanup_heap_min_cmp);
                if(ht_lookup_val(cl->table,entry->key,&val) != 0){
                    if(entry->jsecs == pack_info_get_x(val)){
                        int delay = jiffies_to_msecs(now - entry_jiffies);
                        if(delay > cl->max_delay){
                            cl->max_delay = delay;
                        }
                        if(cl->process_fn){
                            cl->process_fn(cl->table,entry->key,val);
                        }
                        htn_remove(cl->table,entry->key);
                    }
                }
                my_kfree(entry,sizeof(struct cleaner_heap_entry)); 
            }else{
                if(log) jprintk("heap oldest jsecs %d \n", entry->jsecs);
                break;
            }
            count++;
        }

        if(heap_peek_new(&cl->heap)){
            min_jiffies = (jsecs_to_jiffies(heap_peek_new(&cl->heap)->jsecs) + msecs_to_jiffies(cl->period));
        }
        
        now = get_jiffies();
        cl->last_cleanup = now;
        if(!scheduled){
            if(count < cl->cleanup_rate){
                cl->cleanup_rate = 50;
            }else{
                cl->cleanup_rate += 10;
            }
        }

        //if scheduled and heap empty , then rebuild it
        if(scheduled && !heap_peek_new(&cl->heap)){
            int i;
            int hcount = 0;
            unsigned long key, val;
            if(log) jprintk("cleaner filling heap \n");
            ht_open_scan(cl->table);
            count = 0;
            while(ht_scan_val(cl->table,&key,&val) != -1){
                int entry_jiffies = jsecs_to_jiffies(pack_info_get_x(val)) + msecs_to_jiffies(cl->period);
                //printk(KERN_ERR "entry_jiffies %d jiffies %ld val %d jsecs_to_jiffies(val) %ld msecs_to_jiffies(period) %ld\n", entry_jiffies, jiffies, pack_info_get_x(val), jsecs_to_jiffies(pack_info_get_x(val)), msecs_to_jiffies(cl->period));
                if( (pack_info_get_x(val)!=0) && (now >= entry_jiffies) ){
                    int delay = jiffies_to_msecs(now - entry_jiffies);
                    if(delay > cl->max_delay){
                        cl->max_delay = delay;
                    }
                    if(cl->process_fn){
                        cl->process_fn(cl->table,key,val);
                    }
                    htn_remove(cl->table,key);
                }else{
                    entry = (struct cleaner_heap_entry*)my_kmalloc(sizeof(struct cleaner_heap_entry), GFP_KERNEL);
                    if(entry == NULL){
                        jprinte("ERROR could not allocate memory for entry \n");
                    }
                    entry->key = key;
                    entry->jsecs = pack_info_get_x(val);
                    if(entry->jsecs == 0 ){
                        entry->jsecs = jiffies_to_jsecs(now);
                    }
                    if(log) jprintk("adding entry key %lu jsecs %d \n", entry->key, entry->jsecs);
                    if(!heap_full(&cl->heap)){
                        heap_add(&cl->heap, entry, cleanup_heap_max_cmp);
                    }else if(cleanup_heap_max_cmp(entry,heap_peek_new(&cl->heap))){
                        cl->heap.data[0] = entry;
                        heap_sift(&cl->heap, 0, cleanup_heap_max_cmp);
                    }
                    if(entry_jiffies < min_jiffies){
                        min_jiffies = entry_jiffies;
                    }
                }
                count++;
                if(count%cl->cleanup_rate == 0){
                    hcount++;
                    if((hcount*cl->cleanup_rate) > 50000){
                        min_jiffies = 0;
                        break;
                    }
                    //msleep(10);
                    now = get_jiffies();
                    count = 0;
                }
            }
            ht_close_scan(cl->table);
            for (i = cl->heap.used / 2 - 1; i >= 0; --i){
                heap_sift(&cl->heap, i, cleanup_heap_min_cmp);
            }
            if(log) jprintk("cleaner %s heap size %ld min jsecs %d\n", cl->table->name, cl->heap.used, heap_peek_new(&cl->heap)?heap_peek_new(&cl->heap)->jsecs:0);
        }
    
        do_gettimeofday(&end);

        time_diff += diff_time(start,end);
        cl->time_secs+=(time_diff/1000000);
        cl->time_usecs+=(time_diff%1000000);
        if(cl->time_usecs > 1000000){
            cl->time_secs += (cl->time_usecs/1000000);
            cl->time_usecs %= 1000000;
        }

        //spin_unlock(&cl->lock);
        up(&cl->busy_sem);
    }
    return min_jiffies;
}

void do_cleanup(struct cleaner_data* cl){
    if(jiffies_to_msecs(get_jiffies()-cl->last_cleanup) > cl->check_period){
        do_cleanup_helper(cl,false);
    }
}

static void do_cleanup_scheduled(struct work_struct *work){
    struct cleaner_data* cl = container_of(to_delayed_work(work), struct cleaner_data, work);
    int min_jiffies = do_cleanup_helper(cl,true);
    int now = get_jiffies();
    while(true){
        min_jiffies = do_cleanup_helper(cl,true);
        if(min_jiffies != 0){
            break;
        }
        //msleep(10);
    }
    if(min_jiffies <= (now + msecs_to_jiffies(cl->check_period))){
        //jiffies and not get_jiffies() here because passing to schedule_delayed_work()
        min_jiffies = msecs_to_jiffies(cl->check_period);
    }else{
        min_jiffies = (min_jiffies - now);
    }
    if(log) jprintk("cleaner %s scheduled after %d \n", cl->table->name, min_jiffies);
	schedule_delayed_work(&cl->work, min_jiffies);
}
extern hash_table* ht_cleaners;

char* cleanup_stats(char* buff){
    unsigned long val;
    struct cleaner_data* cl = NULL;
    ht_open_scan(ht_cleaners);
    while(ht_scan_val(ht_cleaners, (unsigned long*)&cl,&val) != -1){
        buff += sprintf(buff,"\ncleaner ht name: %s max delay %d msecs total compute time %lu usecs", cl->table->name, cl->max_delay, cl->time_usecs);
    }
    buff += sprintf(buff,"\n"); 
    ht_close_scan(ht_cleaners);
    return buff;
}

void cleanup_unregister(struct cleaner_data* cl){
    unsigned long time;
	cancel_delayed_work_sync(&cl->work);
    ht_remove_val(ht_cleaners,(unsigned long)cl,&time);
    jprinte("Max delay for cleaner ht: %s is %d msecs total compute time spent is %lu usecs\n",cl->table->name, cl->max_delay, cl->time_usecs);
    my_kfree(cl,sizeof(struct cleaner_data));
	free_heap(&cl->heap);
}


struct cleaner_data* cleanup_register(hash_table* table, void(*fn)(hash_table*,unsigned long,unsigned long), int periodinmsecs, int cleanup_rate){
    struct cleaner_data* cl = (struct cleaner_data*) my_kzalloc(sizeof(struct cleaner_data),GFP_KERNEL);
    cl->table = table;
    cl->period = periodinmsecs;
    cl->check_period = periodinmsecs/2;
    if(cl->check_period < 2000){
        cl->check_period = 2000;
    }
	sema_init(&cl->busy_sem,1);
    //spin_lock_init(&cl->lock);
    cl->last_cleanup = get_jiffies();
    cl->cleanup_rate = cleanup_rate; 
    cl->process_fn = fn;
    cl->might_sleep = false;
	if(!init_heap(&cl->heap,CLEANER_HEAP_SIZE, GFP_KERNEL)){
        jprinte("ERROR error initializing heap \n");
    }
    INIT_DELAYED_WORK(&cl->work,do_cleanup_scheduled);
	schedule_delayed_work(&cl->work, msecs_to_jiffies(cl->check_period));
    ht_add_val(ht_cleaners,(unsigned long)cl,jiffies);
    return cl;
}

extern int process_checksum(void* pargs, int chunknum, unsigned long checksum);

#define FNV64

#ifdef FNV64
#define CUSTOM_CHECKSUM_BYTES (8)
#define fnvob fnvob64
#define FNVHashfold FNVHashfold64
#define FNVHashfuncloop FNVHash64loop
#define FNVHashfunc FNVHash64
#else
#define CUSTOM_CHECKSUM_BYTES (16)
#define fnvob fnvob128
#define FNVHashfold FNVHashfold128
#define FNVHashfuncloop FNVHash128loop
#define FNVHashfunc FNVHash128
#endif

struct checksum_state{
    char hash[CUSTOM_CHECKSUM_BYTES];
    char s[CUSTOM_CHECKSUM_BYTES];
    short len;
};

extern char* fnvob64, *fnvob128; 

inline void checksum_state_init(struct checksum_state *state){    
    int i;
    unsigned long* ptr,*ptr1;
    state->len = 0;
    ptr = (unsigned long*)(&(state->s));
    ptr1 = (unsigned long*)(&(state->hash));
    for(i=0;i<(CUSTOM_CHECKSUM_BYTES/sizeof(unsigned long));i++){
        *(ptr+i) = 0UL;
        *(ptr1+i) = *(((unsigned long*)fnvob)+i);
    }
}

inline unsigned long foldhash(struct checksum_state *state){
    return FNVHashfold(state->hash);
}

void custom_checksum(const char* ptr, int size,struct checksum_state *state){
    int tmp;
    //if(debug)
    //    jprinte("ptr %p size %d csumpartial %p\n", ptr, size,state->hash);
    if(unlikely(state->len && ((state->len + size)>=CUSTOM_CHECKSUM_BYTES) )){
        short tocopy = CUSTOM_CHECKSUM_BYTES-state->len;
        memcpy(state->s+state->len,ptr,tocopy);
        state->len += tocopy;
        ptr+=tocopy;
        size-=tocopy;

        if(state->len == CUSTOM_CHECKSUM_BYTES){
            FNVHashfunc(state->s,state->hash);
            state->len = 0;
        }
    }

    //jprinte("ptr %p size %d \n", ptr, size);
    tmp = ((size/CUSTOM_CHECKSUM_BYTES)*CUSTOM_CHECKSUM_BYTES);
    if(tmp > 0){
        FNVHashfuncloop(ptr,state->hash,tmp);
        state->len = 0;
        ptr += tmp;
        size -= tmp;
    }

    //while(size >= CUSTOM_CHECKSUM_BYTES){
    //    FNVHashfunc(ptr,state->hash);
    //    state->len = 0;
    //    size -= CUSTOM_CHECKSUM_BYTES;
    //    ptr += CUSTOM_CHECKSUM_BYTES;
    //}

    if(unlikely(size > 0)){
        memcpy(state->s+state->len,ptr,size);
        state->len += size;
    }
}

/*
void custom_checksum(char* ptr, int size,struct checksum_state *state){
    state->checksum = FNVHash64(ptr,size,state->checksum);
}
*/

int checksum_processor2(void* pargs, struct pair* pairs, int numpairs)
{
    int j; 
    int bytes;
    int remaining = 0;
    struct checksum_state state;
    int chunknum = 0;
    char* source;
    int ret = 0;

    checksum_state_init(&state);

    for(j=0;j<numpairs;j++){
        source = (char*)pairs[j].addr;
        bytes = pairs[j].size;
        if(debug)
            jprinte("source %p bytes %d remaining %d\n", source , bytes, remaining);
        if(remaining < 0){
            if( (bytes + remaining) >=0 ){
                if(debug){
                    int k = 0;
                    jprinte("source %p bytes %d remaining %d\n", source , bytes, remaining);
                    for(k=0;k<numpairs;k++){
                        jprinte("source %lx bytes %d \n", pairs[k].addr , pairs[k].size);
                    }
                }
                custom_checksum((char*)(source),-remaining,&state);
                source += (-remaining);
                ret += process_checksum(pargs, chunknum++,FNVHashfold(state.hash));
                checksum_state_init(&state);
            }else{
                custom_checksum((char*)(source),bytes,&state);
                source += bytes;
            }
        }
        remaining = bytes+remaining;

        while(remaining>=JADU_BLKSIZE){
            custom_checksum((char*)(source),JADU_BLKSIZE,&state);
            source += JADU_BLKSIZE;
            ret += process_checksum(pargs, chunknum++,FNVHashfold(state.hash));
            checksum_state_init(&state);
            remaining -= JADU_BLKSIZE;
        }
        if(remaining > 0){
            custom_checksum((char*)(source),remaining,&state);
            source += remaining;
            remaining -= JADU_BLKSIZE;
        }
    }
    return ret;
}

#define CUSTOMFNV_DIGEST_SIZE  (8)

static int customfnv_init(struct shash_desc *desc)
{
	struct checksum_state *state = shash_desc_ctx(desc);
    checksum_state_init(state);
	return 0;
}

static int customfnv_update(struct shash_desc *desc, const u8 *data, unsigned int len)
{
	struct checksum_state *state = shash_desc_ctx(desc);
    custom_checksum(data,len,state);
	return 0;
}

static int customfnv_final(struct shash_desc *desc, u8 *out)
{
	struct checksum_state *state = shash_desc_ctx(desc);
    *((u64*)out)=foldhash(state);
    checksum_state_init(state);
	return 0;
}

static struct shash_alg alg = {
	.digestsize	=	CUSTOMFNV_DIGEST_SIZE,
	.init		=	customfnv_init,
	.update		=	customfnv_update,
	.final		=	customfnv_final,
	.descsize	=	sizeof(struct checksum_state),
	.statesize	=	sizeof(struct checksum_state),
	.base		=	{
		.cra_name	=	"customfnv",
		.cra_flags	=	CRYPTO_ALG_TYPE_SHASH,
		.cra_module	=	THIS_MODULE,
	}
};

int customfnv_mod_init(void)
{
	return crypto_register_shash(&alg);
}

void customfnv_mod_fini(void)
{
	crypto_unregister_shash(&alg);
}

#endif
