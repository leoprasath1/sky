#define IOCONV_SWITCH (1)
//#define IOCONV_SWITCH (10)
#define IOCONV(x) ((x*1024*1024)*IOCONV_SWITCH)

char filesize_to_ioclass(unsigned long size){
    char ioclass = 0;
    if(size < (IOCONV(10))){
        ioclass = 1;
        if( size < (IOCONV(5))){
            ioclass = 2;
            if( size < (IOCONV(2))){
                ioclass = 3;
                if(size < (IOCONV(1))){
                    ioclass = 4;
                }
            }
        }
    }
    return ioclass;
}


