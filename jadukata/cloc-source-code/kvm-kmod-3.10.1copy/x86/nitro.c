#include <linux/nitro.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <asm/io.h>
#include <asm/checksum.h>
#include "kvm_cache_regs.h"
#include "x86.h"
#include "mmu.h"
#include "tss.h"
#include "checksum_processor.h"
#include "all_common.h"

extern void* leo_kvm_debug;
short int nitro_shutdown = 0;

void* leo_vmx_debug = NULL;
EXPORT_SYMBOL(leo_vmx_debug);

void leo_kvm_debug_func(void* info) {
    int i;
    struct kvm* kvm;
    struct kvm_vcpu* vcpu;

    if(leo_vmx_debug){
       void (*x)(void*) = leo_vmx_debug;
       x(NULL);
    }

    list_for_each_entry(kvm, &vm_list, vm_list){
	    kvm_for_each_vcpu(i, vcpu, kvm){
            printk(KERN_ERR "*****************\n");
            printk(KERN_ERR " leo  vcpu %d on cpu %d \n", vcpu->vcpu_id, vcpu->cpu);
            printk(KERN_ERR " leo vcpu rip %lx \n", kvm_register_read(vcpu, VCPU_REGS_RIP) );
            printk(KERN_ERR "*****************\n");
        }
    }
}

#ifdef JADUKATA
extern bool ndbg;

#define RETURN22 return 22

// this distance threshold avoid matching split syscalls between:
// 1) different processes because cr3 is different
// 2) different threads with closely laid userspace stacks because rip is different
// in 4KB pages
#define IDFORMAT "%lx"
#define SCALL_ID_DIST (1UL) // allow 2 MB distance
#define ID_WITH_TID

#ifdef ID_WITH_TID
#undef IDFORMAT
#define IDFORMAT "%lx"
#undef SCALL_ID_DIST 
#define SCALL_ID_DIST (1UL) //exact match
#endif

#define NO_SCALL (0xfffffff) // no syscall yet
#define UNMON_SCALL (0xffffffe) //denotes unmonitored scall

#undef HANDLERS
#include "handlers.h"

#ifdef JADUKATA_CHECKSUM
extern hash_table* ht_data_hpas;
extern hash_table* ht_data_checksums_write;
#endif
//mmap
hash_table *ht_mmap_process = NULL;
hash_table *ht_mmap_gpas = NULL;
hash_table *ht_mmap_gpas_ptep = NULL;
hash_table *ht_rw_gpas = NULL;
hash_table *ht_rw_gpas_ptep = NULL;
hash_table *ht_tracked_gpas = NULL;
hash_table *ht_tracked_gfns = NULL;
hash_table *ht_new_pids = NULL;
hash_table *ht_fork_parent_pids = NULL;
hash_table *ht_exec_cr3s = NULL;
hash_table *ht_new_process_tracking = NULL;
hash_table *ht_cr3_to_pid = NULL;
hash_table *ht_sig_nu_stash = NULL;
hash_table *ht_new_pids_fdinherit = NULL;
hash_table *ht_file_sizes = NULL;
hash_table *ht_file_eof_guess = NULL;
hash_table *ht_file_lastwrite_content = NULL;

#if defined(BLOCK_LIFETIME) || defined(WRITE_DELAY)
hash_table *ht_blocklifetime = NULL;
#endif
#ifdef BLOCK_LIFETIME
hash_table *ht_series_blocklifetime = NULL;
#endif
#ifdef FILE_ACCESSPATTERN
hash_table *ht_fdaccesspattern = NULL;
hash_table *ht_series_fdaccesspattern = NULL;
#endif

atomic_t in_new_pid_mode = ATOMIC_INIT(0);

struct kmem_cache *nucache = NULL;

//to store split system call procedures in guest context switch
hash_table *ht_splitcalls = NULL;

#define emul_to_vcpu(ctxt) container_of(ctxt, struct kvm_vcpu, arch.emulate_ctxt)

hash_table *ht_mon_process = NULL;
hash_table *ht_mon_pids = NULL;
unsigned long process_cr3s_bloom = 0;

#define ones_match(val,bloom) ((~(val & bloom) & val) == 0)
unsigned long syscall_bloom = 0;

void recalc_syscall_bloom(void){
    int i =0;
    syscall_bloom = 0;
    for(i=0;i<NR_SYSCALLS;i++){
        syscall_bloom |= monitored_syscalls[i];
    }
}

//static DEFINE_SPINLOCK(cr3topidlock);
//unsigned long cr3topidgen = 0;

int lookup_cr3_to_pid(unsigned long cr3){
   return (ht_lookup(ht_cr3_to_pid,cr3) != 0);
}

int lookup_val_cr3_to_pid(unsigned long cr3, unsigned long *pid){
   return (ht_lookup_val(ht_cr3_to_pid,cr3,pid) != 0);
}

int add_cr3_to_pid(unsigned long cr3, int pid){
    return ht_add_val(ht_cr3_to_pid,cr3, pid);
}

int del_cr3_to_pid(unsigned long cr3){
    return (ht_remove(ht_cr3_to_pid,cr3) != -1);
}

void clear_unmonitored_cr3_to_pid(void){
    if(ht_get_size(ht_fork_parent_pids) <=0){
        unsigned long tcr3, tpid;
        ht_open_scan(ht_cr3_to_pid);
        while(ht_scan_val(ht_cr3_to_pid, &tcr3,&tpid) != -1){
            if(!is_targeted_process(tcr3)){
                htn_remove(ht_cr3_to_pid, tcr3);
            }
        }
        ht_close_scan(ht_cr3_to_pid);
    }else{
        unsigned long key, val;
        jprintm("not clearing ht_cr3_to_pid size %d\n", ht_get_size(ht_fork_parent_pids));
        ht_open_scan(ht_fork_parent_pids);
        while(ht_scan_val(ht_fork_parent_pids, &key,&val) != -1){
            jprintm("fork_parent_pids content key %lu val %lu \n", key, val);
        }
        ht_close_scan(ht_fork_parent_pids);
    }
    return;
}

void clear_all_cr3_to_pid(void){
    unsigned long key;
    jprintm("before clearing ht_cr3_to_pid size %d\n", ht_get_size(ht_cr3_to_pid));
    while(pop_cr3_to_pid(&key));
    jprintm("cleared ht_cr3_to_pid size %d\n", ht_get_size(ht_cr3_to_pid));
}

int pop_cr3_to_pid(unsigned long* key){
    return (ht_pop(ht_cr3_to_pid, key) != -1);
}

int del_cr3_to_pid_pid(int pid){
    int ret = 0;
    unsigned long tcr3, tpid;
    ht_open_scan(ht_cr3_to_pid);
    while(ht_scan_val(ht_cr3_to_pid, &tcr3,&tpid) != -1){
        if((int)tpid == pid){
            htn_remove(ht_cr3_to_pid, tcr3);
            ret = 1;
        }
    }
    ht_close_scan(ht_cr3_to_pid);
    return ret;
}

/*
extern int is_sysenter_sysreturn(struct kvm_vcpu *vcpu);
extern int is_int(struct kvm_vcpu *vcpu);
*/

#ifdef NO_ON_OFF
int start_nitro(struct kvm_vcpu *, char , unsigned long);
int stop_nitro(struct kvm_vcpu *, char, unsigned long);
#endif

void stats_update(struct kvm_vcpu* vcpu, int id, unsigned long time, bool isfake){
#ifdef SYS_STATS
    if(unlikely(id >= MAX_STATS)){
        bool targ = is_targeted_process(vcpu->arch.cr3);
        if(!targ){
            id = UNMON_INDEX;
        }else{
            jprinte("ERRORLEO: id is greater than MAX_STATS. id : %d max stats %d\n", id, MAX_STATS);
            return;
        }
    }
    struct sysstats* sse = &(vcpu->nitrodata.ss[id]);
    isfake?sse->fkcount++:sse->count++;
    sse->timeusecs+=(time/1000);
    if(sse->timeusecs > 10000000){
        sse->timesecs+=(sse->timeusecs/1000000);
        sse->timeusecs %= 1000000;
    }
    if(time>sse->maxtime){
        sse->maxtime=time/1000;
        jprintk("maxupdate %d %lu \n", sse->maxtime, time);
    }
    if(time<sse->mintime){
        sse->mintime=time/1000;
        jprintk("minupdate %d %lu \n", sse->mintime, time);
    }
#endif
}

void nu_init(struct nitrodatau *nu){
    //first three fields is not cleared out
    memset( (((char*)nu) + (3*sizeof(unsigned long))) , 0 , (sizeof(struct nitrodatau) - (3*sizeof(unsigned long))) );
    nu->out_syscall = NO_SCALL;
}

void nu_init_full(struct nitrodatau *nu){
    memset( nu , 0 , sizeof(struct nitrodatau) );
    nu->out_syscall = NO_SCALL;
}

void nitro_vcpu_init(struct kvm_vcpu *vcpu){
    nu_init_full(&vcpu->nitrodata.nu);
}

int nitro_kvm_init(struct kvm_vcpu *vcpu){
    int i;
	vcpu->nitrodata.running = 0;
    //vcpu->nitrodata.task_ptrptr = 0;
    vcpu->nitrodata.idcache_cr3 = 0;
    vcpu->nitrodata.idcache_start_rsp = 0;
    vcpu->nitrodata.idcache_end_rsp = 0;
    vcpu->nitrodata.idcache_id = 0;
	vcpu->nitrodata.id[0] = '\0';
	vcpu->nitrodata.sysenter_cs_val = 0;
	vcpu->nitrodata.efer_val = 0;
	vcpu->nitrodata.idt_int_offset = 0;
	vcpu->nitrodata.idt_replaced_offset = 0;
	//vcpu->nitrodata.pae = 0;
	vcpu->nitrodata.mode = UNDEF;
	vcpu->nitrodata.idt_entry_size = 0;
	vcpu->nitrodata.no_int = 0;
	vcpu->nitrodata.syscall_reg = VCPU_REGS_RAX;
#ifdef JADU_SKIP_SYSCALLS
    vcpu->nitrodata.skipped_syscalls = 0;
#endif
    vcpu->nitrodata.scall_count = 0;
    vcpu->nitrodata.sret_count = 0;
    vcpu->nitrodata.scallm_time = 0;
    vcpu->nitrodata.sretm_time = 0;
    vcpu->nitrodata.scallu_time = 0;
    vcpu->nitrodata.sretu_time = 0;
    vcpu->nitrodata.scalli_time = 0;
    vcpu->nitrodata.sreti_time = 0;
    vcpu->nitrodata.pnetlink = 0;
#ifdef SHADOW_IDT
	vcpu->nitrodata.shadow_idt.base = 0;
	vcpu->nitrodata.shadow_idt.limit = 0;
	vcpu->nitrodata.shadow_idt.table = 0;
#endif
#ifdef VCPU_PAIRSBUF
    vcpu->nitrodata.pairsbuf = NULL;
    vcpu->nitrodata.pairsnum = 0;
#endif
   
#ifdef COMPUTE_BETWEEN_IO
    vcpu->nitrodata.compute_time = 0;
    getnstimeofday(&vcpu->nitrodata.compute_time_start);
    vcpu->nitrodata.io_amount = 0;
#endif

    if(vcpu->nitrodata.copybuf != NULL){
        my_kfree(vcpu->nitrodata.copybuf,COPYZEROBUFSIZE);
    }
    vcpu->nitrodata.copybuf = my_kzalloc(COPYZEROBUFSIZE,GFP_KERNEL);
    if(vcpu->nitrodata.zerobuf != NULL){
        my_kfree(vcpu->nitrodata.zerobuf,COPYZEROBUFSIZE);
    }
    vcpu->nitrodata.zerobuf = my_kzalloc(COPYZEROBUFSIZE,GFP_KERNEL);

    nitro_vcpu_init(vcpu);

#ifdef SYS_STATS
    vcpu->nitrodata.ss = (struct sysstats*)my_kzalloc(sizeof(struct sysstats) * MAX_STATS, GFP_KERNEL);
    //memset(vcpu->nitrodata.ss,0,sizeof(struct sysstats)*MAX_STATS);
    for(i=0;i<MAX_STATS;i++){
        vcpu->nitrodata.ss[i].mintime = INT_MAX;
    }
#endif
	return 0;
}

int nitro_kvm_exit(struct kvm_vcpu *vcpu){
    if(vcpu->nitrodata.copybuf != NULL){
        my_kfree(vcpu->nitrodata.copybuf,COPYZEROBUFSIZE);
    }
    vcpu->nitrodata.copybuf = NULL;
    if(vcpu->nitrodata.zerobuf != NULL){
        my_kfree(vcpu->nitrodata.zerobuf,COPYZEROBUFSIZE);
    }
    vcpu->nitrodata.zerobuf = NULL;

#ifdef SHADOW_IDT
	if(vcpu->nitrodata.shadow_idt.table != 0){
		my_kfree(vcpu->nitrodata.shadow_idt.table,XXX);
	}
#endif
#ifdef SYS_STATS
    if(vcpu->nitrodata.ss != 0){
        my_kfree(vcpu->nitrodata.ss,sizeof(struct sysstats)*MAX_STATS);
    }
#endif
#ifdef VCPU_PAIRSBUF
    if(vcpu->nitrodata.pairsbuf != NULL){
        my_kfree(vcpu->nitrodata.pairsbuf,vcpu->nitrodata.pairsnum);
        vcpu->nitrodata.pairsbuf = NULL;
    }
    vcpu->nitrodata.pairsnum = 0;
#endif
	return 0;
}

void clear_splitcalls(void){
    unsigned long tid;
    struct nitrodatau *nu = NULL;
    while(ht_pop_val(ht_splitcalls,&tid, (unsigned long*)&nu) != -1){
        jprinte("ERROR unmatched syscall id " IDFORMAT " syscall %ld \n", tid, nu, nu->out_syscall);
        kmem_cache_free(nucache,nu);
    }
    while(ht_pop_val(ht_sig_nu_stash,&tid, (unsigned long*)&nu) != -1){
        jprinte("ERROR unmatched signal stash id " IDFORMAT " syscall %ld \n", tid, nu, nu->out_syscall);
        kmem_cache_free(nucache,nu);
    }
}
        
void delete_splitcalls(void){
    clear_splitcalls();
    ht_destroy(ht_splitcalls);
    ht_splitcalls = NULL;
    ht_destroy(ht_sig_nu_stash);
    ht_sig_nu_stash = NULL;
}


int nitro_mod_init(void){
    
    process_cr3s_bloom = 0;
    recalc_syscall_bloom();
    
    hash_init();

    leo_kvm_debug = (void*)leo_kvm_debug_func;

    if(ht_mmap_process == NULL){
        ht_create_with_size(&ht_mmap_process,"mmap_process",1000);
    }
    if(ht_mon_process == NULL){
        ht_create_with_size(&ht_mon_process,"mon_process",1000);
    }
    if(ht_mon_pids == NULL){
        ht_create_with_size(&ht_mon_pids,"mon_pids",1000);
    }
    if(ht_mmap_gpas == NULL){
        ht_create_with_size(&ht_mmap_gpas,"mmap_gpas",1000);
    }
    if(ht_mmap_gpas_ptep == NULL){
        ht_create_with_size(&ht_mmap_gpas_ptep,"mmap_gpas_ptep",1000);
    }
    if(ht_rw_gpas == NULL){
        ht_create_with_size(&ht_rw_gpas,"rw_gpas",1000);
    }
    if(ht_rw_gpas_ptep == NULL){
        ht_create_with_size(&ht_rw_gpas_ptep,"rw_gpas_ptep",1000);
    }
    if(ht_tracked_gpas == NULL){
        ht_create_with_size(&ht_tracked_gpas,"tracked_gpas",1000);
    }
    if(ht_tracked_gfns == NULL){
        ht_create_with_size(&ht_tracked_gfns,"tracked_gfns",1000);
    }
    if(ht_new_pids == NULL){
        ht_create_with_size(&ht_new_pids,"newpids",100);
    }
    if(ht_fork_parent_pids == NULL){
        ht_create_with_size(&ht_fork_parent_pids,"fork_pids",100);
    }
    if(ht_exec_cr3s == NULL){
        ht_create_with_size(&ht_exec_cr3s,"exec_cr3s",1000);
    }
    //ht_set_debug(ht_exec_cr3s);
    if(ht_new_process_tracking == NULL){
        ht_create_with_size(&ht_new_process_tracking,"newprocess",100);
    }

    if(ht_cr3_to_pid == NULL){
        ht_create_with_size(&ht_cr3_to_pid,"cr3_pid",1000);
    }
    if(ht_sig_nu_stash == NULL){
        ht_create_with_size(&ht_sig_nu_stash,"sig_nu_stash",100);
    }
    if(ht_new_pids_fdinherit == NULL){
        ht_create_with_size(&ht_new_pids_fdinherit,"newpidsfdinherit",100);
    }
    if(ht_file_sizes == NULL){
        ht_create_with_size(&ht_file_sizes,"fsizes",100000);
    }
    if(ht_file_eof_guess == NULL){
        ht_create_with_size(&ht_file_eof_guess,"feofs",10000);
    }
    if(ht_file_lastwrite_content == NULL){
        ht_create_with_size(&ht_file_lastwrite_content,"lastwrite",1000);
    }

    if(ht_splitcalls == NULL){
        ht_create_with_size(&ht_splitcalls,"splitcalls",1000);
    }
#if defined(BLOCK_LIFETIME) || defined(WRITE_DELAY)
    if(ht_blocklifetime == NULL){
        ht_create_with_size(&ht_blocklifetime,"blklife",500000);
    }
#endif
#ifdef BLOCK_LIFETIME
    if(ht_series_blocklifetime == NULL){
        ht_create_with_size(&ht_series_blocklifetime,"seriesblklife",500000);
    }
#endif
#ifdef FILE_ACCESSPATTERN
    if(ht_fdaccesspattern == NULL){
        ht_create_with_size(&ht_fdaccesspattern,"fdaccesspattern",500000);
    }
    if(ht_series_fdaccesspattern == NULL){
        ht_create_with_size(&ht_series_fdaccesspattern,"seriesfdaccess",500000);
    }
#endif
    if(nucache == NULL){
        nucache = kmem_cache_create("nucache",sizeof(struct nitrodatau),0,SLAB_RED_ZONE, NULL);
        if(!nucache){
            dpanic("ERROR: could not allocate nitrodatau kmem cache alloc \n");
        }
    }
	return 0;
}

int nitro_mod_exit(void){

    if(ht_mmap_process != NULL){
        ht_destroy(ht_mmap_process);
        ht_mmap_process = NULL;
    }
    if(ht_mon_process != NULL){
        ht_destroy(ht_mon_process);
        ht_mon_process = NULL;
    }
    if(ht_mon_pids != NULL){
        ht_destroy(ht_mon_pids);
        ht_mon_pids = NULL;
    }
    if(ht_mmap_gpas != NULL){
        ht_destroy(ht_mmap_gpas);
        ht_mmap_gpas = NULL;
    }
    if(ht_mmap_gpas_ptep != NULL){
        ht_destroy(ht_mmap_gpas_ptep);
        ht_mmap_gpas_ptep = NULL;
    }
    if(ht_rw_gpas != NULL){
        ht_destroy(ht_rw_gpas);
        ht_rw_gpas = NULL;
    }
    if(ht_rw_gpas_ptep != NULL){
        ht_destroy(ht_rw_gpas_ptep);
        ht_rw_gpas_ptep = NULL;
    }
    if(ht_tracked_gpas != NULL){
        ht_destroy(ht_tracked_gpas);
        ht_tracked_gpas = NULL;
    }
    if(ht_tracked_gfns != NULL){
        ht_destroy(ht_tracked_gfns);
        ht_tracked_gfns = NULL;
    }
    if(ht_new_pids != NULL){
        ht_destroy(ht_new_pids);
        ht_new_pids = NULL;
    }
    if(ht_fork_parent_pids != NULL){
        ht_destroy(ht_fork_parent_pids);
        ht_fork_parent_pids = NULL;
    }
    if(ht_exec_cr3s != NULL){
        ht_destroy(ht_exec_cr3s);
        ht_exec_cr3s = NULL;
    }
    if(ht_new_process_tracking != NULL){
        ht_destroy(ht_new_process_tracking);
        ht_new_process_tracking = NULL;
    }
    if(ht_cr3_to_pid != NULL){
        ht_destroy(ht_cr3_to_pid);
        ht_cr3_to_pid = NULL;
    }
    if(ht_new_pids_fdinherit != NULL){
        ht_destroy(ht_new_pids_fdinherit);
        ht_new_pids_fdinherit = NULL;
    }
    if(ht_file_sizes != NULL){
        ht_destroy(ht_file_sizes);
        ht_file_sizes = NULL;
    }
    if(ht_file_eof_guess != NULL){
        ht_destroy(ht_file_eof_guess);
        ht_file_eof_guess = NULL;
    }
    if(ht_file_lastwrite_content != NULL){
        ht_destroy(ht_file_lastwrite_content);
        ht_file_lastwrite_content = NULL;
    }
    if(ht_splitcalls != NULL){
        delete_splitcalls();
    }

#if defined(BLOCK_LIFETIME) || defined(WRITE_DELAY)
    if(ht_blocklifetime != NULL){
        ht_destroy(ht_blocklifetime);
    }
#endif
#ifdef BLOCK_LIFETIME
    if(ht_series_blocklifetime != NULL){
        ht_destroy(ht_series_blocklifetime);
    }
#endif
#ifdef FILE_ACCESSPATTERN
    if(ht_fdaccesspattern != NULL){
        ht_destroy(ht_fdaccesspattern);
    }
    if(ht_series_fdaccesspattern != NULL){
        ht_destroy(ht_series_fdaccesspattern);
    }
#endif
    
    if(nucache != NULL){
       kmem_cache_destroy(nucache);
       nucache = NULL;
    }

    hash_cleanup();

	return 0;
}

static DEFINE_SPINLOCK(nitro_lock);

struct mylock{
    spinlock_t* lock;
    unsigned long* flags;
    int op;
};

long my_helper(void* vm){
    struct mylock* m = (struct mylock*)vm;
    if(m->op==1){
        spin_lock_irqsave(m->lock,(*(m->flags)));
    }else if(m->op ==2){
        spin_unlock_irqrestore(m->lock,(*(m->flags)));
    }else{
        return -1;
    }
    return 0;
}

void my_lock(spinlock_t* lock,unsigned long* flags){
    struct mylock l;
    l.lock = lock;
    l.flags = flags;
    l.op = 1;
    my_helper(&l);
}

void my_unlock(spinlock_t* lock,unsigned long* flags){
    struct mylock l;
    l.lock = lock;
    l.flags = flags;
    l.op = 2;
    my_helper(&l);
}

/*
int start_nitro_one(struct kvm_vcpu* vcpu){
    int ret = 0;
    unsigned long flags;

    my_lock(&nitro_lock,&flags);
    ret = start_nitro(vcpu);
    my_unlock(&nitro_lock,&flags);

    return ret;
}

int stop_nitro_one(struct kvm_vcpu* vcpu){
    int ret = 0;
    unsigned long flags;

    my_lock(&nitro_lock,&flags);
    ret = stop_nitro(vcpu);
    my_unlock(&nitro_lock,&flags);

    return ret;
}
*/

int start_nitro(struct kvm_vcpu *vcpu, char prefix, unsigned long newcr3){
	struct kvm_sregs sregs;
	//u8 *idt;
	//u64 idt_base,efer;
	//u32 error;
	//unsigned long cr4,cr0;
    //struct x86_exception exception;
    int64_t idt_index = 128;

    //tracing_on();
    if(vcpu->nitrodata.running == 1)
        return 0;
    
    jprintk("starting nitro prefix %c vcpuid %d currentcr3 %lx newcr3 %lx CPU %d \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, newcr3, smp_processor_id());

	//printk( KERN_ERR "%d %s starting nitro on CPU %d vcpuid %d cr3 %lx\n", current->pid, current->comm, smp_processor_id(), vcpu->vcpu_id, newcr3);

	if(!is_protmode(vcpu)){
	//	printk( KERN_ERR "kvm:start_syscall_trace: ERROR: guest is running in real mode, nitro can not function.\n");
		return 3;
	}

	vcpu->nitrodata.mode = PROT;
	vcpu->nitrodata.idt_entry_size=8;

	if(is_pae(vcpu)){
		vcpu->nitrodata.mode = PAE;
		//printk( KERN_ERR "kvm:start_syscall_trace: system running in PAE mode\n");
	}
	if(is_long_mode(vcpu)){
		vcpu->nitrodata.mode = LONG;
		vcpu->nitrodata.idt_entry_size=16;
		//printk( KERN_ERR "kvm:start_syscall_trace: system running in long mode (x86_64)\n");
	}

	//vcpu_load(vcpu);
	kvm_arch_vcpu_ioctl_get_sregs(vcpu,&sregs);
	//vcpu_put(vcpu);

	if(idt_index == 0){
		vcpu->nitrodata.no_int = 1;
	}
	else if(idt_index<32 || idt_index>(sregs.idt.limit+1)/vcpu->nitrodata.idt_entry_size){
		jprinte("kvm:start_syscall_trace: ERROR: invalid idt_index passed, start will be aborted.\n");
		return 2;
	}
	else{
		vcpu->nitrodata.idt_int_offset = (u8) idt_index;
	}

	// set syscall_reg
    vcpu->nitrodata.syscall_reg = VCPU_REGS_RAX;
	//printk( KERN_ERR "kvm:start_syscall_trace: starting syscall trace with syscall_reg = %d, name='%s'\n", vcpu->nitrodata.syscall_reg, syscall_reg);

    vcpu->nitrodata.running = 1;
    //printk( KERN_ERR "kvm:nitro enabled on %d cpus.\n",vcpu->nitrodata.running);

/*
	//code to set #GP trap
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
	//kvm_for_each_vcpu(i, vcpu, kvm){
	//	vcpu_load(vcpu);
	//	kvm_x86_ops->set_gp_trap(vcpu);
	//	printk( KERN_ERR "kvm:start_syscall_trace: cpu%d: GP trap set\n",i);
	//	vcpu_put(vcpu);

		//i++;
	//}


	//code to cause sysenter to cause #GP
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
	kvm_for_each_vcpu(i, vcpu, kvm){
		vcpu_load(vcpu);
		kvm_x86_ops->get_msr(vcpu, MSR_IA32_SYSENTER_CS, &(vcpu->nitrodata.sysenter_cs_val));
		kvm_x86_ops->set_msr(vcpu, MSR_IA32_SYSENTER_CS, 0);
		vcpu_put(vcpu);

		//i++;
	}
*/

	//code to cause syscall to cause #UD (64 bit ubuntu)
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
    

	//kvm_for_each_vcpu(i, vcpu, kvm){
	//	vcpu_load(vcpu);
		kvm_get_msr_common(vcpu, MSR_EFER, &(vcpu->nitrodata.efer_val));
        struct msr_data efer_info;
	    efer_info.host_initiated = true;
        efer_info.index = MSR_EFER;
        efer_info.data = (vcpu->nitrodata.efer_val & ~EFER_SCE);
		kvm_set_msr_common(vcpu, &efer_info);
	//	vcpu_put(vcpu);
	//}

/*

#ifdef SHADOW_IDT
	//extern int emulator_read_emulated(unsigned long addr, void *val, unsigned int bytes, unsigned int *error_code, struct kvm_vcpu *vcpu)

	if(!vcpu->nitrodata.no_int){
		kvm_for_each_vcpu(i, vcpu, kvm){
			kvm_arch_vcpu_ioctl_get_sregs(vcpu,&sregs);
			if(vcpu->nitrodata.shadow_idt.base == 0){
				vcpu->nitrodata.shadow_idt.base = sregs.idt.base;
				vcpu->nitrodata.shadow_idt.limit = sregs.idt.limit;
				vcpu->nitrodata.shadow_idt.table = my_kmalloc(sregs.idt.limit + 1,GFP_KERNEL);
				memset(vcpu->nitrodata.shadow_idt.table,0,sregs.idt.limit + 1);
				kvm_read_guest_virt_system(sregs.idt.base,vcpu->nitrodata.shadow_idt.table,(unsigned int)(sregs.idt.limit + 1),vcpu,&error);
			}
			sregs.idt.limit=32*vcpu->nitrodata.idt_entry_size-1;
			kvm_arch_vcpu_ioctl_set_sregs(vcpu,&sregs);
		}
	}
#else

	//old code to cause int x to cause #GP/#NP
	//i=0;

	idt_base = 0;

	if(!vcpu->nitrodata.no_int){

		//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
		kvm_for_each_vcpu(i, vcpu, kvm){
			kvm_arch_vcpu_ioctl_get_sregs(vcpu,&sregs);

			if(sregs.idt.base != idt_base){
				idt_base = sregs.idt.base;

				idt = my_kmalloc(sregs.idt.limit + 1,GFP_KERNEL);
				memset(idt,0,sregs.idt.limit + 1);
				//kvm_read_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit + 1));
				kvm_read_guest_virt(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				//printk( KERN_ERR "kvm:start_syscall_trace: idt size: %d\n", (unsigned int)(sregs.idt.limit + 1));

				vcpu->nitrodata.idt_replaced_offset = 0x81;

				//for(j=32;j<(sregs.idt.limit + 1)/vcpu->nitrodata.idt_entry_size;j++){
				for(j=((sregs.idt.limit + 1)/vcpu->nitrodata.idt_entry_size) - 1; j>=32;j--){
					//printk( KERN_ERR "kvm:start_syscall_trace: checking IDT gate 0x%hX, p=0x%X, seg. sel.=%hu\n",j,(idt[(j*vcpu->nitrodata.idt_entry_size)+5] & 0x80),*((u16*) (idt +  (INT_OFFSET*vcpu->nitrodata.idt_entry_size) + 2)));
					if((idt[(j*vcpu->nitrodata.idt_entry_size)+5] & 0x80) == 0){
						vcpu->nitrodata.idt_replaced_offset = (u8)j;
						break;
					}
				}

				printk( KERN_ERR "kvm:start_syscall_trace: using empty gate 0x%hX\n",vcpu->nitrodata.idt_replaced_offset);

				memcpy(idt + (vcpu->nitrodata.idt_replaced_offset*vcpu->nitrodata.idt_entry_size), idt + (vcpu->nitrodata.idt_int_offset*vcpu->nitrodata.idt_entry_size), vcpu->nitrodata.idt_entry_size);

				*((u16*) (idt +  (vcpu->nitrodata.idt_int_offset*vcpu->nitrodata.idt_entry_size) + 2)) = DUM_SEG_SELECT;  //set selector
				//idt[(INT_OFFSET*vcpu->nitrodata.idt_entry_size) + 5] &= 0x7F;  //unset present bit

				//kvm_write_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit));
				kvm_write_guest_virt_system(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				my_kfree(idt);
			}
		}

	}
#endif
    */

	return 0;
}

int stop_nitro(struct kvm_vcpu* vcpu, char prefix, unsigned long newcr3){
	//struct kvm_sregs sregs;
	//u8 *idt;
	//u64 idt_base;
	//u32 error;
    //struct x86_exception exception;
    
    //tracing_off();

	if(!vcpu->nitrodata.running){
		//printk( KERN_ERR "kvm:stop_syscall_trace: WARNING: nitro is not started, stop will be aborted.\n");
		return 0;
	}
    
    jprintk("stopping nitro prefix %c vcpuid %d currentcr3 %lx newcr3 %lx CPU %d \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, newcr3, smp_processor_id());
	
    //printk( KERN_ERR "%d %s stopping nitro on CPU %d vcpuid %d cr3 %lx\n", current->pid, current->comm, smp_processor_id(), vcpu->vcpu_id, newcr3);
/*
	//code to unset #GP trap
	//i=0;
	//while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
	//	vcpu_load(kvm->vcpus[i]);
	//	kvm_x86_ops->unset_gp_trap(kvm->vcpus[i]);
	//	printk( KERN_ERR "kvm:start_syscall_trace: cpu%d: GP trap unset\n",i);
	//	vcpu_put(kvm->vcpus[i]);

	//	i++;
	//}


	//code to cause sysenter not to cause #GP
	i=0;
	while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
		vcpu_load(kvm->vcpus[i]);
		kvm_x86_ops->set_msr(kvm->vcpus[i], MSR_IA32_SYSENTER_CS, vcpu->nitrodata.sysenter_cs_val);
		vcpu_put(kvm->vcpus[i]);

		i++;
	}
*/

	//code to cause syscall not to cause #UD (64 bit ubuntu)
	//kvm_for_each_vcpu(i, vcpu, kvm){
	//	vcpu_load(vcpu);
        struct msr_data efer_info;
	    efer_info.host_initiated = true;
        efer_info.index = MSR_EFER;
        efer_info.data = (vcpu->nitrodata.efer_val);
		kvm_set_msr_common(vcpu, &efer_info);
	//	vcpu_put(vcpu);
	//}
	
    
    vcpu->nitrodata.running = 0;

    

/*
#ifdef SHADOW_IDT

#else

	//old code to cause int x not to cause #GP/#NP

	i=0;
	idt_base = 0;

	if(!vcpu->nitrodata.no_int){

		while(kvm->vcpus[i] && i<KVM_MAX_VCPUS){
			kvm_arch_vcpu_ioctl_get_sregs(kvm->vcpus[i],&sregs);

			if(sregs.idt.base != idt_base){
				idt_base = sregs.idt.base;

				idt = my_kmalloc(sregs.idt.limit + 1,GFP_KERNEL);
				memset(idt,0,sregs.idt.limit + 1);
				//kvm_read_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit + 1));
				kvm_read_guest_virt(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				memcpy(idt + (vcpu->nitrodata.idt_int_offset*vcpu->nitrodata.idt_entry_size), idt + (vcpu->nitrodata.idt_replaced_offset*vcpu->nitrodata.idt_entry_size), vcpu->nitrodata.idt_entry_size);

				//kvm_write_guest(kvm,kvm->vcpus[i]->arch.mmu.gva_to_gpa(kvm->vcpus[i],sregs.idt.base),idt,(unsigned long)(sregs.idt.limit));
				kvm_write_guest_virt_system(&(kvm->vcpus[i]->arch.emulate_ctxt),sregs.idt.base,idt,(unsigned int)(sregs.idt.limit + 1),&exception);

				my_kfree(idt);
			}

			i++;
		}

	}
#endif
*/

	return 0;
}

int start_nitro_all(char prefix){
    struct kvm* kvm;
    struct kvm_vcpu* vcpu;
    int ret = 0,i;
    unsigned long flags;

    list_for_each_entry(kvm, &vm_list, vm_list){
        my_lock(&nitro_lock,&flags);
	    kvm_for_each_vcpu(i, vcpu, kvm){
            ret |= start_nitro(vcpu,prefix,0);
        }
        my_unlock(&nitro_lock,&flags);
    }
    
    return ret;
}

int stop_nitro_all(char prefix){
    struct kvm* kvm;
    struct kvm_vcpu* vcpu;
    int ret = 0,i;
    unsigned long flags;

    list_for_each_entry(kvm, &vm_list, vm_list){
        my_lock(&nitro_lock,&flags);
        kvm_for_each_vcpu(i, vcpu, kvm){
            ret |= stop_nitro(vcpu,prefix,0);
        }
        my_unlock(&nitro_lock,&flags);
    }

    return ret;
}

int kvm_read_guest_virt_system(struct x86_emulate_ctxt *ctxt, gva_t addr, void *val, unsigned int bytes,struct x86_exception *exception);

DEFINE_RWLOCK(mon_lock);

#define  JADU_GET_RSP_ID(cr3,rsp) (((cr3>>12) << 48) | (((rsp<<16)>>16)))

#ifdef BSD
#define IDCACHE_ENABLE_THREADCOUNT (4)
#else
#define IDCACHE_ENABLE_THREADCOUNT (2)
#endif

unsigned long get_guest_pid_helper(struct kvm_vcpu* vcpu, bool ignorecachedval){
    /* old implementation dependent on little bit of guest OS info and needs a kernel module in guest*/    
    //    struct x86_exception ex;
    //    unsigned long taskptr;
    //    int pid = -1;
    //    if(vcpu->nitrodata.task_ptrptr){
    //        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, vcpu->nitrodata.task_ptrptr, &taskptr, sizeof(unsigned long),&ex);
    //        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + PID_OFFSET, &pid, sizeof(int),&ex);
    //    }
    //    return pid;
    unsigned long rsp = kvm_register_read(vcpu, VCPU_REGS_RSP);
    struct process_data* m = NULL;
    unsigned long cr3 = vcpu->arch.cr3;
    unsigned long pid, cpid = 0;
    unsigned long matched_stack = 0, matched_stacksize = 0;
    bool idcache_update = false;

    //idcache for mysqld style processes with many threads
    if( !ignorecachedval && ((vcpu->nitrodata.idcache_cr3 == cr3) && (rsp>vcpu->nitrodata.idcache_start_rsp) && (rsp<vcpu->nitrodata.idcache_end_rsp))){
        return vcpu->nitrodata.idcache_id;
    }

    read_lock(&mon_lock);
    if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) != 0){
        unsigned long stack, stackval, stacksize;
        hash_table *ht_threads = NULL;
        unsigned long farthest = ULONG_MAX; 
        
        if(m->pid != 0){
            if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
                if(ht_get_size(ht_threads) > IDCACHE_ENABLE_THREADCOUNT){
                    idcache_update = true;
                }

                ht_open_scan(ht_threads);
                while(ht_scan_val(ht_threads, &pid, &stackval) != -1){
                    short updated = 0;
                    stacksize = ((stackval & (THREADSTACKMASK)))*(STACK_SIZE_FACTOR);
                    stack = (stackval & (~THREADSTACKMASK)); 
                    if(stacksize ==0){
                        jprinte("ERROR stacksize is zero\n");
                    }
                    //if(pid != m->pid){
                        //if(rsp <= stack && (!m->stacksize || (rsp >= (stack - m->stacksize)))){
                        //    found = true;
                        //    break;
                        //}
                        if( (rsp <= stack) && (((stack-rsp)<=stacksize))  &&  ((stack-rsp) < farthest) ){
                            cpid = pid;
                            farthest = (stack-rsp);
                            updated = 1;
                            if(idcache_update == true){
                                matched_stack = stack;
                                matched_stacksize = stacksize;
                            }
                        }
                        jprintk("%s farthest, thread pid %lu stackbase %lx rsp %lx stacksize %lu stackval %lx \n", updated?"updated":"not updated",pid, stack, rsp, stacksize, stackval);
                    //}
                }
                ht_close_scan(ht_threads);
                //return found?cpid:m->pid;
                //if(cpid == 0){
                //    jprinte("ERROR: cpid is zero cr3 %lx\n",vcpu->arch.cr3);
                //}
                //return cpid;
            }
        }else{
            //jprintm("ERROR m->pid is zero\n");
        }
    }
    read_unlock(&mon_lock);
    
    if(cpid != 0){
        if(idcache_update == true){
            vcpu->nitrodata.idcache_cr3 = cr3;
            vcpu->nitrodata.idcache_start_rsp = matched_stack-matched_stacksize;
            vcpu->nitrodata.idcache_end_rsp = matched_stack;
            vcpu->nitrodata.idcache_id = cpid;
        }
        return cpid;
    }
    
    if(lookup_val_cr3_to_pid(cr3,&pid)){
        jprintk("getguestpid in cr3topid process \n");
        return pid;
    }else{
        jprintk("getguestpid in rsp \n");
        return JADU_GET_RSP_ID(cr3,rsp);
    }
}

inline unsigned long get_guest_pid(struct kvm_vcpu* vcpu){
    return get_guest_pid_helper(vcpu,false);
}

inline unsigned long get_guest_pid_ignorecachedval(struct kvm_vcpu* vcpu){
    return get_guest_pid_helper(vcpu,true);
}


#define COMM_LEN (16)

int is_guest_comm_known(struct kvm_vcpu* vcpu){
    struct process_data* m = NULL;
    unsigned long cr3 = vcpu->arch.cr3;
    if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) != 0){
        return m->cmd[0];
    }
    return 0;
}

void get_guest_comm(struct kvm_vcpu* vcpu, char* comm){
    //old version before removing dependence on guest OS info
    //struct x86_exception ex;
    //unsigned long taskptr;
    //if(vcpu->nitrodata.task_ptrptr){
    //    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, vcpu->nitrodata.task_ptrptr, &taskptr, sizeof(unsigned long),&ex);
    //    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + COMM_OFFSET, comm, sizeof(char)*COMM_LEN,&ex);
    //}else{
    //    comm[0] = '\0';
    //}
    struct process_data* m = NULL;
    unsigned long cr3 = vcpu->arch.cr3;
    comm[0] = '\0';
    if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) != 0){
        if(m->cmd[0])
            memcpy(comm,m->cmd,COMM_LEN);
    }
}

/*
void print_all_guest_pids(struct kvm_vcpu* vcpu){
    int i;
    int pid;
    char comm[64];
    unsigned long taskptr;
    struct kvm* kvm;
    struct kvm_vcpu* tvcpu = NULL;
    struct x86_exception ex;
    list_for_each_entry(kvm, &vm_list, vm_list){
        kvm_for_each_vcpu(i, tvcpu, kvm){
            if(tvcpu->nitrodata.task_ptrptr){
                kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, tvcpu->nitrodata.task_ptrptr, &taskptr, sizeof(unsigned long),&ex);
                kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + PID_OFFSET, &pid, sizeof(int),&ex);
	            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, taskptr + COMM_OFFSET, comm, sizeof(char)*16,&ex);
                jprintk("guest pid vcpu %d taskptrptr %lx taskptr %lx pid %d comm %s \n",tvcpu->vcpu_id,tvcpu->nitrodata.task_ptrptr,taskptr,pid,comm);
            }
        }
    }
}
*/

void get_process_hardware_id(struct kvm_vcpu *vcpu, unsigned long *cr3, u32 *verifier, unsigned long *pde){
	unsigned long dir_base;
	u32 i;
	u32 pde_32;

	*verifier = 0;
	*cr3 = vcpu->arch.cr3;

	if (vcpu->nitrodata.mode == PAE){//PAE
		dir_base = (*cr3) & 0xFFFFFFFFFFFFFFE0;	//see section 4.3 in intel manual

		for (i=0;i<4*8;i+=8){
			kvm_read_guest(vcpu->kvm, dir_base+i, pde, 8);
			//printk( KERN_ERR "kvm:handle_gp: kvm_read_guest_virt_system error: %u\n",error);
			if(((*pde) & PT_PRESENT_MASK)){//  &&  !(pde & PT_WRITABLE_MASK)){
				*verifier=i;
				goto FOUND;
			}
		}
	}
	else if (vcpu->nitrodata.mode == LONG){//IA-32E
		dir_base = (*cr3) & 0x000FFFFFFFFFF000;	//see section 4.3 in intel manual

		for (i=0;i<512*8;i+=8){
			kvm_read_guest(vcpu->kvm, dir_base+i, pde, 8);
			//printk( KERN_ERR "kvm:handle_gp: kvm_read_guest_virt_system error: %u\n",error);
			if(((*pde) & PT_PRESENT_MASK)){//  &&  !(pde & PT_WRITABLE_MASK)){
				*verifier=i;
				goto FOUND;
			}
		}
	}
	else{//32-bit Protected
		dir_base = (*cr3) & 0xFFFFFFFFFFFFF000;  	//see section 4.3 in intel manual

		for (i=0;i<1024*4;i+=4){
			kvm_read_guest(vcpu->kvm, dir_base+i, &pde_32, 4);
			//printk( KERN_ERR "kvm:handle_gp: kvm_read_guest_virt_system error: %u\n",error);
			if((pde_32 & PT_PRESENT_MASK)){//  &&  !(pde & PT_WRITABLE_MASK)){
				*verifier=i;
				*pde = (unsigned long)pde_32;
				goto FOUND;
			}
		}
	}
	*pde=0;

FOUND:
	/* end copy and paste */
	return;
}

/*
int handle_gp(struct kvm_vcpu *vcpu, struct kvm_run *kvm_run){
	int er;

	printk( KERN_ERR "kvm:handle_gp: #GP trapped\n");

	if(!nitrodata.running)
		return 1;

	if(is_sysenter_sysreturn(vcpu)){//sysenter/sysreturn
		print_trace_proxy('f',vcpu);
		er = emulate_instruction(vcpu, 0, 0, 0);
		if (er != EMULATE_DONE){
			kvm_clear_exception_queue(vcpu);
			kvm_clear_interrupt_queue(vcpu);
			kvm_queue_exception_e(vcpu,GP_VECTOR,kvm_run->ex.error_code);
		}
	}
#ifdef SHADOW_IDT
	else if(vcpu->arch.interrupt.pending && vcpu->arch.interrupt.nr > 31){//int shadow_idt
		printk( KERN_ERR "trapped int 0x%X\n", vcpu->arch.interrupt.nr);
		DEBUG_PRINT("begin_int_handling: EIP is now 0x%08lX.\n", kvm_register_read(vcpu, VCPU_REGS_RIP))
		if (is_int(vcpu)) {
			er = emulate_instruction(vcpu, 0, 0, 0);
		} else {
			DEBUG_PRINT("Asynchronous interrupt detected.\n")
			er = handle_asynchronous_interrupt(vcpu);
		}
		kvm_clear_exception_queue(vcpu);
		kvm_clear_interrupt_queue(vcpu);
		if (er != EMULATE_DONE) {
			kvm_queue_exception_e(vcpu, GP_VECTOR, kvm_run->ex.error_code);
		}
	}
#else
	else if(((DUM_SEG_SELECT & 0xFFF8) == (kvm_run->ex.error_code & 0xFFF8)) && !nitrodata.no_int){ //int no shadow_idt
																		 //check if its our expected error code for int handling
																	     //(disregard bottom 3 bits as these are status)

		print_trace_proxy('i',vcpu);

		kvm_clear_exception_queue(vcpu);
		kvm_clear_interrupt_queue(vcpu);
		kvm_queue_interrupt(vcpu,nitrodata.idt_replaced_offset,true);
	}
#endif
	else{
		//printk( KERN_ERR "kvm:handle_gp: natural #GP trapped, EC=%u\n",kvm_run->ex.error_code);
		kvm_clear_exception_queue(vcpu);
		kvm_clear_interrupt_queue(vcpu);
		kvm_queue_exception_e(vcpu,GP_VECTOR,kvm_run->ex.error_code);
	}
	return 1;
}
EXPORT_SYMBOL(handle_gp);
*/

extern bool rmap_write_protect(struct kvm *kvm, u64 gfn);

extern void jadu_write_protect(struct kvm_vcpu *vcpu, gpa_t gpa);

/*
int nitro_test_helper(struct kvm_vcpu* vcpu){
    gpa_t pa;
    gfn_t fn;
    struct x86_exception ex;
    unsigned long val;
    //struct kvm_vcpu* vcpu;
    //unsigned long flags;
    leo_debug =1 ;
    if(tcr3 > 1 && vcpu->arch.cr3 == tcr3){ 
	    u32 access = (kvm_x86_ops->get_cpl(vcpu) == 3) ? PFERR_USER_MASK : 0;
		pa = vcpu->arch.walk_mmu->gva_to_gpa(vcpu, taddr, access,&ex);
        tpa = pa;
        fn = pa >> PAGE_SHIFT;
        kvm_read_guest_virt(&vcpu->arch.emulate_ctxt, taddr, &val, sizeof(unsigned long),&ex);
        printk( KERN_ERR "fn test input tcr3 %lx 0x%lx pa %lx fn %lx val %lx \n " , tcr3, taddr, pa , fn, val);
        jadu_write_protect(vcpu, pa);
        tcr3 = 1;
    }
    leo_debug = 0;
    return 0;
}

int nitro_test_helper1(struct kvm_vcpu* vcpu, unsigned long pa){
    gfn_t fn = pa >> PAGE_SHIFT;
    gfn_t tfn = tpa >> PAGE_SHIFT;
    if(leo_debug) printk( KERN_ERR " test_helper %s %s %d : 1 pa %lx tpa %lx fn %lx tfn %lx \n",  __FILE__ , __FUNCTION__ ,__LINE__,pa,tpa,fn,tfn);
    if(tcr3 == 1){
        if(leo_debug) printk( KERN_ERR "%s %s %d : 2 pa %lx tpa %lx fn %lx tfn %lx \n",  __FILE__ , __FUNCTION__ ,__LINE__,pa,tpa,fn,tfn);
        if(fn == tfn){ 
            printk( KERN_ERR "fn test helper trapped write protection taddr %lx  va %lx \n" , tpa, pa);
        }
        //printk( KERN_ERR "fn test helper trapped write protection taddr %lx  va %lx \n" , taddr, va);
        tcr3 = 0;
    }
    return 0;
}
*/

static DEFINE_SPINLOCK(mmap_lock);

int nextjiffies = 0;

struct process_data* get_process_data(unsigned long cr3){
    struct process_data* m = NULL;
    struct process_data* n = NULL;
    unsigned long ncr3;
    const int PROCESSDATA_CLEAROUT_MSECS = 10000;
    short clean_count = 3;
    int cjiffies = get_jiffies();
    int i;
    if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) == 0){
        unsigned long flags;
        spin_lock_irqsave(&mmap_lock,flags);
        if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) == 0){
            m = (struct process_data*) my_kmalloc(sizeof(struct process_data),GFP_ATOMIC);
            if(m == NULL){
                dpanic("kmalloc failure in mmap process\n");
            }
            spin_lock_init(&m->lock);
            m->skip_syscalls = false;
            m->just_added = 1;
            m->cloning = false;
            m->shutdown = false;
            m->dedup_war_prefetch = false;
            m->dedup_uniq_hint = false;
            m->expect_iret_handling = 0;
            m->pid = 0;
            m->cr3 = cr3;
            m->jiffies = cjiffies;
            ht_create_with_size(&m->v2p,"v2p",100);
            ht_create_with_size(&m->p2v,"p2v",100);
            ht_create_with_size(&m->pending,"pending",10);
            ht_create_with_size(&m->vbe,"vbe",10);
            ht_create_with_size(&m->mfds,"mfds",100);
            ht_create_with_size(&m->sigactions,"sigactions",10);
            ht_create_with_size(&m->fdoffsets,"fdoffsets",100);
            ht_add_val(ht_mmap_process,cr3,(unsigned long)m);
            for(i=0;i<MAX_THREADS_SMALL_IO;i++){
                m->buffer[i] = 0;
                m->bufferuse[i] = false;
                m->bufferfullalloc[i] = false;
            }
            m->cwd[0] = 0;
            m->cmd[0] = 0;
        }
        spin_unlock_irqrestore(&mmap_lock,flags);
    }
    m->jiffies = cjiffies;
    if(m==NULL){
        jprinte("ERROR process_data NULL for cr3 %lx \n" , cr3);
        dpanic(" process_data NULL\n");
    }

    int maxtdiff = 0;
    //cleanup process data
    if(cjiffies > nextjiffies){
        ht_open_scan(ht_mmap_process);
        while(ht_scan_val(ht_mmap_process,&ncr3, (unsigned long*)&n) != -1){
            if( !is_targeted_process(ncr3)){
                if(jiffies_to_msecs(cjiffies - n->jiffies) > PROCESSDATA_CLEAROUT_MSECS){
                    drop_process_data(ncr3);
                }else{
                    int tdiff = PROCESSDATA_CLEAROUT_MSECS - jiffies_to_msecs(cjiffies - n->jiffies); 
                    if(tdiff > maxtdiff){
                        maxtdiff = tdiff;
                    }
                }
            }
            clean_count--;
            if(clean_count <= 0) break;
        }
        ht_close_scan(ht_mmap_process);

        nextjiffies = cjiffies + maxtdiff;
    }

    return m;
}

void drop_process_data(unsigned long cr3){
    struct process_data* m = NULL;
    unsigned long flags;
    spin_lock_irqsave(&mmap_lock,flags);
    if(ht_remove_val(ht_mmap_process,cr3,(unsigned long*)(&m)) != -1){
        if(m->v2p){
            ht_destroy(m->v2p);
        }
        if(m->p2v){
            ht_destroy(m->p2v);
        }
        if(m->pending){
            ht_destroy(m->pending);
        }
        if(m->vbe){
            ht_destroy(m->vbe);
        }
        if(m->mfds){
            ht_destroy(m->mfds);
        }
        if(m->sigactions){
            ht_destroy(m->sigactions);
        }
        if(m->fdoffsets){
            ht_destroy(m->fdoffsets);
        }
        my_kfree(m,sizeof(struct process_data));
    }
    del_cr3_to_pid(cr3);
    spin_unlock_irqrestore(&mmap_lock,flags);
}

unsigned long cantor_pairing_fn(unsigned long x, unsigned long y){
    return (((x+y)*(x+y+1))/2)+y;
}

unsigned long get_new_file_hash(unsigned long dev, unsigned long inode,unsigned long size){
    unsigned long hash = cantor_pairing_fn(dev,inode);
    ht_update_val(ht_file_sizes,hash,size);
    return hash;
}

unsigned long get_filehash_fd(unsigned long cr3, unsigned long fd){
    unsigned long hash = 0;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->mfds,fd,&hash) ==0){
        jprinte("ERROR: missing file hash for fd %d \n",fd);
    }
    return hash;
}

int get_filesize_fd(unsigned long cr3, unsigned long fd, bool errlog){
    unsigned long hash,size;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->mfds,fd,&hash) !=0){
        if(ht_lookup_val(ht_file_sizes,hash,&size) != 0){
            return size;
        }else{
            if(errlog)
                jprinte("ERROR: missing file size for file hash %lx \n",hash);
        }
    }
    return 0;
}

unsigned long get_offset_fd(unsigned long cr3, unsigned long fd){
    unsigned long hash;
    unsigned long offset=~0;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->fdoffsets,fd,&offset) ==0){
        jprinte("ERROR: missing file offset for fd %lu \n",fd);
    }
    return offset;
}

unsigned long update_fd_offsets(unsigned long cr3, unsigned long fd, unsigned long newsize){
    unsigned long hash,size = 0;
    struct process_data* m = get_process_data(cr3);
    ht_update_val(m->fdoffsets,fd,newsize);
    if(ht_lookup_val(m->mfds,fd,&hash) !=0){
        if(ht_lookup_val(ht_file_sizes,hash,&size) == 0){
            //jprinte("ERROR: missing file size for file hash %lx \n",hash);
            ht_update_val(ht_file_sizes,hash,newsize);
        }else{
            if(filesize_to_ioclass(size) != filesize_to_ioclass(newsize)){
                jprintk("ioclass transition size %lu newsize %lu class %d newclass %d fd %lu hash %lx \n", size,newsize,filesize_to_ioclass(size),filesize_to_ioclass(newsize),fd,hash);
            }
            if(newsize > size){
                ht_update_val(ht_file_sizes,hash,newsize);
            }
        }
        
        if(ht_lookup_val(ht_file_eof_guess,hash,&size) != 0){
            if(newsize > size){
                ht_update_val(ht_file_eof_guess,hash,newsize);
            }
        }
    }
    return newsize;
}

unsigned long increment_fd_offsets(unsigned long cr3, unsigned long fd, unsigned long increment){
    unsigned long offset = 0;
    unsigned long newsize = 0;
    struct process_data* m = get_process_data(cr3);
    if(ht_lookup_val(m->fdoffsets,fd,&offset) == 0){
        jprinte("ERROR: cannot update offsets for fd %ld \n", fd);
        return 1;
    }
    newsize = offset + increment;
    return update_fd_offsets(cr3,fd,newsize);
}

void add_monitored_fd(unsigned long cr3, unsigned long fd,unsigned long hash){
    struct process_data* m = get_process_data(cr3);
    spin_lock(&m->lock);
    if(ht_remove(m->mfds,fd) != -1){
        //TODO: CLOSEONEXEC
        jprinte("ERROR already monitored fd remonitored\n");
    }
    ht_add_val(m->mfds,fd,hash);
    ht_add_val(m->fdoffsets,fd,0);
    jprintk( "added fd %d to monitored fds\n",fd);
    spin_unlock(&m->lock);
}


void del_monitored_fd(unsigned long cr3, unsigned long fd){
    struct process_data* m = get_process_data(cr3);
    unsigned long del_hash=0,fhash;

    if(is_monitored_fd(cr3,fd)){
#ifdef JADUKATA_CHECKSUM
#ifdef DETECT_OVERWRITES
        extern hash_table *ht_file_chunksums;
        fhash = get_filehash_fd(cr3,fd);
        if(fhash != 0){
            ht_open_scan(ht_file_chunksums);
            while( ht_scan(ht_file_chunksums,&del_hash) != -1 ){
                if( UNPACK_INTS_FROM_LONG_X(del_hash) == fhash ){
                    htn_remove(ht_file_chunksums,del_hash);
                }
            }
            ht_close_scan(ht_file_chunksums);
        }
#endif
#endif
        spin_lock(&m->lock);
        if(ht_remove(m->mfds,fd) == -1){
            jprinte("ERROR no fd found to unmonitor\n");
        }else{
            jprintk( "removed fd %d from monitored fds\n",fd);
        }
        if(ht_remove(m->fdoffsets,fd) == -1){
            jprinte("ERROR no fd found to unmonitor\n");
        }else{
            jprintk( "removed fd %d from monitored offsets\n",fd);
        }
        spin_unlock(&m->lock);
    }
}

int is_monitored_fd(unsigned long cr3, unsigned long fd){
    struct process_data* m = get_process_data(cr3);
    return ht_lookup(m->mfds,fd);
}


void print_targeted_processes(void){
    int i = 1, j = 1;
    unsigned long cr3;
    unsigned long pid;
    hash_table *ht_threads = NULL;
    jprintk("leo %d processes are monitored \n",ht_mon_process->table->size);
    read_lock(&mon_lock);
    ht_open_scan(ht_mon_process);
    while(ht_scan(ht_mon_process, &cr3) != -1){
        if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
            ht_open_scan(ht_threads);
            j = 1;
            while(ht_scan(ht_threads, &pid) != -1){
                jprintk("leo monitored cr3 #%d is %lx pid #%d is %lu : ",i,cr3,j,pid);
                j++;
            }
            ht_close_scan(ht_threads);
        }else{
            jprinte( "ERROR no pids associated with targeted process cr3 %lx \n", cr3);
        }
        i++;
    }
    ht_close_scan(ht_mon_process);
    read_unlock(&mon_lock);
}

unsigned long targeted_process_pid_to_cr3(unsigned long tpid){
    unsigned long cr3 = 0;
    unsigned long pid;
    bool found = false;
    hash_table *ht_threads = NULL;
    read_lock(&mon_lock);
    ht_open_scan(ht_mon_process);
    while((found == false) && (ht_scan(ht_mon_process, &cr3) != -1)){
        if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
            ht_open_scan(ht_threads);
            while(ht_scan(ht_threads, &pid) != -1){
                if(pid == tpid){
                    found = true;
                    break;
                }
            }
            ht_close_scan(ht_threads);
        }else{
            jprinte( "ERROR no pids associated with targeted process cr3 %lx \n", cr3);
        }
    }
    ht_close_scan(ht_mon_process);
    read_unlock(&mon_lock);

    if(!found) 
        jprinte( "ERROR no process cr3 found for pid %d \n", tpid);

    return (found==true)?cr3:0;
}

struct process_data* cachem = NULL;

void add_skipped_process(unsigned long cr3){
#ifdef JADU_SKIP_SYSCALLS
    read_lock(&mon_lock);
    struct process_data* m = get_process_data(cr3);
    m->skip_syscalls = true;
    read_unlock(&mon_lock);
#endif
}

void remove_skipped_process(unsigned long cr3){
#ifdef JADU_SKIP_SYSCALLS
    read_lock(&mon_lock);
    struct process_data* m = get_process_data(cr3);
    m->skip_syscalls = false;
    if(cachem && cachem->cr3 == cr3){
        cachem = NULL;
    }
    read_unlock(&mon_lock);
#endif
}

bool is_skipped_process(unsigned long cr3){
#ifdef JADU_SKIP_SYSCALLS
    if( (cachem==NULL) || (cachem->cr3 != cr3) ){
        read_lock(&mon_lock);
        cachem = get_process_data(cr3);
        read_unlock(&mon_lock);
    }
    return cachem->skip_syscalls;
#endif
    return false;
}

int is_targeted_process(unsigned long cr3){
    if(ones_match(cr3,process_cr3s_bloom) ){
        return ht_lookup(ht_mon_process,cr3);
    }
    return 0;
}

void recalc_process_cr3s_bloom(void){
    unsigned long cr3;
    unsigned long new_process_cr3s_bloom = 0;
    ht_open_scan(ht_mon_process);
    while(ht_scan(ht_mon_process, &cr3) != -1){
        new_process_cr3s_bloom |= cr3;
    }
    ht_close_scan(ht_mon_process);
    process_cr3s_bloom = new_process_cr3s_bloom;
}

int add_targeted_process(unsigned long cr3,unsigned long parentcr3, unsigned long pid, unsigned long stackarg, unsigned long stacksize, int vfork, int precisestackbase){
    write_lock(&mon_lock);
    if(cr3 != parentcr3){
        if(ht_add_val(ht_mon_process,cr3,parentcr3) ==1){
            recalc_process_cr3s_bloom();
        }
    }
    
#ifdef BSD
    if(vfork == 0){ 
        ht_remove(ht_exec_cr3s,cr3);
    }
#else
    ht_remove(ht_exec_cr3s,pid);
#endif

    hash_table *ht_threads = NULL;
    if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) == 0){
        ht_create_with_size(&ht_threads, "ht_threads", 10);
        ht_add_val(ht_mon_pids,cr3,(unsigned long)ht_threads);
    }

    unsigned long stack;
    if(precisestackbase == 0){
        stack = ((stackarg >> THREADSTACKROUNDBITS) + 1UL)<<THREADSTACKROUNDBITS;
        if(vfork) stack -= (THREADSTACKMASK + 1UL);
    }else{
        stack = ((stackarg >> THREADSTACKMASKBITS) + 1UL)<<THREADSTACKMASKBITS;
    }

    unsigned long stackval = (stack | (stacksize/(STACK_SIZE_FACTOR)));
    ht_add_val(ht_threads,pid, stackval);

    jprintk("process cr3 %lx pid %lu added to monitored processes with stackpassed %lx stack %lx stacksize %ld stackhtval %lx vfork %d\n",cr3,pid, stackarg, stack, stacksize, stackval,vfork);
    struct process_data* m = get_process_data(cr3);
    m->just_added++;
    if(m->pid == 0){
        m->pid = pid;
    }
    
    del_cr3_to_pid_pid(pid);
    write_unlock(&mon_lock);

#ifdef NO_ON_OFF
    start_nitro_all('a');
#endif

    return 0;
}

int remove_targeted_process_core(unsigned long cr3, int pid){
    write_lock(&mon_lock);
    hash_table *ht_threads = NULL;
    int thread_count = 0;
    if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
        if(pid != -1){
            thread_count++;
            jprintl("thread pid %lu with process cr3 %lx removed from monitored processes\n",pid, cr3);
            //just remove one thread
            ht_remove(ht_threads,pid);
        }

        // if no more threads or if we want to remove whole process 
        if( (ht_get_size(ht_threads)<=0) || pid == -1){
            unsigned long tpid;
            if(ht_remove(ht_mon_process,cr3) != -1){
                unsigned long childcr3, parentcr3;
                //update any children with new dummy parentcr3

                //find all children
                ht_open_scan(ht_mon_process);
                while(ht_scan_val(ht_mon_process, &childcr3,&parentcr3) != -1){
                    if(parentcr3 == cr3){
                        htn_update_val(ht_mon_process,childcr3,UINT_MAX);
                    }
                }
                ht_close_scan(ht_mon_process);

                recalc_process_cr3s_bloom();
            }
            ht_remove(ht_mon_pids,cr3);

            while(ht_pop(ht_threads,&tpid) != -1){
                thread_count++;
                jprintl("thread pid %lu with process cr3 %lx removed from monitored processes\n",tpid, cr3);
            }
            ht_destroy(ht_threads);

            drop_process_data(cr3);
        }
        jprintl("process cr3 %lx removed from monitored processes total threads removed %d threads left %d\n",cr3, thread_count,ht_get_size(ht_threads));
    }else{
        jprinte("ERROR: no pids associated with process cr3 %lx pid %d \n", cr3, pid);
    }
    write_unlock(&mon_lock);

#ifdef NO_ON_OFF
    if(ht_get_size(ht_mon_process)<=0){
        stop_nitro_all('n');
    }
#endif
    return 0;
}

int remove_targeted_process(unsigned long cr3){
    unsigned long childcr3, parentcr3;
    
    hash_table *ht_children = NULL;
    ht_create_with_size(&ht_children, "ht_children", 10); 

    //find all children
    ht_open_scan(ht_mon_process);
    while(ht_scan_val(ht_mon_process, &childcr3,&parentcr3) != -1){
        if(parentcr3 == cr3){
            ht_add(ht_children,childcr3);
            htn_remove(ht_mon_process,childcr3);
        }
    }
    ht_close_scan(ht_mon_process);
    
    if(ht_get_size(ht_children) > 0){
        recalc_process_cr3s_bloom();

        //recursively remove children and their children if any
        ht_open_scan(ht_children);
        while(ht_scan(ht_children, &childcr3) != -1){
            remove_targeted_process(childcr3);
        }
        ht_close_scan(ht_children);
    }

    //remove any threads remaining in ht_new_pids yet to be tracked 
    //this happens with sh cloning again after a first clone failure
    // TODO: detect this failure case and handle it earlier
    if(ht_get_size(ht_new_pids) > 0){
        unsigned long pid;
        unsigned long parentcr3;
        ht_open_scan(ht_new_pids);
        while(ht_scan_val(ht_new_pids, &pid,&parentcr3) != -1){
            if(parentcr3 == cr3){
                hash_table* oldmfds = NULL;
                htn_remove(ht_new_pids,pid);
                if(htn_remove_val(ht_new_pids_fdinherit,pid,(unsigned long*)&oldmfds) != -1){
                    if(oldmfds){
                        ht_destroy(oldmfds);
                    }
                }
                jprinte("WARN REMOVED newly monitored process with pid %d and parentcr3 %lx because parent process is being removed \n", pid, parentcr3);
            }
        }
        ht_close_scan(ht_new_pids);
    }
        
    del_cr3_to_pid(cr3);

    clear_unmonitored_cr3_to_pid();

    if(ht_get_size(ht_new_pids) <= 0){
        clear_all_cr3_to_pid();
    }

    //finally remove self
    remove_targeted_process_core(cr3,-1);
    
    ht_destroy(ht_children);
    return 0;
}

int remove_all_targeted_processes(void){
    unsigned long childcr3, parentcr3;
    int ret;
    int count = ht_get_size(ht_mon_process);
    while(ht_get_size(ht_mon_process)>0){ 
        ret = -1;
        ht_open_scan(ht_mon_process);
        ret = ht_scan_val(ht_mon_process,&childcr3,&parentcr3);
        ht_close_scan(ht_mon_process);
        if(ret != -1){
            jprintk("removing all monitored processes: cleared process cr3 %lx from monitored processes\n",childcr3);
            remove_targeted_process(childcr3);
        }
    }
    recalc_process_cr3s_bloom();
    jprintk("removed all monitored processes: total %d\n", count);
    return 0;
}

int del_mmap_gpa_helper(struct kvm_vcpu *vcpu, unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    unsigned long gpa;
    hva_t hva;
    hpa_t hpa;
    if(ht_lookup_val(ht_mmap_gpas_ptep,ptep, &gpa) !=0){
        ht_remove(ht_mmap_gpas,gpa);
        ht_remove(ht_mmap_gpas_ptep,ptep);
        hva = gfn_to_hva(vcpu->kvm,gpa_to_gfn(gpa)) | (gpa & ~PAGE_MASK);
        hpa = pfn_to_hpa(gfn_to_pfn(vcpu->kvm,gpa_to_gfn(gpa))) | (gpa & ~PAGE_MASK);
        ht_remove(ht_data_hpas,hpa);
        jprintk("removing gpa from mmap gpas %lx ptep %lx \n", gpa,ptep); 
    }
#endif
    return 0;
}

struct kvm* vmmcomm_kvm = NULL;
unsigned long gpa2hpa(unsigned long gpa){
    return pfn_to_hpa(gfn_to_pfn(vmmcomm_kvm,gpa_to_gfn(gpa))) | (gpa & ~PAGE_MASK);
}

int add_mmap_gpa_helper(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    gpa_t gpa = page_boundary(gpas);
    unsigned long ioclass = 0;
    hva_t hva;
    hpa_t hpa;
    hva = gfn_to_hva(vcpu->kvm,gpa_to_gfn(gpa)) | (gpa & ~PAGE_MASK);
    hpa = pfn_to_hpa(gfn_to_pfn(vcpu->kvm,gpa_to_gfn(gpa))) | (gpa & ~PAGE_MASK);
    
    if(vcpu->nitrodata.nu.out_syscall == MMAP_SCALL_NR && vcpu->nitrodata.nu.u.mm.mmap_len){
        ioclass = filesize_to_ioclass(vcpu->nitrodata.nu.u.mm.mmap_len);
    }else{
        //try to lookup old ioclass info
        ht_lookup_val(ht_data_hpas,hpa,&ioclass);
    }

    //remove old
    del_mmap_gpa_helper(vcpu,ptep);
    //add new
    ht_add_val(ht_mmap_gpas,gpa,ptep);
    ht_add_val(ht_mmap_gpas_ptep,ptep,gpa);
    ht_add_val(ht_data_hpas,hpa,ioclass);
    jprintk( " adding gpa to mmap gpas %lx hva %lx hphys %lx ptep %lx \n", gpa,hva,hpa,ptep); 
#endif
    return 0;
}

int del_rw_gpa_helper(struct kvm_vcpu *vcpu,unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    unsigned long gpa;
    hva_t hva;
    hpa_t hpa;
    hash_table *ht_list = NULL;
    if(ht_lookup_val(ht_rw_gpas_ptep,ptep, &gpa) !=0 ){
        ht_remove(ht_rw_gpas,gpa);
        ht_remove(ht_rw_gpas_ptep,ptep);
        jprintk("removing gpa from rw gpas %lx ptep %lx \n", gpa,ptep);
        //untrack the gpa
        ht_remove(ht_tracked_gpas,gpa);

        ht_remove_val(ht_tracked_gfns, gpa_to_gfn(gpa), (unsigned long*)&ht_list);
        if(ht_list != NULL){
            //remove all ranges
            ht_destroy(ht_list);
        }else{
            jprinte("ERROR: empty list for ht_tracked_gfns\n");
        }
    }
#endif
    return 0;
}

int add_rw_gpa_helper(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep){
#ifdef JADUKATA_CHECKSUM
    unsigned long val;
    hash_table *ht_list = NULL;
    struct process_data* m = get_process_data(cr3);
    gpa_t gpa = page_boundary(gpas);
    //remove old
    del_rw_gpa_helper(vcpu,ptep);
    //add new
    ht_add_val(ht_rw_gpas,gpa,ptep);
    ht_add_val(ht_rw_gpas_ptep,ptep,gpa);
    //write protect the actual virtual buffer gpa base
    ht_add_val(ht_tracked_gpas, gpa,GPA_ENCODE(cr3,VBUF_GPA));
    //for VBUF_GPA, ht_tracked_gfns has a list of actual start offset and end offsets that the virt buffers occupy in this page
    val = PACK_INTS_TO_LONG((gpas-gpa),(gpae-gpa));

    ht_lookup_val(ht_tracked_gfns, gpa_to_gfn(gpa), (unsigned long*)&ht_list);
    if(ht_list == NULL){
        ht_create_with_size(&ht_list,"vbeofs",1);
        ht_update_val(ht_tracked_gfns, gpa_to_gfn(gpa),(unsigned long)ht_list);
    }
    ht_add_val(ht_list, val,vaddr);

    jadu_write_protect(vcpu,gpa);
    jprintk("adding gpa to rw gpas %lx ptep %lx \n", gpa,ptep); 
#endif
    return 0;
}

int add_virt_range_gpa(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long vaddr, unsigned long gpas, unsigned long gpae, unsigned long ptep, int code){
    if(code == MMAP_GPA){
        add_mmap_gpa_helper(vcpu,cr3,vaddr,gpas,gpae,ptep);
    }else{
        add_rw_gpa_helper(vcpu,cr3,vaddr,gpas,gpae,ptep);
    }
    return 0;
}

int del_virt_range_gpa(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long ignore0, unsigned long ignore1, unsigned long ignore2, unsigned long ptep, int code){
    if(code == MMAP_GPA){
        del_mmap_gpa_helper(vcpu,ptep);
    }else{
        del_rw_gpa_helper(vcpu,ptep);
    }
    return 0;
}

int is_mmap_gpa(gpa_t gpa){
    int ret = 0;
#ifdef JADUKATA_CHECKSUM
    ret = ht_lookup(ht_mmap_gpas,gpa);
#endif
    return ret;
}

int add_tracked_pte(gpa_t cr3, gpa_t ptep, int code){
    unsigned long val = 0;
    gpa_t ptepfn = gpa_to_gfn(ptep);
    if(ht_lookup(ht_tracked_gpas,ptep) == 0){
        ht_add_val(ht_tracked_gpas, ptep,GPA_ENCODE(cr3,code));
        ht_remove_val(ht_tracked_gfns,ptepfn,&val);
        //we just increment the val without regards to collisions in the ptep within ptepfn
        //because the fact that we are here means there is a ptep that is not write protected
        ht_add_val(ht_tracked_gfns, ptepfn, val+1);
        if(val == 0){
            return 1;
        }
    }else{
        //jprinte("WARN: already being tracked ptep %lx to be added\n", ptep);
    }
    return 0;
}

int del_tracked_pte(gpa_t ptep){
    unsigned long val = 0;
    gpa_t ptepfn = gpa_to_gfn(ptep);
    if(ht_lookup(ht_tracked_gpas,ptep) != 0){
        ht_remove(ht_tracked_gpas, ptep);
        if(ht_lookup_val(ht_tracked_gfns,ptepfn,&val) != 0){
            ht_remove(ht_tracked_gfns,ptepfn);
            if(val > 1){
                ht_add_val(ht_tracked_gfns, ptepfn, val-1);
            }else{
                //actual untracking of step happens automatically during the next page fault handling 
                jprintk( " untracking gfn %lx last ptep %lx \n", ptepfn, ptep);
            }
        }else{
            jprinte("ERROR: no ht_tracked_gfns entry for tracked ptep %lx to be deleted \n", ptep);
        }
        return 1;
    }else{
        jprinte("ERROR: del untracked ptep %lx \n", ptep);
    }
    return 0;
}

int process_pending_remaps(struct kvm_vcpu* vcpu,struct process_data *m, int code);

int handle_tracked_pte_helper(struct kvm_vcpu *vcpu, struct process_data* m, gpa_t addr,int code){
    unsigned long vaddr,vaddrs;
    unsigned long ptep,tptep;
    int count = 0;

    if( ht_lookup_val(m->p2v,addr,&vaddr) !=0 ){
        jprintk( " leo handle fault : ptep %lx vaddr %lx found in cr3 %lx \n", addr, vaddr, m->cr3);
        ptep = addr;
        vaddrs = vaddr;
        count = 1;
        //Leo commented this out so that we process each remap within the same page table entries page as and when it is overwritten
        /*
        vaddr_start = vaddr;
        while(ht_lookup_val(m->v2p,vaddr,&tptep) != 0){
            if(ptep==tptep){
                count++;
                vaddr += PAGE_SIZE;
            }else{
                break;
            }
        }
        if(count<=0){
            jprinte("ERROR: inconsistency between v2p and p2v: addr %lx vaddr_start %lx tptep %lx vaddr %lx \n", addr, vaddr_start, tptep, vaddr );
        }else{
        */
            jprintk( " leo handle fault : ptep %lx vaddrs %lx len %d\n", addr, vaddrs, count);
            add_remap_task(m,vaddrs,count);
        //}

        process_pending_remaps(vcpu,m,code);
        return 1;
    }
    return 0;
}

// TODO add bloom filter here
int handle_tracked_pte(struct kvm_vcpu *vcpu, gpa_t gpa,unsigned long cr3code){
    int i,ret=0;
    unsigned long tgfn;
    int code = GPA_DECODE_VAL(cr3code);

    jprintk( " page fault due to jadu ptep  %lx\n", gpa);
    unsigned long cr3;
    struct process_data* m = get_process_data(GPA_DECODE_CR3(cr3code));
    return handle_tracked_pte_helper(vcpu,m,gpa,code);
}

int handle_tracked_rw(struct kvm_vcpu *vcpu, gpa_t gpa,unsigned long vaddr){
    lock_virt_buffer(vcpu,vaddr,PAGE_SIZE - (gpa & ~PAGE_MASK),false);
    return 0;
}

unsigned long is_jadu_write_protected(gpa_t gpa,gva_t *gva){
    unsigned long val = 0;
    if(ht_lookup_val(ht_tracked_gpas,gpa,&val) == 0){
        gpa_t gpab = page_boundary(gpa);
        //check if this is due to vbuf write protect
        if(ht_lookup_val(ht_tracked_gpas,gpab,&val) != 0){
            hash_table *ht_list = NULL;
            unsigned long offsets,vaddr;
            bool found = false;

            ht_lookup_val(ht_tracked_gfns,gpa_to_gfn(gpa),(unsigned long*)&ht_list);
            if(ht_list != NULL){
                ht_open_scan(ht_list);
                while(ht_scan_val(ht_list, &offsets,&vaddr) != -1){
                    if( (gpa >= (gpab + UNPACK_INTS_FROM_LONG_X(offsets))) && (gpa <= (gpab + UNPACK_INTS_FROM_LONG_Y(offsets))) ){
                        jprintk( "tracked VBUF_GPA gpa %lx gfn %lx val %lu offsets %lx \n", gpa, gpa_to_gfn(gpa), val, offsets);
                        found = true;
                        *gva = vaddr + (gpa-(gpab+UNPACK_INTS_FROM_LONG_X(offsets)));
                        break;
                    }
                }
                ht_close_scan(ht_list);
            }
            if(found == false){
                val = 0;
            }
        }
    }
    if(val){
        jprintk("tracked gpa %lx gfn %lx cr3 %lx code %d \n", gpa, gpa_to_gfn(gpa), GPA_DECODE_CR3(val), GPA_DECODE_VAL(val));
    }
    return val;
}

int add_remap_task(struct process_data* m, unsigned long vaddrs, unsigned long numpages){
    unsigned long vaddre = vaddrs + (numpages* PAGE_SIZE);
    ht_add_val(m->pending,vaddrs,vaddre);
    return 0;
}

int process_virt_range_ptep(struct kvm_vcpu *vcpu, unsigned long cr3, unsigned long ignore0, unsigned long ignore1, unsigned long ignore2, unsigned long ptep,int code){
    //for page table entries due to both mmap and virt buff write protection, we want to write protect
    if(add_tracked_pte(cr3,ptep,code)){
        jadu_write_protect(vcpu,ptep);
    }
    return 0;
}

int process_pending_remaps(struct kvm_vcpu* vcpu,struct process_data *m, int code){
    unsigned long vaddrs, vaddre;
    while(ht_pop_val(m->pending, &vaddrs, &vaddre) != -1){
        if(monitor_virt_range(vcpu,vaddrs,(vaddre-vaddrs),code)){
            jprintk("remapped vaddr %lx \n", vaddrs); 
        }
    }
    return 0;
}

static DEFINE_SPINLOCK(lock_virt_buff_lock);

extern void msleep(unsigned int msecs);

bool overlaps(unsigned long s1, unsigned long e1, unsigned long s2, unsigned long e2){
    return !((e1<s2)||(e2<s1));
}

void lock_virt_buffer(struct kvm_vcpu *vcpu, unsigned long vaddr_start, unsigned long len, bool acquire){
#ifdef LOCK_UNLOCK_BUFFER
    unsigned long vaddr_end = vaddr_start + len;
    unsigned long start,sizewho;
    struct process_data *m = get_process_data(vcpu->arch.cr3);
    int pid = (int)get_guest_pid(vcpu);
    int who,size;
    bool monitor = true;
    bool remoteflush = false;

    while(true){
        bool locked = false;
        spin_lock(&lock_virt_buff_lock);
        ht_open_scan(m->vbe);
        while(ht_scan_val(m->vbe, &start,&sizewho) != -1){
            size = UNPACK_INTS_FROM_LONG_X(sizewho);
            who = UNPACK_INTS_FROM_LONG_Y(sizewho);
            if(overlaps(start,start+size,vaddr_start,vaddr_end)){
                bool exactoverlap = ((start == vaddr_start) && ((start+size)==vaddr_end));
                if(who>0){
                    if(pid != who){
                        locked = true;
                        break;
                    }
                }else{
                    if(pid != -who){
                        remoteflush = true;
                    }
                }
                if(exactoverlap == false){
                    unmonitor_virt_range(vcpu,start,size,VBUF_GPA);
                    htn_remove(m->vbe, start);
                }else{
                    monitor = false;
                    break;
                }
            }
        }
        ht_close_scan(m->vbe);
        if(locked == false){
            if(acquire == true){
                ht_update_val(m->vbe,vaddr_start,PACK_INTS_TO_LONG(len,pid));
            }else{
                //to make sure next lock does a remote tlb flush
                ht_update_val(m->vbe,vaddr_start,PACK_INTS_TO_LONG(len,-1));
            }
        }
        spin_unlock(&lock_virt_buff_lock);
        if(locked == true){
            msleep(3000);
        }else{
            break;
        }
    }

    if(monitor == true){
        monitor_virt_range(vcpu,vaddr_start,len,VBUF_GPA);
    }
    if(remoteflush == true){
        kvm_flush_remote_tlbs(vcpu->kvm);
    }
#endif
}

void unlock_virt_buffer(struct kvm_vcpu *vcpu, unsigned long vaddr_start, unsigned long len){
#ifdef LOCK_UNLOCK_BUFFER
    unsigned long vaddr_end = vaddr_start + len;
    unsigned long start,sizewho;
    struct process_data *m = get_process_data(vcpu->arch.cr3);
    int pid = (int)get_guest_pid(vcpu);
    int who,size;

    ht_update_val(m->vbe,vaddr_start,PACK_INTS_TO_LONG(len,-pid));
#endif
}

inline int is_nonreturning_syscall(unsigned long nr){
    return (nr == EXIT_SCALL_NR || nr == EXECVE_SCALL_NR /*|| nr == EXIT_GROUP_SCALL_NR*/ );
}

inline int is_nosaferet(struct kvm_vcpu *vcpu, unsigned long nr){
#ifdef BSD
    return 0;
#else
    //for linux short cut for thread implementation by avoid iret scall exit handling
    if(nr == CLONE_SCALL_NR){
        struct process_data *m = get_process_data(vcpu->arch.cr3);
        return (m->cloning);
    }
    return 0;
#endif
}

int is_unmonitored_insightcall(unsigned long nr){
    //return (nr == SYSCTL_SCALL_NR  || nr == GETPID_SCALL_NR || nr == GETPPID_SCALL_NR || nr == GETLOGIN_SCALL_NR || nr == MMAP_SCALL_NR|| nr == NANOSLEEP_SCALL_NR);
    int ret = (nr == GETPID_SCALL_NR || nr == GETPPID_SCALL_NR || nr == MMAP_SCALL_NR || nr == DIRTY_SCALL_NR);
#ifdef BSD
    ret |= (nr == SYSCTL_SCALL_NR);
#else
    ret |= (nr == READLINK_SCALL_NR);
#endif
    return ret;
}

int is_monitored_syscall(unsigned long nr){
    int i =0;
    if(ones_match(nr,syscall_bloom)){
        for(i=0;i<NR_SYSCALLS;i++){
            if(monitored_syscalls[i] == nr){
                return 1;
            }
        } 
    }
    return 0;
}
            
#ifdef COMPUTE_BETWEEN_IO
int is_compute_io_scall(unsigned long nr){
    int ret = (nr == READ_SCALL_NR || nr == WRITE_SCALL_NR || nr == READV_SCALL_NR || nr == WRITEV_SCALL_NR || nr == PREADV_SCALL_NR || nr == PWRITEV_SCALL_NR);
#ifndef BSD
    ret |= (nr == IOSUBMIT_SCALL_NR);
#endif
    return ret;
}
#endif

static DEFINE_SPINLOCK(splitcalls_lock);
static DEFINE_SPINLOCK(newprocess_lock);

int check_process_reassignments(struct kvm_vcpu *vcpu, unsigned long cpid){
    hash_table *ht_threads = NULL;
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    //when threads are created the new thread can be scheduled
    // even before the clone system call returns 
    if(unlikely(m->cloning)) return 0;

    read_lock(&mon_lock);
    if( ht_lookup_val(ht_mon_pids,vcpu->arch.cr3,(unsigned long*)&ht_threads) != 0 ){
        unsigned long pid,spid=0;
        int valid = 0;
        bool iret_pid_matching = false;
        unsigned long iret_handling_pid = kvm_register_read(vcpu,VCPU_REGS_RAX);
        char comm[COMM_LEN];
        get_guest_comm(vcpu,comm);

        ht_open_scan(ht_threads);
        while(ht_scan(ht_threads, &pid) != -1){
            if(pid == cpid){
                valid = 1;
                break;
            }
            if(pid == iret_handling_pid){
                iret_pid_matching = true;
            }
        }
        ht_close_scan(ht_threads);
            
        if(valid == 1){
            if(iret_handling_pid && (m->expect_iret_handling!=0) && (m->expect_iret_handling!=cpid) && (m->expect_iret_handling==kvm_register_read(vcpu,VCPU_REGS_RAX))){
                jprintl(" parent's vfork iret syscall handling pid %lu new comm %s cr3 %lx expect_iret_handling %d \n", cpid, comm, vcpu->arch.cr3, m->expect_iret_handling);
                m->expect_iret_handling = 0;
                read_unlock(&mon_lock);
                RETURN22;
            }
        }else{
            // double check with non rsp id for race conditions during thread creation
            cpid = get_guest_pid(vcpu);
            ht_open_scan(ht_threads);
            while(ht_scan(ht_threads, &pid) != -1){
                if(pid == cpid){
                    valid = 1;
                    break;
                }
            }
            ht_close_scan(ht_threads);
            if(valid ==1){ 
                read_unlock(&mon_lock);
                return 0;
            }
            jprinte("ERROR: ERRORLEO a targeted process page table base has been reused for another unmonitored process old pid %lu new pid %lu new comm %s cr3 %lx \n", pid, cpid, comm, vcpu->arch.cr3);
            jprinte("BUG CPU\n");
            dump_stack();
            //remove_targeted_process_core(vcpu->arch.cr3,-1);
            read_unlock(&mon_lock);
            return 1;
        }
    }else{
        jprinte("ERROR could not find pid for a targeted process cr3 %lx \n", vcpu->arch.cr3);
    }
    read_unlock(&mon_lock);
    return 0;
}

static inline int in_new_process_mode(void){
    short int npmode = atomic_read(&in_new_pid_mode);
    npmode |= (ht_get_size(ht_fork_parent_pids) >0);
    npmode |= (ht_get_size(ht_new_pids)>0);
    return npmode;
}

void nitro_handle_guest_task_switch(struct kvm_vcpu* vcpu, unsigned long newcr3){
    //leo
    short int otar = is_targeted_process(vcpu->arch.cr3);
    short int ntar = is_targeted_process(newcr3);
    int npmode = in_new_process_mode();
    short int stashed = 0;
   
    if(otar || ntar || (npmode>0) ){
        jprintk("CR3 write on cpu %d vcpuid %d oldcr3 %lx newcr3 %lx otar %d ntar %d npmode %d fkoffset %ld\n", vcpu->cpu, vcpu->vcpu_id, vcpu->arch.cr3, newcr3, otar, ntar, npmode,vcpu->nitrodata.nu.fkoffset);
    }
    
    if(ht_get_size(ht_fork_parent_pids) >0){
        jprintk("fork parent CR3 write on cpu %d vcpuid %d oldcr3 %lx newcr3 %lx otar %d ntar %d npmode %d fkoffset %ld\n", vcpu->cpu, vcpu->vcpu_id, vcpu->arch.cr3, newcr3, otar, ntar, npmode,vcpu->nitrodata.nu.fkoffset);
    }

#ifdef COMPUTE_BETWEEN_IO
    if(otar){
        if( (vcpu->nitrodata.compute_time_start.tv_sec != 0)  && (vcpu->nitrodata.first_scall == 0) ){
            struct timespec now;
            unsigned long time;
            getnstimeofday(&now);
            time = TIMEDIFF(now,vcpu->nitrodata.compute_time_start)/COMPUTE_TIME_UNIT_NSECS;
            vcpu->nitrodata.compute_time += time;
            jprintk("adding compute time %lu \n", time);
        }
    }
    if(ntar){
        getnstimeofday(&vcpu->nitrodata.compute_time_start);
    }else{
        vcpu->nitrodata.compute_time_start.tv_sec = 0;
        vcpu->nitrodata.compute_time_start.tv_nsec = 0;
    }
#endif

#ifndef NO_SYSCALL_HANDLING
    //if old process is targeted and in between a syscall processing
    if(otar || vcpu->nitrodata.nu.fkoffset){
        if(!is_nonreturning_syscall(vcpu->nitrodata.nu.out_syscall)){
            spin_lock(&splitcalls_lock);
            if( (vcpu->nitrodata.nu.out_syscall == UNMON_SCALL) || is_monitored_syscall(vcpu->nitrodata.nu.out_syscall) || is_unmonitored_insightcall(vcpu->nitrodata.nu.out_syscall) ){
                // we use the userspace stack pointer before syscall saved at the syscall intercept time 
                // to later match this outstanding syscall with its sysret that will happen in a different
                //  or same vcpu after guest context switch
                //  struct nitrodatau *nu  = my_kmalloc(sizeof(struct nitrodatau), GFP_ATOMIC);
                struct nitrodatau *nul = NULL;
                struct nitrodatau *nu = (struct nitrodatau*) kmem_cache_alloc(nucache, GFP_ATOMIC);
                if(!nu){
                    dpanic("ERROR: could not allocate nitrodatau entry \n");
                }

                memcpy(nu,&vcpu->nitrodata.nu,sizeof(struct nitrodatau));
                jprintk("stashing split call with id " IDFORMAT " nu %lx scall %lu \n",nu->id,nu,nu->out_syscall);
                nu->jiffies = get_jiffies();
                if(ht_add_val(ht_splitcalls,nu->id,(unsigned long)nu) == STATUS_DUPL_ENTRY){
                    jprinte( "ERROR duplicate split call info id " IDFORMAT " \n", nu->id);
                    if(ht_remove_val(ht_splitcalls,nu->id,(unsigned long*)&nul) != -1){
                        jprintk("deleting split call with id " IDFORMAT  " nu %lx scall %lu\n",nul->id,nul,nul->out_syscall);
                        kmem_cache_free(nucache,nul);
                    }
                    if(ht_add_val(ht_splitcalls,nu->id,(unsigned long)nu) != 1){
                        jprinte("ERROR inconsistent hash table entry duplicate but not deletable and final add did not succeed\n");
                        kmem_cache_free(nucache,nu);
                    }
                }
                stashed = 1;
            }
            spin_unlock(&splitcalls_lock);
        }
    }
#endif

    //clear nitrodata in vcpu, because if ntar we want to start afresh and if otar we have already stashed
    if((ntar || otar || stashed) && ((npmode <= 0) || stashed)){
        //protects case when splitcall matching searches other vcpus
        spin_lock(&splitcalls_lock);
        nu_init_full(&vcpu->nitrodata.nu);
        spin_unlock(&splitcalls_lock);
    }

#ifndef NO_ON_OFF
    // if it is not tlb flush or thread context switch
    // enable / disable syscall/sysret interception
    if(npmode <= 0){
        if(ntar && !otar){
            start_nitro(vcpu,'t',newcr3);
        }else if(!ntar && otar){
            stop_nitro(vcpu,'t',newcr3);
        }
    }else{
        /*
        struct kvm* kvm;
        struct kvm_vcpu* vcpu;
        int i;
        list_for_each_entry(kvm, &vm_list, vm_list){
            kvm_for_each_vcpu(i, vcpu, kvm){
                vcpu->nitrodata.first_scall = 1;
            }
        }
        start_nitro_all();
        */
        start_nitro(vcpu,'T',newcr3);
    }
    //leo end
#endif
    vcpu->nitrodata.first_scall = 1;
}



//filter out the lower 32 bits of rip
//const unsigned long LOW32_MASK = ((1UL<<32)-1);

//generate id for this system call using the page table base cr3, 
//userspace stack pointer rsp and userspace inst rip right after syscall
#ifndef ID_WITH_TID
unsigned long get_id(struct kvm_vcpu* vcpu, unsigned long cr3, unsigned long rip, unsigned long rsp){
    // higher order 32 bits made of cr3 and rip making mismatch in any avoid matches
    // lower bits are the stack page ( allowed difference is SCALL_ID_DIST)
    return ( ((cr3>>12) << 32) | (((rsp<<32)>>32) >> 12) );
    //return ( ( ((cr3>>12) ^ (rip>>8)) << 32) | (rsp >> 12));
}
#endif

struct nitrodatau* lookup_split_calls(struct kvm_vcpu* vcpu, char prefix, unsigned long id){
    struct nitrodatau *nu = NULL;
    struct kvm_vcpu* tvcpu = NULL;
    //first priority check stashed split calls
    if(ht_lookup_val(ht_splitcalls,id,(unsigned long*)&nu) != 0){
        jprintk("sysret_hook splitcall prefix %c vcpuid %d cr3 %lx scallnr %lu %s nu %lx \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall), nu);
    }else{
        //second check on other vcpus ( because guest os can always reschedule in any order and therefore may lead to a monitored process being moved to a difference cpu before another process is scheduled on the current cpu and hence triggering stashing )
        int i;
        kvm_for_each_vcpu(i, tvcpu, vcpu->kvm){
            if( tvcpu->arch.cr3 == vcpu->arch.cr3 && tvcpu->vcpu_id != vcpu->vcpu_id && (ABSDIFF(tvcpu->nitrodata.nu.id,id) <SCALL_ID_DIST) ){
                nu = &tvcpu->nitrodata.nu;
                jprintk("sysret_hook splitcallvcpu prefix %c vcpuid %d realcpu %d tvcpuid %d tvcpurealcpu %d vcpucr3 %lx tvcpucr3 %lx scallnr %lu %s id " IDFORMAT " nuid %lx diff %lu nu %lx \n", prefix, vcpu->vcpu_id, vcpu->cpu, tvcpu->vcpu_id, tvcpu->cpu, vcpu->arch.cr3, tvcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall),id,nu->id, ABSDIFF(tvcpu->nitrodata.nu.id,id), nu);
                break;
            }
        }
    }
    return nu;
}

int handle_split_syscall(struct kvm_vcpu* vcpu, char prefix, unsigned long id){
    int done = 0;
    struct nitrodatau *nu = NULL;
    struct kvm_vcpu* tvcpu = NULL;
    bool unlocked = false;
    spin_lock(&splitcalls_lock);
    //first priority check stashed split calls
    if(ht_remove_val(ht_splitcalls,id,(unsigned long*)&nu) != -1){
        done = 1;
        jprintk("sysret_hook splitcall prefix %c vcpuid %d cr3 %lx scallnr %lu %s nu %lx \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall), nu);
    }else{
        //second check on other vcpus ( because guest os can always reschedule in any order and therefore may lead to a monitored process being moved to a difference cpu before another process is scheduled on the current cpu and hence triggering stashing )
        int i;
        kvm_for_each_vcpu(i, tvcpu, vcpu->kvm){
            if( tvcpu->arch.cr3 == vcpu->arch.cr3 && tvcpu->vcpu_id != vcpu->vcpu_id && (ABSDIFF(tvcpu->nitrodata.nu.id,id) <SCALL_ID_DIST) ){
                nu = &tvcpu->nitrodata.nu;
                jprintk("sysret_hook splitcallvcpu prefix %c vcpuid %d realcpu %d tvcpuid %d tvcpurealcpu %d vcpucr3 %lx tvcpucr3 %lx scallnr %lu %s id " IDFORMAT " nuid %lx diff %lu nu %lx \n", prefix, vcpu->vcpu_id, vcpu->cpu, tvcpu->vcpu_id, tvcpu->cpu, vcpu->arch.cr3, tvcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall),id,nu->id, ABSDIFF(tvcpu->nitrodata.nu.id,id), nu);
                done = 2;
                break;
            }
        }
    }

    if(!done){
        spin_unlock(&splitcalls_lock);
        unlocked = true;
#ifndef ID_WITH_TID
        // finally try matching the closest esp from stashed calls in same process
        // the 2MB diff limit avoids mathing against other processes because the 
        // cr3 is moved to higher order 32 bits in the id
        const int SPLITCALL_CLEAROUT_MSECS = 50;
        unsigned long lowest = ULONG_MAX, diff,nuldiff,tid;
        struct nitrodatau *nul = NULL;
        ht_open_scan(ht_splitcalls);
        while(ht_scan_val(ht_splitcalls,&tid, (unsigned long*)&nu) != -1){
            //clear out splitcall entries after a time out
            if( jiffies_to_msecs(get_jiffies()-nu->jiffies) > SPLITCALL_CLEAROUT_MSECS){
                htn_remove(ht_splitcalls,tid);
                continue;
            }

            diff = ABSDIFF(id,nu->id);
            jprintk("splitcallclosest working on id " IDFORMAT " tid " IDFORMAT " diff %lu nu %lx lowest %lu \n", id, tid, diff, nu, lowest);
            if(diff < lowest){
                lowest = diff;
                if(diff < SCALL_ID_DIST){
                    nul = nu;
                    nuldiff = diff;
                    jprintk("splitcallclosest updated nuldiff %lu nul %lx \n", nuldiff, nul);
                }
            }
        }
        ht_close_scan(ht_splitcalls);
        if(nul != NULL){
            jprintk("sysret_hook splitcallclosest prefix %c vcpuid %d cr3 %lx scallnr %lu %s id " IDFORMAT " nulid " IDFORMAT " diff %ld nul %lx \n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, nul->out_syscall,syscall_name(nu->out_syscall),id,nul->id,nuldiff,nul);
            nu = nul;
            done = 1;
            ht_remove(ht_splitcalls,nul->id);
        }
#endif
    }

    if(done){
        jprintk("success found split call nu %p scallnr %lu , fkoffset %lu , sysret_state %d, id " IDFORMAT " \n", nu, nu->out_syscall, nu->fkoffset, nu->sysret_state, nu->id);
        //memcpy to handle chained state of fake syscalls
        memcpy(&vcpu->nitrodata.nu,nu,sizeof(struct nitrodatau));
        if(done == 1){
            kmem_cache_free(nucache,nu);
        }else if(done == 2){
            nu_init(&tvcpu->nitrodata.nu);
            //[linux scheduler sometimes schedules without cr3 overwrites]
            tvcpu->nitrodata.first_scall = 1;
        }else{
            jprinte("ERROR: invalid done mode %d  \n", done);
        }
        if(unlocked == false){
            spin_unlock(&splitcalls_lock);
        }
    }
    return done;
}

#ifdef JADU_SKIP_SYSCALLS
bool skip_syscalls(struct x86_emulate_ctxt *ctxt){
    struct kvm_vcpu* vcpu = emul_to_vcpu(ctxt);
    unsigned long scallnr = kvm_register_read(vcpu,VCPU_REGS_RAX);
    if(scallnr == 35 || scallnr == 314){ //  nanosleep, cerny_dummy
        if( is_skipped_process(vcpu->arch.cr3) ){
            vcpu->nitrodata.skipped_syscalls++;
            //kvm_register_write(vcpu,VCPU_REGS_RAX,0UL);
            return true;
        }
    }
    return false;
}
#endif

//TODO: handle int80 and sysenter with correct arg to register mapping
int syscall_hook(char prefix, struct x86_emulate_ctxt *ctxt){
    unsigned long scallnr, rsp, rip, id,time;
    struct timespec ts, te;
    struct kvm_vcpu* vcpu = emul_to_vcpu(ctxt);
    bool isfake,new_proc = false;
    bool istarg = is_targeted_process(vcpu->arch.cr3);  
    getnstimeofday(&ts);

    if(ht_get_size(ht_fork_parent_pids) >0){
        unsigned long rax = kvm_register_read(vcpu,VCPU_REGS_RAX);
        unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
        unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
        unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
        unsigned long rsp = kvm_register_read(vcpu, VCPU_REGS_RSP);
        unsigned long rip = kvm_register_read(vcpu, VCPU_REGS_RIP);
        jprintm("syscall called cr3 %lx first_scall %d last_id %lu rax %lu rdi %lu rsi %lu rdx %lu rsp %lx eflags %lx rip %lx\n",vcpu->arch.cr3, vcpu->nitrodata.first_scall, vcpu->nitrodata.nu.last_id, rax, rdi, rsi, rdx, rsp, vcpu->arch.emulate_ctxt.eflags, rip);
    }

    if( unlikely(!istarg)){
        char comm[COMM_LEN];
        unsigned long rax = kvm_register_read(vcpu,VCPU_REGS_RAX);
        unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
        unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
        unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
        get_guest_comm(vcpu,comm);
        if(vcpu->nitrodata.first_scall == 0){
            jprintm("WARN: syscall called for non targeted process cr3 %lx command %s rax %lu rdi %lu rsi %lu rdx %lu \n",vcpu->arch.cr3,comm, rax, rdi, rsi, rdx );
        }

        struct process_data* m = get_process_data(vcpu->arch.cr3);
        if(unlikely(m->shutdown)){
            extern const int MAX_SYSCALLS;
            if((rax == 0 && rdi >= MAX_SYSCALLS) || (rax >=MAX_SYSCALLS)){
                rax = GETPID_SCALL_NR;
                kvm_register_write(vcpu,VCPU_REGS_RAX,rax);
                jprinte("ERROR: ERRORLEO skipping nosys bad syscall for terminated process cr3 %lx rax %lu rdi %lu rsi %lu rdx %lu \n",vcpu->arch.cr3,comm, rax, rdi, rsi, rdx );
                getnstimeofday(&te);
                time = TIMEDIFF(te,ts);
                vcpu->nitrodata.scall_count++;
                vcpu->nitrodata.scallu_time += time;
#ifdef SYS_STATS
                id = rax;
                stats_update(vcpu,id,time,0);
#endif
                return 22;
            }
        }

    }

    rsp = kvm_register_read(vcpu,VCPU_REGS_RSP);
    //rip = kvm_register_read(vcpu,VCPU_REGS_RIP);
    rip = vcpu->arch.emulate_ctxt._eip;

#ifdef ID_WITH_TID
    id = get_guest_pid(vcpu);
#else
    id = get_id(vcpu,vcpu->arch.cr3,rip,rsp);
#endif
    
    //threads within a process get rescheduled between scall entry & exit
    //without cr3 write, so detect that here
    if(vcpu->nitrodata.nu.last_id != 0 && id != vcpu->nitrodata.nu.last_id){
        nitro_handle_guest_task_switch(vcpu,vcpu->arch.cr3);  
    }
    
    scallnr = kvm_register_read(vcpu,VCPU_REGS_RAX);
  
    if(unlikely( (vcpu->nitrodata.first_scall!=0) || (ht_get_size(ht_new_pids)>0) )){
        vcpu->nitrodata.first_scall = 0;
#ifdef JADUKATA_VMM_COMM
        if(vcpu->nitrodata.pnetlink){
            extern void set_vmmcomm_base_helper(struct kvm_vcpu* tvcpu, gva_t basegva);
            set_vmmcomm_base_helper(vcpu,vcpu->nitrodata.pnetlink);
            vcpu->nitrodata.pnetlink=0;
        }
#endif
        if(istarg){
            int ret = check_process_reassignments(vcpu,id);
            if(ret == 0){
                //valid monitored thread
                //to handle fake syscalls chain, try to match against stashed splitcalls
                
                if(handle_split_syscall(vcpu,'F',id)){
                    if(vcpu->nitrodata.nu.out_syscall != UNMON_SCALL){
                        //if(vcpu->nitrodata.nu.sysret_state != 2){
                           // kvm_register_write(vcpu, VCPU_REGS_RAX,vcpu->nitrodata.nu.out_syscall);
                        //   scallnr = vcpu->nitrodata.nu.out_syscall;
                        //}else{
                        //   scallnr = vcpu->nitrodata.nu.out_syscall;
                        //}
                        scallnr = vcpu->nitrodata.nu.out_syscall;
                    }
                }else{
                    struct process_data* m = get_process_data(vcpu->arch.cr3);
                    if(m->just_added){
                        //try with rsp id for thread race scenarios
                        unsigned long tid = JADU_GET_RSP_ID(vcpu->arch.cr3,rsp);
                        if(handle_split_syscall(vcpu,'f',tid) != 0){
                            if(vcpu->nitrodata.nu.out_syscall != UNMON_SCALL){
                                //if(vcpu->nitrodata.nu.sysret_state != 2){
                                // kvm_register_write(vcpu, VCPU_REGS_RAX,vcpu->nitrodata.nu.out_syscall);
                                //   scallnr = vcpu->nitrodata.nu.out_syscall;
                                //}else{
                                //   scallnr = vcpu->nitrodata.nu.out_syscall;
                                //}
                                scallnr = vcpu->nitrodata.nu.out_syscall;
                            }
                        }
                    } 
                }
            }else if(ret == 22){
                //vfork sysret handling
                jprintk("safe iret handling: treating syscall as sysret cr3 %lx \n",vcpu->arch.cr3);
                //sysret_hook('i',ctxt);
                RETURN22;
            }
        }else{
            if(handle_split_syscall(vcpu,'N',id)){
                if(vcpu->nitrodata.nu.out_syscall != UNMON_SCALL){
                    //if(vcpu->nitrodata.nu.sysret_state != 2){
                    // kvm_register_write(vcpu, VCPU_REGS_RAX,vcpu->nitrodata.nu.out_syscall);
                    //   scallnr = vcpu->nitrodata.nu.out_syscall;
                    //}else{
                    //   scallnr = vcpu->nitrodata.nu.out_syscall;
                    //}
                    scallnr = vcpu->nitrodata.nu.out_syscall;
                }
            }else{
                struct process_data* m = get_process_data(vcpu->arch.cr3);
                if(m->just_added){
                    //try with rsp id for thread race scenarios
                    unsigned long tid = JADU_GET_RSP_ID(vcpu->arch.cr3,rsp);
                    if(handle_split_syscall(vcpu,'n',tid) != 0){
                        if(vcpu->nitrodata.nu.out_syscall != UNMON_SCALL){
                            //if(vcpu->nitrodata.nu.sysret_state != 2){
                            // kvm_register_write(vcpu, VCPU_REGS_RAX,vcpu->nitrodata.nu.out_syscall);
                            //   scallnr = vcpu->nitrodata.nu.out_syscall;
                            //}else{
                            //   scallnr = vcpu->nitrodata.nu.out_syscall;
                            //}
                            scallnr = vcpu->nitrodata.nu.out_syscall;
                        }
                    }
                } 
            }
            if(vcpu->nitrodata.nu.fkoffset ==0){
                if(new_process_tracking(vcpu, &vcpu->nitrodata.nu,0) < 0){
                    new_proc = true;
                    scallnr = kvm_register_read(vcpu,VCPU_REGS_RAX);
#ifdef ID_WITH_TID
                    id = get_guest_pid(vcpu);
#else
                    id = get_id(vcpu,vcpu->arch.cr3,rip,rsp);
#endif
                }
            }
        }
    }

    istarg = is_targeted_process(vcpu->arch.cr3);  
    if(!istarg && !new_proc && ((vcpu->nitrodata.nu.sysret_state != 2) && (vcpu->nitrodata.nu.fkoffset==0))){
        short int npmode = in_new_process_mode();
        if(!npmode || nitro_shutdown){
#ifndef NO_ON_OFF
            stop_nitro(vcpu,'u',vcpu->arch.cr3);
#endif
        }
        vcpu->nitrodata.nu.out_syscall = scallnr ;
        vcpu->nitrodata.nu.cr3 = vcpu->arch.cr3;
        vcpu->nitrodata.nu.id = id;
        vcpu->nitrodata.nu.last_id = id;
        
        getnstimeofday(&te);
        time = TIMEDIFF(te,ts);
        vcpu->nitrodata.scall_count++;
        vcpu->nitrodata.scallu_time += time;
#ifdef SYS_STATS
        id = scallnr;
        stats_update(vcpu,id,time,0);
#endif
        return 0;
    }
    
    jprintk("scall cr3 %lx scallnr %lu %s rsp %lx rip1 %lx eflags %lx sysret_state %d \n",vcpu->arch.cr3, scallnr, syscall_name(scallnr), rsp, rip,vcpu->arch.emulate_ctxt.eflags, vcpu->nitrodata.nu.sysret_state);
#ifdef TEST_MODE
    if(!vcpu->nitrodata.running){
        jprinte("ERROR: sysret interception without nitro running");
    }

    // close for some reason does not have a sysret
    // for now, we dont print error for close 
    if(vcpu->nitrodata.nu.out_syscall != NO_SCALL && vcpu->nitrodata.nu.out_syscall != CLOSE_SCALL_NR){
        jprinte("ERROR: syscall interception while out_syscall is not NO_SCALL but %d vcpuid %d scallnr %lu \n",vcpu->nitrodata.nu.out_syscall,vcpu->vcpu_id, scallnr);
    }
#endif

#ifdef TEST_MODE
    //vcpu->nitrodata.nu.temp = rsp;
#endif
    
    if(unlikely(vcpu->nitrodata.nu.sig_ret_rsp == rsp)){
        struct nitrodatau *nu = NULL;
        if(ht_remove_val(ht_sig_nu_stash,id,(unsigned long*)&nu) != -1){
            jprintk("sig return unstashed nu vcpuid %d cr3 %lx scallnr %lu %s nu %lx \n", vcpu->vcpu_id, vcpu->arch.cr3, nu->out_syscall,syscall_name(nu->out_syscall), nu);
        }else{
            jprinte("ERROR: unable to find sig info nu stashed scallnr %d vcpuid %d scallnr %lu \n",vcpu->nitrodata.nu.out_syscall,vcpu->vcpu_id, scallnr);
        }
        memcpy(&vcpu->nitrodata.nu,nu,sizeof(struct nitrodatau));
        kmem_cache_free(nucache,nu);
        
        vcpu->nitrodata.nu.sig_ret_rsp = 0;
        vcpu->nitrodata.nu.sysret_state = 3;
        jprintk("signal handler returned: unstashing the safe iret handling cr3 %lx rsp %lx\n",vcpu->arch.cr3, rsp);
        jprintk("safe iret handling: treating syscall as sysret cr3 %lx \n",vcpu->arch.cr3);
        sysret_hook('i',ctxt);
        getnstimeofday(&te);
        time = TIMEDIFF(te,ts);
        vcpu->nitrodata.sret_count++;
        if(istarg) vcpu->nitrodata.sretm_time += time;
        else vcpu->nitrodata.sretu_time += time;
#ifdef SYS_STATS
        id = UNMON_INDEX;
        stats_update(vcpu,id,time,0);
#endif
        RETURN22;
    }
    
    if(unlikely(vcpu->nitrodata.nu.sysret_state == 2)){
        if(vcpu->nitrodata.nu.sysret_rsp == rsp){
            scallnr = vcpu->nitrodata.nu.out_syscall;
            vcpu->nitrodata.nu.sysret_state = 3;
    
            bool targval = istarg && (is_monitored_syscall(scallnr) || is_unmonitored_insightcall(scallnr)) && !is_nonreturning_syscall(scallnr) && !is_nosaferet(vcpu,scallnr);
            bool untargval = !istarg && is_unmonitored_insightcall(scallnr);

            if(targval || untargval){
                jprintk("safe iret handling: treating syscall as sysret cr3 %lx \n",vcpu->arch.cr3);

                sysret_hook('i',ctxt);
                /*if(vcpu->nitrodata.nu.fkoffset){
                  vcpu->arch.emulate_ctxt._eip-=2;
                  jprintk("fake scall during safe iret handling: changing rip to %lx \n",vcpu->arch.emulate_ctxt._eip);
                  }
                  */
            }
            getnstimeofday(&te);
            time = TIMEDIFF(te,ts);
            vcpu->nitrodata.sret_count++;
            if(istarg) vcpu->nitrodata.sretm_time += time;
            else vcpu->nitrodata.sretu_time += time;
#ifdef SYS_STATS
            id = UNMON_INDEX;
            stats_update(vcpu,id,time,0);
#endif
            RETURN22;
        }else{
            if( kvm_register_read(vcpu,VCPU_REGS_RAX) != SIGRETURN_SCALL_NR ){
                vcpu->nitrodata.nu.sig_ret_rsp = vcpu->nitrodata.nu.sysret_rsp;
                vcpu->nitrodata.nu.sysret_state = 0;

                struct nitrodatau *nul = NULL;
                struct nitrodatau *nu = (struct nitrodatau*) kmem_cache_alloc(nucache, GFP_ATOMIC);
                if(!nu){
                    dpanic("ERROR: could not allocate nitrodatau entry \n");
                }
                memcpy(nu,&vcpu->nitrodata.nu,sizeof(struct nitrodatau));
                jprintk("stashing signal info call with id " IDFORMAT " nu %lx scall %lu sysret_rsp %lx rsp %lx\n",nu->id,nu,nu->out_syscall,vcpu->nitrodata.nu.sig_ret_rsp, rsp);
                nu->jiffies = get_jiffies();

                if(ht_add_val(ht_sig_nu_stash,nu->id,(unsigned long)nu) == STATUS_DUPL_ENTRY){
                    jprinte( "ERROR duplicate sig statsh info id " IDFORMAT " \n", nu->id);
                    kmem_cache_free(nucache,nu);
                    /*if(ht_remove_val(ht_sig_nu_stash,nu->id,(unsigned long*)&nul) != -1){
                      jprintk("deleting sig stash info with id " IDFORMAT  " nu %lx scall %lu\n",nul->id,nul,nul->out_syscall);
                      kmem_cache_free(nucache,nul);
                      }
                      if(ht_add_val(ht_sig_nu_stash,nu->id,(unsigned long)nu) != 1){
                      jprinte("ERROR inconsistent hash table entry duplicate but not deletable and final add did not succeed\n");
                      }
                      */
                }

                jprintk("signal handler: stashed the safe iret handling cr3 %lx sysret_rsp %lx rsp %lx sig_ret_rsp %lx \n",vcpu->arch.cr3, vcpu->nitrodata.nu.sysret_rsp, rsp, vcpu->nitrodata.nu.sig_ret_rsp);

                scallnr = kvm_register_read(vcpu,VCPU_REGS_RAX);
            }
        }
    }
    
    if(unlikely(nitro_shutdown)){
        jprintk("removing cr3 %lx due to nitro shutdown \n",vcpu->arch.cr3);
        remove_targeted_process(vcpu->arch.cr3);
        if(ht_get_size(ht_mon_process)<=0){
            unsigned long key;
            while(ht_pop(ht_new_pids, &key) != -1);
            recalc_process_cr3s_bloom();
            nitro_shutdown = 0;
            atomic_set(&in_new_pid_mode,0);
            while(ht_pop(ht_new_pids, &key) != -1);
            while(ht_pop(ht_cr3_to_pid, &key) != -1);
            while(ht_pop(ht_fork_parent_pids, &key) != -1);
            while(ht_pop(ht_exec_cr3s, &key) != -1);
            while(ht_pop(ht_new_process_tracking, &key) != -1);
            jprintk("all processes removed nitro shutdown complete \n");
            return 0;
        }
    }

    vcpu->nitrodata.nu.out_syscall = scallnr ;
    vcpu->nitrodata.nu.cr3 = vcpu->arch.cr3;
    vcpu->nitrodata.nu.id = id;
    vcpu->nitrodata.nu.last_id = id;
        
#ifdef COMPUTE_BETWEEN_IO
    if(istarg){
        if(vcpu->nitrodata.compute_time_start.tv_sec != 0){
            struct timespec end;
            unsigned long time;
            getnstimeofday(&end);
            time = TIMEDIFF(end,vcpu->nitrodata.compute_time_start)/COMPUTE_TIME_UNIT_NSECS;
            vcpu->nitrodata.compute_time += time;
            jprintk("adding compute time %lu io_amount %d \n", time, vcpu->nitrodata.io_amount);
            vcpu->nitrodata.compute_time_start.tv_sec = 0;
            vcpu->nitrodata.compute_time_start.tv_nsec = 0;
        }
    }
#endif

    if(is_monitored_syscall(scallnr) || is_unmonitored_insightcall(scallnr)){
        jprintm("syscall_hook prefix %c vcpuid %d cr3 %lx id " IDFORMAT " scallnr %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, vcpu->nitrodata.nu.id, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall) , rsp);
        if(istarg){
            jadukata_syscall(prefix,vcpu,&(vcpu->nitrodata.nu));
        }
        //return print_trace_proxy(prefix,vcpu);
    }else{
        jprintm("syscall_hook prefix %c vcpuid %d cr3 %lx id " IDFORMAT " scallnr UNMON %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, vcpu->nitrodata.nu.id, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall), rsp);
        vcpu->nitrodata.nu.out_syscall = UNMON_SCALL;
    }
    smp_wmb();
    getnstimeofday(&te);
    time = TIMEDIFF(te,ts);
    vcpu->nitrodata.scall_count++;
    if(istarg){
        vcpu->nitrodata.scallm_time += time;
    }else{
        vcpu->nitrodata.scallu_time += time;
    }
#ifdef SYS_STATS
    isfake = vcpu->nitrodata.nu.fkoffset;
    id = vcpu->nitrodata.nu.out_syscall;
    if(!is_monitored_syscall(id)){
        id = UNMON_INDEX;
    }
    stats_update(vcpu,id,time,isfake);
#endif
    
    scallnr = vcpu->nitrodata.nu.out_syscall; // out syscall could change when fake syscall are issued during syscall entry interception, eg. clone->getrlimit in linux
    bool targval = istarg && (is_monitored_syscall(scallnr) || is_unmonitored_insightcall(scallnr)) && !is_nonreturning_syscall(scallnr) && !is_nosaferet(vcpu,scallnr);
    bool untargval = !istarg && is_unmonitored_insightcall(scallnr);

    if(targval || untargval){
        vcpu->nitrodata.nu.sysret_state = 2;
        vcpu->nitrodata.nu.sysret_rsp = rsp;
        //eip point back to syscall to detect iret scenarios
        vcpu->arch.emulate_ctxt._eip-=2;
        jprintl("safe iret handling: changing rip to %lx rsp %lx\n",vcpu->arch.emulate_ctxt._eip, rsp);
    }
    
    return 0;
}

//TODO: think about hooking into host context switches to avoid the barriers in each
// system call /return 
int sysret_hook(char prefix, struct x86_emulate_ctxt *ctxt){
    //long val = 0;
    struct x86_exception ex;
    struct timespec ts, te;
    struct kvm_vcpu* vcpu = emul_to_vcpu(ctxt);
    unsigned long rsp, rip, time, id, scallnr;
    bool isfake;
    bool istarg = is_targeted_process(vcpu->arch.cr3);
    
    getnstimeofday(&ts);
    rsp = kvm_register_read(vcpu, VCPU_REGS_RSP);
    rip = vcpu->arch.emulate_ctxt._eip; // VCPU_REGS_RIP updated only after emulation of sysret
    //unsigned long rip1 = kvm_register_read(vcpu,VCPU_REGS_RIP);
    jprintk("sret cr3 %lx rsp %lx rip1 %lx rax %lu rdi %lu r11 %lu eflags %lx bsdfailedsyscall %d\n",vcpu->arch.cr3, rsp, rip, kvm_register_read(vcpu, VCPU_REGS_RAX), kvm_register_read(vcpu,VCPU_REGS_RDI), kvm_register_read(vcpu,VCPU_REGS_R11), vcpu->arch.emulate_ctxt.eflags, (vcpu->arch.emulate_ctxt.eflags & 1UL));
    
    if(!istarg && (atomic_read(&in_new_pid_mode)<=0) ){
        char comm[COMM_LEN];
        get_guest_comm(vcpu,comm);
        jprintm("WARN: sysret called for non targeted process cr3 %lx %s\n",vcpu->arch.cr3,comm);
        getnstimeofday(&te);
        time = TIMEDIFF(te,ts);
        vcpu->nitrodata.sret_count++;
        vcpu->nitrodata.sretu_time += time;
#ifdef SYS_STATS
        id = UNMON_INDEX;
        stats_update(vcpu,id,time,0);
#endif
        return 0;
    }
    
    //jprintk("sret cr3 %lx rsp %lx rip %lx rip1 %lx \n",vcpu->arch.cr3, rsp, rip, rip1 );
    //print_all_guest_pids(vcpu);
    smp_rmb();
#ifdef TEST_MODE
    if(!vcpu->nitrodata.running){
        jprinte("ERROR: sysret interception without nitro running");
    }
    /*
    val = ABSDIFF(vcpu->nitrodata.nu.temp,rsp);
    if(val > vcpu->nitrodata.nu.max){
        if(vcpu->nitrodata.nu.temp == 0){
            val = 0;
        }
        vcpu->nitrodata.nu.max = val;
        jprintk("max offset %lu scall %lx sret %lx nr %d \n",vcpu->nitrodata.nu.max, vcpu->nitrodata.nu.temp, rsp, vcpu->nitrodata.nu.out_syscall);
    }
    */
#endif
#ifdef ID_WITH_TID
    id = get_guest_pid(vcpu);
#else
    id = get_id(vcpu,vcpu->arch.cr3, rip,rsp);
#endif

    //id during sysret is not equal to id during syscall
    //threads within a process get rescheduled between scall entry & exit
    //without cr3 write, so detect that here
    if(vcpu->nitrodata.nu.last_id != 0){
        if(id != vcpu->nitrodata.nu.last_id){
            nitro_handle_guest_task_switch(vcpu,vcpu->arch.cr3);  
        }
    }else{
#ifdef ID_WITH_TID
        id = get_guest_pid_ignorecachedval(vcpu);
#else
        id = get_id(vcpu,vcpu->arch.cr3, rip,rsp);
#endif
    }


    if(unlikely(vcpu->nitrodata.nu.out_syscall == NO_SCALL)){
        // this is a syscall call whose processing is split across vcpus due to guest level context switch
        // try to match with the saved split syscalls during context switch of targeted processes
        if(handle_split_syscall(vcpu,prefix,id) != 0){
            prefix -= 32;
        }else{
            struct process_data* m = get_process_data(vcpu->arch.cr3);
            if(m->just_added){
                //try with rsp id for thread race scenarios
                unsigned long tid = JADU_GET_RSP_ID(vcpu->arch.cr3,rsp);
                if(handle_split_syscall(vcpu,prefix,tid) != 0){
                    prefix -= 31;
                }
            }
        }
    }

#ifdef TEST_MODE
    if(vcpu->arch.cr3 && vcpu->arch.cr3 != vcpu->nitrodata.nu.cr3){
        jprinte("ERROR: cr3 mismatch cr3 %lx nu.cr3 %lx \n", vcpu->arch.cr3, vcpu->nitrodata.nu.cr3);
    }
#endif
    
    scallnr = vcpu->nitrodata.nu.out_syscall;
    isfake = vcpu->nitrodata.nu.fkoffset;
    
    bool targval = istarg && (is_monitored_syscall(scallnr) || is_unmonitored_insightcall(scallnr)) && !is_nonreturning_syscall(scallnr) && !is_nosaferet(vcpu,scallnr);
    bool untargval = !istarg && is_unmonitored_insightcall(scallnr);
    jprintk("safe iret handling test: targval %d untargval %d syretstate %d \n", targval, untargval, vcpu->nitrodata.nu.sysret_state);
    if(targval || untargval){ 
        if(unlikely(vcpu->nitrodata.nu.sysret_state == 2)){
            vcpu->nitrodata.nu.sysret_state = 3;
            vcpu->nitrodata.nu.sysret_rsp = 0;
            //eip point back to instruction after syscall because this is not an iret scenario
            vcpu->arch.emulate_ctxt._eip+=2;
            jprintk("safe iret handling: changing rip back to %lx \n",vcpu->arch.emulate_ctxt._eip);
        }
    }
   
    
    if(unlikely(nitro_shutdown)){
        jprintk("removing cr3 %lx due to nitro shutdown \n",vcpu->arch.cr3);
        remove_targeted_process(vcpu->arch.cr3);
        return 0;
    }
   
    
    vcpu->nitrodata.nu.id = id;
    vcpu->nitrodata.nu.last_id = id;
     
    if(unlikely(vcpu->nitrodata.nu.out_syscall == NO_SCALL)){
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        bool warn = false;
        if(m->just_added){
            m->just_added--;
            warn = true;
        }
        if(in_new_process_mode()){
            warn = true;
        }
        jprintk("%s: unmatched syscall id " IDFORMAT " cr3 %lx rsp %lx rip %lx \n",warn?"WARN":"ERROR", id, vcpu->arch.cr3, rsp, rip);
    }else if(unlikely(vcpu->nitrodata.nu.out_syscall == UNMON_SCALL)){
        //skip unmonitored sysrets
        vcpu->nitrodata.nu.out_syscall = NO_SCALL;
        jprintm("sysret_hook prefix %c vcpuid %d cr3 %lx id " IDFORMAT " scallnr UNMON %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, id, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall), rsp);
    }else{
        jprintm("sysret_hook prefix %c vcpuid %d cr3 %lx id " IDFORMAT " scallnr %lu %s rsp %lx\n", prefix, vcpu->vcpu_id, vcpu->arch.cr3, id, vcpu->nitrodata.nu.out_syscall, syscall_name(vcpu->nitrodata.nu.out_syscall),rsp);
        if(istarg || isfake){
            jadukata_sysret(prefix,vcpu,&(vcpu->nitrodata.nu));
        }
    }

#ifdef COMPUTE_BETWEEN_IO
    if(istarg){
        getnstimeofday(&vcpu->nitrodata.compute_time_start);
    }
#endif
    
    getnstimeofday(&te);
    time = TIMEDIFF(te,ts);
    vcpu->nitrodata.sret_count++;
    if(istarg) vcpu->nitrodata.sretm_time += time;
    else vcpu->nitrodata.sretu_time += time;
#ifdef SYS_STATS
    id = scallnr;
    if(vcpu->nitrodata.nu.out_syscall == NO_SCALL){
        id = UNMON_INDEX;
    }
    if(!is_monitored_syscall(id)){
        id = UNMON_INDEX;
    }
    stats_update(vcpu,id,time,isfake);
#endif
    if(!vcpu->nitrodata.nu.fkoffset){
        nu_init(&(vcpu->nitrodata.nu));
    }
    return 0;
}

#endif

