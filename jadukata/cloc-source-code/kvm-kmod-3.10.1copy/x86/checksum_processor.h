#include "../include/linux/jadu.h"
#include <linux/kvm_types.h>
#include <linux/delay.h>
#include <asm/checksum.h>
#include "/home/arulraj/cerny/jadukata/kvm-kmod-3.10.1/x86/hash/ht_at_wrappers.h"

#include "heap.h"

#ifndef CLEANER_H

#define CLEANER_H

#define IOCLASS_MAX (5)
#define IOCLASS_MIN (0)

#define METADATA_IOCLASS (5)

#ifdef STRICT_APP_PRIORITY
#undef METADATA_IOCLASS
#define METADATA_IOCLASS (0)
#endif

#define DEDUP_U_IOCLASS_MASK (1<<5)

#define SMALLFILE_IOCLASS_MAX (4)

#define CLEANER_HEAP_SIZE (100000)

#define cleanup_heap_max_cmp(l,r) (l->jsecs < r->jsecs) 
#define cleanup_heap_min_cmp(l,r) (l->jsecs > r->jsecs)

struct cleaner_heap_entry{
    unsigned long key;
    unsigned int jsecs;
};

struct cleaner_data{
    struct delayed_work work;
    hash_table *table;
	DECLARE_HEAP(struct cleaner_heap_entry*, heap);
    unsigned long time_secs, time_usecs;
    int period;
    int check_period;
    //spinlock_t lock;
    bool might_sleep;
	struct semaphore busy_sem;
    int last_cleanup;
    int cleanup_rate;
    int max_delay;
    void (*process_fn)(hash_table*, unsigned long,unsigned long);
};

struct pair {
    unsigned long addr;
    unsigned int size;
};

int checksum_processor2(void* pargs, struct pair* pairs, int numpairs);
void checksum_processor1(struct pair* pairs, int numpairs);
struct cleaner_data* cleanup_register(hash_table* table, void(*fn)(hash_table*, unsigned long,unsigned long),int,int);
void cleanup_unregister(struct cleaner_data* cl);
void do_cleanup(struct cleaner_data* cl);

int customfnv_mod_init(void);
void customfnv_mod_fini(void);

//#define jsecs_to_jiffies(x) (msecs_to_jiffies(x)*1000)
//#define jiffies_to_jsecs(x) (jiffies_to_msecs(x)/1000)

#define jsecs_to_jiffies(x) (msecs_to_jiffies(x*1000))
#define jiffies_to_jsecs(x) (jiffies_to_msecs(x)/1000)
#define get_jiffies() (jiffies%100000000) 

#define SMALLFILE_DIVIDER (1)
#define SMALLFILE_QUOTIENT (0)

unsigned long pack_info_xyz(int time, int sector,char bigfile);
int pack_info_get_x(unsigned long val);
int pack_info_get_y(unsigned long val);
char pack_info_get_z(unsigned long val);
char filesize_to_ioclass(unsigned long size);
char* cleanup_stats(char* buff);

uint32_t murmur_hash2(const void * key, int len, uint32_t seed);
uint32_t murmur_hash3(const void * key, int len, uint32_t seed);
uint32_t crc32(const unsigned char *data, int len, uint32_t crc);
unsigned int BKDRHash(const char* str, unsigned int len, unsigned int csum);
unsigned int FNVHash(const char* str, unsigned int len, unsigned int csum);
void FNVHash128(const char* str, char* csum);
void FNVHash128loop(const char* str, char* csum,unsigned long size);
void FNVHash64(const char* str, char* csum);
void FNVHash64loop(const char* str, char* csum,unsigned long size);
unsigned long FNVHashfold64(const char* hash);
unsigned long FNVHashfold128(const char* hash);
unsigned int ELFHash(const char* str, unsigned int len, unsigned int csum);

#endif
