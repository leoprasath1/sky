#include <linux/nitro.h>
#include <linux/rwlock_types.h>
#include "kvm_cache_regs.h"
#include "x86.h"
#include "mmu.h"
#include "checksum_processor.h"
#include "all_common.h"

#ifdef JADUKATA

extern void jadu_write_protect(struct kvm_vcpu *vcpu, gpa_t gpa);

extern bool ndbg;

static DEFINE_SEMAPHORE(forklock);
static DEFINE_SPINLOCK(newprocesstrackinglock);

extern rwlock_t mon_lock;

//extern int leo_debug;

#ifndef BSD
#define dirty_scall_result_check check_result_zero // for getcwd
int ARGSMAP[MAXFAKESCALLARGS] = {VCPU_REGS_RDI, VCPU_REGS_RSI, VCPU_REGS_RDX, VCPU_REGS_R10, VCPU_REGS_R8, VCPU_REGS_R9};
#define MMAP_SIZE (8192UL)
#define MMAP_FLAGS (32802UL) /* MAP_ANONYMOUS|MAP_PRIVATE|MAP_POPULATE*/
#else
#define dirty_scall_result_check check_result_non_zero // for getlogin
int ARGSMAP[MAXFAKESCALLARGS] = {VCPU_REGS_RDI, VCPU_REGS_RSI, VCPU_REGS_RDX, VCPU_REGS_R10, VCPU_REGS_R8, VCPU_REGS_R9, VCPU_REGS_R11};
#define MMAP_SIZE (8192UL)
#define MMAP_FLAGS (266242UL) /*MAP_ANONYMOUS|MAP_PRIVATE|MAP_PREFAULT_READ*/
#endif

bool is_write(unsigned long scallnr){
    if(scallnr == WRITE_SCALL_NR || scallnr == PWRITE_SCALL_NR || scallnr == WRITEV_SCALL_NR || scallnr == PWRITEV_SCALL_NR){
        return true;
    }else if(scallnr == READ_SCALL_NR || scallnr == PREAD_SCALL_NR || scallnr == READV_SCALL_NR || scallnr == PREADV_SCALL_NR ){
        return false;
#ifndef BSD
     }else if(scallnr == IOSUBMIT_SCALL_NR || scallnr == IOGETEVENTS_SCALL_NR){
         return false;
#endif
    }else{
        jprinte("ERROR: non read write scallr checked for is_write %lu \n", scallnr);
    }
    return false;
}
    
bool is_vectored(unsigned long scallnr){
    if(scallnr == READV_SCALL_NR || scallnr == WRITEV_SCALL_NR || scallnr == PREADV_SCALL_NR || scallnr == PWRITEV_SCALL_NR){
        return true;
    }else if(scallnr == READ_SCALL_NR || scallnr == WRITE_SCALL_NR || scallnr == PREAD_SCALL_NR || scallnr == PWRITE_SCALL_NR){
        return false;
#ifndef BSD
    }else if(scallnr == IOSUBMIT_SCALL_NR || scallnr == IOGETEVENTS_SCALL_NR){
        return false;
#endif
    }else{
        jprinte("ERROR: non read write scallr checked for is_vectored %lu \n", scallnr);
    }
    return false;
}

bool is_preadwrite(unsigned long scallnr){
    if(scallnr == PREAD_SCALL_NR || scallnr == PWRITE_SCALL_NR || scallnr == PREADV_SCALL_NR || scallnr == PWRITEV_SCALL_NR){
        return true;
    }else if(scallnr == READ_SCALL_NR || scallnr == WRITE_SCALL_NR || scallnr == READV_SCALL_NR || scallnr == WRITEV_SCALL_NR){
        return false;
#ifndef BSD
    }else if(scallnr == IOSUBMIT_SCALL_NR || scallnr == IOGETEVENTS_SCALL_NR){
        return false;
#endif
    }else{
        jprinte("ERROR: non read write scallr checked for is_preadwrite %lu \n", scallnr);
    }
    return false;
}

bool is_async(unsigned long scallnr){
    if(scallnr == PREAD_SCALL_NR || scallnr == PWRITE_SCALL_NR || scallnr == PREADV_SCALL_NR || scallnr == PWRITEV_SCALL_NR || scallnr == READ_SCALL_NR || scallnr == WRITE_SCALL_NR || scallnr == READV_SCALL_NR || scallnr == WRITEV_SCALL_NR){
        return false;
#ifndef BSD
    }else if(scallnr == IOSUBMIT_SCALL_NR || scallnr == IOGETEVENTS_SCALL_NR){
        return true;
#endif
    }else{
        jprinte("ERROR: non read write scallr checked for is_preadwrite %lu \n", scallnr);
    }
    return false;
}

hva_t jadukata_gva_to_hva(struct kvm_vcpu *vcpu, gva_t gva){
    struct x86_exception error;
    gpa_t gpa;
    gfn_t gfn;
    hva_t hva;
    hpa_t hpa;
    pfn_t pfn;
    unsigned long kaddr;
    unsigned offset = 0;

	gpa = kvm_mmu_gva_to_gpa_system(vcpu, gva, &error);

    if (gpa == UNMAPPED_GVA) {
        jprinte("ERROR unmapped gpa %lx gva %lx \n", gpa, gva);//fault address %lx\n",gpa, gva, vcpu->arch.fault.address);
        return 0;
    }

    offset = (gpa & ~PAGE_MASK);
    gfn = gpa_to_gfn(gpa);
    hva = gfn_to_hva(vcpu->kvm,gfn) | offset;
    pfn = gfn_to_pfn(vcpu->kvm,gfn);
    hpa = pfn_to_hpa(pfn) | offset;
    kaddr = ((unsigned long)pfn_to_kaddr(pfn)) | offset;
    
    jprintk("gva %lx gpa %lx gfn %lx hva %lx pfn %lx hpa %lx kaddr %lx virt %lx pg %lx \n",gva, gpa, gfn, hva, pfn, hpa, kaddr, __va(hpa), virt_to_page(kaddr));

    return kaddr;
}

void jadukata_fake_save_org(struct kvm_vcpu *vcpu, struct nitrodatau* nu, struct fakescall *fk){
    int i;
    //clear out fakescall structure
    memset(fk,0,sizeof(struct fakescall));
    //save the original syscall
    fk->orgscall = kvm_register_read(vcpu, VCPU_REGS_RAX);
    for(i=0;i<MAXFAKESCALLARGS;i++){
        fk->regs[i] = kvm_register_read(vcpu, ARGSMAP[i]);
    }
    fk->eflags = vcpu->arch.emulate_ctxt.eflags;
}

void jadukata_fake_restore_org(struct kvm_vcpu *vcpu, struct nitrodatau* nu, struct fakescall *fk){
    int i;
    kvm_register_write(vcpu, VCPU_REGS_RAX,fk->orgscall);
    for(i=0;i<MAXFAKESCALLARGS;i++){
        kvm_register_write(vcpu, ARGSMAP[i],fk->regs[i]);
    }
    vcpu->arch.emulate_ctxt.eflags = fk->eflags;
}

int jadukata_fake_syscall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk, chainfntype chainfn, successfntype successfn, unsigned long syscallnr, int num, ...){
    int i;
    unsigned long v;
    va_list args;
    char data[200] = "", *dataptr = data;
    unsigned long fkoffset = ((unsigned long)fk-(unsigned long)nu); 
    if( ((unsigned long)fk <= (unsigned long)nu) || (fkoffset >= sizeof(struct nitrodatau)) ){
        jprinte("ERROR: offset not in range %d \n",fkoffset);
    }
    fk->fakescall = syscallnr;
    nu->fkoffset = fkoffset;
    fk->last = false;
    if(chainfn) fk->chainfn = chainfn;
    if(successfn) fk->successfn = successfn;
    va_start(args,num);
    for(i=0;i<num;i++){
        v = va_arg(args,unsigned long);
        dataptr += sprintf(dataptr, "%d:%lu, ", i,v);
        kvm_register_write(vcpu, ARGSMAP[i], v);
    }
    va_end(args);
    if(num == 0){
        for(i=0;i<MAXFAKESCALLARGS;i++){
            v = kvm_register_read(vcpu, ARGSMAP[i]);
            dataptr += sprintf(dataptr, "%d:%lu, ", i,v);
        }
    }
    kvm_register_write(vcpu,VCPU_REGS_RAX,syscallnr);
    nu->out_syscall = syscallnr;
    jprintk("fake syscall scall nr %lu %s numargs %d chainfn %p fk %p fkoffset %lu args %s \n",syscallnr,syscall_name(syscallnr),num,fk->chainfn,fk,nu->fkoffset,data);
    return 0;
}

int jadukata_fake_sysret(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    int i;
    int fake_ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintk("fake sysret chainfn %p fk %p fake ret %d \n",fk->chainfn,fk,fake_ret);
    //if not an error, proceed else retry
    if(fk->successfn(vcpu,nu,fk) == 0){
        fk->chainfn(vcpu,nu,fk);
    }
    if(fk->last == false){
        //redo syscall 
        vcpu->arch.emulate_ctxt._eip-=2;
        jprintl("fake sysret changing rip to %lx \n",vcpu->arch.emulate_ctxt._eip);
    }else{
        nu->fkoffset = 0;
        jprintl("fake sysret last in chain unfake\n");
    }
    return 0;
}

int check_result_helper(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk, bool failed, char* failuremsg){
    int ret = -1;
    if(failed){
        jprintk("WARN fake scall failed due to %s - retrying \n", failuremsg);
        fk->retrycount++;
        if(fk->retrycount > FAKE_RETRY_LIMIT){
            jprinte("ERROR fake scall failed due to %s - AFTER ALL RETRIES DONE\n", failuremsg);
            ret = 0;
        }else{
            jadukata_fake_syscall(vcpu, nu, fk, NULL, NULL, fk->fakescall, 0);
        }
    }else{
        ret = 0;
    }
    return ret;
}

extern hash_table *ht_file_eof_guess;

int check_result_pread(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    bool failed = false;
    char errormsg[50] = "";
#ifdef BSD 
    //check carry flag for syscall errors in bsd
    failed = (vcpu->arch.emulate_ctxt.eflags & 1UL);
    int actual = kvm_register_read(vcpu,VCPU_REGS_RAX);
    if(failed && (actual == 9)){
        ht_update_val(ht_file_eof_guess,nu->u.rw.fhash,nu->u.rw.offset);
        kvm_register_write(vcpu, VCPU_REGS_RAX, 0);
        failed = false;
    }else if(actual == 0){
        ht_update_val(ht_file_eof_guess,nu->u.rw.fhash,nu->u.rw.offset);
    }else{
        ht_remove(ht_file_eof_guess,nu->u.rw.fhash);
    }

    return check_result_helper(vcpu,nu,fk,failed,errormsg);
#else
    int actual = kvm_register_read(vcpu,VCPU_REGS_RAX);
    int expected = kvm_register_read(vcpu,VCPU_REGS_RDX);
    
    if(actual == 0){
        ht_update_val(ht_file_eof_guess,nu->u.rw.fhash,nu->u.rw.offset);
    }else{
        ht_remove(ht_file_eof_guess,nu->u.rw.fhash);
    }

    //skip retries for short sub sector size reads
    failed = (expected != actual && actual != 0) && (expected>JADU_BLKSIZE);
    if(failed){
        sprintf(errormsg, "%d != %d", expected, actual);
    }
    return check_result_helper(vcpu,nu,fk,failed,errormsg);
#endif
}

int check_result_all_success(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    return check_result_helper(vcpu,nu,fk,false,"");
}

int check_result_minus_one(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    char errormsg[50] = "";
#ifdef BSD 
    //check carry flag for syscall errors in bsd
    return check_result_helper(vcpu,nu,fk,(vcpu->arch.emulate_ctxt.eflags & 1UL),errormsg);
#else
    unsigned long actual = kvm_register_read(vcpu,VCPU_REGS_RAX);
    bool failed = false;
    failed = ((int)actual == -1);
    if(failed){
        sprintf(errormsg, "%lu == -1", actual);
    }
    return check_result_helper(vcpu,nu,fk,failed,errormsg);
#endif
}

int check_result_zero(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    char errormsg[50] = "";
#ifdef BSD 
    //check carry flag for syscall errors in bsd
    return check_result_helper(vcpu,nu,fk,(vcpu->arch.emulate_ctxt.eflags & 1UL),errormsg);
#else
    unsigned long actual = kvm_register_read(vcpu,VCPU_REGS_RAX);
    bool failed = false;
    failed = ((int)actual == 0);
    if(failed){
        sprintf(errormsg, "%lu == 0", actual);
    }
    return check_result_helper(vcpu,nu,fk,failed,errormsg);
#endif
}

int check_result_non_zero(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    char errormsg[50] = "";
#ifdef BSD 
    //check carry flag for syscall errors in bsd
    return check_result_helper(vcpu,nu,fk,(vcpu->arch.emulate_ctxt.eflags & 1UL),errormsg);
#else
    unsigned long actual = kvm_register_read(vcpu,VCPU_REGS_RAX);
    bool failed = false;
    failed = ((int)actual != 0);
    if(failed){
        sprintf(errormsg, "%lu == 0", actual);
    }
    return check_result_helper(vcpu,nu,fk,failed,errormsg);
#endif
}

int check_result_negative(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    char errormsg[50] = "";
#ifdef BSD 
    //check carry flag for syscall errors in bsd
    return check_result_helper(vcpu,nu,fk,(vcpu->arch.emulate_ctxt.eflags & 1UL),errormsg);
#else
    unsigned long actual = kvm_register_read(vcpu,VCPU_REGS_RAX);
    bool failed = false;
    failed = ((int)actual < 0);
    if(failed){
        sprintf(errormsg, "%lu < 0", actual);
    }
    return check_result_helper(vcpu,nu,fk,failed,errormsg);
#endif
}

int alloc_process_buffer_firsthalf(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall* fk){
    int i, inuse=0, sleepcount=0;
    struct process_data * pd = get_process_data(vcpu->arch.cr3);

start:
    jprintk("alloc buffer first half sleep count %d \n",sleepcount);
    if(fk->buffer == 0){
        fk->bufferuse = -1;
        spin_lock(&pd->lock);
        for(i=0;i<MAX_THREADS_SMALL_IO;i++){
            if(pd->bufferuse[i] == false){
                if(pd->buffer[i] != 0){
                    fk->buffer = pd->buffer[i];
                    fk->bufferuse = i;
                    pd->bufferuse[i] = true;
                    break;
                }else{
                    fk->bufferuse = i;
                }
            }else{
                inuse++;
            }
        }
        spin_unlock(&pd->lock);

        if(i>=MAX_THREADS_SMALL_IO){
            if(inuse == MAX_THREADS_SMALL_IO){
                jprintk("waiting because too many threads using buffers concurrently %d \n", inuse);
                msleep(1000);
                sleepcount++;
                if(sleepcount < 20){
                    goto start;
                }
                jprinte("ERROR too many threads using buffers after sleep and retries \n");
            }else{
                return 1;
            }
        }
    }
    return 0;
}

void alloc_process_buffer_secondhalf(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    if(fk->buffer == 0){
        //copy registers
        fk->buffer = kvm_register_read(vcpu,VCPU_REGS_RAX);
        pd->buffer[fk->bufferuse] = fk->buffer;
        jprintk("alloc buffer second half buffer ptr %lx \n",fk->buffer);
    }else{
        //zero the buffer
        if(pd->bufferfullalloc[fk->bufferuse]){
            guestmemzero(vcpu,fk->buffer,MMAP_SIZE); 
        }else{
            guestmemzero(vcpu,fk->buffer,4096); 
        }
    }
}

void free_process_buffer(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    jprintk("free process buffer \n");
    if(fk->buffer != 0){
        spin_lock(&pd->lock);
        pd->bufferuse[fk->bufferuse] = false;
        fk->buffer = 0;
        spin_unlock(&pd->lock);
    }
}

void freebuffer_lastfake(struct kvm_vcpu *vcpu, struct nitrodatau *nu,struct fakescall *fk){
    free_process_buffer(vcpu,nu,fk);
    fk->last = true;
}

void jadukata_iosubmit_loopfinish(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk);
int jadukata_sysret_io_submit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu);

#ifdef JADUKATA_CHECKSUM

int jadukata_process_read_write(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu, bool failed, struct pair* pairs, int* numpairs, int maxpairs);
int jadukata_process_readv_writev(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu, bool failed, struct pair* pairs, int* numpairs, int maxpairs);
#ifndef BSD
int jadukata_process_reada_writea(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu, bool failed, struct pair* pairs, int* numpairs, int maxpairs);
#endif

typedef int (*process_specific_fntype)(struct kvm_vcpu*, char prefix, struct nitrodatau*, bool failed, struct pair* pairs, int* numpairs, int maxpairs);
int jadukata_process_common(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu,bool failed, process_specific_fntype process_specific_fn );

extern spinlock_t read_coll_lock;

#ifdef DETECT_OVERWRITES
extern hash_table *ht_file_chunksums;
#endif
extern hash_table *ht_data_checksums_read;
extern hash_table *ht_data_checksums_read_coll;
extern hash_table *ht_data_checksums_write;
extern hash_table *ht_data_smallfile_sectors;
extern hash_table *ht_outstanding;

#ifdef DEDUP_WAR_PREFETCH
extern hash_table *ht_dedup_war_prefetch_detect;
#endif

extern hash_table *ht_fork_parent_pids;
extern hash_table *ht_cr3_to_pid;

struct outstanding{
    struct pair outpairs[128];
    int outnumpairs;
    unsigned long outstanding_gva;
    int outstanding_size;
    int call_outstanding;
};


extern atomic_t jadu_rmetadata;
extern atomic_t jadu_rdata;
extern atomic_t jadu_wbgf;
extern atomic_t jadu_wsmf;
extern atomic_t jadu_wioclass[6];
extern atomic_t jadu_rbgf;
extern atomic_t jadu_rsmf;
extern atomic_t jadu_rioclass[6];

struct pchksum_args {
    struct kvm_vcpu* vcpu;
    int rw;
    unsigned long fhash;
    int chks;
    short ioclass;
    bool failed;
#ifdef DEDUP_WAR_PREFETCH
    bool dedup_war_prefetch_detect;
    bool dedup_war_prefetch;
#endif
#ifdef DEDUP_U_HINT
    bool dedup_uniq_hint;
#endif
};

extern struct cleaner_data* write_cleaner;
#ifdef DEDUP_WAR_PREFETCH
extern struct cleaner_data* dedup_war_prefetch_detect_cleaner;
#endif

atomic_t debug_time = ATOMIC_INIT(0);

//atomic_t time_sum  = ATOMIC_INIT(0);
//atomic_t time_cnt = ATOMIC_INIT(0);

extern void dedup_war_hint(unsigned long checksum, unsigned long sector);
extern void dedup_w_hint(unsigned long checksum);

#if defined(WRITE_DELAY)
atomic_t writedelay_sum = ATOMIC_INIT(0);
atomic_t writedelay_count = ATOMIC_INIT(0);
#endif
      
#if defined(BLOCK_LIFETIME) || defined(WRITE_DELAY)
extern hash_table *ht_blocklifetime;
#endif

#ifdef BLOCK_LIFETIME
extern hash_table *ht_series_blocklifetime;
#endif

#ifdef WKLD_TRACE            
void add_to_series(hash_table* table, unsigned long val, unsigned long units){
    jprintk("add_to_series: htname %s val %lu units %lu \n", table->name, val ,units);
    ht_add_sub_val(table,val/units,1);
}
#endif

int process_checksum_success(void* pargs, int chunknum, unsigned long checksum){
    
    struct pchksum_args* args = (struct pchksum_args*)pargs;
    int ret = 0;
    if(args->rw == 1){
        //struct timeval s_tv, e_tv;
        //do_gettimeofday(&s_tv);
    
        unsigned long oldpacksum;
#ifdef DETECT_OVERWRITES
        unsigned long fhash,oldpacksum1;
        fhash = PACK_INTS_TO_LONG(args->fhash,(args->chks+chunknum));
        if(ht_lookup_val(ht_file_chunksums,fhash,&oldpacksum) != 0){
            if(ht_lookup_val(ht_data_checksums_write, pack_info_get_y(oldpacksum),&oldpacksum1) != 0){
                ht_update_val(ht_data_checksums_write, pack_info_get_y(oldpacksum),pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,pack_info_get_z(oldpacksum1)));
            }
        }
        ht_update_val(ht_file_chunksums,fhash,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),checksum,0));
#endif
#if defined(BLOCK_LIFETIME) || defined(WRITE_DELAY)
        unsigned long fhash,oldval;
        fhash = PACK_INTS_TO_LONG(args->fhash,(args->chks+chunknum));
        if(ht_lookup_val(ht_blocklifetime,fhash,&oldval) != 0){
            jprintk("jiffies %llu get_jiffies %lu jsecs_to_jiffies(oldval) %lu jiffies %lu \n", jiffies, get_jiffies(), jsecs_to_jiffies(oldval),(get_jiffies() - jsecs_to_jiffies(oldval)));
#if defined(BLOCK_LIFETIME)
            add_to_series(ht_series_blocklifetime,jiffies_to_msecs(get_jiffies() - jsecs_to_jiffies(oldval)),BLOCK_LIFETIME_SERIES_UNIT_MSECS);
#endif
#if defined(WRITE_DELAY)
            int writedelaysumval = atomic_add_return((jiffies_to_msecs(get_jiffies() - jsecs_to_jiffies(oldval))),&writedelay_sum);
            int writedelaycountval = atomic_add_return(1,&writedelay_count);
            if(unlikely(writedelaycountval % 100 == 1)){
                extern void bcache_set_write_delay(int delay);
                int delay = writedelaysumval/writedelaycountval;
                delay += WRITE_DELAY_ADDITIONAL_MSECS ; // add some more secs to calculated write delays
                jprintk("updating writeback delay value to %d msecs = %d secs\n", delay, delay/1000);
                bcache_set_write_delay(delay/1000);
                if(unlikely(writedelaysumval > (INT_MAX/2))){
                    atomic_set(&writedelay_count,1);
                    atomic_set(&writedelay_sum,delay);
                }
            }
#endif
        }
        ht_update_val(ht_blocklifetime,fhash,jiffies_to_jsecs(get_jiffies()));
#endif

        if(ht_lookup_val(ht_data_checksums_write, checksum,&oldpacksum) != 0){
            //if data writes collide, instead of keeping counts of collision approximation using
            //timers is used
            // also currently higher of the ioclass is picked
            short ioclass = pack_info_get_z(oldpacksum);
            if(args->ioclass > ioclass){
                ioclass = args->ioclass;
            }
            ht_update_val(ht_data_checksums_write, checksum, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,ioclass));
        }else{
            ht_update_val(ht_data_checksums_write, checksum, pack_info_xyz(jiffies_to_jsecs(0),~0,args->ioclass));
        }

#ifdef DEDUP_WAR_PREFETCH
        if(args->dedup_war_prefetch_detect && (ht_lookup_val(ht_dedup_war_prefetch_detect, checksum,&oldpacksum) != 0)){
            struct process_data * pd = get_process_data(args->vcpu->arch.cr3);
            pd->dedup_war_prefetch = true;
            jprintm("setting war prefetch to true for process cmd %s cr3 %lx \n", pd->cmd,args->vcpu->arch.cr3);
        }
#endif
#ifdef DEDUP_W_PREFETCH
        jprintk("hint w prefetch checksum %lx\n", checksum);
        dedup_w_hint(checksum);
#endif
    
        //do_gettimeofday(&e_tv);
        //atomic_add(diff_time(s_tv, e_tv),&time_sum);
        //int count = atomic_inc_return(&time_cnt);
        //if(count > 10000){
        //    int sum = atomic_read(&time_sum);
        //    jprinte("PERF: count %d sum %d avg %d \n",count, sum, sum/count);
        //    atomic_set(&time_sum,0);
        //    atomic_set(&time_cnt,0);
        //} 

    }else{
        unsigned long packedinfo;
        if(ht_lookup_val(ht_data_checksums_read,checksum,&packedinfo) != 0){
            //z char used for checksum collisions for reads alone
            // collisions in reads lead to shared fate all metadata or all data
            if(pack_info_get_z(packedinfo) != 0){
                hash_table *ht_temp = NULL;
                unsigned long flags;
                unsigned long temppackinfo;
                spin_lock_irqsave(&read_coll_lock,flags);
                if(ht_remove_val(ht_data_checksums_read_coll, checksum, (unsigned long*)&ht_temp) != -1){
                    int sect = 0;
                    spin_unlock_irqrestore(&read_coll_lock,flags);
                    if(args->ioclass){
                        while(ht_pop(ht_temp, &temppackinfo) != -1){
                            sect = pack_info_get_y(temppackinfo);
                            jprintk("added %ld to ht_smf \n", sect);
                            if(sect % SMALLFILE_DIVIDER == SMALLFILE_QUOTIENT){
                                ht_update_val(ht_data_smallfile_sectors,sect,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,args->ioclass));
                            }
                        }
                    }else{
                        if(ht_pop(ht_temp, &temppackinfo) != -1){
                            sect = pack_info_get_y(temppackinfo);
                        }
                    }
#ifdef DEDUP_WAR_PREFETCH
                    if(args->dedup_war_prefetch){
                        jprintk("hint war prefetch checksum %lx \n", checksum);
                        dedup_war_hint(checksum,sect);
                    }else if(args->dedup_war_prefetch_detect){
                        if(sect){
                            ht_update_val(ht_dedup_war_prefetch_detect, checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),sect,args->ioclass));
                        }
                    }
#endif
                    ht_destroy(ht_temp);
                }else{
                    spin_unlock_irqrestore(&read_coll_lock,flags);
                    jprintl("WARN: hash table not found for collisions in read checksum %lx \n", checksum);
                }
            }else{
                int sect = pack_info_get_y(packedinfo);
                if(args->ioclass){
                    jprintk("added %ld to ht_smf \n", sect);
                    if(sect % SMALLFILE_DIVIDER == SMALLFILE_QUOTIENT){
                        ht_update_val(ht_data_smallfile_sectors,sect,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,args->ioclass));
                    }
                }
#ifdef DEDUP_WAR_PREFETCH
                if(args->dedup_war_prefetch){
                    jprintk("hint war prefetch checksum %lx \n", checksum);
                    dedup_war_hint(checksum,sect);
                }else if(args->dedup_war_prefetch_detect){
                    ht_update_val(ht_dedup_war_prefetch_detect, checksum,pack_info_xyz(jiffies_to_jsecs(get_jiffies()),sect,args->ioclass));
                }
#endif
            }
            ht_remove(ht_data_checksums_read,checksum);

            if(args->ioclass){
                atomic_inc(&jadu_rsmf);
            }else{
                atomic_inc(&jadu_rbgf);
            }
            args->ioclass = ((args->ioclass<0)?0:((args->ioclass>5)?5:args->ioclass));
            atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_rioclass[args->ioclass]);
            atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_rdata);
        }else{
            atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_rioclass[METADATA_IOCLASS]);
            atomic_add(JADU_NR_HARDSECT_PER_BLK,&jadu_rmetadata);
            if(checksum){
                jprintk("nitrowarnmeta ERROR checksum #%d : checksum %lx\n", chunknum, checksum);
            }else{
                jprintk("nitrowarnmeta WARN checksum #%d : checksum %lx\n", chunknum, checksum);
            }
            ret = 1;
        }
    }

    return ret;
}

int process_checksum_failure(void* pargs, int chunknum, unsigned long checksum){
    struct pchksum_args* args = (struct pchksum_args*)pargs;
    int ret = 0;
    if(args->rw == 1) {
        if(ht_lookup(ht_data_checksums_write, checksum) != 0){
            //make the failed write timeout 
            ht_update_val(ht_data_checksums_write, checksum, pack_info_xyz(jiffies_to_jsecs(get_jiffies()),~0,args->ioclass));
        }
    }else{
        jprinte("ERROR: failure handling for READs sector %lu checksum %lx \n", args->chks+chunknum, checksum);
    }

    return ret;
}

int process_checksum(void* pargs, int chunknum, unsigned long checksum){
    struct pchksum_args* args = (struct pchksum_args*)pargs;
    int ret = 0;
#if defined(DEDUP_WAR_PREFETCH)
    jprintl("nitro %s checksums #%d : checksum %lx ioclass %d failed: %s wardetect %d war %d uniq %d\n", args->rw==1 ? "WRITE" : "READ",  chunknum, checksum,args->ioclass,args->failed?"true":"false", args->dedup_war_prefetch_detect, args->dedup_war_prefetch, false);
#elif defined(DEDUP_U_HINT)
    jprintl("nitro %s checksums #%d : checksum %lx ioclass %d failed: %s wardetect %d war %d uniq %d\n", args->rw==1 ? "WRITE" : "READ",  chunknum, checksum,args->ioclass,args->failed?"true":"false", false, false,args->dedup_uniq_hint);
#else
    jprintl("nitro %s checksums #%d : checksum %lx ioclass %d failed: %s wardetect %d war %d uniq %d\n", args->rw==1 ? "WRITE" : "READ",  chunknum, checksum,args->ioclass,args->failed?"true":"false", false, false,false);
#endif
    if(args->failed){
        ret = process_checksum_failure(pargs,chunknum,checksum);
    }else{
        ret = process_checksum_success(pargs,chunknum,checksum);
    }

    do_cleanup(write_cleaner);
#ifdef DETECT_OVERWRITES
    extern struct cleaner_data* fhash_cleaner;
    do_cleanup(fhash_cleaner);
#endif
#ifdef DEDUP_WAR_PREFETCH
    do_cleanup(dedup_war_prefetch_detect_cleaner);
#endif

    return ret;
}

//for failed reads, skip the failed part
//for failed writes, skip the unfailed part because we are removing checksums
int jadukata_adjust_pairs(struct nitrodatau * nu, struct pair** pairsptr, int *totpairs, bool failed){
    int i;
    unsigned long shortoff = 0, excess = 0;
    unsigned long sizesum = 0;
    bool readfailed = false;
    struct pair* pairs = *pairsptr;

    if(nu->u.rw.rw == 0x0){
        readfailed = (nu->u.rw.size - nu->u.rw.act_size);
    }

    if(failed || readfailed){
        if(nu->fkoffset){
            //process the misaligned/small i/o
            shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
            excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
        }

        int chunks = 0;
        //failed partial sector if any is ignored 
        chunks = ((shortoff + nu->u.rw.act_size) /JADU_BLKSIZE);
        
        jprintk("nitro failed i/o adjusting pairs rw %s failed %s size %lu actsize %lu chunks %d \n", nu->u.rw.rw==1 ? "WRITE" : "READ",  failed?"true":"false", nu->u.rw.size, nu->u.rw.act_size, chunks);

        if(chunks <=0){
            return 0;
        }


        {
            struct pair* ppairs = *pairsptr;
            int i;
            jprintk("before totpairs %d \n", *totpairs);
            for(i=0;i<*totpairs;i++){
                jprintk("before %d: addr %p size %d \n", i, ppairs[i].addr, ppairs[i].size);
            }
        }

        int j; 
        int jsize = -1;
        //find the sector uptil which i/o succeeded
        {
            int bytes;
            int remaining = 0;
            int chunknum = 0;

            for(j=0;j<*totpairs;j++){
                bytes = pairs[j].size;
                if(remaining < 0){
                    if( (bytes + remaining) >=0 ){
                        chunknum++;
                        if(chunknum >= chunks){
                            jsize = -remaining;
                            goto done;
                        }
                    }
                }
                remaining = bytes+remaining;
                while(remaining>=JADU_BLKSIZE){
                    chunknum++;
                    remaining -= JADU_BLKSIZE;
                    if(chunknum >= chunks){
                        jsize = bytes-remaining;
                        goto done;
                    }
                }
                if(remaining > 0){
                    remaining -= JADU_BLKSIZE;
                }
            }
        }
        done:
        //adjust pairs
        if(j < *totpairs){
            if(nu->u.rw.rw == 0x0){
                pairs[j].size = jsize;
                *totpairs = j+1;
            }else{
                *pairsptr = pairs + j;
                pairs[j].size -= jsize;
                pairs[j].addr += jsize;
                *totpairs = *totpairs - (j + 1);
            }
            jprintk("after adjust totpairs %d \n", *totpairs);
        }else{
            jprinte("ERROR: all pairs used up in failed mode j %d totpairs %d \n", j , *totpairs);
        }
        
        {
            struct pair* ppairs = *pairsptr;
            int i;
            jprintk("after totpairs %d \n", *totpairs);
            for(i=0;i<*totpairs;i++){
                jprintk("after %d: addr %p size %d \n", i, ppairs[i].addr, ppairs[i].size);
            }
        }
    }
    return 0;
}

int jadukata_translate(struct kvm_vcpu *vcpu, gva_t gva, int size, struct pair* pairs, int *totpairs, int maxpairs){
    unsigned bytes;
    unsigned offset;
    unsigned toread;
    int i,r=0;
    hva_t hva;

    char c;

    i = *totpairs;
    bytes = size;
    
    while (bytes) {
        offset = gva & (PAGE_SIZE-1);
        toread = min(bytes, (unsigned)PAGE_SIZE - offset);

        hva = jadukata_gva_to_hva(vcpu, gva);
        if(hva == 0){
            jprinte("ERROR invalid hva\n");
            r = 1;
            break;
        }

        pairs[*totpairs].addr = hva;
        pairs[*totpairs].size = toread; 
        
        bytes -= toread;
        gva += toread;

        *totpairs = *totpairs + 1;
        if(*totpairs>=maxpairs){
            jprinte("ERROR need more address pairs totpairs %d maxpairs %d \n", *totpairs, maxpairs);
            r = 1;
            break;
        }
    }
    
    for(;i<*totpairs;i++){
        jprintk("gva %lx pair #%d : addr %lx size %u \n", gva, i, pairs[i].addr, pairs[i].size);
    }
    return r;
}

void init_pchksum_args(struct kvm_vcpu *vcpu, struct pchksum_args *pargs, struct nitrodatau* nu, bool failed){
    int size;
    pargs->vcpu = vcpu;
    pargs->rw = nu->u.rw.rw;
    pargs->fhash = get_filehash_fd(vcpu->arch.cr3,nu->u.rw.fd);
    //nu->u.rw.ioclass is set to 1 + application provided ioclass
    //by default it is 0
    if(nu->u.rw.ioclass > 0){
        pargs->ioclass = nu->u.rw.ioclass-1;
    }else{
#ifdef STRICT_APP_PRIORITY
        pargs->ioclass = 0;
#else
        size = get_filesize_fd(vcpu->arch.cr3,nu->u.rw.fd,nu->u.rw.offset?true:false);
        pargs->ioclass = filesize_to_ioclass(size);
        jprintk("fd %d filesize %d ioclass %d offset %d\n", nu->u.rw.fd, size, pargs->ioclass, nu->u.rw.offset);
#endif
    }
    pargs->chks = nu->u.rw.offset/JADU_BLKSIZE;
    pargs->failed = failed;
#if defined(DEDUP_WAR_PREFETCH) || defined(DEDUP_U_HINT)
#ifdef DEDUP_U_HINT
    pargs->dedup_uniq_hint = false;
#endif
    struct process_data * pd = get_process_data(vcpu->arch.cr3);
#ifdef DEDUP_WAR_PREFETCH
    pargs->dedup_war_prefetch_detect = false;
    pargs->dedup_war_prefetch = false;
    if(!pd->dedup_war_prefetch){
        if((strncmp(pd->cmd,"dedupcpclient",13)==0) ||(strncmp(pd->cmd,"cp",2) == 0) || (strncmp(pd->cmd,"dd",2) == 0) || (strncmp(pd->cmd,"mydd",4) == 0)){
            pargs->dedup_war_prefetch_detect = true;
        }
    }else{
        pargs->dedup_war_prefetch = true;
    }
#endif
#ifdef DEDUP_U_HINT
    if(!pd->dedup_uniq_hint){
        if((strncmp(pd->cmd,"dedupgpgclient",14)==0) ||(strncmp(pd->cmd,"gpg",3) == 0) || (strncmp(pd->cmd,"dedupgzipclient",14) == 0) || (strncmp(pd->cmd,"gzip",4) == 0)){
            pd->dedup_uniq_hint = true;
        }
    }else{
        pargs->dedup_uniq_hint = true;
        pargs->ioclass |= DEDUP_U_IOCLASS_MASK;
    }
#endif
#endif
}
    
void checksum_caller(struct kvm_vcpu *vcpu, int rw, int fd, struct pair *pairs, int numpairs, struct pchksum_args* pargs){
    
    long time;
    struct timespec ts, te;
    getnstimeofday(&ts);
        
#ifdef JADUKATA_CHECKSUM
#ifdef JADUKATA_VERBOSE
    checksum_processor1(pairs,numpairs); 
#endif
    checksum_processor2((void*)pargs,pairs,numpairs);
#endif
    
    getnstimeofday(&te);
    time = TIMEDIFF(te,ts);
    atomic_add(time/1000,&debug_time);
}

extern hash_table *ht_file_lastwrite_content;

void padded_finish(struct kvm_vcpu *vcpu, struct nitrodatau *nu,struct fakescall *fk){
    bool inloop = false;
#define should_save_excess(sizearg,excessarg) ((excessarg <= 4080) && (sizearg >= excessarg)) 
#ifndef BSD
    if(!is_async(fk->orgscall)){
#endif
        if(nu->u.rw.rw == 0x1){
            nu->u.rw.act_size = kvm_register_read(vcpu, VCPU_REGS_RAX);
            if(nu->u.rw.act_size != nu->u.rw.size){
                if(is_vectored(fk->orgscall)){
                    jadukata_process_common(vcpu,'f',nu,true,jadukata_process_readv_writev);
                }else{
                    jadukata_process_common(vcpu,'f',nu,true,jadukata_process_read_write);
                }
            }else{
                //for strided write patterns remember the last write to save on a pread system call
                //for shortoff
                unsigned long val = 0;
                unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
                if(ht_remove_val(ht_file_lastwrite_content,nu->u.rw.fhash,&val) == -1){
                    if(should_save_excess(nu->u.rw.size,excess)){
                        val = (unsigned long)my_vzalloc(4096);
                    }
                }else{
                    if(!should_save_excess(nu->u.rw.size,excess)){
                        my_vfree((void*)val,4096);
                    }
                }
                if(should_save_excess(nu->u.rw.size,excess)){
                    struct x86_exception ex;
                    ht_add_val(ht_file_lastwrite_content,nu->u.rw.fhash,val);
                    unsigned long *ptr = (unsigned long*)val;
                    ptr[0] = nu->u.rw.offset + nu->u.rw.size - excess;
                    ptr[1] = excess; 
                    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,nu->u.rw.ptr+nu->u.rw.size-excess,ptr+2,excess,&ex);
                }
            }
        }
        if(nu->u.rw.rw == 0x0){
            if(is_vectored(fk->orgscall)){
                jadukata_process_common(vcpu,'f',nu,false,jadukata_process_readv_writev);
            }else{
                jadukata_process_common(vcpu,'f',nu,false,jadukata_process_read_write);
            }
        }
        kvm_register_write(vcpu, VCPU_REGS_RAX, nu->u.rw.act_size);
        unlock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size);
#ifndef BSD
    }else{
        jadukata_process_common(vcpu,'f',nu,false, jadukata_process_reada_writea);
    }
#endif
    
    //update current offsets 
    if(!is_async(fk->orgscall)){
        if(!is_preadwrite(fk->orgscall)){
            increment_fd_offsets(vcpu->arch.cr3, nu->u.rw.fd, nu->u.rw.act_size);
        }
        freebuffer_lastfake(vcpu,nu,fk);
    }else{
        bool somemoreleft = (nu->u.rw.asyncio_i < nu->u.rw.asyncio_nr);
        free_process_buffer(vcpu,nu,fk);
        if(somemoreleft){
            somemoreleft = nu->u.rw.asyncio_fn('l',vcpu,nu);
        }
        if(!somemoreleft){
#ifndef BSD
            if(fk->orgscall == IOSUBMIT_SCALL_NR){
                //finally issue the original io_submit syscall
                //in case there were any misaligned i/o
                jadukata_fake_restore_org(vcpu,nu,&nu->u.rw.fkd);
                jadukata_fake_syscall(vcpu, nu, &nu->u.rw.fkd, jadukata_iosubmit_loopfinish, check_result_negative, nu->u.rw.fkd.orgscall, 0);
            }else
#endif
            {
                //restore the original io_getevents return code
                kvm_register_write(vcpu, VCPU_REGS_RAX, nu->u.rw.asyncio_nr);
                freebuffer_lastfake(vcpu,nu,fk);
            }
        }
    }
}

void perform_padded_read_write(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    if(!is_async(fk->orgscall) && nu->u.rw.rw == 0x1){
        jprintk("calculate checksums of aligned sectors\n");
        if(is_vectored(fk->orgscall)){
            jadukata_process_common(vcpu,'f',nu,false,jadukata_process_readv_writev);
        }else{
            jadukata_process_common(vcpu,'f',nu,false,jadukata_process_read_write);
        }

        //for writes issue org scall after reading shortoff or excess
        jadukata_fake_restore_org(vcpu,nu,fk);
        jadukata_fake_syscall(vcpu, nu, fk, padded_finish, check_result_minus_one, fk->orgscall, 0);

    }else{
        padded_finish(vcpu,nu,fk);
    }
}

void read_padded_excess(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
            
    jprintk("read padded excess %lu \n", (JADU_BLKSIZE-excess));
    if(excess && !nu->u.rw.skip_eof){
        jadukata_fake_syscall(vcpu, nu, fk, perform_padded_read_write, check_result_pread, (unsigned long)PREAD_SCALL_NR, 4, nu->u.rw.fd,(fk->buffer+JADU_BLKSIZE),(JADU_BLKSIZE-excess), nu->u.rw.offset + nu->u.rw.size);
    }else{
        perform_padded_read_write(vcpu,nu,fk);
    }
}

void read_padded_shortoff(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
        
    if(!is_async(fk->orgscall) && nu->u.rw.rw == 0x0){
        nu->u.rw.act_size = kvm_register_read(vcpu, VCPU_REGS_RAX);
    }

    jprintk("read padded shortoff %lu \n", shortoff);
    if(shortoff){
        bool done = false;
        unsigned long val = 0;
        unsigned long offsetval = (nu->u.rw.offset-shortoff);
        if(ht_lookup_val(ht_file_lastwrite_content,nu->u.rw.fhash,&val) != 0){
            struct x86_exception ex;
            unsigned long *ptr = (unsigned long*)val;
            jprintk("read padded shortoff lastwrite content optimize shortoff %lu offsetval %lu ptr[0] %lu ptr[1] %lu \n", shortoff,offsetval, ptr[0],ptr[1]);
            if(offsetval>=ptr[0] && ((offsetval+shortoff) <= (ptr[0] + ptr[1]))){
                kvm_write_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer,(void*)((char*)(ptr+2)+(offsetval-ptr[0])),shortoff,&ex); 
                read_padded_excess(vcpu,nu,fk);
                done = true;
            }
        }
        if(!done){
            jadukata_fake_syscall(vcpu, nu, fk, read_padded_excess, check_result_pread, (unsigned long)PREAD_SCALL_NR, 4, nu->u.rw.fd,fk->buffer,shortoff, offsetval);
        }
    }else{
        read_padded_excess(vcpu,nu,fk);
    }
}

void read_padded_data(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
    jprintk("read padded data excess %lu shortoff %lu \n", excess, shortoff);
    if(shortoff || excess){
        if(!is_async(fk->orgscall) && nu->u.rw.rw == 0x0){
            //for reads issue org scall before reading shortoff or excess iff sync
            jadukata_fake_restore_org(vcpu,nu,fk);
            jadukata_fake_syscall(vcpu, nu, fk, read_padded_shortoff, check_result_minus_one, fk->orgscall, 0);
        }else{
            //read the padd data first for writes
            read_padded_shortoff(vcpu,nu,fk);
        }
    }
}

void alloc_process_buffer_dirty_it(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    jprintk("alloc process buffer dirty it size %ld\n",nu->u.rw.act_size);
   
    if(nu->u.rw.act_size == MMAP_SIZE){
        alloc_process_buffer_secondhalf(vcpu,nu,fk);
    }

    if(nu->u.rw.act_size >= 4096){
        nu->u.rw.act_size -= 4096;
        jadukata_fake_syscall(vcpu, nu, fk, alloc_process_buffer_dirty_it, dirty_scall_result_check, (unsigned long)DIRTY_SCALL_NR, 2, fk->buffer + nu->u.rw.act_size,4096);
    }else{
        struct process_data * pd = get_process_data(vcpu->arch.cr3);
        pd->bufferfullalloc[fk->bufferuse] = true;
        nu->u.rw.act_size = 0;
        read_padded_data(vcpu,nu,fk);
    }
}

//starting point of fake scalls for read write related scalls
void padded_checksum(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);

    jprintk("padd %s excess %lu shortoff %lu \n", is_vectored(nu->out_syscall)?"rwv":"rw", excess, shortoff);
    if((shortoff || excess) ){
        //avoid saving because padded_checksum gets called during iogetevents sysret for misaligned async reads
        //instead fk->orgscall is set manually before calling padded_checksum
        if(!is_async(fk->orgscall)){
            jadukata_fake_save_org(vcpu,nu,fk);
        }
        short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu,fk);
        if(mmap_needed){
            nu->u.rw.act_size = MMAP_SIZE;
            //mmap anonymous shared to allocate buffer space
            jadukata_fake_syscall(vcpu, nu, fk, alloc_process_buffer_dirty_it, check_result_minus_one, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, MMAP_SIZE, 3UL, MMAP_FLAGS, (unsigned long)-1, 0UL);
        }else{
            struct process_data * pd = get_process_data(vcpu->arch.cr3);
            if(!pd->bufferfullalloc[fk->bufferuse]){
                nu->u.rw.act_size = MMAP_SIZE;
                alloc_process_buffer_dirty_it(vcpu,nu,fk);
            }else{
                alloc_process_buffer_secondhalf(vcpu,nu,fk);
                read_padded_data(vcpu,nu,fk);
            }
        }
    }
}

int jadukata_process_common(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu,bool failed, process_specific_fntype process_specific_fn ){
    int rc = 0;
#ifdef JADUKATA_CHECKSUM
#define PNUM (64)
    int maxpairs;
    int TPAIR_PAGES_ORDER = 0;
    struct pchksum_args pargs;
    struct pair tpairs[PNUM];
    struct pair *pairs = NULL, *alloced_pairs = NULL;
    
    maxpairs = (nu->u.rw.size/512);

    if(maxpairs > PNUM){
        int maxpairssize = (maxpairs*sizeof(struct pair));
        TPAIR_PAGES_ORDER = get_order(maxpairssize); 
    
        jprintm("maxpair %ld\n",maxpairssize);

#ifdef VCPU_PAIRSBUF
        if(vcpu->nitrodata.pairsbuf == NULL ||  vcpu->nitrodata.pairsnum < maxpairssize){
            //pairs = (struct pair*)__get_free_pages(GFP_KERNEL,TPAIR_PAGES_ORDER);
            pairs = my_kmalloc(maxpairssize,GFP_KERNEL);
            if(vcpu->nitrodata.pairsbuf != NULL){
                my_kfree(vcpu->nitrodata.pairsbuf,vcpu->nitrodata.pairsnum);
            }
            vcpu->nitrodata.pairsbuf = pairs;
            vcpu->nitrodata.pairsnum = maxpairssize;
        }
        pairs = vcpu->nitrodata.pairsbuf;
#else
        //pairs = (struct pair*)__get_free_pages(GFP_KERNEL,TPAIR_PAGES_ORDER);
        pairs = my_kmalloc(maxpairssize,GFP_KERNEL);
        alloced_pairs = pairs;
#endif
    }else{
        pairs = tpairs;
        maxpairs = PNUM;
    }

    int numpairs=0;
    unsigned long shortoff=0, excess =0;
    struct fakescall *fk = &nu->u.rw.fkd;

    if((unsigned long)process_specific_fn == (unsigned long)jadukata_process_read_write){
        jprintk("syscall rw %s ptr %lx size %lu actsize %lu skip %d \n", nu->u.rw.rw?"WRITE":"READ", nu->u.rw.ptr, nu->u.rw.size, nu->u.rw.act_size,nu->u.rw.skip_eof);
    }else if((unsigned long)process_specific_fn == (unsigned long)jadukata_process_readv_writev){ 
        jprintk("syscall rwv %s ptr %lx count %lu size %lu actsize %lu offset %lu skip %d \n", nu->u.rw.rw?"WRITE":"READ", nu->u.rw.iov_ptr, nu->u.rw.iov_cnt, nu->u.rw.size, nu->u.rw.act_size, nu->u.rw.offset,nu->u.rw.skip_eof);
#ifndef BSD
    }else if((unsigned long)process_specific_fn == (unsigned long)jadukata_process_reada_writea){
        jprintk("syscall rwa %s ptr %lx size %lu offset %lld \n", nu->u.rw.rw?"WRITE":"READ", nu->u.rw.ptr, nu->u.rw.size, nu->u.rw.offset);
#endif
    }
    
#ifdef COMPUTE_BETWEEN_IO
    vcpu->nitrodata.io_amount += (nu->u.rw.size/COMPUTE_IOAMOUNT_UNIT_BYTES);
#endif

    //process the misaligned/small i/o
    shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);

    if(nu->u.rw.rw == 0x1 && (!excess || excess>4080)){
        ht_remove(ht_file_lastwrite_content,nu->u.rw.fhash);
    }

    if(shortoff){
        rc |= jadukata_translate(vcpu, fk->buffer, shortoff, pairs, &numpairs, maxpairs);
    }

    if(process_specific_fn){
        rc |= process_specific_fn(vcpu,prefix,nu,failed,pairs, &numpairs, maxpairs);
    }

    if(excess){
        if(nu->u.rw.skip_eof){
            pairs[numpairs].addr = (unsigned long)vcpu->nitrodata.zerobuf;
            pairs[numpairs].size = JADU_BLKSIZE-excess; 
            numpairs++;
            if(numpairs>=maxpairs){
                jprinte("ERROR need more address pairs totpairs %d maxpairs %d \n", numpairs, maxpairs);
            }
        }else{
            rc |= jadukata_translate(vcpu, fk->buffer+JADU_BLKSIZE, (JADU_BLKSIZE-excess), pairs, &numpairs, maxpairs);
        }
    }

    jadukata_adjust_pairs(nu,&pairs,&numpairs,failed);

    //if(rc == 0) {
        init_pchksum_args(vcpu,&pargs,nu,failed);
        checksum_caller(vcpu,nu->u.rw.rw,nu->u.rw.fd,pairs,numpairs,&pargs);
    //}
#ifndef VCPU_PAIRSBUF
    if(maxpairs > PNUM){
        //free_pages((unsigned long)pairs,TPAIR_PAGES_ORDER);
        my_kfree(alloced_pairs,maxpairssize);
    }
#endif
    return rc;
#endif
}

//eof skip
inline bool skip_eof(struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    bool eofskip = false;
    unsigned long eofoffset = ULONG_MAX;
    
    nu->u.rw.fhash = get_filehash_fd(vcpu->arch.cr3,nu->u.rw.fd);

    if(ht_lookup_val(ht_file_eof_guess,nu->u.rw.fhash,&eofoffset) != 0){
        if(nu->u.rw.offset >= eofoffset){
            eofskip = true;
        }
    }

    return eofskip;
}

//for reads after syscall finishes, the actual i/o completed is processed
//for writes after syscall finishes, the part of the i/o that failed is discarded
int jadukata_process_read_write(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu,bool failed, struct pair* pairs, int* numpairs, int maxpairs){
    int rc = 0;
    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
    /*
    if(unlikely(nu->u.rw.skip_eof)){
        if(nu->u.rw.size > ((shortoff?(JADU_BLKSIZE-shortoff):0)+excess)){
            rc |= jadukata_translate(vcpu, nu->u.rw.ptr+(shortoff?(JADU_BLKSIZE-shortoff):0), (nu->u.rw.size-(shortoff?(JADU_BLKSIZE-shortoff):0)-excess), pairs, numpairs, maxpairs);
        }
    }else
    {
        */
        rc |= jadukata_translate(vcpu, nu->u.rw.ptr, nu->u.rw.size, pairs, numpairs, maxpairs);
    /*
     } */

    return rc;
}
#endif

int jadukata_syscall_read_write(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadwrite){
    int ret = 0;
    nu->u.rw.fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    //jprintk("syscall read write fd: %d \n", nu->u.rw.fd);
    jprintk("syscall read write fd: %d r10 %lx r8 %lx r9 %lx\n", nu->u.rw.fd, kvm_register_read(vcpu, VCPU_REGS_R10), kvm_register_read(vcpu, VCPU_REGS_R8), kvm_register_read(vcpu, VCPU_REGS_R9));

    /*
    //debuggin purpose
    {
        struct x86_exception ex;
        char tbuf[1024];
        tbuf[1023] = '\0';
        if(nu->u.rw.fd == 2){
            //temp error code
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,kvm_register_read(vcpu, VCPU_REGS_RSI), tbuf , kvm_register_read(vcpu, VCPU_REGS_RDX) ,&ex);
            jprintk("fd 2 content leo %s \n", tbuf);
        }
    }
    */

    if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
        if(preadwrite){
            if(kvm_register_read(vcpu, VCPU_REGS_R8) == GUEST_APP_AIO_MAGIC){
                nu->u.rw.ioclass = kvm_register_read(vcpu, VCPU_REGS_R9) + 1;
            }
        }else{
            if(kvm_register_read(vcpu, VCPU_REGS_R10) == GUEST_APP_AIO_MAGIC){
                nu->u.rw.ioclass = kvm_register_read(vcpu, VCPU_REGS_R8) + 1;
            }
        }

        nu->u.rw.ptr = kvm_register_read(vcpu, VCPU_REGS_RSI);
        nu->u.rw.size = kvm_register_read(vcpu, VCPU_REGS_RDX);

        if( ((int)nu->u.rw.size < 0) || ((int)nu->u.rw.size > 1048576) ){
            return ret;
        } 


        if(preadwrite == false){
            nu->u.rw.offset = get_offset_fd(vcpu->arch.cr3, nu->u.rw.fd);
        }else{
            nu->u.rw.offset = kvm_register_read(vcpu, VCPU_REGS_R10);
        }
        nu->u.rw.rw = is_write(nu->out_syscall);
        jprintk("syscall fd %d rw %s ptr %lx size %lu offset %lu ioclass %d \n", nu->u.rw.fd, nu->u.rw.rw?"WRITE":"READ", nu->u.rw.ptr, nu->u.rw.size, nu->u.rw.offset, nu->u.rw.ioclass-1);
#ifdef JADUKATA_CHECKSUM 
        unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
        unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
        lock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size,true);
        nu->u.rw.skip_eof = skip_eof(vcpu,nu);
        if(shortoff || (excess && (!nu->u.rw.skip_eof))){
            nu->u.rw.fkd.orgscall = nu->out_syscall;
            padded_checksum(vcpu,nu,&nu->u.rw.fkd);
        }else{
            //only for writes
            if(nu->u.rw.rw == 0x1){
                ret = jadukata_process_common(vcpu,prefix,nu,false,jadukata_process_read_write);
            }
        }
#endif
    }
    return ret;
}

int jadukata_sysret_read_write(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau* nu, bool preadwrite){
    int ret = (int)kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if(ret != -1){
#endif
        if(ret !=0){
            if((int)nu->u.rw.size > 0 && ((int)nu->u.rw.size <= 1048576)){
                nu->u.rw.act_size = ret;
                if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
#ifdef JADUKATA_CHECKSUM
                    if( (nu->u.rw.rw == 0x1) && ((nu->u.rw.size-nu->u.rw.act_size)>0) ){
                        //process undo
                        ret = jadukata_process_common(vcpu,prefix,nu,true,jadukata_process_read_write);
                    }
                    //only for reads
                    if(nu->u.rw.rw == 0x0){
                        ret = jadukata_process_common(vcpu,prefix,nu,false,jadukata_process_read_write);
                    }
                    unlock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size);
#endif
                    //update current offsets 
                    if(preadwrite == false){
                        increment_fd_offsets(vcpu->arch.cr3, nu->u.rw.fd, nu->u.rw.size);
                    }
                }
            }
        }
    }
    return 0;
}

int jadukata_syscall_read(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu,nu,false);
}

int jadukata_sysret_read(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu, nu,false);
}

int jadukata_syscall_write(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu, nu,false);
}

int jadukata_sysret_write(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu, nu,false);
}

int jadukata_sysret_open(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu);
void jadukata_syscall_open_complete(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    jadukata_sysret_open('f',vcpu,nu);
    fk->last = true;
}

void jadukata_fstat_sret(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    unsigned long fstataddr = fk->buffer;
    int pair[2], fsize, hash;
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if(ret == 0){
#endif
#ifdef BSD
        //first 4 bytes is device and second 4 bytes is inode
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fstataddr, pair , 8 ,&ex);
        //8 bytes at offset 72 is file size in bytes
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fstataddr+72, &fsize , 8 ,&ex);
#else
        //first 8 bytes is device and second 8 bytes is inode
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fstataddr, pair , 16 ,&ex);
        //8 bytes at offset 48 is file size in bytes
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fstataddr+48, &fsize , 8 ,&ex);
#endif
        nu->u.op.hash = get_new_file_hash(pair[0],pair[1],fsize); 
        jprintk("syscall open fd %d device %u inode %u size %lu hash %lx \n", nu->u.op.fd, pair[0], pair[1], fsize, nu->u.op.hash);
    }else{
        nu->u.op.hash = 0; 
        jprinte("ERROR return value is -1 for fstat with fd %d \n", nu->u.op.fd);
    }
    kvm_register_write(vcpu, VCPU_REGS_RAX, nu->u.op.fd);
    jadukata_syscall_open_complete(vcpu,nu,fk);
    free_process_buffer(vcpu,nu,fk);
}

void jadukata_fstat_scall(struct kvm_vcpu *vcpu, struct nitrodatau* nu, struct fakescall *fk){
    alloc_process_buffer_secondhalf(vcpu,nu,fk);
    jadukata_fake_syscall(vcpu, nu, fk, jadukata_fstat_sret, check_result_minus_one, (unsigned long)FSTAT_SCALL_NR, 2, (unsigned long)nu->u.op.fd, (unsigned long)fk->buffer);
}

//possible starting point of fake scalls for fstat
void jadukata_fstat(struct kvm_vcpu *vcpu, struct nitrodatau* nu, struct fakescall *fk){
    nu->u.op.fd = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if((int)nu->u.op.fd >= 0){
#endif
        short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu,fk);
        if(mmap_needed){
            //mmap anonymous shared to allocate buffer space
            jadukata_fake_syscall(vcpu, nu, fk, jadukata_fstat_scall, check_result_minus_one, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, MMAP_SIZE, 3UL, MMAP_FLAGS, (unsigned long)-1, 0UL);
        }else{
            jadukata_fstat_scall(vcpu,nu,fk);
        }
    }else{
        jadukata_syscall_open_complete(vcpu,nu,fk);
    }
}


#define STRLEN(s) ((sizeof(s)/sizeof(s[0]))-1)
#define PATH_PATTERN "/mnt/disk"
//#define PATH_PATTERN ""
#define MAX_PATH_LEN (512)

void jadukata_path_match_helper(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk, bool match){
    chainfntype fn = NULL;
    if(match){
        nu->u.op.match = 1;
        fn = jadukata_fstat;
    }else{
        fn = jadukata_syscall_open_complete;
    }
    jadukata_fake_save_org(vcpu,nu,fk);
    jadukata_fake_syscall(vcpu, nu, fk, fn, check_result_minus_one, fk->orgscall, 0);
}

void jadukata_path_match(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk, char* path){
    //TODO: canonical paths with single slashes etc
    jadukata_path_match_helper(vcpu,nu,fk,(strncmp(path, PATH_PATTERN, STRLEN(PATH_PATTERN)) == 0));
}

void jadukata_make_abs_path(struct kvm_vcpu *vcpu, struct nitrodatau * nu, struct fakescall *fk){
    struct x86_exception ex;
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    char path[MAX_PATH_LEN];
    unsigned long len = strlen(pd->cwd);
    memcpy(path,pd->cwd,len);
    path[len]='/';
    len++;
    path[len] = '\0';
    //reread the file path and append to dir
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.op.pathaddr, path+len, (MAX_PATH_LEN-len), &ex);
    jprintk("syscall open absolute path %s \n", path);
    jadukata_path_match(vcpu,nu,fk,path); 
}

void jadukata_getcwd_sret(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    struct process_data *pd = get_process_data(vcpu->arch.cr3);
    unsigned long len = kvm_register_read(vcpu,VCPU_REGS_RAX);
    unsigned long pathaddr = fk->buffer;
    if(pathaddr != (unsigned long)NULL){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,pathaddr, pd->cwd, len+1 ,&ex);
        jprintk("syscall open cwd path %s \n", pd->cwd);
    }else{
        //error
        pd->cwd[0] = 0;
    }
    free_process_buffer(vcpu,nu,fk);

    jadukata_fake_restore_org(vcpu,nu,fk);
    
    //make abs path called after restore org to contiue with fstat fake syscall
    jadukata_make_abs_path(vcpu,nu,fk);

    //reset cwd to force getcwd on next open
    //continuous tracking of cwd is not done now
    pd->cwd[0] = 0;
}

void jadukata_getcwd_scall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    alloc_process_buffer_secondhalf(vcpu,nu,fk);
    jadukata_fake_syscall(vcpu, nu, fk, jadukata_getcwd_sret, check_result_zero, (unsigned long)GETCWD_SCALL_NR, 2, (unsigned long)fk->buffer, MMAP_SIZE);
}

//starting point of fake scalls for getcwd
int jadukata_getcwd(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    jadukata_fake_save_org(vcpu,nu,fk);
    short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu,fk);
    if(mmap_needed){
        //mmap anonymous shared to allocate buffer space
        jadukata_fake_syscall(vcpu, nu, fk, jadukata_getcwd_scall, check_result_minus_one, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, MMAP_SIZE, 3UL, MMAP_FLAGS, (unsigned long)-1, 0UL);
    }else{
        jadukata_getcwd_scall(vcpu,nu,fk);
    }
    return 0;
}

void jadukata_open_helper(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, char *path){
#ifdef SIMPLE_FHASH
    if(path[0] == '/'){
        if(strncmp(path, PATH_PATTERN,STRLEN(PATH_PATTERN)) == 0){
            nu->u.op.match = 1;
        }
    }else{
        nu->u.op.match = 1;
    }
    nu->u.op.hash = FNVHash(path,strlen(path),0);
#else
    //during process bootstrap path read is empty in some cases
    if(path[0] != '/' && path[0] != '\0'){
        //if relative path, get cwd and convert to absolute before cheking
        struct process_data *pd = get_process_data(vcpu->arch.cr3);
        if(pd->cwd[0] == 0){
            jadukata_getcwd(vcpu,nu,&nu->u.op.fkd);
        }else{
            jadukata_make_abs_path(vcpu,nu,&nu->u.op.fkd); 
        }
    }else{
        jadukata_path_match(vcpu,nu,&nu->u.op.fkd,path); 
    }
#endif
}


#define JADU_RLIMIT_STACK (3UL)

int jadukata_syscall_setrlimit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    if(kvm_register_read(vcpu, VCPU_REGS_RDI) == JADU_RLIMIT_STACK){
        nu->u.sr.limit = kvm_register_read(vcpu, VCPU_REGS_RSI);
    }
    return 0;
}

int jadukata_sysret_setrlimit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if((kvm_register_read(vcpu, VCPU_REGS_RAX) == 0)){
#endif
        if((nu->u.sr.limit != 0)){
            hash_table *ht_threads = NULL;
            extern hash_table *ht_mon_pids;
            if(ht_lookup_val(ht_mon_pids,vcpu->arch.cr3,(unsigned long*)&ht_threads) != 0){
                unsigned long stackval = 0;
                if(ht_lookup_val(ht_threads, nu->id, &stackval) != 0){
                    unsigned long newstackval = (stackval & ~THREADSTACKMASK) | (nu->u.sr.limit/STACK_SIZE_FACTOR);
                    ht_update_val(ht_threads, nu->id, newstackval);
                    jprintk("updated stackval for pid %d from old %lx to new %lx \n", nu->id, stackval, newstackval);
                }

            }
        }
    }
    return 0;
}
    
int jadukata_syscall_open(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct x86_exception ex;
    char path[MAX_PATH_LEN];
    nu->u.op.pathaddr = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.op.flags = kvm_register_read(vcpu, VCPU_REGS_RSI);
    
    //if writeonly, then make it read write
    //O_RDONLY 0x0000 , O_WRONLY 0x0001, O_RDWR 0x0002       
    if((nu->u.op.flags%2) == 1){
        nu->u.op.flags += 1; 
    }
    kvm_register_write(vcpu, VCPU_REGS_RSI, nu->u.op.flags);
    
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,nu->u.op.pathaddr, path, MAX_PATH_LEN,&ex);
    jprintk("syscall open path %s \n", path);
    jadukata_open_helper(prefix, vcpu, nu, path);
    return 0;
}

// always called from fake chain during open
int jadukata_sysret_open(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    //struct fakescall *fk = &nu->u.op.fkd; 
    int fd = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintk("sysret open match %d fd %d hash %lx \n", nu->u.op.match, fd, nu->u.op.hash);
    if(nu->u.op.match == 1){
#ifdef BSD
        if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
        if(fd >= 0){
#endif
            nu->u.op.fd = fd;
            if(nu->u.op.hash != 0){
                add_monitored_fd(vcpu->arch.cr3,nu->u.op.fd,nu->u.op.hash);
                //O_CREAT
                if(nu->u.op.flags & 512){
                    ht_update_val(ht_file_eof_guess,nu->u.op.hash,0);
                }
            }else{
                jprinte("ERROR hash is 0 for fd %d \n", nu->u.op.fd);
            }
        }
        //nu->u.op.match = 0;
    }
    return 0;
}

int jadukata_syscall_openat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct x86_exception ex;
    char path[MAX_PATH_LEN];
    int dirfd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.op.pathaddr = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.op.flags = kvm_register_read(vcpu, VCPU_REGS_RDX);
    
    //if writeonly, then make it read write
    //O_RDONLY 0x0000 , O_WRONLY 0x0001, O_RDWR 0x0002       
    if((nu->u.op.flags%2) == 1){
        nu->u.op.flags += 1; 
    }
    kvm_register_write(vcpu, VCPU_REGS_RDX, nu->u.op.flags);

    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,nu->u.op.pathaddr, path, MAX_PATH_LEN,&ex);
    jprintl("syscall openat dirfd %d  path %s \n",dirfd, path);
    //openat being used as open due to absolute path
#ifdef SIMPLE_FHASH
    if(path[0] == '/'){
        if(strncmp(path, PATH_PATTERN,STRLEN(PATH_PATTERN)) == 0){
            nu->u.op.match = 1;
        }
    }else{
        // if dirfd is special value AT_FDCWD (-100) , then treat with cwd just as open
        if(dirfd == -100){
            nu->u.op.match = 1;
        }else{
            nu->u.op.match = is_monitored_fd(vcpu->arch.cr3,dirfd);
        }
    }
    nu->u.op.hash = FNVHash(path,strlen(path),0);
#else
    // if dirfd is special value AT_FDCWD (-100) , then treat with cwd just as open
    if(path[0] == '/' || dirfd == -100){
        jadukata_open_helper(prefix, vcpu, nu, path);
    }else{
        //check if the dirfd is monitored
        jadukata_path_match_helper(vcpu,nu,&nu->u.op.fkd,is_monitored_fd(vcpu->arch.cr3,dirfd));
    }
#endif
    return 0;
}

int jadukata_sysret_openat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    return jadukata_sysret_open(prefix,vcpu,nu);
}

int jadukata_syscall_close(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.cl.close_fd = kvm_register_read(vcpu, VCPU_REGS_RDI);

    return 0;
}

int jadukata_sysret_close(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL))
#else
    if(ret == 0)
#endif
    {
        if(nu->u.cl.close_fd >= 0){
            if(is_monitored_fd(vcpu->arch.cr3, nu->u.cl.close_fd)){ 
                extern hash_table *ht_file_sizes;
                unsigned long hash = get_filehash_fd(vcpu->arch.cr3,nu->u.cl.close_fd);
                ht_remove(ht_file_eof_guess,hash);
                del_monitored_fd(vcpu->arch.cr3,nu->u.cl.close_fd);
                ht_remove(ht_file_sizes,hash);
            }

            jprintl("syscall close fd %d \n", nu->u.cl.close_fd);
        }
    }
    return 0;
}
    
int jadukata_syscall_unlink(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BLOCK_LIFETIME
    struct x86_exception ex;
    char path[MAX_PATH_LEN];
    nu->u.op.pathaddr = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.op.flags = kvm_register_read(vcpu, VCPU_REGS_RSI);
    
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,nu->u.op.pathaddr, path, MAX_PATH_LEN,&ex);
    jprintk("syscall unlink path %s \n", path);
    jadukata_open_helper(prefix, vcpu, nu, path);
#endif
    return 0;
}

// always called from fake chain during open
int jadukata_sysret_unlink(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BLOCK_LIFETIME
    //struct fakescall *fk = &nu->u.op.fkd; 
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintk("sysret unlink match %d ret %d hash %lx \n", nu->u.op.match, ret, nu->u.op.hash);
    if(nu->u.op.match == 1){
#ifdef BSD
        if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
        if(ret == 0){
#endif
            if(nu->u.op.hash != 0){
                unsigned long del_hash, oldval;
                ht_open_scan(ht_blocklifetime);
                while( ht_scan_val(ht_blocklifetime,&del_hash,&oldval) != -1 ){
                    if( UNPACK_INTS_FROM_LONG_X(del_hash) == nu->u.op.hash ){
                        add_to_series(ht_series_blocklifetime,jiffies_to_msecs(get_jiffies() - jsecs_to_jiffies(oldval)),BLOCK_LIFETIME_SERIES_UNIT_MSECS);
                        htn_remove(ht_blocklifetime,del_hash);
                    }
                }
                ht_close_scan(ht_blocklifetime);
            }else{
                jprinte("ERROR hash is 0 for fd %d \n", nu->u.op.fd);
            }
        }
        //nu->u.op.match = 0;
    }
#endif
    return 0;
}

int jadukata_syscall_unlinkat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BLOCK_LIFETIME
    struct x86_exception ex;
    char path[MAX_PATH_LEN];
    int dirfd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.op.pathaddr = kvm_register_read(vcpu, VCPU_REGS_RSI);
    
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,nu->u.op.pathaddr, path, MAX_PATH_LEN,&ex);
    jprintl("syscall unlinkat dirfd %d  path %s \n",dirfd, path);
    //openat being used as open due to absolute path
#ifdef SIMPLE_FHASH
    if(path[0] == '/'){
        if(strncmp(path, PATH_PATTERN,STRLEN(PATH_PATTERN)) == 0){
            nu->u.op.match = 1;
        }
    }else{
        // if dirfd is special value AT_FDCWD (-100) , then treat with cwd just as open
        if(dirfd == -100){
            nu->u.op.match = 1;
        }else{
            nu->u.op.match = is_monitored_fd(vcpu->arch.cr3,dirfd);
        }
    }
    nu->u.op.hash = FNVHash(path,strlen(path),0);
#else
    // if dirfd is special value AT_FDCWD (-100) , then treat with cwd just as open
    if(path[0] == '/' || dirfd == -100){
        jadukata_open_helper(prefix, vcpu, nu, path);
    }else{
        //check if the dirfd is monitored
        jadukata_path_match_helper(vcpu,nu,&nu->u.op.fkd,is_monitored_fd(vcpu->arch.cr3,dirfd));
    }
#endif
#endif
    return 0;
}

int jadukata_sysret_unlinkat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BLOCK_LIFETIME
    return jadukata_sysret_unlink(prefix,vcpu,nu);
#else
    return 0;
#endif
}

int jadukata_syscall_truncate(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BLOCK_LIFETIME
    struct x86_exception ex;
    char path[MAX_PATH_LEN];
    nu->u.op.pathaddr = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.op.size = kvm_register_read(vcpu, VCPU_REGS_RSI);
    
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,nu->u.op.pathaddr, path, MAX_PATH_LEN,&ex);
    jprintk("syscall truncate path %s \n", path);
    jadukata_open_helper(prefix, vcpu, nu, path);
#endif
    return 0;
}

// always called from fake chain during open
int jadukata_sysret_truncate(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
#ifdef BLOCK_LIFETIME
    //struct fakescall *fk = &nu->u.op.fkd; 
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintk("sysret truncate match %d ret %d hash %lx \n", nu->u.op.match, ret, nu->u.op.hash);
    if(nu->u.op.match == 1){
#ifdef BSD
        if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
        if(ret == 0){
#endif
            if(nu->u.op.hash != 0){
                unsigned long del_hash, oldval;
                ht_open_scan(ht_blocklifetime);
                while( ht_scan_val(ht_blocklifetime,&del_hash,&oldval) != -1 ){
                    if( UNPACK_INTS_FROM_LONG_X(del_hash) == nu->u.op.hash ){
                        if( UNPACK_INTS_FROM_LONG_Y(del_hash) > (nu->u.op.size/JADU_BLKSIZE)  ){
                            add_to_series(ht_series_blocklifetime,jiffies_to_msecs(get_jiffies() - jsecs_to_jiffies(oldval)),BLOCK_LIFETIME_SERIES_UNIT_MSECS);
                            htn_remove(ht_blocklifetime,del_hash);
                        }
                    }
                }
                ht_close_scan(ht_blocklifetime);
            }else{
                jprinte("ERROR hash is 0 for fd %d \n", nu->u.op.fd);
            }
        }
        //nu->u.op.match = 0;
    }
#endif
    return 0;
}

int jadukata_syscall_ftruncate(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifdef BLOCK_LIFETIME
    nu->u.cl.close_fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.cl.size = kvm_register_read(vcpu, VCPU_REGS_RSI);
#endif
    return 0;
}

int jadukata_sysret_ftruncate(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifdef BLOCK_LIFETIME
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL))
#else
    if(ret == 0)
#endif
    {
        if(nu->u.cl.close_fd >= 0){
            if(is_monitored_fd(vcpu->arch.cr3, nu->u.cl.close_fd)){ 
                unsigned long fhash = get_filehash_fd(vcpu->arch.cr3,nu->u.cl.close_fd);
                unsigned long del_hash,oldval;
                if(fhash != 0){
                    ht_open_scan(ht_blocklifetime);
                    while( ht_scan_val(ht_blocklifetime,&del_hash,&oldval) != -1 ){
                        if( UNPACK_INTS_FROM_LONG_X(del_hash) == fhash ){
                            if( UNPACK_INTS_FROM_LONG_Y(del_hash) > (nu->u.cl.size/JADU_BLKSIZE)  ){
                                add_to_series(ht_series_blocklifetime,jiffies_to_msecs(get_jiffies() - jsecs_to_jiffies(oldval)),BLOCK_LIFETIME_SERIES_UNIT_MSECS);
                                htn_remove(ht_blocklifetime,del_hash);
                            }
                        }
                    }
                    ht_close_scan(ht_blocklifetime);
                }else{
                    jprinte("ERROR hash is 0 for fd %d \n", nu->u.cl.close_fd);
                }
            }

            jprintl("syscall ftruncate fd %d \n", nu->u.cl.close_fd);
        }
    }
#endif
    return 0;
}
    

int jadukata_syscall_stat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_stat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_fstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_lstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_lstat(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_lseek(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.ls.fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    return 0;
}

int jadukata_sysret_lseek(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.ls.fd)){
        int offset = kvm_register_read(vcpu, VCPU_REGS_RAX);
        if(offset != -1){
            update_fd_offsets(vcpu->arch.cr3,nu->u.ls.fd,offset);
        }
    }
    return 0;
}

int print_mmap(struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    struct x86_exception ex;
	unsigned long gpa = kvm_mmu_gva_to_gpa_system(vcpu, (gva_t)nu->u.mm.mmap_ptr, &ex);
    jprintk("syscall io_mmap gva cr3 %lx ptr %lx len %lx gpa %lx \n", vcpu->arch.cr3,nu->u.mm.mmap_ptr, nu->u.mm.mmap_len, gpa);
    return 0;
}

int jadukata_syscall_mmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifndef BSD
#ifdef MMAP_SUPPORT
    nu->u.mm.mmap_len = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.mm.mmap_fd = kvm_register_read(vcpu, VCPU_REGS_R8);
    jprintk("syscall mmap len %lu fd %d \n", nu->u.mm.mmap_len, nu->u.mm.mmap_fd);
#endif
#endif
    return 0;
}

int jadukata_sysret_mmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifndef BSD
#ifdef MMAP_SUPPORT
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.mm.mmap_fd)){
        nu->u.mm.mmap_ptr = (void*)kvm_register_read(vcpu, VCPU_REGS_RAX);
        if(nu->u.mm.mmap_ptr != ((void*)-1)){
            //for mmap code is positive
            monitor_virt_range(vcpu,(unsigned long)nu->u.mm.mmap_ptr,nu->u.mm.mmap_len,MMAP_GPA);
        }
    }
    jprintk("sysret mmap ptr %lx len %lu fd %d \n", nu->u.mm.mmap_ptr, nu->u.mm.mmap_len, nu->u.mm.mmap_fd);
    nu->u.mm.mmap_ptr = 0;
    nu->u.mm.mmap_len = 0;
    nu->u.mm.mmap_fd = 0;
#endif
#endif
    return 0;
}

int jadukata_syscall_munmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifndef BSD
#ifdef MMAP_SUPPORT
    nu->u.um.munmap_ptr = (void*)kvm_register_read(vcpu, VCPU_REGS_RDI);
    jprintk("syscall munmap ptr %lx \n", nu->u.um.munmap_ptr);
#endif
#endif
    return 0;
}

int jadukata_sysret_munmap(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifndef BSD
#ifdef MMAP_SUPPORT
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(ret == 0){
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        unsigned long vaddrs = (unsigned long)nu->u.um.munmap_ptr;
        unsigned long vaddre = vaddrs;
        while(ht_lookup(m->v2p,vaddre) != 0){
            vaddre += PAGE_SIZE;
        }
        jprintk("sysret munmap ptr ret %d vaddrs %lx vaddre %lx \n", ret, vaddrs,vaddre);
        unmonitor_virt_range(vcpu,vaddrs,(vaddre-vaddrs),MMAP_GPA);
    }
    nu->u.um.munmap_ptr = 0;
#endif
#endif
    return 0;
}

int jadukata_syscall_pread64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu,nu,true);
}

int jadukata_sysret_pread64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu,nu,true);
}

int jadukata_syscall_pwrite64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_read_write(prefix, vcpu,nu,true);
}

int jadukata_sysret_pwrite64(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_read_write(prefix, vcpu,nu,true);
}

#ifdef JADUKATA_CHECKSUM 
int jadukata_process_readv_writev(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu,bool failed, struct pair *pairs, int* numpairs, int maxpairs){
    int rc = 0, i;
    unsigned long ptr;
    struct x86_exception ex;
    unsigned long addr;
    unsigned long size;
    
    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);

    ptr = (unsigned long)nu->u.rw.iov_ptr;

    for(i=0;i<nu->u.rw.iov_cnt;i++){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, ptr, &addr, sizeof(void*),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, ptr+8, &size, sizeof(unsigned long),&ex);
        /*
        if(unlikely(nu->u.rw.skip_eof && (i==0))){
            rc |= jadukata_translate(vcpu, addr+(shortoff?(JADU_BLKSIZE-shortoff):0), size-(shortoff?(JADU_BLKSIZE-shortoff):0), pairs, numpairs, maxpairs);
        }else if(unlikely(nu->u.rw.skip_eof && (i==(nu->u.rw.iov_cnt-1)))){
            rc |= jadukata_translate(vcpu, addr, size-excess, pairs, numpairs, maxpairs);
        }else{
            */
            rc |= jadukata_translate(vcpu, addr, size, pairs, numpairs, maxpairs);
        /*
         }
         */
        ptr += sizeof(struct iovec);
    }

    return rc;
}
#endif

int jadukata_syscall_readv_writev_helper(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    int i,ret =0;
#ifdef JADUKATA_CHECKSUM 
    unsigned long ptr;
    struct x86_exception ex;
    unsigned long addr;
    unsigned long size;

    ptr = (unsigned long)nu->u.rw.iov_ptr;
    nu->u.rw.size = 0; 
    
    for(i=0;i<nu->u.rw.iov_cnt;i++){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)ptr, &addr, sizeof(void*),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)(ptr+8), &size, sizeof(unsigned long),&ex);
        jprintk("iov %d ptr %lx addr %lx size %lu \n", i,ptr,addr,size);
        ptr += 16;
        nu->u.rw.size += size;
        lock_virt_buffer(vcpu,addr,size,true);
    }

    unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
    unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);

    nu->u.rw.skip_eof = skip_eof(vcpu,nu);

    jprintk("syscall readv writev helper size %d excess %lu shortoff %lu skipfullchecksums %d\n", nu->u.rw.size, excess, nu->u.rw.skip_eof);

    if(shortoff || (excess && (!nu->u.rw.skip_eof))){
        nu->u.rw.fkd.orgscall = nu->out_syscall;
        padded_checksum(vcpu,nu,&nu->u.rw.fkd);
    }else{
        //only for writes
        if(nu->u.rw.rw == 0x1){
            ret = jadukata_process_common(vcpu,prefix,nu,false, jadukata_process_readv_writev);
        }
    }
#endif
    return ret;
}

int jadukata_syscall_readv_writev(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadvwritev){
    int ret = 0;
    nu->u.rw.rw = is_write(nu->out_syscall);
    nu->u.rw.fd = (int)kvm_register_read(vcpu, VCPU_REGS_RDI);
    if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
        nu->u.rw.iov_ptr = (unsigned long)kvm_register_read(vcpu, VCPU_REGS_RSI);
        nu->u.rw.iov_cnt = (unsigned long)kvm_register_read(vcpu, VCPU_REGS_RDX);
        if(preadvwritev == false){
            nu->u.rw.offset = get_offset_fd(vcpu->arch.cr3, nu->u.rw.fd);
        }else{
            nu->u.rw.offset = (unsigned long)kvm_register_read(vcpu, VCPU_REGS_R10);
        }
        ret = jadukata_syscall_readv_writev_helper(prefix,vcpu,nu);
    }
    return ret;
}

int jadukata_sysret_readv_writev(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu, bool preadvwritev){
    unsigned long addr;
    unsigned long size;
    int i;
    struct x86_exception ex;

    int ret = (int)kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if(ret != -1){
#endif
        if(ret != 0){
            if(is_monitored_fd(vcpu->arch.cr3,nu->u.rw.fd)){
                char* ptr = (char*)nu->u.rw.iov_ptr;
#ifdef JADUKATA_CHECKSUM
                if( (nu->u.rw.rw == 0x1) && ((nu->u.rw.size-ret)>0) ){
                    //process undo
                    ret = jadukata_process_common(vcpu,prefix,nu,true, jadukata_process_readv_writev);
                }
                //only for reads
                if(nu->u.rw.rw == 0x0){
                    ret = jadukata_process_common(vcpu,prefix,nu,false, jadukata_process_readv_writev);
                }
                for(i=0;i<nu->u.rw.iov_cnt;i++){
                    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)ptr, &addr, sizeof(void*),&ex);
                    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, (gva_t)(ptr+8), &size, sizeof(unsigned long),&ex);
                    unlock_virt_buffer(vcpu,addr,size);
                    ptr += sizeof(struct iovec);
                }
#endif
                if(preadvwritev == false){
                    //update current offsets 
                    increment_fd_offsets(vcpu->arch.cr3, nu->u.rw.fd, nu->u.rw.size);
                }
            }
        }
    }
    return ret;
}

int jadukata_syscall_readv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_sysret_readv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_syscall_writev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_sysret_writev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,false);
}

int jadukata_syscall_msync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_msync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_madvise(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_madvise(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

extern spinlock_t newprocess_lock;
extern hash_table *ht_new_pids;
extern hash_table *ht_new_pids_fdinherit;
extern hash_table *ht_mon_process;

extern atomic_t in_new_pid_mode;

bool new_process = true;
void new_process_helper(struct kvm_vcpu* vcpu, int sharedvm, int vfork, int precisestackbase, int pid, unsigned long stack, unsigned long stacksize){
        //if(pid != -1 && pid != 0){
        if(pid > 0){
            jprintl("new child process created with pid %d parent cr3 %lx sharedvm %d vfork %d new_process %d stack %lx stacksize %ld\n", pid, vcpu->arch.cr3, sharedvm, vfork, new_process,stack, stacksize);
            if(new_process){
                if(sharedvm == 0){
                    if(ht_get_size(ht_new_pids) >0){
                        del_cr3_to_pid_pid(pid);
                    }else{
                        clear_all_cr3_to_pid();
                    }
                    
                    unsigned long temp;
                    while(down_interruptible(&forklock) != 0){
                    };
                    temp = targeted_process_pid_to_cr3(pid);
                    if(temp == 0){
                        ht_add_val(ht_new_pids,pid,vcpu->arch.cr3);
                    }
                    up(&forklock);
                    clear_unmonitored_cr3_to_pid();
                }else{
                    add_targeted_process(vcpu->arch.cr3,vcpu->arch.cr3,pid,stack,stacksize,vfork,precisestackbase);
                }
            }
        }else{
            jprinte("ERROR failed new child process creation pid %d parent cr3 %lx sharedvm %d \n", pid, vcpu->arch.cr3, sharedvm);
        }
}

// TODO: O_CLOEXEC and inheriting fds from parent.
// all three versions of dup now just use single set of interceptors
int jadukata_syscall_dup(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.dp.old_fd = kvm_register_read(vcpu, VCPU_REGS_RDI);
    return 0;
}

int jadukata_sysret_dup(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = (int)kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if(ret >= 0){
#endif
        if( (ret != nu->u.dp.old_fd) && is_monitored_fd(vcpu->arch.cr3,nu->u.dp.old_fd)){
            jprintl("duped %d into %d \n", nu->u.dp.old_fd, ret);
            add_monitored_fd(vcpu->arch.cr3, ret, get_filehash_fd(vcpu->arch.cr3,nu->u.dp.old_fd));
        }
    }
    return 0;
}

#ifndef BSD
int jadukata_syscall_fork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu);
int jadukata_sysret_fork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu);
int jadukata_sysret_clone(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu);

void jadukata_syscall_clone_complete(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    freebuffer_lastfake(vcpu,nu,fk);
    jadukata_sysret_clone('l',vcpu,nu);
}

extern hash_table *ht_mmap_process;
extern hash_table *ht_mon_pids;
void adjust_stack_size(struct kvm_vcpu* vcpu, struct nitrodatau *nu){
    struct process_data* m = NULL;
    unsigned long cr3 = vcpu->arch.cr3;
    unsigned long rsp, nextclosest = ULONG_MAX, prevclosest = ULONG_MAX, prevclosest_pid = 0;
    unsigned long pid,stack, stackval, stacksize;
    bool updated;
    hash_table *ht_threads = NULL;
    rsp = nu->u.cn.stack_base;
    read_lock(&mon_lock);
    if(ht_lookup_val(ht_mmap_process,cr3,(unsigned long*)&m) != 0){
        if(m->pid != 0){
            if(ht_lookup_val(ht_mon_pids,cr3,(unsigned long*)&ht_threads) != 0){
                ht_open_scan(ht_threads);
                while(ht_scan_val(ht_threads, &pid, &stackval) != -1){
                    updated = false;
                    stacksize = ((stackval & (THREADSTACKMASK)))*(STACK_SIZE_FACTOR);
                    stack = (stackval & (~THREADSTACKMASK)); 
                    if(stacksize ==0){
                        jprinte("ERROR stacksize is zero\n");
                    }
                    if( (rsp > stack) && ((rsp-stack) < (rsp-nextclosest)) ){
                        nextclosest = stack;
                        updated = true;
                    }
                    if( (rsp < stack) && (rsp > (stack + stacksize)) ){
                        prevclosest = stack;
                        prevclosest_pid = pid;
                        updated = true;
                    }
                    jprintk("%s closest, thread pid %lu stackbase %lx rsp %lx stacksize %lu stackval %lx prevclosest %lx nextclosest %lx \n", updated?"updated":"not updated",pid, stack, rsp, stacksize, stackval, prevclosest, nextclosest);
                }
                ht_close_scan(ht_threads);
            }
        }
    }
    read_unlock(&mon_lock);
    if(nextclosest != ULONG_MAX){
        updated = false;
        unsigned long tempsize = rsp-nextclosest;
        if(tempsize < nu->u.cn.stack_size){
            updated = true;
        }
        jprintk("%s stack base %lx stack size : old value %lu new value %lu \n", updated?"adjusting":"not adjusting",rsp, nu->u.cn.stack_size, tempsize);
        if(updated){
            nu->u.cn.stack_size = tempsize;
        }
    }
    if(prevclosest != ULONG_MAX){
        updated = false;        
        if(ht_lookup_val(ht_threads, prevclosest_pid, &stackval) == 0){
            jprinte("ERROR htlookup failed for pid %lu \n", prevclosest_pid);
        }else{
            stacksize = ((stackval & (THREADSTACKMASK)))*(STACK_SIZE_FACTOR);
            stack = (stackval & (~THREADSTACKMASK)); 
            unsigned long tempsize = prevclosest - rsp;
            if(tempsize < stacksize){
                updated = true;
            }
            jprintk("%s prevclosest stack base %lx stack size : old value %lu new value %lu \n", updated?"adjusting":"not adjusting",stack, stacksize, tempsize);
            if(updated){
                stackval = (stack | (tempsize/(STACK_SIZE_FACTOR)));
                ht_update_val(ht_threads, prevclosest_pid, stackval);
            }
        } 
    }
}

void jadukata_syscall_clone_rlimit_helper(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer, &nu->u.cn.stack_size, sizeof(unsigned long) ,&ex);
    
    //rlimit gives the max limit , the actual size is managed by pthread
    adjust_stack_size(vcpu,nu);

    jadukata_fake_restore_org(vcpu,nu,fk);
    jadukata_fake_syscall(vcpu, nu, fk, jadukata_syscall_clone_complete, check_result_minus_one, fk->orgscall, 0);
}

void jadukata_syscall_clone_helper(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    alloc_process_buffer_secondhalf(vcpu,nu,fk);
    //JADU_RLIMIT_STACK is 3
    jadukata_fake_syscall(vcpu, nu, fk,jadukata_syscall_clone_rlimit_helper, check_result_minus_one,(unsigned long)GETRLIMIT_SCALL_NR, 2, JADU_RLIMIT_STACK, fk->buffer);
}

int jadukata_syscall_clone(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    unsigned long flags = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.cn.sharedvm = 0;
    if(flags & CLONE_VM){
        nu->u.cn.stack_base = kvm_register_read(vcpu, VCPU_REGS_RSI);
        nu->u.cn.ptid = kvm_register_read(vcpu, VCPU_REGS_RDX);
        nu->u.cn.ctid = kvm_register_read(vcpu, VCPU_REGS_R10);
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        nu->u.cn.sharedvm = 1;
        m->cloning = true;
        jadukata_fake_save_org(vcpu,nu,&nu->u.cn.fkd);
        short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu,&nu->u.cn.fkd);
        if(mmap_needed){
            //mmap anonymous shared to allocate buffer space
            jadukata_fake_syscall(vcpu, nu, &nu->u.cn.fkd, jadukata_syscall_clone_helper, check_result_minus_one, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, MMAP_SIZE, 3UL, MMAP_FLAGS, (unsigned long)-1, 0UL);
        }else{
            jadukata_syscall_clone_helper(vcpu,nu,&nu->u.cn.fkd);
        }
    }else{
        jprintl("syscall clone as fork flags %lx\n", flags);
        jadukata_syscall_fork(prefix,vcpu,nu);
    }
    return 0;
}

int jadukata_sysret_clone(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int pid = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(nu->u.cn.sharedvm){
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        if(pid != -1){
            //for linux no adding of stack size to the stack addr passed with clone syscall is needed
            unsigned long eff_stack_base = nu->u.cn.stack_base; 
            jprintl("new thread created pid %d stack base %lx stack size %ld effective stack base %lx\n", pid, nu->u.cn.stack_base, nu->u.cn.stack_size, eff_stack_base);
            new_process_helper(vcpu, 1, 0, 1, pid, eff_stack_base,nu->u.cn.stack_size);
        }else{
            jprintl("new thread creation failed ret %d stack base %lx stack size %ld\n", pid, nu->u.cn.stack_base, nu->u.cn.stack_size);
        }
        m->cloning = false;
    }else{
        jprintl("syret clone as fork pid %d\n", pid);
        jadukata_sysret_fork(prefix,vcpu,nu);
    }
    return 0;
}
#endif

int jadukata_syscall_fork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    unsigned long val=0;
    unsigned long pid = nu->id;
    unsigned long minpid = nu->id;
    unsigned long tpid;
            
    hash_table *ht_threads = NULL;
    extern hash_table *ht_mon_pids;
    read_lock(&mon_lock);
    if(ht_lookup_val(ht_mon_pids,vcpu->arch.cr3,(unsigned long*)&ht_threads) != 0){
        ht_open_scan(ht_threads);
        unsigned long stackval;
        while(ht_scan_val(ht_threads, &tpid, &stackval) != -1){
            if(tpid < minpid){
                minpid = tpid;
            }
        }
        ht_close_scan(ht_threads);
    }
    read_unlock(&mon_lock);
    
    jprintk("minpid is %d pid is %d \n", minpid, pid);

    pid = minpid;
    
    while(down_interruptible(&forklock) != 0){
    }
    if(ht_remove_val(ht_fork_parent_pids,pid,(unsigned long*)&val) !=-1 ){
        if(val ==0){
            jprinte("ERROR: fork parent pids present val is zero for pid %d \n",pid);
        }
        val++;
        if(val){
            ht_add_val(ht_fork_parent_pids,pid,val);
        }
        up(&forklock);
        jprintl("fork adding parent_pids pid %ld val %ld cr3 %lx \n", pid, val, vcpu->arch.cr3);
    }else{
        val = 1;
        jprintl("fork initializing parent_pids pid %ld val %ld cr3 %lx \n", pid, val, vcpu->arch.cr3);
        ht_add_val(ht_fork_parent_pids,pid,val);
        up(&forklock);
    }
    return 0;
}

int jadukata_sysret_fork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL))
#endif
    {
        int pid = kvm_register_read(vcpu, VCPU_REGS_RAX);
        new_process_helper(vcpu,0,0,0,pid,0,DEFAULT_STACK_SIZE);
    }
    return 0;
}

int jadukata_syscall_vfork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    //m->cloning = true;
    nu->u.cn.stack_base = kvm_register_read(vcpu, VCPU_REGS_RSP);
    return 0;
}

void jadukata_vfork_complete(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long pid = kvm_register_read(vcpu,VCPU_REGS_RAX);
    jprintl("vfork helper getpid sysret cr3 %lx pid %d stack %lx \n", vcpu->arch.cr3, pid, nu->u.cn.stack_base);
    
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    m->expect_iret_handling = pid; 
    //m->cloning = false;
  
    new_process_helper(vcpu, 1, 1, 0, pid, nu->u.cn.stack_base,DEFAULT_STACK_SIZE);
    
    // undo the effects of syscall during the sysret path 
    jadukata_fake_restore_org(vcpu,nu,fk);
    
    fk->last = true;
}

int jadukata_sysret_vfork(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    unsigned long pid = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
        if(pid == 0){
            jadukata_fake_save_org(vcpu,nu, &nu->u.cn.fkd);
            jadukata_fake_syscall(vcpu, nu, &nu->u.cn.fkd, jadukata_vfork_complete, check_result_negative, (unsigned long)GETPID_SCALL_NR,0);
            //redo syscall 
            vcpu->arch.emulate_ctxt._eip-=2;
            jprintl("fake sysret changing rip to %lx \n",vcpu->arch.emulate_ctxt._eip);

        }
    }
    return 0;
}

int jadukata_syscall_rfork(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_rfork(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

extern hash_table *ht_exec_cr3s;
extern hash_table *ht_new_process_tracking;
int jadukata_syscall_execve(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    //Linux
    //execve recreates the page table so remove old cr3 and add pid as new
    //but use the ht_exec_cr3 to identify exec cr3s so to not ignore the first syscall as artificial
    //BSD
    //execve uses the same page table, so use ht_exec_cr3 for bsd purposes
    bool found = false;
    unsigned long fd,hash;
    unsigned long childcr3 = 0, parentcr3 = 0;
    unsigned long pid = nu->id;
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    
#ifdef BSD 
    int vfork = (m->expect_iret_handling == nu->id); 
#endif

    nu->u.ex.pid = nu->id;
    
    ht_open_scan(ht_mon_process);
    while(ht_scan_val(ht_mon_process, &childcr3,&parentcr3) != -1){
        if(childcr3 == vcpu->arch.cr3){
            found = true;
            break;        
        }
    }
    ht_close_scan(ht_mon_process);
    
    if(found == false) jprinte("ERROR unable to find the parentcr3 for cr3 %lx \n", vcpu->arch.cr3);
                
    //inherited monitored fds
    hash_table* mfds;
    ht_create_with_size(&mfds,"mfds",10);
    ht_open_scan(m->mfds);
    while(ht_scan_val(m->mfds, &fd,&hash) != -1){
        jprintl("inheriting monitored fd pid %lu cr3 %lx fd %lu hash %lu \n", pid, vcpu->arch.cr3, fd, hash);
        ht_add_val(mfds,fd,hash);
    }
    ht_close_scan(m->mfds);
   
#ifdef BSD 
    if(vfork == 0)
    {
        ht_add_val(ht_exec_cr3s,vcpu->arch.cr3,parentcr3);
    }
#else
    ht_add_val(ht_exec_cr3s,nu->id,parentcr3);
#endif

    ht_add_val(ht_new_pids_fdinherit,pid,(unsigned long)mfds);
    //m->mfds = NULL;
    remove_targeted_process_core(vcpu->arch.cr3,pid);

    ht_add_val(ht_new_pids,pid,parentcr3);
    clear_unmonitored_cr3_to_pid();
    return 0;
}

//no sysret for execve
int jadukata_sysret_execve(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    //execve fails in BSD sometimes
    unsigned long ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    //if execve failed, then undo the assumptions in syscall above
    if((vcpu->arch.emulate_ctxt.eflags & 1UL))
#else
    if(ret == -1)
#endif
    {
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        unsigned long pid = nu->u.ex.pid;
        unsigned long parentcr3;
        unsigned long fd, hash;
        hash_table* mfds;
        int vfork = 0;
#ifdef BSD
        vfork = (m->expect_iret_handling == pid); 
        if(vfork == 0)
        {
            ht_remove(ht_exec_cr3s,vcpu->arch.cr3);
        }
#else
        ht_remove(ht_exec_cr3s,pid);
#endif
        jprintm("failed execve pid %d vfork %d\n",pid,vfork);

        if(ht_remove_val(ht_new_pids,pid,&parentcr3) ==-1){
            jprintm("ERROR: pid %d not found in ht_new_pids for failed execve\n",pid);
        }

        if(ht_remove_val(ht_new_pids_fdinherit,pid,(unsigned long*)&mfds) == -1){
            jprintm("ERROR: pid %d not found in ht_new_pids_fdinherit for failed execve\n",pid);
        }

        add_targeted_process(vcpu->arch.cr3,parentcr3,pid,kvm_register_read(vcpu, VCPU_REGS_RSP),DEFAULT_STACK_SIZE,vfork,0); 
        
        while(ht_pop_val(mfds, &fd,&hash) != -1){
            jprintl("undoing deletion of monitored pid %lu cr3 %lx fd %lu hash %lu vfork %d\n", pid, vcpu->arch.cr3, fd, hash,vfork);
            if(!vfork){
                add_monitored_fd(vcpu->arch.cr3, fd, hash);
            }
        }
        ht_destroy(mfds); 

    }
    return 0;
}

void jadukata_exit_sleep_scall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk);

void jadukata_exit_sysret_handler(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long val;
    if(ht_lookup_val(ht_fork_parent_pids,nu->id,(unsigned long*)&val) !=0){
        jadukata_exit_sleep_scall(vcpu,nu,fk);
    }else{
        struct process_data* m = get_process_data(vcpu->arch.cr3);
#ifdef BSD
        remove_targeted_process_core(vcpu->arch.cr3,-1); 
#else
        if(fk->orgscall == EXIT_GROUP_SCALL_NR){
            remove_targeted_process_core(vcpu->arch.cr3,-1); 
        }else{
            //linux exit is for calling thread, exit_group is for all threads
            remove_targeted_process_core(vcpu->arch.cr3,nu->id);
        }
#endif
        m->shutdown = true;  
        
        jadukata_fake_restore_org(vcpu,nu,fk);
        fk->last = true;
        nu->fkoffset = 0;
        nu->out_syscall = fk->orgscall;
        //redo syscall 

        vcpu->arch.emulate_ctxt._eip-=2;
        jprintl("fake sysret changing pid %lu rip to %lx \n",nu->id, vcpu->arch.emulate_ctxt._eip);
    }
}

void jadukata_exit_sleep_scall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    unsigned long sleep_time[2] = {3,0};
    unsigned long sleep_time1[2] = {0,0};
    unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
    unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
    unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
    kvm_write_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer, &sleep_time, sizeof(long)*2,&ex); 
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer, &sleep_time1, sizeof(long)*2,&ex); 
    jprintl("jadukata_exit_sleep_scall about to issue nanosleep cr3 %lx fk->buffer %lx arg[s %lu, %lu] rax %lu rdi  %lu rsi %lu rdx %lu orgscall %lu\n", vcpu->arch.cr3, fk->buffer, sleep_time1[0], sleep_time1[1], rax, rdi, rsi, rdx, fk->orgscall );
    jadukata_fake_syscall(vcpu, nu, fk, jadukata_exit_sysret_handler, check_result_negative, (unsigned long)NANOSLEEP_SCALL_NR, 2, fk->buffer,0UL);
}

void jadukata_exit_arg_buffer(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    alloc_process_buffer_secondhalf(vcpu,nu,fk);
    jadukata_fake_syscall(vcpu, nu, fk, jadukata_exit_sleep_scall, dirty_scall_result_check,  (unsigned long)DIRTY_SCALL_NR, 2, fk->buffer,MMAP_SIZE);
}

int jadukata_syscall_exit_helper(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct fakescall *fk = &nu->u.exi.fkd;
    unsigned long val;
    if(ht_lookup_val(ht_fork_parent_pids,nu->id,(unsigned long*)&val) !=0){
        short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu,fk);
        jadukata_fake_save_org(vcpu,nu, fk);
        if(mmap_needed){
            unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
            unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
            unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
            unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
            jprintl("nanosleep about to issue mmap rax %lu rdi %lu rsi %lu rdx %lu orgscall %lu cr3 %lx \n", rax, rdi, rsi, rdx, fk->orgscall, vcpu->arch.cr3);
            //mmap anonymous shared to allocate buffer space
            jadukata_fake_syscall(vcpu, nu, fk, jadukata_exit_arg_buffer, check_result_minus_one, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, MMAP_SIZE, 3UL, MMAP_FLAGS, (unsigned long)-1, 0UL);
        }else{
            jadukata_exit_sleep_scall(vcpu,nu,fk);
        }
    }else{
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        unsigned long scallnr = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
        remove_targeted_process_core(vcpu->arch.cr3,-1); 
#else
        if(scallnr == EXIT_GROUP_SCALL_NR){
            remove_targeted_process_core(vcpu->arch.cr3,-1); 
        }else{
            //linux exit is for calling thread, exit_group is for all threads
            remove_targeted_process_core(vcpu->arch.cr3,nu->id);
        }
#endif
        m->shutdown = true;  
    }
    return 0;
}

int jadukata_syscall_exit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_syscall_exit_helper(prefix,vcpu,nu);
}

int jadukata_sysret_exit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}
    
int jadukata_syscall_exit_group(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return jadukata_syscall_exit_helper(prefix,vcpu,nu);
}

int jadukata_sysret_exit_group(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_sysctl(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int i;
    unsigned long v;
    char data[200] = "", *dataptr = data;
    for(i=0;i<MAXFAKESCALLARGS;i++){
        v = kvm_register_read(vcpu, ARGSMAP[i]);
        if(i==2) nu->u.ctl.buffer = v;
        if(i==3) nu->u.ctl.bufferlen = v;
        dataptr += sprintf(dataptr, "%d:%lu, ", i,v);
    }
    jprintl("sysctl args %s bufferlen %lu buffer %lu\n",data, nu->u.ctl.bufferlen, nu->u.ctl.buffer);
    return 0;
}

int jadukata_sysret_sysctl(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    struct x86_exception ex;
    char buffer[1024];
    unsigned long bufferlen=0;
    unsigned long ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL))
#endif
    {
        jprintl("sysctl ret saved bufferlen %lu buffer %lu \n", nu->u.ctl.bufferlen, nu->u.ctl.buffer);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.ctl.bufferlen, &bufferlen,sizeof(unsigned long), &ex);
        jprintl("bufferlen read is %lu \n", bufferlen);
        if(bufferlen >= 1024){
            bufferlen = 1023;
            buffer[1023] = '\0';
        }
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.ctl.buffer, buffer, bufferlen, &ex);
        jprintl("sysctl ret value is %lu buffer len is %lu buffer  %s \n",ret, bufferlen, buffer);
    }
    return 0;
}

int jadukata_syscall_kill(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int id;
    nu->u.kl.pid = kvm_register_read(vcpu, VCPU_REGS_RDI);
    nu->u.kl.signal = kvm_register_read(vcpu, VCPU_REGS_RSI);
    jprintl("kill syscall pid %d signal %d\n", nu->u.kl.pid, nu->u.kl.signal);
    id = nu->id;
    if(id == nu->u.kl.pid){
        switch(nu->u.kl.signal){
            case 1:
            case 2:
            case 3:
            case 9:
            case 12:
            case 15:
                jprintl("killing self: removing process pid %d signal %d\n", nu->u.kl.pid, nu->u.kl.signal);
                remove_targeted_process_core(vcpu->arch.cr3,id);
                break;
            default:
                jprintl("not killing self signal %d for pid %d\n", nu->u.kl.signal, nu->u.kl.pid);
        }
    }
    return 0;
}

int jadukata_sysret_kill(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintl("kill ret pid %d signal %d ret val %d \n", nu->u.kl.pid, nu->u.kl.signal, ret);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if(ret == 0){
#endif
        unsigned long cr3 = targeted_process_pid_to_cr3(nu->u.kl.pid);
        if(cr3 != 0){
            struct process_data* m = get_process_data(vcpu->arch.cr3);
            switch(nu->u.kl.signal){
                case 1:
                case 2:
                case 3:
                case 9:
                case 12:
                case 10:  // filebench on linux sets handlers for sigusr1 using rt_sigaction but still gets aborted without calling the handler when signal is issued
                case 15:
                    remove_targeted_process_core(cr3,-1);
                    m->shutdown = true;  
                    break;
                default:
                    if(ht_lookup(m->sigactions,nu->u.kl.signal) == 0){
                        remove_targeted_process_core(cr3,-1);
                        m->shutdown = true;  
                        jprinte("killing because no custom sig handler signal %d for pid %d cr3 %lx\n", nu->u.kl.signal, nu->u.kl.pid, cr3);
                    }else{
                        jprinte("ERROR ERRORLEO not killing because there was a custom sig handler signal %d for pid %d cr3 %lx\n", nu->u.kl.signal, nu->u.kl.pid, cr3);
                    }
            }
        }
    }
    return 0;
}

int jadukata_syscall_sigaction(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.kl.signal = kvm_register_read(vcpu, VCPU_REGS_RDI);
    jprintl("sigaction syscall signal %d\n", nu->u.kl.signal);
    return 0;
}

int jadukata_sysret_sigaction(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    int ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long cr3;
    jprintl("sigaction ret val %d \n", ret);
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
#else
    if(ret == 0){
#endif
        struct process_data* m = get_process_data(vcpu->arch.cr3);
        jprintl("adding sigaction custom signal handler for signal %d cr3 %lx \n", nu->u.kl.signal,vcpu->arch.cr3);
        ht_add(m->sigactions,nu->u.kl.signal);
    }
    return 0;
}

int jadukata_syscall_fsync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fsync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_fdatasync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_fdatasync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_getcwd(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_getcwd(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}


int jadukata_syscall_creat(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_open(prefix, vcpu, nu);
}

int jadukata_sysret_creat(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_open(prefix, vcpu, nu);
}

int jadukata_syscall_sync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_sync(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

#ifndef BSD
void nitro_fill_aio_params(struct nitrodatau *nu, struct aio_params* params){
    nu->u.rw.fd = params->fd;  
    nu->u.rw.rw = params->opcode;
    nu->u.rw.ptr = params->buf_addr;
    nu->u.rw.offset = params->offset;
    nu->u.rw.size = params->nbytes;
    nu->u.rw.act_size = params->res;
}

void gather_aio_params(struct kvm_vcpu*vcpu, struct nitrodatau *nu, struct aio_params* params){
    struct x86_exception ex;
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->iocb_ptr + 16, &(params->opcode), sizeof(short),&ex);
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->iocb_ptr + 20, &(params->fd), sizeof(int),&ex);
    if(is_monitored_fd(vcpu->arch.cr3,params->fd)){
        unsigned long slot_ptr;
        unsigned long values[2];
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->iocb_ptr + 24, &(params->buf_addr), sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->iocb_ptr + 32, &(params->nbytes), sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->iocb_ptr + 40, &(params->offset), sizeof(long long),&ex);
        //if there is magin in iocb->data , get ioclass from guest application
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->iocb_ptr, &slot_ptr, sizeof(void*),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, slot_ptr, &values, sizeof(unsigned long) + sizeof(short),&ex);
        if(values[0] == GUEST_APP_AIO_MAGIC){
            nu->u.rw.ioclass = (*((short*)(&(values[1]))))+1;
        }
    }
}

void gather_aio_event_params(struct kvm_vcpu*vcpu, struct nitrodatau *nu, struct aio_params* params, int i){
    struct x86_exception ex;
    if(i>=0){
        params->eventptr = params->asyncio_ptr+(i*32);
        params->iocb_ptrptr = params->eventptr + 8;
        params->res_ptr = params->eventptr + 16; 
        params->res2_ptr = params->eventptr + 24;
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,params->iocb_ptrptr, &(params->iocb_ptr), sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->res_ptr, &(params->res), sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, params->res2_ptr, &(params->res2), sizeof(unsigned long),&ex);
    }
}


int jadukata_syscall_io_setup(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_io_setup(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_io_destroy(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_io_destroy(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_io_getevents(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {

    nu->u.rw.asyncio_minnr = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.rw.asyncio_nr = kvm_register_read(vcpu, VCPU_REGS_RDX);
    nu->u.rw.asyncio_ptr = kvm_register_read(vcpu, VCPU_REGS_R10);
    jprintk("syscall io_getevents minnr %ld nr %ld ptr %lx \n", nu->u.rw.asyncio_minnr, nu->u.rw.asyncio_nr, nu->u.rw.asyncio_ptr);
    return 0;
}

#ifdef JADUKATA_CHECKSUM 
int jadukata_process_reada_writea(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu,bool failed, struct pair* pairs, int* numpairs, int maxpairs){
    int rc = 0;
    rc |= jadukata_translate(vcpu, nu->u.rw.ptr, nu->u.rw.size, pairs, numpairs, maxpairs);
    return rc;
}
#endif

int jadukata_io_getevents_loop(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct x86_exception ex;
    int ret = 0;
    bool fakechain = false;
    while(nu->u.rw.asyncio_i < nu->u.rw.asyncio_nr){
        struct aio_params params;
        params.asyncio_ptr = nu->u.rw.asyncio_ptr;
        fakechain = false;

        gather_aio_event_params(vcpu,nu,&params,nu->u.rw.asyncio_i);
        nu->u.rw.asyncio_i++;
        gather_aio_params(vcpu,nu,&params);
        if(is_monitored_fd(vcpu->arch.cr3,params.fd)){
            jprintk("iogetevents process iocb %lx res %lu res2 %lu buf %lx opcode %hd fd %d bytes %lu offset %lld ioclass %d\n", params.iocb_ptr, params.res, params.res2, params.buf_addr, params.opcode, params.fd, params.nbytes, params.offset,nu->u.rw.ioclass-1);
#ifdef JADUKATA_CHECKSUM
            nitro_fill_aio_params(nu,&params);
            unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
            unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
            //for misaligned writes handle in io_submit
            if(nu->u.rw.rw == 0x0){
                nu->u.rw.skip_eof = skip_eof(vcpu,nu);

                if(shortoff || (excess && (!nu->u.rw.skip_eof))){
                    //since we are initiating a fake chain scall from
                    //iogetevents sysret, set orgscall and decreement eip
                    if(!nu->fkoffset){
                        nu->u.rw.fkd.orgscall = IOGETEVENTS_SCALL_NR;
                        //redo syscall 
                        vcpu->arch.emulate_ctxt._eip-=2;
                    }
                    padded_checksum(vcpu,nu,&nu->u.rw.fkd);
                    fakechain = true;
                }else{
                    ret |= jadukata_process_common(vcpu,prefix,nu,false, jadukata_process_reada_writea);
                }
            }else{
                if((nu->u.rw.size - nu->u.rw.act_size) > 0){
                    ret |= jadukata_process_common(vcpu,prefix,nu,true, jadukata_process_reada_writea);

                }
            }
            unlock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size);
#endif
        }
        if(fakechain){
            break;
        }
    }
    return fakechain;
}

int jadukata_sysret_io_getevents(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu){
    struct x86_exception ex;
    nu->u.rw.asyncio_nr = kvm_register_read(vcpu, VCPU_REGS_RAX);
    nu->u.rw.asyncio_i = 0;
    nu->u.rw.asyncio_fn = jadukata_io_getevents_loop;
    nu->u.rw.asyncio_fn(prefix, vcpu, nu);
    return 0;
}

void jadukata_iosubmit_loopfinish(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    jadukata_sysret_io_submit('l',vcpu,nu);
    freebuffer_lastfake(vcpu,nu,fk);
}

int jadukata_io_submit_loop(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    struct x86_exception ex;
    int ret =0;
    bool fakechain = false;

    while(nu->u.rw.asyncio_i < nu->u.rw.asyncio_nr){
        struct aio_params params;
        fakechain = false;

        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.rw.asyncio_ptr+(nu->u.rw.asyncio_i*sizeof(unsigned long)), &(params.iocb_ptr), sizeof(unsigned long),&ex);
        nu->u.rw.asyncio_i++;
        gather_aio_params(vcpu,nu,&params);
        if(is_monitored_fd(vcpu->arch.cr3,params.fd)){
            jprintk("iosubmit process iocb %lx opcode %hd fd %d buf %lx nbytes %lu offset %lld \n", params.iocb_ptr, params.opcode, params.fd, params.buf_addr, params.nbytes, params.offset);
#ifdef JADUKATA_CHECKSUM
            nitro_fill_aio_params(nu,&params);
            unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
            unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
            lock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size,true);
            //for misaligned writes handle in io_submit
            if(nu->u.rw.rw == 0x1){
                nu->u.rw.skip_eof = skip_eof(vcpu,nu);

                if(shortoff || (excess && (!nu->u.rw.skip_eof))){
                    //save org regs first time we make small write fake chain
                    if(!nu->fkoffset){
                        jadukata_fake_save_org(vcpu,nu,&nu->u.rw.fkd);
                    }
                    padded_checksum(vcpu,nu,&nu->u.rw.fkd);
                    fakechain = true;
                }else{
                    ret |= jadukata_process_common(vcpu,prefix,nu,false, jadukata_process_reada_writea);
                }
            }
#endif
        }
        if(fakechain){
            break;
        }
    }
    return fakechain;
}

//async logic outline:

// for async writes, we calculate checksums during io_submit in the syscall ( if we were to do this in sysret it would add unlikely risk that the i/o might finish before we note down the checksum, but with doing this ins syscall we might do more than one checksum calculations with failures and retries but that is fine )

// for asycn reads, we calculate checksums during the io_getevents sysret ( to work on only successfully finished i/os that the disk has seen )

int jadukata_syscall_io_submit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.rw.asyncio_nr = kvm_register_read(vcpu, VCPU_REGS_RSI);
    nu->u.rw.asyncio_ptr = kvm_register_read(vcpu, VCPU_REGS_RDX);
    jprintk("syscall iosubmit nr %lu ptr %lx \n", nu->u.rw.asyncio_nr, nu->u.rw.asyncio_ptr);

    nu->u.rw.asyncio_i = 0;
    nu->u.rw.asyncio_fn = jadukata_io_submit_loop;
    nu->u.rw.asyncio_fn(prefix, vcpu, nu);
    
    return 0;
}

int jadukata_io_submit_possible_failure(struct kvm_vcpu *vcpu, char prefix, struct nitrodatau *nu) {
    int i = 0;
    struct aio_params params;
    struct x86_exception ex;
    
    for(i = nu->u.rw.num_submitted; i < nu->u.rw.asyncio_nr; i++){
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.rw.asyncio_ptr+(i*sizeof(unsigned long)), &(params.iocb_ptr), sizeof(unsigned long),&ex);
        gather_aio_params(vcpu,nu,&params);
        if(is_monitored_fd(vcpu->arch.cr3,params.fd)){
            jprintk("iosubmit failed process iocb %lx opcode %hd fd %d buf %lx nbytes %lu offset %lld \n", params.iocb_ptr, params.opcode, params.fd, params.buf_addr, params.nbytes, params.offset);
#ifdef JADUKATA_CHECKSUM
            params.res = 0;
            nitro_fill_aio_params(nu,&params);
            unsigned long shortoff = nu->u.rw.offset%(JADU_BLKSIZE);
            unsigned long excess = (nu->u.rw.offset+nu->u.rw.size)%(JADU_BLKSIZE);
            //failure handling
            if(nu->u.rw.rw == 0x1){
                jadukata_process_common(vcpu,prefix,nu,true, jadukata_process_reada_writea);
            }
            unlock_virt_buffer(vcpu,nu->u.rw.ptr,nu->u.rw.size);
#endif
        }
    }
    return 0;
}

int jadukata_sysret_io_submit(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
    nu->u.rw.num_submitted = kvm_register_read(vcpu, VCPU_REGS_RAX);
    jprintk("sysret iosubmit nr %ld syscall nr %lu ptr %lx num_submitted %d \n", nu->u.rw.asyncio_nr, nu->u.rw.asyncio_nr, nu->u.rw.asyncio_ptr, nu->u.rw.num_submitted);
    return jadukata_io_submit_possible_failure(vcpu,prefix,nu);
}

int jadukata_syscall_io_cancel(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_io_cancel(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_remap_file_pages(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_remap_file_pages(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_sync_file_range(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_sync_file_range(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}
#endif

int jadukata_syscall_preadv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_sysret_preadv(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_syscall_pwritev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_sysret_pwritev(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_readv_writev(prefix,vcpu,nu,true);
}

int jadukata_syscall_syncfs(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_syncfs(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_thr_new(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    struct x86_exception ex;
    nu->u.thr.params = kvm_register_read(vcpu, VCPU_REGS_RDI);
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.thr.params+16, &nu->u.thr.stack_base,sizeof(unsigned long), &ex);
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.thr.params+24, &nu->u.thr.stack_size,sizeof(unsigned long), &ex);
    jprintl("thr_new params %lx stack base %lx stack size %ld \n", nu->u.thr.params, nu->u.thr.stack_base, nu->u.thr.stack_size);
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    m->cloning = true;
    return 0;
}

int jadukata_sysret_thr_new(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    struct x86_exception ex;
    unsigned long ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long tid_addr;
    long tid;
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    
#ifdef BSD
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL))
#endif
    {
        if(ret == 0){
            unsigned long eff_stack_base = nu->u.thr.stack_base + nu->u.thr.stack_size; 
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, nu->u.thr.params+48, &tid_addr,sizeof(unsigned long), &ex);
            kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt, tid_addr, &tid,sizeof(long), &ex);
            jprintl("new thread created ret %d tid %ld tid_addr %lx params %lx stack base %lx stack size %ld effective stack base %lx\n", ret, tid, tid_addr, nu->u.thr.params, nu->u.thr.stack_base, nu->u.thr.stack_size, eff_stack_base);
            new_process_helper(vcpu, 1, 0, 1, tid, eff_stack_base,nu->u.thr.stack_size);
        }else{
            jprintl("new thread creation failed ret %d params %lx stack base %lx stack size %ld\n", ret, nu->u.thr.params, nu->u.thr.stack_base, nu->u.thr.stack_size);
        }
    }
    m->cloning = false;
    return 0;
}

int jadukata_syscall_thr_kill(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_syscall_kill(prefix,vcpu,nu);
}

int jadukata_sysret_thr_kill(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return jadukata_sysret_kill(prefix,vcpu,nu);
}

int jadukata_syscall_thr_create(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_thr_create(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_syscall_thr_exit(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_thr_exit(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    if(!(vcpu->arch.emulate_ctxt.eflags & 1UL)){
        remove_targeted_process_core(vcpu->arch.cr3,nu->id);
    }
    return 0;
}

int jadukata_syscall_thr_self(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int jadukata_sysret_thr_self(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

#ifdef BSD 
//TODO: handle failure here
void newproc_getcmd_sysret_handler(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    unsigned long ret = kvm_register_read(vcpu, VCPU_REGS_RAX);
    if(ret == 0){
        int i;
        unsigned long size = MMAP_SIZE-(sizeof(int)*6);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer+(sizeof(int)*4),&size, sizeof(unsigned long),&ex);
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer+(sizeof(int)*6),m->cmd, size,&ex);
        //get just the executable name ignore path
        i = size;
        while(i && m->cmd[--i] != '/');
        strncpy(m->cmd,m->cmd+i+1,size);
    }else{
        unsigned long len = 0;
        kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer+(sizeof(int)*4),&len, sizeof(unsigned long),&ex);
        jprinte("ERROR newproc getcmd sysret handler unsuccessful cr3 %lx mcr3 %lx pid %ld cmd %s ret value %lu \n", vcpu->arch.cr3,m->cr3, m->pid, m->cmd, ret);
        jprinte("ERROR buff len is %lu \n", len);
        jprinte("ERROR sizeof int %d unsigned long %d unsigned long long %d \n",sizeof(int), sizeof(unsigned long), sizeof(unsigned long long));
        m->cmd[0] = '\0';
    }
    jprintl("newproc getcmd sysret handler orgscall %lu cr3 %lx mcr3 %lx pid %ld cmd %s \n", fk->orgscall, vcpu->arch.cr3,m->cr3, m->pid, m->cmd);
    free_process_buffer(vcpu,nu,fk);
    jadukata_fake_restore_org(vcpu,nu,fk);
    
    unsigned long pid = nu->id;
    unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
    unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
    unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
    jprintl("end of getcmd_sysret_handler rax %lu rdi %lu rsi %lu rdx %lu pid %lu \n", rax, rdi, rsi, rdx, pid); 
   
    atomic_dec(&in_new_pid_mode);
    fk->last = true;
    nu->fkoffset = 0;
    nu->out_syscall = fk->orgscall;
    //redo syscall 
    
    vcpu->arch.emulate_ctxt._eip-=2;
    jprintl("fake sysret changing pid %lu rip to %lx \n", pid, vcpu->arch.emulate_ctxt._eip);
    return;
}

void jadukata_getcmd_scall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    int syscall_args[6]={1,14,12,-1,0,0};
    int newarr[6] = {0,0,0,0,0,0};
    struct x86_exception ex;
    *((unsigned long*)(syscall_args+4)) = MMAP_SIZE-(sizeof(int)*6);
    
    unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
    unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
    unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
    jprintl("jadukata_getcmd_scall about to issue sysctl cr3 %lx fk->buffer %lx rax %lu rdi  %lu rsi %lu rdx %lu orgscall %lu\n", vcpu->arch.cr3, fk->buffer, rax, rdi, rsi, rdx, fk->orgscall );
    kvm_write_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer, syscall_args, sizeof(int)*6,&ex); 

    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer,newarr, sizeof(int)*6,&ex);

    jprintl("sysctl params %d %d %d %d %lu\n", newarr[0], newarr[1], newarr[2], newarr[3], *((unsigned long*)(newarr+4)) );

    jadukata_fake_syscall(vcpu, nu, fk, newproc_getcmd_sysret_handler, check_result_minus_one, (unsigned long)SYSCTL_SCALL_NR, 6, fk->buffer,4UL,(fk->buffer+(sizeof(int)*6)),(fk->buffer+(sizeof(int)*4)),0UL,0UL);
    return;
}

#else
void newproc_getcmd_sysret_handler(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    char path[] = "/proc/self/exe";
    unsigned long psize = sizeof(path);
    struct process_data* m = get_process_data(vcpu->arch.cr3);
    unsigned long size = kvm_register_read(vcpu, VCPU_REGS_RAX);
    int i;
    kvm_read_guest_virt_system(&vcpu->arch.emulate_ctxt,fk->buffer+psize,m->cmd, size,&ex);
    //get just the executable name ignore path
    size++; //readlink does not add null byte to the result
    m->cmd[size-1] = '\0';
    i = size;
    while(m->cmd[--i] != '/');
    strncpy(m->cmd,m->cmd+i+1,size);
    jprintk("newproc getcmd sysret handler cr3 %lx mcr3 %lx pid %ld cmd %s \n", vcpu->arch.cr3,m->cr3, m->pid, m->cmd);
    free_process_buffer(vcpu,nu,fk);
    jadukata_fake_restore_org(vcpu,nu,fk);
    
    unsigned long pid = nu->id;
    unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
    unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
    unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
    unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
    jprintl("end of getcmd_sysret_handler rax %lu rdi %lu rsi %lu rdx %lu pid %lu \n", rax, rdi, rsi, rdx, pid); 
   
    atomic_dec(&in_new_pid_mode);
    fk->last = true;
    nu->fkoffset = 0;
    nu->out_syscall = fk->orgscall;
    //redo syscall 
    
    vcpu->arch.emulate_ctxt._eip-=2;
    jprintl("fake sysret changing pid %lu rip to %lx \n", pid, vcpu->arch.emulate_ctxt._eip);
    return;
}

void jadukata_getcmd_scall(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    struct x86_exception ex;
    char path[] = "/proc/self/exe";
    unsigned long size = sizeof(path);
    jprintk("jadukata_getcmd_scall about to issue readlink cr3 %lx \n", vcpu->arch.cr3);
    kvm_write_guest_virt_system(&vcpu->arch.emulate_ctxt,(gva_t)(fk->buffer), (void*)path, size,&ex);
    jadukata_fake_syscall(vcpu, nu, fk, newproc_getcmd_sysret_handler, check_result_minus_one, (unsigned long)READLINK_SCALL_NR, 3, fk->buffer, (fk->buffer+size), MMAP_SIZE-size);
    return;
}

#endif

void jadukata_getcmd_dirty_buffer(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    alloc_process_buffer_secondhalf(vcpu,nu,fk);
    jadukata_fake_syscall(vcpu, nu, fk, jadukata_getcmd_scall, dirty_scall_result_check, (unsigned long)DIRTY_SCALL_NR, 2, fk->buffer,MMAP_SIZE);
}

void new_process_tracking_add_helper(struct kvm_vcpu *vcpu,struct nitrodatau *nu,unsigned long parentcr3, unsigned long pid, unsigned long rsp);

void newproc_getpid_sysret_handler(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long val;
    short forkchild = 0, execcr3 =0;
    unsigned long pid = kvm_register_read(vcpu,VCPU_REGS_RAX);
    unsigned long parentcr3 = 0;
    if(ht_get_size(ht_fork_parent_pids) > 0){
        parentcr3 = targeted_process_pid_to_cr3(nu->u.np.ppid);
    }
    
#ifdef BSD 
    execcr3 = (ht_lookup(ht_exec_cr3s,vcpu->arch.cr3)!=0);
#else
    execcr3 = (ht_lookup(ht_exec_cr3s,pid)!=0);
#endif
        
    jprintl("newproc getpid sysret handler cr3 %lx pid %d nu->u.np.ppid %d execcr3 %d parentcr3 %lx\n", vcpu->arch.cr3, pid, nu->u.np.ppid,execcr3, parentcr3);
    while(down_interruptible(&forklock) != 0){
    }
    if( !execcr3 && (ht_remove_val(ht_fork_parent_pids,nu->u.np.ppid,&val)!=-1) ){
        if(val ==0){
            jprinte("ERROR: val is zero for ppid %d \n",nu->u.np.ppid);
        }
        val--;
        if(val){
            ht_add_val(ht_fork_parent_pids,nu->u.np.ppid,val);
        }
        if(ht_lookup(ht_new_pids,pid) == 0){
            new_process_tracking_add_helper(vcpu,nu,parentcr3, pid,kvm_register_read(vcpu, VCPU_REGS_RSP));
        }
        up(&forklock);
        jprintl("newproc child of fork, so ignoring the artificial syscall cr3 %lx eip is %lx \n", vcpu->arch.cr3, vcpu->arch.emulate_ctxt._eip);
        forkchild = 1;
    }else{
        forkchild = 0;
        up(&forklock);
    }
    
    add_cr3_to_pid(vcpu->arch.cr3, pid);

    ht_remove(ht_new_process_tracking, vcpu->arch.cr3);

    jadukata_fake_restore_org(vcpu,nu,fk);
    atomic_dec(&in_new_pid_mode);
    if(forkchild || (new_process_tracking(vcpu,nu,1) != -2)){
        fk->last = true;
        nu->fkoffset = 0;
        nu->out_syscall = fk->orgscall;
        if(!forkchild){
            //redo syscall
            vcpu->arch.emulate_ctxt._eip-=2;
            jprintl("fake sysret changing rip to %lx \n",vcpu->arch.emulate_ctxt._eip);
        }else{
            unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
            unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
            unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
            unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
            unsigned long r11 = kvm_register_read(vcpu, VCPU_REGS_R11);
            jprintl("fork child skipping the artificial syscall : unchanged eip is %lx rax %lu rdi %lu rsi %lu rdx %lu eflags %lx r11 %lu cr3 %lx \n",vcpu->arch.emulate_ctxt._eip, rax,rdi,rsi,rdx, vcpu->arch.emulate_ctxt.eflags, r11, vcpu->arch.cr3);
        }
        //cr3 to pid mapping changes during vforks
        // so disabling this optimization of caching cr3 to pids 
        //ht_remove(ht_cr3_to_pid,vcpu->arch.cr3);
    }
    return;
}

void newproc_getppid_sysret_handler(struct kvm_vcpu *vcpu, struct nitrodatau *nu, struct fakescall *fk){
    unsigned long val;
    nu->u.np.ppid = kvm_register_read(vcpu,VCPU_REGS_RAX);
    jprintl("newproc getppid sysret handler cr3 %lx ppid %d \n", vcpu->arch.cr3, nu->u.np.ppid);

    jadukata_fake_syscall(vcpu, nu, fk, newproc_getpid_sysret_handler, check_result_negative, (unsigned long)GETPID_SCALL_NR,0);
    return;
}

void new_process_tracking_add_helper(struct kvm_vcpu *vcpu,struct nitrodatau *nu,unsigned long parentcr3, unsigned long pid, unsigned long rsp){
    hash_table* oldmfds = NULL;
    add_targeted_process(vcpu->arch.cr3,parentcr3,pid,rsp,DEFAULT_STACK_SIZE,0,0); 
    htn_remove(ht_new_pids,pid);
    clear_unmonitored_cr3_to_pid();
    if(ht_get_size(ht_new_pids) <= 0){
        clear_all_cr3_to_pid();
    }
    if(htn_remove_val(ht_new_pids_fdinherit,pid,(unsigned long*)&oldmfds) != -1){
        unsigned long fd,hash;
        //add inherited monitored fds
        while(ht_pop_val(oldmfds, &fd,&hash) != -1){
            jprintl("adding inherited monitored pid %lu cr3 %lx fd %lu hash %lu \n", pid, vcpu->arch.cr3, fd, hash);
            add_monitored_fd(vcpu->arch.cr3, fd, hash);
        }
        ht_destroy(oldmfds); 
    }
    return;
}
    

int new_process_tracking(struct kvm_vcpu *vcpu,struct nitrodatau *nu, int aftergetpid){
    int ret = 0;
    if((ht_get_size(ht_new_pids)>0) || (ht_get_size(ht_fork_parent_pids)>0)){
        unsigned long pid;
        unsigned long cpid;
        unsigned long parentcr3;
        int cr3topidval = lookup_cr3_to_pid(vcpu->arch.cr3);
        jprintl("new process tracking cr3 %lx \n", vcpu->arch.cr3);

        if(!aftergetpid && !cr3topidval){
            unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
            unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
            unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
            unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
            
            if(vcpu->nitrodata.nu.fkoffset && (rax == GETPID_SCALL_NR)){
                jprintl("getpid fake insight call rax %lu rdi %lu rsi %lu rdx %lu cr3 %lx \n", rax,rdi,rsi,rdx, vcpu->arch.cr3);
                return 0;
            }

            if(rax != MMAP_SCALL_NR){
                jprintl("new process tracking about to issue getpid/getppid insight call rax %lu rdi %lu rsi %lu rdx %lu cr3 %lx \n", rax,rdi,rsi,rdx, vcpu->arch.cr3);
            }else{
                jprintl("new process tracking postponing issuing getpid/getppid insight call rax %lu rdi %lu rsi %lu rdx %lu cr3 %lx \n", rax,rdi,rsi,rdx, vcpu->arch.cr3);
                return ret;
            }
            
            spin_lock(&newprocesstrackinglock);
            unsigned long val;
            bool skip = false;
            if(ht_lookup_val(ht_new_process_tracking,vcpu->arch.cr3,&val) != 0){
                if(val != nu->id){
                    skip = true;
                }
            }else{
                ht_add_val(ht_new_process_tracking, vcpu->arch.cr3, nu->id);
            }
            spin_unlock(&newprocesstrackinglock);
            if(skip == true){
                jprintl("skipping new process tracking skip is %d, rax %lu rdi %lu rsi %lu rdx %lu cr3 %lx \n", skip, rax,rdi,rsi,rdx, vcpu->arch.cr3);
                return 0;
            }

            struct nitrodatau *nu = &(vcpu->nitrodata.nu);
            struct fakescall *fk = &nu->u.np.fkd;

            atomic_inc(&in_new_pid_mode);

            jadukata_fake_save_org(vcpu,nu, fk);
            if(ht_get_size(ht_fork_parent_pids) > 0){
                jadukata_fake_syscall(vcpu, nu, fk, newproc_getppid_sysret_handler, check_result_negative, (unsigned long)GETPPID_SCALL_NR,0);
            }else{
                jadukata_fake_syscall(vcpu, nu, fk, newproc_getpid_sysret_handler, check_result_negative, (unsigned long)GETPID_SCALL_NR,0);
            }
            return -1;
        }
        cpid = get_guest_pid(vcpu);
        nu->id = cpid;

        ht_open_scan(ht_new_pids);
        while(ht_scan_val(ht_new_pids, &pid,&parentcr3) != -1){
            jprintl("looping in pid %d cpid %d ,cr3 %lx and parentcr3 %lx \n", pid, cpid, vcpu->arch.cr3, parentcr3);
            if(pid == cpid){
                char comm[16];
                get_guest_comm(vcpu,comm);
                jprintl("adding newly monitored process with pid %d comm %s ,cr3 %lx and parentcr3 %lx \n", pid,comm, vcpu->arch.cr3, parentcr3);
                new_process_tracking_add_helper(vcpu,nu,parentcr3, pid, kvm_register_read(vcpu, VCPU_REGS_RSP));
                ret = 1;
                break;
            }else{
#ifdef JADUKATA_VERBOSE
                jprintl("UNMATCHED newly monitored process with pid %d cr3 %lx and parentcr3 %lx but cpid %d \n", pid, vcpu->arch.cr3, parentcr3, cpid);
#endif
            }
        }
        ht_close_scan(ht_new_pids);
    }

    if(ret == 1){
        if(!is_guest_comm_known(vcpu)){
            struct nitrodatau *nu = &(vcpu->nitrodata.nu);
            struct fakescall *fk = &nu->u.np.fkd;
            jprintl("new process tracking guest process cmd unknown cr3 %lx \n", vcpu->arch.cr3);
            jadukata_fake_save_org(vcpu,nu, fk);
            short mmap_needed = alloc_process_buffer_firsthalf(vcpu,nu,fk);
            if(mmap_needed){
                unsigned long rax = kvm_register_read(vcpu, VCPU_REGS_RAX);
                unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
                unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
                unsigned long rdx = kvm_register_read(vcpu, VCPU_REGS_RDX);
                jprintl("new process tracking about to issue mmap rax %lu rdi %lu rsi %lu rdx %lu orgscall %lu cr3 %lx \n", rax, rdi, rsi, rdx, fk->orgscall, vcpu->arch.cr3);
                atomic_inc(&in_new_pid_mode);
                //mmap anonymous shared to allocate buffer space
                jadukata_fake_syscall(vcpu, nu, fk, jadukata_getcmd_dirty_buffer, check_result_minus_one, (unsigned long)MMAP_SCALL_NR, 6, 123456UL, MMAP_SIZE, 3UL, MMAP_FLAGS, (unsigned long)-1, 0UL);
            }else{
                jadukata_getcmd_scall(vcpu,nu,fk);
            }
            return -2;
        }
    }
    return ret;
}

int jadukata_syscall_printsys(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    jprintk("leo leo leo printsyscall\n");
    return 0;
}

int jadukata_sysret_printsys(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    jprintk("leo leo leo printsysret\n");
    return 0;
}


unsigned long inline temp_hash(KEYDT a)
{
    return (((unsigned long)a)*0x9e370001UL);
}

int jadukata_syscall_testnosys(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    int i=0;
    unsigned long val = 0;
    int numpairs=0;
    int maxpairs = PNUM;
    struct pair pairs[PNUM];
    unsigned long csumpartial;

    maxpairs = (nu->u.rw.size/JADU_BLKSIZE);
    unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
    unsigned long size = kvm_register_read(vcpu, VCPU_REGS_RDX);

    jadukata_translate(vcpu, rsi, size, pairs, &numpairs, maxpairs);
    for(i=0;i<numpairs;i++){
        csumpartial = FNVHash((char*)pairs[i].addr,pairs[i].size,csumpartial);
    }
    
    for(i=0;i<100;i++){
         ht_lookup_val(ht_data_checksums_read, csumpartial,&val);
         csumpartial += val;
    }

    unsigned long scallnr = kvm_register_read(vcpu, VCPU_REGS_R10);
    kvm_register_write(vcpu, VCPU_REGS_R10, scallnr);
    return 0;
}

int jadukata_sysret_testnosys(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}


int jadukata_syscall_indir(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu);
int jadukata_sysret_indir(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu);

#define HANDLERS
#define HNAME(name) jadukata_syscall_##name
#include "handlers.h"
#undef HNAME

#define HNAME(name) jadukata_sysret_##name
#include "handlers.h"
#undef HNAME


int jadukata_sysret_indir(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    return 0;
}

int is_expected_indir(unsigned long scallnr){
    return (scallnr < MAX_SYSCALLS) && (scallnr == FORK_SCALL_NR || scallnr == GETPID_SCALL_NR);
}

int jadukata_syscall_indir(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
    unsigned long rdi = kvm_register_read(vcpu, VCPU_REGS_RDI);
    unsigned long rsi = kvm_register_read(vcpu, VCPU_REGS_RSI);
    jprintm("syscall indir rdi %lu rsi %lu\n", rdi, rsi);
    if(is_expected_indir(rdi)){
        nu->out_syscall = rdi;
        kvm_register_write(vcpu, VCPU_REGS_RAX, rdi);
        kvm_register_write(vcpu, VCPU_REGS_RDI, rsi);
        if(jadukata_syscall_handlers[nu->out_syscall]){
            return jadukata_syscall_handlers[nu->out_syscall](prefix,vcpu,nu);
        }
    }else{
        jprinte("ERROR: indir nosys for unexpected scall %ld \n", rdi);
    }
    
    return 0;
}

int jadukata_syscall(char prefix, struct kvm_vcpu *vcpu,struct nitrodatau *nu) {
#ifndef NO_SYSCALL_HANDLING
    nu->prefix = prefix;
    if(!nu->fkoffset){
        if(nu->out_syscall < MAX_SYSCALLS){
            if(jadukata_syscall_handlers[nu->out_syscall]){
                struct timespec ts, te;
                int ret;
                getnstimeofday(&ts);
                ret = jadukata_syscall_handlers[nu->out_syscall](prefix,vcpu,nu);
                getnstimeofday(&te);
                vcpu->nitrodata.scalli_time += TIMEDIFF(te,ts);
                return ret;
            }
        }else{
            jprinte("ERROR bad system call fake %d  out_syscall %d rax %lu %d \n", nu->fkoffset, nu->out_syscall, kvm_register_read(vcpu,VCPU_REGS_RAX), (int)kvm_register_read(vcpu,VCPU_REGS_RAX));
        }
    }
#endif
    return 1;
}

int jadukata_sysret(char prefix, struct kvm_vcpu *vcpu, struct nitrodatau *nu) {
#ifndef NO_SYSCALL_HANDLING
    jprintk("jadukata_sysret fake %d  out_syscall %d ret %lu %d \n", nu->fkoffset, nu->out_syscall, kvm_register_read(vcpu,VCPU_REGS_RAX), (int)kvm_register_read(vcpu,VCPU_REGS_RAX));
    nu->prefix = prefix;
    if(!nu->fkoffset){
        if(nu->out_syscall < MAX_SYSCALLS){
            if(jadukata_sysret_handlers[nu->out_syscall]){
                struct timespec ts, te;
                int ret;
                getnstimeofday(&ts);
                ret = jadukata_sysret_handlers[nu->out_syscall](prefix,vcpu,nu);
                getnstimeofday(&te);
                vcpu->nitrodata.sreti_time += TIMEDIFF(te,ts);
                return ret;
            }
        }else{
            jprinte("ERROR bad system call\n");
        }
    }else{
        jadukata_fake_sysret(vcpu,nu,(struct fakescall*)((((unsigned long)nu) + nu->fkoffset)));
    }
#endif
    return 1;
}

#endif    
