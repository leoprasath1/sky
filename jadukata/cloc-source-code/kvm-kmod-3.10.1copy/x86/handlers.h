#include "../include/linux/jadu.h"

#ifdef BSD

/*
 * list of nitro monitored syscalls for bsd 
 *
 
#define __NR_read 3
#define __NR_write 4
#define __NR_open 5
#define __NR_close 6

#define __NR_stat 38
#define __NR_fstat 62
#define __NR_lstat 40
#define __NR_lseek 478

//todo
#define __NR_mmap 477
#define __NR_munmap 73

#define __NR_pread64 475
#define __NR_pwrite64 476
#define __NR_readv 120
#define __NR_writev 121

#define __NR_msync 65
#define __NR_madvise 75

#define __NR_dup 41
#define __NR_dup2 90

//todo
//bsd new
#define __NR_thr_new 455
#define __NR_thr_kill2 481
#define __NR_thr_create 430
#define __NR_thr_exit 431
#define __NR_thr_self 432
#define __NR_thr_kill 433

#define __NR_rfork 251
#define __NR_fork 2
#define __NR_vfork 66
#define __NR_sys_exit 1
#define __NR_kill 37
#define __NR_execve 59

#define __NR_fsync 95
////not in bsd #define __NR_fdatasync 75
#define __NR_getcwd 326
#define __NR_creat 8
#define __NR_setrlimit 145,195
#define __NR_sync 36


//todo 
#define __NR_io_setup 206
#define __NR_io_destroy 207
#define __NR_io_getevents 208
#define __NR_io_submit 209
#define __NR_io_cancel 210

//not in bsd #define __NR_remap_file_pages 216
//not in bsd #define __NR_sync_file_range 277

////not in bsd #define __NR_dup3 292

//todo
#define __NR_preadv 289
#define __NR_pwritev 290

#define __NR_openat 499

////not in bsd #define __NR_syncfs 306

//same argument order for bsd
arguments order :

c version            %rdi, %rsi, %rdx, %rcx, %r8, %r9
syscall version      %rdi, %rsi, %rdx, %r10, %r8, %r9 


 */
#ifdef HANDLERS
static int (*const HNAME(handlers)[])(char prefix, struct kvm_vcpu* vcpu, struct nitrodatau *nu) = {
#else
char* handler_names[] = {
#undef HNAME
#define HNAME(x) #x
#endif
    [0] = HNAME(indir), 
    //[198] = HNAME(indir), 
    [3] = HNAME(read), 
    [4] = HNAME(write),
    [5] = HNAME(open),
    [6] = HNAME(close),
    [10] = HNAME(unlink),
    [503] = HNAME(unlinkat),
    [38] = HNAME(stat), 
    [62] = HNAME(fstat), 
    [40] = HNAME(lstat), 
    [478] = HNAME(lseek), 
    [479] = HNAME(truncate), 
    [480] = HNAME(ftruncate), 
    //[477] = HNAME(mmap), 
    //[73] = HNAME(munmap),
    [475] = HNAME(pread64), 
    [476] = HNAME(pwrite64), 
    [120] = HNAME(readv), 
    [121] = HNAME(writev), 
    [65] = HNAME(msync), 
    [75] = HNAME(madvise), 
    [41] = HNAME(dup), 
    [90] = HNAME(dup), //dup2 
   // [56] = HNAME(clone),
    [455] = HNAME(thr_new),
    [481] = HNAME(thr_kill),
    [416] = HNAME(sigaction), 
    [433] = HNAME(thr_kill),
    [430] = HNAME(thr_create),
    [431] = HNAME(thr_exit),
    [432] = HNAME(thr_self),
    [2] = HNAME(fork),
    [66] = HNAME(vfork),
    [251] = HNAME(rfork),
    [59] = HNAME(execve),
    [1] = HNAME(exit),
    [37] = HNAME(kill),
    [95] = HNAME(fsync), 
    //[75] = HNAME(fdatasync), 
    [326] = HNAME(getcwd), 
    [8] = HNAME(creat), 
    [145] = HNAME(setrlimit), 
    [36] = HNAME(sync), 
    [202] = HNAME(sysctl), 
   // [206] = HNAME(io_setup),
   // [207] = HNAME(io_destroy), 
   // [208] = HNAME(io_getevents),
   // [209] = HNAME(io_submit),
   // [210] = HNAME(io_cancel), 
   // [216] = HNAME(remap_file_pages), 
   // [231] = HNAME(exit_group), 
    [258] = HNAME(testnosys), 
    [499] = HNAME(openat), 
   // [277] = HNAME(sync_file_range), 
   // [292] = HNAME(dup), //dup3 
    [289] = HNAME(preadv), 
    [290] = HNAME(pwritev), 
   // [306] = HNAME(syncfs), 
};

#ifndef HANDLERS

unsigned long monitored_syscalls[] = {0,1,2,3,4,5,6,8,10,19,36,37,38,40,41,59,62,65,66,75,90,95,120,121,145,202,251,258,289,290,326,416,430,431,432,433,455,475,476,478,479,480,481,499,503};

// linux version
//unsigned long monitored_syscalls[] = {0,1,2,3,4,5,6,8,9,11,17,18,19,20,26,28,32,33,56,57,58,59,60,62,74,75,79,85,160,162,206,207,208,209,210,216,231,257,277,292,295,296,306};

#define NR_SYSCALLS (ARRAY_SIZE(monitored_syscalls))

const int MAX_SYSCALLS = ARRAY_SIZE(handler_names);
const int MAX_STATS = (ARRAY_SIZE(handler_names)+2);
const int UNMON_INDEX = (ARRAY_SIZE(handler_names));
const int FAKE_INDEX = (ARRAY_SIZE(handler_names)+1);

char* fake_name = "FAKE";
char* unmon_name = "UNMON";
char* none_name = "NONE";

#include "allnames.h"

char* syscall_name(int scallnr){
    char *ret = NULL;
    if(unlikely(scallnr<0)){
        printk("ERROR: syscall_name scallnr is negative %d  resetting it to NONE_NAME \n",scallnr);
        ret = none_name;
    }else if(scallnr<MAX_SYSCALLS){
        ret = handler_names[scallnr];
    }else if(scallnr == FAKE_INDEX){
        ret = fake_name;
    }else if(scallnr == UNMON_INDEX){
        ret = unmon_name;
    }else{
        ret = none_name;
    }
    
    if(ret == NULL && scallnr < ALL_SYSCALLS){
        ret = allscall_names[scallnr];
    }

    return ret;
}
#undef HNAME
#endif

#else

/*
 * list of nitro monitored syscalls for linux 
 *
#define __NR_read 0
#define __NR_write 1
#define __NR_open 2
#define __NR_close 3

#define __NR_stat 4
#define __NR_fstat 5
#define __NR_lstat 6
#define __NR_lseek 8

#define __NR_mmap 9
#define __NR_munmap 11
#define __NR_pread64 17
#define __NR_pwrite64 18
#define __NR_readv 19
#define __NR_writev 20

#define __NR_msync 26
#define __NR_madvise 28

#define __NR_dup 32
#define __NR_dup2 33

#define __NR_clone 56
#define __NR_fork 57
#define __NR_vfork 58
#define __NR_exit 60
#define __NR_kill 62

#define __NR_fsync 74
#define __NR_fdatasync 75
#define __NR_getcwd 79
#define __NR_creat 85
#define __NR_setrlimit 160
#define __NR_sync 162

#define __NR_io_setup 206
#define __NR_io_destroy 207
#define __NR_io_getevents 208
#define __NR_io_submit 209
#define __NR_io_cancel 210

#define __NR_remap_file_pages 216
#define __NR_sync_file_range 277

#define __NR_dup3 292

#define __NR_preadv 295
#define __NR_pwritev 296

#define __NR_syncfs 306

arguments order :

c version        %rdi, %rsi, %rdx, %rcx, %r8, %r9
syscall version  %rdi, %rsi, %rdx, %r10, %r8, %r9 

 */
#ifdef HANDLERS
static int (*const HNAME(handlers)[])(char prefix, struct kvm_vcpu* vcpu, struct nitrodatau *nu) = {
#else
char* handler_names[] = {
#undef HNAME
#define HNAME(x) #x
#endif
    [0] = HNAME(read), 
    [1] = HNAME(write),
    [2] = HNAME(open),
    [3] = HNAME(close),
    [4] = HNAME(stat), 
    [5] = HNAME(fstat), 
    [6] = HNAME(lstat), 
    [8] = HNAME(lseek), 
    [9] = HNAME(mmap), 
    [11] = HNAME(munmap),
    [13] = HNAME(sigaction),
    [17] = HNAME(pread64), 
    [18] = HNAME(pwrite64), 
    [19] = HNAME(readv), 
    [20] = HNAME(writev), 
    [26] = HNAME(msync), 
    [28] = HNAME(madvise), 
    [32] = HNAME(dup), 
    [33] = HNAME(dup), //dup2 
    [56] = HNAME(clone),
    [57] = HNAME(fork),
    [58] = HNAME(vfork),
    [59] = HNAME(execve),
    [60] = HNAME(exit),
    [62] = HNAME(kill),
    [74] = HNAME(fsync), 
    [75] = HNAME(fdatasync), 
    [76] = HNAME(truncate), 
    [77] = HNAME(ftruncate), 
    [79] = HNAME(getcwd), 
    [85] = HNAME(creat), 
    [87] = HNAME(unlink),
    [263] = HNAME(unlinkat),
    [160] = HNAME(setrlimit), 
    [162] = HNAME(sync), 
    [206] = HNAME(io_setup),
    [207] = HNAME(io_destroy), 
    [208] = HNAME(io_getevents),
    [209] = HNAME(io_submit),
    [210] = HNAME(io_cancel), 
    [216] = HNAME(remap_file_pages), 
    [229] = HNAME(printsys), 
    [231] = HNAME(exit_group), 
    [257] = HNAME(openat), 
    [277] = HNAME(sync_file_range), 
    [292] = HNAME(dup), //dup3 
    [295] = HNAME(preadv), 
    [296] = HNAME(pwritev), 
    [306] = HNAME(syncfs), 
};

#ifndef HANDLERS

unsigned long monitored_syscalls[] = {0,1,2,3,4,5,6,8,9,11,13,17,18,19,20,26,28,32,33,56,57,58,59,60,62,74,75,76,77,79,85,87,160,162,206,207,208,209,210,216,229,231,257,263,277,292,295,296,306};

#define NR_SYSCALLS (ARRAY_SIZE(monitored_syscalls))

const int MAX_SYSCALLS = ARRAY_SIZE(handler_names);
const int MAX_STATS = (ARRAY_SIZE(handler_names)+2);
const int UNMON_INDEX = (ARRAY_SIZE(handler_names));
const int FAKE_INDEX = (ARRAY_SIZE(handler_names)+1);

char* fake_name = "FAKE";
char* unmon_name = "UNMON";
char* none_name = "NONE";

#include "allnames.h"

char* syscall_name(int scallnr){
    char *ret = NULL;
    if(unlikely(scallnr<0)){
        printk("ERROR: syscall_name scallnr is negative %d  resetting it to NONE_NAME \n",scallnr);
        ret = none_name;
    }else if(scallnr<MAX_SYSCALLS){
        ret = handler_names[scallnr];
    }else if(scallnr == FAKE_INDEX){
        ret = fake_name;
    }else if(scallnr == UNMON_INDEX){
        ret = unmon_name;
    }else{
        ret = none_name;
    }
    
    if(ret == NULL && scallnr < ALL_SYSCALLS){
        ret = allscall_names[scallnr];
    }

    return ret;
}
#undef HNAME
#endif

#endif
