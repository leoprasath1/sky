#define MAX_PAYLOAD (600*4096) /* maximum payload size*/
#define MAX_PAYLOAD_KERNEL (50*4096) /* maximum payload size*/
#define MAX_UBUF_SIZE      (600*4096)
#define GUEST_APP_AIO_MAGIC (0xfedcba9876543210)

#define POW2(y) (1<<y)

