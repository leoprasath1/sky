/*
 * Call block device ioctls from the command line.
 *
 * Compile with gcc -std=gnu99 -D_GNU_SOURCE -Wall -Wextra -Wno-sign-compare -O2
 *
 * Based on blockdev by Andries E. Brouwer. Thanks!
 *
 * Copyright (C) 2010, Marek Polacek <mmpolacek@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* POSIX.1 definitions */
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <locale.h>
#include <libintl.h>
#include <unistd.h>

#ifdef __GNUC__

/* &a[0] degrades to a pointer: a different type from an array */
# define __must_be_array(a) \
	BUILD_BUG_ON_ZERO(__builtin_types_compatible_p(typeof(a), typeof(&a[0])))

#else /* !__GNUC__ */
# define __must_be_array(a)	0
# define __attribute__(_arg_)
#endif /* !__GNUC__ */

/* Force a compilation error if condition is true, but also produce a
 * result (of value 0 and type size_t), so the expression can be used
 * e.g. in a structure initializer (or where-ever else comma expressions
 * aren't permitted).
 */
#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int:-!!(e); }))
#define BUILD_BUG_ON_NULL(e) ((void *)sizeof(struct { int:-!!(e); }))

#ifndef ARRAY_SIZE
# define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]) + __must_be_array(arr))
#endif

#define __noreturn __attribute__((__noreturn__))
#define __unused __attribute__((__unused__))

#define IOCTL_ENTRY(io) .ioc = io, .iocname = # io

#ifndef LOCALEDIR
# define LOCALEDIR "/usr/share/locale"
#endif

#define PACKAGE "wolfie-blockdev"

#define _(Text) gettext(Text)
#ifdef gettext_noop
# define N_(String) gettext_noop(String)
#else
# define N_(String) (String)
#endif

#if !defined(BLKROSET) && defined(__linux__)

#define BLKROSET   _IO(0x12,93)	/* set device read-only (0 = read-write) */
#define BLKROGET   _IO(0x12,94)	/* get read-only status (0 = read_write) */
#define BLKRRPART  _IO(0x12,95)	/* re-read partition table */
#define BLKGETSIZE _IO(0x12,96)	/* return device size /512 (long *arg) */
#define BLKFLSBUF  _IO(0x12,97)	/* flush buffer cache */
#define BLKRASET   _IO(0x12,98)	/* set read ahead for block device */
#define BLKRAGET   _IO(0x12,99)	/* get current read ahead setting */
#define BLKFRASET  _IO(0x12,100)/* set filesystem (mm/filemap.c) read-ahead */
#define BLKFRAGET  _IO(0x12,101)/* get filesystem (mm/filemap.c) read-ahead */
#define BLKSECTSET _IO(0x12,102)/* set max sectors per request (ll_rw_blk.c) */
#define BLKSECTGET _IO(0x12,103)/* get max sectors per request (ll_rw_blk.c) */
#define BLKSSZGET  _IO(0x12,104)/* get block device sector size */

/* ioctls introduced in 2.2.16, removed in 2.5.58 */
#define BLKELVGET  _IOR(0x12,106,size_t) /* elevator get */
#define BLKELVSET  _IOW(0x12,107,size_t) /* elevator set */

#define BLKBSZGET  _IOR(0x12,112,size_t)
#define BLKBSZSET  _IOW(0x12,113,size_t)
#define BLKGETSIZE64 _IOR(0x12,114,size_t) /* return device size in bytes (u64 *arg) */

#endif /* BLKROSET && __linux__ */

/* block device topology ioctls, introduced in 2.6.32 */
#ifndef BLKIOMIN
#define BLKIOMIN   _IO(0x12,120)
#define BLKIOOPT   _IO(0x12,121)
#define BLKALIGNOFF _IO(0x12,122)
#define BLKPBSZGET _IO(0x12,123)
#endif

#ifndef FIFREEZE
#define FIFREEZE   _IOWR('X', 119, int)    /* Freeze */
#define FITHAW     _IOWR('X', 120, int)    /* Thaw */
#endif

#define PROC_PARTITIONS "/proc/partitions"

#ifndef HDIO_GETGEO
# ifdef __linux__
#  define HDIO_GETGEO 0x0301
# endif
struct hd_geometry {
	unsigned char heads;
	unsigned char sectors;
	unsigned short cylinders;	/* Truncated */
	unsigned long start;
};
#endif

static const char *exec_name;		/* Name of the program */

struct bdev {
	long ioc;		/* ioctl code */
	const char *iocname;	/* ioctl name */
	long argval;		/* Default argument */
	const char *name;	/* --set */
	const char *argname;	/* Argument name/NULL */
	const char *help;
	int argtype;
	int flags;
};

/* Command flags */
enum {
	FL_NOPTR = 1 << 1,
	FL_NORESULT = 1 << 2
};

/* ioctl argument types */
enum {
	ARG_NONE,
	ARG_USHRT,
	ARG_INT,
	ARG_UINT,
	ARG_LONG,
	ARG_ULONG,
	ARG_LLONG,
	ARG_ULLONG
};

struct bdev bdevms[] =
{
	{
		IOCTL_ENTRY(BLKROSET),
		.name = "--setro",
		.argtype = ARG_INT,
		.argval = 1,
		.flags = FL_NORESULT,
		.help = N_("set read-only")
	},{
		IOCTL_ENTRY(BLKROSET),
		.name = "--setrw",
		.argtype = ARG_INT,
		.argval = 0,
		.flags = FL_NORESULT,
		.help = N_("set read-write")
	},{
		IOCTL_ENTRY(BLKROGET),
		.name = "--getro",
		.argtype = ARG_INT,
		.argval = -1,
		.help = N_("get read-only")
	},{
		IOCTL_ENTRY(BLKSSZGET),
		.name = "--getss",
		.argtype = ARG_INT,
		.argval = -1,
		.help = N_("get logical block (sector) size")
	},{
		IOCTL_ENTRY(BLKPBSZGET),
		.name = "--getpbsz",
		.argtype = ARG_UINT,
		.argval = -1,
		.help = N_("get physical block (sector) size")
	},{
		IOCTL_ENTRY(BLKIOMIN),
		.name = "--getiomin",
		.argtype = ARG_UINT,
		.argval = -1,
		.help = N_("get minimum I/O size")
	},{
		IOCTL_ENTRY(BLKIOOPT),
		.name = "--getioopt",
		.argtype = ARG_UINT,
		.argval = -1,
		.help = N_("get optimal I/O size")
	},{
		IOCTL_ENTRY(BLKALIGNOFF),
		.name = "--getalignoff",
		.argtype = ARG_INT,
		.argval = -1,
		.help = N_("get alignment offset")
	},{
		IOCTL_ENTRY(BLKSECTGET),
		.name = "--getmaxsect",
		.argtype = ARG_USHRT,
		.argval = -1,
		.help = N_("get max sectors per request")
	},{
		IOCTL_ENTRY(BLKBSZGET),
		.name = "--getbsz",
		.argtype = ARG_INT,
		.argval = -1,
		.help = N_("get blocksize")
	},{
		IOCTL_ENTRY(BLKBSZSET),
		.name = "--setbsz",
		.argname = "BLOCKSIZE",
		.argtype = ARG_INT,
		.flags = FL_NORESULT,
	        .help = N_("set blocksize")
	},{
		IOCTL_ENTRY(BLKGETSIZE),
		.name = "--getsize",
		.argtype = ARG_ULONG,
		.argval = -1,
		.help = N_("get 32-bit sector count")
	},{
		IOCTL_ENTRY(BLKGETSIZE64),
		.name = "--getsize64",
		.argtype = ARG_ULLONG,
		.argval = -1,
		.help = N_("get size in bytes")
	},{
		IOCTL_ENTRY(BLKRASET),
		.name = "--setra",
		.argname = "READAHEAD",
		.argtype = ARG_INT,
		.flags = FL_NOPTR | FL_NORESULT,
		.help = N_("set readahead")
	},{
		IOCTL_ENTRY(BLKRAGET),
		.name = "--getra",
		.argtype = ARG_LONG,
		.argval = -1,
		.help = N_("get readahead")
	},{
		IOCTL_ENTRY(BLKFRASET),
		.name = "--setfra",
		.argname = "FSREADAHEAD",
		.argtype = ARG_INT,
		.flags = FL_NOPTR | FL_NORESULT,
		.help = N_("set filesystem readahead")
	},{
		IOCTL_ENTRY(BLKFRAGET),
		.name = "--getfra",
		.argtype = ARG_LONG,
		.argval = -1,
		.help = N_("get filesystem readahead")
	},{
		IOCTL_ENTRY(BLKFLSBUF),
		.name = "--flushbufs",
		.help = N_("flush buffers")
	},{
		IOCTL_ENTRY(BLKRRPART),
		.name = "--rereadpt",
		.help = N_("reread partition table")
	}
};

static void report_device(char *, int);
static int blkdev_get_sectors(int, unsigned long long *);
static int blkdev_get_size(int, unsigned long long *);

static void __noreturn usage(void)
{
	int i;

	fprintf(stderr, _("Usage:\n"));
	fprintf(stderr, _("  %s -V\n"), exec_name);
	fprintf(stderr, _("  %s --report [devices]\n"), exec_name);
	fprintf(stderr, _("  %s [-v|-q] commands devices\n"), exec_name);
	fputc('\n', stderr);

	fprintf(stderr, _("Available commands:\n"));
	fprintf(stderr, "  %-25s %s\n", "--getsz",
			_("get size in 512-byte sectors"));

	for (i = 0; i < ARRAY_SIZE(bdevms); i++) {
		if (bdevms[i].argname)
			fprintf(stderr, "  %s %-*s %s\n", bdevms[i].name,
					(int) (24 - strlen(bdevms[i].name)),
					bdevms[i].argname, _(bdevms[i].help));
		else
			fprintf(stderr, "  %-25s %s\n", bdevms[i].name,
					_(bdevms[i].help));
	}
	fputc('\n', stderr);
	fprintf(stdout, _("\
This is free software; see the source for copying conditions.  There is NO\n\
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\
"));
	fprintf(stdout, _("Written by %s.\n"), "Marek Polacek");

	exit(EXIT_FAILURE);
}

static int find_cmd(char *s)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(bdevms); i++) {
		if (strcmp(s, bdevms[i].name) == 0)
			return i;
	}

	/* Not found */
	return -1;
}

static int blkdev_get_sectors(int fd, unsigned long long *sectors)
{
	unsigned long long bytes;

	if (blkdev_get_size(fd, &bytes) == 0) {
		*sectors = (bytes >> 9);
		return 0;
	}

	return -1;
}

static int blkdev_get_size(int fd, unsigned long long *bytes)
{
#ifdef BLKGETSIZE64
	if (ioctl(fd, BLKGETSIZE64, bytes) >= 0)
		return 0;
#else
# error "Need BLKGETSIZE64"
#endif
	return 1;
}

static void do_commands(int fd, char *argv[], int d)
{
	int i, j;
	int result;
	int iarg;
	unsigned int uarg;
	unsigned short huarg;
	long int larg;
	long long int llarg;
	unsigned long luarg;
	unsigned long long lluarg;
	int verbose = 0;

	for (i = 1; i < d; i++) {
		if (strcmp(argv[i], "-v") == 0) {
			verbose = 1;
			continue;
		}
		if (strcmp(argv[i], "-q") == 0) {
			verbose = 0;
			continue;
		}
		if (strcmp(argv[i], "--getsz") == 0) {
			result = blkdev_get_sectors(fd, &lluarg);
			if (!result)
				printf("%lld\n", lluarg);
			else
				exit(EXIT_FAILURE);
			continue;
		}

		j = find_cmd(argv[i]);
		if (j == -1) {
			fprintf(stderr, _("%s: Unknown command: %s\n"),
				exec_name, argv[i]);
			usage();
		}

		switch (bdevms[j].argtype) {
		default:
		case ARG_NONE:
			result = ioctl(fd, bdevms[j].ioc, 0);
			break;
		case ARG_USHRT:
			huarg = bdevms[j].argval;
			result = ioctl(fd, bdevms[j].ioc, &huarg);
			break;
		case ARG_INT:
			if (bdevms[j].argname) {
				if (i == d - 1) {
					fprintf(stderr, _("%s requires an argument\n"),
					bdevms[j].name);
					usage();
				}
				iarg = atoi(argv[++i]);
			} else {
				iarg = bdevms[j].argval;
			}
			result = bdevms[j].flags & FL_NOPTR ?
				ioctl(fd, bdevms[j].ioc, iarg) :
				ioctl(fd, bdevms[j].ioc, &iarg);
			break;
		case ARG_UINT:
			uarg = bdevms[j].argval;
			result = ioctl(fd, bdevms[j].ioc, &uarg);
			break;
		case ARG_LONG:
			larg = bdevms[j].argval;
			result = ioctl(fd, bdevms[j].ioc, &larg);
			break;
		case ARG_LLONG:
			llarg = bdevms[j].argval;
			result = ioctl(fd, bdevms[j].ioc, &llarg);
			break;
		case ARG_ULONG:
			luarg = bdevms[j].argval;
			result = ioctl(fd, bdevms[j].ioc, &luarg);
			break;
		case ARG_ULLONG:
			lluarg = bdevms[i].argval;
			result = ioctl(fd, bdevms[j].ioc, &lluarg);
			break;
		}

		if (result == -1) {
			perror(bdevms[j].iocname);
			if (verbose)
				printf(_("%s failed.\n"), _(bdevms[j].help));
			exit(EXIT_FAILURE);
		}

		if (bdevms[j].argtype == ARG_NONE
			|| (bdevms[j].flags & FL_NORESULT)) {
			if (verbose)
				printf(_("%s succeeded.\n"), _(bdevms[j].help));
			continue;
		}

		if (verbose)
			printf("%s: ", _(bdevms[j].help));

		switch (bdevms[j].argtype) {
		case ARG_USHRT:
			printf("%hu\n", huarg);
			break;
		case ARG_INT:
			printf("%d\n", iarg);
			break;
		case ARG_UINT:
			printf("%u\n", uarg);
			break;
		case ARG_LONG:
			printf("%ld\n", larg);
			break;
		case ARG_LLONG:
			printf("%lld\n", llarg);
			break;
		case ARG_ULONG:
			printf("%lu\n", luarg);
			break;
		case ARG_ULLONG:
			printf("%llu\n", lluarg);
			break;
		}
	}
}

static void report_all_devices(void)
{
	FILE *fp;
	char line[256];
	char fname[256];
	char device[256];
	int ma, mi, size;

	fp = fopen(PROC_PARTITIONS, "r");
	if (!fp) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	while (fgets(line, sizeof(line), fp)) {
		if (sscanf(line, " %d %d %d %200[^\n ]",
			&ma, &mi, &size, fname) != 4)
			continue;
		sprintf(device, "/dev/%s", fname);
		report_device(device, 1);
	}

	fclose(fp);
}


static void report_device(char *dev, int quiet)
{
	int fd;
	int ro, ssize, bssize;
	long ra;
	unsigned long long bytes;
	struct hd_geometry geometry;

	ro = ssize = bssize = 0;
	geometry.start = ra = 0;

	fd = open(dev, O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		if (!quiet)
			perror("open");
		return;
	}

	if (ioctl(fd, BLKROGET, &ro) == 0
	    && ioctl(fd, BLKRAGET, &ra) == 0
	    && ioctl(fd, BLKSSZGET, &ssize) == 0
	    && ioctl(fd, BLKBSZGET, &bssize) == 0
	    && ioctl(fd, HDIO_GETGEO, &geometry) == 0
	    && blkdev_get_size(fd, &bytes) == 0) {
		printf("%s %5ld %5d %5d %10ld %15lld   %s\n",
		ro ? "ro" : "rw", ra, ssize, bssize, geometry.start,
		bytes, dev);
	} else {
		if (!quiet)
			perror("ioctl");
	}
	close(fd);
}

static inline void report_header(void)
{
	printf(_("RO    RA   SSZ   BSZ   StartSec            Size   Device\n"));
}

int main(int __unused argc, char *argv[])
{
	int k, j, d, fd;

	exec_name = argv[0];

	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);

	if (argc < 2)
		usage();

    char cmd[1024];
    sprintf(cmd,"echo \"%u\" > /sys/kernel/debug/tracing/set_ftrace_pid",getpid());
    printf("cmd %s \n", cmd);
    system(cmd);

    sleep(3);

	/* Don't allow combining -V with others */
	if (strcmp(argv[1], "-V") == 0 || strcmp(argv[1], "--version") == 0) {
		fprintf(stderr, _("%s (%s)\n"), exec_name, PACKAGE);
		exit(EXIT_FAILURE);
	}

	/* Don't allow combining --report with others */
	if (strcmp(argv[1], "--report") == 0) {
		report_header();
		if (argc > 2) {
			for (d = 2; d < argc; d++)
				report_device(argv[d], 0);
		} else {
			report_all_devices();
		}
		exit(EXIT_SUCCESS);
	}

	/* Do each of the commands on each of the devices */
	for (d = 1; d < argc; d++) {
		j = find_cmd(argv[d]);
		if (j >= 0) {
			if (bdevms[j].argname)
				d++;
			continue;
		}
		if (strcmp(argv[d], "--getsz") == 0)
			continue;
		if (strcmp(argv[d], "--") == 0) {
			d++;
			break;
		}
		if (argv[d][0] != '-')
			break;
	}

	if (d >= argc)
		usage();

	for (k = d; k < argc; k++) {
		fd = open(argv[k], O_RDONLY, 0);
		if (fd < 0) {
			perror(argv[k]);
			exit(EXIT_FAILURE);
		}
		do_commands(fd, argv, d);
		close(fd);
	}

	exit(EXIT_SUCCESS);
}
