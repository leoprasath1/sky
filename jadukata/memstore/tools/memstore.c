#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "../memstore_ioctl.h"

#define DEV             "/dev/MEMSTORE0"

int main(int argc, char *argv[])
{
        int fd;

        if (argc < 2) {
                printf("Usage: memstore <enable_logging/disable_logging>\n");
                return -1;
        }

        fd = open(DEV, O_RDONLY);

        if (strcmp(argv[1], "enable_logging") == 0){
                fprintf(stderr, "enabling logging \n");
                ioctl(fd, ENABLE_LOGGING);
        }
        else
        if (strcmp(argv[1], "disable_logging") == 0){
                fprintf(stderr, "disabling logging \n");
                ioctl(fd, DISABLE_LOGGING);
        }
        else
        if (strcmp(argv[1], "get_stats") == 0){
                fprintf(stderr, "get_stats \n");
                ioctl(fd, GET_STATS);
        }
        else
        if (strcmp(argv[1], "clear_stats") == 0){
                fprintf(stderr, "clear_stats \n");
                ioctl(fd, CLEAR_STATS);
        }
        else {
                fprintf(stderr, "Invalid command\n");
        }

        return 1;
}
