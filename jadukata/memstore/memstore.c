#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/genhd.h>
#include <linux/interrupt.h>
#include <linux/completion.h>
#include <linux/buffer_head.h>
#include <linux/bio.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/blkdev.h>
#include <linux/hdreg.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/stat.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/fcntl.h>
#include <linux/random.h>
#include <linux/jbd.h>
#include <linux/workqueue.h>
#include <linux/kthread.h>
#include <linux/bitmap.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kallsyms.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include <asm/checksum.h>
#include <asm/stacktrace.h>
#include <linux/kallsyms.h>


#include <linux/kdev_t.h>
#include <linux/bio.h>
#include <linux/blkdev.h>
#include <linux/blk_types.h>
#include <linux/rwlock.h>

//#define mprintk(a...)
//#define mprinte(a...)

#define DEVICE_NAME 	"MEMSTORE"

#define mprinte(x,y...) do { printk( KERN_ERR "%s %s:%d - %d %s %d - " x, DEVICE_NAME, __FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##y); } while (0);


#define mprintk(x,y...) do { if(enable_logging) printk( KERN_ERR "%s %s:%d - %d %s %d - " x, DEVICE_NAME, __FUNCTION__,__LINE__,current->pid, current->comm, smp_processor_id(), ##y); } while (0);

#define MEMSTORE_HARDSECT		(512)		/* sector size*/
#include "memstore_ioctl.h"

#define CHUNK_SIZE (64*1024) // in sectors
#define CHUNK_BYTES (CHUNK_SIZE * 512) // in sectors

int module_init = 0;
int enable_logging = 0;

char** memstore = NULL;

int memstore_major;       /* 0 - let the system assign the major no */

atomic_t stats_totr, stats_totw;

typedef struct _memstore_dev {
    unsigned long size;
    spinlock_t lock;
    struct gendisk *gd;
    int usage;
}memstore_dev;

/*This holds the memstore device related info*/
memstore_dev memstore_device;
struct block_device * memstore_blk_dev;
struct workqueue_struct *memstore_wkq = NULL;

static struct request_queue *memstore_queue;
unsigned long MEMSTORE_SIZE = 0;
module_param(MEMSTORE_SIZE, ulong, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(MEMSTORE_SIZE, "fake MEMSTORE size in SECTORS integer");


int memstore_ioctl(struct block_device *blkdev, fmode_t mode,
        unsigned int cmd, unsigned long arg)
{
    mprintk("ioctl %d \n", cmd);
	switch (cmd) {

        case ENABLE_LOGGING:
           	enable_logging++;
            break;

        case DISABLE_LOGGING:
            enable_logging = 0;
            break;

        case GET_STATS:
            mprintk("stats totr %d totw %d \n", atomic_read(&stats_totr), atomic_read(&stats_totw));
            break;
        
        case CLEAR_STATS:
            mprintk("stats totr %d totw %d \n", atomic_read(&stats_totr), atomic_read(&stats_totw));
            mprintk("cleared stats");
            atomic_set(&stats_totr,0);
            atomic_set(&stats_totw,0);
            break;

	}

	return 0;
}

int memstore_media_changed(struct gendisk *gd)
{
	return 1;
}

int memstore_revalidate_disk(struct gendisk *gd)
{
	return 0;
}

int memstore_open(struct block_device *bdev, fmode_t mode)
{
    int num = iminor(bdev->bd_inode);
    if (num >= 1) {
        return -ENODEV;
    }

    memstore_device.usage ++;

	return 0;
}

/*Close the device. Decrement the usage count.*/
void memstore_release(struct gendisk* gdisk, fmode_t mode)
{
    struct block_device *bd = bdget(disk_devt(gdisk));
    memstore_device.usage --;

    if (!memstore_device.usage) { 
        fsync_bdev(bd);
    }
}


struct block_device_operations memstore_bdops = {
	.owner = THIS_MODULE,
	.open = memstore_open,
	.release = memstore_release,
	.ioctl = memstore_ioctl,
	.media_changed = memstore_media_changed,
	.revalidate_disk = memstore_revalidate_disk
};


void copy_helper(struct bio* memstore_bio,char* dest, char* src, int size){
    char* temp = NULL;
    if ( bio_data_dir(memstore_bio) == WRITE ) {
        //swap dest, src
        temp = dest;
        dest = src;
        src = temp;
    }

    if(memstore && dest && src){
        memcpy(dest,src,size);
    }else{
        mprinte("ERROR null dereference memstore : %p  dest : %p src : %p\n", (void*)memstore, (void*)dest, (void*)src);
    }
}

void memstore_new_request (struct request_queue* queue, struct bio *memstore_bio) {
	int size,i,mem_offset, chunknum, remaining_bytes = 0;
    int memstore_log = 0;
    char* dest = NULL, *src = NULL;

	struct bio_vec *bv;
	void *addr;
    
    if(bio_data_dir(memstore_bio) == WRITE){
        atomic_add(bio_sectors(memstore_bio),&stats_totw);
    }else{
        atomic_add(bio_sectors(memstore_bio),&stats_totr);
    }

	if(enable_logging)
		mprintk("memstore_make_request sector %ld sectors %d r/w %lu\n",memstore_bio->bi_sector,bio_sectors(memstore_bio),bio_data_dir(memstore_bio));

	if( !bio_sectors(memstore_bio) || !module_init ){
		mprintk("going to out !bio_sectors %d \n", !bio_sectors(memstore_bio));
		 goto out;
	}

	if( (memstore_bio->bi_sector < 0 || memstore_bio->bi_sector > (MEMSTORE_SIZE)) ){
		mprinte("ERROR incorrect sector number %ld\n",memstore_bio->bi_sector);
		goto out;
	} 

	chunknum = ((memstore_bio->bi_sector/CHUNK_SIZE));
	mem_offset = ((memstore_bio->bi_sector%CHUNK_SIZE)*512);

	bio_for_each_segment(bv,memstore_bio,i){
		if(memstore_log)	
			mprintk("memstore_make_request bio_vec page: %p len: %d offset: %d\n",(void*)bv->bv_page, bv->bv_len, bv->bv_offset);
       
		size = bv->bv_len;
		addr = kmap_atomic(bv->bv_page);
		dest = addr + bv->bv_offset;
        if((mem_offset + size) > CHUNK_BYTES){
            remaining_bytes = (mem_offset + size) - CHUNK_BYTES;
            size = (CHUNK_BYTES-mem_offset);
        }
		src = memstore[chunknum] + mem_offset;
    
        if(memstore_log)	
            mprintk("memstore dest %p src %p size %d mem_offset %d \n",(void*)dest,(void*)src,size, mem_offset);

        copy_helper(memstore_bio, dest, src, size);
        
        if(remaining_bytes){
            dest = addr + bv->bv_offset  + size;
            chunknum++;
            mem_offset = 0;
            src = memstore[chunknum];
            size = remaining_bytes;

            copy_helper(memstore_bio, dest, src, size);

            remaining_bytes = 0;
        }


		kunmap_atomic(addr);
		mem_offset += size;
	}

out:
    bio_endio(memstore_bio,0);
}

int memory_cleanup (void) {
    int i;
    int num_chunks = ((MEMSTORE_SIZE/CHUNK_SIZE)+1);
	if(memstore){
        for(i=0;i<num_chunks;i++){
            if(memstore[i]){
                vfree(memstore[i]);
                memstore[i] = NULL;
            }
        }
        kfree(memstore);
        memstore = NULL;
    }
	return 0;
}

int memory_init(void) {
    int i;
    int num_chunks = ((MEMSTORE_SIZE/CHUNK_SIZE)+1);
    int chunk_bytes = (CHUNK_SIZE*512);
    if(MEMSTORE_SIZE == 0){
		mprinte("ERROR MEMSTORE size is 0 sectors\n");
        return 1;
    }
	memstore = (char**)kzalloc(num_chunks*sizeof(char*),GFP_KERNEL);
	if(memstore == NULL){
		mprinte("ERROR could not allocate memory 1\n");
        memory_cleanup();
        return 1;
	}
    for(i=0;i<num_chunks;i++){
        memstore[i] = (char*)vmalloc(chunk_bytes);
        if(memstore[i] == NULL){
            mprinte("ERROR could not allocate memory 2\n");
            memory_cleanup();
            return 1;
        }
    }
    mprinte("memstore allocated memory successfully\n");
	return 0;
}


/* This method will initialize the device specific structures */
int __init memstore_init(void)
{
    int nsectors;
    char temp_name[20];
    
    if(memory_init()){
        return -ENOMEM;
    }

    spin_lock_init(&(memstore_device.lock));

    memstore_device.size = MEMSTORE_SIZE;
    nsectors = memstore_device.size;

    memstore_queue = blk_alloc_queue(GFP_KERNEL);
    if (!memstore_queue) {
        mprinte( "ERROR memstore: queue mem alloc fails\n");
        goto out;
    }
    blk_queue_physical_block_size(memstore_queue, MEMSTORE_HARDSECT);
    blk_queue_make_request(memstore_queue, memstore_new_request);

    /* Register */
    sprintf(temp_name,"%s0",DEVICE_NAME);
    memstore_major = register_blkdev(0, temp_name);

    if (memstore_major <= 0) {
        mprinte( "ERROR memstore: can't get major \n");
        goto out_unregister;
    }

    /* Add the gendisk structure */
    memstore_device.gd = alloc_disk(1);
    if (!memstore_device.gd) {
        goto out_unregister;
    }
    memstore_device.gd->major = memstore_major;
    memstore_device.gd->first_minor = 0;
    memstore_device.gd->fops = &memstore_bdops;
    memstore_device.gd->private_data = &memstore_device;
    memstore_device.gd->queue = memstore_queue;
    strcpy(memstore_device.gd->disk_name, temp_name);
    set_capacity(memstore_device.gd, nsectors);

    add_disk(memstore_device.gd);

   
    if(memstore_wkq == NULL){
        memstore_wkq = create_workqueue("memstore_queue");
    }

    mprinte( "MEMSTORE init over ... successfully added the driver (total secs em_disk : %lu )\n", MEMSTORE_SIZE);

    module_init = 1;
    return 0;

out_unregister:
    sprintf(temp_name,"%s0",DEVICE_NAME);
    unregister_blkdev(memstore_major, temp_name);
out:
    mprinte( "ERROR Unable to load the device\n");
    return -ENOMEM;
}

/*
 * Free up the allocated memory and cleanup.
 */
static __exit void memstore_cleanup(void)
{

    char *temp_name = (char*) kmalloc((4096*32),GFP_KERNEL);
    mprintk( "MEMSTORE cleanup starting ...\n");

    if(memstore_wkq != NULL){
        flush_workqueue(memstore_wkq);
        destroy_workqueue(memstore_wkq);
        memstore_wkq = NULL;
    }

    sprintf(temp_name,"%s0",DEVICE_NAME);
    del_gendisk(memstore_device.gd);
    put_disk(memstore_device.gd);
    unregister_blkdev(memstore_major, temp_name);

    blk_cleanup_queue(memstore_queue);

    if(memstore_blk_dev) blkdev_put(memstore_blk_dev, FMODE_READ | FMODE_WRITE );
    memstore_blk_dev = NULL;

    kfree(temp_name);
    
    memory_cleanup();

    module_init = 0;

    mprintk( "MEMSTORE cleanup over ... exiting\n");
}

module_init(memstore_init);
module_exit(memstore_cleanup);

MODULE_LICENSE("GPL");
