You can use this code for academic purposes with others in your research team working on the project.
We have plans to open-source this in a few months time.
Until then, please do not share this code with others outside your research group.

To run:

Compile the modules in folders kvm-kmod-3.10.1 , diskdriver ,  dm-dedup, memstore, and the cmdclient.c,cmdserver.c in main folder : all by running make command
Then run the start_vm.sh script which will startup the guest vm.
After this, login to the guest vm using command " ssh -Y -p 2222 arulraj@localhost "
Inside the vm, run the following:
    - cd /usr/home/arulraj/cerny/jadukata/
    - change the #define SERVER_IP "128.105.104.152" to IP address of host machine and then compile using make
    - Run : ./dedupgpgclient.o 1 1  (with unique hints)
    - Run :  ./dedupgpgclient.o -1 0  ; (without unique hints)
    - these benchmarks output lots of statistics at different points in execution
    - If you look for the following stats after the gpg command completion, you can see amount of unique hints I/O vs duplicate I/o
    "before: unique 1031200 duplicate 72 hits[init-0,war-0,write-0] inserts 41 hints[war-0,w-192448] count 128929 time 59087872 (usecs) prio : time1total[0] 0 time1avg 0 (nsecs) time2total[0] 0 time2avg 0 (nsecs)  normal : time1total[1] 694925641 time1avg 3610 (nsecs) time2total[1] 825266206640 time2avg 4287319 (nsecs) cache ht_size 12 work ht_size 0
after: unique 0 duplicate 0 hits[init-0,war-0,write-0] inserts 0 hints[war-0,w-0] count 0 time 0 (usecs) prio : time1total[0] 0 time1avg 0 (nsecs) time2total[0] 0 time2avg 0 (nsecs)  normal : time1total[1] 0 time1avg 0 (nsecs) time2total[1] 0 time2avg 0 (nsecs) cache ht_size 12 work ht_size 0"


In the kvm-kmod-3.10.1 module:
    The code for system call interception is in file: emulate.c, nitro.c
    The code for selective interception, insight calls etc is spread out under files: jadukata.c ,nitro.c

Details:

This was tested extensively on the host machine with Intel 64 bit processor. I suggest you also run in an
Intel 64 bit host machine with host running a 64 bit OS.
I have given the guest OS images (both BSD and Linux) as a Qemu raw image file.
The host machine needs approx. 4 GB for the in memory cache device.

The modified kvm module is in folder kvm-kmod-3.10.1
The pseudo disk driver is in folder diskdriver
The modified dmdedup  module is in folder dm-dedup
The memstore module to create an in-memory disk is in folder memstore

The config header for kvm module is kvm-kmod-3.10.1/include/linux/jadu.h
The config header for pseudo disk driver is diskdrver/includes.h

The guest is run using qemu-2.5.0 which is in a folder named qemu-2.5.0

The guest launched using the script named "start_nitro.sh" - This script has several config variables:
    - location of qemu binary to use etc.
    - the guest OS image file
    - the 80GB virtual disk image which will be enhanced using the 4GB in memory cache
    - how much memory to use for qemu guest ( variable named "mem" )
    - to compile the diskdriver module you also need to have a header file named pdev.h in homedirectory
      with the major minor number of the 80GB physical backing disk:
      e.g.
        //static int pdev[] = {MKDEV(8, 18)}; // backing disk
        //static int pdev[] = {MKDEV(8, 36)}; // backing disk
        
        static int pdev[] = {MKDEV(253, 0)}; // backing disk
        //static int pdev[] = {MKDEV(249, 0)}; // backing disk
        //static int pdev[] = {MKDEV(252, 0)}; // backing disk
        //static int pdev[] = {MKDEV(251, 0)}; // backing disk
        
        #ifdef EXTCACHE
        static int cdev[] = {MKDEV(8, 37)}; // cache disk
        #endif

        where each pair is the major number and the minor number of the backing physical disk.
        The major number 253 and minor 0 is for the dmdedup device ( you might need to change these numbers
        based on the numbers that are assigned to your dmdedup psedodevice - 
    - this script start the cmdserver process which listens to commands from the guest benchmark application.

 



